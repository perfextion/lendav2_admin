﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlTypes;
using System.Web.UI;

public class BasePage : Page
{
    protected static string dbKey = "gp_conn";

    protected static string userid = string.Empty;

    protected static bool IsDevAdmin = false;
    protected static bool IsSysAdmin = false;
    protected static bool IsRiskAdmin = false;
    protected static bool IsAdmin = false;

    protected static string Table_Name = string.Empty;
    protected static string Count_Query = string.Empty;

    protected static string NUMBER_REGEX = @"\d+\.*\d+";

    protected static string Loan_Full_ID = string.Empty;

    public BasePage()
    {
        Count_Query = string.Empty;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserID"] == null)
        {
            Response.Redirect("~/Login.aspx");
        }
        else
        {
            userid = Session["UserID"].ToString();
            IsSysAdmin = Session["IsSysAdmin"].ToString() == "1";
            IsDevAdmin = Session["IsDevAdmin"].ToString() == "1";
            IsRiskAdmin = Session["IsRiskAdmin"].ToString() == "1";
            IsAdmin = Session["IsAdmin"].ToString() == "1";
        }

        if(Session["Loan_Full_ID"] != null)
        {
            Loan_Full_ID = Session["Loan_Full_ID"].ToString();
        }
    }

    protected static void Update_Table_Audit()
    {
        gen_db_utils.Update_Table_Audit_Trail(Table_Name, userid, dbKey);
    }

    protected static string Get_Table_Info()
    {
        try
        {
            if(string.IsNullOrEmpty(Count_Query))
            {
                Count_Query = string.Format(@"DECLARE @Count int;
                                Select @Count = Count(*)  from {0}", Table_Name);
            }

            string sql_query = Count_Query;
            sql_query += Environment.NewLine;
            sql_query += string.Format(
                            @"SELECT
	                            Table_UI_Name as [Table_Name],
                                Sys_Edit_Ind,
	                            Dev_Edit_Ind,
	                            Risk_Edit_Ind,
	                            CONCAT('Records Count: ', @Count) as [Records_Count],
	                            CASE
		                            WHEN Modified_By IS NOT NULL THEN CONCAT('Last Updated by: ', DBO.GetUserNameByUserID(Modified_By), ' ', FORMAT( Modified_On, 'MM/dd/yyyy hh:mm:ss tt', 'en-US' ))
		                            ELSE 'Last Updated by: '
	                            END AS [Last_Updated]
                            FROM Table_Audit_Trail
                            WHERE Table_Name = '{0}'", Table_Name);

            DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_query, dbKey);

            var obj = new
            {
                Table_Info = dt,
                CanEdit = CheckCanEdit(dt.Rows[0])
            };
            return JsonConvert.SerializeObject(obj);
        }
        catch
        {
            return JsonConvert.SerializeObject(new
            {
                Table_Info = new List<object>
                {
                    new object()
                },
                CanEdit = false
            });
        }
    }

    protected static bool CheckCanEdit(DataRow row)
    {
        var canEdit = false;

        if (row != null)
        {
            var Sys_Edit_Ind = row["Sys_Edit_Ind"].ToString() == "1";
            var Dev_Edit_Ind = row["Dev_Edit_Ind"].ToString() == "1";
            var Risk_Edit_Ind = row["Risk_Edit_Ind"].ToString() == "1";

            if (IsDevAdmin)
            {
                canEdit = Dev_Edit_Ind;
            }            

            if (IsSysAdmin)
            {
                canEdit = canEdit || Sys_Edit_Ind;
            }

            if (IsRiskAdmin)
            {
                canEdit = canEdit || Risk_Edit_Ind;
            }
        }
        return canEdit;
    }

    protected static dynamic Get_ID(dynamic Id)
    {
        if(Id != null)
        {
            Id = Id.ToString();
        }

        if (string.IsNullOrEmpty(Id))
        {
            return SqlInt32.Null;
        }
        return Id;
    }

    protected static string TrimSQL(dynamic str)
    {
        if(!string.IsNullOrEmpty(str))
        {
            return str.ToString().Replace("'", "''")
                .Replace("--", "&ndash;")
                .Replace("@@", "@ @")
                .Replace("truncate", "''truncate''")
                .Replace("drop", "''drop''")
                .Replace("delete", "''delete''");
        }
        else
        {
            return string.Empty;
        }
    }

    protected static dynamic Get_Value(dynamic item, string key)
    {
        try
        {
            return item[key].ToString();
        }
        catch
        {
            return string.Empty;
        }
    }
}