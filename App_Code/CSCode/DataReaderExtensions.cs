﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Reflection;
using System.Web;


public static class DataReaderExtensions
{
    /// <Summary>
    /// Map data from DataReader to list of objects
    /// </Summary>
    /// <typeparam name="T">Object</typeparam>
    /// <param name="dr">Data Reader</param>
    /// <returns>List of objects having data from data reader</returns>
    public static List<T> MapToList<T>(this DbDataReader dr) where T : new()
    {
        List<T> RetVal = null;
        var Entity = typeof(T);
        var PropDict = new Dictionary<string, PropertyInfo>();
        try
        {
            if (dr != null && dr.HasRows)
            {
                RetVal = new List<T>();
                var Props = Entity.GetProperties(BindingFlags.Instance | BindingFlags.Public);
                PropDict = Props.ToDictionary(p => p.Name.ToUpper(), p => p);
                while (dr.Read())
                {
                    T newObject = new T();
                    for (int Index = 0; Index < dr.FieldCount; Index++)
                    {
                        if (PropDict.ContainsKey(dr.GetName(Index).ToUpper()))
                        {
                            var Info = PropDict[dr.GetName(Index).ToUpper()];
                            if (Info.Name == "ProposedAIP")
                            {

                            }
                            if ((Info != null) && Info.CanWrite)
                            {
                                var Val = dr.GetValue(Index);
                                Info.SetValue(newObject, (Val == DBNull.Value) ? null : Val, null);
                            }
                        }
                    }
                    RetVal.Add(newObject);
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        return RetVal;
    }



    /// <Summary>
    /// Map data from DataReader to list of objects
    /// </Summary>
    /// <typeparam name="T">Object</typeparam>
    /// <param name="dr">Data Reader</param>
    /// <returns>List of objects having data from data reader</returns>
    public static List<String> MapToListString(this DbDataReader dr)
    {
        List<String> RetVal = null;
        var Entity = typeof(String);
        var PropDict = new Dictionary<string, PropertyInfo>();
        try
        {
            if (dr != null && dr.HasRows)
            {
                RetVal = new List<String>();
                var Props = Entity.GetProperties(BindingFlags.Instance | BindingFlags.Public);
                PropDict = Props.ToDictionary(p => p.Name.ToUpper(), p => p);
                while (dr.Read())
                {
                    string newObject = "";
                    for (int Index = 0; Index < dr.FieldCount; Index++)
                    {
                        if (PropDict.ContainsKey(dr.GetName(Index).ToUpper()))
                        {
                            var Info = PropDict[dr.GetName(Index).ToUpper()];
                            if (Info.Name == "ProposedAIP")
                            {

                            }
                            if ((Info != null) && Info.CanWrite)
                            {
                                var Val = dr.GetValue(Index);
                                Info.SetValue(newObject, (Val == DBNull.Value) ? null : Val, null);
                            }
                        }
                    }
                    RetVal.Add(newObject);
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        return RetVal;
    }
    /// <Summary>
    /// Map data from DataReader to an object
    /// </Summary>
    /// <typeparam name="T">Object</typeparam>
    /// <param name="dr">Data Reader</param>
    /// <returns>Object having data from Data Reader</returns>
    public static T MapToSingle<T>(this DbDataReader dr) where T : new()
    {
        T RetVal = new T();
        var Entity = typeof(T);
        var PropDict = new Dictionary<string, PropertyInfo>();
        try
        {
            if (dr != null && dr.HasRows)
            {
                var Props = Entity.GetProperties(BindingFlags.Instance | BindingFlags.Public);
                PropDict = Props.ToDictionary(p => p.Name.ToUpper(), p => p);
                dr.Read();
                for (int Index = 0; Index < dr.FieldCount; Index++)
                {
                    if (PropDict.ContainsKey(dr.GetName(Index).ToUpper()))
                    {

                        var Info = PropDict[dr.GetName(Index).ToUpper()];
                        //if(Info.Name=='ActionStatus')
                        if ((Info != null) && Info.CanWrite)
                        {
                            var Val = dr.GetValue(Index);
                            Info.SetValue(RetVal, (Val == DBNull.Value) ? null : Val, null);
                        }
                    }
                }
            }
        }
        catch (Exception)
        {
            throw;
        }
        return RetVal;
    }
}