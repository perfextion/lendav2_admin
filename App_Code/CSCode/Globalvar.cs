﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

public class Globalvar
{
    public enum Preferred_Contacts
    {
        Phone = 0,
        Email = 1,
        Contact = 2,
        contact1 = 3,
        contact2 = 4,
        contact3 = 5
    }
    public enum Preferred_Contacts1
    {
        Phone1 = 0,
        Email = 1,
        Contact = 2,
        contact1 = 3,
        contact2 = 4,
        contact3 = 5
    }
    public enum Preferred_Contacts2
    {
        Phone2 = 0,
        Email = 1,
        Contact = 2,
        contact1 = 3,
        contact2 = 4,
        contact3 = 5
    }
    public enum Status
    {
        Active = 0,
        InAcctive = 1,
    }
    public enum Preferred_Contacts4
    {
        Phone = 0,
        Email = 1,
        Contact = 2,
        contact1 = 3,
        contact2 = 4,
        contact3 = 5
    }
    public enum Budget_Expences
    {
        Budget = 0,
        Expence_Type = 1,
        Expence_Type_2 = 2,
        Expence_Type_3 = 3,
        Expence_Type_4 = 4,
        Expence_Type_5 = 5,
    }
    public enum fieldids
    {
        [Description("Agency")] AGY,
        [Description("Buyer")] BUY,
        [Description("Distributor")] DIS,
    }
}

