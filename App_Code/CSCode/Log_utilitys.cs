﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Log_utilitys
/// </summary>
/// 

public class Log_utilitys
{
    static string dbKey = "gp_conn";
    public static void Log_utilit(Log_utility dt)
    {
        string qry = "insert into Admin_Logs " +
                        "values" +
                        "(" +
                            "'" + ManageQuotes(dt.Log_Section) + "'," +
                            "'" + ManageQuotes(dt.Log_Message) + "'," +
                            "GETDATE()," +
                            "'" + dt.Exec_Time + "'," +
                            "'" + dt.Severity + "'," +
                            "'" + ManageQuotes(dt.Sql_Error) + "'," +
                            "'" + dt.userID + "'," +
                            "'" + dt.Loan_Full_ID + "'," +
                            "'" + dt.Batch_ID + "'," +
                            "'" + dt.SourceName + "'," +
                            "'" + dt.SourceDetail + "'" +
                        ")";

        gen_db_utils.gp_sql_execute(qry, dbKey);
    }
    public static void update_ref_key()
    {
        string qry = "update Ref_Budget_Default set ref_key=concat(Z_Region_ID,'_',Z_Office_ID)";

        gen_db_utils.gp_sql_execute(qry, dbKey);
    }

    public static string ManageQuotes(string value)
    {
        if (!string.IsNullOrEmpty(value) && value.Contains("'"))
        {
            value = value.Replace("'", "''");
        }
        return value;
    }

}
public class Log_utility
{
    public string Log_Section { set; get; }
    public string Log_Message { set; get; }
    public string Log_datetime { set; get; }
    public string Exec_Time { set; get; }
    public string Severity { set; get; }
    public string Sql_Error { set; get; }
    public string userID { set; get; }
    public string Loan_Full_ID { set; get; }
    public string Batch_ID { set; get; }
    public string SourceName { set; get; }
    public string SourceDetail { set; get; }
}