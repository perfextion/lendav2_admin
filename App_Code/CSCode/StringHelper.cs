﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

/// <summary>
/// Use this for String Extension Methods
/// </summary>
public static class StringHelper
{
    /// <summary>
    /// Adds Extension Method <see cref="ToHash"/>
    /// which generate SHA512 Hash from a Plain String
    /// </summary>
    /// <param name="password"></param>
    /// <returns></returns>
    public static string ToHash(this string password)
    {
        if (password == null)
        {
            return null;
        }

        var bytes = new UTF8Encoding().GetBytes(password);
        byte[] hashBytes;

        using (var sha512 = new SHA512Managed())
        {
            hashBytes = sha512.ComputeHash(bytes);
        }

        return Convert.ToBase64String(hashBytes);
    }

    /// <summary>
    /// Use this method <see cref="IsEqualPassword(plainPassword, hashedPassword)"/> to compare plain password to hashed password
    /// </summary>
    /// <param name="plainPassword">Plain string Password</param>
    /// <param name="hashedPassword">Hashed String Password</param>
    /// <returns></returns>
    public static bool IsEqualPassword(this string plainPassword, string hashedPassword)
    {
        return String.Equals(plainPassword.ToHash(), hashedPassword);
    }

    /// <summary>
    /// Convert string to boolean
    /// </summary>
    /// <param name="str"></param>
    /// <returns></returns>
    public static bool ToBoolean(this string str)
    {
        bool res;

        if (bool.TryParse(str, out res))
        {
            return res;
        }

        return false;
    }

    public static string TrimSpace(this string str)
    {
        return new string(str.ToCharArray()
            .Where(c => !char.IsWhiteSpace(c))
            .ToArray());
    }

    public static string TrimSQL(this string str)
    {
        return str.Replace("'", "''")
            .Replace("--", "&ndash;")
            .Replace("@@", "@ @")
            .Replace("truncate", "''truncate''")
            .Replace("drop", "''drop''")
            .Replace("delete", "''delete''");
    }

    /// <summary>
    /// Performs <see cref="TrimSpace(string)"/> and <see cref="TrimSQL(string)"/>
    /// </summary>
    /// <param name="str"></param>
    /// <returns></returns>
    public static string TrimAll(this string str)
    {
        return str.TrimSpace().TrimSQL();
    }
}