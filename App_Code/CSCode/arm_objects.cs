﻿using System;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Data.Odbc;
using System.Configuration;
using System.Collections;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Generic;

public class o_loan
{
    public List <o_crop> o_crop_grp;
    public List<o_BalanceSheetItems> o_BalanceSheetItems_grp;
    public List<o_loancrop> o_loancrop_grp;

    public string sortorder;
    public string uom;
    public string minimumdeductiblepercent;
    public string minimumdeductible;

    public string dbKey = "gp_conn";

    public o_loan()
    {
        


    }

    public o_loan(int loan_id)
    {

        load_balancesheetitems_grp();
        load_loancrop_grp();


    }

    public string print_format()
    {
        string str_return = print_balancesheetitems_grp()
                              + print_loancrop_grp();



       

            return str_return;


    }


    public void load_balancesheetitems_grp()
    {
        o_BalanceSheetItems_grp = new List<o_BalanceSheetItems>();
        string str_sql = "select * from BalanceSheetItems where loanid = 2161";

        DataTable Dt = gen_db_utils.gp_sql_get_datatable(str_sql, dbKey);

        foreach (DataRow dtr in Dt.Rows)
        {
            o_BalanceSheetItems item_b = new o_BalanceSheetItems();

            item_b.name = dtr["name"].ToString();
            item_b.discount = Convert.ToDouble(dtr["discount"].ToString());
            item_b.assets = Convert.ToDouble(dtr["assets"].ToString());
            item_b.liability = Convert.ToDouble(dtr["liability"].ToString());
            item_b.id = Int32.Parse(dtr["id"].ToString());
            item_b.loanid = Int32.Parse(dtr["loanid"].ToString());

            o_BalanceSheetItems_grp.Add(item_b);
        }


    }


    public void load_loancrop_grp()
    {
        o_loancrop_grp = new List<o_loancrop>();
        string str_sql = "select * from loancrop lc "
                                 + "  join crop c "
                                 + " on lc.CropId = c.Id "
                                 + " where loanid = 1059";

        DataTable Dt = gen_db_utils.gp_sql_get_datatable(str_sql, dbKey);

        foreach (DataRow dtr in Dt.Rows)
        {
            o_loancrop item_lc = new o_loancrop();

            item_lc.name = dtr["name"].ToString();
            item_lc.saleprice = Convert.ToDouble(dtr["saleprice"].ToString());
            item_lc.productionhistory1 = Convert.ToDouble(dtr["productionhistory1"].ToString());
            item_lc.productionhistory2 = Convert.ToDouble(dtr["productionhistory2"].ToString());
            item_lc.productionhistory3 = Convert.ToDouble(dtr["productionhistory3"].ToString());
            if (dtr["productionhistory4"] == null || dtr["productionhistory4"].ToString().Trim() == "")
            {
                item_lc.productionhistory4 = 0.0;
            }
            else
            {
                item_lc.productionhistory4 = Convert.ToDouble(dtr["productionhistory4"].ToString());
            }

            if (dtr["productionhistory5"] == null || dtr["productionhistory5"].ToString().Trim() == "")
            {
                item_lc.productionhistory5 = 0.0;
            }
            else
            {
                item_lc.productionhistory5 = Convert.ToDouble(dtr["productionhistory5"].ToString());
            }
            if (dtr["productionhistory6"] == null || dtr["productionhistory6"].ToString().Trim() == "")
            {
                item_lc.productionhistory6 = 0.0;
            }
            else
            {
                item_lc.productionhistory6 = Convert.ToDouble(dtr["productionhistory6"].ToString());
            }
          
        
           
            item_lc.loanid = Int32.Parse(dtr["loanid"].ToString());

            o_loancrop_grp.Add(item_lc);
        }


    }


    public string print_balancesheetitems_grp()
    {
        string str_return = "<table id='datatable_fixed_column' class='table table-striped table-bordered'> "
                              + "      <thead> "
                              + "     <tr> "
                              + "       <th >Income Type</th> "
                              + "       <th >Assets</th> "
                              + "       <th >Discount</th> "
                              + "       <th >Liability</th> "
                              + "    </tr> "
                              + "      </thead> "
                              + "        <tbody>";




        foreach (o_BalanceSheetItems item_b in this.o_BalanceSheetItems_grp)
        {
            str_return +=
                            "<tr>"
                             + "<td>" + item_b.name + "</td>"
                             + "<td>" + item_b.assets + "</td>"
                             + "<td>" + item_b.discount + "</td>"
                             + "<td>" + item_b.liability + "</td>"
                             + "<td>" + item_b.loanid + "</td>"
                           + "</tr>";


        }

        str_return += "</table>";

        return str_return;


    }


    public string print_loancrop_grp()
    {
        string str_return = "<table id='datatable_fixed_column' class='table table-striped table-bordered'> "
                              + "      <thead> "
                              + "     <tr> "
                              + "       <th >Crop</th> "
                              + "       <th >PH1</th> "
                              + "       <th >PH2</th> "
                              + "       <th >PH3</th> "
                              + "       <th >PH4</th> "
                              + "    </tr> "
                              + "      </thead> "
                              + "        <tbody>";




        foreach (o_loancrop item_lc in this.o_loancrop_grp)
        {
            str_return +=
                            "<tr>"
                             + "<td>" + item_lc.name + "</td>"
                             + "<td>" + item_lc.productionhistory1 + "</td>"
                             + "<td>" + item_lc.productionhistory2 + "</td>"
                             + "<td>" + item_lc.productionhistory3 + "</td>"
                             + "<td>" + item_lc.productionhistory4 + "</td>"
                           + "</tr>";


        }

        str_return += "</table>";

        return str_return;


    }


}



public class o_crop
{
    public string id;
    public string sortorder;
    public string uom;
    public string minimumdeductiblepercent;
    public string minimumdeductible;
    


    public o_crop()
    {
        id = "";
        sortorder = "";
        uom = "";
 
     

    }



}


public class o_BalanceSheetItems
{
    public int id;
    public int loanid;
    public double assets;
    public double discount;
    public double liability;
    public string name;



    public o_BalanceSheetItems()
    {
        id = 0;
        loanid = 0;
        assets = 0.0;
        discount = 0.0;
        liability = 0.0;
        name = "";

    }



}



public class o_loancrop
{
    public int loanid;
    public int cropid;
    public string name;
    public double saleprice;
    public double productionhistory1;
    public double productionhistory2;
    public double productionhistory3;
    public double productionhistory4;
    public double productionhistory5;
    public double productionhistory6;



    public o_loancrop()
    {
       loanid =0;
      cropid = 0;
      saleprice = 0.0 ;
      name = "";
       productionhistory1 = 0.0;
       productionhistory2 = 0.0;
       productionhistory3 = 0.0;
       productionhistory4 = 0.0;
       productionhistory5 = 0.0;
       productionhistory6 = 0.0;



    }



}






