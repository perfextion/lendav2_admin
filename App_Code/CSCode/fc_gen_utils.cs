using System;
using System.Collections;
using System.Data;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI.WebControls;

public class fc_gen_utils
{
    public static bool ValidateAnswer(string CorrectAnswer, string UserAnswer)
    {
        if (CorrectAnswer == UserAnswer)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static string new_sticky_id()
    {
        string temp_str = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        char[] charArray = temp_str.ToCharArray();
        Random random = new Random();

        string str_return = "";

        for (int i = 1; i <= 20; i++)
        {
            str_return += charArray[random.Next(0, 61)];
        }

        return str_return;
    }

    public static string new_sticky_id_1(Random rnd1)
    {
        string temp_str = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        char[] charArray = temp_str.ToCharArray();

        string str_return = "";

        for (int i = 1; i <= 20; i++)
        {
            str_return += charArray[rnd1.Next(0, 61)];
        }

        return str_return;
    }

    public static string arraylist2string(ArrayList al)
    {
        int i = 0;
        string ret_string = "";

        foreach (object o in al)
        {
            if (i == 0)
            {
                ret_string = ret_string + o.ToString();
                i = 1;
            }
            else
            {
                ret_string = ret_string + "," + o.ToString();
            }
        }
        return ret_string;
    }

    public static ArrayList string2arraylist(string str_input)
    {
        ArrayList item_al = new ArrayList();

        item_al.AddRange(str_input.Split(','));

        return item_al;
    }

    public static string array2string(string[] al)
    {
        int i = 0;
        string ret_string = "";

        foreach (object o in al)
        {
            if (i == 0)
            {
                ret_string = ret_string + o.ToString();
                i = 1;
            }
            else
            {
                ret_string = ret_string + "," + o.ToString();
            }
        }
        return ret_string;
    }

    public static ArrayList arraylist_a_minus_b(ArrayList a, ArrayList b)
    {
        ArrayList al_return = new ArrayList();
        foreach (string item in a)
        {
            if (!b.Contains(item)) al_return.Add(item);
        }
        return al_return;
    }

    public static void arraylist_shuffle(ArrayList source)
    {
        Random rnd = new Random();
        for (int inx = source.Count - 1; inx > 0; --inx)
        {
            int position = rnd.Next(inx);
            object temp = source[inx];
            source[inx] = source[position];
            source[position] = temp;
        }
    }

    public static string new_batch_id()
    {
        string temp_str = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        char[] charArray = temp_str.ToCharArray();
        Random random = new Random();

        string str_return = "";

        for (int i = 1; i <= 10; i++)
        {
            str_return += charArray[random.Next(0, 61)];
        }

        return str_return;
    }

    public static ArrayList string_split_by_delimter(string orig_str, string str_delim)
    {
        ArrayList ar_return = new ArrayList();

        string[] stringSeparators = new string[] { str_delim };
        string[] substrings = orig_str.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);

        for (int ii = 1; ii <= substrings.Length; ii++)
        {
            ar_return.Add(substrings[ii - 1]);
        }

        return ar_return;
    }

    public static ArrayList string_split_by_delimter_empties(string orig_str, string str_delim)
    {
        ArrayList ar_return = new ArrayList();

        string[] stringSeparators = new string[] { str_delim };
        string[] substrings = orig_str.Split(stringSeparators, StringSplitOptions.None);

        for (int ii = 1; ii <= substrings.Length; ii++)
        {
            ar_return.Add(substrings[ii - 1]);
        }

        return ar_return;
    }

    public static string replace_quotes(string orig_str)
    {
        string final_str;
        final_str = orig_str.Replace("'", "''");
        return final_str;
    }

    public static Hashtable datatable2hash(DataTable dt1)
    {
        Hashtable temp_hash = new Hashtable();

        foreach (DataRow row in dt1.Rows)
        {
            temp_hash.Add(row[0].ToString(), row[1].ToString());
        }

        return temp_hash;
    }

    public static string hash_lookup(Hashtable h, string t_key, string def_value)
    {
        string str_return = def_value;

        if (h.ContainsKey(t_key))
        {
            str_return = h[t_key].ToString();
        }

        return str_return;
    }

    public static string sess_lookup(string t_key, string def_value)
    {
        if (HttpContext.Current.Session[t_key] == null)
        {
            return def_value;
        }
        else
        {
            return HttpContext.Current.Session[t_key].ToString();
        }
    }

    public static void set_chk_box(string session_var, CheckBox chk_ctrl, bool def_value)
    {
        if (HttpContext.Current.Session[session_var] == null)
        {
            chk_ctrl.Checked = def_value;
        }
        else
        {
            if (HttpContext.Current.Session[session_var].ToString() == "1") chk_ctrl.Checked = true;
            if (HttpContext.Current.Session[session_var].ToString() == "0") chk_ctrl.Checked = false;
        }
    }

    public static void set_ddl(string session_var, DropDownList ddl_ctrl)
    {
        ddl_ctrl.SelectedIndex = Int32.Parse(HttpContext.Current.Session[session_var].ToString());
    }

    //bind dropdown

    public static ArrayList get_inside_tags(string str_input, string tag)
    {
        string str_inp = str_input;
        ArrayList ar_return = new ArrayList();
        string str_temp = "";

        str_inp = str_inp.Replace("\n", "").Replace("\r", "");

        str_inp = str_inp.Replace("<tr>", "[TR]").Replace("</tr>", "[/TR]").Replace("<TR>", "[TR]").Replace("</TR>", "[/TR]");
        Regex _reg = new Regex("\\[" + tag + "\\].*?\\[/" + tag + "\\]", RegexOptions.IgnoreCase);

        MatchCollection _element_tag_coll = _reg.Matches(str_inp);

        if (_element_tag_coll.Count > 0)
        {
            foreach (Match _element_tag in _element_tag_coll)
            {
                str_temp = _element_tag.ToString();
                str_temp = str_temp.Replace("[" + tag + "]", "").Replace("[/" + tag + "]", "");
                ar_return.Add(str_temp);
            }
        }

        return ar_return;
    }

    public static string list_datatable(DataTable inp_dt)
    {
        string str_return = "";
        int cnt_columns = inp_dt.Columns.Count;

        foreach (DataRow i_row in inp_dt.Rows)
        {
            str_return += "<br>";

            for (int jj = 0; jj < cnt_columns; jj++)
            {
                str_return += i_row[jj].ToString() + "||";
            }
        }

        return str_return;
    }

    public static string list_hashtable(Hashtable inp_h)
    {
        string str_return = "";

        foreach (DictionaryEntry de in inp_h)
        {
            str_return += "<br>" + de.Key.ToString() + " || " + de.Value.ToString();
        }

        return str_return;
    }

    public static string padded_string(int int_num, int str_length)
    {
        string str_return = "";

        string pad_str = "000000000";

        if (int_num >= 0 && int_num < 10)
        {
            str_return = pad_str.Substring(0, str_length - 1) + int_num.ToString();
        }
        else if (int_num >= 10 && int_num < 100)
        {
            str_return = pad_str.Substring(0, str_length - 2) + int_num.ToString();
        }
        else if (int_num >= 10 && int_num < 100)
        {
            str_return = pad_str.Substring(0, str_length - 3) + int_num.ToString();
        }
        return str_return;
    }

    public static int CountStringOccurrences(string text, string pattern)
    {
        // Loop through all instances of the string 'text'.
        int count = 0;
        int i = 0;
        while ((i = text.IndexOf(pattern, i)) != -1)
        {
            i += pattern.Length;
            count++;
        }
        return count;
    }

    public static string time_diff(DateTime time_begin, DateTime time_end)
    {
        string str_return = "";

        TimeSpan ts = time_end - time_begin;

        str_return = (ts.Milliseconds).ToString();

        return str_return;
    }

    public static string put_space(int int_width)
    {
        string str_return = "<img src=\"/images/spacer.gif\"  width=\"" + int_width.ToString() + "\" height=\"10\" />";

        return str_return;
    }
}