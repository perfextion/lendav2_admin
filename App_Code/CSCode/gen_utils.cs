using System;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Data.Odbc;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;
using System.Text;
using System.Web.Script.Serialization;


public class gen_utils
    {

    public static ArrayList stringarray2arraylist(string[] str_ar)
    {
        int i = 0;
        ArrayList ar_return = new ArrayList();

        foreach (string item in str_ar)
        {
            ar_return.Add(item);
        }
        return ar_return;
    }

    public static string arraylist2string(ArrayList al)
    {
        int i = 0;
        string ret_string = "";

        foreach (object o in al)
        {
            if (i == 0)
            {
                ret_string = ret_string + o.ToString();
                i = 1;
            }
            else
            {
                ret_string = ret_string + "," + o.ToString();
            }
        }
        return ret_string;
    }

    public static ArrayList string2arraylist(string str_input)
    {
        ArrayList item_al = new ArrayList();

        item_al.AddRange(str_input.Split(','));

        return item_al;
    }

    public static ArrayList string2arraylist(string str_input, char delim)
    {
        ArrayList item_al = new ArrayList();

        item_al.AddRange(str_input.Split(delim));

        return item_al;
    }

   

    public static string int_array2string(int[] i_ar)
    {
      
        string ret_string = "";

        for (int kk =0; kk < i_ar.Length; kk++)
        {
            if (kk == 0)
            {
                ret_string += i_ar[kk].ToString();
              
            }
            else
            {
                ret_string += "," + i_ar[kk].ToString();
            }
        }
        return ret_string;
    }

    public static int [] string2int_array(string str_input)
    {
        string[] str_temp = str_input.Split(',');

        int[] ints_temp = new int [ str_temp.Length ] ;

        for (int i = 0; i < ints_temp.Length; i++)
        {
            ints_temp[i] = Int32.Parse(str_temp[i]);
        }


        return ints_temp;
    }

    public static string spacer_html(int width, int height)
    {

     
        string str_return;

        str_return = "<img src=\"spacer.gif\" "
                     + "width=\"" + width.ToString()
                     + "\"height=\"" + height.ToString() + "\" />";

        return str_return;
    }

    public static string replace_quotes(string orig_str)
    {
        string final_str;
        final_str = orig_str.Replace("'", "''");
        return final_str;

    }



    public static string hash_lookup(Hashtable hash_t, string item, string str_null)
    {
        string str_return = "0";
        if (hash_t.ContainsKey(item))
        {
            str_return = hash_t[item].ToString();
        }
        else
        {
            str_return = str_null;
        }

        return str_return;

    }




    public static ArrayList arr_diff(ArrayList A, ArrayList B)
    {
        ArrayList arr_temp = new ArrayList();
        foreach (string item_a in A)
        {
            if (!B.Contains(item_a)) arr_temp.Add(item_a);
            
        }

        return arr_temp;

    }


    public static ArrayList dedupe_arraylist(ArrayList A)
    {
        ArrayList arr_temp = new ArrayList();
        foreach (string item_a in A)
        {
            if (!arr_temp.Contains(item_a)) arr_temp.Add(item_a);

        }

        return arr_temp;

    }

    public static string string2shortdateformat(string inp_date)
    {
        string str_return;
        string format = "MM/dd/yy";


        DateTime time1 = DateTime.Parse(inp_date);

        str_return = time1.ToString(format);


        return str_return;

    }

    public static string DataTableToJSONWithStringBuilder(DataTable table)
    {
        StringBuilder JSONString = new StringBuilder();
        if (table.Rows.Count > 0)
        {
            JSONString.Append("[");
            for (int i = 0; i < table.Rows.Count; i++)
            {
                JSONString.Append("{");
                for (int j = 0; j < table.Columns.Count; j++)
                {
                    if (j < table.Columns.Count - 1)
                    {
                        JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":" + "\"" + table.Rows[i][j].ToString() + "\",");
                    }
                    else if (j == table.Columns.Count - 1)
                    {
                        JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":" + "\"" + table.Rows[i][j].ToString() + "\"");
                    }
                }
                if (i == table.Rows.Count - 1)
                {
                    JSONString.Append("}");
                }
                else
                {
                    JSONString.Append("},");
                }
            }
            JSONString.Append("]");
        }
        return JSONString.ToString();
    }

    public static string DataTableToJSONWithJavaScriptSerializer(DataTable table)
    {
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
        Dictionary<string, object> childRow;
        foreach (DataRow row in table.Rows)
        {
            childRow = new Dictionary<string, object>();
            foreach (DataColumn col in table.Columns)
            {
                childRow.Add(col.ColumnName, row[col]);
            }
            parentRow.Add(childRow);
        }
        return jsSerializer.Serialize(parentRow);
    }  

    }