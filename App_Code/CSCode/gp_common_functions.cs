using System;
using System.Collections;
using System.Data;

public class gp_common_functions
{
    private static string dbKey = "gp_conn";

    public static void update_quiz_summary()
    {
        string st_id = "";
        int cnt_total = 0;
        string duration = "";

        string str_sql = "select distinct(test_key) from omr_answers where test_key not in "
                 + " (select test_key from quiz_summary)";

        ArrayList ar_keys = gen_db_utils.gp_sql_get_arraylist(str_sql, dbKey);

        foreach (string item_key in ar_keys)
        {
            string sql_prep = "  select count(*) as cnt_total, sum(duration) as duration , st_id  from omr_answers where test_key = '" + item_key + "'"
                              + " group by st_id ";

            DataTable dt1 = gen_db_utils.gp_sql_get_datatable(sql_prep, dbKey);

            foreach (DataRow dtr1 in dt1.Rows)
            {
                st_id = dtr1["st_id"].ToString();
                cnt_total = Int32.Parse(dtr1["cnt_total"].ToString());
                duration = dtr1["duration"].ToString();
            }
            string str_ins = "insert into quiz_summary (test_key,st_id,cnt_total,duration) "
                + " values ( "
                + "'" + item_key + "',"
                + st_id + ","
                + cnt_total + ","
                + duration + ""
                + " ) ";

            gen_db_utils.gp_sql_execute(str_ins, dbKey);
        }
    }

    public static string get_admin_top_menu(string role_id)
    {
        string str_return = "";
        str_return = "<nav>" +
                    " <ul style='z-index:99999'>" +
                        "<li>" +
                          "<a href='/admin/dashboard1.aspx' title='Dashboard'><i class='fa fa-lg fa-fw fa-home'></i><span class='menu-item-parent'>Home</span></a>" +
                            //" <ul>" +
                            //    "<li>" +
                            //     "	<a href='../admin/MyTaskLog.aspx'>My Task Log</a>" +
                            //    "</li>" +
                            //    "<li>" +
                            //     "	<a href='../admin/dashboard.aspx'>Check In/Out</a>" +
                            //    "</li>" +
                            //"</ul>" +
                          " </li>" +
                       "<li>" +
                          "<a href='#'><i class='fa fa-table reference-table'></i> <span class='menu-item-parent'>Reference</span></a>" +
                            "<ul class='ref-menu' style='z-index:99999'>" +
                                    "<li>" +
                                        "<a href='../admin/RefAssocDefault.aspx'>Association Default</a>" +
                                    "</li>" +
                                    "<li>" +
                                        "<a href='../admin/ReferenceAssociation.aspx'>Association Name</a>" +
                                    "</li>" +
                                    "<li>" +
                                        "<a href='../admin/RefAssociationReferralType.aspx'>Association Referral Type</a>" +
                                    "</li>" +
                                    "<li>" +
                                        "<a href='../admin/RefAssociationType.aspx'>Association Type</a>" +
                                    "</li>" +
                                    "<li>" +
                                        "<a href='../admin/BorrowerEntityType.aspx'>Borrower Entity Type</a>" +
                                    "</li>" +
                                     "<li>" +
                                        " <a href='../admin/Ref_Farm_Financial_Rating_New.aspx'>Borrower Farm Financial Ratio</a>" +
                                    "</li>" +
                                     "<li>" +
                                        "	<a href='../admin/ReferenceMasterFarmerNew.aspx'>Borrower Farmer Affiliate Name</a>" +
                                    "</li>" +
                                    "<li>" +
                                        "<a href='../admin/BorrowerFinancialAssetDiscount.aspx'>Borrower Financial Discount</a>" +
                                    "</li>" +
                                    "<li>" +
                                        "<a href='../admin/BorrowerIncomeHistory.aspx'>Borrower Income History</a>" +
                                    "</li>" +
                                    "<li>" +
                                    "	<a href='../admin/MasterBorrowerNew.aspx'>Borrower Name</a>" +
                                    "</li>" +
                                    "<li>" +
                                    "	<a href='../admin/BorrowerNameDetail.aspx'>Borrower Name Detail</a>" +
                                    "</li>" +
                                    "<li>" +
                                        "<a href='../admin/RefBorrowerRating.aspx'>Borrower Rating</a>" +
                                    "</li>" +
                                     "<li>" +
                                        "<a href='../admin/ReferenceBudget.aspx'>Budget Default</a>" +
                                    "</li>" +
                                    "<li>" +
                                        "	<a href='../admin/ReferenceBudgetViewArmsNew.aspx'>Budget Default 3D Maintainance</a>" +
                                    "</li>" +
                                    "<li>" +
                                        "	<a href='../admin/RefBudgetExpenseType.aspx'>Budget Expense Type</a>" +
                                    "</li>" +
                                    " <li>" +
                                        "	<a href='../admin/RefCollateralActionAdjDisc.aspx' title='Ref Collateral Action Adj. Disc.'>Collateral Action Disc Adj</a>" +
                                    "</li>" +
                                    " <li>" +
                                        "	<a href='../admin/RefCollateralCategory.aspx' title='Ref Collateral Category'>Collateral Category</a>" +
                                    "</li>" +
                                    " <li>" +
                                        "	<a href='../admin/RefCollateralInsDisc.aspx' title='Ref Collateral Ins Disc.'>Collateral Ins Disc</a>" +
                                    "</li>" +
                                     " <li>" +
                                        "<a href='../admin/RefMeasurementTypes.aspx' title='Collateral Measurement Type'>Collateral Measurement Type</a>" +
                                    "</li>" +
                                    //"<li>" +
                                    //    "<a href='../admin/Reference_VCA.aspx'>Collateral Misc Credit Agreement</a>" +
                                    //"</li>" +
                                    " <li>" +
                                        "<a href='../admin/RefCollateralMarketDisc.aspx' title='Ref Collateral Mkt Disc.'>Collateral Mkt Disc</a>" +
                                    "</li>" +
                                     "<li>" +
                                        "<a href='../admin/RefCommentEmoji.aspx'>Comment Emoji</a>" +
                                    "</li>" +
                                     "<li>" +
                                        "<a href='../admin/RefCommentType.aspx'>Comment Type</a>" +
                                    "</li>" +
                                    " <li>" +
                                        "<a href='../admin/RefCommitteeLevel.aspx'>Committee Level</a>" +
                                    "</li>" +
                                    " <li>" +
                                        "<a href='../admin/RefCommitteeLevelDetail.aspx'>Committee Level Detail</a>" +
                                    "</li>" +
                                    "<li>" +
                                        "<a href='../admin/ReferenceCropBasis.aspx'>Crop Basis Default</a>" +
                                    "</li>" +
                                    " <li>" +
                                        "	<a href='../admin/RefCollateralContractType.aspx' title='Collateral Contract Type'>Crop Contract Type</a>" +
                                    "</li>" +
                                    "<li>" +
                                        "<a href='../admin/V_Crop_Price_Details.aspx'>Crop Key</a>" +
                                    "</li>" +
                                    "<li>" +
                                    "	<a href='../admin/ReferenceCrop.aspx'>Crop Price Default</a>" +
                                    "</li>" +
                                    "<li>" +
                                    "	<a href='../admin/RefDocumentType.aspx'>Document</a>" +
                                    "</li>" +
                                    "<li>" +
                                    "	<a href='../admin/RefDocumentDetail.aspx'>Document Detail</a>" +
                                    "</li>" +                                    
                                     "<li>" +
                                        "<a href='../admin/RefInsuranceAPH.aspx'>Insurance APH</a>" +
                                    "</li>" +
                                    "<li>" +
                                        "<a href='../admin/RefInsuranceEligibiltiyRules.aspx'>Insurance Eligibility Rules</a>" +
                                    "</li>" +
                                    "<li>" +
                                        "<a href='../admin/Ref_InsuranceOptionAdj.aspx'>Insurance Option Adj</a>" +
                                    "</li>" +
                                    "<li>" +
                                        "<a href='../admin/InsurancePlanActurial.aspx'>Insurance Plan Actuarial</a>" +
                                    "</li>" +
                                    "<li>" +
                                        "<a href='../admin/RefInsurancePolicyPlan.aspx'>Insurance Policy Plan</a>" +
                                    "</li>" +
                                    "<li>" +
                                    " <a href='../admin/RMA_Insurance_Offer_New.aspx'>Insurance RMA MPCI Offer</a>" +
                                    "</li>" +
                                    "<li>" +
                                    "	<a href='../admin/RefInsuranceValidationRules.aspx'>Insurance Validation Rules</a>" +
                                    "</li>" +                                    
                                    "<li>" +
                                        "<a href='../admin/RiskAdminDefault.aspx'>Risk Admin Default</a>" +
                                    "</li>" +
                                    "<li>" +
                                    "	<a href='../admin/RefException.aspx'>Risk Exception</a>" +
                                    "</li>" +
                                    "<li>" +
                                    "	<a href='../admin/ReferenceQuestions.aspx'>Risk Question</a>" +
                                    "</li>" +
                                     "<li>" +
                                    "	<a href='../admin/RefValidation.aspx'>Risk Validation</a>" +
                                    "</li>" +
                                     "<li>" +
                                        "<a href='../admin/RiskVerify.aspx'>Risk Verify</a>" +
                                    "</li>" +
                                     "<li>" +
                                        "<a href='../admin/SystemAdminDefault.aspx'>System Admin Default</a>" +
                                    "</li>" +
                                    "<li>" +
                                    "	<a href='../admin/ReferenceChevron.aspx'>System Chevron</a>" +
                                    "</li>" +
                                     "<li>" +
                                      "	<a href='../admin/ReferenceCounty.aspx'>System County</a>" +
                                    "</li>" +
                                     "<li>" +
                                        "<a href='../admin/ReferenceLoanStatus.aspx'>System Loan Status</a>" +
                                     "</li>" +
                                     "<li>" +
                                        "<a href='../admin/RefLoanType.aspx'>System Loan Type</a>" +
                                     "</li>" +
                                      "<li>" +
                                        "<a href='../admin/ReferenceOffice.aspx'>System Office</a>" +
                                     "</li>" +
                                     "<li>" +
                                       "	<a href='../admin/ReferencePendingAction.aspx'>System Pending Action</a>" +
                                     "</li>" +
                                     "<li>" +
                                        "<a href='../admin/ReferenceRegion.aspx'>System Region</a>" +
                                     "</li>" +
                                     "<li>" +
                                        "<a href='../admin/ReturnScale.aspx'>System Return Scale</a>" +
                                    "</li>" +
                                    "<li>" +
                                        "<a href='../admin/RiskScale.aspx'>System Risk Scale</a>" +
                                    "</li>" +
                                    "<li>" +
                                      "	<a href='../admin/ReferenceState.aspx'>System State</a>" +
                                    "</li>" +
                                     "<li>" +
                                       "	<a href='../admin/Ref_Tab.aspx'>System Tab</a>" +
                                     "</li>" +                                    
                                     "<li>" +
                                        "	<a href='../admin/UserManagement.aspx'>User Maintenance</a>" +
                                    "</li>" +
                                    "<li>" +
                                    "	<a href='../admin/JobTitleNew.aspx'>User Role Type</a>" +
                                    "</li>" +  
                                    "<li>" +
                                    "	<a href='../admin/UserTypeAccess.aspx'>User Type Access</a>" +
                                    "</li>" +                                    
                                "</ul>" +
                                "</li>" +
                                "<li>" +
                                    "<a href='#' title='Loan'><i class='fa fa-table'></i> <span class='menu-item-parent'>Loan</span></a>" +
                                    "<ul style='z-index:99999' class='ref-menu'>" +
                                        "<li>" +
                                            "<a href='../admin/Loan_Full_Details.aspx'>Display Loan Summary</a>" +
                                        "</li>" +
                                        "<li>" +
                                        "	<a href='../admin/LoanAssociation.aspx'>Loan Association</a>" +
                                        "</li>" +
                                         "<li>" +
                                        "	<a href='../admin/Loan_Audit_Trail.aspx'>Loan Audit Trail</a>" +
                                        "</li>" +
                                        "<li>" +
                                        "	<a href='../admin/LoanBudget.aspx'>Loan Budget</a>" +
                                        "</li>" +
                                        "<li>" +
                                        "	<a href='../admin/LoanCollateralDetail.aspx'>Loan Collateral Detail</a>" +
                                        "</li>" +
                                        "<li>" +
                                        "	<a href='../admin/LoanComment_New.aspx'>Loan Comment</a>" +
                                        "</li>" +
                                        "<li>" +
                                         "	<a href='../admin/LoanCommittee_New.aspx'>Loan Committee</a>" +
                                       "</li>" +                                       
                                        "<li>" +
                                        "	<a href='../admin/LoanCropMarketing.aspx'>Loan Crop Contract</a>" +
                                        "</li>" +
                                        "<li>" +
                                        "	<a href='../admin/LoanCropPractice.aspx'>Loan Crop Practice</a>" +
                                        "</li>" +
                                        "<li>" +
                                        "	<a href='../admin/LoanCropPracticeYield.aspx'>Loan Crop Practice Yield History</a>" +
                                        "</li>" +
                                        "<li>" +
                                        "	<a href='../admin/LoanCropPrice.aspx'>Loan Crop Price</a>" +
                                        "</li>" +
                                         "<li>" +
                                        "	<a href='../admin/LoanCrossCollateralNew.aspx'>Loan Cross Collateral</a>" +
                                        "</li>" +
                                       "<li>" +
                                         "	<a href='../admin/Loan_Detail_New.aspx'>Loan Detail</a>" +
                                       "</li>" +
                                       "<li> " +
                                        "	<a href='../admin/LoanDisburse.aspx'>Loan Disburse</a>" +
                                        "</li>" +
                                        "<li> " +
                                        "	<a href='../admin/LoanDisburseDetail.aspx'>Loan Disburse Detail</a>" +
                                        "</li>" +
                                        "<li> " +
                                        "	<a href='../admin/LoanDisburseRentDetail.aspx'>Loan Disburse Rent Detail</a>" +
                                        "</li>" +
                                        "<li>" +
                                        "	<a href='../admin/LoanDocument.aspx'>Loan Document</a>" +
                                        "</li>" +
                                        "<li>" +
                                        "	<a href='../admin/LoanDocumentDetail.aspx'>Loan Document Detail</a>" +
                                        "</li>" +
                                        "<li>" +
                                        "	<a href='../admin/LoanExceptions.aspx'>Loan Exception</a>" +
                                        "</li>" +
                                        "<li> " +
                                        "	<a href='../admin/LoanFarm.aspx'>Loan Farm</a>" +
                                        "</li>" +
                                        "<li> " +
                                        "	<a href='../admin/LoanFarmCropDetail.aspx'>Loan Farm Crop Detail</a>" +
                                        "</li>" +
                                        "<li> " +
                                        "	<a href='../admin/LoanMaster.aspx'>Loan Master</a>" +
                                        "</li>" +
                                        "<li> " +
                                        "	<a href='../admin/LoanOtherIncome.aspx'>Loan Other Income</a>" +
                                        "</li>" +
                                        "<li> " +
                                        "	<a href='../admin/LoanPolicyPlan.aspx'>Loan Policy Plan</a>" +
                                        "</li>" +
                                        "<li> " +
                                        "	<a href='../admin/LoanPolicyPlanOptionDetail.aspx'>Loan Policy Plan Option Detail</a>" +
                                        "</li>" +
                                        "<li> " +
                                        "	<a href='../admin/LoanQuestion.aspx'>Loan Question</a>" +
                                        "</li>" +
                                        "<li>" +
                                        "	<a href='../admin/UserPendingAction.aspx'>Loan User Pending Action</a>" +
                                        "</li>" +
                                        "<li>" +
                                        "	<a href='../admin/LoanValidationNew.aspx'>Loan Validation</a>" +
                                        "</li>" +                                                                                                                  
                                    "</ul>" +
                                "</li>" +

                       "<li title='Calender'>" +
                          "<a href='/admin/Calender.aspx' ><i class='fa fa-lg fa-fw fa-calendar'></i><span class='menu-item-parent'>Calender</span></a>" +
                     " </li>" +

                        "<li  title='Logs'>" +
                          "<a href='#'><i class='fa fa-lg fa-fw fa-envelope'></i><span class='menu-item-parent'>Logs</span></a>" +
                          "<ul style='z-index:99999'>" +
                              "<li>" +
                              "	<a href='../admin/Logs.aspx'>Logs</a>" +
                              "</li>" +
                              "<li>" +
                              "	<a href='../admin/Log_Charts.aspx'>Logs Chart</a>" +
                              "</li>" +
                              "<li>" +
                              "	<a href='../admin/LogReview.aspx'>Logs Review</a>" +
                              "</li>" +
                              "<li>" +
                              "	<a href='../admin/Log_viewer.aspx'>Log viewer</a>" +
                              "</li>" +
                          "</ul>" +
                     " </li>" +
                          "<li title='Documents'>" +
                          "<a href='/admin/Z_Doc_Index.aspx' ><i class='fa fa-lg fa-fw fa-file'></i><span class='menu-item-parent'>Documents</span></a>" +
                         "<ul style='z-index:99999'>" +
                              "<li>" +
                              "	<a href='../admin/Z_Document.aspx'>TOC Document</a>" +
                              "</li>" +
                              "<li>" +
                              "	<a href='../admin/Documents.aspx'>All Documents</a>" +
                              "</li>" +
                              "<li>" +
                              "	<a href='../admin/Add_Z_Doc_Index.aspx'>Update Doc Index</a>" +
                              "</li>" +
                               "<li>" +
                              "	<a href='../admin/ScreensSections.aspx'>Screens & Sub Sections</a>" +
                              "</li>" +
                              "<li>" +
                              "	<a href='../admin/DBMapping.aspx'>DB Mapping</a>" +
                              "</li>" +
                               "<li>" +
                              "	<a href='../admin/E2EMapping.aspx'>E2E Mapping</a>" +
                              "</li>" +
                              "<li>" +
                              "	<a href='../admin/Z_Brad_DB_Design.aspx'>Z_Brad_DB_Design</a>" +
                              "</li>" +
                          "</ul>" +
                          " </li>" +

                     "<li title='Tracker'>" + /*glyphicon glyphicon - map - marker*/
                          "<a href='/admin/Tracker.aspx' ><i class='fa fa-map-marker'></i><span class='menu-item-parent'>Tracker</span></a>" +

                     " </li>" +
                     "<li title='LoanDebug'>" +
                          "<a href='#' ><i class='fa fa-lg fa-fw fa-envelope'></i><span class='menu-item-parent'>Loan Debug</span></a>" +
                          "<ul style='z-index:99999; width: 230px;'>" +
                          "<li>" +
                            "<a href='#' title='Loan'>Loan</span></a>" +
                            " <ul class='ref-menu'>" +
                                "<li>" +
                                "	<a href='../admin/loan_validator.aspx'>Loan Validator</a>" +
                                "</li>" +
                                "<li>" +
                                "	<a href='../admin/dbg_full_loan1.aspx'>Debug Full Loan</a>" +
                                "</li>" +
                                "<li>" +
                                "	<a href='../admin/LoanComment.aspx'>Loan Comment</a>" +
                                "</li>" +
                                "<li>" +
                                    "	<a href='../admin/LoanCommittee.aspx'>Loan Committee</a>" +
                                "</li>" +
                                "<li>" +
                                    "<a href='../admin/Loan_Full_Details_Old.aspx'>Loan Summary</a>" +
                                "</li>" +
                                 "<li>" +
                                    "	<a href='../admin/Loans_List.aspx'>Loan List</a>" +
                                "</li>" +
                                "<li>" +
                                    "<a href='../admin/LoanComment.aspx'>Loan Comment</a>" +
                                    "</li>" +
                                "</ul>" +
                            " </li>" +
                            "<li>" +
                            "<a href='#' title='Reference'>Reference</span></a>" +
                                " <ul class='ref-menu'>" +
                                    "<li>" +
                                   "<a href='../admin/RefListItem.aspx'>List Items</a>" +
                                  "</li>" +
                                    "<li>" +
                                      "	<a href='../admin/BudgetDefalutSummery.aspx'>Budget Default Summary</a>" +
                                    "</li>" +
                                    "<li>" +
                                      "	<a href='../admin/CropKeySummary.aspx'>Crop Key Summary</a>" +
                                    "</li>" +
                                     "<li>" +
                                      "	<a href='../admin/RefBudgetValues.aspx'>Create Budgets</a>" +
                                      "</li>" +
                                      "<li>" +
                                      "	<a href='../admin/Budget3DtableByLocations.aspx'>3D Table By Locations</a>" +
                                      "</li>" +
                                      "<li>" +
                                      "	<a href='../admin/ReferenceDiscounts.aspx'>System Discounts</a>" +
                                      "</li>" +
                                      "<li>" +
                                         "	<a href='#'>Sync Budget Routine</a>" +
                                       "</li>" +                                       
                                        "<li>" +
                                         "	<a href='../admin/ArmDefault.aspx'>ARM Defaults</a>" +
                                       "</li>" +
                                       "<li>" +
                                        "	<a href='../admin/Loan_Purge.aspx'>Loan Purge</a>" +
                                        "</li>" +
                                        "<li>" +
                                         "	<a href='../admin/RiskOtherQueue.aspx'>Other Queue</a>" +
                                       "</li>" +
                                       "<li>" +
                                          "	<a href='../admin/MaxUserPendingActions.aspx'>Max User Pending Actions</a>" +
                                          "</li>" +                                           
                                          "<li>" +
                                            "	<a href='../admin/AllTaskLogs.aspx'>All Task Logs</a>" +
                                          "</li>" +
                                          "<li>" +
                                            "	<a href='../admin/Enums.aspx'>Enums</a>" +
                                          "</li>" +
                                          "<li>" +
                                            "	<a href='../admin/ClearTables.aspx'>Clear Table Data</a>" +
                                          "</li>" +
                                           "<li>" +
                                          "	<a href='../admin/UserMaintenance.aspx'>User Maintenance - 2</a>" +
                                          "</li>" +
                                           "<li>" +
                                            "<a href='../admin/InsuranceValidation2.aspx'>Insurance Eligibility Rules</a>" +
                                          "</li>" +
                                          "<li>" +
                                            "	<a href='../admin/InsuranceValidation.aspx'>Insurance Validation Rules</a>" +
                                            "</li>" +
                                            "<li>" +
                                            "	<a href='../admin/AffiliatedLoans.aspx'>View Loan Affiliation</a>" +
                                            "</li>" +
                                "</ul>" +
                            " </li>" +
                            "<li>" +
                            "	<a href='../admin/Migration_Summary.aspx'>Loan Migration Comparison - Old</a>" +
                            "</li>" +
                            "<li>" +
                            "	<a href='../admin/Migration_Summary_New.aspx'>Loan Migration Comparison V.1</a>" +
                            "</li>" +                            
                            "<li>" +
                            "	<a href='../admin/Migration_Summary_V.2.aspx'>Loan Migration Comparison V.2</a>" +
                            "</li>" +
                            "<li>" +
                            "	<a href='../admin/Migration_Summary_V_3.aspx'>Loan Migration Comparison V.3</a>" +
                            "</li>" +
                            "<li>" +
                            "	<a href='../admin/Migration_Summary_Results.aspx'>Migration Summary Results</a>" +
                            "</li>" +
                          "</ul>" +
                     " </li>" +
                      "<li title='Maintenance'>" +
                          "<a href='#' ><i class='fa fa-cog' style=width:80px></i><span class='menu-item-parent' style=max-width:100px;align=center>Maintenance</span></a>" +
                          "<ul style='z-index:99999' class='ref-menu'>" +
                                "<li>" +
                                 "	<a href='#'>Consolidate Expense Type Routine</a>" +
                               "</li>" +
                               "<li>" +
                                "	<a href='../admin/Migration_Summary_V_4.aspx'>Loan Migration Comparison</a>" +
                                "</li>" +
                               "<li>" +
                                "	<a href='../admin/Loan_Purge_New.aspx'>Loan Purge</a>" +
                                "</li>" +
                                "<li>" +
                                 "	<a href='../admin/RiskOtherQueue_New.aspx'>Other Queue</a>" +
                               "</li>" + 
                                "<li>" +
                                 "	<a href='../admin/usersummary.aspx'>Location User Summary</a>" +
                               "</li>" +                               
                          "</ul>" +
                     " </li>" +
                 " </ul>" +

              "</nav>";

        return str_return;
    }

    public static Hashtable fc_counts_at_level(string inp_cc0)
    {
        Hashtable h_return = new Hashtable();

        //  gp_integrity_checks.sync_fc_ccs();

        string str_h_cc = "select cc1, count(*) from fc_repository where cc0 = " + inp_cc0 + " and cc1 > 0 and cc2 = -1 group by cc1"
                            + " union "
                            + "select cc2, count(*) from fc_repository where cc0 = " + inp_cc0 + " and cc2 > 0 and cc3 = -1 group by cc2"
                            + " union "
                             + "select cc3, count(*) from fc_repository where cc0 = " + inp_cc0 + " and cc3 > 0 and cc4 = -1 group by cc3"
                            ;
        h_return = gen_db_utils.gp_sql_get_hashtable(str_h_cc, dbKey);

        return h_return;
    }

    public static Hashtable t2q_counts_at_level(string inp_cc0)
    {
        Hashtable h_return = new Hashtable();

        //gp_integrity_checks.sync_type2_qs_ccs();

        string str_h_cc = "select cc1, count(*) from type2_qs where cc0 = " + inp_cc0 + " and cc1 > 0 and cc2 = -1 group by cc1"
                            + " union "
                            + "select cc2, count(*) from type2_qs where cc0 = " + inp_cc0 + " and cc2 > 0 and cc3 = -1 group by cc2"
                            + " union "
                             + "select cc3, count(*) from type2_qs where cc0 = " + inp_cc0 + " and cc3 > 0 and cc4 = -1 group by cc3"
                            ;
        h_return = gen_db_utils.gp_sql_get_hashtable(str_h_cc, dbKey);

        return h_return;
    }
}