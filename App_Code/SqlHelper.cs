﻿using System;
using System.Data;
using System.Reflection;
using System.Text;

/// <summary>
/// Summary description for SqlHelper
/// </summary>
public static class SqlHelper
{
    //public static int ExecuteNonQuery(string commandText, CommandType commandType, params SqlParameter[] commandParameters)
    //{
    //    int affectedRows = 0;
    //    using (var connection = ConnectionManager.GetSqlConnection())
    //    {
    //        using (var command = new SqlCommand(commandText, connection))
    //        {
    //            command.CommandType = commandType;
    //            command.Parameters.AddRange(commandParameters);
    //            affectedRows = command.ExecuteNonQuery();
    //        }
    //    }
    //    return affectedRows;
    //}

    public static string DataTableToJsonstring(this DataTable dt)
    {
        DataSet ds = new DataSet();
        ds.Merge(dt);
        StringBuilder JsonStr = new StringBuilder();
        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            JsonStr.Append("{");
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                JsonStr.Append("\"" + ds.Tables[0].Rows[i][0].ToString() + "\":" + "\"" + ds.Tables[0].Rows[i][1].ToString() + "\",");
            }
            JsonStr = JsonStr.Remove(JsonStr.Length - 1, 1); JsonStr.Append("}");
            return JsonStr.ToString();
        }
        else
        {
            return null;
        }
    }


    // function that creates an object from the given data row
    public static T CreateItemFromRow<T>(this DataRow row) where T : new()
    {
        // create a new object
        T item = new T();

        // set the item
        SetItemFromRow(item, row);

        // return 
        return item;
    }

    public static void SetItemFromRow<T>(T item, DataRow row) where T : new()
    {
        // go through each column
        foreach (DataColumn c in row.Table.Columns)
        {
            // find the property for the column
            PropertyInfo p = item.GetType().GetProperty(c.ColumnName);

            // if exists, set the value
            if (p != null && row[c] != DBNull.Value)
            {
                p.SetValue(item, row[c], null);
            }
        }
    }
}