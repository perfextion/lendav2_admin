﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Loan_Association
/// </summary>
public class Loan_Association
{
    public int Assoc_ID { get; set; }
    public int? Ref_Assoc_ID { get; set; }
    public int? Loan_ID { get; set; }
    public int? Loan_Seq_Num { get; set; }
    public string Loan_Full_ID { get; set; }
    public string Assoc_Type_Code { get; set; }
    public string Assoc_Name { get; set; }
    public int Principal_Ind { get; set; }
    public string Contact { get; set; }
    public string Location { get; set; }
    public string Phone { get; set; }
    public string Email { get; set; }
    public double? Amount { get; set; }
    public string Referred_Type { get; set; }
    public string Response { get; set; }
    public string Documentation { get; set; }
    public int? Is_CoBorrower { get; set; }
    public int? Preferred_Contact_Ind { get; set; }
    public int? Assoc_Status { get; set; }
    public int? Status { get; set; }
    public int? ActionStatus { get; set; }
    public int ActionDefault { get; set; }
    public bool IsDelete { get; set; }
    public string Office_ID { get; set; }
}

public class Loansettings
{
    public object insurance_policy_Settings { get; set; }
    public bool HrInclusion { get; set; }
    public dynamic validation_errors { get; set; }
    public errormodel[] Validations { get; set; }
    public object Loan_key_Settings { get; set; }
    public int committeeLevel { get; set; }
    public dynamic Export_Excel_Settings { get; set; }
    public object dashboardSettings { get; set; }
    public object reportsSettings { get; set; }

}

public class errormodel
{
    public string loan_full_id { get; set; }
    public string tab { get; set; }
    public string tab_id { get; set; }
    public string cellid { get; set; }
    public string rowId { get; set; }
    public string[] details { get; set; }
    public string chevron { get; set; }
    public string ToolTipText { get; set; }
    public int level { get; set; }
    public int? questionId { get; set; }
    public int? Validation_ID { get; set; }
    public string Validation_ID_Text { get; set; }
    public string hoverText { get; set; }
    public bool ignore { get; set; }
    public string Status { get; set; }
}