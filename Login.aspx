﻿<%@ Page Language="C#" AutoEventWireup="true" ValidateRequest="false" CodeFile="Login.aspx.cs" Inherits="Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title>Synthesis | Admin</title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="icon" type="image/x-icon" href="images/lendaplus_logo.ico">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,400italic" />

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />

    <script src='https://cdnjs.cloudflare.com/ajax/libs/nprogress/0.2.0/nprogress.min.js'></script>
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/nprogress/0.2.0/nprogress.min.css' />
    <style>
        .auth-page {
            position: absolute;
            top: 0;
            left: 0;
            background-position: center center;
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-size: cover;
            transition: .5s;
            width: 100%;
            height: 100%;
            display: flex;
            align-items: center;
            justify-content: center;
            background: #333;
        }

        .bg1 {
            background-image: url(./assets/images/bg/arm-bg.png)
        }

        .bg2 {
            background-image: url(./assets/images/bg/arm-bg2.png)
        }

        .bg3 {
            background-image: url(./assets/images/bg/arm-bg3.png)
        }

        .bg4 {
            background-image: url(./assets/images/bg/arm-bg4.png)
        }

        .logo-header {
            margin: 10px 0
        }

        .logo {
            width: 140px;
            height: 25px
        }

        .plus {
            color: #2ebda3;
            font-style: italic;
            font-size: 22px;
            font-weight: 700
        }

        h5 {
            color: #bdcffd;
            text-align: center;
            font-weight: 500
        }

        .login-card {
            border-radius: 5px;
            -webkit-animation: 2s forwards fade-in;
            animation: 2s forwards fade-in
        }

        .login-header {
                height: 90px;
                background-color: rgb(226,127,55);
                border-top-left-radius: 5px;
                border-top-right-radius: 5px;
        }

        .login-header .logo-header {
            margin: 10px 0px;
            text-align: center;
        }

        .login-header .logo-header img.logo {
            width: auto;
            height: 70px;
            margin-top: 10px;
        }

        .login-content > form {
            height: 450px;
            width: 380px;
            border-bottom-left-radius: 5px;
            border-bottom-right-radius: 5px;
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
            background-color: rgb(217, 217, 217);
        }

        .form-input {
            margin: 10px 0;
            color: #4980c7;
            font-weight: 500;
            text-align: center
        }

        .mat-form-field-underline {
            height: 2px !important
        }

        @-webkit-keyframes fade-in {
            0% {
                -webkit-transform: rotate(0) translateY(-20%);
                transform: rotate(0) translateY(-20%);
                display: none;
                opacity: 0
            }

            100% {
                -webkit-transform: rotate(0) translateY(0);
                transform: rotate(0) translateY(0);
                display: block
            }
        }

        @keyframes fade-in {
            0% {
                -webkit-transform: rotate(0) translateY(-20%);
                transform: rotate(0) translateY(-20%);
                display: none;
                opacity: 0
            }

            100% {
                -webkit-transform: rotate(0) translateY(0);
                transform: rotate(0) translateY(0);
                display: block
            }
        }

        .login-form-input {
            margin-top: 10px;
            width: 320px;
            flex-grow: 1
        }

        .chk-remember-me {
            margin: 0
        }

        .btn-signin {
            width: 105px;
            height: 35px;
            font-size: 13px;
            font-weight: 500;
            background-color: #3177e6;
            margin-bottom: 90px;
            box-shadow: 0 -1px 2px 0 #5091e5 inset,0 1px 2px 3px #065881
        }

            .btn-signin :hover {
                background-color: #1db796;
                box-shadow: 0 -1px 2px 0 #2f8272 inset,0 1px 2px 3px #2e8271
            }

        .login-actions {
            display: flex;
            flex-direction: column;
            align-items: center;
            font-weight: 400;
            font-size: 12px;
            flex-grow: 1
        }

        .other-login-opt {
            text-align: center;
            margin-bottom: 20px
        }

        .forgot-password {
            font-size: 12px
        }

        .register {
            text-decoration: underline
        }

        .fab-arm {
            position: fixed;
            right: 10px;
            bottom: 10px;
            height: 7vh
        }

        .login-inputs {
            margin: 10px 0
        }

            .login-inputs > span {
                color: #fff
            }

            .login-inputs > input {
                height: 35px;
                width: 100%;
                padding: 5px;
                border: 2px solid #5091e5
            }

        @media (min-width:320px) and (max-width:767px) {
            .login-content > form {
                height: 400px;
                width: 300px
            }

            .login-form-input {
                width: 280px;
                margin-top: -10px;
            }

            .login-header .logo-header img.logo {
                height: 60px;
                margin-top: 15px;
            }
        }

        .btn-signin:hover {
            background-color: #1db796;
            box-shadow: 0 -1px 2px 0 #2f8272 inset, 0 1px 2px 3px #2e8271;
        }

        body {
            font-family: Roboto,sans-serif;
            font-size: 12px;
        }

        .btn:focus {
            outline: none;
        }
    </style>
    <script>

</script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript"></script>


</head>
<body>
    <div id="background" class="auth-page">
        <div class="overlay">
            <div class="login-card mat-elevation-z14">
                <div class="login-header">
                    <div>
                      <div class="logo-header">
                        <img src="./assets/images/synthesis.white-1.png" class="logo" alt="Lenda Logo"/>
                      </div>
                    </div>
                </div>
                <div class="login-content">                    
                    <form novalidate="" class="ng-dirty ng-touched ng-valid" runat="server">
                        <h4>Admin</h4>
                        <div class="login-form-input">
                            <div class="login-inputs">
                                <span>username</span>
                                <%--<input id="inputEmail" name="LoginId" placeholder="" required="" type="text" class="ng-dirty ng-valid ng-touched" />--%>

                                <asp:TextBox runat="server" ID="txtUserName" />


                            </div>
                            <div class="login-inputs">
                                <span>password</span>
                                <%--<input id="inputPassword" name="Password" placeholder="" required="" type="password" class="ng-dirty ng-valid ng-touched" />--%>
                                <asp:TextBox runat="server" TextMode="Password" ID="txtPassword" />
                                <section style="margin-left: 22px;">
                                    <label class="checkbox">
                                        <input type="checkbox" name="remember" checked="checked" />
                                        <i></i>remember me</label>
                                </section>
                                <asp:Label ID="lblerrormsg" Style="color: red" runat="server" />
                            </div>
                            <div class="login-actions">
                                <asp:Button Text="Log in" ID="btnSignIn" class="btn btn-lg btn-block btn-signin" OnClick="btnSignIn_Click" runat="server" Style="border-radius: 15px; color: white; border-radius: 15px; color: white; margin-top: 32px; padding-top: 8px; width: 30%; outline: none;" />

                                <%--<asp:Button Text="Sign in" ID="btnSignIn" OnClick="btnSignIn_Click" runat="server" class="btn btn-primary" />--%>

                                <div class="other-login-opt">
                                    <span class="forgot-password">Forgot password? Need an account?</span><br />
                                    <span class="register">Register Here</span>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <img alt="arm_lets_grow" class="fab-arm" src="./assets/images/bg/arm_lets_grow.png" />
    <script type="text/javascript">
        $(document).ready(function () {
            NProgress.start();
        });
        $(window).load(function () {
            //setTimout() for 2 seconds progress stop delay
            setTimeout(function () { NProgress.done() }, 2000);
        });
    </script>
</body>
</html>
