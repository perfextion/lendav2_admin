﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Login : System.Web.UI.Page
{
    static string dbKey = "gp_conn";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["st_1"] == null) load_lookups();
            load_lookups();
        }
    }

    protected void btnSignIn_Click(object sender, EventArgs e)
    {

        // string qry = "Select count(*) from HR_Users where Username='" + txtUserName.Text + "' and Password='" + txtPassword.Text + "' ";
        string qry = "Select count(*) from Users where Username='" + txtUserName.Text + "'";

        DataTable dt = gen_db_utils.gp_sql_get_datatable(qry, dbKey);

        string valid = dt.Rows[0]["Column1"].ToString();

        if (valid != "0")
        {
            string password = "admin";

            string query = "Select * from Users where Username='" + txtUserName.Text + "'";
            DataTable dat = gen_db_utils.gp_sql_get_datatable(query, dbKey);

            DataRow row = dat.Rows[0];

            string Username = row["Username"].ToString();
            bool Access = row["IsAdmin"].ToString() == "1" && row["UserID"].ToString() != "1";
            string loggedinuser = row["UserID"].ToString();

            if (Username.ToLower() == txtUserName.Text.ToLower() && password.ToLower() == txtPassword.Text.ToLower())
            {
                Session["access"] = Access ? 1 : 0;
                Session["Username"] = txtUserName.Text;
                Session["UserID"] = loggedinuser;
                Session["IsSysAdmin"] = row["SysAdmin"];
                Session["IsAdmin"] = row["IsAdmin"];
                Session["IsDevAdmin"] = row["DevAdmin"];
                Session["IsRiskAdmin"] = row["RiskAdmin"];
                Response.Redirect("~/admin/dashboard1.aspx");
            }
            else
            {
                lblerrormsg.Text = "User Is Inactive";
                return;
            }
        }
        else
        {
            lblerrormsg.Text = "Invalid credentials";
            return;
        }

    }

    protected void load_lookups()
    {
        string strSql = " select state_id,state_abbrev,state_name from ref_state ";
        DataTable dt1 = gen_db_utils.gp_sql_get_datatable(strSql, dbKey);
        foreach (DataRow dtr1 in dt1.Rows)
        {
            Session["st_" + dtr1["state_id"].ToString()] = dtr1["state_name"].ToString();
            Session["st_" + dtr1["state_abbrev"].ToString()] = dtr1["state_name"].ToString();

        }
        strSql = " select county_id, county_name from ref_county ";
        dt1 = gen_db_utils.gp_sql_get_datatable(strSql, dbKey);
        foreach (DataRow dtr1 in dt1.Rows)
        {
            Session["county_" + dtr1["county_id"].ToString()] = dtr1["county_name"].ToString();
        }

        strSql = " select crop_and_practice_id, crop_full_key from v_crop_price_details ";
        dt1 = gen_db_utils.gp_sql_get_datatable(strSql, dbKey);
        foreach (DataRow dtr1 in dt1.Rows)
        {
            Session["cp_" + dtr1["crop_and_practice_id"].ToString()] = dtr1["crop_full_key"].ToString();
        }

        strSql = " select userid,username from Users ";
        dt1 = gen_db_utils.gp_sql_get_datatable(strSql, dbKey);
        foreach (DataRow dtr1 in dt1.Rows)
        {
            Session["user_" + dtr1["userid"].ToString()] = dtr1["username"].ToString();
        }


    }
}

