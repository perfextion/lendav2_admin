﻿<%@ Page Title="" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="Add_Z_Doc_Index.aspx.cs" Inherits="admin_Add_Z_Doc_Index" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet" />
    <script src="//code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>

    <!-- jquery-contextmenu (https://github.com/mar10/jquery-ui-contextmenu/) -->
    <script src="//cdn.jsdelivr.net/npm/ui-contextmenu/jquery.ui-contextmenu.min.js"></script>

    <link href="http://wwwendt.de/tech/fancytree/src/skin-win8/ui.fancytree.css" rel="stylesheet" />
    <script src="http://wwwendt.de/tech/fancytree/src/jquery.fancytree.js"></script>
    <script src="http://wwwendt.de/tech/fancytree/src/jquery.fancytree.dnd.js"></script>
    <script src="http://wwwendt.de/tech/fancytree/src/jquery.fancytree.edit.js"></script>
    <script src="http://wwwendt.de/tech/fancytree/src/jquery.fancytree.gridnav.js"></script>
    <script src="http://wwwendt.de/tech/fancytree/src/jquery.fancytree.table.js"></script>
    <!--
  <script src="../../build/jquery.fancytree-all.min.js"></script>
-->

    <!-- (Irrelevant source removed.) -->

    <style type="text/css">
        .ui-menu {
            width: 180px;
            font-size: 63%;
        }

            .ui-menu kbd { /* Keyboard shortcuts for ui-contextmenu titles */
                float: right;
            }
        /* custom alignment (set by 'renderColumns'' event) */
        td.alignRight {
            text-align: right;
        }

        td.alignCenter {
            text-align: center;
        }

        td input[type=input] {
            width: 40px;
        }

        span.fancytree-title {
            width: 300px
        }
    </style>

    <div id="btnSave" class="btn btn-success" style="width: 60px;">Save</div>
    <div class="loader"></div>
    <br />
    <br />
    <table id="tree">
        <colgroup>
            <col width="30px" />
            <col width="50px" />
            <col width="450px" />
            <%-- <col width="50px" />--%>
        </colgroup>
        <thead>
            <tr>
                <th></th>
                <th></th>
                <th></th>
                <%--<th>Ed1</th>--%>
            </tr>
        </thead>
        <tbody>
            <!-- Define a row template for all invariant markup: -->
            <tr>
                <td class="alignCenter"></td>
                <td></td>
                <td></td>
                <%--<td>
                    <input name="input1" type="input" />
                </td>--%>
            </tr>
        </tbody>
    </table>


    <script type="text/javascript">
        var CLIPBOARD = null;
        var DeleteRecords = [];
        $(function () {
            $.ajax({
                type: "POST",
                url: "Add_Z_Doc_Index.aspx/GetSections",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    $('.loader').show();
                    var res = getJsonData(JSON.parse(data.d));
                    LoadTree(res);
                    $('.loader').hide();
                }
            });
            $("#btnSave").click(function () {
                $('.loader').show();
                var allKeys = $.map($('#tree').fancytree('getRootNode').getChildren(), function (node) {
                    return node;
                });
                var res = [];
                var output = {}
                for (var i = 0; i < allKeys.length; i++) {
                    var obj = {}
                    obj.title = allKeys[i].title;
                    obj.id = allKeys[i].data.id;
                    obj.childrens = [];
                    recursive(allKeys[i], obj.childrens);
                    res.push(obj);
                }
                $.ajax({
                    type: "POST",
                    url: "Add_Z_Doc_Index.aspx/SaveTree",
                    data: JSON.stringify({ lst: res, deleteRecords: DeleteRecords }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        location.reload();
                    },
                    failure: function (response) {
                        alert(response.d);
                    }
                });
            });
            function recursive(object, node) {
                if (object != null && object != undefined && object.children != null && object.children != undefined)
                    for (let child of object.children) {
                        var item = {};
                        item.id = child.data.id;
                        item.title = child.title;
                        item.childrens = [];
                        var res = [];
                        if (child.children != null) {
                            var obj = {}
                            obj.title = item.title;
                            obj.id = item.id;
                            obj.childrens = [];
                            recursive(child, item.childrens);
                        }
                        node.push(item);
                    }
            }

            function LoadTree(res) {
                $("#tree").fancytree({
                    checkbox: false,
                    titlesTabbable: true,     // Add all node titles to TAB chain
                    quicksearch: true,        // Jump to nodes when pressing first character
                    source: res,
                    extensions: ["edit", "dnd", "table", "gridnav"],
                    dnd: {
                        preventVoidMoves: true,
                        preventRecursiveMoves: true,
                        autoExpandMS: 400,
                        dragStart: function (node, data) {
                            return true;
                        },
                        dragEnter: function (node, data) {
                            // return ["before", "after"];
                            return true;
                        },
                        dragDrop: function (node, data) {                           
                            data.otherNode.moveTo(node, data.hitMode);
                        }
                    },
                    edit: {
                        triggerStart: ["f2", "shift+click", "mac+enter"],
                        close: function (event, data) {                            
                            if (data.save && data.isNew) {
                                // Quick-enter: add new nodes until we hit [enter] on an empty title
                                $("#tree").trigger("nodeCommand", { cmd: "addSibling" });
                            }
                        }
                    },
                    table: {
                        indentation: 20,
                        nodeColumnIdx: 2,
                        checkboxColumnIdx: 0
                    },
                    gridnav: {
                        autofocusInput: false,
                        handleCursorKeys: true
                    },

                    //lazyLoad: function (event, data) {
                    //    data.result = { url: "../demo/ajax-sub2.json" };
                    //},
                    createNode: function (event, data) {                        
                        var node = data.node,
                            $tdList = $(node.tr).find(">td");
                        // Span the remaining columns if it's a folder.
                        // We can do this in createNode instead of renderColumns, because
                        // the `isFolder` status is unlikely to change later
                        if (node.isFolder()) {
                            $tdList.eq(2)
                                .prop("colspan", 6)
                                .nextAll().remove();
                        }
                    },
                    renderColumns: function (event, data) {
                        var node = data.node,
                            $tdList = $(node.tr).find(">td");

                        // (Index #0 is rendered by fancytree by adding the checkbox)
                        // Set column #1 info from node data:
                        $tdList.eq(1).text(node.getIndexHier());
                        // (Index #2 is rendered by fancytree)
                        // Set column #3 info from node data:
                        $tdList.eq(3).find("input").val(node.key);
                        $tdList.eq(4).find("input").val(node.data.foo);

                        // Static markup (more efficiently defined as html row template):
                        // $tdList.eq(3).html("<input type='input' value='" + "" + "'>");
                        // ...
                    }
                }).on("nodeCommand", function (event, data) {
                    // Custom event handler that is triggered by keydown-handler and
                    // context menu:
                    var refNode, moveMode,
                        tree = $(this).fancytree("getTree"),
                        node = tree.getActiveNode();

                    switch (data.cmd) {
                        case "moveUp":
                            refNode = node.getPrevSibling();
                            if (refNode) {
                                node.moveTo(refNode, "before");
                                node.setActive();
                            }
                            break;
                        case "moveDown":
                            refNode = node.getNextSibling();
                            if (refNode) {
                                node.moveTo(refNode, "after");
                                node.setActive();
                            }
                            break;
                        case "indent":
                            refNode = node.getPrevSibling();
                            if (refNode) {
                                node.moveTo(refNode, "child");
                                refNode.setExpanded();
                                node.setActive();
                            }
                            break;
                        case "outdent":
                            if (!node.isTopLevel()) {
                                node.moveTo(node.getParent(), "after");
                                node.setActive();
                            }
                            break;
                        case "rename":
                            node.editStart();
                            break;
                        case "remove":
                            refNode = node.getNextSibling() || node.getPrevSibling() || node.getParent();
                            DeleteRecords.push(node.data.id);
                            node.remove();
                            if (refNode) {
                                refNode.setActive();
                            }
                            break;
                        case "addChild":
                            node.editCreateNode("child", "");                            
                            break;
                        case "addSibling":
                            node.editCreateNode("after", "");                            
                            break;
                        case "cut":
                            CLIPBOARD = { mode: data.cmd, data: node };
                            break;
                        case "copy":
                            CLIPBOARD = {
                                mode: data.cmd,
                                data: node.toDict(function (n) {
                                    delete n.key;
                                })
                            };
                            break;
                        case "clear":
                            CLIPBOARD = null;
                            break;
                        case "paste":
                            if (CLIPBOARD.mode === "cut") {
                                // refNode = node.getPrevSibling();
                                CLIPBOARD.data.moveTo(node, "child");
                                CLIPBOARD.data.setActive();
                            } else if (CLIPBOARD.mode === "copy") {
                                node.addChildren(CLIPBOARD.data).setActive();
                            }
                            break;
                        default:
                            alert("Unhandled command: " + data.cmd);
                            return;
                    }

                    // }).on("click dblclick", function(e){
                    //   console.log( e, $.ui.fancytree.eventToString(e) );

                }).on("keydown", function (e) {
                    var cmd = null;

                    // console.log(e.type, $.ui.fancytree.eventToString(e));
                    switch ($.ui.fancytree.eventToString(e)) {
                        case "ctrl+shift+n":
                        case "meta+shift+n": // mac: cmd+shift+n
                            cmd = "addChild";
                            break;
                        case "ctrl+c":
                        case "meta+c": // mac
                            cmd = "copy";
                            break;
                        case "ctrl+v":
                        case "meta+v": // mac
                            cmd = "paste";
                            break;
                        case "ctrl+x":
                        case "meta+x": // mac
                            cmd = "cut";
                            break;
                        case "ctrl+n":
                        case "meta+n": // mac
                            cmd = "addSibling";
                            break;
                        case "del":
                        case "meta+backspace": // mac
                            cmd = "remove";
                            break;
                        // case "f2":  // already triggered by ext-edit pluging
                        //   cmd = "rename";
                        //   break;
                        case "ctrl+up":
                            cmd = "moveUp";
                            break;
                        case "ctrl+down":
                            cmd = "moveDown";
                            break;
                        case "ctrl+right":
                        case "ctrl+shift+right": // mac
                            cmd = "indent";
                            break;
                        case "ctrl+left":
                        case "ctrl+shift+left": // mac
                            cmd = "outdent";
                    }
                    if (cmd) {
                        $(this).trigger("nodeCommand", { cmd: cmd });
                        // e.preventDefault();
                        // e.stopPropagation();
                        return false;
                    }
                });
            }

            $("#tree").contextmenu({
                delegate: "span.fancytree-node",
                menu: [
                    { title: "Edit <kbd>[F2]</kbd>", cmd: "rename", uiIcon: "ui-icon-pencil" },
                    { title: "Delete <kbd>[Del]</kbd>", cmd: "remove", uiIcon: "ui-icon-trash" },
                    { title: "----" },
                    { title: "New sibling <kbd>[Ctrl+N]</kbd>", cmd: "addSibling", uiIcon: "ui-icon-plus" },
                    { title: "New child <kbd>[Ctrl+Shift+N]</kbd>", cmd: "addChild", uiIcon: "ui-icon-arrowreturn-1-e" },
                    { title: "----" },
                    { title: "Cut <kbd>Ctrl+X</kbd>", cmd: "cut", uiIcon: "ui-icon-scissors" },
                    { title: "Copy <kbd>Ctrl-C</kbd>", cmd: "copy", uiIcon: "ui-icon-copy" },
                    { title: "Paste as child<kbd>Ctrl+V</kbd>", cmd: "paste", uiIcon: "ui-icon-clipboard", disabled: true }
                ],
                beforeOpen: function (event, ui) {
                    var node = $.ui.fancytree.getNode(ui.target);
                    $("#tree").contextmenu("enableEntry", "paste", !!CLIPBOARD);
                    node.setActive();
                },
                select: function (event, ui) {
                    var that = this;
                    // delay the event, so the menu can close and the click event does
                    // not interfere with the edit control
                    setTimeout(function () {
                        $(that).trigger("nodeCommand", { cmd: ui.cmd });
                    }, 100);
                }
            });
        });

        function getJsonData(dataobj) {

            var ParentData = $.grep(dataobj, function (e) {
                if (e != undefined) {
                    return e.ParentId == 0
                }
            });
            var res = [];
            loadTreeRecursive(dataobj, ParentData, res);
            return res;
        }
        function loadTreeRecursive(dataobj, ParentData, res) {

            for (var i = 0; i < ParentData.length; i++) {
                var ChildData = $.grep(dataobj, function (e) {
                    if (e != undefined) {
                        return e.ParentId == ParentData[i].Id
                    }
                });
                var childens = [];
                loadTreeRecursive(dataobj, ChildData, childens);
                res.push({
                    title: ParentData[i].Title,
                    folder: (childens.length > 0) ? true : false,
                    id: ParentData[i].Id,                  
                    children: childens
                });

            }
        }

    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" runat="Server">
</asp:Content>

