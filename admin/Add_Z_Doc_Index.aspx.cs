﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_Add_Z_Doc_Index : System.Web.UI.Page
{
    static string dbKey = "gp_conn";
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    [WebMethod]
    public static string GetSections()
    {
        string sql_qry = "select Id,Title,ParentId from Z_Doc_Index order by SortOrder";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static void SaveTree(Z_Doc_Index[] lst, List<int> deleteRecords)
    {       
        SaveRecursive(lst, 0);
        for (int i = 0; i < deleteRecords.Count; i++)
        {
            string qry = " DELETE FROM Z_DOC_INDEX WHERE ID=" + deleteRecords[i];
            gen_db_utils.gp_sql_execute(qry, dbKey);
        }
    }

    public static void SaveRecursive(Z_Doc_Index[] lst, int parentID)
    {
        int sortOrder = 0;
        foreach (Z_Doc_Index item in lst)
        {
            string sql_qry = "";
            if (item.id > 0)
            {
                sql_qry = "update Z_Doc_Index set Title='" + item.title + "',ParentID='" + parentID + "',SortOrder=" + sortOrder + " where ID=" + item.id + "";
            }
            else
            {
                sql_qry = "INSERT INTO Z_Doc_Index(Title,ParentID,SortOrder) VALUES('" + item.title + "'," + parentID + "," + sortOrder + ")";
            }
            string res = gen_db_utils.base_sql_scalar(sql_qry, dbKey);
            if (item.childrens != null && item.childrens.Count() > 0)
            {
                SaveRecursive(item.childrens, item.id);
            }
            sortOrder++;
        }
    }

    public static string BindDocumentDetails(DataTable dt)
    {
        string strOutput = "";
        if (dt != null && dt.Rows.Count > 0)
        {
            foreach (DataRow dtr1 in dt.Rows)
            {
                DocKeyWord[] keyWord = null;
                string keywordsStr = "<ul id='lstKey' class='list-inline'>";
                if (dtr1["Key_Fields"] != null && !String.IsNullOrEmpty(dtr1["Key_Fields"].ToString()))
                {
                    keyWord = Newtonsoft.Json.JsonConvert.DeserializeObject<DocKeyWord[]>(dtr1["Key_Fields"].ToString());
                }

                if (keyWord != null)
                {
                    foreach (var item in keyWord)
                    {
                        keywordsStr = keywordsStr + " <li> <div class='chip'><span class='keyword'>" + item.title + "</span> </div> </li>";
                    }

                }
                string strDocTitle = (!string.IsNullOrEmpty(dtr1["id"].ToString())) ? "<h5>" + dtr1["ParentTitle"].ToString() + "," + dtr1["Title"].ToString() + "</h5>" : "";
                keywordsStr = keywordsStr + "</ul>";
                strOutput += "<h2 style='padding-top:10px'>" + dtr1["Page_Name"].ToString() + "</h2>"
                                         + strDocTitle
                                          // + "<h5>" + dtr1["ParentTitle"].ToString() + "," + dtr1["Title"].ToString() + "</h5>"
                                          + "<a href='/admin/Document_View.aspx?DocId= " + dtr1["Z_Doc_ID"] + "&pageName=" + dtr1["Page_Name"] + "&tabName=" + dtr1["Tab_Name"] + "' >  [view] </a>"
                                          + "<a href='/admin/Document_Editor.aspx?DocId= " + dtr1["Z_Doc_ID"] + "&pageName=" + dtr1["Page_Name"] + "&tabName=" + dtr1["Tab_Name"] + "' >  [edit] </a>"
                                          + "<a href='#' onclick='copyContent(" + dtr1["Z_Doc_ID"] + ")' >  [copy] </a>"
                                          + "<div class='col-md-12' style='padding-bottom:5px;'> " + keywordsStr + " </div>"
                                          + "<p>" + dtr1["doc_body"].ToString() + "</p>";
            }
        }
        else
            strOutput = "no documents available...";

        return strOutput;
    }
}

public class Z_Doc_Index
{
    public int id { get; set; }
    public string title { get; set; }
    public Z_Doc_Index[] childrens { get; set; }
}