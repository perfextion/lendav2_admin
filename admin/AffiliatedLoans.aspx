﻿<%@ Page Title="" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="AffiliatedLoans.aspx.cs" Inherits="admin_AffiliatedLoans" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    

    <link href='../css/fullcalendar.min.css' rel='stylesheet' />
    <link href='../css/fullcalendar.print.min.css' rel='stylesheet' media='print' />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" />
    <script src='../js/moment.min.js'></script>
    <script src='../js/fullcalendar/jquery.min.js'></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src='../js/fullcalendar/fullcalendar.min.js'></script>

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <div class="loader"></div>
    <div class="col-md-12 col-sm-4" style="margin-top: 15px">

        <%--<div class="col-md-10" style="margin-left: 400px">--%>
        <div class="col-md-2">
        </div>
        <div class="col-md-6">
            <div class="form-inline">
                <label for="txtSearch1">Farmer Id:</label>
                <input type="text" class="form-control" name="search" id="txtSearch" style="width: 200px" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="loader"></div>
        <div class="col-md-11" style="padding-left: 51px; padding-top: 8px;">
            <asp:Label ID="lbl_table" runat="server" Text="lbl_table"></asp:Label>
        </div>
    </div>
    
    
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <%--style="width:758px"--%>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Loan Details</h4>
                </div>
                <div class="modal-body">
                    <div class="row" style="margin-left: 5px">
                        <div id="divModelSave">
                            <div class="col-md-12">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtSearch1">Loan Master id</label>
                                        <input type="text" id="txtloanMasterID" class="form-control" disabled="disabled" style="width: 200px" />
                                    </div>
                                </div>                                
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtSearch1">Loan Full id</label>
                                        <input type="text" id="txtloanfullid" class="form-control"style="width: 200px" />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtSearch1">Crop Year</label>
                                        <input type="text" id="txtcropyear" class="form-control" name="UserName" style="width: 200px" />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtSearch1"> Region Id</label>
                                        <input type="text" id="txtregionId" class="form-control" name="UserName" style="width: 200px" />
                                    </div>
                                </div>
                           <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtSearch1">Pre Year Loan Id</label>
                                        <input type="text" id="txtpreyearid" class="form-control" name="UserName" style="width: 200px" />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtSearch1">Farmer name</label>
                                        <input type="text" id="txtFormarname" class="form-control" disabled="disabled" name="UserName" style="width: 200px" />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtSearch1">Borrower name</label>
                                        <input type="text" id="txtBorrowername" class="form-control" disabled="disabled" name="UserName" style="width: 200px" />
                                    </div>
                                </div>                               
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-8">
                                </div>
                                <div class="col-md-4 text-center">
                                    <div class="btn btn-primary" id="btnModalSave">Save</div>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>

            </div>
            
        </div>
    </div>
    
    <script>
         $(function () {
            $("#txtSearch").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "AffiliatedLoans.aspx/GetLoanFullId",
                        data: "{searchVal:'" + $("#txtSearch").val() + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            response(data.d);
                        }
                    });
                },
                select: function (event, ui) {
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "AffiliatedLoans.aspx/LoadTable",
                        data: "{farmer_id:'" + ui.item.value + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            $('#ContentPlaceHolder1_lbl_table').html(data.d);
                        }
                    });
                }
            });

             $("#navbtndownload").click(function () {
                 exportTableToExcel('tblLoanlist', 'Affiliated Loans');
             });

             $("#navbtnrefresh").click(function () {
                 $('.loader').show();
                 obj.loadtable();
             });

             $('#txtSearchBar').on('keyup', function () {
                 var value = $(this).val().toLowerCase();

                 var count = 0;
                 $("#tblLoanlist tbody tr").filter(function () {
                     var lineStr = $(this).text().toLowerCase();
                     if (lineStr.indexOf(value) === -1) {
                         $(this).hide();
                     } else {
                         $(this).show();
                         count++;
                     }
                 });
             });

            var obj = {
                Init: function () {
                    obj.buttonsave();
                    $('.loader').hide();
                    $("#txtSearch").on("keyup", function () {
                        var value = $('#txtSearch').val();
                        if (value == '') {
                            var self = this;
                            obj.loadtable();
                        }
                    });
                    $('#ddlAssocTypeCode').change(function () {
                        var assocTypecode = $('#ddlAssocTypeCode').val();
                        $('#ContentPlaceHolder1_lbl_table').text('');
                        obj.changeAssocTypeCode(assocTypecode);
                    });
                    $('#btnModalYes').click(function () {
                        obj.buttonYes();
                    });
                },                
                buttonsave: function () {
                    $('#btnModalSave').click(function () {
                        $('.loader').show();                        
                        var Masterid = $('#txtloanMasterID').val();
                        var loanfullid = $('#txtloanfullid').val();
                        var cropyear= $('#txtcropyear').val();
                        var regionid = $('#txtregionId').val();
                        var preyearid = $('#txtpreyearid').val();

                        var divModelSave = $('#divModelSave').show();
                        $('#spnException').hide();
                        $.ajax({
                            type: "POST",
                            url: "AffiliatedLoans.aspx/Updateloandetails",
                            data: JSON.stringify({ MasterId: Masterid,LoanfullId:loanfullid,CropyearId: cropyear, Regionid: regionid ,PreyearId: preyearid }),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (data) {
                                $('.loader').show();
                                obj.loadtable();
                                $('#myModal').modal('hide');
                            },
                            failure: function (response) {
                            }
                        });
                    });
                },             
                loadtable: function () {
                    var farmerid = $('#txtSearch').val();
                    $('#ContentPlaceHolder1_lbl_table').text('');
                    $.ajax({
                        type: "POST",
                        url: "AffiliatedLoans.aspx/LoadTable",
                        data: JSON.stringify({ farmer_id: farmerid }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            $('.loader').show();
                            $('#ContentPlaceHolder1_lbl_table').html(res.d);
                            $('.loader').hide();
                        }
                    });

                }
            }
            obj.Init();
        });
        function manageRecord(id) {
            $('#divUserID').show();
            $.ajax({
                type: "POST",
                url: "AffiliatedLoans.aspx/GetFarmerDetails",
                data: JSON.stringify({ MasterId: id }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (res) {
                    var objdata = JSON.parse(res.d);
                    var data = objdata[0];
                    
                    $('#txtloanMasterID').val(data.Loan_Master_ID);
                    $('#txtloanfullid').val(data.loan_full_id);
                    $('#txtcropyear').val(data.crop_year);
                    $('#txtregionId').val(data.region_id);
                    $('#txtpreyearid').val(data.Prev_Yr_Loan_ID);
                    $('#txtFormarname').val(data.farmer_first_name+' '+data.farmer_last_name,);
                    $('#txtBorrowername').val(data.Borrower_first_name+' '+data.borrower_last_name);
                }
            });
            $('#divModelSave').show();
            $('#myModal').modal('show');
        }
    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" Runat="Server">
</asp:Content>

