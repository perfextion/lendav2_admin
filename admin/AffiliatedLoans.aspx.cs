﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Text;
using System.Web.UI.WebControls;

public partial class admin_AffiliatedLoans : System.Web.UI.Page
{
    static string dbKey = "gp_conn";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            lbl_table.Text = LoadTable("");
        }
    }
    [WebMethod]
    public static string LoadTable(string farmer_id)
    {
        string strQry = "";

        strQry = "select Aff_ID, Loan_Master_ID, Loan_Status, loan_full_id, crop_year, region_name, office_name, farmer_id," +
            "farmer_last_name,farmer_first_name ,borrower_id, borrower_last_name , Borrower_first_name, Active_Ind from loan_master ";

        if (!string.IsNullOrEmpty(farmer_id))
            strQry += " Where farmer_id='" + farmer_id + "'";

        DataTable dt = gen_db_utils.gp_sql_get_datatable(strQry, dbKey);
        return GeneteateGrid(dt);
    }

    [WebMethod]
    public static string GetFarmerDetails(string MasterId)
    {
        string sql_qry = "select Loan_Master_ID,loan_full_id, crop_year, region_id, Prev_Yr_Loan_ID, farmer_id, farmer_last_name , farmer_first_name ,borrower_id, borrower_last_name , Borrower_first_name from loan_master where Loan_Master_ID = '" + MasterId + "'";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);

    }
    [WebMethod]
    public static List<string> GetLoanFullId(string searchVal)
    {
        string sql_qry = " select distinct farmer_id from Loan_Master where farmer_id like '%" + searchVal + "%'";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        List<string> emp = new List<string>();

        return (from DataRow row in dt.Rows
                select row["farmer_id"].ToString()
               ).ToList();
    }

    [WebMethod]
    public static string GetLoanByID(string loan_id)
    {
        string sql_qry = "select * from Loan_Master  where Loan_Full_ID='" + loan_id + "'";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);

    }
    [WebMethod]
    public static void Updateloandetails(string MasterId, string LoanfullId, string CropyearId, string Regionid, string PreyearId)
    {
        string qry = "";
        qry = "update loan_master set Loan_Full_Id='" + LoanfullId + "',crop_year='" + CropyearId + "',region_id='" + Regionid + "'," +
        "Prev_Yr_Loan_ID='" + PreyearId + "' where Loan_Master_ID='" + MasterId + "'";

        gen_db_utils.gp_sql_execute(qry, dbKey);
    }
    [WebMethod]
    public static string GeneteateGrid(DataTable dt)
    {
        StringBuilder sb = new StringBuilder();
        string str_return = "";
        if (dt != null && dt.Rows.Count > 0)
        {
            str_return += "<span style='color: green;'>Count of records : " + dt.Rows.Count + "</span><div> <table id='tblLoanlist' class='table table-striped table-bordered' >  "
                                 + "<thead> "
                                 + "     <tr> "
                                      + "       <th >Farmer Affiliate ID</th> "
                                      + "       <th >Farmer Name</th> "
                                      + "       <th >Loan Status</th> "
                                      + "       <th >Loan Full ID</th> "
                                      + "       <th >Borrower Name</th> "
                                      + "       <th >Crop Year</th> "
                                      + "       <th >Region Name</th> "
                                      + "       <th >Office Name</th> "
                                      + "       <th >Status</th> "
                                 + "    </tr> "
                                 + "      </thead> "
                                 + "        <tbody> ";

            foreach (DataRow dtr1 in dt.Rows)
            {
                str_return += "<tr>"
                               //select loan_full_id, crop_year, region_id, Prev_Yr_Loan_ID, farmer_id," +
                               //"farmer_last_name,farmer_first_name ,borrower_id, borrower_last_name , Borrower_first_name
                               + "<td>" + dtr1["Aff_ID"].ToString() + "</td>"
                               + "<td>" + dtr1["farmer_first_name"].ToString() + " " + dtr1["farmer_last_name"].ToString() + "</td>"
                               + "<td>" + dtr1["Loan_Status"].ToString() + "</td>"
                               + "<td>" + dtr1["Loan_Full_ID"].ToString() + "</td>"
                               + "<td>" + dtr1["Borrower_First_Name"].ToString() + " " + dtr1["Borrower_Last_Name"].ToString() + "</td>"
                               + "<td>" + dtr1["crop_year"].ToString() + "</td>"
                               + "<td>" + dtr1["region_name"].ToString() + "</td>"
                               + "<td>" + dtr1["office_name"].ToString() + "</td>"
                               + "<td>" + dtr1["Active_Ind"].ToString()  + "</td>"

                             //+ "<td>" + dtr1["Total_Commitment"].ToString() + "</td>"
                             //+ "<td>" + dtr1["Loan_Status"].ToString() + "</td>"
                               + "</tr>";

            }

            str_return += "</tbody> "
                                    + " </table> "
                                      + "</div></div></div >";
            sb.Append(str_return);

        }
        else
        {
            str_return = "    No data available...";
        }
        return sb.ToString();
    }





}