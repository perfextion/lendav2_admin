﻿<%@ Page Title="" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="ArmDefault.aspx.cs" Inherits="admin_ArmDefault" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="../js/jquery-json-editor.min.js"></script>

    <script src="https://rawgit.com/abodelot/jquery.json-viewer/master/json-viewer/jquery.json-viewer.js"></script>
    <link href="https://rawgit.com/abodelot/jquery.json-viewer/master/json-viewer/jquery.json-viewer.css" rel="stylesheet" />
    <div style="float: left;">
        <div class="btn btn-primary" id="translate">Translate</div>
    </div>
    <div style="margin-left: 42%;">
        <div class="btn btn-primary" style="background-color: green;" id="btnupdate">Update</div>
    </div>
    <div>
        <div>
            <textarea class="loadJsonViewer" id="jsoninput" cols="50" rows="10" style="height: 450px; float: left; width: 600px">
   </textarea>
        </div>
        <div style="margin-left: 42%; margin-top: 1%;">
            <pre class="loadJsonViewer" id="preloanSetting" style="width: 600px; float: left;" jsondata></pre>
        </div>
    </div>
    <script>
        $(function () {
            var data;
            var strdata;
            var timer;
            var obj = {
                Init: function () {
                    $.ajax({
                        type: "POST",
                        //url: "ArmDefault.aspx/getarmdetails",
                        url: "ArmDefault.aspx/getarmdetails1",
                        contentType: "application/json; charset=utf-8",
                        async: false,
                        dataType: "json",
                        success: function (res) {
                            data = res.d[1]
                            var objdata = JSON.parse(res.d[1]);
                            var objsample = {};
                            objsample.loanSettings = objdata.loanSettings;
                            objsample.userSettings = objdata.userSettings;
                            var strgeneralSettings = JSON.stringify(objsample)
                            document.getElementById('jsoninput').innerHTML = strgeneralSettings;
                            //var formattedJson = data;
                            //var objdata = JSON.parse(data.ARM_Default_Value);
                            $('#preloanSetting').html(data);
                            let data1 = $('#preloanSetting').text();
                            if (data1 != undefined && data1 != "") {
                                $('#preloanSetting').jsonViewer(JSON.parse(data1));
                                $('#preloanSetting').find('.json-toggle').click();
                            }

                            //var objdata = JSON.parse(res.d);
                            //data = objdata[0];
                            //document.getElementById('jsoninput').innerHTML = data.ARM_Default_Value;
                            //var formattedJson = objdata[0].ARM_Default_Value;
                            //var objdata = JSON.parse(data.ARM_Default_Value);
                            //$('#preloanSetting').html(formattedJson);
                            //let data1 = $('#preloanSetting').text();
                            //if (data1 != undefined && data1 != "") {
                            //    $('#preloanSetting').jsonViewer(JSON.parse(data1));
                            //    $('#preloanSetting').find('.json-toggle').click();
                            //}
                        }
                    });
                    $('#translate').on('click', function () {
                        getJson();
                        var objdata = JSON.parse(data);
                        var sample = $('#jsoninput').val()
                        var objsample = JSON.parse(sample);
                        objdata.loanSettings = objsample.loanSettings
                        objdata.userSettings = objsample.userSettings
                        strdata = JSON.stringify(objdata)
                        $('#preloanSetting').html(strdata);
                        let data1 = strdata;
                        if (data1 != undefined && data1 != "") {
                            $('#preloanSetting').jsonViewer(objdata);
                            $('#preloanSetting').find('.json-toggle').click();
                        }
                    });
                    prettyPrint();
                    $('#btnupdate').on('click', function () {
                        if (strdata == "" || strdata == undefined) {
                            return alert("Data is not Translated. Please translate and update")
                        }
                        var armjsondata = strdata;
                        $.ajax({
                            type: "POST",
                            url: "ArmDefault.aspx/UpdateArmDetails",
                            data: JSON.stringify({ armjson: armjsondata }),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (res) {
                                toastr.success("Saved Sucessful");
                                if (timer) { clearTimeout(timer); }
                                timer = setTimeout(function () {
                                    location.reload();
                                }, 2000);
                            },
                            failure: function (response) {
                                var val = response.d;
                                toastr.warning(val);
                            }
                        });
                    });
                },
            }
            obj.Init();
        });
        function getJson() {
            try {
                return JSON.parse($('#jsoninput').val());
            } catch (ex) {
                alert('Wrong JSON Format: ' + ex);
            }
        }

        function prettyPrint() {
            var ugly = document.getElementById('jsoninput').value;
            var obj = JSON.parse(ugly);
            var pretty = JSON.stringify(obj, undefined, 4);
            document.getElementById('jsoninput').value = pretty;
        }
        function loadJsonViewer() {
            $('.loadJsonViewer').each(function (index) {
                let data = $(this).attr('jsondata');
                if (data != undefined && data != "") {
                    $(this).jsonViewer(JSON.parse(data));
                    $(this).find('.json-toggle').click();
                }
            });
        }
    </script>
    <div onload="loadJsonViewer()"></div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" runat="Server">
</asp:Content>

