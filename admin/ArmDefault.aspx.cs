﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_ArmDefault : System.Web.UI.Page
{
    static string dbKey = "gp_conn";
    protected void Page_Load(object sender, EventArgs e)
    {
        ClientScript.RegisterStartupScript(this.GetType(), "UpdateTime", "loadJsonViewer()", true);
    }
    [WebMethod]
    public static string getarmdetails()
    {
        string sql_qry = "SELECT [Default_ID],[ARM_Default_Value],[Default_Value_Type],[Status]FROM [ARM_Defaults]";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);

    }
    [WebMethod]
    public static Dictionary<string, string> getarmdetails1()
    {
        string sql_qry = "SELECT [Default_ID],[ARM_Default_Value] FROM [ARM_Defaults]";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);

        return dt.AsEnumerable().ToDictionary<DataRow, string, string>(row => row[0].ToString(),
                                       row => row[1].ToString());
    }
    [WebMethod]
    public static void UpdateArmDetails(string armjson)
    {
        string sql_qry = "";
        sql_qry = "update ARM_Defaults set ARM_Default_Value='" + armjson + "' where Default_ID=1";
        gen_db_utils.gp_sql_execute(sql_qry, dbKey);
    }
}