﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Services;

public partial class admin_BorrowerEntityType : BasePage
{
    public admin_BorrowerEntityType()
    {
        Table_Name = "Borrower_Entity_Type";
    }

    [WebMethod]
    public static string GetTableInfo()
    {
        Count_Query = @"DECLARE @Count int;
                            Select @Count = Count(*)  from [Ref_List_Item] where list_group_code = 'BORROWER_ENTITY_TYPE'";
        return Get_Table_Info();
    }

    [WebMethod]
    public static string GetBorrowerEntityTypes()
    {
        string sql_qry = @"SELECT *, 0 as [Actionstatus] from [Ref_List_Item] where list_group_code = 'BORROWER_ENTITY_TYPE'";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static void SaveBorrowerEntityType(dynamic ListItems)
    {
        foreach (var item in ListItems)
        {
            string qry = "";
            int val = Convert.ToInt32(item["Actionstatus"]);

            if (val == 1)
            {
                qry = @"INSERT INTO [dbo].[Ref_List_Item]
                               ([List_Item_Value]
                               ,[List_Item_Name]
                               ,[List_Group_Code]
                               ,[Status])
                         VALUES
                               (
                                    '" + item["List_Item_Value"].Replace("'", "''") + @"'" +
                                   ",'" + item["List_Item_Name"].Replace("'", "''") + @"'" +
                                   ",'BORROWER_ENTITY_TYPE'" +
                                   ",'" + item["Status"] + @"'
                                )  ";

                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 2)
            {
                qry = @"UPDATE [dbo].[Ref_List_Item]
                           SET  [List_Item_Value] = '" + item["List_Item_Value"].Replace("'", "''") + @"' " +
                              ",[List_Item_Name] = '" + item["List_Item_Name"].Replace("'", "''") + @"' " +
                              ",[List_Group_Code] = 'BORROWER_ENTITY_TYPE' " +
                              ",[Status] = '" + item["Status"] + @"' " +
                         "WHERE List_Item_ID = '" + item["List_Item_ID"] + "' ";

                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 3)
            {
                qry = "DELETE FROM Ref_List_Item  where List_Item_ID = '" + item["List_Item_ID"] + "' ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
        }

        if ( ListItems != null)
        {
            Update_Table_Audit();
        }
    }
}