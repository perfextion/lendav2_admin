﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Services;

public partial class admin_BorrowerFinancialAssestDiscount : BasePage
{
    public admin_BorrowerFinancialAssestDiscount()
    {
        Table_Name = "Ref_BorrowerFinancialAssetDiscount";
    }

    [WebMethod]
    public static string GetTableInfo()
    {
        Count_Query = @"DECLARE @Count int;
                        Select @Count = Count(*)  from ref_discounts where Disc_Group_Code = 'BRW_ASSET_DISC'";
        return Get_Table_Info();
    }

    [WebMethod]
    public static string GetFinancialDisc()
    {
        string sql_qry = @"select * from ref_discounts where Disc_Group_Code = 'BRW_ASSET_DISC'";

        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static void SaveFinancialDisc(dynamic lst)
    {
        foreach (var item in lst)
        {
            string qry = "";
            int val = Convert.ToInt32(item["Actionstatus"]);

            if (val == 1)
            {
                qry = @"INSERT INTO [dbo].[ref_discounts]
                               ([discount_key]
                               ,[disc_detail]
                               ,[discount_value]
                               ,[CreatedOn]
                               ,[Status]
                               ,[Disc_Group_Code])
                         VALUES
                               (
                                    '" + item["Discount_Key"].Replace("'", "''") + @"'" +
                                   ",'" + item["Disc_Detail"].Replace("'", "''") + @"'" +
                                   ",'" + item["Discount_Value"] + @"'" +
                                   ", GETDATE() " + 
                                   ",'" + item["Discount_Value"] + "'" +
                                   ",'BRW_ASSET_DISC'" + @"
                                )  ";

                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 2)
            {
                qry = @"UPDATE [dbo].[ref_discounts]
                           SET  [Discount_Key] = '" + item["Discount_Key"].Replace("'", "''") + @"' " +
                              ",[Disc_Detail] = '" + item["Disc_Detail"].Replace("'", "''") + @"' " +
                              ",[Discount_Value] = '" + item["Discount_Value"] + @"' " +
                              ",[Status] = '" + item["Status"] + @"' " +
                         "WHERE Discount_Id = '" + item["Discount_Id"] + "' ";

                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 3)
            {
                qry = "DELETE FROM ref_discounts  where Discount_Id = '" + item["Discount_Id"] + "' ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
        }

        gen_db_utils.Update_Table_Audit_Trail("Ref_BorrowerFinancialAssetDiscount", userid, dbKey);
    }
}