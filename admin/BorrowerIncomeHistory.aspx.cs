﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_BorrowerIncomeHistory : BasePage
{
    public admin_BorrowerIncomeHistory()
    {
        Table_Name = "Ref_Borrower_Income_History";
    }

    [WebMethod]
    public static string GetTableInfo()
    {
        return Get_Table_Info();
    }

    [WebMethod]
    public static Dictionary<string, string> GetBorrowerList()
    {
        string sql_qry = @"SELECT 
	                        Master_Borrower_ID, 
	                        dbo.Get_Entity_Name(Borrower_Entity_Type_Code, Borrower_First_Name, Borrower_MI, Borrower_Last_Name) as [Borrower_Name]
                        From Master_Borrower
                        Where Borrower_First_Name IS NOT NULL AND Borrower_First_Name != ''";

        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return dt.AsEnumerable().ToDictionary(row => row[0].ToString(), row => row[1].ToString());
    }

    [WebMethod]
    public static string GetIncomeHistory()
    {
        string sql_qry = @"SELECT 
                                RBIH.*, 
                                dbo.Get_Entity_Name(MB.Borrower_Entity_Type_Code, MB.Borrower_First_Name, MB.Borrower_MI, MB.Borrower_Last_Name) as [Borrower_Name] 
                            From Ref_Borrower_Income_History RBIH
                            JOIN Master_Borrower MB on RBIH.Borrower_ID = MB.Master_Borrower_ID";

        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static void SaveIncomeHistory(dynamic lst)
    {
        foreach (var item in lst)
        {
            string qry = "";
            int val = Convert.ToInt32(item["Actionstatus"]);

            if (val == 1)
            {
                qry = @"INSERT INTO [dbo].[Ref_Borrower_Income_History]
                                   ([Borrower_ID]
                                   ,[Borrower_Year]
                                   ,[Borrower_Revenue]
                                   ,[Borrower_Expense]
                                   ,[Borrower_Income]
                                   ,[Status])
                             VALUES
                               (
                                    '" + item["Borrower_ID"] + @"'" +
                                   ",'" + item["Borrower_Year"] + @"'" +
                                   ",'" + item["Borrower_Revenue"] + @"'" +
                                   ",'" + item["Borrower_Expense"] + "'" +
                                   ",'" + item["Borrower_Income"] + "'" +
                                   ",'1'" +
                                ")  ";

                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 2)
            {
                qry = @"UPDATE [dbo].[Ref_Borrower_Income_History]
                           SET  [Borrower_ID] = '" + item["Borrower_ID"] + @"' " +
                              ",[Borrower_Year] = '" + item["Borrower_Year"] + @"' " +
                              ",[Borrower_Revenue] = '" + item["Borrower_Revenue"] + @"' " +
                              ",[Borrower_Expense] = '" + item["Borrower_Expense"] + @"' " +
                              ",[Borrower_Income] = '" + item["Borrower_Income"] + @"' " +
                              ",[Status] = '" + item["Status"] + @"' " +
                         "WHERE Borrower_Income_History_ID = '" + item["Borrower_Income_History_ID"] + "' ";

                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 3)
            {
                qry = "DELETE FROM Ref_Borrower_Income_History  where Borrower_Income_History_ID = '" + item["Borrower_Income_History_ID"] + "' ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
        }

        Update_Table_Audit();
    }
}