﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_BorrowerNameDetail : BasePage
{
    public admin_BorrowerNameDetail()
    {
        Table_Name = "Ref_BorrowerNameDetail";
    }

    [WebMethod]
    public static string GetTableInfo()
    {
        Count_Query = @"DECLARE @Count int;
                            Select @Count = Count(*)  From Borrower_Signer;";
        return Get_Table_Info();
    }

    [WebMethod]
    public static Dictionary<string, string> GetBorrowerList()
    {
        string sql_qry = @"SELECT 
	                        Master_Borrower_ID, 
	                        dbo.Get_Entity_Name(Borrower_Entity_Type_Code, Borrower_First_Name, Borrower_MI, Borrower_Last_Name) as [Borrower_Name]
                        From Master_Borrower
                        Where Borrower_First_Name IS NOT NULL AND Borrower_First_Name != ''";

        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return dt.AsEnumerable().ToDictionary(row => row[0].ToString(), row => row[1].ToString());
    }

    [WebMethod]
    public static Dictionary<string, string> GetPreferredContactTypes()
    {
        string sql_qry = @"select List_Item_Value, List_Item_Name from Ref_List_Item Where List_Group_Code = 'PREF_CONTACT'";

        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return dt.AsEnumerable().ToDictionary(row => row[0].ToString(), row => row[1].ToString());
    }

    [WebMethod]
    public static string GetBorrowerNameDetailList()
    {
        string sql_qry = @"SELECT 
                                BS.*, 
                                dbo.Get_Entity_Name(MB.Borrower_Entity_Type_Code, MB.Borrower_First_Name, MB.Borrower_MI, MB.Borrower_Last_Name) as [Borrower_Name] 
							FROM Borrower_Signer BS
                            JOIN Master_Borrower MB on BS.Borrower_ID = MB.Master_Borrower_ID
                            Where BS.[Status] = 1";

        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static void SaveBorrowerNameDetailList(dynamic lst)
    {
        foreach (var item in lst)
        {
            string qry = "";
            int val = Convert.ToInt32(item["Actionstatus"]);

            if (val == 1)
            {
                qry = @"INSERT INTO [dbo].[Borrower_Signer]
                                   ([Borrower_ID]
                                   ,[Loan_Full_ID]
                                   ,[Signer_Name]
                                   ,[Location]
                                   ,[Phone]
                                   ,[Email]
                                   ,[Preferred_Contact_Ind]
                                   ,[Title]
                                   ,[Percent_Owned]
                                   ,[SBI_Ind]
                                   ,[Signer_Ind]
                                   ,[Guarantor_Ind]
                                   ,[Status])
                             VALUES
                               (
                                    '" + item["Borrower_ID"] + @"'" +
                                   ",'" + item["Loan_Full_ID"].Replace("'", "''") + @"'" +
                                   ",'" + item["Signer_Name"].Replace("'", "''") + @"'" +
                                   ",'" + item["Location"].Replace("'", "''") + @"'" +
                                   ",'" + item["Phone"].Replace("'", "''") + @"'" +
                                   ",'" + item["Email"].Replace("'", "''") + @"'" +
                                   ",'" + item["Preferred_Contact_Ind"] + @"'" +
                                   ",'" + item["Title"].Replace("'", "''") + @"'" +
                                   ",'" + item["Percent_Owned"] + @"'" +
                                   ",'" + item["SBI_Ind"] + @"'" +
                                   ",'" + item["Signer_Ind"] + "'" +
                                   ",'" + item["Guarantor_Ind"] + "'" +
                                   ",'1'" +
                                ")  ";

                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 2)
            {
                qry = @"UPDATE [dbo].[Borrower_Signer]
                           SET  [Borrower_ID] = '" + item["Borrower_ID"] + @"' " +
                              ",[Loan_Full_ID] = '" + item["Loan_Full_ID"] + @"' " +
                              ",[Signer_Name] = '" + item["Signer_Name"] + @"' " +
                              ",[Location] = '" + item["Location"] + @"' " +
                              ",[Phone] = '" + item["Phone"] + @"' " +
                              ",[Email] = '" + item["Email"] + @"' " +
                              ",[Preferred_Contact_Ind] = '" + item["Preferred_Contact_Ind"] + @"' " +
                              ",[Title] = '" + item["Title"] + @"' " +
                              ",[Percent_Owned] = '" + item["Percent_Owned"] + @"' " +
                              ",[SBI_Ind] = '" + item["SBI_Ind"] + @"' " +
                              ",[Signer_Ind] = '" + item["Signer_Ind"] + @"' " +
                              ",[Guarantor_Ind] = '" + item["Guarantor_Ind"] + @"' " +
                              ",[Status] = '" + item["Status"] + @"' " +
                         "WHERE Signer_ID = '" + item["Signer_ID"] + "' ";

                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 3)
            {
                qry = "DELETE FROM Borrower_Signer  where Signer_ID = '" + item["Signer_ID"] + "' ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
        }

        Update_Table_Audit();
    }
}