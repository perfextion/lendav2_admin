﻿<%@ Page Title="" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="Budget3DtableByLocations.aspx.cs" Inherits="admin_Budget3DtableByLocations" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <style>
        .ui-jqgrid .ui-jqgrid-htable th div {
            height: auto;
            overflow: hidden;
            padding-right: 4px;
            padding-top: 2px;
            position: relative;
            vertical-align: text-top;
            white-space: normal !important;
        }

        .fc-head-container thead tr, .table thead tr {
            height: 45px;
        }
    </style>
    <div class="">
        <div style="width: 1500px">
            <div style="width: 11%; margin-left: 27px; float: left;">
                <div style="margin-top: 5%; margin-bottom: 1%;">
                    <%--<div id="btnAddRow" class="btn btn-info" style="width: 70px;">
                        add
                    </div>--%>
                    <div id="btnSave" class="btn btn-success" style="width: 70px;">Save</div>
                    <div id="btnreload" class="btn btn-primary fa fa-refresh" title="Reload" style="width: 80px;"></div>
                </div>
            </div>
            <div style="width: 62%; float: left;">
                <div class="form-inline" style="margin-top: 1%; margin-left: 3%; margin-bottom: 2%;">
                    <label for="ddlRegions">Regions :</label>
                    <select id="ddlRegions" class="form-control" style="width: 200px">
                        <%--<option value="4">All Locations</option>--%>
                        <option value="0">ARM</option>
                    </select>
                    <label for="ddlofficename">Office Names :</label>
                    <select id="ddlofficename" class="form-control" style="width: 200px">
                        <%--<option value="">Select</option>--%>
                        <option value="0">0:ARM Default</option>
                    </select>
                    <div style="display: none">
                        <label for="ddlCropkeys">Crop Practice ID :</label>
                        <select id="ddlCropkeys" class="form-control" style="width: 200px">
                            <%--<option value="0">Select</option>--%>
                        </select>
                    </div>
                    <label for="txtSearch">Search :</label>
                    <input type="text" class="form-control" name="search" id="txtSearch" style="width: 200px" />
                </div>
            </div>
        </div>
        <div class="loader" style="margin-top: 1%; margin-left: 12%; margin-bottom: 0%"></div>
        <div class="col-md-12" style="margin-bottom: 7%;">
            <div class="row">
                <div style="margin-left: 24px; overflow-x: auto; overflow-y: auto;">
                    <table id="jqGrid"></table>
                    <div id="jqGridPager"></div>
                </div>
            </div>
        </div>
        <br />
        <br />
        <br />
    </div>
    <asp:HiddenField ID="Budget3dOffice_id" runat="server" />
    <asp:HiddenField ID="Budget3dRegion_id" runat="server" />
    <script type="text/javascript">  

        var DeleteRows = [];
        var colmodel1 = [];
        var timer;
        var colid;
        var rowidno;
        var lastSelection;
        var execute = false;
        $(function () {
            $.urlParam = function (name) {
                var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
                if (results == null) {
                    return null;
                }
                return decodeURI(results[1]) || 0;
            }
            var obj = {
                Init: function () {
                    //var Office = $.urlParam('OfficeId');
                    obj.officename();
                    obj.getcropkeys();
                    obj.getregions();
                    obj.loadgridcols();
                    //if (Office || 0 > 0 && Office.length > 0) {
                    //    var Result = Office.split('!');
                    //    var officename = Result[0];
                    //    var region = Result[1];
                    //    if (officename != undefined || "") {
                    //        $('#ddlofficename').val(officename);
                    //    }
                    //    if (region != undefined || "") {
                    //        $('#ddlRegions').val(region);
                    //    }
                    //}
                    if ($('#ContentPlaceHolder1_Budget3dOffice_id').val() != '') {
                        $('#ddlofficename').val($('#ContentPlaceHolder1_Budget3dOffice_id').val());
                    }
                    if ($('#ContentPlaceHolder1_Budget3dRegion_id').val() != '') {
                        $('#ddlRegions').val($('#ContentPlaceHolder1_Budget3dRegion_id').val());
                    }
                    obj.bindGrid();
                    $("#btnSave").click(function () {
                        obj.save();
                    });
                    $("#btnreload").click(function () {
                        obj.Reload();
                    });
                    $("#txtSearch").on("keyup", function () {
                        var self = this;
                        obj.search(self);
                    });
                    $('a').on("click", function (ev) {
                        var grid = $("#jqGrid");
                        var id = $('.inline-edit-cell').parent().parent().prop('id');
                        grid.jqGrid('saveRow', id);
                        var rowData = grid.jqGrid('getRowData', id);
                        if (rowData.Budget_Default_ID > 0) {
                            rowData.Actionstatus = 2;
                            grid.jqGrid('getGridParam', 'data')[id] = rowData;
                        }
                        var dataobj = grid.jqGrid('getGridParam', 'data');
                        var newRows = $.grep(dataobj, function (e) {
                            if (e != undefined) {
                                return e.Actionstatus == 2
                            }
                        });
                        if (newRows.length > 0) {
                            var result = confirm("Are you sure you Want to leave without Saving ?");
                            if (result == false) {
                                ev.preventDefault();
                            }
                        }

                    });
                    var previous;
                    $("#ddlofficename").on('focus', function () {
                        previous = this.value;
                    }).change(function () {
                        lastSelection = undefined;
                        var grid = $("#jqGrid");
                        var id = $('.inline-edit-cell').parent().parent().prop('id');
                        grid.jqGrid('saveRow', id);
                        var rowData = grid.jqGrid('getRowData', id);
                        if (rowData.Budget_Default_ID > 0) {
                            rowData.Actionstatus = 2;
                            grid.jqGrid('getGridParam', 'data')[id] = rowData;
                        }
                        var dataobj = grid.jqGrid('getGridParam', 'data');
                        var newRows = $.grep(dataobj, function (e) {
                            if (e != undefined) {
                                return e.Actionstatus == 2
                            }
                        });

                        if (newRows.length > 0) {
                            var result = confirm("Are you sure you Want to leave without Saving ?");
                            if (result == true) {
                                $('.loader').show();
                                obj.bindGrid();
                            } else {
                                $("#ddlofficename").val(previous)
                            }
                        } else {
                            $('.loader').show();
                            obj.bindGrid();
                        }
                    });
                    $("#ddlRegions").on('focus', function () {
                        previous = this.value;
                    }).change(function () {
                        var regionid = $('#ddlRegions').val();
                        lastSelection = undefined;
                        var grid = $("#jqGrid");
                        var id = $('.inline-edit-cell').parent().parent().prop('id');
                        grid.jqGrid('saveRow', id);
                        var rowData = grid.jqGrid('getRowData', id);
                        if (rowData.Budget_Default_ID > 0) {
                            rowData.Actionstatus = 2;
                            grid.jqGrid('getGridParam', 'data')[id] = rowData;
                        }
                        var dataobj = grid.jqGrid('getGridParam', 'data');
                        var newRows = $.grep(dataobj, function (e) {
                            if (e != undefined) {
                                return e.Actionstatus == 2
                            }
                        });

                        if (newRows.length > 0) {
                            var result = confirm("Are you sure you Want to leave without Saving ?");
                            if (result == true) {
                                $('.loader').show();
                                obj.officename();
                                obj.bindGrid();
                            } else {
                                $("#ddlRegions").val(previous)
                            }
                        } else {
                            $('.loader').show();
                            obj.officename();
                            obj.bindGrid();
                        }

                    });
                },
                bindGrid: function () {
                    var Regionid = $('#ddlRegions').val();
                    var officename = $('#ddlofficename').val();
                    $.ajax({
                        type: "POST",
                        url: "Budget3DtableByLocations.aspx/GetReferenceBudgetViewArms",
                        data: "{Office_id:'" + officename + "',Region_id:'" + Regionid + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var resData = JSON.parse(data.d);
                            $('.loader').show();
                            for (var i = 0; i < resData.length; i++) {
                                resData[i].rowid = i + 1;
                            }
                            obj.loadgrid(resData);
                            $('.loader').hide();
                        }
                    });
                },
                loadgrid: function (data) {
                    $.jgrid.gridUnload("#jqGrid");
                    $("#jqGrid").jqGrid({
                        datatype: "local",
                        styleUI: 'Bootstrap',
                        data: data,
                        colModel: colmodel1,
                        //    [
                        //    { label: 'rowid', name: 'rowid', width: 75, editable: false, key: true, frozen: true, hidden: true },
                        //    { label: 'Sort Order', name: 'Sort_order', align: 'center', sorttype: 'number', width: 70, frozen: true, editable: false },
                        //    { label: 'Expense ID', name: 'Budget_Expense_Type_ID', align: 'center', sorttype: 'number', frozen: true, hidden: true, width: 70, editable: false },
                        //    { label: 'Expense Name', name: 'Budget_Expense_Name', width: 170, editable: false, frozen: true },
                        //    { label: 'Office Id', name: 'Z_Office_ID', width: 70, align: 'center', hidden: true, editable: false },
                        //    { label: 'Region Id', name: 'Z_Region_ID', width: 70, align: 'center', hidden: true, editable: false },

                        //    { label: 'COT_0_IRR_IRR', name: 'COT_0_IRR_IRR', width: 110, editable: true, align: 'center', editoptions: { maxlength: 20, dataEvents: [{ type: 'keypress', fn: function (event) { return isNumber(event, this) } }] } },
                        //    { label: 'COT_0_NI_NI', name: 'COT_0_NI_NI', width: 100, editable: true, align: 'center', editoptions: { maxlength: 20, dataEvents: [{ type: 'keypress', fn: function (event) { return isNumber(event, this) } }] } },
                        //    { label: 'COT_0_NIR_NIR', name: 'COT_0_NIR_NIR', width: 110, editable: true, align: 'center', editoptions: { maxlength: 20, dataEvents: [{ type: 'keypress', fn: function (event) { return isNumber(event, this) } }] } },
                        //    { label: 'CRN_0_IRR_IRR', name: 'CRN_0_IRR_IRR', width: 110, editable: true, align: 'center', editoptions: { maxlength: 20, dataEvents: [{ type: 'keypress', fn: function (event) { return isNumber(event, this) } }] } },
                        //    { label: 'CRN_0_IRR_ORG', name: 'CRN_0_IRR_ORG', width: 110, editable: true, align: 'center', editoptions: { maxlength: 20, dataEvents: [{ type: 'keypress', fn: function (event) { return isNumber(event, this) } }] } },
                        //    { label: 'CRN_0_NI_NI', name: 'CRN_0_NI_NI', width: 100, editable: true, align: 'center', editoptions: { maxlength: 20, dataEvents: [{ type: 'keypress', fn: function (event) { return isNumber(event, this) } }] } },
                        //    { label: 'CRN_0_NI_ORG', name: 'CRN_0_NI_ORG', width: 110, editable: true, align: 'center', editoptions: { maxlength: 20, dataEvents: [{ type: 'keypress', fn: function (event) { return isNumber(event, this) } }] } },
                        //    { label: 'CRN_0_NIR', name: 'CRN_0_NIR', width: 100, editable: true, align: 'center', editoptions: { maxlength: 20, dataEvents: [{ type: 'keypress', fn: function (event) { return isNumber(event, this) } }] } },
                        //    { label: 'CRN_CNV_IRR_IRR', name: 'CRN_CNV_IRR_IRR', width: 110, editable: true, align: 'center', editoptions: { maxlength: 20, dataEvents: [{ type: 'keypress', fn: function (event) { return isNumber(event, this) } }] } },
                        //    { label: 'CRN_CNV_NI_NI', name: 'CRN_CNV_NI_NI', width: 110, editable: true, align: 'center', editoptions: { maxlength: 20, dataEvents: [{ type: 'keypress', fn: function (event) { return isNumber(event, this) } }] } },
                        //    { label: 'PNT_0_IRR_IRR', name: 'PNT_0_IRR_IRR', width: 110, editable: true, align: 'center', editoptions: { maxlength: 20, dataEvents: [{ type: 'keypress', fn: function (event) { return isNumber(event, this) } }] } },
                        //    { label: 'PNT_0_NI_NI', name: 'PNT_0_NI_NI', width: 100, editable: true, align: 'center', editoptions: { maxlength: 20, dataEvents: [{ type: 'keypress', fn: function (event) { return isNumber(event, this) } }] } },
                        //    { label: 'RIC_0_IRR', name: 'RIC_0_IRR', width: 100, editable: true, align: 'center', editoptions: { maxlength: 20, dataEvents: [{ type: 'keypress', fn: function (event) { return isNumber(event, this) } }] } },
                        //    { label: 'RIC_0_NIR', name: 'RIC_0_NIR', width: 100, editable: true, align: 'center', editoptions: { maxlength: 20, dataEvents: [{ type: 'keypress', fn: function (event) { return isNumber(event, this) } }] } },
                        //    { label: 'SOY', name: 'SOY', width: 80, editable: true, align: 'center', editoptions: { maxlength: 20, dataEvents: [{ type: 'keypress', fn: function (event) { return isNumber(event, this) } }] } },
                        //    { label: 'SOY_0_IRR', name: 'SOY_0_IRR', width: 90, editable: true, align: 'center', editoptions: { maxlength: 20, dataEvents: [{ type: 'keypress', fn: function (event) { return isNumber(event, this) } }] } },
                        //    { label: 'SOY_0_IRR_IRR', name: 'SOY_0_IRR_IRR', width: 110, editable: true, align: 'center', editoptions: { maxlength: 20, dataEvents: [{ type: 'keypress', fn: function (event) { return isNumber(event, this) } }] } },
                        //    { label: 'SOY_0_NI_NI', name: 'SOY_0_NI_NI', width: 100, editable: true, align: 'center', editoptions: { maxlength: 20, dataEvents: [{ type: 'keypress', fn: function (event) { return isNumber(event, this) } }] } },
                        //    { label: 'SRG_0_IRR', name: 'SRG_0_IRR', width: 90, editable: true, align: 'center', editoptions: { maxlength: 20, dataEvents: [{ type: 'keypress', fn: function (event) { return isNumber(event, this) } }] } },
                        //    { label: 'SRG_0_IRR_IRR', name: 'SRG_0_IRR_IRR', width: 110, editable: true, align: 'center', editoptions: { maxlength: 20, dataEvents: [{ type: 'keypress', fn: function (event) { return isNumber(event, this) } }] } },
                        //    { label: 'SRG_0_NIR', name: 'SRG_0_NIR', width: 100, editable: true, align: 'center', editoptions: { maxlength: 20, dataEvents: [{ type: 'keypress', fn: function (event) { return isNumber(event, this) } }] } },
                        //    { label: 'SWP_0_IRR_IRR', name: 'SWP_0_IRR_IRR', width: 110, editable: true, align: 'center', editoptions: { maxlength: 20, dataEvents: [{ type: 'keypress', fn: function (event) { return isNumber(event, this) } }] } },
                        //    { label: 'SWP_0_NI_NI', name: 'SWP_0_NI_NI', width: 100, editable: true, align: 'center', editoptions: { maxlength: 20, dataEvents: [{ type: 'keypress', fn: function (event) { return isNumber(event, this) } }] } },
                        //    { label: 'TBC_0_IRR_IRR', name: 'TBC_0_IRR_IRR', width: 110, editable: true, align: 'center', editoptions: { maxlength: 20, dataEvents: [{ type: 'keypress', fn: function (event) { return isNumber(event, this) } }] } },
                        //    { label: 'TBC_0_NI_NI', name: 'TBC_0_NI_NI', width: 100, editable: true, align: 'center', editoptions: { maxlength: 20, dataEvents: [{ type: 'keypress', fn: function (event) { return isNumber(event, this) } }] } },
                        //    { label: 'WHT_0_IRR', name: 'WHT_0_IRR', width: 90, editable: true, align: 'center', editoptions: { maxlength: 20, dataEvents: [{ type: 'keypress', fn: function (event) { return isNumber(event, this) } }] } },
                        //    { label: 'WHT_0_NIR', name: 'WHT_0_NIR', width: 90, editable: true, align: 'center', editoptions: { maxlength: 20, dataEvents: [{ type: 'keypress', fn: function (event) { return isNumber(event, this) } }] } },
                        //    { label: 'XYZ_0_IRR_IRR', name: 'XYZ_0_IRR_IRR', width: 110, editable: true, align: 'center', editoptions: { maxlength: 20, dataEvents: [{ type: 'keypress', fn: function (event) { return isNumber(event, this) } }] } },
                        //   { label: 'Actionstatus', name: 'Actionstatus', width: 70, editable: false, hidden: true },
                        //],
                        viewrecords: true,
                        loadonce: true,
                        restoreAfterSelect: false,
                        saveAfterSelect: true,
                        sortable: true,
                        onSelectRow: obj.edit,
                        onCellSelect: function (row, col, content, event) {
                            var cm = jQuery("#jqGrid").jqGrid("getGridParam", "colModel");
                            colid = cm[col].name;
                            rowidno = row;
                        },
                        onPaging: obj.changePage,
                        height: 'auto',
                        rowNum: 100,
                        width: 1450,
                        pager: "#jqGridPager",
                        shrinkToFit: false,
                        gridComplete: function () {

                            var txtid = rowidno + '_' + colid;
                            $('#' + txtid + '').focus();
                        }
                    });
                    $("#jqGrid").jqGrid("setFrozenColumns");
                },
                loadgridcols: function () {
                    var colvalue = "";
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "Budget3DtableByLocations.aspx/GetCropFullKeys",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            colmodel1.push({ label: 'rowid', name: 'rowid', width: 75, editable: false, key: true, frozen: true, hidden: true }, );
                            colmodel1.push({ label: 'Sort Order', name: 'Sort_order', align: 'center', sorttype: 'number', width: 70, frozen: true, editable: false }, );
                            colmodel1.push({ label: 'Expense ID', name: 'Budget_Expense_Type_ID', align: 'center', sorttype: 'number', frozen: true, hidden: true, width: 70, editable: false }, );
                            colmodel1.push({ label: 'Expense Name', name: 'Budget_Expense_Name', width: 170, editable: false, frozen: true }, );
                            colmodel1.push({ label: 'Office Id', name: 'Z_Office_ID', width: 70, align: 'center', hidden: true, editable: false }, );
                            colmodel1.push({ label: 'Region Id', name: 'Z_Region_ID', width: 70, align: 'center', hidden: true, editable: false }, );
                            $.each(res.d, function (data, value) {
                                colmodel1.push({ label: data, name: value, width: 110, editable: true, align: 'center', editoptions: { maxlength: 20, dataEvents: [{ type: 'keypress', fn: function (event) { return isNumber(event, this) } }] } }, );
                            });
                            colmodel1.push({ label: 'Actionstatus', name: 'Actionstatus', width: 70, editable: false, hidden: true });
                        }
                    });
                },
                getcropkeys: function () {
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "Budget3DtableByLocations.aspx/GetCropFullKeys",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            $.each(res.d, function (data, value) {
                                $("#ddlCropkeys").append($("<option></option>").val(data).html(value));
                            });
                        }
                    })
                },
                getregions: function () {
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "Budget3DtableByLocations.aspx/GetRegions",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            $.each(res.d, function (data, value) {
                                $("#ddlRegions").append($("<option></option>").val(data).html(value));
                            });
                        }
                    })
                },
                officename: function () {
                    var Regionid = $('#ddlRegions').val();
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "Budget3DtableByLocations.aspx/GetAllofficenames",
                        data: JSON.stringify({ Region_Id: Regionid }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            $("#ddlofficename").html('');
                            $("#ddlofficename").append($("<option></option>").val(0).html("0:ARM Default"));
                            $.each(res.d, function (data, value) {
                                $("#ddlofficename").append($("<option></option>").val(data).html(value));
                            });
                        }

                    });
                },
                add: function () {

                    var grid = $("#jqGrid");
                    var rowsperPage = parseInt(grid.getGridParam('rowNum'));
                    var gridlength = grid.jqGrid('getGridParam', 'data').length;
                    var curpage = parseInt($(".ui-pg-input").val());
                    var totPages = Math.ceil(gridlength / rowsperPage);


                    if (rowsperPage * totPages == gridlength) {
                        var id = $('.inline-edit-cell').parent().parent().prop('id');
                        grid.jqGrid('saveRow', id);
                        var row = obj.newrow();

                        var newRowId = row.rowid;
                        grid.jqGrid('addRowData', newRowId, row);
                        grid.trigger('reloadGrid');
                        lastSelection = newRowId;
                        grid.jqGrid('saveRow', lastSelection);
                        grid.jqGrid('restoreRow', lastSelection);
                        $('.glyphicon-step-forward').trigger('click');
                    } else {
                        $('.glyphicon-step-forward').trigger('click');
                        var row = obj.newrow();

                        var newRowId = row.rowid;
                        grid.jqGrid('addRowData', newRowId, row);
                    }


                    lastSelection = newRowId;
                    grid.jqGrid('saveRow', lastSelection);
                    grid.jqGrid('restoreRow', lastSelection);

                    var eid = $('.inline-edit-cell').parent().parent().prop('id')
                    grid.jqGrid('saveRow', eid);
                    grid.jqGrid('restoreRow', eid);
                    grid.jqGrid('editRow', newRowId);
                },
                edit: function (id) {
                    execute = true;
                    if (id && id !== lastSelection) {
                        var grid = $("#jqGrid");
                        grid.jqGrid('saveRow', lastSelection);
                        grid.jqGrid('restoreRow', lastSelection);
                        var row = grid.jqGrid('getRowData', lastSelection);

                        if (!jQuery.isEmptyObject(row)) {
                            var dataobj = jQuery("#jqGrid").jqGrid('getGridParam', 'data');
                            index = dataobj.findIndex(x => x.rowid == row.rowid);
                            if (row.rowid > 0 && row.Actionstatus != 1) {
                                row.Actionstatus = 2;
                                grid.jqGrid('getGridParam', 'data')[index] = row;
                            }
                        }
                        grid.jqGrid('editRow', id);
                        lastSelection = id;
                        obj.focus();
                    }
                },
                focus: function () {
                    setTimeout(function () {
                        var txtid = rowidno + '_' + colid;
                        $('#' + txtid + '').focus();
                    }, 1);

                },
                newrow: function () {
                    var newid = 0;

                    var grid = $("#jqGrid");
                    var length = jQuery("#jqGrid").jqGrid('getGridParam', 'records');
                    var dataobj = grid.jqGrid('getGridParam', 'data');
                    var dataobj = grid.jqGrid('getGridParam', 'data');
                    var id = [];
                    ids = dataobj.map(function (e) { return e.rowid });
                    if (ids.length > 0) {
                        newid = ids.reduce(function (a, b) { return Math.max(a, b); });
                    }
                    var row = {
                        Sort_order: 0,
                        Actionstatus: 1,
                        rowid: newid + 1
                    };
                    return row;
                },
                save: function () {
                    $('.loader').show();
                    var grid = $("#jqGrid");
                    var allrows = [];
                    var id = $('.inline-edit-cell').parent().parent().prop('id');
                    grid.jqGrid('saveRow', id);
                    var rowData = grid.jqGrid('getRowData', id);
                    if (rowData.rowid > 0) {
                        rowData.Actionstatus = 2;
                        grid.jqGrid('getGridParam', 'data')[id] = rowData;
                    }

                    for (var i = 0; i < DeleteRows.length; i++) {
                        DeleteRows[i].Actionstatus = 3
                        allrows.push(DeleteRows[i]);
                    }
                    var dataobj = grid.jqGrid('getGridParam', 'data');
                    var newRows = $.grep(dataobj, function (e) {
                        if (e != undefined) {
                            return e.Actionstatus == 1 || e.Actionstatus == 2
                        }
                    });
                    for (var i = 0; i < newRows.length; i++) {
                        allrows.push(newRows[i]);
                    }
                    //var ddlName = $('#ddlCropkeys :selected').text();
                    //var officename = $('#ddlofficename').val();
                    //var Regionid = $('#ddlRegions').val();
                    $.ajax({
                        type: "POST",
                        url: "Budget3DtableByLocations.aspx/SaveReferenceBudgetViewArms",
                        data: JSON.stringify({ lst: allrows }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            toastr.success("Saved Sucessful");
                            if (timer) { clearTimeout(timer); }
                            timer = setTimeout(function () {
                                window.location.href = "Budget3DtableByLocations.aspx";
                            }, 2000);
                        },
                        failure: function (response) {
                            var val = console.log(response.d);
                            toastr.warning(val);
                        }
                    });
                },
                Reload: function () {
                    $('.loader').show();
                    var officename = $('#ddlofficename').val();
                    var Regionid = $('#ddlRegions').val();
                    window.location.href = "Budget3DtableByLocations.aspx?OfficeId=" + officename + "!" + Regionid;
                    $('.loader').hide();
                },
                search: function (self) {
                    if (timer) { clearTimeout(timer); }
                    timer = setTimeout(function () {
                        obj.changePage();
                        $("#jqGrid").jqGrid('filterInput', self.value);
                    }, 0);
                },
                changePage: function () {

                    lastSelection = "";
                    var grid = $("#jqGrid");
                    var id = $('.inline-edit-cell').parent().parent().prop('id');
                    if (id != undefined) {
                        grid.jqGrid('saveRow', id);
                        grid.jqGrid('restoreRow', id);
                        var row = grid.jqGrid('getRowData', id);
                        if (!jQuery.isEmptyObject(row)) {
                            var dataobj = jQuery("#jqGrid").jqGrid('getGridParam', 'data');
                            index = dataobj.findIndex(x => x.rowid == row.rowid);
                            if (row.rowid > 0 && row.Actionstatus != 1) {
                                row.Actionstatus = 2;
                                grid.jqGrid('getGridParam', 'data')[index] = row;
                            }
                        }
                    }
                },
                deleteLink: function (cellValue, options, rowdata, action) {
                    return "<a href='javascript:deleteRecord(" + options.rowId + ")' class='glyphicon glyphicon-trash' style='color:red'></a>";
                }
            }
            obj.Init();

        });
        function isNumber(evt, element) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (
                (charCode != 45 || $(element).val().indexOf('-') != -1) &&      // “-” CHECK MINUS, AND ONLY ONE.
                (charCode != 46 || $(element).val().indexOf('.') != -1) &&      // “.” CHECK DOT, AND ONLY ONE.
                (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
        function editRow(rowId) {

            var grid = $('#jqGrid');
            grid.editRow(rowId, true, function () {
                var colModel = grid.getGridParam('colMode');
                var colName = colModel[colIndex].name;
                var input = $('#' + rowId + '_' + colName);
                input.get(0).focus();
            });
        }
        function deleteRecord(id) {

            var grid = $('#jqGrid');
            var result = confirm("Are you sure you Want to delete?");

            if (result == true) {
                var ediId = $('.inline-edit-cell').parent().parent().prop('id');
                grid.jqGrid('saveRow', ediId);
                var rowData = grid.jqGrid('getRowData', ediId);
                if (rowData.rowid > 0 && rowData.Actionstatus != 1) {
                    rowData.Actionstatus = 2;
                    grid.jqGrid('getGridParam', 'data')[ediId - 1] = rowData;
                }

                var row = grid.jqGrid('getRowData', id);
                grid.jqGrid('delRowData', id);
                if (row.Actionstatus != 1) {
                    DeleteRows.push(row);
                }
                var data = jQuery("#jqGrid").jqGrid('getGridParam', 'data');
                for (var i = 0; i < data.length; i++) {
                    if (data[i].Sort_order == 0) {
                        data[i].rowid = i + 1;
                        data[i].id = i + 1;
                    } else
                        data[i].rowid = i + 1;
                }
                var curpage = parseInt($(".ui-pg-input").val());
                jQuery('#jqGrid').jqGrid('clearGridData');
                jQuery('#jqGrid').jqGrid('setGridParam', { data: data });
                $("#jqGrid").trigger("reloadGrid", [{ page: curpage }]);
                $("#jqGrid").trigger("reloadGrid", [{ page: curpage }]);
                lastSelection = id;
            }
        }
        function unique(list) {
            var result = [];
            $.each(list, function (i, e) {
                if ($.inArray(e, result) == -1) result.push(e);
            });
            return result;
        }
    </script>



</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" runat="Server">
</asp:Content>

