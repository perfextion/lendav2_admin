﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_Budget3DtableByLocations : System.Web.UI.Page
{
    static string dbKey = "gp_conn";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Budget3dOffice_id"] != null)
        {
            Budget3dOffice_id.Value = Session["Budget3dOffice_id"].ToString();
        }
        if (Session["Budget3dRegion_id"] != null)
        {
            Budget3dRegion_id.Value = Session["Budget3dRegion_id"].ToString();
        }
    }
    [WebMethod]
    public static string GetReferenceBudgetViewArms(string Office_id, string Region_id)
    {
        HttpContext.Current.Session["Budget3dOffice_id"] = Office_id;
        HttpContext.Current.Session["Budget3dRegion_id"] = Region_id;

        string sql_qry = @"DECLARE @cols AS NVARCHAR(MAX),
    @query  AS NVARCHAR(MAX)

select @cols = STUFF((SELECT distinct ',' + QUOTENAME(Crop_Full_Key) 
                    from Ref_Budget_Default 
                   -- group by Crop_Full_Key
                    --order by Office_ID
            FOR XML PATH(''), TYPE
            ).value('.', 'NVARCHAR(MAX)') 
        ,1,1,'')	
set @query = 'SELECT 0 as Actionstatus,Sort_order,Budget_Expense_Type_ID,Z_Office_ID,Z_Region_ID,Budget_Expense_Name,' + @cols + ' from 
             (
               select [Sort_order], RBE.[Budget_Expense_Type_ID],[Crop_Full_Key],[Budget_Expense_Name],z_region_id,Z_Office_ID,[ARM_Budget]
                  from[Ref_Budget_Default] RBD join[Ref_Budget_Expense_Type] RBE on RBE.[Budget_Expense_Type_ID] = RBD.[Budget_Expense_Type_ID]
                  where  [Standard_Ind] = ''1'' and z_office_id = " + Office_id + @" and Z_Region_ID =" + Region_id + @"
            ) x 
            pivot 
            (
                sum(ARM_Budget)
                for Crop_Full_Key in (' + @cols + ')
            ) p order by sort_order;'
execute(@query);";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
    }
    [WebMethod]
    public static Dictionary<string, string> GetCropFullKeys()
    {
        string sql_qry = "select  Distinct [Crop_Full_Key] from Ref_Budget_Default";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        //return DataTableToJsonstring(dt);
        return dt.AsEnumerable().ToDictionary<DataRow, string, string>(row => row[0].ToString(),
                                       row => row[0].ToString());

    }
    [WebMethod]
    public static Dictionary<string, string> GetAllofficenames(string Region_Id)
    {
        string sql_qry = "select Office_ID,CONCAT( Office_ID,'.',Office_Name) as Office_Name from Ref_Office ";
        if (Region_Id != "0")
        {
            sql_qry += " where Region_ID=" + Region_Id + " ";
        }
        sql_qry += " order by Office_ID";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return dt.AsEnumerable().ToDictionary<DataRow, string, string>(row => row[0].ToString(),
                                       row => row[1].ToString());
    }
    //[WebMethod]
    //public static Dictionary<string, string> GetCropFullKeys()
    //{
    //    string sql_qry = "select  Distinct [Crop_Full_Key] from Ref_Budget_Default";
    //    DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
    //    //return DataTableToJsonstring(dt);
    //    return dt.AsEnumerable().ToDictionary<DataRow, string, string>(row => row[0].ToString(),
    //                                   row => row[0].ToString());

    //}
    [WebMethod]
    public static Dictionary<string, string> GetRegions()
    {
        string sql_qry = "select  Distinct [Region_ID],[Region_Name] from Ref_Region";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        //return DataTableToJsonstring(dt);
        return dt.AsEnumerable().ToDictionary<DataRow, string, string>(row => row[0].ToString(),
                                       row => row[1].ToString());

    }

    [WebMethod]
    public static void SaveReferenceBudgetViewArms(dynamic lst)
    {
        DataTable dtoffice = new DataTable();
        foreach (var item in lst)
        {
            string qry = "";
            int val = Convert.ToInt32(item["Actionstatus"]);
            if (val == 2)
            {
                foreach (KeyValuePair<string, dynamic> item1 in item)
                {
                    string Key = item1.Key;
                    dynamic Value = item1.Value;
                    if (Key == "COT_0_IRR_IRR" || Key == "COT_0_NI_NI" || Key == "COT_0_NIR_NIR" || Key == "CRN_0_IRR_IRR" ||
                        Key == "CRN_0_IRR_ORG" || Key == "CRN_0_NI_NI" || Key == "CRN_0_NI_ORG" || Key == "CRN_0_NIR" ||
                        Key == "CRN_CNV_IRR_IRR" || Key == "CRN_CNV_NI_NI" || Key == "PNT_0_IRR_IRR" || Key == "PNT_0_NI_NI" ||
                        Key == "RIC_0_IRR" || Key == "RIC_0_NIR" || Key == "SOY" || Key == "SOY_0_IRR" || Key == "SOY_0_IRR_IRR" ||
                        Key == "SOY_0_NI_NI" || Key == "SRG_0_IRR" || Key == "SRG_0_IRR_IRR" || Key == "SRG_0_NIR" ||
                        Key == "SWP_0_IRR_IRR" || Key == "SWP_0_NI_NI" || Key == "TBC_0_IRR_IRR" || Key == "TBC_0_NI_NI" ||
                        Key == "WHT_0_IRR" || Key == "WHT_0_NIR" || Key == "XYZ_0_IRR_IRR")
                    {
                        Value = (string.IsNullOrEmpty(Value)) ? "null" : "'" + Value + "'";

                        qry = "update Ref_Budget_Default set ARM_Budget=" + Value +
                            " where Crop_Full_Key='" + Key + "' and Budget_Expense_Type_ID='" + item["Budget_Expense_Type_ID"].Replace("'", "''") + "'" +
                            " and Z_Office_ID='" + item["Z_Office_ID"].Replace("'", "''") + "' and Z_Region_ID='" + item["Z_Region_ID"].Replace("'", "''") + "'";
                        int count = gen_db_utils.count_gp_sql_execute(qry, dbKey);
                        if (count == 0 && Value != "null")
                        {
                            string qry1 = " insert into Ref_Budget_Default (Crop_Full_Key,Budget_Expense_Type_ID,Z_Region_ID,Z_Office_ID,ARM_Budget)" +
                        "values('" + Key + "','" + item["Budget_Expense_Type_ID"].Replace("'", "''") + "'," +
                        "'" + item["Z_Region_ID"].Replace("'", "''") + "','" + item["Z_Office_ID"].Replace("'", "''") + "'," + Value + "); ";
                            gen_db_utils.gp_sql_execute(qry1, dbKey);
                        }
                    }
                }
            }
        }
    }
}