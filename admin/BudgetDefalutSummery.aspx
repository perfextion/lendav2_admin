﻿<%@ Page Title="" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="BudgetDefalutSummery.aspx.cs" Inherits="admin_BudgetDefalutSummery" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .ddlcolor {
            background-color: #C3BEBC;
        }
    </style>
    <div style="width: 42%; margin: 1%; margin-left: 3%!important;">
        <div class="form-inline">
            <label for="ddlofficename">Office Names :</label>
            <select id="ddlofficename" class="form-control" style="width: 200px">
                <option value="">Select</option>
                <option value="0_0">0:ARM Default</option>
                <%-- <option value="1_0" style="background-color: #C3BEBC">1:Central</option>
                <option value="2_0" style="background-color: #C3BEBC">2:East</option>
                <option value="3_0" style="background-color: #C3BEBC">3:West</option>--%>
            </select>
        </div>
    </div>
    <div class="row">
        <div class="loader"></div>
        <div class="col-md-11" style="padding-left: 51px; padding-top: 8px;">
            <asp:Label ID="lbl_table" runat="server" Text="lbl_table"></asp:Label>
        </div>
    </div>

    <script type="text/javascript"> 
        var timer;

        $(function () {

            var obj = {
                Init: function () {
                    obj.regionName();
                    obj.officename();
                    $('#ddlofficename').change(function () {
                        obj.loadtable();
                    })

                    $('.deleteIcon').each(function () {
                        $(this).click(function (e) {
                            deleteCropKey(e);
                        });
                    });

                    $('.loader').hide();
                },
                loadtable: function () {
                    $('#ContentPlaceHolder1_lbl_table').text('');
                    var officeid = $('#ddlofficename').val();
                    $.ajax({
                        type: "POST",
                        url: "BudgetDefalutSummery.aspx/LoadTable",
                        data: JSON.stringify({ OfficeID: officeid }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            $('#ContentPlaceHolder1_lbl_table').html(res.d);
                            $('.loader').hide();

                            $('.deleteIcon').each(function () {
                                $(this).click(function (e) {
                                    deleteCropKey(e);
                                });
                            });
                        }
                    });
                },
                regionName: function () {
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "ReferenceBudget.aspx/GetAllRegionnames",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            $.each(res.d, function (data, value) {
                                $("#ddlofficename").append($("<option class='ddlcolor' ></option>").val(data).html(value));
                            });
                        }
                    });
                },
                officename: function () {
                    $.ajax({
                        type: "POST",
                        url: "BudgetDefalutSummery.aspx/GetAllofficenames",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            $.each(res.d, function (data, value) {
                                $("#ddlofficename").append($("<option></option>").val(data).html(value));
                            });
                        }

                    });
                },

            }
            obj.Init();

        });

        function deleteCropKey(e) {
            var result = confirm('Are you sure you want to delete this crop key from budget?');
            if (result) {
                $('.loader').show();
                var aTag = $(e.target);
                var cropKey = aTag.data('cropKey');
                var regionId = aTag.data('regionId');
                var officeId = aTag.data('officeId');
                $.ajax({
                    type: "POST",
                    url: "BudgetDefalutSummery.aspx/DeleteCropKey",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ Crop_Key: cropKey, region_id: regionId, office_id: officeId }),
                    success: function (res) {
                        toastr.success("Saved Sucessful");
                        if (timer) { clearTimeout(timer); }
                        timer = setTimeout(function () {
                            location.reload();
                        }, 2000);
                    },
                    failure: function (response) {
                        $('.loader').hide();
                        var val = response.d;
                        toastr.warning(val);
                    }

                });
            }
        }


    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" runat="Server">
</asp:Content>

