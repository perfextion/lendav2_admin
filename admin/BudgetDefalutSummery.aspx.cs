﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_BudgetDefalutSummery : System.Web.UI.Page
{
    static string dbKey = "gp_conn";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            lbl_table.Text = LoadTable("");
        }
    }
    [WebMethod]
    public static string LoadTable(string OfficeID)
    {
        string strQry = "";
        strQry = " select Z_Region_ID,Z_Office_ID,Crop_Full_Key, count(*) as Count from Ref_Budget_Default" +
            " where ";

        string[] ids = OfficeID.Split('_');
        if (ids.Length >= 2)
        {
            if (!string.IsNullOrEmpty(ids[0]))
            {
                strQry += " Z_Region_ID = '" + ids[0] + "' and ";
            }
            if (!string.IsNullOrEmpty(ids[1]))
            {
                strQry += " Z_Office_ID = '" + ids[1] + "' and ";
            }
        }
        else
        {
            if (!string.IsNullOrEmpty(OfficeID))
            {
                strQry += " Z_Office_ID = '" + OfficeID + "' and ";
            }
        }
        strQry += " Crop_Full_Key is not null group by Crop_Full_Key,Z_Office_ID,Z_Region_ID";

        DataTable dt = gen_db_utils.gp_sql_get_datatable(strQry, dbKey);
        return GeneteateGrid(dt);
    }
    [WebMethod]
    public static Dictionary<string, string> GetAllofficenames()
    {
        string sql_qry = "select Office_ID,CONCAT( Office_ID,'.',Office_Name) as Office_Name from Ref_Office order by Office_ID";
        //  string sql_qry = "select Office_ID,Office_Name from Ref_Office order by Office_ID";

        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);

        return dt.AsEnumerable().ToDictionary<DataRow, string, string>(row => row[0].ToString(),
                                       row => row[1].ToString());
    }
    [WebMethod]
    public static Dictionary<string, string> GetAllRegionnames()
    {
        Log_utilitys.update_ref_key();
        string sql_qry = "select  concat([Region_ID],'_0') as Region_ID,CONCAT([Region_ID],'.',[Region_Name]) from Ref_Region ";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return dt.AsEnumerable().ToDictionary<DataRow, string, string>(row => row[0].ToString(),
                                       row => row[1].ToString());
    }

    [WebMethod]
    public static string GeneteateGrid(DataTable dt)
    {
        string str_return = "";

        if (dt != null && dt.Rows.Count > 0)
        {
            str_return += "<span style='color: green;'>Count of Crop Full Keys : " + dt.Rows.Count + "</span><div> <table id='tblLoanlist' class='table table-striped table-bordered' style='width:40%;' >  "
                                              + "<thead> "
                                 + "     <tr> "
                                      + "       <th >Crop Full Keys</th> "
                                      + "       <th style='text-align:center;'>Number Of Count</th> "
                                     + "       <th style='text-align:center;'>Region ID</th> "
                                     + "       <th style='text-align:center;'>Office ID</th> "
                                     + "       <th style='text-align:center;'>Action</th> "
                                 + "    </tr> "
                                 + "      </thead> "
                                 + "        <tbody> ";

            foreach (DataRow dtr1 in dt.Rows)
            {
                string commonid = "";
                if (dtr1["Z_Office_ID"].ToString() == "0")
                {
                    commonid = dtr1["Z_Region_ID"].ToString() + '_' + dtr1["Z_Office_ID"].ToString();
                }
                else
                {
                    commonid = dtr1["Z_Office_ID"].ToString();
                }

                str_return += "<tr>"
                                + "<td><a href ='../admin/ReferenceBudget.aspx?Crop_Full_Key=" + dtr1["Crop_Full_Key"].ToString() + "!" + commonid + "' > " + dtr1["Crop_Full_Key"].ToString() + "</a> </td>" +
                                  "<td style='text-align:center;'>" + dtr1["Count"].ToString() + "</td>"
                                + "<td style='text-align:center;'>" + dtr1["Z_Region_ID"].ToString() + "</td>"
                                + "<td style='text-align:center;'>" + dtr1["Z_Office_ID"].ToString() + "</td>"
                                + "<td style='text-align:center;'><a data-region-id='" + dtr1["Z_Region_ID"].ToString() + "' data-office-id='" + dtr1["Z_Office_ID"].ToString() + "' data-crop-key='" + dtr1["Crop_Full_Key"].ToString() + "' href='javascript:void(0)' class='glyphicon glyphicon-trash deleteIcon' style='color:red'></a></td>"
                            + "</tr>";

            }

            str_return += "</tbody> "
                    + " </table> "
                    + "</div>" +
                    "</div>" +
                "</div >";
        }
        else
        {
            str_return = "    No data available...";
        }
        return str_return;
    }

    [WebMethod]
    public static void DeleteCropKey(string Crop_Key, string region_id, string office_id)
    {
        string sql_query = "DELETE FROM Ref_Budget_Default Where Crop_Full_Key = '" + Crop_Key + "' And Z_Region_Id = '" + region_id + "' and Z_Office_Id = '" + office_id + "'";
        gen_db_utils.base_sql_execute(sql_query, dbKey);
    }
}