﻿<%@ Page Title="" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="Calender.aspx.cs" Inherits="admin_Calender" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">



    <meta charset='utf-8' />
    <link href='../css/fullcalendar.min.css' rel='stylesheet' />
    <link href='../css/fullcalendar.print.min.css' rel='stylesheet' media='print' />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <script src='../js/moment.min.js'></script>
    <script src='../js/fullcalendar/jquery.min.js'></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src='../js/fullcalendar/fullcalendar.min.js'></script>

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <style>
        body {
            /*margin: 40px 10px;*/
            padding: 0;
            font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
            font-size: 14px;
        }

        #calendar {
            max-width: 900px;
            margin: 0 auto;
        }

        .error {
            color: red;
            font-size: 12px;
        }
    </style>
    <div class="loader"></div>

    <div class="btn btn-success" id="btnSave">Save</div>
    <div id='calendar'></div>

    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-sm" style="width: 30%;">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Event</h4>
                </div>
                <div class="modal-body form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="email">User: <span class="error">*</span></label>
                        <div class="col-sm-9">
                            <select class="form-control isvalid" id="ddlUsers">
                                <option value="">Select</option>
                            </select>
                            <span id="spnUser" class="error">User is required</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="pwd">Title:<span class="error">*</span></label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control isvalid" id="txtTitle" placeholder="Enter Title" />
                            <span id="spnTitle" class="error">Title is required</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="email">Stat Date:<span class="error">*</span></label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control datepicker isvalid" id="txtSrart" placeholder="Enter Stat Date" />
                            <span id="spnStartDate" class="error">Stat Date is required</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="pwd">End Date:<span class="error">*</span></label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control datepicker isvalid" id="txtEnd" placeholder="Enter End Date" />
                            <span id="spnEndDate" class="error">End Date is required</span>
                            <%--       <span id="spnDate" class="error">End Date is greater than Start Date</span>--%>
                            <span id="spnDate" class="error">End Date should be after start date</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-9 text-align-right">
                            <div class="btn btn-success" id="btnAdd">Add</div>
                            <div class="btn btn-danger" id="btnDelete">Delete</div>
                            <input id="txt_current_event" type="hidden" value="" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                </div>
            </div>

        </div>
    </div>

    <script>
        $(function () {
            $(".datepicker").datepicker({ dateFormat: 'yy-mm-dd' });

        });

        $(document).ready(function () {
            var data;
            var today = new Date();
            var deleteRowIds = [];
            $('.error').hide()

            $.ajax({
                type: "POST",
                url: "Calender.aspx/GetUsers",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (res) {
                    var lstUsers = JSON.parse(res.d);
                    $.each(lstUsers, function (data, value) {
                        $("#ddlUsers").append($("<option></option>").val(value.userid).html(value.username));
                    })

                }
            });
            $.ajax({
                type: "POST",
                url: "Calender.aspx/GetVacation",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    $('.loader').show();
                    $('#calendar').fullCalendar({
                        header: {
                            left: 'prev,next today',
                            center: 'title',
                            right: 'month,agendaWeek,agendaDay'
                        },
                        defaultDate: today,
                        navLinks: true,
                        selectable: true,
                        selectHelper: true,
                        select: function (start, end) {
                            resetModel();
                            var startdate = FormattedDate(start._d);
                            $('#txtSrart').val(startdate);
                            $('#txtEnd').val(startdate);
                            $('#myModal').modal('show');
                        },
                        editable: true,
                        eventClick: function (event, element, jsEvent, view) {

                            var startDate = FormattedDate(event.start._d);
                            var endDt = FormattedDate(event.end._d);

                            var endDate = new Date(new Date(endDt).getTime() - (1 * 24 * 60 * 60 * 1000));
                            var endDate = FormattedDate(endDate);

                            $('#txtTitle').val(event.title);
                            $('#txtSrart').val(startDate);
                            $('#txtEnd').val(endDate);
                            $('#ddlUsers').val(event.userid);
                            $('#btnAdd').text("Update");
                            $('#txt_current_event').val(event._id);

                            $('#btnDelete').show();
                            $('#myModal').modal('show');

                        },
                        // eventLimit: true, // allow "more" link when too many events                       
                        events: JSON.parse(data.d),
                        eventRender: function (event, element) {
                            element.find('.fc-title').prepend(event.username + " : ");
                        }

                    });
                    $('.loader').hide();
                }
            });

            $('#btnSave').click(function () {

                var rowsData = [];
                var events = $('#calendar').fullCalendar('clientEvents');
                for (var i = 0; i < events.length; i++) {

                    if (events[i].actionstatus > 0) {

                        var start_date = new Date(events[i].start._d);
                        var end_date = '';
                        if (events[i].end != null) {
                            end_date = new Date(events[i].end._d);
                        }
                        var title = events[i].title;
                        var userid = events[i].userid;
                        var rowId = events[i].ID;

                        var st_day = start_date.getDate();
                        var st_monthIndex = start_date.getMonth() + 1;
                        var st_year = start_date.getFullYear();

                        var en_day = '';
                        var en_monthIndex = '';
                        var en_year = '';
                        if (end_date != '') {
                            en_day = end_date.getDate() - 1;
                            // en_day = end_date.getDate();
                            en_monthIndex = end_date.getMonth() + 1;
                            en_year = end_date.getFullYear();
                        }

                        eventData = {
                            title: title,
                            start: st_monthIndex + '/' + st_day + '/' + st_year,
                            end: en_monthIndex + '/' + en_day + '/' + en_year,
                            userid: userid,
                            id: rowId,
                            actionstatus: events[i].actionstatus
                        };
                        rowsData.push(eventData);
                    }

                }
                $.ajax({
                    type: "POST",
                    url: "Calender.aspx/SaveCalendar",
                    data: JSON.stringify({ lst: rowsData, deletelst: deleteRowIds }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {

                        location.reload();
                    },
                    failure: function (response) {
                        // alert(response.d);
                    }

                });

            });
            $('#btnAdd').click(function () {


                var isvalid = IsValidForm();
                if (isvalid) {

                    $('.error').hide();
                    var endDate = new Date(new Date($('#txtEnd').val()).getTime() + (1 * 24 * 60 * 60 * 1000));
                    if ($('#btnAdd').text() == "Add") {
                        eventData = {
                            title: $('#txtTitle').val(),
                            start: $('#txtSrart').val(),
                            end: endDate,
                            userid: $('#ddlUsers').val(),
                            username: $('#ddlUsers option:selected').text(),
                            actionstatus: 1
                        };
                        $('#calendar').fullCalendar('renderEvent', eventData, true);
                    }
                    else {
                        var current_event_id = $('#txt_current_event').val();
                        var current_event = $('#calendar').fullCalendar('clientEvents', current_event_id);
                        if (current_event && current_event.length == 1) {
                            //Retrieve current event from array
                            current_event = current_event[0];

                            //Set values                        
                            current_event.title = $('#txtTitle').val();
                            current_event.start = $('#txtSrart').val();
                            current_event.end = endDate;
                            current_event.id = $('#ddlUsers').val();
                            current_event.userid = $('#ddlUsers').val();
                            current_event.username = $('#ddlUsers option:selected').text();
                            var status = current_event.actionstatus || 0;
                            if (status == 0) {
                                current_event.actionstatus = 2;
                            }
                            //Update event
                            $('#calendar').fullCalendar('updateEvent', current_event);
                        }

                    }
                    resetModel();
                    $('#myModal').modal('hide');
                } else {
                    return false;
                }
            });
            $('#btnDelete').click(function () {

                var current_event_id = $('#txt_current_event').val();
                var current_event = $('#calendar').fullCalendar('clientEvents', current_event_id);

                //  if (current_event && current_event.length == 1) {
                current_event = current_event[0];
                deleteRowIds.push(current_event.ID);
                // }


                $('#calendar').fullCalendar('removeEvents', current_event_id);
                $('#myModal').modal('hide');
            });

            $('#ddlUsers').change(function () {
                var user = $('#ddlUsers').val().trim() || "";
                if (user != "") {
                    $('#spnUser').hide();
                }
            });
            $('#txtTitle').blur(function () {
                var title = $('#txtTitle').val().trim() || "";
                if (title.length > 0) {
                    $('#spnTitle').hide();
                }
            });
            $('#txtSrart').blur(function () {
                var date = $('#txtSrart').val().trim() || "";
                if (date.length > 0) {
                    $('#spnStartDate').hide();
                }
            });
            $('#txtEnd').blur(function () {
                var date = $('#txtEnd').val().trim() || "";
                if (date.length > 0) {
                    $('#spnEndDate').hide();
                }
            });
            setInterval(function () { $('.fc-time').hide(); }, 100);
        });

        function IsValidForm() {
            var count = 0;
            var user = $('#ddlUsers').val().trim() || "";
            var title = $('#txtTitle').val().trim() || "";
            var startDate = $('#txtSrart').val().trim() || "";
            var endDate = $('#txtEnd').val().trim() || "";

            if (user == "") {
                $('#spnUser').show();
                count++;
            } else {
                $('#spnUser').hide();
            }
            if (title == "") {
                $('#spnTitle').show();
                count++;
            } else {
                $('#spnTitle').hide();
            }
            if (startDate == "") {
                $('#spnStartDate').show();
                count++;
            } else {
                $('#spnStartDate').hide();
            }
            if (endDate == "") {
                $('#spnEndDate').show();
                count++;
            } else {
                $('#spnEndDate').hide();
            }

            if (startDate != "" && endDate != "") {

                var stDate = new Date($('#txtSrart').val());
                var edDate = new Date($('#txtEnd').val());

                if (stDate <= edDate) {
                    $('#spnDate').hide();
                } else {
                    $('#spnDate').show();
                    count++;
                }
            }

            return (count == 0) ? true : false;

        }
        function FormattedDate(val) {
            var today = new Date(val);
            var dd = today.getDate();
            var mm = today.getMonth() + 1;

            var yyyy = today.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            // var date = dd + '/' + mm + '/' + yyyy;
            var date = yyyy + '-' + mm + '-' + dd;
            return date
        }
        function resetModel() {
            $('#txtTitle').val('');
            $('#txtSrart').val('');
            $('#txtEnd').val('');
            $('#ddlUsers').val('');
            $('#btnAdd').text("Add");
            $('#btnDelete').hide();
        }
    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" runat="Server">
</asp:Content>

