﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_Calender : System.Web.UI.Page
{
    static string dbKey = "gp_conn";
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [WebMethod]
    public static string GetVacation()
    {
        string sql_qry = "select vac.title as title ,vac.ID,LEFT(CONVERT(VARCHAR, vac.StartDate, 120), 10) as start,LEFT(CONVERT(VARCHAR, vac.EndDate+1, 120), 10) as 'end',0 as Actionstatus,usr.UserID as userid,usr.firstName as username  from Vacation vac inner join Users usr on usr.UserID=vac.UserID";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);

        string daresult = null;
        daresult = Newtonsoft.Json.JsonConvert.SerializeObject(dt);
        return daresult;
    }

    [WebMethod]
    public static string GetUsers()
    {
        string sql_qry = "select userid,firstname as username from users";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
    }

    [WebMethod, ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static void SaveCalendar(dynamic lst, dynamic deletelst)
    {
        foreach (var item in lst)
        {
            string qry = "";

            int val = Convert.ToInt32(item["actionstatus"]);
            if (val == 1)
            {
                qry = "INSERT INTO vacation(Title,StartDate,EndDate,UserID)" +
                    " VALUES('" + ManageQuotes(item["title"]) + "','" + ManageQuotes(item["start"]) + "','" + ManageQuotes(item["end"]) + "'," + ManageQuotes(item["userid"]) + ")";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 2)
            {
                qry = "UPDATE vacation SET Title = '" + ManageQuotes(item["title"]) + "' ,StartDate = '" + ManageQuotes(item["start"]) + "' ,EndDate = '" + ManageQuotes(item["end"]) + "' ,userid = '" + ManageQuotes(item["userid"]) + "' where ID=" + item["id"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
        }

        foreach (var item in deletelst)
        {
            var qry = "DELETE FROM vacation  where ID=" + item + "  ";
            gen_db_utils.gp_sql_execute(qry, dbKey);
        }
    }

    public static string ManageQuotes(string value)
    {
        if (value.Contains("'"))
        {
            value = value.Replace("'", "''");
        }
        return value;
    }
}