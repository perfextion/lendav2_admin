﻿<%@ Page Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="ClearTables.aspx.cs" Inherits="admin_ClearTables" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div style="padding-top: 25px;padding-left:25px">
        <div class="form-horizontal">
            <div class="row">
                <div class="form-group">
                    <label for="inputPassword" class="col-sm-1 control-label">Enter Loan ID</label>
                    <div class="col-sm-2">
                        
                        <asp:TextBox ID="txtLoanid" class="form-control" runat="server"></asp:TextBox>
                        <cc1:MaskedEditExtender ID="MaskedEditExtender1" runat="server" TargetControlID="txtLoanid"

            MaskType="Number" Mask="9999" MessageValidatorTip="true" AcceptNegative="None"

                                InputDirection="RightToLeft" ErrorTooltipEnabled="true" >

            </cc1:MaskedEditExtender>
                    </div>
                    <asp:Button ID="Btn_Disburse" CssClass="btn btn-danger" runat="server" Text="Clear Disburse" OnClick="Btn_Disburse_Click" />
                </div>

                
             </div>
        </div>
    </div>
</asp:Content>



