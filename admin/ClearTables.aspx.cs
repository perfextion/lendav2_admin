﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_ClearTables : System.Web.UI.Page
{
    string dbKey = "gp_conn";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["UserID"] == null)
            {
                Response.Redirect("../Login.aspx");
            }
            else
            {
                
            }
        }
    }


    protected void Btn_Disburse_Click(object sender, EventArgs e)
    {
        string loanid = txtLoanid.Text;
        //Clear Disburse Data here
        string dataquery = @"Delete from [dbo].[Loan_Disburse] where [Loan_ID]='" + loanid + @"'

    Delete from [dbo].[Loan_Disburse_Approval_Hist] where [Loan_ID]='" + loanid + @"'

    Delete from [dbo].[Loan_Disburse_Detail] where [Loan_ID]='" + loanid + @"'

    Delete from [dbo].[Loan_Disburse_Rent_Detail] where [Loan_ID]='" + loanid + @"'

    Delete from [dbo].[User_Pending_Action] where [Ref_PA_ID]=18 and [Loan_Full_ID] like '%" + loanid + "%'";
        gen_db_utils.base_sql_execute(dataquery, dbKey);
    }
}