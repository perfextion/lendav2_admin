﻿<%@ Page Title="Crop Key Summary" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="CropKeySummary.aspx.cs" Inherits="admin_CropKeySummary" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style>
        #content {
            padding: 24px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div>
        <div class="loader"></div>
        <div class="row">
            <div style="margin-left: 50px">
                <table id="jqGrid"></table>
                <div id="jqGridPager"></div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" Runat="Server">
    <script type="text/javascript">  
        var DeleteRows = [];
        var timer;
        var lastSelection;
        var cropPracticeType = {};

        $('#navbtnsave').addClass('disabled');
        $('#navbtnadd').addClass('disabled');

        $(function () {
            var obj = {
                Init: function () {
                    $('.loader').show();
                    obj.bindGrid();

                    $("#navbtnrefresh").on('click', function () {
                        $('.loader').show();
                        $('#txtSearchBar').val('');
                        obj.bindGrid();
                    });

                    $("#txtSearchBar").on("keyup", function () {
                        var self = this;
                        obj.search(self);
                    });
                },
                bindGrid: function () {
                    $.ajax({
                        type: "POST",
                        url: "CropKeySummary.aspx/LoadTable",
                        data: JSON.stringify({  }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var resData = JSON.parse(data.d);
                            $('.loader').show();
                            for (var i = 0; i < resData.length; i++) {
                                resData[i].rowid = i + 1;
                            }
                            obj.loadgrid(resData);
                            $('.loader').hide();
                        }
                    });
                },
                loadgrid: function (data) {
                    $.jgrid.gridUnload("#jqGrid");
                    $("#jqGrid").jqGrid({
                        datatype: "local",
                        styleUI: 'Bootstrap',
                        data: data,
                        colModel: [
                            { label: 'rowid', name: 'rowid', width: 75, editable: false, key: true, hidden: true, hidedlg: true },
                            { label: 'Crop And Practice ID', name: 'Crop_And_Practice_ID', width: 150, align: 'center', editable: false, hidden: false },
                            {
                                label: 'Crop_Code',
                                name: 'Crop_Code',
                                width: 90,
                                align: "left",
                                classes: 'left',
                                editable: false
                            },
                            {
                                label: 'Crop_Type_Code',
                                name: 'Crop_Type_Code',
                                width: 150,
                                align: "left",
                                classes: 'left',
                                editable: false
                            },
                            {
                                label: 'Irr_Prac_Code',
                                name: 'Irr_Prac_Code',
                                width: 150,
                                align: "left",
                                classes: 'left',
                                editable: false
                            },
                            {
                                label: 'Crop_Prac_Code',
                                name: 'Crop_Prac_Code',
                                width: 150,
                                align: "left",
                                classes: 'left',
                                editable: false
                            },
                            {
                                label: 'Crop_Full_Key',
                                name: 'Crop_Full_Key',
                                width: 300,
                                align: "left",
                                classes: 'left',
                                editable: false
                            },
                            {
                                label: 'ARM Budget Count',
                                name: 'Count',
                                align: "center",
                                width: 150,
                                editable: false
                            },
                            {
                                label: 'Add',
                                name: '',
                                align: "center",
                                width: 90,
                                editable: false,
                                formatter: obj.actionLink
                            },
                            {
                                label: 'Delete',
                                name: '',
                                align: "center",
                                width: 90,
                                editable: false,
                                formatter: obj.deleteLink
                            }
                        ],
                        viewrecords: true,
                        loadonce: true,
                        restoreAfterSelect: false,
                        saveAfterSelect: true,
                        sortable: true,
                        onPaging: obj.changePage,
                        height: 'auto',
                        rowNum: 100,
                        pager: "#jqGridPager"
                    });
                },
                search: function (self) {
                    if (timer) { clearTimeout(timer); }
                    timer = setTimeout(function () {
                        obj.changePage();
                        $("#jqGrid").jqGrid('filterInput', self.value);
                        var length = jQuery("#jqGrid").jqGrid('getGridParam', 'records');
                        $('#txtRecordsCount').html('Records Count: ' + length);
                    }, 0);
                },
                changePage: function () {
                    lastSelection = "";
                    var grid = $("#jqGrid");
                    var id = $('.inline-edit-cell').parent().parent().prop('id');
                    if (id != undefined) {
                        grid.jqGrid('saveRow', id);
                        grid.jqGrid('restoreRow', id);
                        var row = grid.jqGrid('getRowData', id);
                        if (!jQuery.isEmptyObject(row)) {
                            var dataobj = jQuery("#jqGrid").jqGrid('getGridParam', 'data');
                            index = dataobj.findIndex(x => x.rowid == row.rowid);
                            if (row.rowid > 0 && row.Actionstatus != 1) {
                                row.Actionstatus = 2;
                                grid.jqGrid('getGridParam', 'data')[index] = row;
                            }
                        }
                    }
                },
                actionLink: function (cellValue, options, rowdata, action) {
                    return !rowdata.Count ? "<a href='javascript:addToBudget(" + options.rowId + ")' class='glyphicon glyphicon-plus' style='color:red'></a>" : "";
                },
                deleteLink: function (cellValue, options, rowdata, action) {
                    return rowdata.Count ? "<a href='javascript:deleteRecord(" + options.rowId + ")' class='glyphicon glyphicon-trash' style='color:red'></a>" : "";
                }
            }
            obj.Init();
        });


        function addToBudget(ediId) {
            var res = confirm('Are you sure you want to add Budget for this key?');
            if (res) {
                $('.loader').show();
                var grid = $('#jqGrid');
                grid.jqGrid('saveRow', ediId);
                var rowData = grid.jqGrid('getRowData', ediId);

                $.ajax({
                    type: "POST",
                    async: false,
                    url: "ReferenceBudget.aspx/AddNewCropKey",
                    data: JSON.stringify({ Crop_Full_Key: rowData.Crop_Full_Key }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (res) {
                        toastr.success("Saved Sucessful");

                        if (timer) { clearTimeout(timer); }
                        timer = setTimeout(function () {
                            location.reload();
                        }, 2000);
                    },
                    failure: function (e) {
                        $('.loader').hide();
                    }
                });
            }
        }

        function deleteRecord(ediId) {
            var result = confirm('Are you sure you want to delete this crop key from budget?');
            if (result) {
                $('.loader').show();
                var grid = $('#jqGrid');
                grid.jqGrid('saveRow', ediId);
                var rowData = grid.jqGrid('getRowData', ediId);

                $.ajax({
                    type: "POST",
                    url: "BudgetDefalutSummery.aspx/DeleteCropKey",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ Crop_Key: rowData.Crop_Full_Key, region_id: 0, office_id: 0 }),
                    success: function (res) {
                        toastr.success("Saved Sucessful");
                        if (timer) { clearTimeout(timer); }
                        timer = setTimeout(function () {
                            location.reload();
                        }, 2000);
                    },
                    failure: function (response) {
                        $('.loader').hide();
                        var val = response.d;
                        toastr.warning(val);
                    }

                });
            }
        }
    </script>
</asp:Content>

