﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_CropKeySummary : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        base.Page_Load(sender, e);
    }

    [WebMethod]
    public static string LoadTable()
    {
        string sql_qry = @"select *, ISNULL(A.[Count], 0)
                            from V_Crop_price_Details VC
                            LEFT JOIN 
                            ( 
                                Select Count(Crop_Full_Key) as [Count], Crop_Full_Key From Ref_Budget_Default where ref_key = '0_0' Group by Crop_Full_Key
                            ) A on A.Crop_Full_Key = Vc.Crop_Full_Key";        

        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return JsonConvert.SerializeObject(dt);
    }
}