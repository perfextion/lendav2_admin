﻿<%@ Page Title="" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="DBMapping.aspx.cs" Inherits="admin_DBMapping" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="http://www.guriddo.net/demo/css/trirand/ui.jqgrid-bootstrap.css" />

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/ecmascript" src="http://www.guriddo.net/demo/js/trirand/i18n/grid.locale-en.js"></script>
    <script type="text/ecmascript" src="http://www.guriddo.net/demo/js/trirand/src/jquery.jqGrid.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

    <style>
        .ui-jqgrid .ui-jqgrid-htable th div {
            height: auto;
            overflow: hidden;
            padding-right: 4px;
            padding-top: 2px;
            position: relative;
            vertical-align: text-top;
            white-space: normal !important;
        }
    </style>
    <div class="">
        <div class="panel-body row-padding">
            <div class="row">
                <div class="col-md-3" style="padding-left: 51px;">
                    <div id="btnAddRow" class="btn btn-info" style="width: 60px;">
                        add
                    </div>
                    <div id="btnSave" class="btn btn-success" style="width: 60px;">Save</div>
                </div>
                <div class="col-md-3">
                    <div class="form-inline">
                        <div class="form-group">
                            <label for="ddlTabNames">Tab Name :</label>
                            <select id="ddlTabNames" class="form-control loadscreen" style="width: 200px">
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-inline">
                        <div class="form-group">
                            <label for="ddlSectionNames">Section Name :</label>
                            <select id="ddlSectionNames" class="form-control loadscreen" style="width: 200px">
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-inline">
                        <div class="form-group">
                            <label for="txtSearch">Search :</label>
                            <input type="text" class="form-control" name="search" id="txtSearch" style="width: 200px" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="loader"></div>
        <div class="row">
            <div style="margin-left: 50px">
                <table id="jqGrid"></table>
                <div id="jqGridPager"></div>
            </div>
        </div>
    </div>
    <br />
    <br />
    <br />


    <script type="text/javascript">
        var DeleteRows = [];
        var timer;
        var lastSelection;
        function loadScreen() {
            var ModifyRows = [];
            DeleteRows = [];
            $(function () {
                var obj = {
                    Init: function () {
                        obj.getGridData();
                        $("#btnAddRow").click(function () {
                            obj.add();
                        });
                        $("#btnSave").click(function () {
                            obj.save();
                        });
                        $("#txtSearch").on("keyup", function () {
                            var self = this;
                            obj.search(self);
                        });
                        $(".loadscreen").on("change", function () {
                            obj.filedSearch();
                        });
                    },
                    getGridData: function () {

                        $.ajax({
                            type: "POST",
                            url: "DBMapping.aspx/GetSections",
                            contentType: "application/json; charset=utf-8",
                            data: "{tabName:'" + $('#ddlTabNames').val() + "',sectionName:'" + $('#ddlSectionNames').val() + "'}",
                            dataType: "json",
                            success: function (data) {
                                var resData = JSON.parse(data.d);
                                $('.loader').show();
                                for (var i = 0; i < resData.length; i++) {
                                    resData[i].rowid = i + 1;
                                }
                                obj.loadgrid(resData);
                                $('.loader').hide();
                            }
                        });
                    },
                    loadgrid: function (data) {
                        $.jgrid.gridUnload("#jqGrid");
                        $("#jqGrid").jqGrid({
                            datatype: "local",
                            styleUI: 'Bootstrap',
                            data: data,
                            colModel: [
                                { label: 'rowid', name: 'rowid', width: 75, editable: false, key: true, hidden: true },
                                { label: 'Id', name: 'Id', width: 35, editable: false },
                                { label: 'LendaPlus Tab', name: 'LendaPlus_Tab', width: 100, editable: true },
                                { label: 'LendaPlus Section', name: 'LendaPlus_Section', width: 150, editable: true, hidden: false },
                                { label: 'LendaPlus UI Element', name: 'LendaPlus_UI_Element', width: 150, editable: true },
                                { label: 'UI Entry Type', name: 'UI_Entry_Type', width: 100, editable: true },
                                { label: 'Is Calculated', name: 'is_Calculated', width: 120, editable: true },
                                { label: 'Formula', name: 'Formula', width: 250, editable: true },
                                { label: 'Local Loan Object', name: 'Local_object', width: 150, editable: true },
                                { label: 'Sort Order', name: 'Sort_Order', sorttype: 'number', width: 70, editable: true },
                                { label: 'LendaPlus DB Table', name: 'LendaPlus_DB_Table', width: 150, editable: true },
                                { label: 'LendaPlus DB Field', name: 'LendaPlus_DB_Field', width: 150, editable: true },
                                { label: 'API_Element', name: 'API_Element', width: 150, editable: true, hidden: true },
                                { label: 'Actionstatus', name: 'Actionstatus', width: 70, editable: false, hidden: true },
                                { label: '', name: '', width: 35, formatter: obj.deleteLink },
                            ],
                            viewrecords: true,
                            loadonce: true,
                            restoreAfterSelect: false,
                            saveAfterSelect: true,
                            sortable: true,
                            'cellsubmit': 'clientArray',
                            onSelectRow: obj.edit,
                            onPaging: obj.changePage,
                            height: 'auto',
                            rowNum: 100,
                            pager: "#jqGridPager"
                        });

                        if ($('#txtSearch').val() != "") {
                            $('#txtSearch').trigger('keyup');
                        }
                    },
                    add: function () {
                        var grid = $("#jqGrid");
                        var rowsperPage = parseInt(grid.getGridParam('rowNum'));
                        var gridlength = grid.jqGrid('getGridParam', 'data').length;
                        var curpage = parseInt($(".ui-pg-input").val());
                        var totPages = Math.ceil(gridlength / rowsperPage);
                        if (rowsperPage * totPages == gridlength) {
                            var id = $('.inline-edit-cell').parent().parent().prop('id');
                            grid.jqGrid('saveRow', id);
                            var row = obj.newrow();
                            var newRowId = row.rowid;
                            grid.jqGrid('addRowData', newRowId, row);
                            grid.trigger('reloadGrid');
                            lastSelection = newRowId;
                            grid.jqGrid('saveRow', lastSelection);
                            grid.jqGrid('restoreRow', lastSelection);
                            $('.glyphicon-step-forward').trigger('click');
                        } else {
                            $('.glyphicon-step-forward').trigger('click');
                            var row = obj.newrow();
                            var newRowId = row.rowid;
                            grid.jqGrid('addRowData', newRowId, row);
                        }
                        lastSelection = newRowId;
                        grid.jqGrid('saveRow', lastSelection);
                        grid.jqGrid('restoreRow', lastSelection);

                        var eid = $('.inline-edit-cell').parent().parent().prop('id')
                        grid.jqGrid('saveRow', eid);
                        grid.jqGrid('restoreRow', eid);
                        grid.jqGrid('editRow', newRowId);
                    },
                    edit: function (id) {
                  
                        if (id && id !== lastSelection) {
                            var grid = $("#jqGrid");
                            grid.jqGrid('saveRow', lastSelection);
                            grid.jqGrid('restoreRow', lastSelection);
                            var row = grid.jqGrid('getRowData', lastSelection);

                            if (!jQuery.isEmptyObject(row)) {
                                var dataobj = jQuery("#jqGrid").jqGrid('getGridParam', 'data');
                                index = dataobj.findIndex(x => x.rowid == row.rowid);
                                if (row.rowid > 0 && row.Actionstatus != 1) {
                                    row.Actionstatus = 2;
                                    grid.jqGrid('getGridParam', 'data')[index] = row;
                                }
                            }
                            grid.jqGrid('editRow', id);
                            lastSelection = id;
                        }
                    },
                    newrow: function () {
                        var newid = 0;

                        var grid = $("#jqGrid");
                        var length = jQuery("#jqGrid").jqGrid('getGridParam', 'records');
                        var dataobj = grid.jqGrid('getGridParam', 'data');
                        var dataobj = grid.jqGrid('getGridParam', 'data');

                        IDs = dataobj.map(function (e) { return e.rowid });
                        if (IDs.length > 0) {
                            newid = IDs.reduce(function (a, b) { return Math.max(a, b); });
                        }

                        var row = {
                            Actionstatus: 1,
                            rowid: newid + 1,
                            Id: 0,
                            LendaPlus_Tab: "",
                            LendaPlus_Section: "",
                            LendaPlus_UI_Element: "",
                            API_Element: "",
                            UI_Entry_Type: "",
                            is_Calculated: "",
                            Local_object: "",
                            LendaPlus_DB_Table: "",
                            LendaPlus_DB_Field: "",
                            Formula: ""
                        };
                        return row;
                    },
                    save: function () {
                        $('.loader').show();
                        var grid = $("#jqGrid");
                        var allrows = [];
                        var id = $('.inline-edit-cell').parent().parent().prop('id');
                        grid.jqGrid('saveRow', id);
                        var rowData = grid.jqGrid('getRowData', id);
                        if (rowData.rowid > 0) {
                            rowData.Actionstatus = 2;
                            grid.jqGrid('getGridParam', 'data')[id] = rowData;
                        }
                        for (var i = 0; i < DeleteRows.length; i++) {
                            DeleteRows[i].Actionstatus = 3
                            allrows.push(DeleteRows[i]);
                        }
                        var dataobj = grid.jqGrid('getGridParam', 'data');
                        var newRows = $.grep(dataobj, function (e) {
                            if (e != undefined) {
                                return e.Actionstatus == 1 || e.Actionstatus == 2
                            }
                        });
                        for (var i = 0; i < newRows.length; i++) {
                            allrows.push(newRows[i]);
                        }

                        $.ajax({
                            type: "POST",
                            url: "DBMapping.aspx/SaveSections",
                            data: JSON.stringify({ lst: allrows }),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (data) {
                                $("#ddlTabNames").val("");
                                $("#ddlSectionNames").val("");
                                obj.getGridData();
                            },
                            failure: function (response) {
                                console.log(response.d);
                            }

                        });
                    },
                    delete: function (id) {

                    },
                    search: function (self) {
                        if (timer) { clearTimeout(timer); }
                        timer = setTimeout(function () {
                            $("#jqGrid").jqGrid('filterInput', self.value);
                        }, 0);
                    },
                    filedSearch: function () {

                        var postData = $("#jqGrid").jqGrid("getGridParam", "postData"),
                            colModel = $("#jqGrid").jqGrid("getGridParam", "colModel"),
                            rules = [],
                            searchText = $("#ddlTabNames").val(),
                            l = colModel.length,
                            i,
                            cm;
                        if (searchText != "") {
                            rules.push({ field: "LendaPlus_Tab", op: "cn", data: searchText });
                        }

                        if ($("#ddlSectionNames").val() != "") {
                            rules.push({ field: "LendaPlus_Section", op: "cn", data: $("#ddlSectionNames").val() });
                        }

                        //for (i = 0; i < l; i++) {
                        //    cm = colModel[i];
                        //    //if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                        //    if (cm.name) {


                        //        rules.push({
                        //            field: cm.name,
                        //            op: "cn",
                        //            data: searchText
                        //        });
                        //    }
                        //}
                        postData.filters = JSON.stringify({
                            groupOp: "OR",
                            rules: rules
                        });

                        $("#jqGrid").jqGrid("setGridParam", { search: true });
                        $("#jqGrid").trigger("reloadGrid", [{ page: 1, current: true }]);
                        // return false;

                    },
                    changePage: function () {
                        lastSelection = "";
                        var grid = $("#jqGrid");

                        var id = $('.inline-edit-cell').parent().parent().prop('id');
                        if (id != undefined) {
                            grid.jqGrid('saveRow', id);
                            grid.jqGrid('restoreRow', id);
                            var row = grid.jqGrid('getRowData', id);
                            if (!jQuery.isEmptyObject(row)) {
                                var dataobj = jQuery("#jqGrid").jqGrid('getGridParam', 'data');
                                index = dataobj.findIndex(x => x.rowid == row.rowid);
                                if (row.rowid > 0 && row.Actionstatus != 1) {
                                    row.Actionstatus = 2;
                                    grid.jqGrid('getGridParam', 'data')[index] = row;
                                }
                            }
                        }
                    },
                    deleteLink: function (cellValue, options, rowdata, action) {
                        return "<a href='javascript:deleteRecord(" + options.rowId + ")' class='glyphicon glyphicon-trash' style='color:red'></a>";
                    }
                }
                obj.Init();
            });
        }
        function deleteRecord(id) {

            var grid = $('#jqGrid');
            var result = confirm("Are you sure you Want to delete?");

            if (result == true) {
                var ediId = $('.inline-edit-cell').parent().parent().prop('id');
                grid.jqGrid('saveRow', ediId);
                var rowData = grid.jqGrid('getRowData', ediId);
                if (rowData.rowid > 0 && rowData.Actionstatus != 1) {
                    rowData.Actionstatus = 2;
                    grid.jqGrid('getGridParam', 'data')[ediId - 1] = rowData;
                }

                var row = grid.jqGrid('getRowData', id);
                grid.jqGrid('delRowData', id);
                if (row.Actionstatus != 1) {
                    DeleteRows.push(row);
                }
                var data = jQuery("#jqGrid").jqGrid('getGridParam', 'data');
                for (var i = 0; i < data.length; i++) {
                    if (data[i].Id == 0) {
                        data[i].rowid = i + 1;
                        data[i].id = i + 1;
                    } else
                        data[i].rowid = i + 1;
                }
                var curpage = parseInt($(".ui-pg-input").val());


                jQuery('#jqGrid').jqGrid('clearGridData');
                jQuery('#jqGrid').jqGrid('setGridParam', { data: data });
                $("#jqGrid").trigger("reloadGrid", [{ page: curpage }]);
                $("#jqGrid").trigger("reloadGrid", [{ page: curpage }]);
                lastSelection = id;
            }
        }
        function unique(list) {
            var result = [];
            $.each(list, function (i, e) {
                if ($.inArray(e, result) == -1) result.push(e);
            });
            return result;
        }

        $(document).ready(function () {

            $.ajax({
                type: "POST",
                async: false,
                url: "DBMapping.aspx/GetTabNames",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (res) {

                    $.each(res.d, function (data, value) {
                        $("#ddlTabNames").append($("<option></option>").val(value).html(value));
                    })
                }
            });
            $.ajax({
                type: "POST",
                async: false,
                url: "DBMapping.aspx/GetSectionsNames",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (res) {

                    $.each(res.d, function (data, value) {
                        $("#ddlSectionNames").append($("<option></option>").val(value).html(value));
                    })
                }
            });
            loadScreen();
        });



    </script>


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" runat="Server">
</asp:Content>

