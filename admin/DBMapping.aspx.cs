﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_DBMapping : System.Web.UI.Page
{
    static string dbKey = "gp_conn";
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    [WebMethod]
    public static string GetSections(string tabName, string sectionName)
    {
        string sql_qry = "SELECT 0 as Actionstatus,Id,LendaPlus_Tab,LendaPlus_Section,LendaPlus_UI_Element,API_Element,UI_Entry_Type,is_Calculated,Local_object,LendaPlus_DB_Table,LendaPlus_DB_Field,Formula,Sort_Order from  Z_DB_Mapping where 0=0 ";

        //if (!String.IsNullOrEmpty(tabName) && !String.IsNullOrWhiteSpace(tabName))
        //    sql_qry = sql_qry + " and LendaPlus_Tab='"+ tabName + "' ";
        //if (!String.IsNullOrEmpty(sectionName) && !String.IsNullOrWhiteSpace(sectionName))
        //    sql_qry = sql_qry + " and LendaPlus_Section='" + sectionName + "' ";

        sql_qry = sql_qry + " order by Id asc ";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
    }
    [WebMethod]
    public static List<string> GetTabNames()
    {
        string sql_qry = "SELECT distinct LendaPlus_Tab FROM Z_DB_Mapping order by LendaPlus_Tab";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);

        List<string> emp = new List<string>();
        emp = (from DataRow row in dt.Rows
               select row["LendaPlus_Tab"].ToString()
               ).ToList();
        return emp;
    }
    [WebMethod]
    public static List<string> GetSectionsNames()
    {
        string sql_qry = "SELECT distinct coalesce(LendaPlus_Section,'') as LendaPlus_Section FROM Z_DB_Mapping order by  LendaPlus_Section";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);

        List<string> emp = new List<string>();
        emp = (from DataRow row in dt.Rows
               select row["LendaPlus_Section"].ToString()
               ).ToList();
        return emp;
    }

    [WebMethod]
    public static void SaveSections(dynamic lst)
    {
        foreach (var item in lst)
        {
            string qry = "";
            int val = Convert.ToInt32(item["Actionstatus"]);
            if (val == 1)
            {
                qry = "INSERT INTO Z_DB_Mapping(LendaPlus_Tab,LendaPlus_Section,LendaPlus_UI_Element,API_Element,UI_Entry_Type,is_Calculated,Local_object,LendaPlus_DB_Table,LendaPlus_DB_Field,Formula,Sort_Order)" +
                    " VALUES('" + ManageQuotes(item["LendaPlus_Tab"]) + "','" + ManageQuotes(item["LendaPlus_Section"]) + "','" + ManageQuotes(item["LendaPlus_UI_Element"]) + "','" + ManageQuotes(item["API_Element"]) +
                    "','" + ManageQuotes(item["UI_Entry_Type"]) + "','" + ManageQuotes(item["is_Calculated"]) + "','" + ManageQuotes(item["Local_object"]) + "','" + ManageQuotes(item["LendaPlus_DB_Table"]) + "','"
                    + ManageQuotes(item["LendaPlus_DB_Field"]) + "','" + ManageQuotes(item["Formula"]) + "','" + item["Sort_Order"] + "')";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 2)
            {
                qry = "UPDATE Z_DB_Mapping SET " +
                    "LendaPlus_Tab = '" + ManageQuotes(item["LendaPlus_Tab"]) + "' ," +
                    "LendaPlus_Section = '" + ManageQuotes(item["LendaPlus_Section"]) + "' ," +
                    "LendaPlus_UI_Element = '" + ManageQuotes(item["LendaPlus_UI_Element"]) + "' ," +
                    "API_Element = '" + ManageQuotes(item["API_Element"]) + "' ," +
                    "UI_Entry_Type = '" + ManageQuotes(item["UI_Entry_Type"]) + "' ," +
                    "is_Calculated = '" + ManageQuotes(item["is_Calculated"]) + "' ," +
                    "Local_object = '" + ManageQuotes(item["Local_object"]) + "' ," +
                    "LendaPlus_DB_Table = '" + ManageQuotes(item["LendaPlus_DB_Table"]) + "' ," +
                    "LendaPlus_DB_Field = '" + ManageQuotes(item["LendaPlus_DB_Field"]) + "' ," +
                    "Formula = '" + ManageQuotes(item["Formula"]) + "' ," +
                    "Sort_Order = '" + ManageQuotes(item["Sort_Order"]) + "'" +
                    " where Id=" + item["Id"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 3)
            {
                qry = "DELETE FROM Z_DB_Mapping  where Id=" + item["Id"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }

        }
    }

    public static string ManageQuotes(string value)
    {
        if (value != null && value.Contains("'"))
        {
            value = value.Replace("'", "''");
        }
        return value;
    }
}