﻿<%@ Page Title="" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="Dashboard.aspx.cs" Inherits="admin_Dashboard" ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="../js/tinymce/tinymce.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/js/materialize.min.js">
    </script>



</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="container" style="padding-top: 25px;">
        <div class="form-horizontal">
            <div class="row">
                <div class="col-md-6">
                </div>
                <div class="col-md-6" style="text-align: right">
                    <asp:Label ID="lblmsg" Style="color: forestgreen" runat="server"></asp:Label>
                    <asp:Button ID="btn_CheckinorCheckout" CssClass="btn btn-success" runat="server" OnClick="btn_CheckinorCheckout_Click" />
                </div>
            </div>
            <div class="row">
                <div class="col-md-12" runat="server" id="divTaskDescriptoin">
                    <div class="col-md-9">
                        <div class="form-group" style="margin-bottom: 0;">
                            <label for="pwd">Check in Time from:</label>
                            <asp:Label ID="lblCheckinTime" runat="server" Style="font-size: 15px; font-weight: 600;" />
                        </div>
                        <div class="form-group">
                            <label for="pwd">Task Descripton :</label>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Rows="5" ID="txt_TaskDesc" />
                        </div>
                        <div class="form-group">
                            <asp:Button ID="btnsave" CssClass="btn btn-primary" Text="Save" runat="server" OnClick="btnsave_Click" />
                            <asp:Label ID="lblchecking" Style="color: forestgreen" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="col-md-3">
                    </div>
                </div>

  </div>
            <div class="row">
                <div class="col-md-6">
                    <asp:Label ID="lbl_table" runat="server" Text="lbl_table"></asp:Label>
                </div>
            <div class="col-md-6">
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        // $('#header,#left-panel').hide();
        //function HideLabel() {
        //    $('#ContentPlaceHolder1_lblchecking').delay(7000).fadeOut('slow');
        //};

        tinymce.init({
            selector: 'textarea',
            height: 150,
            theme: 'modern',
            //   plugins: 'autoresize code print preview searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount   imagetools    contextmenu colorpicker textpattern help',
            toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
            image_advtab: true,
            paste_data_images: true,
            templates: [

            ],
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tinymce.com/css/codepen.min.css'
            ],

        });
        $('#mceu_257').click(function () {
            
        });
    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" runat="Server">
</asp:Content>

