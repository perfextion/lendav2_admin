﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_Dashboard : System.Web.UI.Page
{
    string dbKey = "gp_conn";
    protected void Page_Load(object sender, EventArgs e)
    {
        lblchecking.Text = "";
        lblmsg.Text = "";
        if (!IsPostBack)
        {
            if (Session["UserID"] == null)
            {
                Response.Redirect("../Login.aspx");
            }
            else
            {
                string loggedInUser = Session["UserID"].ToString();
                btn_CheckinorCheckout.Text = "Check In";
                string sql_qry = "SELECT loggedInUser,chkin_time,chkout_time,Task_desc,is_active,CURRENT_TIMESTAMP as currentTime from Z_Team_Status where is_active=1 and loggedInUser=" + loggedInUser + " ";

                DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
                if (dt.Rows.Count > 0)
                {
                    string status = dt.Rows[0]["is_active"].ToString();
                    // TimeSpan diffTime = (DateTime.Now - Convert.ToDateTime(dt.Rows[0][1]));
                    TimeSpan diffTime = (Convert.ToDateTime(dt.Rows[0]["currentTime"]) - Convert.ToDateTime(dt.Rows[0]["chkin_time"]));
                    lblCheckinTime.Text = diffTime.Hours + "hr : " + diffTime.Minutes + " min";
                    if (status == "1")
                    {
                        txt_TaskDesc.Text = dt.Rows[0]["Task_desc"].ToString();
                        btn_CheckinorCheckout.Text = "Check Out";
                        divTaskDescriptoin.Visible = true;
                    }
                    else
                    {
                        btn_CheckinorCheckout.Text = "Check In";
                        divTaskDescriptoin.Visible = false;
                    }
                }
                else
                {
                    divTaskDescriptoin.Visible = false;
                }
                lbl_table.Text = LoadTable();
            }
        }

    }
    protected string LoadTable()
    {
        string str_return = "";

        //string str_sql_1 = " select chkin_time, chkout_time, Task_desc, is_active, loggedInUser, username from Z_Team_Status ts"
        //                 + " left join hr_users hu on ts.loggedInUser = hu.userid where is_active=1 ";

        string str_sql_1 = " select  CONVERT(VARCHAR, chkin_time, 111) + ' ' + "
                           + " CONVERT(VARCHAR, DATEPART(hh, chkin_time)) + ':' + "
                           + " RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, chkin_time)), 2) AS chkin_time, "
                           + " CONVERT(VARCHAR, chkout_time, 111) +' ' + "
                           + " CONVERT(VARCHAR, DATEPART(hh, chkout_time)) + ':' + "
                           + " RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, chkout_time)), 2) AS chkout_time,"
                           + " Task_desc, is_active, loggedInUser, username from Z_Team_Status ts "
                           + " left join hr_users hu on ts.loggedInUser = hu.userid where is_active=1 ";

        DataTable dt1 = gen_db_utils.gp_sql_get_datatable(str_sql_1, dbKey);


        str_return += "<div> <table id='tblActiveUsers' class='table table-striped table-bordered' >  "
                                      + "      <thead> "
                              + "     <tr> "
                              + "       <th >Checkin Time</th> "
                              + "       <th >Active User </th> "

                              + "    </tr> "
                              + "      </thead> "
                              + "        <tbody> ";

        foreach (DataRow dtr1 in dt1.Rows)
        {


            str_return += "<tr>"
                              + "<td>" + dtr1["chkin_time"].ToString() + "</td>"
                               + "<td>" + dtr1["username"].ToString() + "</td>"
                             + "</tr>";
        }

        str_return += " </tbody> "
                                + " </table> "
                                  + "</div></div></div >";

        return str_return;

    }

    protected void btn_CheckinorCheckout_Click(object sender, EventArgs e)
    {
        string query = "";
        string loggedInUser = Session["UserID"].ToString();

        if (btn_CheckinorCheckout.Text == "Check In")
        {
            query = "INSERT INTO Z_Team_Status(chkin_time,is_active,loggedInUser)" +
                " VALUES(CURRENT_TIMESTAMP,'" + 1 + "','" + loggedInUser + "')";
            gen_db_utils.gp_sql_execute(query, dbKey);
            btn_CheckinorCheckout.Text = "Check Out";
            divTaskDescriptoin.Visible = true;
            txt_TaskDesc.Text = "";
            lblmsg.Text = "Checked in successfully";
        }
        else if (btn_CheckinorCheckout.Text == "Check Out")
        {
            query = "UPDATE Z_Team_Status SET chkout_time = CURRENT_TIMESTAMP,Task_desc = '" + txt_TaskDesc.Text + "',is_active = '" + 0 + "' where is_active=1 and loggedInUser='" + loggedInUser + "' ";
            gen_db_utils.gp_sql_execute(query, dbKey);
            btn_CheckinorCheckout.Text = "Check In";
            divTaskDescriptoin.Visible = false;
            lblmsg.Text = "Check out successfully";
        }
        ClientScript.RegisterStartupScript(this.GetType(), "alert", "$('#ContentPlaceHolder1_lblmsg').delay(5000).fadeOut('slow');", true);
        lblCheckinTime.Text = "0hr : 0 min";
        lbl_table.Text = LoadTable();
    }

    protected void btnsave_Click(object sender, EventArgs e)
    {
        string query = "";
        string loggedInUser = Session["UserID"].ToString();
        query = "UPDATE Z_Team_Status SET Task_desc = '" + txt_TaskDesc.Text + "' where is_active=1 and loggedInUser='" + loggedInUser + "' ";
        gen_db_utils.gp_sql_execute(query, dbKey);
        lblchecking.Text = "Saved successfully";
        ClientScript.RegisterStartupScript(this.GetType(), "alert", "$('#ContentPlaceHolder1_lblchecking').delay(5000).fadeOut('slow');", true);

    }
}