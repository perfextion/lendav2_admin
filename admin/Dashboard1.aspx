﻿<%@ Page Title="Systhesis | Admin | Dashboard" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="Dashboard1.aspx.cs" Inherits="admin_Dashboard1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .dashboard-menu .row div {
            margin: 10px;
            width: calc(100%/4 - 20px);
            border-radius: 1px;
            height: 191px;
            color: #fff !important;
            font-size: 18px;
            display: flex;
            flex-direction: column;
            justify-content: center;
            cursor: pointer;
        }

        @media (min-width: 768px) {
            .dashboard-menu .row div {
                width: calc(50% - 20px);
            }
        }

        @media (max-width: 768px) {
            .dashboard-menu .row div {
                width: calc(100% - 100px);
                margin-left: 50px;
            }
        }

        @media (min-width: 992px) {
            .dashboard-menu .row div {
                width: calc(100%/4 - 20px);
            }
        }

        .dashboard-menu .row.ref-tables div {
            background-color: rgb(236, 133, 60);
        }

        .dashboard-menu .row.ref-tables div:hover {
            background-color: rgb(248, 103, 1);
        }

        .dashboard-menu .row.loan-tables div {
            background-color: rgb(36, 114, 179);
        }

        .dashboard-menu .row.loan-tables div:hover {
            background-color: rgb(0, 110, 202);
        }
        .dashboard-menu .row span {
            display: block;
            font-size: 14px;
        }

        .icon {
            margin-top: 77px;
            margin-bottom: 10px;
            margin-left: 6px;
            color: #fff !important
        }

        .name {
            margin-top: 80px;
            margin-bottom: 10px;
            margin-left: 18px;
            font-family: Arial,sans-serif;
            font-size: 18px;
        }

        .heig {
            height: 176px;
            border-radius: 5px;
        }

        a {
            color: #f8f8f8;
        }
    </style>

    <div class="container dashboard-menu">
         <div class="row ref-tables">
              <% foreach (var table in ref_dashboard_data){%>
                 <div class="col-md-3 col-sm-6 col-xs-12" onclick="javascript:redirectToPage('<%= table.Table_UI_Path %>')">
                     <span>
                         <%= table.Table_UI_Name %>
                     </span>
                     <span>Last Edited by <%= table.Username %></span>
                     <span><%= table.Modified_On %></span>
                 </div>
             <% } %>             
        </div>
         <div class="row loan-tables">
             <% foreach (var table in loan_dashboard_data){%>
                 <div class="col-md-3 col-sm-6 col-xs-12" onclick="javascript:redirectToPage('<%= table.Table_UI_Path %>')">
                     <span>
                         <%= table.Table_UI_Name %>
                     </span>
                     <span>Last Edited by <%= table.Username %></span>
                     <span><%= table.Modified_On %></span>
                 </div>
             <% } %> 
        </div>
    </div>
   

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" runat="Server">

    <script>
        function redirectToPage(page) {
            let url = '/admin/' + page;
            window.location.href = window.location.origin + url;
        }
    </script>
</asp:Content>

