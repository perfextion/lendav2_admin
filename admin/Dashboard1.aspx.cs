﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

public partial class admin_Dashboard1 : System.Web.UI.Page
{
    private static string dbKey = "gp_conn";
    public List<Dashboard_Model> loan_dashboard_data;
    public List<Dashboard_Model> ref_dashboard_data;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["username"] != null)
        {
            Load_Dashboard_Data();
        }
        else
        {
            Response.Redirect("~/Login.aspx");
        }
    }

    public void Load_Dashboard_Data()
    {
        string query = @"WITH TopRows AS
                        (
	                        SELECT *
		                          ,ROW_NUMBER() OVER
							                        (
								                        PARTITION BY Reference_Table_Ind
								                        ORDER BY Modified_On DESC
         					                        ) AS [ROW NUMBER]
	                          FROM Table_Audit_Trail
                        )
                        SELECT
	                        DBO.GetUserNameByUserID(TR.Modified_By) as [Username],
	                        TR.Table_Name,
	                        TR.Table_UI_Name,
	                        TR.Table_UI_Path,
	                        TR.Reference_Table_Ind,
	                        TR.Modified_By,
	                        FORMAT(TR.Modified_On, 'MM/dd/yyyy hh:mm:ss tt', 'en-US' ) as [Modified_On]
                        FROM TopRows TR
                        WHERE TR.[ROW NUMBER] <= 4";

        DataTable dt = gen_db_utils.gp_sql_get_datatable(query, dbKey);

        loan_dashboard_data = new List<Dashboard_Model>();
        ref_dashboard_data = new List<Dashboard_Model>();

        foreach (var row in dt.AsEnumerable())
        {
            var Dashboard_Model = row.CreateItemFromRow<Dashboard_Model>();
            if (Dashboard_Model.Reference_Table_Ind == 1)
            {
                ref_dashboard_data.Add(Dashboard_Model);
            }
            else
            {
                loan_dashboard_data.Add(Dashboard_Model);
            }
        }
    }
}

public class Dashboard_Model
{
    public string Username { get; set; }
    public string Table_Name { get; set; }
    public string Table_UI_Name { get; set; }
    public string Table_UI_Path { get; set; }
    public int Reference_Table_Ind { get; set; }
    public int Modified_By { get; set; }
    public string Modified_On { get; set; }
}