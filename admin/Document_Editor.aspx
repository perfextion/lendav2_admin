﻿<%@ Page Title="" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="Document_Editor.aspx.cs" Inherits="admin_Document_Editor" ValidateRequest="false" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="../js/tinymce/tinymce.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />

    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/js/materialize.min.js">
    </script>
    <style>
        .chip {
            display: inline-block;
            padding: 0 25px;
            height: 30px;
            font-size: 16px;
            line-height: 30px;
            border-radius: 25px;
            background-color: #f1f1f1;
        }

            .chip img {
                float: left;
                margin: 0 10px 0 -25px;
                height: 30px;
                width: 30px;
                border-radius: 50%;
            }

        .closebtn {
            padding-left: 10px;
            padding-top: 5px;
            color: #888;
            font-weight: normal;
            float: right;
            font-size: 5px;
            cursor: pointer;
        }

            .closebtn:hover {
                color: #000;
            }
    </style>
    <style>
        div.mce-fullscreen {
            z-index: 1050;
        }
    </style>
    <%--  <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>--%>

    <script>
        imgUrl = "http://lenda.tips4spelling.com/uploads";
        tinymce.init({
            selector: 'textarea',
            height: 500,
            theme: 'modern',
            //  plugins: 'autoresize print preview fullpage powerpaste searchreplace autolink directionality advcode visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount tinymcespellchecker a11ychecker imagetools mediaembed  linkchecker contextmenu colorpicker textpattern help',
            plugins: 'code print preview searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount   imagetools    contextmenu colorpicker textpattern help',
            toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
            image_advtab: true,
            paste_data_images: true,
            templates: [
                { title: 'Test template 1', content: 'Test 1' },
                { title: 'Test template 2', content: 'Test 2' }
            ],
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tinymce.com/css/codepen.min.css'
            ],
            file_picker_callback: function (callback, value, meta) {
                if (meta.filetype == 'image') {
                    var inputFile = document.createElement("INPUT");
                    inputFile.setAttribute("type", "file");
                    inputFile.setAttribute("style", "display: none");
                    inputFile.click();
                    inputFile.addEventListener("change", function () {
                        
                        var file = this.files[0];
                        var data = new FormData();
                        var guid = CreateGuid();
                        var retutnFileName

                        $.each(this.files, function (key, value) {

                            data.append(key, value);
                            data.append("fname", guid + value.name);
                            retutnFileName = guid + value.name;
                        });

                        $.ajax({
                            type: "POST",
                            enctype: 'multipart/form-data',
                            url: "FileUploader.ashx",
                            data: data,
                            processData: false,
                            contentType: false,
                            cache: false,
                            async: false,
                            timeout: 600000,
                            success: function (data) {

                                $('.mce-textbox').eq(0).val("http://lenda.tips4spelling.com/uploads/" + retutnFileName);
                                $("#result").text(data);
                                console.log("SUCCESS : ", data);
                                $("#btnSubmit").prop("disabled", false);

                            },
                            error: function (e) {

                                $("#result").text(e.responseText);
                                console.log("ERROR : ", e);
                                $("#btnSubmit").prop("disabled", false);

                            }
                        });

                    });
                }
            },


        });
        $('#mceu_257').click(function () {
            
        });
    </script>

    <div class="row" style="padding-top: 15px; padding-bottom: 10px">
        <div class="form-inline">
            <div class="col-md-6">
                <asp:HiddenField ID="hdndocID" runat="server" />
                <div class="form-group">
                    <label>Page Name :</label>
                    <asp:TextBox CssClass="form-control" ID="txtPageName" runat="server" />
                </div>

                <div class="form-group">
                    <label>Tab Name :</label>
                    <asp:TextBox CssClass="form-control" ID="txtTabName" runat="server" />
                </div>
                <div class="form-group">
                    <asp:Button Text="Save" ID="btnSaveDoc" CssClass="btn btn-info" OnClientClick="removekeyword()" OnClick="btnSaveDoc_Click" runat="server" />
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="padding-top: 10px; padding-bottom: 10px">

        <div class="form-inline">
            <div class="col-md-9">
                <div class="form-group">
                    <label>Keywords :</label>
                    <asp:TextBox CssClass="form-control" ID="txtKeyName" runat="server" />
                </div>
                <div class="form-group">
                    <div id="btnAdd" class="btn btn-info">Add </div>
                </div>
                <div class="form-group">
                    <label for="ddlParent">Parent Selection:</label>
                    <asp:DropDownList runat="server" ID="ddlParent" class="form-control loadscreen">
                    </asp:DropDownList>
                </div>
                <div class="form-group">
                    <label for="ddlChild">Child Selection :</label>
                    <asp:DropDownList runat="server" ID="ddlChild" class="form-control loadscreen">
                    </asp:DropDownList>
                    <%-- <select id="ddlChild" class="form-control loadscreen">
                    </select>--%>
                    <asp:HiddenField ID="hdnddlChild" runat="server" />
                </div>
                <br />
                <ul id="lstKey" class="list-inline"></ul>
            </div>
        </div>
        <asp:HiddenField ID="hdnList" runat="server" />
    </div>
    <asp:TextBox ID="texteditor" TextMode="MultiLine" runat="server" />
    <script>

        $.urlParam = function (name) {
            var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
            if (results == null) {
                return null;
            }
            else {
                return decodeURI(results[1]) || 0;
            }
        }
        
        $(function () {
            var data = JSON.parse($('#ContentPlaceHolder1_hdnList').val());
            $.each(data, function (index, item) {
                $("<li> <div class='chip'><span class='keyword'>" + item.title + "</span><i class='material-icons' >close</i> </div> </li>").appendTo("#lstKey");
            });

        });

        $('#ContentPlaceHolder1_ddlChild').change(function () {
            $('#ContentPlaceHolder1_hdnddlChild').val(this.value);
        });
        $('#ContentPlaceHolder1_ddlParent').change(function () {
            
            if (this.value == "Select") {
                $('#ContentPlaceHolder1_ddlChild').empty();
            }
            else {
                $.ajax({
                    type: "POST",
                    async: false,
                    url: "Document_Editor.aspx/GetChilds",
                    data: JSON.stringify({ id: $('#ContentPlaceHolder1_ddlParent').val() }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (res) {
                        
                        $('#ContentPlaceHolder1_ddlChild').empty();
                        $.each(JSON.parse(res.d), function (data, value) {
                            $("#ContentPlaceHolder1_ddlChild").append($("<option></option>").val(value.id).html(value.Title));
                        })
                    }
                });
            }

        });

        $('#ContentPlaceHolder1_btnSaveDoc').click(function () {
            $('#ContentPlaceHolder1_hdnddlChild').val($('#ContentPlaceHolder1_ddlChild').val());
        });


        $('#btnAdd').click(function () {
            var text = $('#ContentPlaceHolder1_txtKeyName').val();
            if (text.length) {
                // $('<li  />', { html: text, value: text }).appendTo('#lstKey');
                $("<li> <div class='chip'><span class='keyword'>" + text + "</span><i class='material-icons' >close</i> </div> </li>").appendTo("#lstKey");
                $('#ContentPlaceHolder1_txtKeyName').val('');

                var items = [];
                $('#lstKey').find('.keyword').each(function () {
                    items.push({ title: $(this).text() })
                });

                var res = JSON.stringify(items);

                $('#ContentPlaceHolder1_hdnList').val(res);

            }
        });
        
        function removekeyword() {
            var items = [];
            $('#lstKey').find('.keyword').each(function () {
                items.push({ title: $(this).text() })
            });

            var res = JSON.stringify(items);

            $('#ContentPlaceHolder1_hdnList').val(res);
        }

        $('#btnSaveDoc').click(function () {

            var items = [];
            $('#lstKey').find('.keyword').each(function () {
                items.push({ title: $(this).text() })
            });
            var res = JSON.stringify(items);
            $('#hdnList').val(res);
        })

        function CreateGuid() {
            function _p8(s) {
                var p = (Math.random().toString(16) + "000000000").substr(2, 8);
                return s ? "-" + p.substr(0, 4) + "-" + p.substr(4, 4) : p;
            }
            return _p8() + _p8(true) + _p8(true) + _p8();
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" runat="Server">
</asp:Content>

