﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_Document_Editor : System.Web.UI.Page
{
    static string dbKey = "gp_conn";
    int docID = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            docID = Convert.ToInt32(Request.QueryString["DocId"].ToString());
            hdndocID.Value = docID.ToString();
            GetParents();
            if (docID > 0)
            {
                txtPageName.Text = Request.QueryString["pageName"].ToString();
                txtTabName.Text = Request.QueryString["tabName"].ToString();
                GetDocumentBodyByID(docID);
            }

        }
    }

    [WebMethod]
    public static string GetDocumentByID(int docID)
    {
        string sql_qry = "SELECT Doc_Body FROM Z_Documents where Z_Doc_ID=" + docID;
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        if (dt.Rows.Count > 0)
            return dt.Rows[0][0].ToString();
        else
            return null;
    }

    public void GetDocumentBodyByID(int docID)
    {
        // string sql_qry = "SELECT Doc_Body,Key_Fields FROM Z_Documents where Z_Doc_ID=" + docID;
        string sql_qry = "SELECT Doc_Body,Key_Fields,Title,attach_to_id,id,ParentId FROM Z_Documents zd  left join Z_Doc_Index zi on  zd.attach_to_id = zi.Id where Z_Doc_ID=" + docID;
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);

        if (dt.Rows.Count > 0)
        {
            texteditor.Text = dt.Rows[0][0].ToString();
            hdnList.Value = dt.Rows[0][1].ToString();

            string parentID = dt.Rows[0]["ParentId"].ToString();
            string childID = dt.Rows[0]["id"].ToString();
            if (!string.IsNullOrEmpty(parentID))
            {
                ddlParent.SelectedValue = parentID;
                // GetChilds(parentID);
                GetChildDetails(parentID);
                ddlChild.SelectedValue = childID;
            }
        }
    }

    [WebMethod]
    public static bool UpdateDocument(string body, int docID, string pageName, string tabName)
    {
        string sql_qry = "";
        if (docID > 0)
            sql_qry = "update Z_Documents set Doc_Body='" + body + "', Page_Name='" + pageName + "', Tab_Name='" + pageName + "'  where  Z_Doc_ID=" + docID;
        else
            sql_qry = "INSERT INTO Z_Documents(Page_Name,Tab_Name,Doc_Body) VALUES('" + pageName + "','" + tabName + "','" + body + "')";
        string res = gen_db_utils.base_sql_scalar(sql_qry, dbKey);
        return true;
    }


    public void GetParents()
    {
        //string sql_qry = "select id,Title,ParentID from Z_Doc_Index where parentId=0";
        string sql_qry = "SELECT distinct T1.ParentId as id,T2.TITLE as title FROM Z_DOC_INDEX T1, Z_DOC_INDEX T2 where T1.ParentId=T2.id";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        admin_Document_Editor doc = new admin_Document_Editor();
        ddlParent.DataSource = dt;
        ddlParent.DataTextField = "title";
        ddlParent.DataValueField = "id";
        ddlParent.DataBind();
        ddlParent.Items.Insert(0, "Select");
    }


    public void GetChildDetails(string id)
    {
        string sql_qry = "select id,Title,ParentID from Z_Doc_Index where parentId=" + id;
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        ddlChild.DataSource = dt;
        ddlChild.DataTextField = "title";
        ddlChild.DataValueField = "id";
        ddlChild.DataBind();
    }

    [WebMethod]
    public static string GetChilds(string id)
    {
        string sql_qry = "select id,Title,ParentID from Z_Doc_Index where parentId=" + id;
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
    }

    public bool UpdateDocumentDetails(string body, int docID, string pageName, string tabName, string lstData, string strUserName, string childId)
    {
        if (body.Contains("'"))
        {
            body = body.Replace("'", "''");
        }
        string sql_qry = "";
        if (docID > 0)
            sql_qry = "update Z_Documents set Doc_Body='" + body + "', Page_Name='" + pageName + "', Tab_Name='" + tabName + "',Key_Fields='" + lstData + "',Lastmod_by='" + strUserName + "',Lastmod_at=current_timestamp,attach_to_id='" + childId + "'  where  Z_Doc_ID=" + docID;
        else
            sql_qry = "INSERT INTO Z_Documents(Page_Name,Tab_Name,Doc_Body,Key_Fields,Lastmod_by,Lastmod_at,attach_to_id) VALUES('" + pageName + "','" + tabName + "','" + body + "','" + lstData + "','" + strUserName + "',current_timestamp,'" + childId + "');Select @@IDENTITY ";

        string res = gen_db_utils.base_sql_scalar(sql_qry, dbKey);
        if (docID == 0)
        {
            hdndocID.Value = res;
        }
        return true;
    }


    protected void btnSaveDoc_Click(object sender, EventArgs e)
    {
        string listdata = hdnList.Value;
        Label lblUsername = (Label)Page.Master.FindControl("lblUsername");
        string strUserName = lblUsername.Text;
        //bool result = UpdateDocumentDetails(texteditor.Text, Convert.ToInt32(hdndocID.Value), txtPageName.Text, txtTabName.Text, listdata, strUserName, ddlChild.SelectedValue);
        bool result = UpdateDocumentDetails(texteditor.Text, Convert.ToInt32(hdndocID.Value), txtPageName.Text, txtTabName.Text, listdata, strUserName, hdnddlChild.Value);

        if (result == true)
        {
            Response.Redirect("~/admin/Document_View.aspx?DocId= " + Convert.ToInt32(hdndocID.Value) + "&pageName=" + txtPageName.Text + "&tabName=" + txtTabName.Text + "'");
        }
    }

    //protected void ddlParent_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    string id = ddlParent.SelectedValue;
    //    GetChilds(id);
    //}
}


