﻿<%@ Page Title="" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="Document_View.aspx.cs" Inherits="admin_Documents_View" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" />

    <script type="text/ecmascript" src="http://www.guriddo.net/demo/js/trirand/i18n/grid.locale-en.js"></script>
    <script type="text/ecmascript" src="http://www.guriddo.net/demo/js/trirand/src/jquery.jqGrid.js"></script>
    <link rel="stylesheet" type="text/css" href="http://www.guriddo.net/demo/css/trirand/ui.jqgrid-bootstrap.css" />
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
     <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />

    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/js/materialize.min.js">
      </script>
    
    <style>
        .chip {
            display: inline-block;
            padding: 0 25px;
            height: 30px;
            font-size: 16px;
            line-height: 30px;
            border-radius: 25px;
            background-color: #f1f1f1;
        }

            .chip img {
                float: left;
                margin: 0 10px 0 -25px;
                height: 30px;
                width: 30px;
                border-radius: 50%;
            }

        .closebtn {
            padding-left: 10px;
            padding-top: 5px;
            color: #888;
            font-weight: normal;
            float: right;
            font-size: 5px;
            cursor: pointer;
        }

            .closebtn:hover {
                color: #000;
            }
    </style>

    <style>
        .ui-jqgrid .ui-jqgrid-htable th div {
            height: auto;
            overflow: hidden;
            padding-right: 4px;
            padding-top: 2px;
            position: relative;
            vertical-align: text-top;
            white-space: normal !important;
        }
    </style>
    <div class="">
        <div class="panel-body row-padding">
            <div class="row">
                <div style="margin-left: 50px">
                    <table id="jqGrid"></table>
                    <div id="jqGridPager"></div>
                </div>
            </div>
            <ul id="lstKey" class="list-inline"></ul>
            <div id="dvContainer" runat="server">
            </div>
        </div>
        <br />
        <br />
        <br />
    </div>
    <asp:HiddenField ID="hdnList" runat="server" />
    <script type="text/javascript">

        $(document).ready(function () {

            $("#btnAddRow").on("click", function () {
                window.location.href = "/admin/Document_Editor.aspx?DocId=0";
            });

        });
        function copyContent(docid) {
            $.ajax({
                type: "POST",
                url: "Documents.aspx/SaveCopy",
                data: "{docid:" + docid + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    location.reload();
                }
            });
        }



    </script>



</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" runat="Server">
</asp:Content>

