﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_Documents : System.Web.UI.Page
{
    static string dbKey = "gp_conn";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            GetDocumentsDetails();
        }
    }

    [WebMethod]
    public static List<string> GetLogColumnNames()
    {
        string sql_qry = "SELECT Column_Name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Z_Documents'";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);

        List<string> emp = new List<string>();
        emp = (from DataRow row in dt.Rows
               select row["Column_Name"].ToString()
               ).ToList();
        return emp;
    }

    [WebMethod]
    public static string GetDocumentDetails()
    {
        string sql_qry = "SELECT Z_Doc_ID ,Page_Name,Tab_Name,Doc_Body,Lastmod_at,Lastmod_by,status  FROM Z_Documents";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static string GetDocumentSearch(string colName, string serchText)
    {
        string sql_qry = "SELECT Z_Doc_ID ,Page_Name,Tab_Name,Doc_Body,Lastmod_at,Lastmod_by,status  FROM Z_Documents  where " + colName + " like '%" + serchText + "%'  ";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);

        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static bool SaveCopy(int docid)
    {
        string sql_qry = "INSERT INTO Z_Documents(Page_Name,Tab_Name,Doc_Body) SELECT top 1 Page_Name,Tab_Name,Doc_Body  FROM Z_Documents  where Z_Doc_ID=" + docid + "  ";
        string res = gen_db_utils.base_sql_scalar(sql_qry, dbKey);
        return true;
    }
    public void GetDocumentsDetails()
    {
        string sql_qry = "SELECT Z_Doc_ID ,Page_Name,Tab_Name,Doc_Body,Lastmod_at,Lastmod_by,status  FROM Z_Documents";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        BindDocumentDetails(dt);
    }
    public void GetDocumentsSearch(string colName, string serchText)
    {
        if (serchText.Contains("'"))
        {
            serchText = serchText.Replace("'", "''");
        }
        string sql_qry = "";
        if (!String.IsNullOrEmpty(serchText))
            if (String.IsNullOrEmpty(colName))
                sql_qry = "SELECT Z_Doc_ID ,Page_Name,Tab_Name,Doc_Body,Lastmod_at,Lastmod_by,status  FROM Z_Documents  where Page_Name like '%" + serchText + "%' or Tab_Name like '%" + serchText + "%' or Doc_Body like '%" + serchText + "%'  ";
            else
                sql_qry = "SELECT Z_Doc_ID ,Page_Name,Tab_Name,Doc_Body,Lastmod_at,Lastmod_by,status  FROM Z_Documents  where " + colName + " like '%" + serchText + "%'  ";
        else
            sql_qry = "SELECT Z_Doc_ID ,Page_Name,Tab_Name,Doc_Body,Lastmod_at,Lastmod_by,status  FROM Z_Documents ";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        BindDocumentDetails(dt);
    }
    public void BindDocumentDetails(DataTable dt)
    {
        if (dt != null && dt.Rows.Count > 0)
        {
            string docContent = "";
            foreach (DataRow row in dt.Rows)
            {
                docContent = docContent + "<div style='padding-bottom:55px;'><div class='row' style='padding-top:15px'>" +
                                                "<div class='col-md-3'>" +
                                                    "<span>Page Name :  </span>" +
                                                    "<label style='font-size: large' id='lblPageName'>  " + row["Page_Name"] + "</label>" +
                                                "</div>" +
                                                "<div class='col-md-3'>" +
                                                    "<span>Tab Name :  </span>" +
                                                    "<label style='font-size: large' id='lblTabName'>   " + row["Tab_Name"] + "</label> " +
                                                    "<a href='/admin/Document_Editor.aspx?DocId= " + row["Z_Doc_ID"] + "&pageName=" + row["Page_Name"] + "&tabName=" + row["Tab_Name"] + "' >  [edit] </a>" +
                                                    "<a href='#' onclick='copyContent(" + row["Z_Doc_ID"] + ")' >  [copy] </a>" +
                                                "</div> " +
                                            "</div> " +
                                            "<div class='col-md-12' style='padding-bottom:15px;border-style: outset;' id='dvEditorContent'>" + row["Doc_Body"] + " </div>" +
                                            "</div>";
            }
            dvContainer.InnerHtml = docContent;
        }
        else
            dvContainer.InnerHtml = "no documents available...";
    }


    protected void btnSearch_Click(object sender, EventArgs e)
    {
        GetDocumentsSearch(ddlColNames.SelectedValue, txtSearch.Text);
    }


}