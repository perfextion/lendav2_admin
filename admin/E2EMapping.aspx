﻿<%@ Page Title="" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="E2EMapping.aspx.cs" Inherits="admin_E2EMapping" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="http://www.guriddo.net/demo/css/trirand/ui.jqgrid-bootstrap.css" />

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/ecmascript" src="http://www.guriddo.net/demo/js/trirand/i18n/grid.locale-en.js"></script>
    <script type="text/ecmascript" src="http://www.guriddo.net/demo/js/trirand/src/jquery.jqGrid.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <style>
        .ui-jqgrid .ui-jqgrid-htable th div {
            height: auto;
            overflow: hidden;
            padding-right: 4px;
            padding-top: 2px;
            position: relative;
            vertical-align: text-top;
            white-space: normal !important;
        }
    </style>
    <div class="">
        <div class="panel-body row-padding">
            <div class="row">
                <div class="col-md-3" style="padding-left: 51px;">
                    <div id="btnAddRow" class="btn btn-info" style="width: 60px;">
                        add
                    </div>
                    <div id="btnSave" class="btn btn-success" style="width: 60px;">Save</div>
                </div>
                <div class="col-md-3">
                    <div class="form-inline">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-inline">
                        <div class="form-group">
                            <label for="ddlLocalObject">Local Object :</label>
                            <select id="ddlLocalObject" class="form-control loadscreen" style="width: 200px">
                                <option value="">Select</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-inline">
                        <div class="form-group">
                            <label for="txtSearch">Search :</label>
                            <input type="text" class="form-control" name="search" id="txtSearch" style="width: 200px" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="loader"></div>
        <div class="row">
            <div style="margin-left: 50px">
                <table id="jqGrid"></table>
                <div id="jqGridPager"></div>
            </div>
        </div>
    </div>
    <br />
    <br />
    <br />
    <script type="text/javascript">
        var DeleteRows = [];
        var timer;
        var lastSelection;
        $(function () {
            var obj = {
                Init: function () {
                    obj.getGridData();
                    $("#btnAddRow").click(function () {
                        obj.add();
                    });
                    $("#btnSave").click(function () {
                        obj.save();
                    });
                    $("#txtSearch").on("keyup", function () {
                        var self = this;
                        obj.search(self);
                    });
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "E2EMapping.aspx/GetLocalObject",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            
                            $.each(res.d, function (data, value) {
                                $("#ddlLocalObject").append($("<option></option>").val(value).html(value));
                            })
                        }
                    });

                    $(".loadscreen").on("change", function () {
                        obj.filedSearch();
                    });

                },
                getGridData: function () {
                    $.ajax({
                        type: "POST",
                        url: "E2EMapping.aspx/GetDetails",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            $('.loader').show();
                            obj.loadgrid(data.d);
                            $('.loader').hide();
                        }
                    });
                },
                loadgrid: function (data) {
                    $.jgrid.gridUnload("#jqGrid");
                    $("#jqGrid").jqGrid({
                        datatype: "local",
                        styleUI: 'Bootstrap',
                        data: JSON.parse(data),
                        colModel: [
                            { label: 'Id', name: 'Id', width: 35, editable: false },
                            { label: 'Local Object', name: 'Local_object', width: 150, editable: true },
                            { label: 'Local field', name: 'Local_field', width: 150, editable: true },
                            { label: 'FE Objectt', name: 'FE_Object', width: 150, editable: true },
                            { label: 'FE Field', name: 'FE_Field', width: 150, editable: true },
                            { label: 'API Object', name: 'API_Object', width: 150, editable: true },
                            { label: 'API Element', name: 'API_Element', width: 150, editable: true },
                            { label: 'LendaPlus_DB_Table', name: 'LendaPlus_DB_Table', width: 150, editable: true },
                            { label: 'LendaPlus_DB_Field', name: 'LendaPlus_DB_Field', width: 150, editable: true },
                            { label: 'Actionstatus', name: 'Actionstatus', width: 70, editable: false, hidden: true },
                            //{ label: '', name: '', width: 35, formatter: obj.deleteLink },
                        ],
                        viewrecords: true,
                        loadonce: true,
                        restoreAfterSelect: false,
                        saveAfterSelect: true,
                        sortable: true,
                        'cellsubmit': 'clientArray',
                        onSelectRow: obj.edit,
                        onPaging: obj.changePage,
                        height: 'auto',
                        rowNum: 100,
                        pager: "#jqGridPager"
                    });

                    if ($('#txtSearch').val() != "") {
                        $('#txtSearch').trigger('keyup');
                    }


                },
                add: function () {
                    
                    var grid = $("#jqGrid");

                    var rowsperPage = parseInt(grid.getGridParam('rowNum'));
                    var gridlength = grid.jqGrid('getGridParam', 'data').length;
                    var curpage = parseInt($(".ui-pg-input").val());

                    if (rowsperPage * curpage == gridlength) {

                        var row = obj.newrow();
                        var newRowId = grid.jqGrid('getGridParam', 'data').length + 1;
                        grid.jqGrid('addRowData', newRowId, row);
                        grid.trigger('reloadGrid');
                        $('.glyphicon-step-forward').trigger('click');

                    } else {

                        $('.glyphicon-step-forward').trigger('click');
                        var row = obj.newrow();
                        var newRowId = grid.jqGrid('getGridParam', 'data').length + 1;
                        grid.jqGrid('addRowData', newRowId, row);
                    }


                    lastSelection = newRowId;
                    grid.jqGrid('saveRow', lastSelection);
                    grid.jqGrid('restoreRow', lastSelection);

                    var eid = $('.inline-edit-cell').parent().parent().prop('id')
                    grid.jqGrid('saveRow', eid);
                    grid.jqGrid('restoreRow', eid);
                    grid.jqGrid('editRow', newRowId);
                },
                edit: function (id) { 
                    
                    if (id && id !== lastSelection) {
                        var grid = $("#jqGrid");
                        var currentindex, prevpage;
                        var rowsperPage = parseInt(grid.getGridParam('rowNum'));
                        var curpage = parseInt($(".ui-pg-input").val());

                        if (curpage > 1) {
                            prevpage = curpage - 1;
                            currentindex = rowsperPage * prevpage + parseInt(lastSelection) - 1
                        } else {
                            currentindex = lastSelection;
                        }

                        if (lastSelection != undefined) {

                            grid.jqGrid('saveRow', lastSelection);
                            grid.jqGrid('restoreRow', lastSelection);
                            var row = grid.jqGrid('getRowData', lastSelection);

                            var dataobj = jQuery("#jqGrid").jqGrid('getGridParam', 'data');
                            index = dataobj.findIndex(x => x.Id == row.Id);

                            if (row.Id > 0) {
                                row.Actionstatus = 2;
                                grid.jqGrid('getGridParam', 'data')[index] = row;
                            }
                        }
                        grid.jqGrid('editRow', id);
                        lastSelection = id;
                    }
                },
                newrow: function () {
                    var length = jQuery("#jqGrid").jqGrid('getGridParam', 'records');
                    var newid = length + 1;
                    var row = {
                        Actionstatus: 1,
                        Id: 0,
                        Local_object: "",
                        Local_field: "",
                        FE_Object: "",
                        FE_Field: "",
                        API_Object: "",
                        API_Element: "",
                        LendaPlus_DB_Table: "",
                        LendaPlus_DB_Table: "",
                        LendaPlus_DB_Field: "",

                    };
                    return row;
                },
                save: function () {
                    
                    $('.loader').show();
                    var grid = $("#jqGrid");
                    var allrows = [];
                    var dataobj = grid.jqGrid('getGridParam', 'data');

                    // var id = grid.jqGrid('getGridParam', 'selrow');
                    var id = $('.inline-edit-cell').parent().parent().prop('id');
                    grid.jqGrid('saveRow', id);

                    var rowData = grid.jqGrid('getRowData', id);
                    index = dataobj.findIndex(x => x.Id == rowData.Id);
                    if (rowData.Id > 0) {
                        rowData.Actionstatus = 2;
                        grid.jqGrid('getGridParam', 'data')[index] = rowData;
                    }

                    for (var i = 0; i < DeleteRows.length; i++) {
                        DeleteRows[i].Actionstatus = 3
                        allrows.push(DeleteRows[i]);
                    }

                    var newRows = $.grep(dataobj, function (e) { return e.Actionstatus == 1 || e.Actionstatus == 2 });
                    for (var i = 0; i < newRows.length; i++) {
                        allrows.push(newRows[i]);
                    }

                    $.ajax({
                        type: "POST",
                        url: "E2EMapping.aspx/SaveSections",
                        data: JSON.stringify({ lst: allrows }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            // location.reload();

                            $("#ddlLocalObject").val("");
                            // $("#ddlSectionNames").val("");
                            obj.getGridData();
                        },
                        failure: function (response) {
                            alert(response.d);
                        }

                    });
                },
                search: function (self) {
                    if (timer) { clearTimeout(timer); }
                    timer = setTimeout(function () {
                        $("#jqGrid").jqGrid('filterInput', self.value);
                    }, 0);
                },
                filedSearch: function () {

                    var postData = $("#jqGrid").jqGrid("getGridParam", "postData"),
                        colModel = $("#jqGrid").jqGrid("getGridParam", "colModel"),
                        rules = [],
                        searchText = $("#ddlLocalObject").val(),
                        l = colModel.length,
                        i,
                        cm;
                    if (searchText != "") {
                        rules.push({ field: "Local_object", op: "cn", data: searchText });
                    }                    
                    postData.filters = JSON.stringify({
                        groupOp: "OR",
                        rules: rules
                    });

                    $("#jqGrid").jqGrid("setGridParam", { search: true });
                    $("#jqGrid").trigger("reloadGrid", [{ page: 1, current: true }]);
                    // return false;

                },
                changePage: function () {
                    
                    var grid = $("#jqGrid");
                    var id = $('.inline-edit-cell').parent().parent().prop('id');

                    var currentindex, prevpage;
                    var rowsperPage = parseInt(grid.getGridParam('rowNum'));
                    var curpage = parseInt($(".ui-pg-input").val());


                    if (curpage > 1) {
                        prevpage = curpage - 1;
                        currentindex = rowsperPage * prevpage + parseInt(id) - 1
                    } else {
                        currentindex = id;
                    }

                    grid.jqGrid('saveRow', id);
                    grid.jqGrid('restoreRow', id);

                    var rowData = grid.jqGrid('getRowData', id);
                    if (rowData.Id > 0) {
                        rowData.Actionstatus = 2;
                        grid.jqGrid('getGridParam', 'data')[currentindex] = rowData;
                    }
                },
                deleteLink: function (cellValue, options, rowdata, action) {
                    return "<a href='javascript:deleteRecord(" + options.rowId + ")' class='glyphicon glyphicon-trash' style='color:red'></a>";
                }
            }
            obj.Init();

        });


        function deleteRecord(id) {
            
            lastSelection = id;
            var grid = $('#jqGrid');
            var result = confirm("Are you sure you Want to delete?");

            if (result == true) {

                var ediId = grid.jqGrid('getGridParam', 'selrow');
                grid.jqGrid('saveRow', ediId);

                var rowData = grid.jqGrid('getRowData', ediId);
                if (rowData.Id > 0) {
                    rowData.Actionstatus = 2;
                    grid.jqGrid('getGridParam', 'data')[ediId] = rowData;
                }

                var row = grid.jqGrid('getRowData', id);
                grid.jqGrid('delRowData', id);

                var rowsperPage = parseInt(grid.getGridParam('rowNum'));
                var gridlength = grid.jqGrid('getGridParam', 'data').length;
                var curpage = parseInt($(".ui-pg-input").val());

                if (curpage > 1) {
                    if (gridlength == rowsperPage * (curpage - 1)) {
                        $('#prev_jqGridPager').trigger('click');
                    }
                }
                if (row.Id > 0) {
                    DeleteRows.push(row);
                }
                // grid.trigger('reloadGrid');


            }
        }
        function unique(list) {
            var result = [];
            $.each(list, function (i, e) {
                if ($.inArray(e, result) == -1) result.push(e);
            });
            return result;
        }



    </script>


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" runat="Server">
</asp:Content>

