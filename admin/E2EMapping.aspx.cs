﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_E2EMapping : System.Web.UI.Page
{

    static string dbKey = "gp_conn";
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    [WebMethod]
    public static string GetDetails()
    {
        string sql_qry = "select Id,Local_object, Local_field, FE_Object, FE_Field, API_Object, API_Element, LendaPlus_DB_Table, LendaPlus_DB_Field from Z_DB_Mapping where 0=0 ";

        sql_qry = sql_qry + " order by Id asc ";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
    }
    [WebMethod]
    public static List<string> GetLocalObject()
    {
        string sql_qry = "SELECT distinct Local_object FROM Z_DB_Mapping order by Local_object";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);

        List<string> emp = new List<string>();
        emp = (from DataRow row in dt.Rows
               select row["Local_object"].ToString()
               ).ToList();
        return emp;
    }
    [WebMethod]
    public static void SaveSections(dynamic lst)
    {
        foreach (var item in lst)
        {
            string qry = "";
            int val = Convert.ToInt32(item["Actionstatus"]);
            if (val == 1)
            {
                qry = "INSERT INTO Z_DB_Mapping(Local_object, Local_field, FE_Object, FE_Field, API_Object, API_Element, LendaPlus_DB_Table, LendaPlus_DB_Field)" +
                    " VALUES('" + ManageQuotes(item["Local_object"]) + "','" + ManageQuotes(item["Local_field"]) + "','" + ManageQuotes(item["FE_Object"]) + "','" + ManageQuotes(item["FE_Field"]) +
                    "','" + ManageQuotes(item["API_Object"]) + "','" + ManageQuotes(item["API_Element"]) + "','" + ManageQuotes(item["LendaPlus_DB_Table"]) + "','" + ManageQuotes(item["LendaPlus_DB_Field"]) + "')";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 2)
            {
                qry = "UPDATE Z_DB_Mapping SET " +
                    "Local_object = '" + ManageQuotes(item["Local_object"]) + "' ," +
                    "Local_field = '" + ManageQuotes(item["Local_field"]) + "' ," +
                    "FE_Object = '" + ManageQuotes(item["FE_Object"]) + "' ," +
                    "FE_Field = '" + ManageQuotes(item["FE_Field"]) + "' ," +
                    "API_Object = '" + ManageQuotes(item["API_Object"]) + "' ," +
                    "API_Element = '" + ManageQuotes(item["API_Element"]) + "' ," +
                    "LendaPlus_DB_Table = '" + ManageQuotes(item["LendaPlus_DB_Table"]) + "' ," +
                    "LendaPlus_DB_Field = '" + ManageQuotes(item["LendaPlus_DB_Field"]) + "'" +
                    " where Id=" + item["Id"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 3)
            {
                qry = "DELETE FROM Z_DB_Mapping  where Id=" + item["Id"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }

        }
    }
    public static string ManageQuotes(string value)
    {
        if (value != null && value.Contains("'"))
        {
            value = value.Replace("'", "''");
        }
        return value;
    }


}