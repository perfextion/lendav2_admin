﻿<%@ Page Title="" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="Enums.aspx.cs" Inherits="admin_Enums" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href='../css/fullcalendar.min.css' rel='stylesheet' />
    <link href='../css/fullcalendar.print.min.css' rel='stylesheet' media='print' />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" />
    <script src='../js/moment.min.js'></script>
    <script src='../js/fullcalendar/jquery.min.js'></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src='../js/fullcalendar/fullcalendar.min.js'></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


    <div class="col-md-12 col-sm-4" style="margin-top: 10px">
        <div class="col-md-4">
            <div class="form-inline">
                <div class="form-group" style="margin-left: 30px; margin-bottom: 30PX;">
                    <label for="lblofficenames">Enums :</label>
                    <select id="ddlenums" class="form-control" style="width: 200px">
                        <option value="0">Select</option>
                        <option value="1">Preferred_Contacts1</option>
                        <option value="2">Preferred_Contacts2</option>
                        <option value="3">Status</option>
                        <option value="4">Budget_Expence_Type</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="container" style="padding-top: 25px;">
        <div class="form-horizontal">
            <div class="row">
                <div class="col-md-12">
                    <asp:Label ID="lbl_table" runat="server" Text="lbl_table"></asp:Label>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(function () {
            var obj = {
                Init: function () {
                    $('#ddlenums').change(function () {
                        var enums = $('#ddlenums').val();
                        $('#ContentPlaceHolder1_lbl_table').text('');
                        obj.enums(enums);
                    });
                },
                enums: function (enums) {
                    $('.loader').show();
                    $.ajax({
                        type: "POST",
                        url: "Enums.aspx/LoadTable",
                        data: JSON.stringify({ enumsli: enums }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            $('#ContentPlaceHolder1_lbl_table').html(res.d);
                            $('.loader').hide();
                        }
                    });
                },
            }
            obj.Init();
        });
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" runat="Server">
</asp:Content>

