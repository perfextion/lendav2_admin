﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_Enums : System.Web.UI.Page

{
    static Globalvar.Preferred_Contacts precont;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            lbl_table.Text = LoadTable("");
        }
    }
    [WebMethod]
    public static string LoadTable(string enumsli)
    {
        DataTable dt = new DataTable();
        if (enumsli == "1")
        {
            dt = EnumToDataTable(typeof(Globalvar.Preferred_Contacts));
        }
        else if (enumsli == "2")
        {
            dt = EnumToDataTable(typeof(Globalvar.Preferred_Contacts2));
        }
        else if (enumsli == "3")
        {
            dt = EnumToDataTable(typeof(Globalvar.Status));
        }
        else if (enumsli == "4")
        {
            dt = EnumToDataTable(typeof(Globalvar.Budget_Expences));
        }
        else
        {
            dt.Rows.Clear();
        }
        return GeneteateGrid(dt);
    }
    public static DataTable EnumToDataTable(Type enumType)
    {
        DataTable table = new DataTable();
        table.Columns.Add("Desc", typeof(string));
        table.Columns.Add("Id", Enum.GetUnderlyingType(enumType));
        foreach (string name in Enum.GetNames(enumType))
        {
            table.Rows.Add(name.Replace('_', ' '), Enum.Parse(enumType, name));
        }
        return table;
    }
    [WebMethod]
    public static string GeneteateGrid(DataTable dt)
    {
        string str_return = "";
        if (dt != null && dt.Rows.Count > 0)
        {
            str_return += "<div> <table id='tblActiveUsers' class='table table-striped table-bordered' style='width: 500px;'>  "
                                             + "<thead> "
                                 + "     <tr> "
                                      + "       <th >Desc</th> "
                                      + "       <th >Id</th> "
                                 + "    </tr> "
                                 + "      </thead> "
                                 + "        <tbody> ";

            foreach (DataRow dtr1 in dt.Rows)
            {
                str_return += "<tr>"
                               + "<td>" + dtr1["Desc"].ToString() + "</td>"
                               + "<td>" + dtr1["Id"].ToString() + "</td>"
                               + "</tr>";
            }

            str_return += "</tbody> "
                                    + " </table> "
                                      + "</div></div></div >";

        }
        else
        {
            str_return = "    No data available...";
        }
        return str_return;
    }
}