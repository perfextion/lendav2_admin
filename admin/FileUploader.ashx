﻿<%@ WebHandler Language="C#" Class="FileUploader" %>

using System;
using System.Web;
using System.Net;
using System.IO;
using System.Text;

public class FileUploader : IHttpHandler {
    
    public void ProcessRequest(HttpContext context)
    {
        string siteUrl = "ftp://lenda.tips4spelling.com/wwwroot/uploads/";
        string fileName = context.Request.Form["fname"];

        Stream streamObj = context.Request.Files[0].InputStream;
        Byte[] buffer = new Byte[context.Request.Files[0].ContentLength];
        streamObj.Read(buffer, 0, buffer.Length);
        streamObj.Close();
        streamObj = null;

        using (WebClient client = new WebClient())
        {
            client.Credentials = new NetworkCredential("suresh_lenda", "Bruins$123");
            client.BaseAddress = siteUrl;
            client.UploadData(fileName, buffer);
        }
        

    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}