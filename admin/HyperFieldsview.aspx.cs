﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_HyperFieldsview : System.Web.UI.Page
{
    static string dbKey = "gp_conn";
    string loan_full_id = "0";
    protected void Page_Load(object sender, EventArgs e)
    {
        // string loan_full_id = Request.QueryString.GetValues("loan_full_id") == null ? "000101-000" : (Request.QueryString.GetValues("loan_full_id")[0].ToString());
        lbl_table.Text = LoadTable();
    }
    public static string LoadTable()
    {
        string str_sql = "";
        str_sql = "select HyperField_ID,HyperField_Tablename,HyperField_Name,HyperField_Expression,exc.Exception_ID," +
            "Exception_ID_Text,Exception_ID_Level,Threshold_Operative,Threshold_Value  from Ref_HyperFields hyp " +
            "join Ref_Exception exc on hyp.Exception_ID=exc.Exception_ID " +
            "join Ref_Exception_Level lev on lev.Exception_ID = exc.Exception_ID order by hyperfield_ID";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(str_sql, dbKey);
        str_sql = "";
        str_sql = "select distinct HyperField_ID, HyperField_Tablename,HyperField_Name,HyperField_Expression," +
            "exc.Exception_ID,Exception_ID_Text from Ref_HyperFields hyp " +
            "join Ref_Exception exc on hyp.Exception_ID = exc.Exception_ID " +
            "join Ref_Exception_Level lev on lev.Exception_ID = exc.Exception_ID order by hyperfield_ID";
        DataTable dt1 = gen_db_utils.gp_sql_get_datatable(str_sql, dbKey);
        return GeneteateGrid(dt,dt1);
        
    }
    [WebMethod]
    public static string GeneteateGrid(DataTable dt, DataTable dt1)
    {
        string str_return = "";
        if (dt != null && dt.Rows.Count > 0)
        {
            str_return += "<div> <table id='tblActiveUsers' class='table table-striped table-bordered' >  "
                                 + "<thead> "
                                 + "     <tr> "
                                      + "<th>Hyper Field Details</th> "
                                 + "    </tr> "
                                 + "      </thead> "
                                 + "        <tbody> ";

            foreach (DataRow dtr1 in dt1.Rows)
            {
                string level = "";
                string hyperid = dtr1["HyperField_ID"].ToString();
                foreach (DataRow dtr2 in dt.Rows)
                {
                    if (hyperid == dtr2["HyperField_ID"].ToString())
                    {
                        level += "<br>Level:" + dtr2["Exception_ID_Level"].ToString() + " " + dtr2["Threshold_Operative"].ToString() + " " + dtr2["Threshold_Value"].ToString();
                    }
                }
                str_return += "<tr>"
                         + "<td>Hyper Field : " + dtr1["HyperField_Tablename"].ToString() + "." + dtr1["HyperField_Name"].ToString() +
                          "<br>Expression :" + dtr1["HyperField_Expression"].ToString() +
                          "<br>Triggers :Exception_ID:(" + dtr1["Exception_ID"].ToString() + ")" + dtr1["Exception_ID_Text"].ToString() 
                           + level + "</td>"
                           //"<br>Level:" + dtr1["Threshold_Operative"].ToString() + " " + dtr1["Threshold_Value"].ToString() + "</td>"
                           + "</tr>";
            }

            str_return += " </tbody> "
                                    + " </table> "
                                      + "</div></div></div >";
        }
        else
        {
            str_return = "    No data available...";
        }
        return str_return;
    }
}