﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_InsurancePlanActurial : BasePage
{
    public admin_InsurancePlanActurial()
    {
        Table_Name = "Ref_Ins_Plan_Actuarial";
    }

    [WebMethod]
    public static string GetTableInfo()
    {
        return Get_Table_Info();
    }

    [WebMethod]
    public static string LoadTable()
    {
        string sql_qry = @"SELECT *, 0 as [Actionstatus] from Ref_Ins_Plan_Actuarial";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static void SaveTable(dynamic ListItems)
    {
        foreach (var item in ListItems)
        {
            string qry = string.Empty;
            int val = Convert.ToInt32(item["Actionstatus"]);

            if (val == 1)
            {
                qry = @"INSERT INTO [dbo].[Ref_Ins_Plan_Actuarial]
                               ([State_ID]
                               ,[County_ID]
                               ,[Hail_Actuarial_Percent]
                               ,[Hail_Wind_Acturial_Percent]
                               ,[Status])
                         VALUES
                               (
                                    '" + TrimSQL(item["State_ID"]) + @"'" +
                                   ",'" + TrimSQL(item["County_ID"]) + @"'" +
                                   ",'" + item["Hail_Actuarial_Percent"] + @"'" +
                                   ",'" + item["Hail_Wind_Acturial_Percent"] + @"'" +
                                   ",'" + item["Status"] + @"'
                                )  ";

                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 2)
            {
                qry = @"UPDATE [dbo].[Ref_Ins_Plan_Actuarial]
                           SET  [State_ID] = '" + TrimSQL(item["State_ID"]) + @"' " +
                              ",[County_ID] = '" + TrimSQL(item["County_ID"]) + @"' " +
                              ",[Hail_Actuarial_Percent] = '" + item["Hail_Actuarial_Percent"] + @"' " +
                              ",[Hail_Wind_Acturial_Percent] = '" + item["Hail_Wind_Acturial_Percent"] + @"' " +
                              ",[Status] = '" + item["Status"] + @"' " +
                         "WHERE Ins_Plan_Actuarial_ID = '" + item["Ins_Plan_Actuarial_ID"] + "' ";

                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 3)
            {
                qry = "DELETE FROM Ref_Ins_Plan_Actuarial  where Ins_Plan_Actuarial_ID = '" + item["Ins_Plan_Actuarial_ID"] + "' ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
        }

        if (ListItems != null)
        {
            Update_Table_Audit();
        }
    }
}