﻿<%@ Page Title="Insurance Validation Rules" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="InsuranceValidation.aspx.cs" Inherits="admin_InsuranceValidation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        input[type="text"] {
            width: 200px;
        }

        /*input[type="text"] {
            width: 200px;
        }*/
        input.wid100 {
            width: 70px;
        }

        table {
            border-collapse: collapse;
        }

        table, td, th {
            border: 1px solid black;
        }

        tr {
            text-align: center;
        }

        #content {
            padding: 14px 35px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
      <div class="row switch-controls">
        <div class="switch-control disabled" id="editControl">
            <label for="editBtn">Edit</label>
            <div class="toggle-btn small">            
                <input type="checkbox" id="editBtn" class="cb-value" />
                <span class="round-btn"></span>
            </div>
        </div>
        <div class="switch-control disabled" id="enableControl">
            <label for="enableBtn">Enable</label>
            <div class="toggle-btn small">            
                <input type="checkbox" id="enableBtn" class="cb-value" />
                <span class="round-btn"></span>
            </div>
        </div>
    </div>

    <div class="container1">
        <div style="overflow-x: auto; overflow-y: auto; height: 525px">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th rowspan="2" style="text-align: center;" class="secure-code">Ins Plan</th>
                        <th rowspan="2" style="text-align: center;">Ins Plan Type</th>
                        <th rowspan="2" style="text-align: center;">Unit</th>
                        <th rowspan="2" style="text-align: center;">Option</th>
                        <th rowspan="2" style="text-align: center;">Upper %</th>
                        <th rowspan="2" style="text-align: center;">Lower %</th>
                        <th rowspan="2" style="text-align: center;">Range Max</th>
                        <th rowspan="2" style="text-align: center;">Area Yield </th>
                        <th rowspan="2" style="text-align: center;">Yield %</th>
                        <th rowspan="2" style="text-align: center;">Price %</th>
                        <th colspan="2" style="text-align: center;">Liability Max                          
                        </th>
                        <th rowspan="2" style="text-align: center;">Deduct % Max</th>
                        <th colspan="4" style="text-align: center;">Deduct Unit </th>
                        <th rowspan="2" style="text-align: center;">Late Deduct Unit </th>
                        <th rowspan="2" style="text-align: center;">FCMC </th>
                        <th rowspan="2" style="text-align: center;">AIP</th>
                    </tr>
                    <tr>
                        <th style="text-align: center;">Upper % <= 75% </th>
                        <th style="text-align: center;">Upper % <= 85% </th>
                        <th style="text-align: center;">Corn </th>
                        <th style="text-align: center;">Soybeans </th>
                        <th style="text-align: center;">Rice </th>
                        <th style="text-align: center;">Wheat  </th>

                    </tr>
                </thead>
                <tbody>
                    <%--MPCI CAT--%>
                    <tr style="text-align: center;">
                        <td>MPCI</td>
                        <td>CAT</td>
                        <td>
                            <input type="text" id="MPCI_CAT_UNIT" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_CAT_OPTION" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_CAT_UPPER" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_CAT_LOWER" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_CAT_RANGEMAX" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_CAT_AREAYIELD" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_CAT_YIELD" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_CAT_PRICE" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_CAT_LIABILITYMAX75" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_CAT_LIABILITYMAX85" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_CAT_DEDUCTMAX" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_CAT_DEDUCTUNITCORN" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_CAT_DEDUCTUNITSOYBEANS" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_CAT_DEDUCTUNITRICE" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_CAT_DEDUCTUNITWHEAT" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_CAT_LATEDEDUCT" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_CAT_FCMC" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_CAT_AIP" class="form-control savetbox" />
                        </td>
                    </tr>
                    <%--MPCI YP--%>
                    <tr style="text-align: center;">
                        <td>MPCI</td>
                        <td>YP</td>
                        <td>
                            <input type="text" id="MPCI_YP_UNIT" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_YP_OPTION" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_YP_UPPER" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_YP_LOWER" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_YP_RANGEMAX" class="form-control savetbox wid100" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_YP_AREAYIELD" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_YP_YIELD" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_YP_PRICE" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_YP_LIABILITYMAX75" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_YP_LIABILITYMAX85" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_YP_DEDUCTMAX" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_YP_DEDUCTUNITCORN" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_YP_DEDUCTUNITSOYBEANS" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_YP_DEDUCTUNITRICE" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_YP_DEDUCTUNITWHEAT" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_YP_LATEDEDUCT" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_YP_FCMC" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_YP_AIP" class="form-control savetbox" />
                        </td>
                    </tr>
                    <%--MPCI RP-HPE--%>
                    <tr style="text-align: center;">
                        <td>MPCI</td>
                        <td>RP-HPE</td>
                        <td>
                            <input type="text" id="MPCI_RP-HPE_UNIT" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_RP-HPE_OPTION" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_RP-HPE_UPPER" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_RP-HPE_LOWER" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_RP-HPE_RANGEMAX" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_RP-HPE_AREAYIELD" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_RP-HPE_YIELD" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_RP-HPE_PRICE" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_RP-HPE_LIABILITYMAX75" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_RP-HPE_LIABILITYMAX85" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_RP-HPE_DEDUCTMAX" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_RP-HPE_DEDUCTUNITCORN" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_RP-HPE_DEDUCTUNITSOYBEANS" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_RP-HPE_DEDUCTUNITRICE" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_RP-HPE_DEDUCTUNITWHEAT" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_RP-HPE_LATEDEDUCT" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_RP-HPE_FCMC" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_RP-HPE_AIP" class="form-control savetbox" />
                        </td>
                    </tr>
                    <%--MPCI_RP--%>
                    <tr>
                        <td>MPCI</td>
                        <td>RP</td>
                        <td>
                            <input type="text" id="MPCI_RP_UNIT" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_RP_OPTION" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_RP_UPPER" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_RP_LOWER" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_RP_RANGEMAX" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_RP_AREAYIELD" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_RP_YIELD" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_RP_PRICE" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_RP_LIABILITYMAX75" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_RP_LIABILITYMAX85" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_RP_DEDUCTMAX" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_RP_DEDUCTUNITCORN" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_RP_DEDUCTUNITSOYBEANS" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_RP_DEDUCTUNITRICE" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_RP_DEDUCTUNITWHEAT" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_RP_LATEDEDUCT" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_RP_FCMC" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_RP_AIP" class="form-control savetbox" />
                        </td>

                    </tr>
                    <%--MPCI ARH--%>
                    <tr>
                        <td>MPCI</td>
                        <td>ARH</td>
                        <td>
                            <input type="text" id="MPCI_ARH_UNIT" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_ARH_OPTION" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_ARH_UPPER" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_ARH_LOWER" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_ARH_RANGEMAX" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_ARH_AREAYIELD" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_ARH_YIELD" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_ARH_PRICE" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_ARH_LIABILITYMAX75" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_ARH_LIABILITYMAX85" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_ARH_DEDUCTMAX" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_ARH_DEDUCTUNITCORN" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_ARH_DEDUCTUNITSOYBEANS" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_ARH_DEDUCTUNITRICE" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_ARH_DEDUCTUNITWHEAT" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_ARH_LATEDEDUCT" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_ARH_FCMC" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="MPCI_ARH_AIP" class="form-control savetbox" />
                        </td>

                    </tr>
                    <%--WFRP--%>
                    <tr>
                        <td>WFRP</td>
                        <td>-</td>
                        <td>
                            <input type="text" id="WFRP_UNIT" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="WFRP_OPTION" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="WFRP_UPPER" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="WFRP_LOWER" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="WFRP_RANGEMAX" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="WFRP_AREAYIELD" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="WFRP_YIELD" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="WFRP_PRICE" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="WFRP_LIABILITYMAX75" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="WFRP_LIABILITYMAX85" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="WFRP_DEDUCTMAX" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="WFRP_DEDUCTUNITCORN" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="WFRP_DEDUCTUNITSOYBEANS" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="WFRP_DEDUCTUNITRICE" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="WFRP_DEDUCTUNITWHEAT" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="WFRP_LATEDEDUCT_UNIT" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="WFRP_FCMC" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="WFRP_AIP" class="form-control savetbox" />
                        </td>
                    </tr>
                    <%--STAX--%>
                    <tr>
                        <td>STAX</td>
                        <td>-</td>
                        <td>
                            <input type="text" id="STAX_UNIT" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="STAX_OPTION" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="STAX_UPPER" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="STAX_LOWER" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="STAX_RANGEMAX" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="STAX_AREAYIELD" class="form-control savetbox wid100" />
                        </td>
                        <td>
                            <input type="text" id="STAX_YIELD" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="STAX_PRICE" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="STAX_LIABILITYMAX75" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="STAX_LIABILITYMAX85" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="STAX_DEDUCTMAX" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="STAX_DEDUCTUNITCORN" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="STAX_DEDUCTUNITSOYBEANS" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="STAX_DEDUCTUNITRICE" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="STAX_DEDUCTUNITWHEAT" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="STAX_LATEDEDUCT_UNIT" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="STAX_FCMC" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="STAX_AIP" class="form-control savetbox" />
                        </td>
                    </tr>
                    <%--SCO--%>
                    <tr>
                        <td>SCO</td>
                        <td>-</td>
                        <td>
                            <input type="text" id="SCO_UNIT" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="SCO_OPTION" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="SCO_UPPER" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="SCO_LOWER" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="SCO_RANGEMAX" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="SCO_AREAYIELD" class="form-control savetbox wid100" />
                        </td>
                        <td>
                            <input type="text" id="SCO_YIELD" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="SCO_PRICE" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="SCO_LIABILITYMAX75" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="SCO_LIABILITYMAX85" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="SCO_DEDUCTMAX" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="SCO_DEDUCTUNITCORN" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="SCO_DEDUCTUNITSOYBEANS" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="SCO_DEDUCTUNITRICE" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="SCO_DEDUCTUNITWHEAT" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="SCO_LATEDEDUCT_UNIT" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="SCO_FCMC" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="SCO_AIP" class="form-control savetbox" />
                        </td>
                    </tr>
                    <%--hmax standard--%>
                    <tr>
                        <td>Hmax</td>
                        <td>Standard</td>
                        <td>
                            <input type="text" id="HMAX_STANDARD_UNIT" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="HMAX_STANDARD_OPTION" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="HMAX_STANDARD_UPPER" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="HMAX_STANDARD_LOWER" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="HMAX_STANDARD_RANGEMAX" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="HMAX_STANDARD_AREAYIELD" class="form-control savetbox wid100 " />
                        </td>
                        <td>
                            <input type="text" id="HMAX_STANDARD_YIELD" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="HMAX_STANDARD_PRICE" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="HMAX_STANDARD_LIABILITYMAX75" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="HMAX_STANDARD_LIABILITYMAX85" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="HMAX_STANDARD_DEDUCTMAX" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="HMAX_STANDARD_DEDUCTUNITCORN" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="HMAX_STANDARD_DEDUCTUNITSOYBEANS" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="HMAX_STANDARD_DEDUCTUNITRICE" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="HMAX_STANDARD_DEDUCTUNITWHEAT" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="HMAX_STANDARD_LATEDEDUCT_UNIT" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="HMAX_STANDARD_FCMC" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="HMAX_STANDARD_AIP" class="form-control savetbox disabled" />
                        </td>
                    </tr>
                    <%--hmax x1--%>
                    <tr>
                        <td>Hmax</td>
                        <td>X1</td>
                        <td>
                            <input type="text" id="HMAX_X1_UNIT" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="HMAX_X1_OPTION" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="HMAX_X1_UPPER" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="HMAX_X1_LOWER" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="HMAX_X1_RANGEMAX" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="HMAX_X1_AREAYIELD" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="HMAX_X1_YIELD" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="HMAX_X1_PRICE" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="HMAX_X1_LIABILITYMAX75" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="HMAX_X1_LIABILITYMAX85" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="HMAX_X1_DEDUCTMAX" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="HMAX_X1_DEDUCTUNITCORN" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="HMAX_X1_DEDUCTUNITSOYBEANS" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="HMAX_X1_DEDUCTUNITRICE" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="HMAX_X1_DEDUCTUNITWHEAT" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="HMAX_X1_LATEDEDUCT_UNIT" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="HMAX_X1_FCMC" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="HMAX_X1_AIP" class="form-control savetbox disabled" />
                        </td>
                    </tr>
                    <%--  hmax  MaxRP--%>
                    <tr>
                        <td>Hmax</td>
                        <td>MAXRP</td>
                        <td>
                            <input type="text" id="HMAX_MAXRP_UNIT" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="HMAX_MAXRP_OPTION" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="HMAX_MAXRP_UPPER" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="HMAX_MAXRP_LOWER" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="HMAX_MAXRP_RANGEMAX" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="HMAX_MAXRP_AREAYIELD" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="HMAX_MAXRP_YIELD" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="HMAX_MAXRP_PRICE" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="HMAX_MAXRP_LIABILITYMAX75" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="HMAX_MAXRP_LIABILITYMAX85" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="HMAX_MAXRP_DEDUCTMAX" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="HMAX_MAXRP_DEDUCTUNITCORN" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="HMAX_MAXRP_DEDUCTUNITSOYBEANS" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="HMAX_MAXRP_DEDUCTUNITRICE" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="HMAX_MAXRP_DEDUCTUNITWHEAT" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="HMAX_MAXRP_LATEDEDUCT" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="HMAX_MAXRP_FCMC" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="HMAX_MAXRP_AIP" class="form-control savetbox disabled" />
                        </td>
                    </tr>
                    <%--  RAMP  RY--%>
                    <tr>
                        <td>RAMP</td>
                        <td>RY</td>
                        <td>
                            <input type="text" id="RAMP_RY_UNIT" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="RAMP_RY_OPTION" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="RAMP_RY_UPPER" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="RAMP_RY_LOWER" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="RAMP_RY_RANGEMAX" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="RAMP_RY_AREAYIELD" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="RAMP_RY_YIELD" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="RAMP_RY_PRICE" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="RAMP_RY_LIABILITYMAX75" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="RAMP_RY_LIABILITYMAX85" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="RAMP_RY_DEDUCTMAX" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="RAMP_RY_DEDUCTUNITCORN" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="RAMP_RY_DEDUCTUNITSOYBEANS" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="RAMP_RY_DEDUCTUNITRICE" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="RAMP_RY_DEDUCTUNITWHEAT" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="RAMP_RY_LATEDEDUCT" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="RAMP_RY_FCMC" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="RAMP_RY_AIP" class="form-control savetbox disabled" />
                        </td>
                    </tr>
                    <%--  RAMP  RR--%>
                    <tr>
                        <td>RAMP</td>
                        <td>RR</td>
                        <td>
                            <input type="text" id="RAMP_RR_UNIT" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="RAMP_RR_OPTION" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="RAMP_RR_UPPER" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="RAMP_RR_LOWER" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="RAMP_RR_RANGEMAX" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="RAMP_RR_AREAYIELD" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="RAMP_RR_YIELD" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="RAMP_RR_PRICE" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="RAMP_RR_LIABILITYMAX75" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="RAMP_RR_LIABILITYMAX85" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="RAMP_RR_DEDUCTMAX" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="RAMP_RR_DEDUCTUNITCORN" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="RAMP_RR_DEDUCTUNITSOYBEANS" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="RAMP_RR_DEDUCTUNITRICE" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="RAMP_RR_DEDUCTUNITWHEAT" class="form-control savetbox wid100" />
                        </td>
                        <td>
                            <input type="text" id="RAMP_RR_LATEDEDUCT" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="RAMP_RR_FCMC" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="RAMP_RR_AIP" class="form-control savetbox disabled" />
                        </td>
                    </tr>
                    <%--  ICE BY--%>
                    <tr>
                        <td>ICE</td>
                        <td>BY</td>
                        <td>
                            <input type="text" id="ICE_BY_UNIT" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="ICE_BY_OPTION" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_BY_UPPER" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_BY_LOWER" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_BY_RANGEMAX" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_BY_AREAYIELD" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_BY_YIELD" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="ICE_BY_PRICE" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_BY_LIABILITYMAX75" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_BY_LIABILITYMAX85" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_BY_DEDUCTMAX" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_BY_DEDUCTUNITCORN" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_BY_DEDUCTUNITSOYBEANS" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_BY_DEDUCTUNITRICE" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_BY_DEDUCTUNITWHEAT" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_BY_LATEDEDUCT" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_BY_FCMC" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_BY_AIP" class="form-control savetbox disabled" />
                        </td>
                    </tr>
                    <%--  ICE BR--%>
                    <tr>
                        <td>ICE</td>
                        <td>BR</td>
                        <td>
                            <input type="text" id="ICE_BR_UNIT" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="ICE_BR_OPTION" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_BR_UPPER" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_BR_LOWER" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_BR_RANGEMAX" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_BR_AREAYIELD" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_BR_YIELD" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="ICE_BR_PRICE" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_BR_LIABILITYMAX75" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_BR_LIABILITYMAX85" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_BR_DEDUCTMAX" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_BR_DEDUCTUNITCORN" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_BR_DEDUCTUNITSOYBEANS" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_BR_DEDUCTUNITRICE" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_BR_DEDUCTUNITWHEAT" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_BR_LATEDEDUCT" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_BR_FCMC" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_BR_AIP" class="form-control savetbox disabled" />
                        </td>
                    </tr>
                    <%--  ICE CY--%>
                    <tr>
                        <td>ICE</td>
                        <td>CY</td>
                        <td>
                            <input type="text" id="ICE_CY_UNIT" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="ICE_CY_OPTION" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_CY_UPPER" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_CY_LOWER" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_CY_RANGEMAX" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_CY_AREAYIELD" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_CY_YIELD" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="ICE_CY_PRICE" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_CY_LIABILITYMAX75" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_CY_LIABILITYMAX85" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_CY_DEDUCTMAX" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_CY_DEDUCTUNITCORN" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_CY_DEDUCTUNITSOYBEANS" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_CY_DEDUCTUNITRICE" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_CY_DEDUCTUNITWHEAT" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_CY_LATEDEDUCT" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_CY_FCMC" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_CY_AIP" class="form-control savetbox disabled" />
                        </td>
                    </tr>
                    <%--  ICE_CR--%>
                    <tr>
                        <td>ICE</td>
                        <td>CR</td>
                        <td>
                            <input type="text" id="ICE_CR_UNIT" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="ICE_CR_OPTION" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_CR_UPPER" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_CR_LOWER" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_CR_RANGEMAX" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_CR_AREAYIELD" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_CR_YIELD" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="ICE_CR_PRICE" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_CR_LIABILITYMAX75" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_CR_LIABILITYMAX85" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_CR_DEDUCTMAX" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_CR_DEDUCTUNITCORN" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_CR_DEDUCTUNITSOYBEANS" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_CR_DEDUCTUNITRICE" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_CR_DEDUCTUNITWHEAT" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_CR_LATEDEDUCT" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_CR_FCMC" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_CR_AIP" class="form-control savetbox disabled" />
                        </td>
                    </tr>
                    <%--  ABC_AY--%>
                    <tr>
                        <td>ABC</td>
                        <td>AY</td>
                        <td>
                            <input type="text" id="ABC_AY_UNIT" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="ABC_AY_OPTION" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ABC_AY_UPPER" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="ABC_AY_LOWER" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="ABC_AY_RANGEMAX" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="ABC_AY_AREAYIELD" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="ABC_AY_YIELD" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ABC_AY_PRICE" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ABC_AY_LIABILITYMAX75" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ABC_AY_LIABILITYMAX85" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ABC_AY_DEDUCTMAX" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ABC_AY_DEDUCTUNITCORN" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="ABC_AY_DEDUCTUNITSOYBEANS" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="ABC_AY_DEDUCTUNITRICE" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="ABC_AY_DEDUCTUNITWHEAT" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="ABC_AY_LATEDEDUCT" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ABC_AY_FCMC" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ABC_AY_AIP" class="form-control savetbox disabled" />
                        </td>
                    </tr>
                    <%--  ABC_AR--%>
                    <tr>
                        <td>ABC</td>
                        <td>AR</td>
                        <td>
                            <input type="text" id="ABC_AR_UNIT" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="ABC_AR_OPTION" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ABC_AR_UPPER" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="ABC_AR_LOWER" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="ABC_AR_RANGEMAX" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="ABC_AR_AREAYIELD" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="ABC_AR_YIELD" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ABC_AR_PRICE" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ABC_AR_LIABILITYMAX75" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ABC_AR_LIABILITYMAX85" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ABC_AR_DEDUCTMAX" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ABC_AR_DEDUCTUNITCORN" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="ABC_AR_DEDUCTUNITSOYBEANS" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="ABC_AR_DEDUCTUNITRICE" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="ABC_AR_DEDUCTUNITWHEAT" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="ABC_AR_LATEDEDUCT" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ABC_AR_FCMC" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ABC_AR_AIP" class="form-control savetbox disabled" />
                        </td>
                    </tr>
                    <%--  PCI--%>
                    <tr>
                        <td>PCI</td>
                        <td>-</td>
                        <td>
                            <input type="text" id="PCI_UNIT" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="PCI_OPTION" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="PCI_UPPER" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="PCI_LOWER" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="PCI_RANGEMAX" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="PCI_AREAYIELD" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="PCI_YIELD" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="PCI_PRICE" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="PCI_LIABILITYMAX75" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="PCI_LIABILITYMAX85" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="PCI_DEDUCTMAX" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="PCI_DEDUCTUNITCORN" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="PCI_DEDUCTUNITSOYBEANS" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="PCI_DEDUCTUNITRICE" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="PCI_DEDUCTUNITWHEAT" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="PCI_LATEDEDUCT" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="PCI_FCMC" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="PCI_AIP" class="form-control savetbox" />
                        </td>
                    </tr>
                    <%--  Crop_Hail_BASIC--%>
                    <tr>
                        <td>CROP HAIL</td>
                        <td>BASIC</td>
                        <td>
                            <input type="text" id="CROP_HAIL_BASIC_UNIT" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="CROP_HAIL_BASIC_OPTION" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="CROP_HAIL_BASIC_UPPER" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="CROP_HAIL_BASIC_LOWER" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="CROP_HAIL_BASIC_RANGEMAX" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="CROP_HAIL_BASIC_AREAYIELD" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="CROP_HAIL_BASIC_YIELD" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="CROP_HAIL_BASIC_PRICE" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="CROP_HAIL_BASIC_LIABILITYMAX75" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="CROP_HAIL_BASIC_LIABILITYMAX85" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="CROP_HAIL_BASIC_DEDUCTMAX" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="CROP_HAIL_BASIC_DEDUCTUNITCORN" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="CROP_HAIL_BASIC_DEDUCTUNITSOYBEANS" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="CROP_HAIL_BASIC_DEDUCTUNITRICE" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="CROP_HAIL_BASIC_DEDUCTUNITWHEAT" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="CROP_HAIL_BASIC_LATEDEDUCT" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="CROP_HAIL_BASIC_FCMC" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="CROP_HAIL_BASIC_AIP" class="form-control savetbox" />
                        </td>
                    </tr>
                    <%--  CROP_HAIL_COMPANION--%>
                    <tr>
                        <td>CROP HAIL</td>
                        <td>COMPANION</td>
                        <td>
                            <input type="text" id="CROP_HAIL_COMPANION_UNIT" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="CROP_HAIL_COMPANION_OPTION" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="CROP_HAIL_COMPANION_UPPER" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="CROP_HAIL_COMPANION_LOWER" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="CROP_HAIL_COMPANION_RANGEMAX" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="CROP_HAIL_COMPANION_AREAYIELD" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="CROP_HAIL_COMPANION_YIELD" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="CROP_HAIL_COMPANION_PRICE" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="CROP_HAIL_COMPANION_LIABILITYMAX75" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="CROP_HAIL_COMPANION_LIABILITYMAX85" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="CROP_HAIL_COMPANION_DEDUCTMAX" class="form-control savetbox" />
                        </td>
                        <td>
                            <input type="text" id="CROP_HAIL_COMPANION_DEDUCTUNITCORN" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="CROP_HAIL_COMPANION_DEDUCTUNITSOYBEANS" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="CROP_HAIL_COMPANION_DEDUCTUNITRICE" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="CROP_HAIL_COMPANION_DEDUCTUNITWHEAT" class="form-control savetbox wid100 disabled" />
                        </td>
                        <td>
                            <input type="text" id="CROP_HAIL_COMPANION_LATEDEDUCT" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="CROP_HAIL_COMPANION_FCMC" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="CROP_HAIL_COMPANION_AIP" class="form-control savetbox" />
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <script>
        $(function () {
            $('.disabled').prop('disabled', 'disabled');
            var obj = {
                Init: function () {
                    obj.Getdata();
                    obj.EnableDisableControls();
                    $(".savetbox").blur(function () {
                        var value = this.value;
                        var id = this.id;
                        $.ajax({
                            type: "POST",
                            url: "InsuranceValidation.aspx/UpdateKeysDetails",
                            data: JSON.stringify({ VKey: id, VValue: value }),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (data) {
                                //$('.loader').show();
                                //obj.loadtable();
                                //$('#myModal').modal('hide');
                            },
                            failure: function (response) {
                            }
                        });
                    });
                },
                EnableDisableControls: function () {
                    $('#enableBtn').change(function () {
                         var value = $(this).is(':checked');
                         if (value) {
                             $('input.disabled').prop('disabled', '');
                         } else {
                            $('input.disabled').prop('disabled', 'disabled');
                         }
                    });

                    $('#editBtn').change(function () {
                        canEdit = $(this).is(':checked');
                        if (!canEdit) {
                            $('#enableBtn').parent().parent().addClass('disabled');
                            $('#enableBtn').prop('checked', false);
                            $('#enableBtn').parent().removeClass('active');
                            $('input.disabled').prop('disabled', 'disabled');
                        } else {
                            $('#enableBtn').parent().parent().removeClass('disabled');
                        }
                    });
                },
                getTableInfo: function () {
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "InsuranceValidation.aspx/GetTableInfo",
                        data: JSON.stringify({ }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            var data = JSON.parse(res.d);

                            var table_info = data.Table_Info[0];
                            $('#txtTableName').html(table_info.Table_Name);
                            $('#txtLastUpdated').html(table_info.Last_Updated);
                            $('#txtRecordsCount').html(table_info.Records_Count);

                            if (data.CanEdit) {
                                $('#editControl').removeClass('disabled');
                            } else {
                                $('#editControl').addClass('disabled');
                            }

                            $('#enableControl').addClass('disabled');
                            
                        }
                    });
                },
                Getdata: function () {
                    $.ajax({
                        type: "POST",
                        url: "InsuranceValidation.aspx/GetinsDetails",
                        //  data: JSON.stringify({ lst: dataobj }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            
                            var objdata = JSON.parse(data.d);
                            for (var i = 0; i < objdata.length; i++) {
                                $('#' + objdata[i].ins_V_Key).val(objdata[i].Ins_V_Value);
                            }
                            obj.getTableInfo();
                        },
                        failure: function (response) {
                        }
                    });
                }
            }
            obj.Init();
        });
        $('#navbtncolumns').addClass('disabled');
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" runat="Server">
</asp:Content>

