﻿<%@ Page Title="Insurance Eligibility Rules" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="InsuranceValidation2.aspx.cs" Inherits="admin_InsuranceValidation2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style>
        input[type="text"] {
            width: 450px;
        }

        /*input[type="text"] {
            width: 200px;
        }*/
        input.wid100 {
            width: 70px;
        }

        table {
            border-collapse: collapse;
        }

        table, td, th {
            border: 1px solid black;
        }

        tr {
            text-align: center;
        }

        #content {
            padding: 14px 35px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
     <div class="row switch-controls">
        <div class="switch-control disabled" id="editControl">
            <label for="editBtn">Edit</label>
            <div class="toggle-btn small">            
                <input type="checkbox" id="editBtn" class="cb-value" />
                <span class="round-btn"></span>
            </div>
        </div>
        <div class="switch-control disabled" id="enableControl">
            <label for="enableBtn">Enable</label>
            <div class="toggle-btn small">            
                <input type="checkbox" id="enableBtn" class="cb-value" />
                <span class="round-btn"></span>
            </div>
        </div>
    </div>

    <div class="container1">
        <div style="overflow-x: auto; overflow-y: auto; height: 525px">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr style="height: 40px;">
                        <th style="text-align: center;width: 50px;" class="secure-code">Ins Plan</th>
                        <th style="text-align: center;width: 80px;" >Ins Plan Type</th>
                        <th style="text-align: center; width: 430px;">Eligible Crop Code</th>
                        <th style="text-align: center;">Eligible State Abbrev</th>
                        <th style="text-align: center;">Ineligible Crop Prac</th>
                    </tr>
                </thead>
                <tbody>
                    <%--Hmax Standard--%>
                    <tr style="text-align: center;">
                        <td>Hmax</td>
                        <td>Standard</td>
                        <td>
                            <input type="text" id="HMAX_STANDARD_ELLIGIBLECROPS" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="HMAX_STANDARD_ELLIGIBLESTATES" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="HMAX_STANDARD_INELLIGIBLEPRACTICES" class="form-control savetbox disabled" />
                        </td>
                    </tr>
                    <%--Hmax X1--%>
                    <tr style="text-align: center;">
                        <td>Hmax</td>
                        <td>X1</td>
                        <td>
                            <input type="text" id="HMAX_X1_ELLIGIBLECROPS" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="HMAX_X1_ELLIGIBLESTATES" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="HMAX_X1_INELLIGIBLEPRACTICES" class="form-control savetbox disabled" />
                        </td>
                    </tr>
                    <%--HMAX MAXRP--%>
                    <tr style="text-align: center;">
                        <td>Hmax</td>
                        <td>MaxRP</td>
                        <td>
                            <input type="text" id="HMAX_MAXRP_ELLIGIBLECROPS" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="HMAX_MAXRP_ELLIGIBLESTATES" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="HMAX_MAXRP_INELLIGIBLEPRACTICES" class="form-control savetbox disabled" />
                        </td>
                    </tr>                    
                    <%--RAMP RY--%>
                    <tr style="text-align: center;">
                        <td>RAMP</td>
                        <td>RY</td>
                        <td>
                            <input type="text" id="RAMP_RY_ELLIGIBLECROPS" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="RAMP_RY_ELLIGIBLESTATES" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="RAMP_RY_INELLIGIBLEPRACTICES" class="form-control savetbox disabled" />
                        </td>
                     </tr>
                        <%--RAMP RR--%>
                    <tr style="text-align: center;">
                        <td>RAMP</td>
                        <td>RR</td>
                        <td>
                            <input type="text" id="RAMP_RR_ELLIGIBLECROPS" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="RAMP_RR_ELLIGIBLESTATES" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="RAMP_RR_INELLIGIBLEPRACTICES" class="form-control savetbox disabled" />
                        </td>
                    </tr>
                    <%--ICE BY--%>
                    <tr style="text-align: center;">
                        <td>ICE</td>
                        <td>BY</td>
                        <td>
                            <input type="text" id="ICE_BY_ELLIGIBLECROPS" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_BY_ELLIGIBLESTATES" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_BY_INELLIGIBLEPRACTICES" class="form-control savetbox disabled" />
                        </td>
                    </tr>
                    <%--ICE BR--%>
                    <tr style="text-align: center;">
                        <td>ICE</td>
                        <td>BR</td>
                        <td>
                            <input type="text" id="ICE_BR_ELLIGIBLECROPS" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_BR_ELLIGIBLESTATES" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_BR_INELLIGIBLEPRACTICES" class="form-control savetbox disabled" />
                        </td>
                    </tr>
                    <%--ICE CY--%>
                    <tr style="text-align: center;">
                        <td>ICE</td>
                        <td>CY</td>
                        <td>
                            <input type="text" id="ICE_CY_ELLIGIBLECROPS" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_CY_ELLIGIBLESTATES" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_CY_INELLIGIBLEPRACTICES" class="form-control savetbox disabled" />
                        </td>
                    </tr> 
                    <%--ICE CR--%>
                    <tr style="text-align: center;">
                        <td>ICE</td>
                        <td>CR</td>
                        <td>
                            <input type="text" id="ICE_CR_ELLIGIBLECROPS" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_CR_ELLIGIBLESTATES" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_CR_INELLIGIBLEPRACTICES" class="form-control savetbox disabled" />
                        </td>
                    </tr>  
                    <%--ABC AY--%>
                    <tr style="text-align: center;">
                        <td>ABC</td>
                        <td>AY</td>
                        <td>
                            <input type="text" id="ABC_AY_ELLIGIBLECROPS" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ABC_AY_ELLIGIBLESTATES" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ABC_AY_INELLIGIBLEPRACTICES" class="form-control savetbox disabled" />
                        </td>
                    </tr> 
                    <%--ABC AR--%>
                    <tr style="text-align: center;">
                        <td>ABC</td>
                        <td>AR</td>
                        <td>
                            <input type="text" id="ABC_AR_ELLIGIBLECROPS" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ABC_AR_ELLIGIBLESTATES" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ABC_AR_INELLIGIBLEPRACTICES" class="form-control savetbox disabled" />
                        </td>
                    </tr>
                    <%--PCI - --%>
                    <tr style="text-align: center;">
                        <td>PCI</td>
                        <td>-</td>
                        <td>
                            <input type="text" id="PCI_ELLIGIBLECROPS" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="PCI_ELLIGIBLESTATES" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="PCI_INELLIGIBLEPRACTICES" class="form-control savetbox disabled" />
                        </td>
                    </tr>                    
                    <%--CROPHAIL BASIC --%>
                    <tr style="text-align: center;">
                        <td>CROPHAIL</td>
                        <td>BASIC</td>
                        <td>
                            <input type="text" id="CROPHAIL_BASIC_ELLIGIBLECROPS" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="CROPHAIL_BASIC_ELLIGIBLESTATES" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="CROPHAIL_BASIC_INELLIGIBLEPRACTICES" class="form-control savetbox disabled" />
                        </td>
                    </tr>
                    <%--CROPHAIL COMPANION --%>
                    <tr style="text-align: center;">
                        <td>CROPHAIL</td>
                        <td>COMPANION</td>
                        <td>
                            <input type="text" id="CROPHAIL_COMPANION_ELLIGIBLECROPS" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="CROPHAIL_COMPANION_ELLIGIBLESTATES" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="CROPHAIL_COMPANION_INELLIGIBLEPRACTICES" class="form-control savetbox disabled" />
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <script>
        $(function () {            
            var canEdit = false;

            var obj = {
                Init: function () {
                    obj.Getdata();
                    obj.EnableDisableControls();

                    $('#navbtnadd').addClass('disabled');
                    $('#navbtnsave').addClass('disabled');
                    $('.disabled').prop('disabled', 'disabled');

                    $(".savetbox").blur(function () {
                        var value = this.value;
                        var id = this.id;
                        $.ajax({
                            type: "POST",
                            url: "InsuranceValidation2.aspx/UpdateKeysDetails",
                            data: JSON.stringify({ VKey: id, VValue: value }),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (data) {
                                //$('.loader').show();
                                //obj.loadtable();
                                //$('#myModal').modal('hide');
                            },
                            failure: function (response) {
                            }
                        });
                    });
                },

                EnableDisableControls: function () {
                    $('#enableBtn').change(function () {
                         var value = $(this).is(':checked');
                         if (value) {
                             $('input.disabled').prop('disabled', '');
                         } else {
                            $('input.disabled').prop('disabled', 'disabled');
                         }
                    });

                    $('#editBtn').change(function () {
                        canEdit = $(this).is(':checked');
                        if (!canEdit) {
                            $('#enableBtn').parent().parent().addClass('disabled');
                            $('#enableBtn').prop('checked', false);
                            $('#enableBtn').parent().removeClass('active');
                            $('input.disabled').prop('disabled', 'disabled');
                        } else {
                            $('#enableBtn').parent().parent().removeClass('disabled');
                        }
                    });
                },
                getTableInfo: function () {
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "InsuranceValidation2.aspx/GetTableInfo",
                        data: JSON.stringify({ }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            var data = JSON.parse(res.d);

                            var table_info = data.Table_Info[0];
                            $('#txtTableName').html(table_info.Table_Name);
                            $('#txtLastUpdated').html(table_info.Last_Updated);
                            $('#txtRecordsCount').html(table_info.Records_Count);

                            if (data.CanEdit) {
                                $('#editControl').removeClass('disabled');
                            } else {
                                $('#editControl').addClass('disabled');
                            }

                            $('#enableControl').addClass('disabled');
                            
                        }
                    });
                },
                Getdata: function () {
                    $.ajax({
                        type: "POST",
                        url: "InsuranceValidation2.aspx/GetinsDetails",
                        //  data: JSON.stringify({ lst: dataobj }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            
                            var objdata = JSON.parse(data.d);
                            for (var i = 0; i < objdata.length; i++) {
                                $('#' + objdata[i].ins_V_Key).val(objdata[i].Ins_V_Value);
                            }
                            obj.getTableInfo();
                        },
                        failure: function (response) {
                        }
                    });
                }
            }
            obj.Init();
        });

        $('#navbtncolumns').addClass('disabled');
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" runat="Server">
</asp:Content>

