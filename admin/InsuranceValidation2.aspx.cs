﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_InsuranceValidation2 : BasePage
{
    public admin_InsuranceValidation2()
    {        
        Table_Name = "Ref_Insurance_Eligibility_Rules";
    }

    [WebMethod]
    public static string GetTableInfo()
    {
        Count_Query = @"DECLARE @Count int;
                            Select @Count = 14";
        return Get_Table_Info();
    }

    [WebMethod]
    public static string GetinsDetails()
    {
        string sql_qry = "select ins_V_Key,Ins_V_Value from Ref_Ins_Validation";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
    }


    [WebMethod]
    public static void UpdateKeysDetails(string VValue, string VKey)
    {
        string qry = "";
        qry = "select * from Ref_Ins_Validation where ins_V_Key='" + VKey + "'";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(qry, dbKey);
        if (dt != null && dt.Rows.Count > 0)
        {
            qry = "update Ref_Ins_Validation set Ins_V_Value='" + VValue + "' where ins_V_Key='" + VKey + "'";
        }
        else
        {
            qry = "insert into Ref_Ins_Validation (ins_V_Key,Ins_V_Value)values('" + VKey + "','" + VValue + "')";
        }
        gen_db_utils.gp_sql_execute(qry, dbKey);

        gen_db_utils.Update_Table_Audit_Trail("Ref_Insurance_Eligibility_Rules", userid, dbKey);
    }
}