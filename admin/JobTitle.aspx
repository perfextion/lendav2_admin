﻿<%@ Page Title="User Role Type" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="JobTitle.aspx.cs" Inherits="admin_JobTitle" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div>
         <asp:Label ID="lbl_table" runat="server" Text="lbl_table"></asp:Label>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" runat="Server">
    <script>
        $(function () {
            $('#txtSearchBar').on('keyup', function () {
                var value = $(this).val().toLowerCase();

                var count = 0;
                $("#tblJobTitle tbody tr").filter(function () {
                    var lineStr = $(this).text().toLowerCase();
                    if (lineStr.indexOf(value) === -1) {
                        $(this).hide();
                    } else {
                        $(this).show();
                        count++;
                    }
                });

                if (count > 0) {
                    $("#tblJobTitle tfoot").hide();
                } else {
                    $("#tblJobTitle tfoot").show();
                }
            });
            $('#navbtnsave').addClass('disabled');
            $('#navbtnrefresh').addClass('disabled');
            $('#navbtnadd').addClass('disabled');
            $("#navbtndownload").click(function () {
                exportTableToExcel('tblJobTitle', 'User Role Type')
            });
            $('#navbtncolumns').addClass('disabled');

            function getTableInfo() {
                $.ajax({
                    type: "POST",
                    async: false,
                    url: "JobTitle.aspx/GetTableInfo",
                    data: JSON.stringify({}),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (res) {
                        var data = JSON.parse(res.d)[0];
                        $('#txtTableName').html(data.Table_Name);
                        $('#txtLastUpdated').html(data.Last_Updated);
                        $('#txtRecordsCount').html(data.Records_Count);
                    }
                });
            }

            getTableInfo();
        });
</script>
</asp:Content>

