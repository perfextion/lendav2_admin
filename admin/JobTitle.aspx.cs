﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Services;

public partial class admin_JobTitle : System.Web.UI.Page
{
    private static string dbKey = "gp_conn";
    static string userid;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["UserID"] == null)
            {
                Response.Redirect("../Login.aspx");
            }
            else
            {
                userid = Session["UserID"].ToString();
                lbl_table.Text = LoadTable();
            }
        }
    }

    [WebMethod]
    public static string GetTableInfo()
    {
        try
        {
            string sql_query = @"DECLARE @Count int;
                            Select @Count = Count(*)  From Job_Title;

                            SELECT
	                            Table_UI_Name as [Table_Name],
	                            CONCAT('Records Count: ', @Count) as [Records_Count],
	                            CASE 
		                            WHEN Modified_By IS NOT NULL THEN CONCAT('Last Updated by: ', DBO.GetUserNameByUserID(Modified_By), ' ', FORMAT( Modified_On, 'MM/dd/yyyy hh:mm:ss tt', 'en-US' ))
		                            ELSE ''
	                            END AS [Last_Updated]
                            FROM Table_Audit_Trail
                            WHERE Table_Name = 'Job_Title' ";

            DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_query, dbKey);
            return JsonConvert.SerializeObject(dt);
        }
        catch
        {
            return JsonConvert.SerializeObject(new List<object> { new object() });
        }
    }

    protected string LoadTable()
    {
        string str_return = "";
        string strQuery = " SELECT  Job_Title_Code , Team , Job_Title, Reports_To  FROM Job_Title";
        DataTable dt1 = gen_db_utils.gp_sql_get_datatable(strQuery, dbKey);

        str_return += "<div> <table id='tblJobTitle' class='table table-striped table-bordered' >  "
                                      + "      <thead> "
                              + "     <tr> "
                              + "       <th style='width: 15%;' >Role Type Code </th> "
                              + "       <th style='width: 30%;' >Role Type Name</th> "
                              + "       <th style='width: 19%;' >Team </th> "
                              + "       <th style='width: 19%;' >Reports To </th> "
                              + "    </tr> "
                              + "   </thead> "
                              + " <tbody> ";

        foreach (DataRow dtr1 in dt1.Rows)
        {
            str_return += "<tr>"
                               + "<td>" + dtr1["Job_Title_Code"].ToString() + "</td>"
                                + "<td>" + dtr1["Job_Title"].ToString() + "</td>"
                               + "<td>" + dtr1["Team"].ToString() + "</td>"
                               + "<td>" + dtr1["Reports_To"].ToString() + "</td>"
                             + "</tr>";
        }

        str_return += " </tbody> "
                        + "<tfoot style='display: none'> <td colspan='4'>No Data found </td><tfoot>"
                      + " </table> "
              + "</div></div></div >";

        return str_return;
    }
}