﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_JobTitleNew : BasePage
{
    public admin_JobTitleNew()
    {
        Count_Query = string.Empty;
        Table_Name = "Job_Title";
    }

    [WebMethod]
    public static string GetTableInfo()
    {
        return Get_Table_Info();
    }

    [WebMethod]
    public static string LoadTable()
    {
        string sql_qry = @"SELECT *, 0 as [Actionstatus] from Job_Title";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static void SaveTable(dynamic JobTitles)
    {
        foreach (var item in JobTitles)
        {
            string qry = "";
            int val = Convert.ToInt32(item["Actionstatus"]);

            if (val == 1)
            {
                qry = @"INSERT INTO [dbo].[Job_Title]
                                   ([Job_Title_Code]
                                   ,[Job_Title]
                                   ,[Team]
                                   ,[Reports_To]
                                   ,[Status])
                             VALUES
                               (
                                    '" + item["Job_Title_Code"].Replace("'", "''") + @"'" +
                                   ",'" + item["Job_Title"].Replace("'", "''") + @"'" +
                                   ",'" + item["Team"].Replace("'", "''") + @"'" +
                                   ",'" + item["Reports_To"].Replace("'", "''") + @"'" +
                                   ",'" + item["Status"] + @"'
                                )  ";

                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 2)
            {
                qry = @"UPDATE [dbo].[Job_Title]
                           SET  [Job_Title_Code] = '" + item["Job_Title_Code"].Replace("'", "''") + @"' " +
                              ",[Job_Title] = '" + item["Job_Title"].Replace("'", "''") + @"' " +
                              ",[Team] = '" + item["Team"].Replace("'", "''") + @"' " +
                              ",[Reports_To] = '" + item["Reports_To"].Replace("'", "''") + @"' " +
                              ",[Status] = '" + item["Status"] + @"' " +
                         "WHERE Job_Title_ID = '" + item["Job_Title_ID"] + "' ";

                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 3)
            {
                qry = "DELETE FROM Job_Title  where Job_Title_ID = '" + item["Job_Title_ID"] + "' ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
        }

        Update_Table_Audit();
    }
}