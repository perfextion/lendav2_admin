﻿<%@ Page Title="" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="LoanAssociation.aspx.cs" Inherits="admin_LoanAssociation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style>
        .ui-jqgrid .ui-jqgrid-htable th div {
            height: auto;
            overflow: hidden;
            padding-right: 4px;
            padding-top: 2px;
            position: relative;
            vertical-align: text-top;
            white-space: normal !important;
        }

        .ui-jqgrid tr.jqgrow td {
            word-wrap: break-word; /* IE 5.5+ and CSS3 */
            white-space: pre-wrap; /* CSS3 */
            white-space: normal !important;
            height: auto;
            vertical-align: text-top;
            padding-top: 2px;
            padding-bottom: 3px;
        }

        .ui-jqgrid .ui-jqgrid-htable th {
            height: 45px;
        }

        .cvteste {
            padding-right: 14px !important;
        }

        #content {
            padding: 14px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="loan-association loan-transaction-table">
        <div class="row switch-controls">
            <div class="switch-control disabled" id="editControl">
                <label for="editBtn">Edit</label>
                <div class="toggle-btn small">            
                    <input type="checkbox" id="editBtn" class="cb-value" />
                    <span class="round-btn"></span>
                </div>
            </div>
            <div class="switch-control disabled" id="enableControl">
                <label for="enableBtn">Enable</label>
                <div class="toggle-btn small">            
                    <input type="checkbox" id="enableBtn" class="cb-value" />
                    <span class="round-btn"></span>
                </div>
            </div>
        </div>
        <div class="loader"></div>
        <div class="row main-content">
            <div>
                <table id="jqGrid"></table>
                <div id="jqGridPager"></div>
            </div>
        </div>
        <asp:HiddenField ID="LoanFullID" runat="server" />
    </div>
    <br />
    <br />
    <br />
    <script type="text/javascript">  
        var DeleteRows = [];
        var timer;
        var lastSelection;
        var pref_contacts = {};
        var referral_types = {};
        var response_type = {};
        var assocTypes = {};

        $('#navbtnsave').addClass('disabled');
        $('#navbtnadd').addClass('disabled');

        $(function () {
            var obj = {
                Init: function () {
                    $('.loader').show();
                    obj.getListItems();

                    obj.bindGrid();

                    $("#navbtnrefresh").on('click', function () {
                        $('.loader').show();
                        obj.bindGrid();
                    });

                    $("#txtSearchBar").on("keyup", function () {
                        var self = this;
                        obj.search(self);
                    });

                    $('#navbtncolumns').click(function () {
                        $("#jqGrid").columnChooser({});
                    });

                    obj.initLoanSelect();

                    $("#navbtndownload").on("click", function () {
                        $("#jqGrid").jqGrid("exportToExcel", {
                            includeLabels: true,
                            includeGroupHeader: true,
                            includeFooter: true,
                            fileName: "Loan Association.xlsx",
                            maxlength: 40
                        });
                    });
                },
                getListItems: function () {
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "LoanAssociation.aspx/GetListItems",
                        data: JSON.stringify({  }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            var data = JSON.parse(res.d);
                            assocTypes = {};
                            response_type = {};
                            pref_contacts = {};
                            referral_types = {};

                            response_type[''] = '';
                            pref_contacts[''] = '';
                            referral_types[''] = '';

                            for (var i of data) {
                                if (i.List_Group_Code == 'ASSOC_TYPE') {
                                    assocTypes[i.List_Item_Value] = i.List_Item_Name;
                                }

                                if (i.List_Group_Code == 'PREF_CONTACT') {
                                    pref_contacts[i.List_Item_Value] = i.List_Item_Name;
                                }

                                if (i.List_Group_Code == 'ASSOC_REFERRAL_TYPE') {
                                    referral_types[i.List_Item_Value] = i.List_Item_Name;
                                }

                                if (i.List_Group_Code == 'ASSOC_RESPONSE_TYPE') {
                                    response_type[i.List_Item_Value] = i.List_Item_Name;
                                }

                                if (i.List_Group_Code == 'ASSOC_DOC_TYPE') {
                                    response_type[i.List_Item_Value] = i.List_Item_Name;
                                }
                            }                            
                        }                        
                    });
                },
                initLoanSelect: function () {
                    var hiddenVal = $('#ContentPlaceHolder1_LoanFullID').val();

                    if (hiddenVal) {
                        $('#txtSearchLoan').val(hiddenVal);
                    }

                    $('.select-loan').show();

                    $('#selectLoanBtn').click(function () {
                        $('.loader').show();
                        var loan = $('#txtSearchLoan').val();
                        $('#ContentPlaceHolder1_LoanFullID').val(loan);

                        obj.bindGrid();
                    });
                },
                getTableInfo: function () {
                    var loan_full_id = $('#ContentPlaceHolder1_LoanFullID').val();
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "LoanAssociation.aspx/GetTableInfo",
                        data: JSON.stringify({ loan_full_id: loan_full_id }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            var data = JSON.parse(res.d);

                            var table_info = data.Table_Info[0];
                            $('#txtTableName').html(table_info.Table_Name);
                            $('#txtLastUpdated').html(table_info.Last_Updated);
                            $('#txtRecordsCount').html(table_info.Records_Count);

                            if (data.CanEdit) {
                                $('#editControl').removeClass('disabled');
                            } else {
                                $('#editControl').addClass('disabled');
                            }

                            $('#enableControl').addClass('disabled');
                            
                        }
                    });
                },
                bindGrid: function () {
                    var loan_full_id = $('#ContentPlaceHolder1_LoanFullID').val();
                    $.ajax({
                        type: "POST",
                        url: "LoanAssociation.aspx/LoadTable",
                        data: JSON.stringify({ loan_full_id: loan_full_id}),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var resData = JSON.parse(data.d);
                            $('#lblcount').html('Count of records: ' + resData.length);
                            $('.loader').show();
                            for (var i = 0; i < resData.length; i++) {
                                resData[i].rowid = i + 1;
                            }
                            obj.loadgrid(resData);
                            obj.getTableInfo();
                            $('.loader').hide();
                        }
                    });
                },
                loadgrid: function (data) {
                    $.jgrid.gridUnload("#jqGrid");
                    $("#jqGrid").jqGrid({
                        datatype: "local",
                        styleUI: 'Bootstrap',
                        data: data,
                        colModel: [
                            { label: 'rowid', name: 'rowid', width: 75, editable: false, key: true, hidden: true, hidedlg: true },
                            { label: 'Loan Assoc ID', name: 'Assoc_ID', width: 120, editable: false, hidden: false },
                            { label: 'Loan Full ID', name: 'Loan_Full_ID', align: 'center', width: 120, editable: false, hidden: false },
                            {
                                label: 'Assoc Type Code',
                                name: 'Assoc_Type_Code',
                                width: 90,
                                align: "center",
                                classes: "center",
                                editable: false
                            },
                            {
                                label: 'Assoc Name',
                                name: 'Assoc_Name',
                                width: 240,
                                align: "left",
                                classes: "left",
                                editable: false
                            },                            
                            {
                                label: 'Contact',
                                name: 'Contact',
                                width: 120,
                                align: "left",
                                classes: "left",
                                editable: false,
                                hidden: false
                            },
                            {
                                label: 'Location',
                                name: 'Location',
                                width: 120,
                                align: "left",
                                classes: "left",
                                editable: false,
                                hidden: false
                            },
                            {
                                label: 'Email',
                                align: "left",
                                classes: "left",
                                name: 'Email',
                                width: 200,
                                editable: false,
                                hidden: false
                            },
                            {
                                label: 'Phone',
                                align: "left",
                                classes: "left",
                                name: 'Phone',
                                width: 120,
                                editable: false,
                                hidden: false
                            },
                            {
                                label: 'Amount',
                                align: "right",
                                classes: "right",
                                name: 'Amount',
                                width: 120,
                                editable: false,
                                hidden: false,
                                formatter: 'assocAmountFormatter'
                            },
                            {
                                label: 'Response Ind',
                                align: "center",
                                classes: "center",
                                name: 'Response',
                                width: 120,
                                editable: false,
                                hidden: false
                            },
                            {
                                label: 'Referred Type',
                                align: "center",
                                classes: "center",
                                name: 'Referred_Type',
                                width: 90,
                                editable: false,
                                hidden: false
                            },
                            {
                                label: 'Preferred Contact Ind',
                                name: 'Preferred_Contact_Ind',
                                align: "center",
                                classes: "center",
                                width: 90,
                                editable: false,
                                hidden: false
                            },
                            {
                                label: 'Status',
                                name: 'Status_Updated',
                                align: "center",
                                classes: "center",
                                width: 90,
                                editable: false,
                                hidden: false
                            },
                            { label: 'Actionstatus', name: 'Actionstatus', width: 70, editable: false, hidden: true, exportcol: false, hidedlg: true }
                        ],
                        viewrecords: true,
                        loadonce: true,
                        restoreAfterSelect: false,
                        saveAfterSelect: true,
                        sortable: true,
                        onPaging: obj.changePage,
                        height: 'auto',
                        rowNum: 100,
                        pager: "#jqGridPager"
                    });
                },
                search: function (self) {
                    if (timer) { clearTimeout(timer); }
                    timer = setTimeout(function () {
                        obj.changePage();
                        $("#jqGrid").jqGrid('filterInput', self.value);
                        var length = jQuery("#jqGrid").jqGrid('getGridParam', 'records');
                        $('#txtRecordsCount').html('Records Count: ' + length);
                    }, 0);
                },
                changePage: function () {

                    lastSelection = "";
                    var grid = $("#jqGrid");
                    var id = $('.inline-edit-cell').parent().parent().prop('id');
                    if (id != undefined) {
                        grid.jqGrid('saveRow', id);
                        grid.jqGrid('restoreRow', id);
                        var row = grid.jqGrid('getRowData', id);
                        if (!jQuery.isEmptyObject(row)) {
                            var dataobj = jQuery("#jqGrid").jqGrid('getGridParam', 'data');
                            index = dataobj.findIndex(x => x.rowid == row.rowid);
                            if (row.rowid > 0 && row.Actionstatus != 1) {
                                row.Actionstatus = 2;
                                grid.jqGrid('getGridParam', 'data')[index] = row;
                            }
                        }
                    }
                }
            }
            obj.Init();
        });   
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" Runat="Server">
</asp:Content>

