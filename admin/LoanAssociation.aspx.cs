﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_LoanAssociation : BasePage
{
    public admin_LoanAssociation()
    {
        Table_Name = "Loan_Association";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        base.Page_Load(sender, e);
        LoanFullID.Value = Loan_Full_ID;
    }

    [WebMethod]
    public static string GetTableInfo(string loan_full_id)
    {
        string sql_qry = @"DECLARE @Count int;
                        SELECT @Count = Count(*) from [Loan_Association]";

        if (!string.IsNullOrEmpty(loan_full_id))
        {
            sql_qry += " Where Loan_Full_ID like '%" + loan_full_id + "%'";
            sql_qry += " AND [IsDelete] <> 1";
        }
        else
        {
            sql_qry += " WHERE [IsDelete] <> 1";
        }

        Count_Query = sql_qry;

        return Get_Table_Info();
    }

    [WebMethod]
    public static Dictionary<string, string> GetLoanFullIds()
    {
        string sql_qry = "select distinct Loan_Full_ID from [Loan_Association]  order by Loan_Full_ID ";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return dt.AsEnumerable().ToDictionary(row => row[0].ToString(), row => row[0].ToString());
    }

    [WebMethod]
    public static string GetListItems()
    {
        string sql_qry = @"SELECT List_Item_Value, List_Item_Name, List_Group_Code 
                            FROM REF_LIST_ITEM
                            WHERE LIST_GROUP_CODE IN ('PREF_CONTACT', 'ASSOC_TYPE', 'ASSOC_RESPONSE_TYPE', 'ASSOC_DOC_TYPE', 'ASSOC_REFERRAL_TYPE')";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static string LoadTable(string loan_full_id)
    {
        HttpContext.Current.Session["Loan_Full_ID"] = loan_full_id;

        string sql_qry = @"SELECT *, 0 as [Actionstatus], 1 as [Status_Updated] from [Loan_Association]";

        if (!string.IsNullOrEmpty(loan_full_id))
        {
            sql_qry += " Where Loan_Full_ID like '%" + loan_full_id + "%'";
            sql_qry += " AND [IsDelete] <> 1";
        }
        else
        {
            sql_qry += " WHERE [IsDelete] <> 1";
        }


        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static void SaveTable(List<Loan_Association> AssociationList, string LoanFullID)
    {
        if(!string.IsNullOrEmpty(LoanFullID))
        {
            foreach (var assoc in AssociationList)
            {
                string qry = "";
                int val = Convert.ToInt32(assoc.ActionStatus);

                if (val == 1)
                {
                    qry += InsertQuery(assoc, LoanFullID);
                    qry += Environment.NewLine;
                }
                else if (val == 2)
                {
                    qry += UpdateQuery(assoc, LoanFullID);
                    qry += Environment.NewLine;
                    
                }
                else if (val == 3)
                {
                    qry += DeleteQuery(assoc.Assoc_ID.ToString(), LoanFullID);
                }
            }
        }        
    }

    [WebMethod]
    public static void SaveAssoc(dynamic Assoc)
    {
        string qry = InsertQuery(Assoc, Assoc["Loan_Full_ID"]);
        gen_db_utils.gp_sql_execute(qry, dbKey);
    }

    [WebMethod]
    public static void DeleteAssoc(string Assoc_ID, string LoanFullID)
    {
        string qry = DeleteQuery(Assoc_ID, LoanFullID);
        gen_db_utils.gp_sql_execute(qry, dbKey);
    }

    public static string InsertQuery(dynamic reqmodel, string LoanFullID)
    {
        var loan_id = LoanFullID.Split('-')[0];
        var seq_no = LoanFullID.Split('-')[1];

        string query = @"Insert Into [dbo].[Loan_Association]
                                        (
                                            [Ref_Assoc_ID],
                                            [Z_Loan_ID],
                                            [Z_Loan_Seq_Num],
                                            [Loan_Full_ID],
                                            [Assoc_Type_Code],
                                            [Principal_Ind],
                                            [Assoc_Name],
                                            [Contact],
                                            [Location],
                                            [Phone],
                                            [Email],
                                            [Assoc_Status],
                                            [Status],
                                            [Amount],
                                            [Referred_Type],
                                            [Response],
                                            [Documentation],
                                            [Is_CoBorrower],
                                            [Preferred_Contact_Ind]
                                        )
                                    VALUES(
                                            '" + 0 +
                                            "','" + loan_id +
                                            "','" + seq_no +
                                            "','" + LoanFullID +
                                            "','" + reqmodel["Assoc_Type_Code"] +
                                            "','" + 0 +
                                            "','" + reqmodel["Assoc_Name"] +
                                            "','" + reqmodel["Contact"] +
                                            "','" + reqmodel["Location"] +
                                            "','" + reqmodel["Phone"] +
                                            "','" + reqmodel["Email"] +
                                            "',1" +
                                            ",0" +
                                            ",'" + reqmodel["Amount"] +
                                            "','" + reqmodel["Referred_Type"] +
                                            "','" + reqmodel["Response"] +
                                            "','" + reqmodel["Documentation"] +
                                            "','" + 0 +
                                            "','" + reqmodel["Preferred_Contact_Ind"] + "'" +
                                        ")";

        return query;
    }


    public static string UpdateQuery(Loan_Association assoc, string LoanFullID)
    {
        return string.Empty;
    }

    public static string DeleteQuery(string Assoc_ID, string LoanFullID)
    {
        string query = "Delete From Loan_Association where Assoc_ID = '" + Assoc_ID + "' And Loan_Full_ID = '" + LoanFullID + "'";
        query += Environment.NewLine;

        return query;
    }
}