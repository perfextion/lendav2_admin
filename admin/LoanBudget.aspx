﻿<%@ Page Title="" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="LoanBudget.aspx.cs" Inherits="admin_LoanBudget" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .ui-jqgrid .ui-jqgrid-htable th div {
            height: auto;
            overflow: hidden;
            padding-right: 4px;
            padding-top: 2px;
            position: relative;
            vertical-align: text-top;
            white-space: normal !important;
        }

        .ui-jqgrid tr.jqgrow td {
            word-wrap: break-word; /* IE 5.5+ and CSS3 */
            white-space: pre-wrap; /* CSS3 */
            white-space: normal !important;
            height: auto;
            vertical-align: text-top;
            padding-top: 2px;
            padding-bottom: 3px;
        }

        .ui-jqgrid .ui-jqgrid-htable th {
            height: 45px;
        }

        .cvteste {
            padding-right: 25px 14px !important;
        }

        #content {
            padding: 14px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="loan-budget loan-transaction-table">
        <div class="row switch-controls">
            <div class="switch-control disabled" id="editControl">
                <label for="editBtn">Edit</label>
                <div class="toggle-btn small">            
                    <input type="checkbox" id="editBtn" class="cb-value" />
                    <span class="round-btn"></span>
                </div>
            </div>
            <div class="switch-control disabled" id="enableControl">
                <label for="enableBtn">Enable</label>
                <div class="toggle-btn small">            
                    <input type="checkbox" id="enableBtn" class="cb-value" />
                    <span class="round-btn"></span>
                </div>
            </div>
        </div>
        <div class="loader"></div>
        <div class="row main-content">
            <div>
                <table id="jqGrid"></table>
                <div id="jqGridPager"></div>
            </div>
        </div>
        <asp:HiddenField ID="LoanFullID" runat="server" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" Runat="Server">
    <script type="text/javascript">  
        var DeleteRows = [];
        var timer;
        var lastSelection;

        $(function () {
            var obj = {
                Init: function () {
                    $('.loader').show();
                    obj.bindGrid();

                    $('#navbtnadd').addClass('disabled');
                    $('#navbtnsave').addClass('disabled');

                    $("#navbtnrefresh").on('click', function () {
                        $('.loader').show();
                        obj.bindGrid();
                    });

                    $("#txtSearchBar").on("keyup", function () {
                        var self = this;
                        obj.search(self);
                    });

                    $("#navbtndownload").on("click", function () {
                        $("#jqGrid").jqGrid("exportToExcel", {
                            includeLabels: true,
                            includeGroupHeader: true,
                            includeFooter: true,
                            fileName: "Loan Budget.xlsx",
                            maxlength: 40
                        });
                    });

                    $('#navbtncolumns').click(function () {
                        $("#jqGrid").columnChooser({});
                    });

                    obj.initLoanSelect();
                },
                initLoanSelect: function () {
                    var hiddenVal = $('#ContentPlaceHolder1_LoanFullID').val();

                    if (hiddenVal) {
                        $('#txtSearchLoan').val(hiddenVal);
                    }

                    $('.select-loan').show();

                    $('#selectLoanBtn').click(function () {
                        $('.loader').show();
                        var loan = $('#txtSearchLoan').val();
                        $('#ContentPlaceHolder1_LoanFullID').val(loan);

                        obj.bindGrid();
                    });
                },
                getTableInfo: function () {
                    var loan_full_id = $('#ContentPlaceHolder1_LoanFullID').val();
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "LoanBudget.aspx/GetTableInfo",
                        data: JSON.stringify({ loan_full_id: loan_full_id }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            var data = JSON.parse(res.d);

                            var table_info = data.Table_Info[0];
                            $('#txtTableName').html(table_info.Table_Name);
                            $('#txtLastUpdated').html(table_info.Last_Updated);
                            $('#txtRecordsCount').html(table_info.Records_Count);

                            if (data.CanEdit) {
                                $('#editControl').removeClass('disabled');
                            } else {
                                $('#editControl').addClass('disabled');
                            }

                            $('#enableControl').addClass('disabled');
                            
                        }
                    });
                },
                bindGrid: function () {
                    var loan_full_id = $('#ContentPlaceHolder1_LoanFullID').val();
                    $.ajax({
                        type: "POST",
                        url: "LoanBudget.aspx/LoadTable",
                        data: JSON.stringify({ loan_full_id: loan_full_id }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var resData = JSON.parse(data.d);
                            $('.loader').show();
                            for (var i = 0; i < resData.length; i++) {
                                resData[i].rowid = i + 1;
                            }
                            obj.getTableInfo();
                            obj.loadgrid(resData);
                            $('.loader').hide();
                        }
                    });
                },
                loadgrid: function (data) {
                    $.jgrid.gridUnload("#jqGrid");
                    $("#jqGrid").jqGrid({
                        datatype: "local",
                        styleUI: 'Bootstrap',
                        data: data,
                        colModel: [
                            { label: 'rowid', name: 'rowid', width: 75, editable: false, key: true, hidden: true, hidedlg: true },
                            { label: 'Loan Budget ID', name: 'Loan_Budget_ID', width: 120, editable: false, hidden: false },
                            { label: 'Loan Full ID', name: 'Loan_Full_ID', width: 90, editable: false, hidden: false },
                            {
                                label: 'Crop Practice ID',
                                name: 'Crop_Practice_ID',
                                width: 120,
                                align: "center",
                                editable: false
                            },
                            {
                                label: 'Budget Expense ID',
                                name: 'Expense_Type_ID',
                                width: 120,
                                align: "center",
                                editable: false
                            },
                            {
                                label: 'ARM Budget Per Acre',
                                name: 'ARM_Budget_Acre',
                                width: 120,
                                align: "right",
                                classes: "right",
                                editable: false,
                                formatter: 'amountFormatter'
                            },
                            {
                                label: 'ARM Budget',
                                name: 'ARM_Budget_Crop',
                                width: 120,
                                align: "right",
                                classes: "right",
                                editable: false,
                                formatter: 'totalBudgetFormatter'
                            },
                            {
                                label: 'ARM Used',
                                name: 'ARM_Used',
                                width: 120,
                                align: "right",
                                classes: "right",
                                editable: false,
                                formatter: 'totalBudgetFormatter'
                            },
                            {
                                label: 'Dist Budget Per Acre',
                                name: 'Distributor_Budget_Acre',
                                width: 120,
                                align: "right",
                                classes: "right",
                                editable: false,
                                hidden: false,
                                formatter: 'amountFormatter'
                            },
                            {
                                label: 'Dist Budget',
                                name: 'Distributor_Budget_Crop',
                                width: 120,
                                align: "right",
                                classes: "right",
                                editable: false,
                                hidden: false,
                                formatter: 'totalBudgetFormatter'
                            },
                            {
                                label: 'Dist Used',
                                name: 'Distributor_Used',
                                width: 120,
                                align: "right",
                                classes: "right",
                                editable: false,
                                hidden: false,
                                formatter: 'totalBudgetFormatter'
                            },
                            {
                                label: 'Third Party Per Acre',
                                align: "right",
                                classes: "right",
                                name: 'Third_Party_Budget_Acre',
                                width: 120,
                                editable: false,
                                hidden: false,
                                formatter: 'amountFormatter'
                            },
                            {
                                label: 'Third Party Budget',
                                align: "right",
                                classes: "right",
                                name: 'Third_Party_Budget_Crop',
                                width: 120,
                                editable: false,
                                hidden: false,
                                formatter: 'totalBudgetFormatter'
                            },
                            {
                                label: 'Total Budget',
                                align: "right",
                                classes: "right",
                                name: 'Total_Budget_Crop_ET',
                                width: 120,
                                editable: false,
                                hidden: false,
                                formatter: 'totalBudgetFormatter'
                            },
                            {
                                label: 'Notes',
                                align: "left",
                                classes: "left",
                                name: 'Notes',
                                width: 120,
                                editable: false,
                                hidden: false
                            },
                            {
                                label: 'Status',
                                name: 'Status_Formatted',
                                align: "center",
                                classes: "center",
                                width: 90,
                                editable: false,
                                hidden: false
                            },
                        ],
                        viewrecords: true,
                        loadonce: true,
                        restoreAfterSelect: false,
                        saveAfterSelect: true,
                        sortable: true,
                        onPaging: obj.changePage,
                        height: 'auto',
                        rowNum: 100,
                        pager: "#jqGridPager"
                    });
                },                
                search: function (self) {
                    if (timer) { clearTimeout(timer); }
                    timer = setTimeout(function () {
                        obj.changePage();
                        $("#jqGrid").jqGrid('filterInput', self.value);
                        var length = jQuery("#jqGrid").jqGrid('getGridParam', 'records');
                        $('#txtRecordsCount').html('Records Count: ' + length);
                    }, 0);
                },
                changePage: function () {
                    lastSelection = "";
                    var grid = $("#jqGrid");
                    var id = $('.inline-edit-cell').parent().parent().prop('id');
                    if (id != undefined) {
                        grid.jqGrid('saveRow', id);
                        grid.jqGrid('restoreRow', id);
                        var row = grid.jqGrid('getRowData', id);
                        if (!jQuery.isEmptyObject(row)) {
                            var dataobj = jQuery("#jqGrid").jqGrid('getGridParam', 'data');
                            index = dataobj.findIndex(x => x.rowid == row.rowid);
                            if (row.rowid > 0 && row.Actionstatus != 1) {
                                row.Actionstatus = 2;
                                grid.jqGrid('getGridParam', 'data')[index] = row;
                            }
                        }
                    }
                }
            }
            obj.Init();
        });
    </script>
</asp:Content>
