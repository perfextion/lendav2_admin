﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_LoanBudget : BasePage
{
    public admin_LoanBudget()
    {
        Table_Name = "Loan_Budget";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        base.Page_Load(sender, e);
        LoanFullID.Value = Loan_Full_ID;
    }

    [WebMethod]
    public static string GetTableInfo(string loan_full_id)
    {
        Count_Query = @"DECLARE @Count int; Select @Count = Count(*) From Loan_Budget LC";
        if (!string.IsNullOrEmpty(loan_full_id))
        {
            Count_Query += " Where LC.Loan_Full_ID LIKE '%" + loan_full_id + "%'";
        }

        return Get_Table_Info();
    }

    [WebMethod]
    public static Dictionary<string, string> GetLoanFullIds()
    {
        string sql_qry = "select distinct loan_full_id from Loan_Budget order by loan_full_id";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return dt.AsEnumerable().ToDictionary(row => row[0].ToString(), row => row[0].ToString());
    }

    [WebMethod]
    public static string GetCrops()
    {
        string sql_qry = @"SELECT DISTINCT      
	                        LC.Crop_Practice_ID,
	                        CONCAT(LC.Crop_Practice_ID, ':', VC.Crop_Full_Key) as [Crop_Full_Key]
                        from Loan_Budget LC
                        JOIN V_Crop_Price_Details VC on LC.Crop_Practice_ID = VC.Crop_And_Practice_ID
						Order by LC.Crop_Practice_ID";

        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static string LoadTable(string loan_full_id)
    {

        HttpContext.Current.Session["Loan_Full_ID"] = loan_full_id;

        string sql_qry = @"select * , VC.Crop_Name, VC.Crop_Full_Key, VC.Irr_Prac_Code, VC.Crop_Type_Code, 
                        case when isdelete is null or isdelete = 0 then 1 else 0 end as Status_Formatted
                        from Loan_Budget LC
                        LEFT JOIN V_Crop_Price_Details VC on LC.Crop_Practice_ID = VC.Crop_And_Practice_ID";

        if(!string.IsNullOrEmpty(loan_full_id))
        {
            sql_qry += " Where LC.Loan_Full_ID LIKE '%" + loan_full_id + "%'";
        }

        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return JsonConvert.SerializeObject(dt);
    }
}
