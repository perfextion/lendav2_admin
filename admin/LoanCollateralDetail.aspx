﻿<%@ Page Title="" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="LoanCollateralDetail.aspx.cs" Inherits="admin_LoanCollateralDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
     <style>
        .ui-jqgrid .ui-jqgrid-htable th div {
            height: auto;
            overflow: hidden;
            padding-right: 4px;
            padding-top: 2px;
            position: relative;
            vertical-align: text-top;
            white-space: normal !important;
        }

        .ui-jqgrid tr.jqgrow td {
            word-wrap: break-word; /* IE 5.5+ and CSS3 */
            white-space: pre-wrap; /* CSS3 */
            white-space: normal !important;
            height: auto;
            vertical-align: text-top;
            padding-top: 2px;
            padding-bottom: 3px;
        }

        .ui-jqgrid .ui-jqgrid-htable th {
            height: 45px;
        }

        .cvteste {
            padding-right: 14px !important;
        }

        #content {
            padding: 14px !important
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="loan-transaction-table">
        <div class="row switch-controls">
            <div class="switch-control disabled" id="editControl">
                <label for="editBtn">Edit</label>
                <div class="toggle-btn small">            
                    <input type="checkbox" id="editBtn" class="cb-value" />
                    <span class="round-btn"></span>
                </div>
            </div>
            <div class="switch-control disabled" id="enableControl">
                <label for="enableBtn">Enable</label>
                <div class="toggle-btn small">            
                    <input type="checkbox" id="enableBtn" class="cb-value" />
                    <span class="round-btn"></span>
                </div>
            </div>
        </div>
        <div class="loader"></div>
        <div class="row main-content">
            <div>
                <table id="jqGrid"></table>
                <div id="jqGridPager"></div>
            </div>
        </div>
        <asp:HiddenField ID="LoanFullID" runat="server" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" Runat="Server">
    <script type="text/javascript">  
        var DeleteRows = [];
        var timer;
        var lastSelection;

        $(function () {
            var obj = {
                Init: function () {
                    $('.loader').show();
                    obj.bindGrid();

                    $('#navbtnadd').addClass('disabled');
                    $('#navbtnsave').addClass('disabled');

                    $("#navbtnrefresh").on('click', function () {
                        $('.loader').show();
                        $('#ddlloanfullid').val('');
                        $('#txtSearchBar').val('');
                        obj.bindGrid();
                    });

                    $("#txtSearchBar").on("keyup", function () {
                        var self = this;
                        obj.search(self);
                    });

                    $("#ddlloanfullid").on("change", function () {
                        $('.loader').show();
                        obj.bindGrid();
                    });

                    $("#ddlCropPractice").on("change", function () {
                        $('.loader').show();
                        obj.bindGrid();
                    });

                    $("#navbtndownload").on("click", function () {
                        $("#jqGrid").jqGrid("exportToExcel", {
                            includeLabels: true,
                            includeGroupHeader: true,
                            includeFooter: true,
                            fileName: "Loan Collateral Detail.xlsx",
                            maxlength: 40
                        });
                    });

                    $('#navbtncolumns').click(function () {
                        $("#jqGrid").columnChooser({});
                    });

                    obj.initLoanSelect();
                },
                initLoanSelect: function () {
                    var hiddenVal = $('#ContentPlaceHolder1_LoanFullID').val();

                    if (hiddenVal) {
                        $('#txtSearchLoan').val(hiddenVal);
                    }

                    $('.select-loan').show();

                    $('#selectLoanBtn').click(function () {
                        $('.loader').show();
                        var loan = $('#txtSearchLoan').val();
                        $('#ContentPlaceHolder1_LoanFullID').val(loan);

                        obj.bindGrid();
                    });
                },
                getTableInfo: function () {
                    var loan_full_id = $('#ContentPlaceHolder1_LoanFullID').val();
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "LoanCollateralDetail.aspx/GetTableInfo",
                        data: JSON.stringify({ Loan_Full_ID: loan_full_id }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            var data = JSON.parse(res.d);

                            var table_info = data.Table_Info[0];
                            $('#txtTableName').html(table_info.Table_Name);
                            $('#txtLastUpdated').html(table_info.Last_Updated);
                            $('#txtRecordsCount').html(table_info.Records_Count);

                            if (data.CanEdit) {
                                $('#editControl').removeClass('disabled');
                            } else {
                                $('#editControl').addClass('disabled');
                            }

                            $('#enableControl').addClass('disabled');
                            
                        }
                    });
                },
                bindGrid: function () {
                    var loan_full_id = $('#ContentPlaceHolder1_LoanFullID').val();
                    $.ajax({
                        type: "POST",
                        url: "LoanCollateralDetail.aspx/LoadTable",
                        data: JSON.stringify({ Loan_Full_ID: loan_full_id }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var resData = JSON.parse(data.d);
                            $('.loader').show();
                            for (var i = 0; i < resData.length; i++) {
                                resData[i].rowid = i + 1;
                            }
                            obj.getTableInfo();
                            obj.loadgrid(resData);
                            $('.loader').hide();
                        }
                    });
                },
                loadgrid: function (data) {
                    $.jgrid.gridUnload("#jqGrid");
                    $("#jqGrid").jqGrid({
                        datatype: "local",
                        styleUI: 'Bootstrap',
                        data: data,
                        colModel: [
                            { label: 'rowid', name: 'rowid', width: 75, editable: false, key: true, hidden: true, hidedlg: true },
                            { label: 'Loan Col Detail ID', name: 'Collateral_ID', align: 'center', width: 90, editable: false, hidden: false },
                            { label: 'Loan Full ID', name: 'Loan_Full_ID', width: 90, editable: false, hidden: false },
                            {
                                label: 'Collateral Category',
                                name: 'Collateral_Category_Code',
                                width: 90,
                                align: "center",
                                editable: false
                            },
                            {
                                label: 'Crop',
                                name: 'Crop_Code',
                                width: 90,
                                align: "center",
                                editable: false
                            },
                            {
                                label: 'Crop Type',
                                name: 'Crop_Type',
                                width: 90,
                                align: "center",
                                editable: false
                            },
                            {
                                label: 'Col Description',
                                name: 'Collateral_Description',
                                width: 240,
                                align: "left",
                                classes: 'left',
                                editable: false
                            },
                            {
                                label: 'Meas Type Code',
                                name: 'Measure_Code',
                                width: 90,
                                align: "center",
                                editable: false,
                                hidden: false
                            },
                            {
                                label: 'Measurement Type Name',
                                name: 'Measurement_Type_Name',
                                width: 90,
                                align: "center",
                                editable: false,
                                hidden: true,
                                hidedlg: true
                            },
                            {
                                label: 'Meas Description',
                                name: 'Measure_Description',
                                width: 120,
                                align: "left",
                                classes: 'left',
                                editable: false,
                                hidden: false
                            },
                            {
                                label: 'Contract Type Code',
                                align: "center",
                                name: 'Contract_Type_Code',
                                width: 90,
                                editable: false,
                                hidden: false
                            },
                            {
                                label: 'Contract Type Name',
                                align: "left",
                                name: 'Contract_Type_Name',
                                width: 60,
                                editable: false,
                                hidden: true,
                                hidedlg: true
                            },
                            {
                                label: 'Buyer Assoc ID',
                                name: 'Assoc_ID',
                                width: 90,
                                align: "center",
                                editable: false,
                                hidden: false
                            },
                            {
                                label: 'Contract Num',
                                align: "left",
                                classes: 'left',
                                name: 'Contract',
                                width: 90,
                                editable: false,
                                hidden: false
                            },
                            {
                                label: 'Qty',
                                align: "right",
                                classes: 'right',
                                name: 'Qty',
                                width: 90,
                                editable: false,
                                hidden: false,
                                formatter: 'numberFormatter'
                            },
                            {
                                label: 'Price',
                                align: "right",
                                classes: 'right',
                                name: 'Price',
                                width: 90,
                                editable: false,
                                hidden: false,
                                formatter: 'priceFormatter'
                            },
                            {
                                label: 'Lien holder Assoc ID',
                                align: "left",
                                classes: 'left',
                                name: 'Lien_Holder',
                                width: 90,
                                editable: false,
                                hidden: false
                            },
                            {
                                label: 'Pre Lien Mkt Value',
                                name: 'Market_Value',
                                width: 120,
                                align: "right",
                                classes: 'right',
                                editable: false,
                                hidden: false,
                                formatter: 'totalBudgetFormatter'
                            },
                            {
                                label: 'Prior Lien Amount',
                                name: 'Prior_Lien_Amount',
                                width: 120,
                                align: "right",
                                classes: 'right',
                                editable: false,
                                hidden: false,
                                formatter: 'totalBudgetFormatter'
                            },
                            {
                                label: 'Mkt Value',
                                name: 'Net_Market_Value',
                                width: 120,
                                align: "right",
                                classes: 'right',
                                editable: false,
                                hidden: false,
                                formatter: 'totalBudgetFormatter'
                            },
                            {
                                label: 'Disc Mkt Value',
                                align: "right",
                                classes: 'right',
                                name: 'Disc_Mkt_Value',
                                width: 120,
                                editable: false,
                                hidden: false,
                                formatter: 'totalBudgetFormatter'
                            },
                            {
                                label: 'Ins Value',
                                align: "right",
                                classes: 'right',
                                name: 'Insurance_Value',
                                width: 90,
                                editable: false,
                                hidden: false,
                                formatter: 'totalBudgetFormatter'
                            },
                            {
                                label: 'Ins Disc Value',
                                align: "right",
                                classes: 'right',
                                name: 'Insurance_Disc_Value',
                                width: 120,
                                editable: false,
                                hidden: false,
                                formatter: 'totalBudgetFormatter'
                            },
                            {
                                label: 'Insured Flag',
                                align: "center",
                                name: 'Insured_Flag',
                                width: 90,
                                editable: false,
                                hidden: false
                            },
                            {
                                label: 'Income Ind',
                                align: "center",
                                name: 'Income_Ind',
                                width: 90,
                                editable: false,
                                hidden: false
                            },
                            {
                                label: 'Loan Other Income ID',
                                align: "center",
                                name: 'Loan_Other_Income_ID',
                                width: 60,
                                editable: false,
                                hidden: true,
                                hidedlg: true
                            },
                            {
                                label: 'Status',
                                name: 'Status_Updated',
                                align: "center",
                                classes: "center",
                                width: 90,
                                editable: false,
                                hidden: false
                            },
                        ],
                        viewrecords: true,
                        loadonce: true,
                        restoreAfterSelect: false,
                        saveAfterSelect: true,
                        sortable: true,
                        onPaging: obj.changePage,
                        height: 'auto',
                        rowNum: 100,
                        pager: "#jqGridPager"
                    });
                },
                search: function (self) {
                    if (timer) { clearTimeout(timer); }
                    timer = setTimeout(function () {
                        obj.changePage();
                        $("#jqGrid").jqGrid('filterInput', self.value);
                        var length = jQuery("#jqGrid").jqGrid('getGridParam', 'records');
                        $('#lblcount').html('Count of records: ' + length);
                    }, 0);
                },
                changePage: function () {
                    lastSelection = "";
                    var grid = $("#jqGrid");
                    var id = $('.inline-edit-cell').parent().parent().prop('id');
                    if (id != undefined) {
                        grid.jqGrid('saveRow', id);
                        grid.jqGrid('restoreRow', id);
                        var row = grid.jqGrid('getRowData', id);
                        if (!jQuery.isEmptyObject(row)) {
                            var dataobj = jQuery("#jqGrid").jqGrid('getGridParam', 'data');
                            index = dataobj.findIndex(x => x.rowid == row.rowid);
                            if (row.rowid > 0 && row.Actionstatus != 1) {
                                row.Actionstatus = 2;
                                grid.jqGrid('getGridParam', 'data')[index] = row;
                            }
                        }
                    }
                }
            }
            obj.Init();
        });
    </script>
</asp:Content>
