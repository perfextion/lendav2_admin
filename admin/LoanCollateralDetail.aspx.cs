﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_LoanCollateralDetail : BasePage
{
    public admin_LoanCollateralDetail()
    {
        Table_Name = "Loan_Collateral_Detail";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        base.Page_Load(sender, e);
        LoanFullID.Value = Loan_Full_ID;
    }

    [WebMethod]
    public static string GetTableInfo(string Loan_Full_ID)
    {
        Count_Query = "Declare @count int; Select @Count = Count(*) from Loan_Collateral_Detail";
        if (!string.IsNullOrEmpty(Loan_Full_ID))
        {
            Count_Query += " Where Loan_Full_ID like '%" + Loan_Full_ID + "%'";
        }
        return Get_Table_Info();
    }

    [WebMethod]
    public static Dictionary<string, string> GetLoanFullIds()
    {
        string sql_qry = "select distinct loan_full_id from Loan_Collateral_Detail order by loan_full_id";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return dt.AsEnumerable().ToDictionary(row => row[0].ToString(), row => row[0].ToString());
    }

    [WebMethod]
    public static string GetCollateralCategories()
    {
        string sql_qry = @"SELECT DISTINCT
	                            RC.Collateral_Category_Name,
	                            RC.Collateral_Category_Code
                            FROM [Loan_Collateral_Detail] LC
                            LEFT JOIN Ref_Collateral_Category RC ON LC.Collateral_Category_Code = RC.Collateral_Category_Code";

        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static string LoadTable(string Loan_Full_ID)
    {
        HttpContext.Current.Session["Loan_Full_ID"] = Loan_Full_ID;

        string stQry = string.Empty;
        stQry = @"SELECT
	                RCT.Contract_Type_Name,
	                RMT.Measurement_Type_Name,
	                LC.*,
                    CASE WHEN IsDelete = 0 OR IsDelete IS NULL THEN 1 ELSE 0 END as [Status_Updated]
                FROM [Loan_Collateral_Detail] LC
                LEFT JOIN Ref_Collateral_Category RC ON LC.Collateral_Category_Code = RC.Collateral_Category_Code
                LEFT JOIN Ref_Contract_Type RCT ON RCT.Contract_Type_Code = LC.Contract_Type_Code
                LEFT JOIN Ref_Measurement_Type RMT on RMT.Measurement_Type_Code = LC.Measure_Code";

        if(!string.IsNullOrEmpty(Loan_Full_ID))
        {
            stQry  += " Where Loan_Full_ID like '%" + Loan_Full_ID + "%'";
        }

        stQry += " Order by RC.[Sort_Order]";

        DataTable dtLoanBudget = gen_db_utils.gp_sql_get_datatable(stQry, dbKey);
        return JsonConvert.SerializeObject(dtLoanBudget);
    }
}