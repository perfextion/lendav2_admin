﻿<%@ Page Title="" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="LoanComment.aspx.cs" Inherits="admin_LoanComment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="loader"></div>
    <div class="col-md-12 col-sm-4" style="margin-top: 15px">
        <div class="col-md-3" style="margin-left: 10px;">
            <div class="form-inline">
                <div class="form-group">
                    <label for="lbljobtitlename">Loan Full Id :</label>
                    <select id="ddlloanfullid" class="form-control" style="width: 200px">
                        <option value="">Select</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-6" style="margin-left: 10px;">
            <div class="form-inline">
                <div class="form-group">
                    <label for="lbljobtitlename">Type Code :</label>
                    <select id="ddlTypeCode" class="form-control" style="width: 200px">
                        <option value="">Select</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="lbljobtitlename">Type Level Code :</label>
                    <select id="ddlLevelTypeCode" class="form-control" style="width: 200px">
                        <option value="">Select</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-2" style="margin-left: 10px; display: none">
            <a id="lnkrequestapi" style="cursor: pointer; display: block">request lenda api</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-11" style="padding-left: 51px; padding-top: 8px;">
            <asp:Label ID="lbl_table" runat="server" Text="lbl_table"></asp:Label>
        </div>
    </div>
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <%--style="width:758px"--%>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Loan Comment Details</h4>
                </div>
                <div class="modal-body">
                    <div class="row" style="margin-left: 5px">
                        <div id="divModelSave">
                            <div class="col-md-12">
                                <div id="divCommentID" class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtId">Loan Comment ID</label>
                                        <input type="text" id="txtCid" class="form-control" disabled="disabled" name="Loan Comment ID" style="width: 200px" />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtSearch">Loan Full ID</label>
                                        <input type="text" id="txtLoanfullid" class="form-control" name="Loan Full ID" style="width: 200px" disabled="disabled" />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtSearch">Loan_ID</label>
                                        <input type="text" id="txtLoanId" class="form-control" name="UserName" disabled="disabled" style="width: 200px" />
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtSearch">Loan Seq Num</label>
                                        <input type="text" id="txtLoanSeqNum" class="form-control" name="UserName" disabled="disabled" style="width: 200px" />

                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtSearch">Parent Comment ID</label>
                                        <input type="text" id="txtParentCommentID" class="form-control" name="ParentCommentID" style="width: 200px" disabled="disabled" />
                                    </div>
                                </div>
                                <div id="divCommentTime" class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtSearch">Comment Time</label>
                                        <input type="text" id="txtCommentTime" class="form-control" name="Comment Time" style="width: 200px" disabled="disabled" />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="lblzip">Parent Emoji ID</label>
                                        <input type="text" id="txtParentEmojiID" class="form-control" name="UserName" style="width: 200px" disabled="disabled" />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtSearch">Comment Emoji ID</label>
                                        <input type="text" id="txtCommentEmojiID" class="form-control" name="UserName" style="width: 200px" disabled="disabled" />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtSearch">Comment Read Ind</label>
                                        <input type="text" id="txtCommentReadInd" class="form-control" name="UserName" style="width: 200px" disabled="disabled" />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtSearch">User ID</label>
                                        <%--<input type="text" id="txtUserID" class="form-control" name="UserID" style="width: 200px" />--%>
                                        <select id="txtUserID" class="form-control" style="width: 80%;">
                                            <option value="">Select</option>
                                        </select>
                                    </div>
                                </div>


                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtSearch">Comment Type Code</label>
                                        <select id="txtCommentTypeCode" class="form-control" style="width: 80%;">
                                            <option value="">Select</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="lblstate">Comment Type Level Code</label>
                                        <input type="text" id="txtCommentTypeLevelCode" class="form-control" name="UserName" style="width: 200px" />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtSearch">Status</label>
                                        <select id="ddlStatus" class="form-control" style="width: 80%;">
                                            <option value="1">Active</option>
                                            <option value="0">InActive</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label for="txtSearch">Comment Text</label>
                                        <textarea id="txtCommentText" class="form-control" style="width: 485px"></textarea>
                                        <%-- <input type="text" id="txtCommentText" class="form-control" name="City" style="width: 200px" />--%>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-8">
                                </div>
                                <div class="col-md-4 text-center">
                                    <div class="btn btn-primary" id="btnModalSave">Save</div>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>

            </div>
            <asp:HiddenField ID="hdnOfficeID" runat="server" />
            <asp:HiddenField ID="hdnjobID" runat="server" />
        </div>
    </div>
    <asp:HiddenField ID="LoanCommentloanfullid" runat="server" />
    <asp:HiddenField ID="LoanCommenttypecode" runat="server" />
    <asp:HiddenField ID="LoanCommentlevelcode" runat="server" />
    <script>
        $('#navbtncolumns').addClass('disabled');
        $(function () {
            var obj = {
                Init: function () {
                    obj.buttonsave();
                    $('.loader').show();
                    obj.getLoanFullIds();
                    obj.GetCommentTypeCodes();
                    obj.GetCommentLevelCodes();
                    obj.changeLoanfullid();
                    obj.getusername();
                    obj.loadtable();

                    $('#navbtnadd').addClass('disabled');
                    $('#navbtnadd').on("click", function () {
                        obj.ClearModelData();
                        var loanfullid = $('#ddlloanfullid').val();
                        $('#txtLoanfullid').val(loanfullid);
                        $('#divCommentID').hide();
                        $('#divModelSave').show();
                        $('#myModal').modal('show');

                    });

                    $('#navbtnrefresh').on('click', function () {
                        $('.loader').show();
                        $('#ddlloanfullid').val('');
                        $('#txtSearchBar').val('');
                        obj.loadtable();
                    });

                    $("#txtCommentTypeLevelCode").keyup(function () {
                        var $this = $(this);
                        $this.val($this.val().replace(/[^\d.]/g, ''));
                    });
                    if ($('#ddlloanfullid').val() == '') {
                        $('#navbtnadd').addClass('disabled');
                    }
                    //sample testing
                    $('#lnkrequestapi').on("click", function () {
                        alert('test');

                        var val = 'test';
                        $.ajax({
                            type: "POST",
                            url: "http://lendav2vm.southcentralus.cloudapp.azure.com/api/Loan/ReadComment",
                            data: JSON.stringify({ "Loan_Comment_ID": "srikanthn", "Loan_Full_ID": "srikanthn", "User_ID": "1" }),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (res) {
                                var data = res;
                            }
                        });
                    });
                },
                getusername: function () {
                    $.ajax({
                        type: "POST",
                        url: "LoanComment.aspx/Getallusers",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            $.each(res.d, function (data, value) {
                                $("#txtUserID").append($("<option></option>").val(data).html(value));
                            });
                        }

                    });
                },
                ClearModelData: function () {
                    $('#txtCid').val('');
                    $('#txtLoanfullid').val('');
                    $('#txtLoanId').val('');
                    $('#txtLoanSeqNum').val('');
                    $('#txtParentCommentID').val('');
                    $('#txtUserID').val('');
                    $('#txtCommentTime').val('');
                    $('#txtCommentTypeCode').val('');
                    $('#txtCommentTypeLevelCode').val('');
                    $('#txtCommentText').val('');
                    $('#txtParentEmojiID').val('');
                    $('#txtCommentEmojiID').val('');
                    $('#txtCommentReadInd').val('');
                    $('#ddlStatus').val('');

                },
                buttonsave: function () {

                    $('#btnModalSave').click(function () {
                        $('.loader').show();
                        var dataobj = {}

                        dataobj.CID = $('#txtCid').val();
                        dataobj.Loanfullid = $('#txtLoanfullid').val();
                        dataobj.LoanId = $('#txtLoanId').val();
                        dataobj.LoanSeqNum = $('#txtLoanSeqNum').val();
                        dataobj.ParentCommentID = $('#txtParentCommentID').val();
                        dataobj.UserID = $('#txtUserID').val();
                        dataobj.CommentTime = $('#txtCommentTime').val();
                        dataobj.CommentTypeCode = $('#txtCommentTypeCode').val();
                        dataobj.CommentTypeLevelCode = $('#txtCommentTypeLevelCode').val();
                        dataobj.CommentText = $('#txtCommentText').val();
                        dataobj.ParentEmojiID = $('#txtParentEmojiID').val();
                        dataobj.CommentEmojiID = $('#txtCommentEmojiID').val();
                        dataobj.CommentReadInd = $('#txtCommentReadInd').val();
                        dataobj.Status = $('#ddlStatus').val();
                        var divModelSave = $('#divModelSave').show();
                        $('#spnException').hide();
                        $.ajax({
                            type: "POST",
                            url: "LoanComment.aspx/UpdateLoanCommentDetails",
                            data: JSON.stringify({ lst: dataobj }),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (data) {
                                $('.loader').show();
                                obj.loadtable();

                                $('#myModal').modal('hide');
                            },
                            failure: function (response) {
                            }
                        });
                    });
                },
                getLoanFullIds: function () {
                    $.ajax({
                        type: "POST",
                        url: "LoanComment.aspx/GetLoanFullIds",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            $.each(res.d, function (data, value) {
                                $("#ddlloanfullid").append($("<option></option>").val(data).html(value));
                            });
                        }
                    });
                },
                GetCommentTypeCodes: function () {
                    $.ajax({
                        type: "POST",
                        url: "LoanComment.aspx/GetCommentTypeCodes",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            $.each(res.d, function (data, value) {
                                $("#ddlTypeCode").append($("<option></option>").val(data).html(value));
                            });
                        }
                    });
                },
                GetCommentLevelCodes: function () {
                    $.ajax({
                        type: "POST",
                        url: "LoanComment.aspx/GetCommentLevelCodes",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            $.each(res.d, function (data, value) {
                                $("#ddlLevelTypeCode").append($("<option></option>").val(data).html(value));
                            });
                        }
                    });
                },
                loadtable: function () {
                    $('#ContentPlaceHolder1_lbl_table').text('');
                    var loanfullid = $('#ddlloanfullid').val();
                    var ddlTypeCode = $('#ddlTypeCode').val();
                    var ddlLevelTypeCode = $('#ddlLevelTypeCode').val();
                    $.ajax({
                        type: "POST",
                        url: "LoanComment.aspx/LoadTable",
                        data: JSON.stringify({ loanfullid: loanfullid, typecode: ddlTypeCode, levelcode: ddlLevelTypeCode }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            $('#ContentPlaceHolder1_lbl_table').html(res.d);
                            if ($('#ddlloanfullid').val() == '') {
                                $('#navbtnadd').addClass('disabled');
                            } else {
                                $('#navbtnadd').removeClass('disabled');
                            }
                            $('.loader').hide();
                        }
                    });
                },
                changeLoanfullid: function () {
                    $('#ddlloanfullid').change(function () {
                        obj.loadtable();
                    });
                    $('#ddlTypeCode').change(function () {
                        obj.loadtable();
                    });
                    $('#ddlLevelTypeCode').change(function () {
                        obj.loadtable();
                    });
                }

            }
            obj.Init();
        });
        function manageRecord(id) {
            $('#divCommentID').show();
            $.ajax({
                type: "POST",
                url: "LoanComment.aspx/GetLoanDetailsByCommentID",
                data: JSON.stringify({ Commentid: id }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (res) {
                    var objdata = JSON.parse(res.d);
                    var data = objdata[0];
                    $('#txtCid').val(data.Loan_Comment_ID);
                    $('#txtLoanfullid').val(data.Loan_Full_ID);
                    $('#txtLoanId').val(data.Loan_ID);
                    $('#txtLoanSeqNum').val(data.Loan_Seq_Num);
                    $('#txtParentCommentID').val(data.Parent_Comment_ID);
                    $('#txtUserID').val(data.User_ID);
                    $('#txtCommentTime').val(data.Comment_Time);
                    $('#txtCommentTypeCode').val(data.Comment_Type_Code);
                    $('#txtCommentTypeLevelCode').val(data.Comment_Type_Level_Code);
                    $('#txtCommentText').val(data.Comment_Text);
                    $('#txtParentEmojiID').val(data.Parent_Emoji_ID);
                    $('#txtCommentEmojiID').val(data.Comment_Emoji_ID);
                    $('#txtCommentReadInd').val(data.Comment_Read_Ind);
                    $('#ddlStatus').val(data.Status);
                }
            });

            $('#divModelSave').show();
            $('#myModal').modal('show');
        }
        function Delete(id) {
            var result = confirm("Are you sure you Want to delete?");
            $('.loader').show();

            if (result == true) {
                $.ajax({
                    type: "POST",
                    url: "LoanComment.aspx/DeleteByCommentID",
                    data: JSON.stringify({ Commentid: id }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        $('#ContentPlaceHolder1_lbl_table').text('');
                        var loanfullid = $('#ddlloanfullid').val();
                        $.ajax({
                            type: "POST",
                            url: "LoanComment.aspx/LoadTable",
                            data: JSON.stringify({ loanfullid: loanfullid }),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (res) {
                                $('#ContentPlaceHolder1_lbl_table').html(res.d);
                                $('.loader').hide();
                            }
                        });
                    },
                    failure: function (response) {
                        console.log(response.d);
                    }
                });
            }
            $('.loader').hide();
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" runat="Server">
    <script>
        $('#navbtnsave').addClass('disabled');       
        $('#navbtnadd').addClass('disabled');

        $("#navbtndownload").click(function () {
            var loan_full_id = $('#ddlloanfullid').val();
            exportTableToExcel_Exclude('tblLoanComment', 'LoanComment' + loan_full_id, [7,8]);
        });

        $('#txtSearchBar').on('keyup', function () {
            var value = $(this).val().toLowerCase();

            var count = 0;
            $("#tblLoanComment tbody tr").filter(function () {
                var lineStr = $(this).text().toLowerCase();
                if (lineStr.indexOf(value) === -1) {
                    $(this).hide();
                } else {
                    $(this).show();
                    count++;
                }
            });

            //$('#spanCount').html('Count : ' + count);

            if (count > 0) {
                $("#tblLoanComment tfoot").hide();
            } else {
                $("#tblLoanComment tfoot").show();
            }
        });
    </script>
</asp:Content>
