﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_LoanComment : System.Web.UI.Page
{
    static string dbKey = "gp_conn";
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!IsPostBack)
        //{
        //    lbl_table.Text = LoadTable("", "", "");
        //}
        if (Session["LoanCommentloanfullid"] != null)
        {
            LoanCommentloanfullid.Value = Session["LoanCommentloanfullid"].ToString();
        }
        if (Session["LoanCommenttypecode"] != null)
        {
            LoanCommenttypecode.Value = Session["LoanCommenttypecode"].ToString();
        }
        if (Session["LoanCommentlevelcode"] != null)
        {
            LoanCommentlevelcode.Value = Session["LoanCommentlevelcode"].ToString();
        }
    }
    [WebMethod]
    public static Dictionary<string, string> GetLoanFullIds()
    {
        string sql_qry = " select distinct Loan_Full_ID,Loan_Full_ID from [Loan_Comment]  where Loan_Full_ID <>''";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return dt.AsEnumerable().ToDictionary<DataRow, string, string>(row => row[0].ToString(),
                                       row => row[1].ToString());
    }
    [WebMethod]
    public static Dictionary<string, string> GetCommentTypeCodes()
    {
        string sql_qry = " select distinct Comment_Type_Code,Comment_Type_Code from [Loan_Comment]  ";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return dt.AsEnumerable().ToDictionary<DataRow, string, string>(row => row[0].ToString(),
                                       row => row[1].ToString());
    }
    [WebMethod]
    public static Dictionary<string, string> GetCommentLevelCodes()
    {
        string sql_qry = " select distinct Comment_Type_Level_Code,Comment_Type_Level_Code from [Loan_Comment]  ";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return dt.AsEnumerable().ToDictionary<DataRow, string, string>(row => row[0].ToString(),
                                       row => row[1].ToString());
    }
    public static string GetUserDetails()
    {
        string sql_qry = "SELECT UserID,Username,IsAdmin,User_Type_ID,FirstName,LastName,MI,Phone," +
                        "Email,Address,City,State,Zip,Job_Title_ID,Office_ID,Report_To_ID,Hire_Date," +
                        "Last_Committe_Assignment,Status,SysAdmin,RiskAdmin FROM Users";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);

    }
    [WebMethod]
    public static Dictionary<string, string> Getallusers()
    {
        string sql_qry = "select UserID,CONCAT( UserID,'.',Username) as Username from Users order by UserID";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);

        return dt.AsEnumerable().ToDictionary<DataRow, string, string>(row => row[0].ToString(),
                                       row => row[1].ToString());
    }
    [WebMethod]
    public static string LoadTable(string loanfullid, string typecode, string levelcode)
    {
        HttpContext.Current.Session["LoanCommentloanfullid"] = loanfullid;
        HttpContext.Current.Session["LoanCommenttypecode"] = typecode;
        HttpContext.Current.Session["LoanCommentlevelcode"] = levelcode;
        string str_sql = "";
        str_sql = "select Loan_Comment_ID,Loan_Full_ID,Loan_ID,Loan_Seq_Num,Parent_Comment_ID,CONCAT( b.UserID,'.',b.Username) as Username," +
            "Comment_Time,Comment_Type_Code,Comment_Type_Level_Code,Comment_Text,Parent_Emoji_ID,Comment_Emoji_ID," +
            "Comment_Read_Ind,a.Status,User_ID FROM Loan_Comment a LEFT JOIN Users b on a.User_ID = b.UserID where 1=1 ";
        if (loanfullid.Length > 0)
        {
            str_sql += " and Loan_Full_ID='" + loanfullid + "'";
        }
        if (typecode.Length > 0)
        {
            str_sql += " and Comment_Type_Code='" + typecode + "'";
        }
        if (levelcode.Length > 0)
        {
            str_sql += " and Comment_Type_Level_Code='" + levelcode + "'";
        }
        str_sql += "order by Comment_Time desc";
        DataTable dt1 = gen_db_utils.gp_sql_get_datatable(str_sql, dbKey);
        return GeneteateGrid(dt1);
    }

    [WebMethod]
    public static string GetLoanDetailsByCommentID(string Commentid)
    {
        string str_sql = "";
        str_sql = "SELECT Loan_Comment_ID,Loan_Full_ID,Loan_ID,Loan_Seq_Num,Parent_Comment_ID,User_ID," +
                        "Comment_Time,Comment_Type_Code,Comment_Type_Level_Code,Comment_Text,Parent_Emoji_ID,Comment_Emoji_ID," +
                        "Comment_Read_Ind,Status FROM Loan_Comment where Loan_Comment_ID='" + Commentid + "'";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(str_sql, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
    }
    [WebMethod]
    public static void DeleteByCommentID(string Commentid)
    {
        string str_sql = "";
        str_sql = "delete from Loan_Comment where Loan_Comment_ID='" + Commentid + "'";
        gen_db_utils.gp_sql_execute(str_sql, dbKey);
    }

    [WebMethod]
    public static string GeneteateGrid(DataTable dt1)
    {
        string str_return = "";
        if (dt1 != null && dt1.Rows.Count > 0)
        {
            str_return += "<div> <table id='tblLoanComment' class='table table-striped table-bordered' >  "
                + " <thead> "
                + " <tr> "
                + "<th> Comment ID </th>"
                + "<th> Loan Details </th>"
                //+ "<th> Parent_Comment_ID </th>"
                + "<th> User Name </th>"
                 + "<th> Comment Details </th>"
                + "<th> Comment Text </th>"
                //+ "<th> Parent_Emoji_ID </th>"
                + "<th> Comment Emojis </th>"
                + "<th> Status </th>"
                + "<th> Action </th>"
                + "<th> Delete</th>"
                 + "  </tr> "
                 + "    </thead> "
                 + "      <tbody>";
            foreach (DataRow dtrow in dt1.Rows)
            {
                str_return += "<tr>"
                + "<td> " + dtrow["Loan_Comment_ID"].ToString() + " </td>"

                + "<td width='20%'>Loan Full ID : " + dtrow["Loan_Full_ID"].ToString()
                + "<br>Loan ID :" + dtrow["Loan_ID"].ToString()
                + "<br>Loan Seq Num :" + dtrow["Loan_Seq_Num"].ToString() + " </td>"
                + "<td width='100px'> " + dtrow["Username"].ToString() + " </td>"
                + "<td width='20%'>Comment Time : " + dtrow["Comment_Time"].ToString()
                + "<br>Parent Comment ID :" + dtrow["Parent_Comment_ID"].ToString()
                + "<br>Comment Type Code :" + dtrow["Comment_Type_Code"].ToString()
                + "<br>Comment Type Level Code : " + dtrow["Comment_Type_Level_Code"].ToString() + " </td>"
                + "<td width='25%'> " + dtrow["Comment_Text"].ToString() + " </td>"
                + "<td width='15%'> Comment Emoji ID : " + dtrow["Comment_Emoji_ID"].ToString()
                + "<br> Parent Emoji ID : " + dtrow["Parent_Emoji_ID"].ToString()
                 + "<br> Comment Read Ind : " + dtrow["Comment_Read_Ind"].ToString() + " </td>"
                + "<td> " + dtrow["Status"].ToString() + " </td>"
                + "<td><a href =' javascript:manageRecord(" + dtrow["Loan_Comment_ID"] + ")' > Manage</a></td>"
                + "<td><a href =' javascript:Delete(" + dtrow["Loan_Comment_ID"] + ")'class='glyphicon glyphicon-trash' style='color:red' ></a></td>"

                + " </td>"
                + "</tr>";
            }
            str_return += " </tbody> "
                                    + " </table> "
                                      + "</div></div></div >";
        }

        else
        {
            str_return = "    No data available...";
        }
        return str_return;
    }
    [WebMethod]
    public static void UpdateLoanCommentDetails(Dictionary<string, string> lst)
    {
        string qry = "";
        LoanComment objmanag = new LoanComment();
        string json = JsonConvert.SerializeObject(lst);
        LoanComment lcm = JsonConvert.DeserializeObject<LoanComment>(json);
        //string qry = "";
        //    qry = "UPDATE Loan_Comment  SET Loan_Full_ID='"+lcm.Loanfullid+ "' ,Loan_ID='" + lcm.LoanId + "',Loan_Seq_Num='" + lcm.LoanSeqNum + "'," +
        //    "Parent_Comment_ID='" + lcm.ParentCommentID + "',User_ID='" + lcm.UserID + "',Comment_Time='" + lcm.CommentTime + "'," +
        //    "Comment_Type_Code='" + lcm.CommentTypeCode + "',Comment_Type_Level_Code='" + lcm.CommentTypeLevelCode + "'," +
        //    "Comment_Text='" + lcm.CommentText + "',Parent_Emoji_ID='" + lcm.ParentEmojiID + "',Comment_Emoji_ID='" + lcm.CommentEmojiID + "',Comment_Read_Ind='" + lcm.CommentReadInd + "'," +
        //    "Status='" + lcm.Status + "'   where Loan_Comment_ID = '" + lcm.Cid + "'";


        if (lcm.Cid != string.Empty)
        {
            qry = "UPDATE Loan_Comment  SET User_ID='" + lcm.UserID + "',Comment_Time=current_timestamp," +
        "Comment_Type_Code='" + lcm.CommentTypeCode + "',Comment_Type_Level_Code='" + lcm.CommentTypeLevelCode + "'," +
        "Comment_Text='" + lcm.CommentText + "',Status='" + lcm.Status + "'   where Loan_Comment_ID = '" + lcm.Cid + "'";
        }
        else
        {
            qry = "INSERT INTO  Loan_Comment(User_ID,Loan_Full_ID,Comment_Time,Comment_Type_Code,Comment_Type_Level_Code," +
                "Comment_Text,Status) VALUES('" + lcm.UserID + "','" + lcm.Loanfullid + "',current_timestamp,'" + lcm.CommentTypeCode + "'," +
                "'" + lcm.CommentTypeLevelCode + "','" + lcm.CommentText + "','" + lcm.Status + "')";
        }
        gen_db_utils.gp_sql_execute(qry, dbKey);
    }
}

public class LoanComment
{
    public string Cid { set; get; }
    public string Loanfullid { set; get; }
    public string LoanId { set; get; }
    public string LoanSeqNum { set; get; }
    public string ParentCommentID { set; get; }
    public string UserID { set; get; }
    public string CommentTime { set; get; }
    public string CommentTypeCode { set; get; }
    public string CommentTypeLevelCode { set; get; }
    public string CommentText { set; get; }
    public string ParentEmojiID { set; get; }
    public string CommentEmojiID { set; get; }
    public string CommentReadInd { set; get; }
    public string Status { set; get; }

    public LoanComment()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}
