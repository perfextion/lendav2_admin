﻿using Newtonsoft.Json;
using System;
using System.Data;
using System.Web;
using System.Web.Services;

public partial class admin_LoanComment_New : BasePage
{
    public admin_LoanComment_New()
    {
        Table_Name = "Loan_Comment";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        base.Page_Load(sender, e);
        LoanFullID.Value = Loan_Full_ID;
    }

    [WebMethod]
    public static string GetTableInfo(string Loan_Full_ID)
    {
        Count_Query = "Declare @Count int; Select @Count = Count(*) From Loan_Comment";
        if(!string.IsNullOrEmpty(Loan_Full_ID))
        {
            Count_Query += " Where Loan_Full_ID like '%" + Loan_Full_ID + "%'";
        }

        return Get_Table_Info();
    }

    [WebMethod]
    public static string LoadTable(string Loan_Full_ID)
    {
        HttpContext.Current.Session["Loan_Full_ID"] = Loan_Full_ID;

        string sql_qry = @"SELECT 
                            Loan_Comment_ID, 
                            Loan_Full_ID, 
                            Parent_Comment_ID, 
                            CONCAT( b.UserID, '.', b.Username) as Username,
                            Format(Comment_Time, 'MM/dd/yyyy hh:mm:ss tt') as [Comment_Time],
                            Comment_Type_Code,
                            Comment_Type_Level_Code,
                            Comment_Text,
                            System_Comment_Text,
                            Parent_Emoji_ID,
                            Comment_Emoji_ID,
                            Comment_Read_Ind,
                            a.Status,
                            User_ID 
                        FROM Loan_Comment a 
                        LEFT JOIN Users b on a.User_ID = b.UserID";

        if(!string.IsNullOrEmpty(Loan_Full_ID))
        {
            sql_qry += " Where Loan_Full_ID like '%" + Loan_Full_ID + "%'";
        }

        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return JsonConvert.SerializeObject(dt);
    }
}