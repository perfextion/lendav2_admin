﻿<%@ Page Title="" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="LoanCommittee.aspx.cs" Inherits="admin_LoanCommittee" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    


    <link href='../css/fullcalendar.min.css' rel='stylesheet' />
    <link href='../css/fullcalendar.print.min.css' rel='stylesheet' media='print' />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" />
    <script src='../js/moment.min.js'></script>
    <script src='../js/fullcalendar/jquery.min.js'></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src='../js/fullcalendar/fullcalendar.min.js'></script>

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <div class="loader"></div>
    <div class="col-md-12 col-sm-4" style="margin-top: 15px">
        <div class="col-md-10" style="margin-left: 10px;">
            <div class="form-inline">
                <div class="form-group">
                    <label for="lbljobtitlename">Loan Full Id :</label>
                    <select id="ddlloanfullid" class="form-control" style="width: 200px">
                        <option value="">Select</option>
                    </select>
                </div>
               
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-11" style="padding-left: 51px; padding-top: 8px;">
            <asp:Label ID="lbl_table" runat="server" Text="lbl_table"></asp:Label>
        </div>
    </div>
    
    <script>
        $('#navbtncolumns').addClass('disabled');
        $(function () {
            var obj = {
                Init: function () {
                    $('.loader').hide();
                    obj.changeLoanfullid();
                    obj.getLoanFullIds();
                  //  obj.managerecord(vote, committeeid);
                    $("#txtCommentTypeLevelCode").keyup(function () {
                        var $this = $(this);
                        $this.val($this.val().replace(/[^\d.]/g, ''));
                    });
                },
               
                loadtable: function () {
                    
                    $('#ContentPlaceHolder1_lbl_table').text('');
                    var loanfullid = $('#ddlloanfullid').val();
                    $.ajax({
                        type: "POST",
                        url: "LoanCommittee.aspx/LoadTable",
                        data: JSON.stringify({ loanfullid: loanfullid }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            $('#ContentPlaceHolder1_lbl_table').html(res.d);
                            $('.loader').hide();
                        }
                    });
                },
                getLoanFullIds: function () {
                    $.ajax({
                        type: "POST",
                        url: "LoanCommittee.aspx/GetLoanFullIds",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            $.each(res.d, function (data, value) {
                                $("#ddlloanfullid").append($("<option></option>").val(data).html(value));
                            });
                        }
                    });
                },
                changeLoanfullid: function () {
                    
                    $('#ddlloanfullid').change(function () {
                        $('#ContentPlaceHolder1_lbl_table').text('');
                        var loanfullid = $('#ddlloanfullid').val();
                        $.ajax({
                            type: "POST",
                            url: "LoanCommittee.aspx/LoadTable",
                            data: JSON.stringify({ loanfullid: loanfullid }),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (res) {
                                $('.loader').show();
                                $('#ContentPlaceHolder1_lbl_table').html(res.d);
                                //if ($('#ddlloanfullid').val() == '') {
                                //    $('#btnAddComment').hide();
                                //} else
                                //    $('#btnAddComment').show();
                                //  obj.autosearch();
                                $('.loader').hide();
                            }
                        });
                    });
                },
                //managerecord: function (vote, committeeid) {
                //    
                //    $.ajax({
                //            type: "POST",
                //            url: "LoanCommittee.aspx/Updatecommittee",
                //            data: JSON.stringify({ Vote:vote,Commiteeid: committeeid}),
                //            contentType: "application/json; charset=utf-8",
                //            dataType: "json",
                //            success: function (data) {
                //                $('.loader').show();
                //                obj.loadtable(); 
                //            },
                //            failure: function (response) {
                //       }
                //    });
                //}   
            }
            obj.Init();
        });
        function manageRecord(vote,committeeid) {
                        $.ajax({
                            type: "POST",
                            url: "LoanCommittee.aspx/Updatecommittee",
                            data: JSON.stringify({ Vote:vote,Commiteeid: committeeid}),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (data) {
                                $('.loader').show();
                                var loanfullid = $('#ddlloanfullid').val();
                                $.ajax({
                                    type: "POST",
                                    url: "LoanCommittee.aspx/LoadTable",
                                    data: JSON.stringify({ loanfullid: loanfullid }),
                                    contentType: "application/json; charset=utf-8",
                                    dataType: "json",
                                    success: function (res) {
                                        $('#ContentPlaceHolder1_lbl_table').html(res.d);
                                        $('.loader').hide();
                                    }
                                }); 
                            },
                            failure: function (response) {
                       }
             });
        }
    </script>

    

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" Runat="Server">
    <script>
        $('#navbtnsave').addClass('disabled');
        $('#navbtnrefresh').addClass('disabled');
        $('#navbtnadd').addClass('disabled');
        //$('#navbtndownload').addClass('disabled');

        $("#navbtndownload").click(function () {
            var loan_full_id = $('#ContentPlaceHolder1_ddlLoanfullid').val();
            exportTableToExcel_Exclude('tblLoanCommittee', loan_full_id ? 'LoanCommittee_' + loan_full_id : 'LoanCommittee',[6,7]);
        });

        $('#txtSearchBar').on('keyup', function () {
            var value = $(this).val().toLowerCase();
            var count = 0;
            $("#tblLoanCommittee tbody tr").filter(function () {
                var lineStr = $(this).text().toLowerCase();
                if (lineStr.indexOf(value) === -1) {
                    $(this).hide();
                } else {
                    $(this).show();
                    count++;
                }
            });

            //$('#spanCount').html('Count : ' + count);

            if (count > 0) {
                $("#tblLoanCommittee tfoot").hide();
            } else {
                $("#tblLoanCommittee tfoot").show();
            }
        });
    </script>
</asp:Content>

