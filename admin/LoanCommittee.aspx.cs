﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_LoanCommittee : System.Web.UI.Page
{
    static string dbKey = "gp_conn";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            lbl_table.Text = LoadTable("");
        }
    }

    [WebMethod]
    public static Dictionary<string, string> GetLoanFullIds()
    {
        string sql_qry = " select distinct Loan_Full_ID,Loan_Full_ID from [Loan_Comment]  where Loan_Full_ID <>''";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return dt.AsEnumerable().ToDictionary<DataRow, string, string>(row => row[0].ToString(),
                                       row => row[1].ToString());
    }
    [WebMethod]
    public static string LoadTable(string loanfullid)
    {
        string str_sql = "";
        str_sql = "select CM_ID,Loan_Full_ID,CONCAT( b.UserID,'.',b.Username) as Username,User_ID,Role," +
            "c.job_title,Added_Date,Voted_Date,Vote from Loan_Committee a " +
            "LEFT JOIN Users b on a.User_ID = b.UserID " +
            "LEFT JOIN Job_Title c on a.Role = c.Job_title_id";
        if (loanfullid.Length > 0)
        {
            str_sql += " where Loan_Full_ID='" + loanfullid + "'";
        }
        str_sql += " order by CM_ID";
        DataTable dt1 = gen_db_utils.gp_sql_get_datatable(str_sql, dbKey);
        return GeneteateGrid(dt1);
    }

    public static string GeneteateGrid(DataTable dt1)
    {
        string str_return = "";
        if (dt1 != null && dt1.Rows.Count > 0)
        {
            str_return += "<div> <table id='tblLoanCommittee' class='table table-striped table-bordered' >  "
                + " <thead> "
                + " <tr> "
                + "<th> Loan Committee ID </th>"
                 + "<th> Loan Full ID </th>"
                + "<th> User ID </th>"
                + "<th> Job title </th>"
                + "<th> Added_Date </th>"
                + "<th> Voted_Date </th>"
                + "<th class='action'> Status </th>"
                + "<th class='action'> Action </th>"
                 + "  </tr> "
                 + "    </thead> "
                 + "      <tbody>";
            foreach (DataRow dtrow in dt1.Rows)
            {
                string value = "";
                string tdvalue = "";
                string vote = dtrow["Vote"].ToString();
                if (vote == "1")
                {
                    value = "Approved &nbsp&nbsp  <a href = 'javascript:manageRecord(" + 0 + "," + dtrow["CM_ID"].ToString() + ")' ><img src='../images/delete-icon.jpg' height='20' width='20' alt='' onclick='' /></a>";
                }
                else if (vote == "2")
                {
                    value = "Declined &nbsp&nbsp&nbsp  <a href = 'javascript:manageRecord(" + 0 + "," + dtrow["CM_ID"].ToString() + ")' ><img src='../images/delete-icon.jpg' height='20' width='20' alt='' onclick='' /></a>";
                }
                if (vote == "1")
                {
                    tdvalue = "";
                }
                else if (vote == "2")
                {
                    tdvalue = "";
                }
                else
                {
                    tdvalue = "<a href = 'javascript:manageRecord(" + 1 + "," + dtrow["CM_ID"].ToString() + ")' class='btn btn-default'style='background-color:green; color:white;'> Approve </a>&nbsp" +
                              "<a href = 'javascript:manageRecord(" + 2 + "," + dtrow["CM_ID"].ToString() + ")' class='btn btn-default'style='background-color:brown; color:white;'> Decline </a>";
                }
                str_return += "<tr>"
                 + "<td> " + dtrow["CM_ID"].ToString() + " </td>"
                + "<td> " + dtrow["Loan_Full_ID"].ToString() + " </td>"
                + "<td> " + dtrow["Username"].ToString() + " </td>"
                + "<td> " + dtrow["job_title"].ToString() + " </td>"
                + "<td> " + dtrow["Added_Date"].ToString() + " </td>"
                + "<td> " + dtrow["Voted_Date"].ToString() + " </td>"
                + "<td class='action'> " + value + "</td>"
                + "<td class='action'> " + tdvalue + "</td>"
                   + " </td>"
                + "</tr>";
            }
            str_return += " </tbody> "
                + "<tfoot style='display: none'><tr><td colspan='8'>No Data Found</td></tr></tfoot>"
                                    + " </table> "
                                      + "</div></div></div >";
        }

        else
        {
            str_return = "    No data available...";
        }
        return str_return;
    }


    [WebMethod]
    public static void Updatecommittee(string Vote, string Commiteeid)
    {
        string qry = "";
        if (Vote == "0")
            qry = "update Loan_Committee set Voted_Date=NULL,Vote=NULL,Vote_Emoji_Type=NULL, Status=NULL where CM_ID='" + Commiteeid + "'";
        else
            qry = "update Loan_Committee set Voted_Date=current_timestamp,Vote='" + Vote + "' where CM_ID='" + Commiteeid + "'";
        gen_db_utils.gp_sql_execute(qry, dbKey);
    }
}

