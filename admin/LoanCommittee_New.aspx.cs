﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_LoanCommittee_New : BasePage
{
    public admin_LoanCommittee_New()
    {
        Table_Name = "Loan_Committee";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        base.Page_Load(sender, e);
        LoanFullID.Value = Loan_Full_ID;
    }

    [WebMethod]
    public static string GetTableInfo(string Loan_Full_ID)
    {
        Count_Query += "Declare @Count int; Select @Count  = Count(*) From Loan_Committee";
        if(!string.IsNullOrEmpty(Loan_Full_ID))
        {
            Count_Query += " Where Loan_Full_ID like '%" + Loan_Full_ID + "%'";
        }
        return Get_Table_Info();
    }

    [WebMethod]
    public static string LoadTable(string Loan_Full_ID)
    {
        HttpContext.Current.Session["Loan_Full_ID"] = Loan_Full_ID;
        string sql_qry = @"select 
                            CM_ID,
                            Loan_Full_ID,
                            CONCAT( b.UserID,'.',b.Username) as Username,
                            User_ID,
                            Role,
                            FORMAT(Added_Date, 'MM/dd/yyyy hh:mm:ss tt') as [Added_Date],
                            FORMAT(Voted_Date, 'MM/dd/yyyy hh:mm:ss tt') as [Voted_Date],
                            Vote, 
                            a.[Status] 
                        from Loan_Committee a 
                        LEFT JOIN Users b on a.User_ID = b.UserID";

        if (!string.IsNullOrEmpty(Loan_Full_ID))
        {
            sql_qry += " Where Loan_Full_ID like '%" + Loan_Full_ID + "%'";
        }

        sql_qry += " order by cm_id";

        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static void Updatecommittee(string Vote, string Commiteeid)
    {
        string qry = string.Empty;

        if (Vote == "0")
        {
            qry = "update Loan_Committee set Voted_Date = NULL,Vote=NULL,Vote_Emoji_Type=NULL, Status=NULL where CM_ID='" + Commiteeid + "'";
        }
        else
        {
            qry = "update Loan_Committee set Voted_Date=current_timestamp,Vote='" + Vote + "' where CM_ID='" + Commiteeid + "'";
        }

        gen_db_utils.gp_sql_execute(qry, dbKey);

        Update_Table_Audit();
    }
}