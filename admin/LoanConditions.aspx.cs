﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_LoanConditions : System.Web.UI.Page
{
    static string dbKey = "gp_conn";
    protected void Page_Load(object sender, EventArgs e)
    {
    }
    [WebMethod]
    public static string GetLoanConditions()
    {
        
        string sql_qry = "SELECT 0 as Actionstatus,User_PA_ID,User_ID,Loan_Full_ID,PA_ID,Status FROM User_Pending_Action";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
    }
    [WebMethod]
    public static void SaveLoanConditions(dynamic lst)
    {
        foreach (var item in lst)
        {
            string qry = "";
            int val = Convert.ToInt32(item["Actionstatus"]);
            if (val == 1)
            {
                qry = "INSERT INTO User_Pending_Action (User_ID,Loan_Full_ID,PA_ID,Status)" +
                    " VALUES('" + item["User_ID"].Replace("'", "''") + "','" + item["Loan_Full_ID"].Replace("'", "''") + "'," +
                    "'" + item["PA_ID"].Replace("'", "''") + "','" + item["Status"].Replace("'", "''") + "')";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 2)
            {
                qry = "UPDATE User_Pending_Action SET User_ID = '" + item["User_ID"].Replace("'", "''") + "' ," +
                    "Loan_Full_ID = '" + item["Loan_Full_ID"].Replace("'", "''") + "',PA_ID = '" + item["PA_ID"].Replace("'", "''") + "'," +
                    "Status = '" + item["Status"].Replace("'", "''") + "' where User_PA_ID=" + item["User_PA_ID"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 3)
            {
                qry = "DELETE FROM User_Pending_Action  where User_PA_ID=" + item["User_PA_ID"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
        }
    }


}