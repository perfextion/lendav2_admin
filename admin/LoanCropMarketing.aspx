﻿<%@ Page Title="Loan Crop Contract" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="LoanCropMarketing.aspx.cs" Inherits="admin_LoanCropMarketing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <style>
        .cvteste {
            padding-right: 14px !important;
        }

        #content {
            padding: 14px !important;
        }
    </style>

    <div class="ref-collateral-category-section loan-transaction-table">
        <div class="row switch-controls">
            <div class="switch-control disabled" id="editControl">
                <label for="editBtn">Edit</label>
                <div class="toggle-btn small">            
                    <input type="checkbox" id="editBtn" class="cb-value" />
                    <span class="round-btn"></span>
                </div>
            </div>
            <div class="switch-control disabled" id="enableControl">
                <label for="enableBtn">Enable</label>
                <div class="toggle-btn small">            
                    <input type="checkbox" id="enableBtn" class="cb-value" />
                    <span class="round-btn"></span>
                </div>
            </div>
        </div>
        <div class="loader"></div>
        <div class="row main-content">
            <div>
                <table id="jqGrid"></table>
                <div id="jqGridPager"></div>
            </div>
        </div>
        <asp:HiddenField ID="LoanFullID" runat="server" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" Runat="Server">
    <asp:HiddenField ID="loanfullid" runat="server" />
    <script type="text/javascript">  
        var DeleteRows = [];
        var timer;
        var lastSelection;
        var cropPracticeType = {};

        $('#navbtnsave').addClass('disabled');

        $(function () {
            var obj = {
                Init: function () {
                    $('.loader').show();

                    obj.bindGrid();

                    $('#navbtnadd').addClass('disabled');

                    $("#navbtnrefresh").on('click', function () {
                        $('.loader').show();
                        $("#ddlCropPractice").val('');
                        $('#ddlloanfullid').val('');
                        $('#txtSearchBar').val('');
                        obj.bindGrid();
                    });

                    $("#txtSearchBar").on("keyup", function () {
                        var self = this;
                        obj.search(self);
                    });

                    $("#ddlloanfullid").on("change", function () {
                        $('.loader').show();
                        $('#loanfullid').val($("#ddlloanfullid").val());
                        obj.bindGrid();
                    });

                    $("#ddlCropPractice").on("change", function () {
                        $('.loader').show();
                        obj.bindGrid();
                    });

                    $("#navbtndownload").on("click", function () {
                        $("#jqGrid").jqGrid("exportToExcel", {
                            includeLabels: true,
                            includeGroupHeader: true,
                            includeFooter: true,
                            fileName: "Loan Crop Marketing.xlsx",
                            maxlength: 40
                        });
                    });

                    $('#navbtncolumns').click(function () {
                        $("#jqGrid").columnChooser({});
                    });

                    obj.initLoanSelect();
                },
                initLoanSelect: function () {
                    var hiddenVal = $('#ContentPlaceHolder1_LoanFullID').val();

                    if (hiddenVal) {
                        $('#txtSearchLoan').val(hiddenVal);
                    }

                    $('.select-loan').show();

                    $('#selectLoanBtn').click(function () {
                        $('.loader').show();
                        var loan = $('#txtSearchLoan').val();
                        $('#ContentPlaceHolder1_LoanFullID').val(loan);

                        obj.bindGrid();
                    });
                },
                getTableInfo: function () {
                    var hiddenVal = $('#ContentPlaceHolder1_LoanFullID').val();
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "LoanCropMarketing.aspx/GetTableInfo",
                        data: JSON.stringify({ Loan_Full_ID: hiddenVal }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            var data = JSON.parse(res.d);

                            var table_info = data.Table_Info[0];
                            $('#txtTableName').html(table_info.Table_Name);
                            $('#txtLastUpdated').html(table_info.Last_Updated);
                            $('#txtRecordsCount').html(table_info.Records_Count);

                            if (data.CanEdit) {
                                $('#editControl').removeClass('disabled');
                            } else {
                                $('#editControl').addClass('disabled');
                            }

                            $('#enableControl').addClass('disabled');
                            
                        }
                    });
                },
                bindGrid: function () {
                    var hiddenVal = $('#ContentPlaceHolder1_LoanFullID').val();
                    $.ajax({
                        type: "POST",
                        url: "LoanCropMarketing.aspx/LoadTable",
                        data: JSON.stringify({ Loan_Full_ID: hiddenVal }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var resData = JSON.parse(data.d);                            
                            for (var i = 0; i < resData.length; i++) {
                                resData[i].rowid = i + 1;
                            }
                            obj.getTableInfo();
                            obj.loadgrid(resData);
                            $('.loader').hide();
                        }
                    });
                },
                loadgrid: function (data) {
                    $.jgrid.gridUnload("#jqGrid");
                    $("#jqGrid").jqGrid({
                        datatype: "local",
                        styleUI: 'Bootstrap',
                        data: data,
                        colModel: [
                            { label: 'rowid', name: 'rowid', width: 75, editable: false, key: true, hidden: true, hidedlg: true },
                            { label: 'Loan Crop Contract ID', name: 'Contract_ID', align: 'center', width: 150, editable: false, hidden: false },
                            { label: 'Loan Full ID', name: 'Loan_Full_ID', width: 90, editable: false, hidden: false },
                            {
                                label: 'Buyer Assoc ID',
                                name: 'Assoc_ID',
                                width: 120,
                                align: "center",
                                editable: false
                            },
                            {
                                label: 'Contract Num',
                                name: 'Contract',
                                width: 120,
                                align: "left",
                                editable: false,
                                hidden: false
                            },                            
                            {
                                label: 'Crop Code',
                                name: 'Crop_Code',
                                width: 90,
                                align: "center",
                                editable: false
                            },
                            {
                                label: 'Crop Type',
                                name: 'Crop_Type_Code',
                                width: 90,
                                align: "center",
                                editable: false
                            },
                            {
                                label: 'Contract Type Code',
                                name: 'Contract_Type_Code',
                                width: 150,
                                align: "center",
                                editable: false
                            },
                            {
                                label: 'Description Text',
                                align: "left",
                                name: 'Description_Text',
                                width: 120,
                                editable: false
                            },
                            {
                                label: 'Qty',
                                align: "right",
                                classes: 'right',
                                name: 'Quantity',
                                width: 120,
                                editable: false,
                                hidden: false,
                                formatter: 'numberFormatter'
                            },
                            {
                                label: 'Price',
                                align: "right",
                                classes: 'right',
                                name: 'Price',
                                width: 120,
                                editable: false,
                                hidden: false,
                                formatter: 'priceFormatter'
                            },
                            {
                                label: 'Market Value',
                                align: "right",
                                classes: 'right',
                                name: 'Market_Value',
                                width: 120,
                                editable: false,
                                formatter: 'amountFormatter',
                                hidden: true,
                                hidedlg: true
                            },
                            {
                                label: 'Contract Pct',
                                align: "right",
                                classes: 'right',
                                name: 'Contract_Per',
                                width: 90,
                                editable: false,
                                formatter: 'percentFormatter',
                                hidden: true,
                                hidedlg: true
                            },                    
                            {
                                label: 'Status',
                                name: 'Status',
                                align: "center",
                                classes: "center",
                                width: 90,
                                editable: false,
                                hidden: false
                            },
                            { label: 'Actionstatus', name: 'Actionstatus', width: 70, editable: false, hidden: true, exportcol: false, hidedlg: true }
                            //{ label: '', name: '', width: 30, align: 'center', formatter: obj.deleteLink },
                        ],
                        viewrecords: true,
                        loadonce: true,
                        restoreAfterSelect: false,
                        saveAfterSelect: true,
                        sortable: true,
                        onPaging: obj.changePage,
                        height: 'auto',
                        rowNum: 100,
                        pager: "#jqGridPager"
                    });
                },
                search: function (self) {
                    if (timer) { clearTimeout(timer); }
                    timer = setTimeout(function () {
                        obj.changePage();
                        $("#jqGrid").jqGrid('filterInput', self.value);
                        var length = jQuery("#jqGrid").jqGrid('getGridParam', 'records');
                        $('#txtRecordsCount').html('Records Count: ' + length);
                    }, 0);
                },
                changePage: function () {

                    lastSelection = "";
                    var grid = $("#jqGrid");
                    var id = $('.inline-edit-cell').parent().parent().prop('id');
                    if (id != undefined) {
                        grid.jqGrid('saveRow', id);
                        grid.jqGrid('restoreRow', id);
                        var row = grid.jqGrid('getRowData', id);
                        if (!jQuery.isEmptyObject(row)) {
                            var dataobj = jQuery("#jqGrid").jqGrid('getGridParam', 'data');
                            index = dataobj.findIndex(x => x.rowid == row.rowid);
                            if (row.rowid > 0 && row.Actionstatus != 1) {
                                row.Actionstatus = 2;
                                grid.jqGrid('getGridParam', 'data')[index] = row;
                            }
                        }
                    }
                }
            }
            obj.Init();
        });
    </script>
</asp:Content>

