﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_LoanCropPractice : BasePage
{
    public admin_LoanCropPractice()
    {
        Table_Name = "Loan_Crop_Practice";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        base.Page_Load(sender, e);
        LoanFullID.Value = Loan_Full_ID;
    }

    [WebMethod]
    public static string GetTableInfo(string Loan_Full_ID)
    {
        Count_Query = "Declare @count int; Select @Count = Count(*) From Loan_Crop_Practice";

        if(!string.IsNullOrEmpty(Loan_Full_ID))
        {
            Count_Query += " Where Loan_Full_ID like '%" + Loan_Full_ID + "%'";
        }

        return Get_Table_Info();
    }

    [WebMethod]
    public static Dictionary<string, string> GetLoanFullIds()
    {
        string sql_qry = "select distinct Loan_Full_ID from [Loan_Crop_Practice] Where Loan_Full_ID IS NOT NULL OR Loan_Full_ID != ''  order by Loan_Full_ID";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return dt.AsEnumerable().ToDictionary(row => row[0].ToString(), row => row[0].ToString());
    }

    [WebMethod]
    public static string GetCropPractices()
    {
        string sql_qry = @"select DISTINCT LC.Crop_Practice_ID, VC.Crop_Full_Key from Loan_Crop_Practice LC
                        JOIN V_Crop_Price_Details VC on LC.Crop_Practice_ID = VC.Crop_And_Practice_ID";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static string LoadTable(string Loan_Full_ID)
    {
        HttpContext.Current.Session["Loan_Full_ID"] = Loan_Full_ID;

        string sql_qry = @"select VC.Crop_Full_Key, LC.*, 1 as [ActionStatus] from Loan_Crop_Practice LC
                            JOIN V_Crop_Price_Details VC on LC.Crop_Practice_ID = VC.Crop_And_Practice_ID";

        if (!string.IsNullOrEmpty(Loan_Full_ID))
        {
            sql_qry += " Where Loan_Full_ID like '%" + Loan_Full_ID + "%'";
        }

        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return JsonConvert.SerializeObject(dt);
    }
}