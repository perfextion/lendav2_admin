﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_LoanCropPracticeYield : BasePage
{
    public admin_LoanCropPracticeYield()
    {
        Table_Name = "Loan_Crop_Type_Practice_Type_Yield";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        base.Page_Load(sender, e);
        LoanFullID.Value = Loan_Full_ID;
    }

    [WebMethod]
    public static string GetTableInfo(string Loan_Full_ID)
    {
        Count_Query = "Declare @count int; Select @Count = Count(*) From Loan_Crop_Type_Practice_Type_Yield";

        if (!string.IsNullOrEmpty(Loan_Full_ID))
        {
            Count_Query += " Where Loan_Full_ID like '%" + Loan_Full_ID + "%'";
        }

        return Get_Table_Info();
    }

    [WebMethod]
    public static Dictionary<string, string> GetLoanFullIds()
    {
        string sql_qry = "select distinct loan_full_id from Loan_Crop_Type_Practice_Type_Yield order by loan_full_id";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return dt.AsEnumerable().ToDictionary(row => row[0].ToString(), row => row[0].ToString());
    }

    [WebMethod]
    public static string GetCrops()
    {
        string sql_qry = @"SELECT DISTINCT      
	                            LC.Crop_ID,
		                        VC.Crop_Full_Key 
                            from Loan_Crop_Type_Practice_Type_Yield LC
                            JOIN V_Crop_Price_Details VC on LC.Crop_ID = VC.Crop_And_Practice_ID";

        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static string LoadTable(string Loan_Full_ID)
    {
        HttpContext.Current.Session["Loan_Full_ID"] = Loan_Full_ID;
        string sql_qry = @"select * , case when LC.IsDelete <> 1 or LC.IsDelete IS null then 1 else 0 end as [Inactive_Status], 
                        VC.Crop_Code, VC.Crop_Name, VC.Crop_Full_Key, VC.Irr_Prac_Code, VC.Crop_Type_Code,VC.Crop_Prac_Code  
                        from Loan_Crop_Type_Practice_Type_Yield LC
                        JOIN V_Crop_Price_Details VC on LC.Crop_ID = VC.Crop_And_Practice_ID";

        if(!string.IsNullOrEmpty(Loan_Full_ID))
        {
            sql_qry += " where Loan_Full_ID like '%" + Loan_Full_ID + "%'";
        }

        sql_qry += " ORDER BY Inactive_Status";

        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return JsonConvert.SerializeObject(dt);
    }
}