﻿<%@ Page Title="Loan Crop Price" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="LoanCropPrice.aspx.cs" Inherits="admin_LoanCropPrice" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style>
        .ui-jqgrid .ui-jqgrid-htable th div {
            height: auto;
            overflow: hidden;
            padding-right: 4px;
            padding-top: 2px;
            position: relative;
            vertical-align: text-top;
            white-space: normal !important;
        }

        .ui-jqgrid tr.jqgrow td {
            word-wrap: break-word; /* IE 5.5+ and CSS3 */
            white-space: pre-wrap; /* CSS3 */
            white-space: normal !important;
            height: auto;
            vertical-align: text-top;
            padding-top: 2px;
            padding-bottom: 3px;
        }

        .ui-jqgrid .ui-jqgrid-htable th {
            height: 45px;
        }

        .cvteste {
            padding-right: 14px !important;
        }

        .left {
            padding-left: 14px !important;
        }

        #content  {
            padding: 14px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">   

    <div class="ref-collateral-category-section loan-transaction-table">
        <div class="row switch-controls">
            <div class="switch-control disabled" id="editControl">
                <label for="editBtn">Edit</label>
                <div class="toggle-btn small">            
                    <input type="checkbox" id="editBtn" class="cb-value" />
                    <span class="round-btn"></span>
                </div>
            </div>
            <div class="switch-control disabled" id="enableControl">
                <label for="enableBtn">Enable</label>
                <div class="toggle-btn small">            
                    <input type="checkbox" id="enableBtn" class="cb-value" />
                    <span class="round-btn"></span>
                </div>
            </div>
        </div>
        <div class="loader"></div>
        <div class="row main-content">
            <div>
                <table id="jqGrid"></table>
                <div id="jqGridPager"></div>
            </div>
        </div>
         <asp:HiddenField ID="LoanFullID" runat="server" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" Runat="Server">
    <script type="text/javascript">  
        var DeleteRows = [];
        var timer;
        var lastSelection;

        $(function () {
            var obj = {
                Init: function () {
                    $('.loader').show();
                    obj.bindGrid();

                    $('#navbtnadd').addClass('disabled');
                    $('#navbtnsave').addClass('disabled');

                    $("#navbtnrefresh").on('click', function () {
                        $('.loader').show();
                        $('#ddlloanfullid').val('');
                        $('#txtSearchBar').val('');
                        obj.bindGrid();
                    });

                    $("#txtSearchBar").on("keyup", function () {
                        var self = this;
                        obj.search(self);
                    });

                    $("#navbtndownload").on("click", function () {
                        $("#jqGrid").jqGrid("exportToExcel", {
                            includeLabels: true,
                            includeGroupHeader: true,
                            includeFooter: true,
                            fileName: "Loan Crop Price.xlsx",
                            maxlength: 40
                        });
                    });

                    $('#navbtncolumns').click(function () {
                        $("#jqGrid").columnChooser({});
                    });

                    obj.initLoanSelect();
                },
                initLoanSelect: function () {
                    var hiddenVal = $('#ContentPlaceHolder1_LoanFullID').val();

                    if (hiddenVal) {
                        $('#txtSearchLoan').val(hiddenVal);
                    }

                    $('.select-loan').show();

                    $('#selectLoanBtn').click(function () {
                        $('.loader').show();
                        var loan = $('#txtSearchLoan').val();
                        $('#ContentPlaceHolder1_LoanFullID').val(loan);

                        obj.bindGrid();
                    });
                },
                getTableInfo: function () {
                    var loan_full_id = $('#ContentPlaceHolder1_LoanFullID').val();
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "LoanCropPrice.aspx/GetTableInfo",
                        data: JSON.stringify({ Loan_Full_ID: loan_full_id }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            var data = JSON.parse(res.d);

                            var table_info = data.Table_Info[0];
                            $('#txtTableName').html(table_info.Table_Name);
                            $('#txtLastUpdated').html(table_info.Last_Updated);
                            $('#txtRecordsCount').html(table_info.Records_Count);

                            if (data.CanEdit) {
                                $('#editControl').removeClass('disabled');
                            } else {
                                $('#editControl').addClass('disabled');
                            }

                            $('#enableControl').addClass('disabled');
                            
                        }
                    });
                },
                bindGrid: function () {
                    var loan_full_id = $('#ContentPlaceHolder1_LoanFullID').val();

                    $.ajax({
                        type: "POST",
                        url: "LoanCropPrice.aspx/LoadTable",
                        data: JSON.stringify({ Loan_Full_ID: loan_full_id }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var resData = JSON.parse(data.d);
                            $('#lblcount').html('Count of records: ' + resData.length);
                            $('.loader').show();
                            for (var i = 0; i < resData.length; i++) {
                                resData[i].rowid = i + 1;
                            }
                            obj.loadgrid(resData);
                            obj.getTableInfo();
                            $('.loader').hide();
                        }
                    });
                },
                loadgrid: function (data) {
                    $.jgrid.gridUnload("#jqGrid");
                    $("#jqGrid").jqGrid({
                        datatype: "local",
                        styleUI: 'Bootstrap',
                        data: data,
                        colModel: [
                            { label: 'rowid', name: 'rowid', width: 75, editable: false, key: true, hidden: true, hidedlg: true },
                            { label: 'Loan Crop Price ID', name: 'Loan_Crop_ID', width: 120, align: 'center', editable: false, hidden: false },
                            { label: 'Loan Full ID', name: 'Loan_Full_ID', width: 120, align: 'center', editable: false, hidden: false },
                            {
                                label: 'Crop Code',
                                name: 'Crop_Code',
                                width: 90,
                                align: "center",
                                editable: false
                            },
                            {
                                label: 'Crop Type',
                                name: 'Crop_Type_Code',
                                width: 120,
                                align: "center",
                                editable: false
                            },
                            {
                                label: 'Crop Price',
                                name: 'Crop_Price',
                                width: 120,
                                align: "right",
                                classes: 'right',
                                editable: false,
                                formatter: 'priceFormatter'
                            },
                            {
                                label: 'Basic Adj',
                                name: 'Basic_Adj',
                                width: 120,
                                align: "right",
                                classes: 'right',
                                editable: false,
                                hidden: false,
                                formatter: 'priceFormatter'
                            },
                            {
                                label: 'Mkt Adj',
                                name: 'Marketing_Adj',
                                width: 120,
                                align: "right",
                                classes: 'right',
                                editable: false,
                                hidden: false,
                                formatter: 'priceFormatter'
                            },
                            {
                                label: 'Rebate Adj',
                                name: 'Rebate_Adj',
                                width: 120,
                                align: "right",
                                classes: 'right',
                                editable: false,
                                hidden: false,
                                formatter: 'priceFormatter'
                            },
                            {
                                label: 'Adj Crop Price',
                                align: "right",
                                classes: 'right',
                                name: 'Adj_Price',
                                width: 120,
                                editable: false,
                                hidden: false,
                                formatter: 'priceFormatter'
                            },
                            {
                                label: 'Contract Qty',
                                align: "right",
                                classes: 'right',
                                name: 'Contract_Qty',
                                width: 120,
                                editable: false,
                                hidden: false,
                                formatter: 'numberFormatter'
                            },
                            {
                                label: 'Contract Price',
                                align: "right",
                                classes: 'right',
                                name: 'Contract_Price',
                                width: 120,
                                editable: false,
                                hidden: false,
                                formatter: 'priceFormatter'
                            },
                            {
                                label: 'Contract Pct',
                                align: "right",
                                classes: 'right',
                                name: 'Percent_booked',
                                width: 120,
                                editable: false,
                                hidden: false,
                                formatter: 'percentFormatter'
                            },
                            {
                                label: 'Acres',
                                align: "right",
                                classes: 'right',
                                name: 'Acres',
                                width: 120,
                                editable: false,
                                hidden: false,
                                formatter: 'numberFormatter'
                            },
                            {
                                label: 'Revenue',
                                align: "right",
                                classes: 'right',
                                name: 'Revenue',
                                width: 120,
                                editable: false,
                                hidden: false,
                                formatter: 'amountFormatter'
                            },
                            {
                                label: 'Crop Yield',
                                align: "right",
                                classes: 'right',
                                name: 'W_Crop_Yield',
                                width: 120,
                                editable: false,
                                hidden: false,
                                formatter: 'numberFormatter'
                            },
                            {
                                label: 'Prod Share',
                                align: "right",
                                classes: 'right',
                                name: 'LC_Share',
                                width: 120,
                                editable: false,
                                hidden: false,
                                formatter: 'percentFormatter'
                            },
                            {
                                label: 'Status',
                                name: 'Status',
                                align: "center",
                                classes: "center",
                                width: 90,
                                editable: false
                            }
                        ],
                        viewrecords: true,
                        loadonce: true,
                        restoreAfterSelect: false,
                        saveAfterSelect: true,
                        sortable: true,
                        onPaging: obj.changePage,
                        height: 'auto',
                        rowNum: 100,
                        pager: "#jqGridPager"
                    });
                },                
                search: function (self) {
                    if (timer) { clearTimeout(timer); }
                    timer = setTimeout(function () {
                        obj.changePage();
                        $("#jqGrid").jqGrid('filterInput', self.value);
                        var length = jQuery("#jqGrid").jqGrid('getGridParam', 'records');
                        $('#txtRecordsCount').html('Records Count: ' + length);
                    }, 0);
                },
                changePage: function () {
                    lastSelection = "";
                    var grid = $("#jqGrid");
                    var id = $('.inline-edit-cell').parent().parent().prop('id');
                    if (id != undefined) {
                        grid.jqGrid('saveRow', id);
                        grid.jqGrid('restoreRow', id);
                        var row = grid.jqGrid('getRowData', id);
                        if (!jQuery.isEmptyObject(row)) {
                            var dataobj = jQuery("#jqGrid").jqGrid('getGridParam', 'data');
                            index = dataobj.findIndex(x => x.rowid == row.rowid);
                            if (row.rowid > 0 && row.Actionstatus != 1) {
                                row.Actionstatus = 2;
                                grid.jqGrid('getGridParam', 'data')[index] = row;
                            }
                        }
                    }
                }
            }
            obj.Init();
        });
    </script>
</asp:Content>

