﻿<%@ Page Title="" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="LoanCrossCollateral.aspx.cs" Inherits="admin_LoanCrossCollateral" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <div class="loader"></div>
    <div class="col-md-12">
        <div class="form-inline">
            <div class="row">
                <div class="col-md-12" style="margin-top: 20px;">
                    <div class="form-group" style="margin-right: 22px; margin-left: 37px;">
                        <label for="txtSearch">Add ID :</label>
                        <input type="text" id="txtaddid" class="form-control" name="UserName" style="width: 300px" />
                    </div>
                    <div class="form-group">
                        <label for="txtSearch">Master Loan ID :</label>
                        <input type="text" id="txtccid" class="form-control" name="UserName" style="width: 300px; margin-right: 12px;" />
                    </div>
                    <div class="form-group" style="margin-right: 22px;">
                        <label for="txtSearch">Delete Ids :</label>
                        <input type="text" id="txtdeleteids" class="form-control" name="UserName" style="width: 300px" />
                        <div class="btn btn-primary" id="btngoproc">Go</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12" style="padding-top: 50px;">
        <div class="form-inline">
            <div class="form-group">
                <label for="txtSearch">CC Group ID :</label>
                <input type="text" id="txtgetid" class="form-control" name="UserName" style="width: 300px; margin-right: 12px;" />
                <div class="btn btn-primary" id="btngetdata">Get</div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-11" style="padding-left: 51px; padding-top: 8px;">
            <asp:Label ID="lbl_table" runat="server"></asp:Label>
        </div>
    </div>
    <script>
        $(function () {
            var obj = {
                Init: function () {
                    $('.loader').hide();
                    $("#btngetdata").on("click", function () {
                        obj.loadtable();
                    })
                    $("#btngoproc").on("click", function () {
                        obj.sqlproc();
                    })
                },
                loadtable: function () {
                    $('#ContentPlaceHolder1_lbl_table').text('');
                    var loanmasterid = $('#txtgetid').val();
                    var deleteid = $('#txtdeleteids').val();
                    $.ajax({
                        type: "POST",
                        url: "LoanCrossCollateral.aspx/LoadTable",
                        data: JSON.stringify({ CrossInd: loanmasterid, Deletedid: deleteid }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            $('#ContentPlaceHolder1_lbl_table').html(res.d);
                            $('.loader').hide();
                        }
                    });
                },
                sqlproc: function () {
                    $('#ContentPlaceHolder1_lbl_table').text('');
                    var addid = $('#txtaddid').val();
                    var deleteid = $('#txtdeleteids').val();
                    var LoanId = $('#txtccid').val();
                    if (addid == '' && deleteid == '' && LoanId == '') {
                        return false;
                    }
                    $.ajax({
                        type: "POST",
                        url: "LoanCrossCollateral.aspx/Sqlproc",
                        data: JSON.stringify({ Addid: addid, Deleteid: deleteid, LoanID: LoanId }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            $('#txtgetid').val(res.d);
                            obj.loadtable();
                        }
                    });
                }
            }
            obj.Init();
        });

    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" runat="Server">
</asp:Content>

