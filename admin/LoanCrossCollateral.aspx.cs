﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_LoanCrossCollateral : System.Web.UI.Page
{
    static string dbKey = "gp_conn";

    protected void Page_Load(object sender, EventArgs e)
    {
    }
    [WebMethod]
    public static string LoadTable(string CrossInd, string Deletedid)
    {
        string str_sql = "";
        if (!string.IsNullOrEmpty(CrossInd))
        {
            str_sql = "select Cross_Collateral_ID, Loan_ID,Cross_Collateral_Group_ID,Status from Loan_Cross_Collateral where Cross_Collateral_Group_ID='" + CrossInd + "'";
        }
        else if (!string.IsNullOrEmpty(Deletedid))
        {
            str_sql = "select Cross_Collateral_ID, Loan_ID,Cross_Collateral_Group_ID,Status from Loan_Cross_Collateral where Loan_ID='" + Deletedid + "'";
        }
        else
        {
            str_sql = "select Cross_Collateral_ID, Loan_ID,Cross_Collateral_Group_ID,Status from Loan_Cross_Collateral";
        }
        DataTable dt = gen_db_utils.gp_sql_get_datatable(str_sql, dbKey);
        return GeneteateGrid(dt, Deletedid);
    }
    [WebMethod]
    public static string GetLoanMasterID(string LoanID)
    {
        string str_sql = "";
        str_sql = "select Loan_CC_Group_ID from Loan_Cross_Collateral where Loan_ID='" + LoanID + "'";
        DataTable dt1 = gen_db_utils.gp_sql_get_datatable(str_sql, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt1);
    }
    [WebMethod]
    public static string Sqlproc(string Addid, string Deleteid, string LoanID)
    {
        string strQry = "";
        SqlCommand com;
        DataSet dataset = new DataSet();
        string strConnString = ConfigurationManager.ConnectionStrings["gp_conn"].ConnectionString;
        SqlConnection con = new SqlConnection(strConnString);
        con.Open();
        com = new SqlCommand("spLoanCollateral", con);
        com.CommandType = CommandType.StoredProcedure;
        com.Parameters.AddWithValue("@AddLoanID", Addid);
        com.Parameters.AddWithValue("@MasterloanID", LoanID);
        com.Parameters.AddWithValue("@DeleteLoanID", Deleteid);
        com.ExecuteNonQuery();
        //com.ExecuteScalar();
        con.Close();
        if (string.IsNullOrEmpty(LoanID) && !string.IsNullOrEmpty(Deleteid))
        {
            LoanID = Deleteid;
        }
        strQry = "select Cross_Collateral_Group_ID from Loan_Cross_Collateral where Loan_ID='" + LoanID + "'";
        return gen_db_utils.gp_sql_scalar(strQry, dbKey);
    }
    [WebMethod]
    public static string GeneteateGrid(DataTable dt1, string Deletedid)
    {
        string str_return = "";
        if (dt1 != null && dt1.Rows.Count > 0)
        {
            str_return += "<div> <table id='tblActiveUsers' class='table table-striped table-bordered' >  "
                                 + "<thead> "
                                 + "     <tr> "
                                      + "       <th >Cross_Collateral_ID</th> "
                                      + "       <th >Loan_ID</th> "
                                      + "       <th >Cross_Collateral_Group_ID</th> "
                                      + "       <th >Status</th> "

                                 + "    </tr> "
                                 + "      </thead> "
                                 + "        <tbody> ";

            foreach (DataRow dtr1 in dt1.Rows)
            {
                string value = dtr1["Cross_Collateral_Group_ID"].ToString();
                string statusval = dtr1["Loan_ID"].ToString();
                string trval;
                if (Convert.ToString(dtr1["Loan_ID"]) == Deletedid && Convert.ToString(dtr1["Status"]) == "1")
                {
                    trval = "<tr style='color:red;'>";
                }
                else
                {
                    trval = "<tr>";
                }
                str_return += trval
                             + "<td>" + dtr1["Cross_Collateral_ID"].ToString() + "</td>"
                             + "<td>" + dtr1["Loan_ID"].ToString() + "</td>"
                             + "<td>" + dtr1["Cross_Collateral_Group_ID"].ToString() + "</td>"
                             + "<td>" + dtr1["Status"].ToString() + "</td>"
                               + "</tr>";
            }
            str_return += " </tbody> "
                                    + " </table> "
                                      + "</div></div></div >";
        }
        else
        {
            str_return = "    No data available...";
        }
        return str_return;
    }
}