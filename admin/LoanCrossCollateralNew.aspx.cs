﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_LoanCrossCollateralNew : BasePage
{
    public admin_LoanCrossCollateralNew()
    {
        Table_Name = "Loan_Cross_Collateral";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        base.Page_Load(sender, e);
        LoanFullID.Value = Loan_Full_ID;
    }

    [WebMethod]
    public static string GetTableInfo(string Loan_Full_ID)
    {
        Count_Query = "Declare @count int; Select @Count = Count(*) From Loan_Cross_Collateral";

        if (!string.IsNullOrEmpty(Loan_Full_ID))
        {
            Count_Query += " Where Loan_ID like '%" + Loan_Full_ID + "%'";
        }

        return Get_Table_Info();
    }

    [WebMethod]
    public static string LoadTable(string Loan_Full_ID)
    {
        HttpContext.Current.Session["Loan_Full_ID"] = Loan_Full_ID;

        string sql_qry = @"select * from Loan_Cross_Collateral";

        if (!string.IsNullOrEmpty(Loan_Full_ID))
        {
            sql_qry += " Where Loan_ID like '%" + Loan_Full_ID + "%'";
        }

        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return JsonConvert.SerializeObject(dt);
    }
}