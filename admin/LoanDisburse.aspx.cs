﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_LoanDisburse : BasePage
{
    public admin_LoanDisburse()
    {
        Table_Name = "Loan_Disburse";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        base.Page_Load(sender, e);
        LoanFullID.Value = Loan_Full_ID;
    }

    [WebMethod]
    public static string GetTableInfo(string Loan_Full_ID)
    {
        Count_Query = "Declare @count int; Select @Count = Count(*) From Loan_Disburse";

        if (!string.IsNullOrEmpty(Loan_Full_ID))
        {
            Count_Query += " Where Loan_ID like '%" + Loan_Full_ID + "%'";
        }

        return Get_Table_Info();
    }

    [WebMethod]
    public static string LoadTable(string Loan_Full_ID)
    {
        HttpContext.Current.Session["Loan_Full_ID"] = Loan_Full_ID;

        string sql_qry = @"SELECT [Loan_Disburse_ID]
                              ,[Loan_ID]
                              ,[Total_Disburse_Amount]
                              ,[Request_User_ID]
                              ,[Request_Status_Ind]
                              ,[Request_Date_Time]
                              ,[Processor_User_ID]
                              ,[Processor_Status_Ind]
                              ,[Processor_Date_time]
                              ,CAST([Special_ACH_Ind] as INT) as [Special_ACH_Ind]
                              ,CAST([Special_CRP_Ind] as INT) as [Special_CRP_Ind]
                              ,[Special_Harvest_Comment]
                              ,[Special_Comment]
                              ,CAST ([Special_COC_Ind] as INT) as [Special_COC_Ind]
                              ,[Status]
                          FROM [dbo].[Loan_Disburse]";

        if(!string.IsNullOrEmpty(Loan_Full_ID))
        {
            sql_qry += " where loan_id like '%" + Loan_Full_ID + "%'";
        }

        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return JsonConvert.SerializeObject(dt);
    }
}