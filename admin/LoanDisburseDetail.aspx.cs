﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_LoanDisburseDetail : BasePage
{
    public admin_LoanDisburseDetail()
    {
        Table_Name = "Loan_Disburse_Detail";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        base.Page_Load(sender, e);
        LoanFullID.Value = Loan_Full_ID;
    }

    [WebMethod]
    public static string GetTableInfo(string Loan_Full_ID)
    {
        Count_Query = "Declare @count int; Select @Count = Count(*) From Loan_Disburse_Detail";

        if (!string.IsNullOrEmpty(Loan_Full_ID))
        {
            Count_Query += " Where Loan_ID like '%" + Loan_Full_ID + "%'";
        }

        return Get_Table_Info();
    }

    [WebMethod]
    public static string LoadTable(string Loan_Full_ID)
    {
        HttpContext.Current.Session["Loan_Full_ID"] = Loan_Full_ID;

        string sql_qry = @"SELECT [Loan_Disburse_Detail_ID]
                          ,[Loan_ID]
                          ,[Loan_Disburse_ID]
                          ,[Budget_Expense_ID]
                          ,[Disburse_Detail_Commit_Amount]
                          ,[Disburse_Detail_Used_Amount]
                          ,[Disburse_Detail_Requested_Amount]
                          ,LD.[Status]
	                      , RB.[Budget_Expense_Name]
                      FROM [dbo].[Loan_Disburse_Detail] LD
                      JOIN Ref_Budget_Expense_Type RB on LD.[Budget_Expense_ID] = RB.Budget_Expense_Type_ID";


        if (!string.IsNullOrEmpty(Loan_Full_ID))
        {
            sql_qry += " Where LD.Loan_ID like '%" + Loan_Full_ID + "%'";
        }

        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return JsonConvert.SerializeObject(dt);
    }
}