﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_LoanDisburseRentDetail : BasePage
{
    public admin_LoanDisburseRentDetail()
    {
        Table_Name = "Loan_Disburse_Rent_Detail";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        base.Page_Load(sender, e);
        LoanFullID.Value = Loan_Full_ID;
    }

    [WebMethod]
    public static string GetTableInfo(string Loan_Full_ID)
    {
        Count_Query = "Declare @count int; Select @Count = Count(*) From Loan_Disburse_Rent_Detail";

        if (!string.IsNullOrEmpty(Loan_Full_ID))
        {
            Count_Query += " Where Loan_ID like '%" + Loan_Full_ID + "%'";
        }

        return Get_Table_Info();
    }

    [WebMethod]
    public static string LoadTable(string Loan_Full_ID)
    {
        HttpContext.Current.Session["Loan_Full_ID"] = Loan_Full_ID;

        string sql_qry = @"SELECT [Loan_Disburse_Rent_Detail_ID]
                                  ,[Loan_ID]
                                  ,[Loan_Disburse_ID]
                                  ,[Landowner_ID]
                                  ,LD.[Farm_ID]
                                  ,[Cash_Rent_Amount]
                                  ,LD.[Status]
                                  ,[Reimbusement]
	                              ,LF.Landowner
	                              ,LF.Section
	                              ,LF.FSN
	                              ,LF.Rated
	                              ,LF.Farm_State_ID
	                              ,LF.Farm_County_ID
                              FROM [dbo].[Loan_Disburse_Rent_Detail] LD
                              JOIN Loan_Farm LF ON LD.Farm_ID = LF.Farm_ID";

        if (!string.IsNullOrEmpty(Loan_Full_ID))
        {
            sql_qry += " Where LD.Loan_ID like '%" + Loan_Full_ID + "%'";
        }

        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return JsonConvert.SerializeObject(dt);
    }
}