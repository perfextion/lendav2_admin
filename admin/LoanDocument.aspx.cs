﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
public partial class admin_LoanDocument : BasePage
{
    public admin_LoanDocument()
    {
        Table_Name = "Loan_Document";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        base.Page_Load(sender, e);
        LoanFullID.Value = Loan_Full_ID;
    }

    [WebMethod]
    public static string GetTableInfo(string Loan_Full_ID)
    {
        Count_Query = "Declare @count int; Select @Count = Count(*) From Loan_Document where [status] = 1";

        if (!string.IsNullOrEmpty(Loan_Full_ID))
        {
            Count_Query += " And Loan_ID like '%" + Loan_Full_ID + "%'";
        }

        return Get_Table_Info();
    }

    [WebMethod]
    public static string LoanDocument(string Loan_Full_ID)
    {
        HttpContext.Current.Session["Loan_Full_ID"] = Loan_Full_ID;

        string sql_qry = "SELECT 0 as Actionstatus, *, " +
            "convert(varchar, Upload_Date_Time, 101) as Upload_Date_Time_Formatted, " +
            "convert(varchar, Request_Date_Time, 101) as Request_Date_Time1 " +
            "FROM Loan_Document where [status] = 1";

        if (!string.IsNullOrEmpty(Loan_Full_ID))
        {
            sql_qry += " And Loan_ID like '%" + Loan_Full_ID + "%'";
        }

        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
    }
    [WebMethod]
    public static void SaveLoanDocument(dynamic lst)
    {
        foreach (var item in lst)
        {
            string qry = "";
            int val = Convert.ToInt32(item["Actionstatus"]);
            if (val == 1)
            {
                qry = "INSERT INTO Loan_Document(Loan_Full_ID, Document_Type_ID, Document_Template_ID," +
                    " Document_State_Code, Replication_Association_Type, Replication_Association_ID, " +
                    "Critical_Change_Ind, Resquest_User_ID, Request_Date_Time, KL_Document_ID, " +
                    "Upload_User_ID, Upload_Date_Time, Document_URL, Status)" +
                    " VALUES('" + item["Loan_Full_ID"].Replace("'", "''") + "','" + item["Document_Type_ID"].Replace("'", "''") + "'," +
                    "'" + item["Document_Template_ID"].Replace("'", "''") + "','" + item["Document_State_Code"].Replace("'", "''") + "'," +
                    "'" + item["Replication_Association_Type"].Replace("'", "''") + "','" + item["Replication_Association_ID"].Replace("'", "''") + "','" + item["Critical_Change_Ind"].Replace("'", "''") + "'," +
                    "'" + item["Resquest_User_ID"].Replace("'", "''") + "','" + item["Request_Date_Time"].Replace("'", "''") + "'," +
                    "'" + item["KL_Document_ID"].Replace("'", "''") + "','" + item["Upload_User_ID"].Replace("'", "''") + "'," +
                    "'" + item["Upload_Date_Time"].Replace("'", "''") + "','" + item["Document_URL"].Replace("'", "''") + "','" + item["Status"].Replace("'", "''") + "')";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 2)
            {
                qry = "UPDATE Loan_Document SET Loan_Full_ID = '" + item["Loan_Full_ID"].Replace("'", "''") + "' ," +
                    "Document_Type_ID = '" + item["Document_Type_ID"].Replace("'", "''") + "',Document_Template_ID = '" + item["Document_Template_ID"].Replace("'", "''") + "'," +
                    "Document_State_Code = '" + item["Document_State_Code"].Replace("'", "''") + "',Replication_Association_Type = '" + item["Replication_Association_Type"].Replace("'", "''") + "'," +
                    "Replication_Association_ID = '" + item["Replication_Association_ID"].Replace("'", "''") + "',Critical_Change_Ind = '" + item["Critical_Change_Ind"].Replace("'", "''") + "'," +
                    "Resquest_User_ID = '" + item["Resquest_User_ID"].Replace("'", "''") + "',Request_Date_Time = '" + item["Request_Date_Time"].Replace("'", "''") + "'," +
                    "KL_Document_ID = '" + item["KL_Document_ID"].Replace("'", "''") + "',Upload_User_ID = '" + item["Upload_User_ID"].Replace("'", "''") + "'," +
                    "Upload_Date_Time = '" + item["Upload_Date_Time"].Replace("'", "''") + "',Document_URL = '" + item["Document_URL"].Replace("'", "''") + "'," +
                    "Status = '" + item["Status"].Replace("'", "''") + "' where Loan_Document_ID=" + item["Loan_Document_ID"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 3)
            {
                qry = "DELETE FROM Loan_Document  where Loan_Document_ID=" + item["Loan_Document_ID"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
        }
    }


}