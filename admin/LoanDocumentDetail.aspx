﻿<%@ Page Title="Loan Document Detail" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="LoanDocumentDetail.aspx.cs" Inherits="admin_LoanDocumentDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .ui-jqgrid .ui-jqgrid-htable th div {
            height: auto;
            overflow: hidden;
            padding-right: 4px;
            padding-top: 2px;
            position: relative;
            vertical-align: text-top;
            white-space: normal !important;
        }

        #content {
            padding: 14px !important;
        }
    </style>
    <div class="loan-transaction-table">
        <div class="row switch-controls">
            <div class="switch-control disabled" id="editControl">
                <label for="editBtn">Edit</label>
                <div class="toggle-btn small">            
                    <input type="checkbox" id="editBtn" class="cb-value" />
                    <span class="round-btn"></span>
                </div>
            </div>
            <div class="switch-control disabled" id="enableControl">
                <label for="enableBtn">Enable</label>
                <div class="toggle-btn small">            
                    <input type="checkbox" id="enableBtn" class="cb-value" />
                    <span class="round-btn"></span>
                </div>
            </div>
        </div>
        <div class="loader"></div>
        <div class="row main-content">
            <div>
                <table id="jqGrid"></table>
                <div id="jqGridPager"></div>
            </div>
        </div>
        <asp:HiddenField ID="LoanFullID" runat="server" />
    </div>
    <br />
    <br />
    <br />
    <script type="text/javascript">  
        var DeleteRows = [];
        var timer;
        var lastSelection;

        $(function () {
            var obj = {
                Init: function () {
                    $('.loader').show();
                    obj.bindGrid();

                    $("#navbtnadd").addClass('disabled');

                    $("#navbtnsave").addClass('disabled');

                    $("#navbtnrefresh").click(function () {
                        $('.loader').show();
                        obj.bindGrid();
                    });

                    $("#txtSearchBar").on("keyup", function () {
                        var self = this;
                        obj.search(self);
                    });

                    obj.initLoanSelect();
                },
                initLoanSelect: function () {
                    var hiddenVal = $('#ContentPlaceHolder1_LoanFullID').val();

                    if (hiddenVal) {
                        $('#txtSearchLoan').val(hiddenVal);
                    }

                    $('.select-loan').show();

                    $('#selectLoanBtn').click(function () {
                        $('.loader').show();
                        var loan = $('#txtSearchLoan').val();
                        $('#ContentPlaceHolder1_LoanFullID').val(loan);

                        obj.bindGrid();
                    });
                },
                getTableInfo: function () {
                    var loan_full_id = $('#ContentPlaceHolder1_LoanFullID').val();
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "LoanDocumentDetail.aspx/GetTableInfo",
                        data: JSON.stringify({ Loan_Full_ID: loan_full_id }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            var data = JSON.parse(res.d);

                            var table_info = data.Table_Info[0];
                            $('#txtTableName').html(table_info.Table_Name);
                            $('#txtLastUpdated').html(table_info.Last_Updated);
                            $('#txtRecordsCount').html(table_info.Records_Count);

                            if (data.CanEdit) {
                                $('#editControl').removeClass('disabled');
                            } else {
                                $('#editControl').addClass('disabled');
                            }

                            $('#enableControl').addClass('disabled');
                            
                        }
                    });
                },
                bindGrid: function () {
                     var loan_full_id = $('#ContentPlaceHolder1_LoanFullID').val();
                    $.ajax({
                        type: "POST",
                        url: "LoanDocumentDetail.aspx/GetLoanDocumentDetail",
                        data: JSON.stringify({ Loan_Full_ID: loan_full_id }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {

                            var resData = JSON.parse(data.d);
                            $('.loader').show();
                            for (var i = 0; i < resData.length; i++) {
                                resData[i].rowid = i + 1;
                            }
                            obj.loadgrid(resData);
                            obj.getTableInfo();
                            $('.loader').hide();
                        }
                    });
                },
                loadgrid: function (data) {
                    $("#jqGrid").jqGrid({
                        datatype: "local",
                        styleUI: 'Bootstrap',
                        data: data,
                        colModel: [
                            { label: 'rowid', name: 'rowid', width: 200, editable: false, key: true, hidden: true },
                            { label: 'Loan Doc Detail ID', name: 'Loan_Document_Detail_ID', align: 'center', width: 150, editable: false },
                            { label: 'Loan Full ID', name: 'Loan_Full_ID', width: 120, align: 'center', editable: false },
                            { label: 'Loan ID', name: 'Loan_ID', width: 120, align: 'center', editable: false },
                            {
                                label: 'Loan Doc ID', name: 'Loan_Document_ID', width: 150, align: 'center', editable: true, editoptions: {
                                    maxlength: 20, dataEvents: [
                                        {
                                            type: 'keypress', fn: function (e) {
                                                var charCode = (e.which) ? e.which : e.keyCode;
                                                if (charCode == 13 || (charCode > 31 && (charCode < 48 || charCode > 57))) {
                                                    return false;
                                                }
                                            }
                                        }
                                    ]
                                },
                            },
                            {
                                label: 'Field ID', name: 'Field_ID', align: 'center', width: 150, editable: true, editoptions: {
                                    maxlength: 20, dataEvents: [
                                        {
                                            type: 'keypress', fn: function (e) {
                                                var charCode = (e.which) ? e.which : e.keyCode;
                                                if (charCode == 13 || (charCode > 31 && (charCode < 48 || charCode > 57))) {
                                                    return false;
                                                }
                                            }
                                        }
                                    ]
                                },
                            },
                            {
                                label: 'Critical Change Ind', name: 'Critical_Change_Detail_Ind', align: 'center', width: 150, editable: true, editoptions: {
                                    maxlength: 20, dataEvents: [
                                        {
                                            type: 'keypress', fn: function (e) {
                                                var charCode = (e.which) ? e.which : e.keyCode;
                                                if (charCode == 13 || (charCode > 31 && (charCode < 48 || charCode > 57))) {
                                                    return false;
                                                }
                                            }
                                        }
                                    ]
                                },
                            },
                            { label: 'Field ID Value', name: 'Field_ID_Value', align: 'center', width: 150, editable: true },
                            {
                                label: 'Status', name: 'Status', width: 120, align: 'center', editable: true, editoptions: {
                                    maxlength: 20, dataEvents: [
                                        {
                                            type: 'keypress', fn: function (e) {
                                                var charCode = (e.which) ? e.which : e.keyCode;
                                                if (charCode == 13 || (charCode > 31 && (charCode < 48 || charCode > 57))) {
                                                    return false;
                                                }
                                            }
                                        }
                                    ]
                                },
                            }
                        ],
                        viewrecords: true,
                        loadonce: true,
                        restoreAfterSelect: false,
                        saveAfterSelect: true,
                        sortable: true,
                        onPaging: obj.changePage,
                        height: 'auto',
                        rowNum: 100,
                        pager: "#jqGridPager"
                    });
                },
                search: function (self) {
                    if (timer) { clearTimeout(timer); }
                    timer = setTimeout(function () {
                        obj.changePage();
                        $("#jqGrid").jqGrid('filterInput', self.value);
                        var length = jQuery("#jqGrid").jqGrid('getGridParam', 'records');
                        $('#txtRecordsCount').html('Records Count: ' + length);
                    }, 0);
                },
                changePage: function () {

                    lastSelection = "";
                    var grid = $("#jqGrid");
                    var id = $('.inline-edit-cell').parent().parent().prop('id');
                    if (id != undefined) {
                        grid.jqGrid('saveRow', id);
                        grid.jqGrid('restoreRow', id);
                        var row = grid.jqGrid('getRowData', id);
                        if (!jQuery.isEmptyObject(row)) {
                            var dataobj = jQuery("#jqGrid").jqGrid('getGridParam', 'data');
                            index = dataobj.findIndex(x => x.rowid == row.rowid);
                            if (row.rowid > 0 && row.Actionstatus != 1) {
                                row.Actionstatus = 2;
                                grid.jqGrid('getGridParam', 'data')[index] = row;
                            }
                        }
                    }
                }
            }
            obj.Init();

        });
    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" runat="Server">
</asp:Content>

