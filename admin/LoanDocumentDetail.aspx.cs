﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_LoanDocumentDetail : BasePage

{
    public admin_LoanDocumentDetail()
    {
        Table_Name = "Loan_Document_Detail";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        base.Page_Load(sender, e);
        LoanFullID.Value = Loan_Full_ID;
    }

    [WebMethod]
    public static string GetTableInfo(string Loan_Full_ID)
    {
        Count_Query = "Declare @count int; Select @Count = Count(*) From Loan_Document_Detail";

        if (!string.IsNullOrEmpty(Loan_Full_ID))
        {
            Count_Query += " Where Loan_Full_ID like '%" + Loan_Full_ID + "%'";
        }

        return Get_Table_Info();
    }

    [WebMethod]
    public static string GetLoanDocumentDetail(string Loan_Full_ID)
    {
        HttpContext.Current.Session["Loan_Full_ID"] = Loan_Full_ID;

        string sql_qry = "SELECT 0 as Actionstatus,Loan_Document_Detail_ID,Loan_Document_ID,Loan_Full_ID," +
            "Field_ID,Field_ID_Value,Critical_Change_Detail_Ind,Status FROM Loan_Document_Detail";

        if (!string.IsNullOrEmpty(Loan_Full_ID))
        {
            sql_qry += " Where Loan_Full_ID like '%" + Loan_Full_ID + "%'";
        }

        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
    }


    [WebMethod]
    public static void SaveLoanDocumentDetail(dynamic lst)
    {
        foreach (var item in lst)
        {
            string qry = "";
            int val = Convert.ToInt32(item["Actionstatus"]);
            if (val == 1)
            {
                qry = "INSERT INTO Loan_Document_Detail(Loan_Document_ID,Loan_Full_ID,Field_ID,Field_ID_Value,Critical_Change_Detail_Ind,Status)" +
                    " VALUES('" + item["Loan_Document_ID"].Replace("'", "''") + "','" + item["Loan_Full_ID"].Replace("'", "''") + "'," +
                    "'" + item["Field_ID"].Replace("'", "''") + "','" + item["Field_ID_Value"].Replace("'", "''") + "'," +
                    "'" + item["Critical_Change_Detail_Ind"].Replace("'", "''") + "','" + item["Status"].Replace("'", "''") + "')";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 2)
            {//,,,,,,
                qry = "UPDATE Loan_Document_Detail SET Loan_Document_ID = '" + item["Loan_Document_ID"].Replace("'", "''") + "' ," +
                    "Loan_Full_ID = '" + item["Loan_Full_ID"].Replace("'", "''") + "',Field_ID = '" + item["Field_ID"].Replace("'", "''") + "'," +
                    "Field_ID_Value = '" + item["Field_ID_Value"].Replace("'", "''") + "',Critical_Change_Detail_Ind = '" + item["Critical_Change_Detail_Ind"].Replace("'", "''") + "'," +
                    "Status = '" + item["Status"].Replace("'", "''") + "' where Loan_Document_Detail_ID=" + item["Loan_Document_Detail_ID"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 3)
            {
                qry = "DELETE FROM Loan_Document_Detail  where Loan_Document_Detail_ID=" + item["Loan_Document_Detail_ID"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }



        }
    }


}