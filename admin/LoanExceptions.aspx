﻿<%@ Page Title="Loan Exceptions" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="LoanExceptions.aspx.cs" Inherits="admin_LoanExceptions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .ui-jqgrid .ui-jqgrid-htable th div {
            height: auto;
            overflow: hidden;
            padding-right: 4px;
            padding-top: 2px;
            position: relative;
            vertical-align: text-top;
            word-wrap: break-word; /* IE 5.5+ and CSS3 */
            white-space: pre-wrap; /* CSS3 */
            white-space: normal !important;
        }

        .ui-jqgrid tr.jqgrow td {
            word-wrap: break-word; /* IE 5.5+ and CSS3 */
            white-space: pre-wrap; /* CSS3 */
            white-space: normal !important;
            height: auto;
            max-height: 100px;
            vertical-align: text-top;
            padding-top: 2px;
            padding-bottom: 3px;
        }

        .cvteste {
            padding-right: 14px !important;
        }

        #content {
            padding: 14px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="loan-exceptions-content-section loan-transaction-table">
        <div class="row switch-controls">
            <div class="switch-control disabled" id="editControl">
                <label for="editBtn">Edit</label>
                <div class="toggle-btn small">            
                    <input type="checkbox" id="editBtn" class="cb-value" />
                    <span class="round-btn"></span>
                </div>
            </div>
            <div class="switch-control disabled" id="enableControl">
                <label for="enableBtn">Enable</label>
                <div class="toggle-btn small">            
                    <input type="checkbox" id="enableBtn" class="cb-value" />
                    <span class="round-btn"></span>
                </div>
            </div>
        </div>
        <div class="loader"></div>
        <div class="row main-content">
            <div>
                <table id="jqGrid"></table>
                <div id="jqGridPager"></div>
            </div>
        </div>
        <asp:HiddenField ID="LoanFullID" runat="server" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" runat="Server">
    <script type="text/javascript"> 
        var DeleteRows = [];
        var timer;
        var lastSelection;
        $(function () {
            var obj = {
                Init: function () {
                    $('.loader').show();

                    obj.bindGrid();

                    $('#navbtnsave').addClass('disabled');

                    $("#navbtnsave").addClass('disabled');

                    $("#navbtnrefresh").click(function () {
                        $('.loader').show();
                        lastSelection = null;
                        obj.bindGrid();
                    });

                    $("#txtSearchBar").on("keyup", function () {
                        var self = this;
                        obj.search(self);
                    });

                    $('#ddLoanList').change(function () {
                        $('.loader').show();
                        obj.bindGrid();
                    });

                     $("#navbtndownload").on("click", function () {
                        $("#jqGrid").jqGrid("exportToExcel", {
                            includeLabels: true,
                            includeGroupHeader: true,
                            includeFooter: true,
                            fileName: "Loan Exceptions.xlsx",
                            maxlength: 40
                        });
                     });

                     $('#navbtncolumns').click(function () {
                        $("#jqGrid").columnChooser({});
                     });

                    obj.initLoanSelect();
                },
                initLoanSelect: function () {
                    var hiddenVal = $('#ContentPlaceHolder1_LoanFullID').val();

                    if (hiddenVal) {
                        $('#txtSearchLoan').val(hiddenVal);
                    }

                    $('.select-loan').show();

                    $('#selectLoanBtn').click(function () {
                        $('.loader').show();
                        var loan = $('#txtSearchLoan').val();
                        $('#ContentPlaceHolder1_LoanFullID').val(loan);

                        obj.bindGrid();
                    });
                },
                getTableInfo: function () {
                    var loan_full_id = $('#ContentPlaceHolder1_LoanFullID').val();
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "LoanExceptions.aspx/GetTableInfo",
                        data: JSON.stringify({ Loan_Full_ID: loan_full_id }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            var data = JSON.parse(res.d);

                            var table_info = data.Table_Info[0];
                            $('#txtTableName').html(table_info.Table_Name);
                            $('#txtLastUpdated').html(table_info.Last_Updated);
                            $('#txtRecordsCount').html(table_info.Records_Count);

                            if (data.CanEdit) {
                                $('#editControl').removeClass('disabled');
                            } else {
                                $('#editControl').addClass('disabled');
                            }

                            $('#enableControl').addClass('disabled');
                            
                        }
                    });
                },
                bindGrid: function () {
                    var loan_full_id = $('#ContentPlaceHolder1_LoanFullID').val();
                    $.ajax({
                        type: "POST",
                        url: "LoanExceptions.aspx/GetLoanExceptions",
                        data: JSON.stringify({ Loan_Full_ID: loan_full_id }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var resData = JSON.parse(data.d);
                            $('.loader').show();
                            for (var i = 0; i < resData.length; i++) {
                                resData[i].rowid = i + 1;
                            }
                            obj.loadgrid(resData);
                            obj.getTableInfo();
                            $('.loader').hide();
                        }
                    });
                },
                loadgrid: function (data) {
                    $('.loader').show();
                    $.jgrid.gridUnload("#jqGrid");
                    $("#jqGrid").jqGrid({
                        datatype: "local",
                        styleUI: 'Bootstrap',
                        data: data,
                        emptyrecords: "No Exceptions to display",
                        colModel: [
                            { label: 'rowid', name: 'rowid', width: 75, editable: false, key: true, hidden: true },
                            { label: 'Loan Exception ID', name: 'Loan_Exception_ID', width: 120, align: 'center', editable: false },
                            { label: 'Loan Full ID', name: 'Loan_Full_ID', width: 120, align: 'center', editable: false },
                            {
                                label: 'Exception ID', name: 'Exception_ID', align: 'center', width: 120
                            },
                            { label: 'Exception Text', name: 'Exception_ID_Text', width: 240, editable: false },
                            {
                                label: 'Exception ID Level', name: 'Exception_ID_Level', align: 'center', width: 120
                            },
                            {
                                label: 'Tab ID', name: 'Tab_ID', width: 90, align: 'center'
                            },
                            {
                                label: 'Mitigation Ind', name: 'Mitigation_Ind', width: 90, align: 'center',
                            },
                            {
                                label: 'Mitigation Text', name: 'Mitigation_Text', width: 300
                            },
                            {
                                label: 'Support Ind', name: 'Support_Ind', width: 90, align: 'center'
                            },
                            { label: 'Support Role 1', name: 'Support_Role_Type_Code_1', width: 120, hidden: false },
                            {
                                label: 'Support User ID 1', name: 'Support_User_ID_1', width: 120, align: 'center'
                            },
                            {
                                label: 'Support Date Time 1',
                                name: 'Support_Date_Time_1',
                                width: 150
                            },
                            { label: 'Support Role 2', name: 'Support_Role_Type_Code_2', align: 'center', width: 120, hidden: false },
                            {
                                label: 'Support User ID 2', name: 'Support_User_ID_2', align: 'center', width: 120
                            },
                            {
                                label: 'Support Date Time 2',
                                name: 'Support_Date_Time_2',
                                width: 150
                            },
                            { label: 'Support Role 3', name: 'Support_Role_Type_Code_3', width: 120, editable: true, hidden: false },
                            {
                                label: 'Support User ID 3', name: 'Support_User_ID_3', width: 120, align: 'center'
                            },
                            {
                                label: 'Support Date Time 3',
                                name: 'Support_Date_Time_3',
                                width: 150
                            },
                            { label: 'Support Role 4', name: 'Support_Role_Type_Code_4', width: 120, editable: true, hidden: false },
                            {
                                label: 'Support User ID 4', name: 'Support_User_ID_4', width: 120, align: 'center'
                            },
                            {
                                label: 'Support Date Time 4',
                                name: 'Support_Date_Time_4',
                                width: 150,
                            },
                            {
                                label: 'Status',
                                name: 'Status',
                                align: "center",
                                width: 90,
                                editable: false
                            }
                        ],
                        viewrecords: true,
                        loadonce: true,
                        restoreAfterSelect: false,
                        saveAfterSelect: true,
                        sortable: true,
                        onPaging: obj.changePage,
                        height: 'auto',
                        rowNum: 100,
                        pager: "#jqGridPager"
                    });
                },
                search: function (self) {
                    if (timer) { clearTimeout(timer); }
                    timer = setTimeout(function () {
                        obj.changePage();
                        $("#jqGrid").jqGrid('filterInput', self.value);
                        var length = jQuery("#jqGrid").jqGrid('getGridParam', 'records');
                        $('#txtRecordsCount').html('Records Count: ' + length);
                    }, 0);
                },
                changePage: function () {
                    lastSelection = "";
                    var grid = $("#jqGrid");
                    var id = $('.inline-edit-cell').parent().parent().prop('id');
                    if (id != undefined) {
                        grid.jqGrid('saveRow', id);
                        grid.jqGrid('restoreRow', id);
                        var row = grid.jqGrid('getRowData', id);
                        if (!jQuery.isEmptyObject(row)) {
                            var dataobj = jQuery("#jqGrid").jqGrid('getGridParam', 'data');
                            index = dataobj.findIndex(x => x.rowid == row.rowid);
                            if (row.rowid > 0 && row.Actionstatus != 1) {
                                row.Actionstatus = 2;
                                grid.jqGrid('getGridParam', 'data')[index] = row;
                            }
                        }
                    }
                }
            }
            obj.Init();
        });
    </script>
</asp:Content>

