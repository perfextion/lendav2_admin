﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI.WebControls;

public partial class admin_LoanExceptions : BasePage
{
    public admin_LoanExceptions()
    {
        Table_Name = "Loan_Exceptions";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        base.Page_Load(sender, e);
        LoanFullID.Value = Loan_Full_ID;
    }

    [WebMethod]
    public static string GetTableInfo(string Loan_Full_ID)
    {
        Count_Query = "Declare @count int; Select @Count = Count(*) From Loan_Exceptions";

        if (!string.IsNullOrEmpty(Loan_Full_ID))
        {
            Count_Query += " Where Loan_Full_ID like '%" + Loan_Full_ID + "%'";
        }

        return Get_Table_Info();
    }

    [WebMethod]
    public static string GetLoanExceptions(string Loan_Full_ID)
    {
        HttpContext.Current.Session["Loan_Full_ID"] = Loan_Full_ID;

        string sql_qry = "SELECT " +
                            "0 as Actionstatus," +
                            "Loan_Exception_ID,Loan_Full_ID," +
                            "Exception_ID,Exception_ID_Level,Exception_ID_Text," +
                            "Tab_ID," +
                            "Mitigation_Ind,Mitigation_Text," +
                            "Support_Ind," +
                            "Support_Role_Type_Code_1," +
                            "Support_User_ID_1," +
                            "convert(varchar, Support_Date_Time_1, 101) as [Support_Date_Time_1]," +
                            "Support_Role_Type_Code_2," +
                            "Support_User_ID_2," +
                            "convert(varchar, Support_Date_Time_2, 101) as [Support_Date_Time_2]," +
                            "Support_Role_Type_Code_3," +
                            "Support_User_ID_3," +
                            "convert(varchar, Support_Date_Time_3, 101) as [Support_Date_Time_3]," +
                            "Support_Role_Type_Code_4," +
                            "Support_User_ID_4," +
                            "convert(varchar, Support_Date_Time_4, 101) as [Support_Date_Time_4]," +
                            "Status " +
                        "FROM Loan_Exceptions";

        if (!string.IsNullOrEmpty(Loan_Full_ID))
        {
            sql_qry += " Where Loan_Full_ID LIKE '%" + Loan_Full_ID + "%' And [Status] = 1";
        }
        else
        {
            sql_qry += " Where [Status] = 1";
        }

        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static void SaveLoanExceptions(dynamic lst)
    {
        foreach (var item in lst)
        {
            string qry = "";
            int val = Convert.ToInt32(item["Actionstatus"]);
            if (val == 1)
            {
                qry = "INSERT INTO Loan_Exceptions" +
                        "(" +
                            "Loan_Full_ID," +
                            "Exception_ID," +
                            "Exception_ID_Level," +
                            "Tab_ID," +
                            "Mitigation_Ind," +
                            "Mitigation_Text," +
                            "Support_Ind," +
                            "Support_Role_Type_Code_1," +
                            "Support_User_ID_1," +
                            "Support_Date_Time_1," +
                            "Support_Role_Type_Code_2," +
                            "Support_User_ID_2," +
                            "Support_Date_Time_2," +
                            "Support_Role_Type_Code_3," +
                            "Support_User_ID_3," +
                            "Support_Date_Time_3," +
                            "Support_Role_Type_Code_4," +
                            "Support_User_ID_4," +
                            "Support_Date_Time_4," +
                            "Status" +
                        ")" +
                    " VALUES(" +
                        "'" + item["Loan_Full_ID"].Replace("'", "''") + "'," +
                        "'" + item["Exception_ID"].Replace("'", "''") + "'," +
                        "'" + item["Exception_ID_Level"].Replace("'", "''") + "'," +
                        "'" + item["Tab_ID"].Replace("'", "''") + "'," +
                        "'" + item["Mitigation_Ind"].Replace("'", "''") + "'," +
                        "'" + item["Mitigation_Text"].Replace("'", "''") + "'," +
                        "'" + item["Support_Ind"].Replace("'", "''") + "'," +
                        "'" + item["Support_Role_Type_Code_1"].Replace("'", "''") + "'," +
                        "'" + item["Support_User_ID_1"].Replace("'", "''") + "'," +
                        "'" + item["Support_Date_Time_1"].Replace("'", "''") + "'," +
                        "'" + item["Support_Role_Type_Code_2"].Replace("'", "''") + "'," +
                        "'" + item["Support_User_ID_2"].Replace("'", "''") + "'," +
                        "'" + item["Support_Date_Time_2"].Replace("'", "''") + "'," +
                        "'" + item["Support_Role_Type_Code_3"].Replace("'", "''") + "'," +
                        "'" + item["Support_User_ID_3"].Replace("'", "''") + "'," +
                        "'" + item["Support_Date_Time_3"].Replace("'", "''") + "'," +
                        "'" + item["Support_Role_Type_Code_4"].Replace("'", "''") + "'," +
                        "'" + item["Support_User_ID_4"].Replace("'", "''") + "'," +
                        "'" + item["Support_Date_Time_4"].Replace("'", "''") + "'," +
                        "'" + item["Status"] + "'" +
                    ")";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 2)
            {
                qry = "UPDATE Loan_Exceptions " +
                    "SET Loan_Full_ID = '" + item["Loan_Full_ID"].Replace("'", "''") + "' ," +
                        "Exception_ID = '" + item["Exception_ID"].Replace("'", "''") + "'," +
                        "Exception_ID_Level = '" + item["Exception_ID_Level"].Replace("'", "''") + "'," +
                        "Tab_ID = '" + item["Tab_ID"].Replace("'", "''") + "'," +
                        "Mitigation_Ind = '" + item["Mitigation_Ind"].Replace("'", "''") + "'," +
                        "Mitigation_Text = '" + item["Mitigation_Text"].Replace("'", "''") + "'," +
                        "Support_Ind = '" + item["Support_Ind"].Replace("'", "''") + "'," +
                        "Support_Role_Type_Code_1 = '" + item["Support_Role_Type_Code_1"].Replace("'", "''") + "'," +
                        "Support_User_ID_1 = '" + item["Support_User_ID_1"].Replace("'", "''") + "'," +
                        "Support_Date_Time_1 = '" + item["Support_Date_Time_1"].Replace("'", "''") + "'," +
                        "Support_Role_Type_Code_2 = '" + item["Support_Role_Type_Code_2"].Replace("'", "''") + "'," +
                        "Support_User_ID_2 = '" + item["Support_User_ID_2"].Replace("'", "''") + "'," +
                        "Support_Date_Time_2 = '" + item["Support_Date_Time_2"].Replace("'", "''") + "'," +
                        "Support_Role_Type_Code_3 = '" + item["Support_Role_Type_Code_3"].Replace("'", "''") + "'," +
                        "Support_User_ID_3 = '" + item["Support_User_ID_3"].Replace("'", "''") + "'," +
                        "Support_Date_Time_3 = '" + item["Support_Date_Time_3"].Replace("'", "''") + "'," +
                        "Support_Role_Type_Code_4 = '" + item["Support_Role_Type_Code_4"].Replace("'", "''") + "'," +
                        "Support_User_ID_4 = '" + item["Support_User_ID_4"].Replace("'", "''") + "'," +
                        "Support_Date_Time_4 = '" + item["Support_Date_Time_4"].Replace("'", "''") + "'," +
                        "Status = '" + item["Status"] + "' " +
                    "Where Loan_Exception_ID=" + item["Loan_Exception_ID"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 3)
            {
                qry = "DELETE FROM Loan_Exceptions  where Loan_Exception_ID=" + item["Loan_Exception_ID"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
        }
    }
}