﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_LoanFarmCropDetail : BasePage
{
    public admin_LoanFarmCropDetail()
    {
        Table_Name = "Loan_Farm_Crop_Detail";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        base.Page_Load(sender, e);
        LoanFullID.Value = Loan_Full_ID;
    }

    [WebMethod]
    public static string GetTableInfo(string Loan_Full_ID)
    {
        Count_Query = "Declare @count int; Select @Count = Count(*) From Loan_Farm_Crop_Detail";

        if (!string.IsNullOrEmpty(Loan_Full_ID))
        {
            Count_Query += " Where Loan_Full_ID like '%" + Loan_Full_ID + "%'";
        }

        return Get_Table_Info();
    }


    [WebMethod]
    public static string LoadTable(string Loan_Full_ID)
    {
        HttpContext.Current.Session["Loan_Full_ID"] = Loan_Full_ID;

        string sql_qry = @"SELECT VC.Crop_Code as [CROP_CODE], VC.Crop_Type_Code, VC.Irr_Prac_Code, VC.Crop_Prac_Code, LF.*, '1' as [Active_Status]
                            FROM [dbo].[Loan_Farm_Crop_Detail] LF 
                            LEFT JOIN V_CROP_PRICE_DETAILS VC ON LF.CROP_CODE = VC.CROP_FULL_KEY
                            WHERE LF.[Status] != 3";

        if(!string.IsNullOrEmpty(Loan_Full_ID))
        {
            sql_qry += " and Loan_full_id like '%" + Loan_Full_ID + "%'";
        }

        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return JsonConvert.SerializeObject(dt);
    }
}