﻿<%@ Page Title="Loan Master" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="LoanMaster.aspx.cs" Inherits="admin_LoanMaster" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style>
         .ui-jqgrid .ui-jqgrid-htable th div {
            height: auto;
            overflow: hidden;
            padding-right: 4px;
            padding-top: 2px;
            position: relative;
            vertical-align: text-top;
            word-wrap: break-word; /* IE 5.5+ and CSS3 */
            white-space: pre-wrap; /* CSS3 */
            white-space: normal !important;
        }

        .ui-jqgrid tr.jqgrow td {
            word-wrap: break-word; /* IE 5.5+ and CSS3 */
            white-space: pre-wrap; /* CSS3 */
            white-space: normal !important;
            height: auto;
            vertical-align: text-top;
            padding-top: 2px;
            padding-bottom: 3px;
        }

        .ui-jqgrid tr.jqgrow td[aria-describeby=jqGrid_Loan_Comment] {
            text-overflow: ellipsis;
            -o-text-overflow: ellipsis;
            word-break: unset;
            white-space:unset;
            white-space: unset;
            height: 100px;
        }

        .ui-jqgrid .ui-jqgrid-htable th {
            height: 70px;
        }

        .cvteste {
            padding-right: 14px !important;
        }

        #content {
            padding: 14px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="loan-transaction-table">
        <div class="row switch-controls">
            <div class="switch-control disabled" id="editControl">
                <label for="editBtn">Edit</label>
                <div class="toggle-btn small">            
                    <input type="checkbox" id="editBtn" class="cb-value" />
                    <span class="round-btn"></span>
                </div>
            </div>
            <div class="switch-control disabled" id="enableControl">
                <label for="enableBtn">Enable</label>
                <div class="toggle-btn small">            
                    <input type="checkbox" id="enableBtn" class="cb-value" />
                    <span class="round-btn"></span>
                </div>
            </div>
        </div>
        <div class="loader"></div>
        <div class="row main-content">
            <div>
                <table id="jqGrid"></table>
                <div id="jqGridPager"></div>
            </div>
        </div>
        <asp:HiddenField ID="LoanFullID" runat="server" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" Runat="Server">
    <script type="text/javascript">  
        var DeleteRows = [];
        var timer;
        var lastSelection;

        var entity_types = {};
        var pref_contacts = {};

        $(function () {
            var obj = {
                Init: function () {
                    $('.loader').show();
                    obj.getListItems();
                    obj.bindGrid();

                    $('#navbtnadd').addClass('disabled');
                    $('#navbtnsave').addClass('disabled');

                    $("#navbtnrefresh").on('click', function () {
                        $('.loader').show();
                        $('#txtSearchBar').val('');
                        obj.bindGrid();
                    });

                    $("#txtSearchBar").on("keyup", function () {
                        var self = this;
                        obj.search(self);
                    });

                    $("#navbtndownload").on("click", function () {
                        $("#jqGrid").jqGrid("exportToExcel", {
                            includeLabels: true,
                            includeGroupHeader: true,
                            includeFooter: true,
                            fileName: "Loan Master.xlsx",
                            maxlength: 40
                        });
                    });

                    $('#navbtncolumns').click(function () {
                        $("#jqGrid").columnChooser({});
                    });

                    obj.initLoanSelect();
                },
                initLoanSelect: function () {
                    var hiddenVal = $('#ContentPlaceHolder1_LoanFullID').val();

                    if (hiddenVal) {
                        $('#txtSearchLoan').val(hiddenVal);
                    }

                    $('.select-loan').show();

                    $('#selectLoanBtn').click(function () {
                        $('.loader').show();
                        var loan = $('#txtSearchLoan').val();
                        $('#ContentPlaceHolder1_LoanFullID').val(loan);

                        obj.bindGrid();
                    });
                },
                getTableInfo: function () {
                    var hiddenVal = $('#ContentPlaceHolder1_LoanFullID').val();
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "LoanMaster.aspx/GetTableInfo",
                        data: JSON.stringify({ Loan_Full_ID: hiddenVal }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            var data = JSON.parse(res.d);

                            var table_info = data.Table_Info[0];
                            $('#txtTableName').html(table_info.Table_Name);
                            $('#txtLastUpdated').html(table_info.Last_Updated);
                            $('#txtRecordsCount').html(table_info.Records_Count);

                            if (data.CanEdit) {
                                $('#editControl').removeClass('disabled');
                            } else {
                                $('#editControl').addClass('disabled');
                            }

                            $('#enableControl').addClass('disabled');
                            
                        }
                    });
                },
                getListItems: function () {
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "LoanMaster.aspx/GetListItems",
                        data: JSON.stringify({}),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            var data = JSON.parse(res.d);

                            entity_types = {};
                            pref_contacts = {};

                            for (var i of data) {
                                if (i.List_Group_Code == 'BORROWER_ENTITY_TYPE') {
                                    entity_types[i.List_Item_Code] = i.List_Item_Name;
                                }

                                if (i.List_Group_Code == 'PREF_CONTACT') {
                                    pref_contacts[i.List_Item_Value] = i.List_Item_Name;
                                }
                            }
                        }

                    });
                },
                bindGrid: function () {
                    var hiddenVal = $('#ContentPlaceHolder1_LoanFullID').val();
                    $.ajax({
                        type: "POST",
                        url: "LoanMaster.aspx/LoadTable",
                        data: JSON.stringify({ Loan_Full_ID: hiddenVal }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var resData = JSON.parse(data.d);
                            $('.loader').show();
                            for (var i = 0; i < resData.length; i++) {
                                resData[i].rowid = i + 1;
                            }
                            obj.loadgrid(resData);
                            obj.getTableInfo();
                            $('.loader').hide();
                        }
                    });
                },
                loadgrid: function (data) {
                    $.jgrid.gridUnload("#jqGrid");
                    $("#jqGrid").jqGrid({
                        datatype: "local",
                        styleUI: 'Bootstrap',
                        data: data,
                        colModel: [
                            { label: 'rowid', name: 'rowid', width: 75, editable: false, key: true, hidden: true, hidedlg: true },
                            { label: 'Loan ID', name: 'Loan_ID', width: 90, align: 'center', editable: false, hidden: false },
                            { label: 'Loan Seq num', name: 'Loan_Seq_num', width: 90, align: 'center', editable: false, hidden: false },
                            { label: 'Loan Full ID', name: 'Loan_Full_ID', width: 120, align: 'center', editable: false, hidden: false },
                            { label: 'Crop Year', name: 'Crop_Year', width: 90, align: 'center', editable: false, hidden: false },
                            { label: 'Region ID', name: 'Region_ID', width: 90, align: 'center', editable: false, hidden: false },
                            { label: 'Region Name', name: 'Region_Name', width: 120, align: 'left', editable: false, hidden: false },
                            { label: 'Office ID', name: 'Office_ID', width: 90, align: 'center', editable: false, hidden: false },
                            { label: 'Office Name', name: 'Office_Name', width: 120, align: 'left', editable: false, hidden: false },
                            { label: 'Loan Officer ID', name: 'Loan_Officer_ID', width: 120, align: 'center', editable: false, hidden: false },
                            { label: 'Loan Officer Name', name: 'Loan_Officer_Name', width: 150, align: 'left', editable: false, hidden: false },
                            { label: 'Farmer ID', name: 'Farmer_ID', width: 90, align: 'center', editable: false, hidden: false },
                            { label: 'Borrower ID', name: 'Borrower_ID', width: 90, align: 'center', editable: false, hidden: false },
                            { label: 'Loan Type Code', name: 'Loan_Type_Code', width: 90, align: 'center', editable: false, hidden: false },
                            { label: 'Loan Type Name', name: 'Loan_Type_Name', width: 90, align: 'center', editable: false, hidden: false },
                            { label: 'Borrower ID Type', name: 'Borrower_ID_Type', width: 90, align: 'center', editable: false, hidden: false },
                            {
                                label: 'Borrower SSN Hash', name: 'Borrower_SSN_Hash', width: 90, align: 'center', editable: false, hidden: false,
                                formatter: function (cellvalue, row, options) {
                                    return obj.formatSSN(cellvalue);
                                }
                            },
                            {
                                label: 'Borrower Entity Type Code', name: 'Borrower_Entity_Type_Code', width: 120, editable: false, hidden: false,
                                edittype: 'select',
                                editoptions: {
                                    value: entity_types
                                },
                                formatter: 'select'
                            },
                            { label: 'Borrower Last Name', name: 'Borrower_Last_Name', width: 120, editable: false, hidden: false },
                            { label: 'Borrower First Name', name: 'Borrower_First_Name', width: 180, editable: false, hidden: false },
                            { label: 'Borrower MI', name: 'Borrower_MI', width: 90, align: 'center', editable: false, hidden: false },
                            { label: 'Borrower Address', name: 'Borrower_Address', width: 180, editable: false, hidden: false },
                            { label: 'Borrower City', name: 'Borrower_City', width: 90, editable: false, hidden: false },
                            { label: 'Borrower State Abbrev', name: 'Borrower_State_Abbrev', width: 90, align: 'center', editable: false, hidden: false },
                            { label: 'Borrower State', name: 'Borrower_State', width: 120, editable: false, hidden: false },
                            { label: 'Borrower Zip', name: 'Borrower_Zip', width: 90, editable: false, hidden: false },
                            {
                                label: 'Borrower Phone', name: 'Borrower_Phone', width: 120, editable: false, hidden: false, formatter: function (cellvalue, row, options) {
                                    return obj.formatPhone(cellvalue);
                                }
                            },
                            { label: 'Borrower Email', name: 'Borrower_Email', width: 180, editable: false, hidden: false },
                            { label: 'Borrower DL State Abbrev', name: 'Borrower_DL_State_Abbrev', width: 90, align: 'center', editable: false, hidden: false },
                            { label: 'Borrower DL State', name: 'Borrower_DL_State', width: 120, editable: false, hidden: false },
                            { label: 'Borrower DL Num', name: 'Borrower_DL_Num', width: 90, editable: false, hidden: false },
                            { label: 'Borrower DOB', name: 'Borrower_DOB', width: 120, align: 'center', editable: false, hidden: false, formatter: 'date', formatoptions: { srcformat: 'd/m/Y', newformat: 'd/m/Y' } },
                            {
                                label: 'Borrower Preferred Contact Ind', name: 'Borrower_Preferred_Contact_Ind', width: 90, editable: false, hidden: false,
                                edittype: 'select',
                                editoptions: {
                                    value: pref_contacts
                                },
                                formatter: 'select'
                            },
                            { label: 'Borrower County ID', name: 'Borrower_County_ID', width: 90, align: 'center', editable: false, hidden: false },
                            { label: 'Borrower County Name', name: 'Borrower_County_Name', width: 120, editable: false, hidden: false },
                            { label: 'Borrower Lat', name: 'Borrower_Lat', width: 120, editable: false, hidden: false },
                            { label: 'Borrower Long', name: 'Borrower_Long', width: 120, editable: false, hidden: false },
                            { label: 'Co Borrower Count', name: 'Co_Borrower_Count', width: 90,  align: 'center', editable: false, hidden: false },
                            {
                                label: 'Spouse SSN Hash', name: 'Spouse_SSN_Hash', width: 90, align: 'center', editable: false, hidden: false,
                                formatter: function (cellvalue, row, options) {
                                    return obj.formatSSN(cellvalue);
                                }
                            },
                            { label: 'Spouse Last name', name: 'Spouse_Last_name', width: 120, editable: false, hidden: false },
                            { label: 'Spouse First Name', name: 'Spouse_First_Name', width: 120, editable: false, hidden: false },
                            { label: 'Spouse MI', name: 'Spouse_MI', width: 90, align: 'center', editable: false, hidden: false },
                            { label: 'Spouse Address', name: 'Spouse_Address', width: 120, editable: false, hidden: false },
                            { label: 'Spouse City', name: 'Spouse_City', width: 90, editable: false, hidden: false },
                            { label: 'Spouse State', name: 'Spouse_State', width: 120, editable: false, hidden: false },
                            { label: 'Spouse State Abbrev', name: 'Spouse_State_Abbrev',align: 'center', width: 90, editable: false, hidden: false },
                            { label: 'Spouse Zip', name: 'Spouse_Zip', width: 90, editable: false, hidden: false },
                            {
                                label: 'Spouse Phone', name: 'Spouse_Phone', width: 120, editable: false, hidden: false, formatter: function (cellvalue, row, options) {
                                    return obj.formatPhone(cellvalue);
                                }
                            },
                            { label: 'Spouse Email', name: 'Spouse_Email', width: 180, editable: false, hidden: false },
                            {
                                label: 'Spouse Preferred Contact Ind', name: 'Spouse_Preferred_Contact_Ind', width: 90, editable: false, hidden: false,
                                edittype: 'select',
                                editoptions: {
                                    value: pref_contacts
                                },
                                formatter: 'select'
                            },
                            { label: 'Farmer ID Type', name: 'Farmer_ID_Type', width: 90, align: 'center', editable: false, hidden: false },
                            {
                                label: 'Farmer SSN Hash', name: 'Farmer_SSN_Hash', width: 90, align: 'center', editable: false, hidden: false, formatter: function (cellvalue, row, options) {
                                    return obj.formatSSN(cellvalue);
                                }
                            },
                            {
                                label: 'Farmer Entity Type', name: 'Farmer_Entity_Type', width: 120, editable: false, hidden: false,
                                edittype: 'select',
                                editoptions: {
                                    value: entity_types
                                },
                                formatter: 'select'
                            },
                            { label: 'Farmer Last Name', name: 'Farmer_Last_Name', width: 120, editable: false, hidden: false },
                            { label: 'Farmer First Name', name: 'Farmer_First_Name', width: 180, editable: false, hidden: false },
                            { label: 'Farmer MI', name: 'Farmer_MI', width: 90, align: 'center', editable: false, hidden: false },
                            { label: 'Farmer Address', name: 'Farmer_Address', width: 120, editable: false, hidden: false },
                            { label: 'Farmer City', name: 'Farmer_City', width: 120, editable: false, hidden: false },
                            { label: 'Farmer State', name: 'Farmer_State', width: 90, editable: false, hidden: false },
                            { label: 'Farmer State Abbrev', name: 'Farmer_State_Abbrev', align: 'center', width: 90, editable: false, hidden: false },
                            { label: 'Farmer Zip', name: 'Farmer_Zip', width: 90, align: 'center', editable: false, hidden: false },
                            {
                                label: 'Farmer Phone', name: 'Farmer_Phone', width: 120, editable: false, hidden: false, formatter: function (cellvalue, row, options) {
                                    return obj.formatPhone(cellvalue);
                                }
                            },
                            { label: 'Farmer Email', name: 'Farmer_Email', width: 180, editable: false, hidden: false },
                            { label: 'Farmer DL State Abbrev', name: 'Farmer_DL_State_Abbrev', align: 'center', width: 90, editable: false, hidden: false },
                            { label: 'Farmer DL State', name: 'Farmer_DL_State', width: 90, editable: false, hidden: false },
                            { label: 'Farmer DL Num', name: 'Farmer_DL_Num', width: 90, editable: false, hidden: false },
                            { label: 'Farmer DOB', name: 'Farmer_DOB', width: 90, align: 'center',  editable: false, hidden: false, formatter: 'date', formatoptions: { srcformat: 'd/m/Y', newformat: 'd/m/Y' } },
                            {
                                label: 'Farmer Preferred Contact Ind', name: 'Farmer_Preferred_Contact_Ind', width: 90, editable: false, hidden: false,
                                edittype: 'select',
                                editoptions: {
                                    value: pref_contacts
                                },
                                formatter: 'select'
                            },
                            { label: 'FARMing', name: 'Year_Begin_Farming', width: 90, align: 'center', editable: false, hidden: false },
                            { label: 'ARMing', name: 'Year_Begin_Client', width: 90, align: 'center', editable: false, hidden: false },
                            { label: 'Referral Type Code', name: 'Referral_Type_Code', align: 'center', width: 90, editable: false, hidden: false },
                            { label: 'Primary AIP ID', name: 'Primary_AIP_ID', align: 'center', width: 90, editable: false, hidden: false },
                            { label: 'Primary AIP Name', name: 'Primary_AIP_Name', width: 120, editable: false, hidden: false },
                            { label: 'Distributor ID', name: 'Distributor_ID', align: 'center',  width: 90, editable: false, hidden: false },
                            { label: 'Distributor Name', name: 'Distributor_Name', width: 120, editable: false, hidden: false },
                            { label: 'Primary Agency ID', name: 'Primary_Agency_ID', align: 'center', width: 90, editable: false, hidden: false },
                            { label: 'Primary Agency Name', name: 'Primary_Agency_Name', width: 120, editable: false, hidden: false },
                            { label: 'Current Bankruptcy Status', name: 'Current_Bankruptcy_Status', align: 'center', width: 90, editable: false, hidden: false },
                            { label: 'Original Bankruptcy Status', name: 'Original_Bankruptcy_Status', align: 'center', width: 90, editable: false, hidden: false },
                            { label: 'Previous Bankruptcy Status', name: 'Previous_Bankruptcy_Status', align: 'center', width: 90, editable: false, hidden: false },
                            { label: 'Judgement Ind', name: 'Judgement_Ind', width: 90, align: 'center', editable: false, hidden: false },
                            { label: 'Local Ind', name: 'Local_Ind', width: 90, align: 'center', editable: false, hidden: false },
                            { label: 'Controlled Disbursement Ind', name: 'Controlled_Disbursement_Ind', align: 'center', width: 90, editable: false, hidden: false },
                            { label: 'Watch List Ind', name: 'Watch_List_Ind', width: 90, align: 'center', editable: false, hidden: false },
                            { label: 'Rate Yield Ref Yield Percent', name: 'Rate_Yield_Ref_Yield_Percent', width: 90, align: 'center', editable: false, hidden: false },
                            { label: 'Credit Score Date', name: 'Credit_Score_Date', width: 90, align: 'center', editable: false, hidden: false, formatter: 'date', formatoptions: { srcformat: 'd/m/Y', newformat: 'd/m/Y' } },
                            { label: 'Credit Score', name: 'Credit_Score', width: 90, align: 'center', editable: false, hidden: false, align: 'center' },
                            { label: 'Financials Date', name: 'Financials_Date', width: 90, align: 'center', editable: false, hidden: false, formatter: 'date', formatoptions: { srcformat: 'd/m/Y', newformat: 'd/m/Y' } },
                            { label: 'CPA Prepared Financials', name: 'CPA_Prepared_Financials', align: 'center', width: 90, editable: false, hidden: false },
                            {
                                label: 'Total Acres', name: 'Total_Acres', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatNumber(cellvalue);
                                }
                            },
                            {
                                label: 'Current Assets', name: 'Current_Assets', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Inter Assets', name: 'Inter_Assets', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Fixed Assets', name: 'Fixed_Assets', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Total Assets', name: 'Total_Assets', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Current Assets Disc Percent', name: 'Current_Assets_Disc_Percent', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatNumber(cellvalue, 1) + '%';
                                }
                            },
                            {
                                label: 'Inter Assets Disc Percent', name: 'Inter_Assets_Disc_Percent', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatNumber(cellvalue, 1) + '%';
                                }
                            },
                            {
                                label: 'Fixed Assets Disc Percent', name: 'Fixed_Assets_Disc_Percent', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatNumber(cellvalue, 1) + '%';
                                }
                            },
                            {
                                label: 'Current Liabilities', name: 'Current_Liabilities', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Inter Liabilities', name: 'Inter_Liabilities', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Fixed Liabilities', name: 'Fixed_Liabilities', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Total Liabilities', name: 'Total_Liabilities', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Current Net Worth', name: 'Current_Net_Worth', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Inter Net Worth', name: 'Inter_Net_Worth', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Fixed Net Worth', name: 'Fixed_Net_Worth', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Total Net Worth', name: 'Total_Net_Worth', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Current Disc Net Worth', name: 'Current_Disc_Net_Worth', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Inter Disc Net Worth', name: 'Inter_Disc_Net_Worth', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Fixed Disc Net Worth', name: 'Fixed_Disc_Net_Worth', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Total Disc Net Worth', name: 'Total_Disc_Net_Worth', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            { label: 'Income Detail Ind', name: 'Income_Detail_Ind', width: 90, editable: false, hidden: false },
                            { label: 'Borrower 3yr Tax Returns', name: 'Borrower_3yr_Tax_Returns', width: 90, editable: false, hidden: false },
                            { label: 'Borrower Farm Financial Rating', name: 'Borrower_Farm_Financial_Rating', width: 90, editable: false, hidden: false, align: 'center' },
                            { label: 'Borrower Rating', name: 'Borrower_Rating', width: 90, editable: false, hidden: false, align: 'center' },
                            {
                                label: 'Requested Credit', name: 'Requested_Credit', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            { label: 'Maturity Date', name: 'Maturity_Date', width: 90, editable: false, hidden: false, formatter: 'date', formatoptions: { srcformat: 'd/m/Y', newformat: 'd/m/Y' } },
                            { label: 'Original Maturity Date', name: 'Original_Maturity_Date', width: 90, editable: false, hidden: false, formatter: 'date', formatoptions: { srcformat: 'd/m/Y', newformat: 'd/m/Y' } },
                            { label: 'Estimated Days', name: 'Estimated_Days', width: 90, editable: false, hidden: false, align: 'center' },
                            {
                                label: 'Origination Fee Percent', name: 'Origination_Fee_Percent', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatNumber(cellvalue, 1) + '%';
                                }
                            },
                            {
                                label: 'Service Fee Percent', name: 'Service_Fee_Percent', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatNumber(cellvalue, 1) + '%';
                                }
                            },
                            {
                                label: 'Extension Fee Percent', name: 'Extension_Fee_Percent', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatNumber(cellvalue, 1) + '%';
                                }
                            },
                            {
                                label: 'Interest Percent', name: 'Interest_Percent', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatNumber(cellvalue, 1) + '%';
                                }
                            },
                            {
                                label: 'Origination Fee Amount', name: 'Origination_Fee_Amount', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Service Fee Amount', name: 'Service_Fee_Amount', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Extension Fee Amount', name: 'Extension_Fee_Amount', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Interest Est Amount', name: 'Interest_Est_Amount', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Delta Origination Fee Amount', name: 'Delta_Origination_Fee_Amount', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Delta Service Fee Amount', name: 'Delta_Service_Fee_Amount', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Delta Extension Fee Amount', name: 'Delta_Extension_Fee_Amount', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Delta Interest Est Amount', name: 'Delta_Interest_Est_Amount', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Total Crop Acres', name: 'Total_Crop_Acres', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatNumber(cellvalue, 1);
                                }
                            },
                            {
                                label: 'Non Rev Ins Col', name: 'Non_Rev_Ins_Col', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Rev Ins Col', name: 'Rev_Ins_Col', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'ARM Commitment', name: 'ARM_Commitment', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Dist Commitment', name: 'Dist_Commitment', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Third Party Credit', name: 'Third_Party_Credit', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Total Commitment', name: 'Total_Commitment', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Delta Crop Acres', name: 'Delta_Crop_Acres', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatNumber(cellvalue, 1);
                                }
                            },
                            {
                                label: 'Delta ARM Commitment', name: 'Delta_ARM_Commitment', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Delta Dist Commitment', name: 'Delta_Dist_Commitment', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Delta Third Party Credit', name: 'Delta_Third_Party_Credit', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Delta Total Commitment', name: 'Delta_Total_Commitment', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Net Other Income', name: 'Net_Other_Income', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Total Revenue', name: 'Total_Revenue', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Cash Flow Amount', name: 'Cash_Flow_Amount', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Cash Flow Percent', name: 'Cash_Flow_Percent', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatNumber(cellvalue, 1) + '%';
                                }
                            },
                            {
                                label: 'Break Even Percent', name: 'Break_Even_Percent', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatNumber(cellvalue, 1) + '%';
                                }
                            },
                            {
                                label: 'Risk Cushion Amount', name: 'Risk_Cushion_Amount', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Risk Cushion Percent', name: 'Risk_Cushion_Percent', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatNumber(cellvalue, 1) + '%';
                                }
                            },
                            {
                                label: 'ARM Margin Amount', name: 'ARM_Margin_Amount', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'ARM Margin Percent', name: 'ARM_Margin_Percent', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatNumber(cellvalue, 1) + '%';
                                }
                            },
                            {
                                label: 'Total Margin Percent', name: 'Total_Margin_Percent', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatNumber(cellvalue, 1) + '%';
                                }
                            },
                            {
                                label: 'Total margin Amount', name: 'Total_margin_Amount', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'LTV Percent', name: 'LTV_Percent', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatNumber(cellvalue, 1) + '%';
                                }
                            },
                            {
                                label: 'Return Percent', name: 'Return_Percent', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatNumber(cellvalue, 1) + '%';
                                }
                            },
                            {
                                label: 'Ag Pro Requested Credit', name: 'Ag_Pro_Requested_Credit', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Delta Total Revenue', name: 'Delta_Total_Revenue', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Delta Total Return Fee', name: 'Delta_Total_Return_Fee', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Delta Cash Flow Amount', name: 'Delta_Cash_Flow_Amount', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Delta Cash Flow Percent', name: 'Delta_Cash_Flow_Percent', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatNumber(cellvalue, 1) + '%';
                                }
                            },
                            {
                                label: 'Delta Risk Cushion Amount', name: 'Delta_Risk_Cushion_Amount', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Delta Risk Cushion Percent', name: 'Delta_Risk_Cushion_Percent', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatNumber(cellvalue, 1) + '%';
                                }
                            },
                            {
                                label: 'Delta Return Percent', name: 'Delta_Return_Percent', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatNumber(cellvalue, 1) + '%';
                                }
                            },
                            {
                                label: 'Delta Current Addendum Percent', name: 'Delta_Current_Addendum_Percent', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatNumber(cellvalue, 1) + '%';
                                }
                            },
                            {
                                label: 'Net Market Value Crops', name: 'Net_Market_Value_Crops', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Net Market Value Insurance', name: 'Net_Market_Value_Insurance', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Net Market Value FSA', name: 'Net_Market_Value_FSA', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Net Market Value Livestock', name: 'Net_Market_Value_Livestock', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Net Market Value Stored Crops', name: 'Net_Market_Value_Stored_Crops', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Net Market Value Equipment', name: 'Net_Market_Value_Equipment', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Net Market Value Real Estate', name: 'Net_Market_Value_Real_Estate', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Net Market Value Other', name: 'Net_Market_Value_Other', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Net Market Value Total', name: 'Net_Market_Value_Total', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Disc value Crops', name: 'Disc_value_Crops', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Disc value Insurance', name: 'Disc_value_Insurance', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Disc value FSA', name: 'Disc_value_FSA', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Disc value Livestock', name: 'Disc_value_Livestock', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Disc value Stored Crops', name: 'Disc_value_Stored_Crops', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Disc value Equipment', name: 'Disc_value_Equipment', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Disc value Real Estate', name: 'Disc_value_Real_Estate', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Disc value Other', name: 'Disc_value_Other', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Disc value Total', name: 'Disc_value_Total', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Ins Value Crops', name: 'Ins_Value_Crops', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Ins Value FSA', name: 'Ins_Value_FSA', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Ins Value Livestock', name: 'Ins_Value_Livestock', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Ins Value Stored Crops', name: 'Ins_Value_Stored_Crops', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Ins Value Equipment', name: 'Ins_Value_Equipment', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Ins Value Real Estate', name: 'Ins_Value_Real_Estate', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Ins Value Other', name: 'Ins_Value_Other', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Ins Value Total', name: 'Ins_Value_Total', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Disc Ins Value Crops', name: 'Disc_Ins_Value_Crops', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Disc Ins Value FSA', name: 'Disc_Ins_Value_FSA', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Disc Ins Value Livestock', name: 'Disc_Ins_Value_Livestock', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Disc Ins Value Stored Crops', name: 'Disc_Ins_Value_Stored_Crops', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Disc Ins Value Equipment', name: 'Disc_Ins_Value_Equipment', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Disc Ins Value Real Estate', name: 'Disc_Ins_Value_Real_Estate', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Disc Ins Value Other', name: 'Disc_Ins_Value_Other', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Disc Ins Value Total', name: 'Disc_Ins_Value_Total', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'CEI Value', name: 'CEI_Value', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Prev Addendum Percent', name: 'Prev_Addendum_Percent', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatNumber(cellvalue, 1) + '%';
                                }
                            },
                            {
                                label: 'Current Addendum Percent', name: 'Current_Addendum_Percent', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatNumber(cellvalue, 1) + '%';
                                }
                            },
                            { label: 'Decision Date', name: 'Decision_Date', width: 90, editable: false, hidden: false ,formatter: 'date', formatoptions: { srcformat: 'd/m/Y', newformat: 'd/m/Y' } },
                            { label: 'Close Date', name: 'Close_Date', width: 90, editable: false, hidden: false, formatter: 'date', formatoptions: { srcformat: 'd/m/Y', newformat: 'd/m/Y' } },
                            { label: 'Primary Col', name: 'Primary_Col', width: 90, editable: false, hidden: false },
                            { label: 'Loan Pending Action Type Code', name: 'Loan_Pending_Action_Type_Code', width: 90, editable: false, hidden: false },
                            { label: 'Loan Pending Action Level', name: 'Loan_Pending_Action_Level', width: 90, editable: false, hidden: false },
                            { label: 'Loan Status', name: 'Loan_Status', width: 90, editable: false, hidden: false, align: 'center' },
                            { label: 'Prev Loan Status', name: 'Prev_Loan_Status', width: 90, editable: false, hidden: false, align: 'center' },
                            { label: 'Nortridge API In ID', name: 'Nortridge_API_In_ID', width: 90, editable: false, hidden: false },
                            {
                                label: 'Third Party Total Budget', name: 'Third_Party_Total_Budget', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Dist Total Budget', name: 'Dist_Total_Budget', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'ARM Total Budget', name: 'ARM_Total_Budget', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Total ARM Fees And Interest', name: 'Total_ARM_Fees_And_Interest', width: 90, editable: false, align: 'right', hidden: false, formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Total Return Fee', name: 'Total_Return_Fee', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Loan Total Budget', name: 'Loan_Total_Budget', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'ARM Extension Fee', name: 'ARM_Extension_Fee', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'Dist Rate Fee', name: 'Dist_Rate_Fee', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'ARM Rate Fee', name: 'ARM_Rate_Fee', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            { label: 'Aff ID', name: 'Aff_ID', width: 90, editable: false, hidden: false },
                            { label: 'Cross Col ID', name: 'Cross_Col_ID', width: 90, editable: false, hidden: false },
                            {
                                label: 'Outstanding Balance', name: 'Outstanding_Balance', width: 90, editable: false, align: 'right', hidden: false, formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'AIP Verified', name: 'AIP_Verified', width: 90, editable: false, hidden: false, edittype: 'select',
                                editoptions:
                                {
                                    value:
                                    {
                                        'true': 'Yes',
                                        'false': 'No'
                                    }
                                },
                                formatter: 'select'
                            },
                            {
                                label: 'AIP Acres Verified', name: 'AIP_Acres_Verified', width: 90, editable: false, hidden: false, edittype: 'select',
                                editoptions:
                                {
                                    value:
                                    {
                                        'true': 'Yes',
                                        'false': 'No'
                                    }
                                },
                                formatter: 'select'
                            },
                            {
                                label: 'FSA Acres Verified', name: 'FSA_Acres_Verified', width: 90, editable: false, hidden: false, edittype: 'select',
                                editoptions:
                                {
                                    value:
                                    {
                                        'true': 'Yes',
                                        'false': 'No'
                                    }
                                },
                                formatter: 'select'
                            },
                            {
                                label: 'Acres Mapped', name: 'Acres_Mapped', width: 90, editable: false, hidden: false, edittype: 'select',
                                editoptions:
                                {
                                    value:
                                    {
                                        'true': 'Yes',
                                        'false': 'No'
                                    }
                                },
                                formatter: 'select'
                            },
                            {
                                label: 'Past Due Amount', name: 'Past_Due_Amount', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            { label: 'Last Nortridge Sync Date', name: 'Last_Nortridge_Sync_Date', width: 90, editable: false, hidden: false, formatter: 'date', formatoptions: { srcformat: 'd/m/Y', newformat: 'd/m/Y' } },
                            { label: 'Loan Tolerance Level Ind', name: 'Loan_Tolerance_Level_Ind', width: 90, editable: false, hidden: false },
                            {
                                label: 'Latest', name: 'Is_Latest', width: 90, editable: false, hidden: false, edittype: 'select',
                                editoptions:
                                {
                                    value:
                                    {
                                        'true': 'Yes',
                                        'false': 'No'
                                    }
                                },
                                formatter: 'select'
                            },
                            { label: 'Legacy Loan ID', name: 'Legacy_Loan_ID', width: 90, editable: false, hidden: false },
                            {
                                label: 'CAF1', name: 'CAF1', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'CAF2', name: 'CAF2', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'CAF3', name: 'CAF3', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'CAF4', name: 'CAF4', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'CA5', name: 'CA5', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'CA6', name: 'CA6', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'CA7', name: 'CA7', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'CA8', name: 'CA8', width: 90, editable: false, hidden: false, align: 'right', formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'CA9', name: 'CA9', width: 90, editable: false, hidden: false, align: 'right',
                                formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            {
                                label: 'CA10', name: 'CA10', width: 90, editable: false, hidden: false, align: 'right',
                                formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue);
                                }
                            },
                            { label: 'Nortridge Upload Date', name: 'Nortridge_UploadDate', width: 90, editable: false, hidden: false, formatter: 'date', formatoptions: { srcformat: 'd/m/Y', newformat: 'd/m/Y' } },
                            { label: 'Nortridge Status', name: 'Nortridge_Status', width: 90, editable: false, hidden: false, align: 'center' },
                            { label: 'Status', name: 'Active_Ind', width: 90, editable: false, hidden: false, align: 'center' }
                        ],
                        viewrecords: true,
                        loadonce: true,
                        restoreAfterSelect: false,
                        saveAfterSelect: true,
                        sortable: true,
                        onPaging: obj.changePage,
                        height: 'auto',
                        rowNum: 100,
                        pager: "#jqGridPager"
                    });
                },
                toNumber: function toNumber(val) {
                    let value = parseFloat(val);

                    if (isNaN(value)) {
                        value = 0;
                    }
                    return value;
                },
                formatPhone: function (phone) {
                    if (phone) {
                        phone = phone.replace(/[^\d]/g, "");

                        if (phone.length == 10) {
                            return phone.replace(/(\d{3})(\d{3})(\d{4})/, "($1) $2-$3");
                        }

                        return phone;
                    }
                    return '';
                },
                formatSSN: function (ssn) {
                    if (ssn) {
                        ssn = ssn.replace(/[^\d]/g, "");

                        if (ssn.length == 9) {
                            return ssn.replace(/(\d{3})(\d{2})(\d{4})$/, "$1-$2-$3");
                        }

                        return ssn;
                    }
                    return '';
                },
                formatCurrency: function formatCurrency(val, decimals = 0) {
                    var value = obj.toNumber(val);

                    const formatter = new Intl.NumberFormat('en-US', {
                        style: 'currency',
                        currency: 'USD',
                        minimumFractionDigits: decimals
                    });

                    return formatter.format(value);
                },
                formatNumber: function formatNumber(val, decimals = 0) {
                    var value = obj.toNumber(val);
                    const formatter = new Intl.NumberFormat('en-US', {
                        minimumFractionDigits: decimals
                    });

                    return formatter.format(value);
                },
                search: function (self) {
                    if (timer) { clearTimeout(timer); }
                    timer = setTimeout(function () {
                        obj.changePage();
                        $("#jqGrid").jqGrid('filterInput', self.value);
                    }, 0);
                },
                changePage: function () {
                    lastSelection = "";
                    var grid = $("#jqGrid");
                    var id = $('.inline-edit-cell').parent().parent().prop('id');
                    if (id != undefined) {
                        grid.jqGrid('saveRow', id);
                        grid.jqGrid('restoreRow', id);
                        var row = grid.jqGrid('getRowData', id);
                        if (!jQuery.isEmptyObject(row)) {
                            var dataobj = jQuery("#jqGrid").jqGrid('getGridParam', 'data');
                            index = dataobj.findIndex(x => x.rowid == row.rowid);
                            if (row.rowid > 0 && row.Actionstatus != 1) {
                                row.Actionstatus = 2;
                                grid.jqGrid('getGridParam', 'data')[index] = row;
                            }
                        }
                    }
                }
            }
            obj.Init();
        });
    </script>
</asp:Content>




