﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_LoanOtherIncome : BasePage
{
    public admin_LoanOtherIncome()
    {
        Table_Name = "Loan_Other_Income";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        base.Page_Load(sender, e);
        LoanFullID.Value = Loan_Full_ID;
    }

    [WebMethod]
    public static string GetTableInfo(string Loan_Full_ID)
    {
        Count_Query = @"Declare @Count int;
                        select @Count = Count(*) from Loan_Other_Income LM";

        if (!string.IsNullOrEmpty(Loan_Full_ID))
        {
            Count_Query += " Where Loan_Full_ID like '%" + Loan_Full_ID + "%'";
        }

        return Get_Table_Info();
    }

    [WebMethod]
    public static string LoadTable(string Loan_Full_ID)
    {
        HttpContext.Current.Session["Loan_Full_ID"] = Loan_Full_ID;

        string sql_qry = @"SELECT LO.*, RO.Sort_Order
                            FROM [dbo].[Loan_Other_Income] LO 
                            JOIN Ref_Other_Income RO ON LO.Other_Income_Name = RO.Other_Income_Name";



        if (!string.IsNullOrEmpty(Loan_Full_ID))
        {
            sql_qry += "  and LO.Loan_Full_ID like '%" + Loan_Full_ID + "%'";
        }

        sql_qry += " Order By LO.Loan_Full_ID, RO.Sort_Order";
                            

        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return JsonConvert.SerializeObject(dt);
    }
}