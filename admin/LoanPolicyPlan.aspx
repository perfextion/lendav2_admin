﻿<%@ Page Title="Loan Policy Plan" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="LoanPolicyPlan.aspx.cs" Inherits="admin_LoanPolicyPlan" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style>
        .ui-jqgrid .ui-jqgrid-htable th div {
            height: auto;
            overflow: hidden;
            padding-right: 4px;
            padding-top: 2px;
            position: relative;
            vertical-align: text-top;
            word-wrap: break-word; /* IE 5.5+ and CSS3 */
            white-space: pre-wrap; /* CSS3 */
            white-space: normal !important;
        }

        .ui-jqgrid tr.jqgrow td {
            word-wrap: break-word; /* IE 5.5+ and CSS3 */
            white-space: pre-wrap; /* CSS3 */
            white-space: normal !important;
            height: auto;
            vertical-align: text-top;
            padding-top: 2px;
            padding-bottom: 3px;
        }

        .cvteste {
            padding-right: 14px !important;
        }        

        #content {
            padding: 14px  !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="loan-transaction-table">
        <div class="row switch-controls">
            <div class="switch-control disabled" id="editControl">
                <label for="editBtn">Edit</label>
                <div class="toggle-btn small">            
                    <input type="checkbox" id="editBtn" class="cb-value" />
                    <span class="round-btn"></span>
                </div>
            </div>
            <div class="switch-control disabled" id="enableControl">
                <label for="enableBtn">Enable</label>
                <div class="toggle-btn small">            
                    <input type="checkbox" id="enableBtn" class="cb-value" />
                    <span class="round-btn"></span>
                </div>
            </div>
        </div>
        <div class="loader"></div>
        <div class="row main-content">
            <div>
                <table id="jqGrid"></table>
                <div id="jqGridPager"></div>
            </div>
        </div>
        <asp:HiddenField ID="LoanFullID" runat="server" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" Runat="Server">
    <script type="text/javascript">  
        var DeleteRows = [];
        var timer;
        var lastSelection;

        $(function () {
            var obj = {
                Init: function () {
                    $('.loader').show();
                    obj.bindGrid();

                    $('#navbtnadd').addClass('disabled');
                    $('#navbtnsave').addClass('disabled');

                    $("#navbtnrefresh").on('click', function () {
                        $('.loader').show();
                        $('#txtSearchBar').val('');
                        obj.bindGrid();
                    });

                    $("#txtSearchBar").on("keyup", function () {
                        var self = this;
                        obj.search(self);
                    });

                    $("#navbtndownload").on("click", function () {
                        $("#jqGrid").jqGrid("exportToExcel", {
                            includeLabels: true,
                            includeGroupHeader: true,
                            includeFooter: true,
                            fileName: "Loan Policy Plan.xlsx",
                            maxlength: 40,
                            loadIndicator: true
                        });
                    });

                     $('#navbtncolumns').click(function () {
                        $("#jqGrid").columnChooser({});
                     });

                    obj.initLoanSelect();
                },
                initLoanSelect: function () {
                    var hiddenVal = $('#ContentPlaceHolder1_LoanFullID').val();

                    if (hiddenVal) {
                        $('#txtSearchLoan').val(hiddenVal);
                    }

                    $('.select-loan').show();

                    $('#selectLoanBtn').click(function () {
                        $('.loader').show();
                        var loan = $('#txtSearchLoan').val();
                        $('#ContentPlaceHolder1_LoanFullID').val(loan);

                        obj.bindGrid();
                    });
                },
                getTableInfo: function () {
                    var hiddenVal = $('#ContentPlaceHolder1_LoanFullID').val();
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "LoanPolicyPlan.aspx/GetTableInfo",
                        data: JSON.stringify({ Loan_Full_ID: hiddenVal }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            var data = JSON.parse(res.d);

                            var table_info = data.Table_Info[0];
                            $('#txtTableName').html(table_info.Table_Name);
                            $('#txtLastUpdated').html(table_info.Last_Updated);
                            $('#txtRecordsCount').html(table_info.Records_Count);

                            if (data.CanEdit) {
                                $('#editControl').removeClass('disabled');
                            } else {
                                $('#editControl').addClass('disabled');
                            }

                            $('#enableControl').addClass('disabled');
                            
                        }
                    });
                },
                bindGrid: function () {
                    var hiddenVal = $('#ContentPlaceHolder1_LoanFullID').val();
                    $.ajax({
                        type: "POST",
                        url: "LoanPolicyPlan.aspx/LoadTable",
                        data: JSON.stringify({ Loan_Full_ID: hiddenVal }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var resData = JSON.parse(data.d);
                            $('.loader').show();
                            for (var i = 0; i < resData.length; i++) {
                                resData[i].rowid = i + 1;
                            }
                            obj.loadgrid(resData);
                            obj.getTableInfo();
                            $('.loader').hide();
                        }
                    });
                },
                loadgrid: function (data) {
                    $.jgrid.gridUnload("#jqGrid");
                    $("#jqGrid").jqGrid({
                        datatype: "local",
                        styleUI: 'Bootstrap',
                        data: data,
                        colModel: [
                            { label: 'rowid', name: 'rowid', width: 75, editable: false, key: true, hidden: true,hidedlg: true },
                            { label: 'Loan Policy Plan ID', name: 'Loan_Policy_ID', width: 150, align: 'center', editable: false, hidden: false },
                            { label: 'Loan Full ID', name: 'Loan_Full_ID', width: 120, align: 'center', editable: false, hidden: false },
                            {
                                label: 'State ID',
                                name: 'State_ID',
                                width: 90,
                                align: "center",
                                editable: false
                            },
                            {
                                label: 'County ID',
                                name: 'County_ID',
                                width: 90,
                                align: "center",
                                editable: false
                            },
                            {
                                label: 'Crop Code',
                                name: 'Crop_Code',
                                width: 90,
                                align: "center",
                                editable: false
                            },
                            {
                                label: 'Crop Type',
                                name: 'Crop_Type_Code',
                                width: 120,
                                align: "left",
                                editable: false
                            },
                            {
                                label: 'Irr Prac',
                                name: 'Irr_Practice_Type_Code',
                                width: 120,
                                align: "left",
                                editable: false
                            },
                            {
                                label: 'HR Exclusion Ind',
                                name: 'HR_Exclusion_Ind_Formatted',
                                width: 120,
                                align: "center",
                            },
                            {
                                label: 'Ins Plan',
                                name: 'Ins_Plan',
                                width: 120,
                                align: "center",
                                editable: false
                            },
                            {
                                label: 'Ins Plan Type',
                                name: 'Ins_Plan_Type',
                                width: 120,
                                align: "center",
                                editable: false
                            },
                            {
                                label: 'Eligible Ind',
                                name: 'Eligible_Ind_Formatted',
                                width: 120,
                                align: "center",
                                editable: false
                            },
                            {
                                label: 'Select Ind',
                                name: 'Select_Ind',
                                width: 120,
                                align: "center",
                                editable: false,
                                hidden: true,
                                hidedlg: true
                            },
                            {
                                label: 'Unit Type Code',
                                name: 'Ins_Unit_Type_Code',
                                width: 120,
                                align: "center",
                                editable: false
                            },
                            {
                                label: 'Upper Level',
                                name: 'Upper_Level',
                                width: 120,
                                align: "right",
                                classes: 'right',
                                editable: false
                            },
                            {
                                label: 'Lower Level',
                                name: 'Lower_Level',
                                width: 120,
                                align: "right",
                                classes: 'right',
                                editable: false
                            },
                            {
                                label: 'Area Yield',
                                name: 'Area_Yield',
                                width: 120,
                                align: "right",
                                classes: 'right',
                                editable: false,
                                formatter: function (cellvalue, row, options) {
                                    return obj.formatNumber(cellvalue, 0);
                                }
                            },
                            {
                                label: 'Yield Percent',
                                name: 'Yield_Percent',
                                width: 120,
                                align: "right",
                                classes: 'right',
                                editable: false,
                                formatter: function (cellvalue, row, options) {
                                    return obj.formatNumber(cellvalue, 1);
                                }
                            },
                            {
                                label: 'Price Percent',
                                name: 'Price_Percent',
                                width: 120,
                                align: "right",
                                classes: 'right',
                                editable: false,
                                formatter: function (cellvalue, row, options) {
                                    return obj.formatNumber(cellvalue, 1);
                                }
                            },
                            {
                                label: 'Liability',
                                name: 'Liability_Percent',
                                width: 120,
                                align: "right",
                                classes: 'right',
                                editable: false,
                                formatter: function (cellvalue, row, options) {
                                    return obj.formatNumber(cellvalue, 2);
                                }
                            },
                            {
                                label: 'Deduct Percent',
                                name: 'Deductible_Percent',
                                align: "right",
                                classes: 'right',
                                width: 120,
                                editable: false,
                                hidden: false
                            },
                            {
                                label: 'Deduct Units',
                                name: 'Deductible_Units',
                                align: "right",
                                classes: 'right',
                                width: 120,
                                editable: false,
                                hidden: false
                            },
                            {
                                label: 'Late Deduct Units',
                                name: 'Late_Deductible',
                                align: "right",
                                classes: 'right',
                                width: 120,
                                editable: false,
                                hidden: false
                            },
                            {
                                label: 'FCMC',
                                name: 'Custom1',
                                align: "right",
                                classes: 'right',
                                width: 120,
                                editable: false,
                                hidden: false
                            },
                            {
                                label: 'Prem ium',
                                name: 'Premium',
                                align: "right",
                                classes: 'right',
                                width: 120,
                                editable: false,
                                hidden: false
                            },
                            {
                                label: 'Agent ID',
                                name: 'Agent_ID',
                                align: "center",
                                width: 120,
                                editable: false,
                                hidden: false
                            },
                            {
                                label: 'Agency ID',
                                name: 'Agency_ID',
                                align: "center",
                                width: 120,
                                editable: false,
                                hidden: false
                            },
                            {
                                label: 'AIP ID',
                                name: 'AIP_ID',
                                align: "center",
                                width: 120,
                                editable: false,
                                hidden: false
                            },
                            {
                                label: 'Option',
                                name: 'Option',
                                width: 120,
                                align: "center",
                                editable: false
                            },
                            {
                                label: 'Price',
                                name: 'Price',
                                width: 120,
                                align: "right",
                                classes: 'right',
                                editable: false,
                                formatter: function (cellvalue, row, options) {
                                    return obj.formatCurrency(cellvalue, 4);
                                }
                            }, 
                            {
                                label: 'Liability Amount',
                                name: 'Liability_Amount',
                                width: 120,
                                align: "right",
                                classes: 'right',
                                editable: false
                            },
                            {
                                label: 'Actuarial Hail',
                                name: 'Actuarial_Hail',
                                align: "right",
                                classes: 'right',
                                width: 120,
                                editable: false,
                                hidden: false
                            },
                            {
                                label: 'Actuarial Wind',
                                name: 'Actuarial_Wind',
                                align: "right",
                                classes: 'right',
                                width: 120,
                                editable: false,
                                hidden: false
                            },
                            
                            {
                                label: 'Ins Value',
                                name: 'Ins_Value',
                                align: "right",
                                classes: 'right',
                                width: 120,
                                editable: false,
                                hidden: false
                            },
                            {
                                label: 'Verify Policy Ind',
                                name: 'Policy_Verification_Ind',
                                align: "center",
                                width: 120,
                                editable: false,
                                hidden: false
                            },
                            {
                                label: 'Verify Policy Doc ID',
                                name: 'Policy_Verification_Doc_ID',
                                align: "center",
                                width: 150,
                                editable: false,
                                hidden: false
                            },
                            {
                                label: 'Verify Policy User ID',
                                name: 'Policy_Verification_User_ID',
                                align: "center",
                                width: 150,
                                editable: false,
                                hidden: false
                            },
                            {
                                label: 'Verify Policy Date Time',
                                name: 'Policy_Verification_Date_Time_Formatted',
                                align: "center",
                                width: 160,
                                editable: false,
                                hidden: false
                            },
                            {
                                label: 'Other Description Text',
                                name: 'Other_Description_Text',
                                align: "left",
                                classes: 'left',
                                width: 180,
                                editable: false,
                                hidden: false
                            },
                            {
                                label: 'Status',
                                name: 'Status',
                                align: "center",
                                width: 90,
                                editable: false,
                                hidden: false
                            }
                        ],
                        viewrecords: true,
                        loadonce: true,
                        restoreAfterSelect: false,
                        saveAfterSelect: true,
                        sortable: true,
                        onPaging: obj.changePage,
                        height: 'auto',
                        rowNum: 100,
                        shrinkToFit: false,
                        forceFit:true,
                        pager: "#jqGridPager"
                    });
                },
                toNumber: function toNumber(val) {
                    let value = parseFloat(val);

                    if (isNaN(value)) {
                        value = 0;
                    }
                    return value;
                },
                formatCurrency: function formatCurrency(val, decimals) {
                    var value = obj.toNumber(val);

                    const formatter = new Intl.NumberFormat('en-US', {
                        style: 'currency',
                        currency: 'USD',
                        minimumFractionDigits: decimals || 2
                    });

                    return formatter.format(value);
                },
                formatNumber: function formatNumber(val, decimals) {
                    var value = obj.toNumber(val);
                    return parseFloat(value.toFixed(decimals || 0)).toLocaleString('en-US');
                },
                search: function (self) {
                    if (timer) { clearTimeout(timer); }
                    timer = setTimeout(function () {
                        obj.changePage();
                        $("#jqGrid").jqGrid('filterInput', self.value);
                    }, 0);
                },
                changePage: function () {
                    lastSelection = "";
                    var grid = $("#jqGrid");
                    var id = $('.inline-edit-cell').parent().parent().prop('id');
                    if (id != undefined) {
                        grid.jqGrid('saveRow', id);
                        grid.jqGrid('restoreRow', id);
                        var row = grid.jqGrid('getRowData', id);
                        if (!jQuery.isEmptyObject(row)) {
                            var dataobj = jQuery("#jqGrid").jqGrid('getGridParam', 'data');
                            index = dataobj.findIndex(x => x.rowid == row.rowid);
                            if (row.rowid > 0 && row.Actionstatus != 1) {
                                row.Actionstatus = 2;
                                grid.jqGrid('getGridParam', 'data')[index] = row;
                            }
                        }
                    }
                }
            }
            obj.Init();
        });
    </script>
</asp:Content>
