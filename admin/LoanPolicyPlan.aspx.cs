﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_LoanPolicyPlan : BasePage
{
    public admin_LoanPolicyPlan()
    {
        Table_Name = "Loan_Policy";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        base.Page_Load(sender, e);
        LoanFullID.Value = Loan_Full_ID;
    }

    [WebMethod]
    public static string GetTableInfo(string Loan_Full_ID)
    {
        Count_Query = @"Declare @Count int;
                        select @Count = Count(*) from Loan_Policy";

        if (!string.IsNullOrEmpty(Loan_Full_ID))
        {
            Count_Query += " Where Loan_Full_ID like '%" + Loan_Full_ID + "%'";
        }

        return Get_Table_Info();
    }

    [WebMethod]
    public static string LoadTable(string Loan_Full_ID)
    {
        HttpContext.Current.Session["Loan_Full_ID"] = Loan_Full_ID;

        string sql_qry = @"select * , 
                                cast(HR_Exclusion_Ind as int) HR_Exclusion_Ind_Formatted, 
                                cast(Eligible_Ind as int) Eligible_Ind_Formatted,
                                format(Policy_Verification_Date_Time, 'MM/dd/yyyy hh:mm:ss tt') as [Policy_Verification_Date_Time_Formatted]
                            from Loan_Policy";

        if (!string.IsNullOrEmpty(Loan_Full_ID))
        {
            sql_qry += " Where Loan_Full_ID like '%" + Loan_Full_ID + "%'";
        }

        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return JsonConvert.SerializeObject(dt);
    }
}