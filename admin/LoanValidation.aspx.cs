﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_Loan_LoanValidation : System.Web.UI.Page
{
    static string dbKey = "gp_conn";
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    [WebMethod]
    public static string GetLoanValidation()
    {//SELECT Loan_Validation_ID,Loan_Full_ID,Validation_ID,Validation_ID_Level,Tab_ID,Validation_Action_Ind,Status FROM Loan_Validation
        string sql_qry = "SELECT 0 as Actionstatus,Loan_Validation_ID,Loan_Full_ID,Validation_ID,Validation_ID_Level,Tab_ID,Validation_Action_Ind,Status FROM Loan_Validation";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
    }
    [WebMethod]
    public static void SaveLoanValidation(dynamic lst)
    {
        foreach (var item in lst)
        {
            string qry = "";
            int val = Convert.ToInt32(item["Actionstatus"]);
            if (val == 1)
            {
                qry = "INSERT INTO Loan_Validation(Loan_Full_ID,Validation_ID,Validation_ID_Level,Tab_ID,Validation_Action_Ind,Status)" +
                    " VALUES('" + item["Loan_Full_ID"].Replace("'", "''") + "'," +
                    "'" + item["Validation_ID"].Replace("'", "''") + "','" + item["Validation_ID_Level"].Replace("'", "''") + "'," +
                    "'" + item["Tab_ID"].Replace("'", "''") + "','" + item["Validation_Action_Ind"].Replace("'", "''") + "'," +
                    "'" + item["Status"].Replace("'", "''") + "')";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 2)
            {
                qry = "UPDATE Loan_Validation SET Loan_Full_ID = '" + item["Loan_Full_ID"].Replace("'", "''") + "' ," +
                    "Validation_ID = '" + item["Validation_ID"].Replace("'", "''") + "',Validation_ID_Level = '" + item["Validation_ID_Level"].Replace("'", "''") + "'," +
                    "Tab_ID = '" + item["Tab_ID"].Replace("'", "''") + "',Validation_Action_Ind = '" + item["Validation_Action_Ind"].Replace("'", "''") + "'," +
                    "Status = '" + item["Status"].Replace("'", "''") + "'  where Loan_Validation_ID=" + item["Loan_Validation_ID"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 3)
            {
                qry = "DELETE FROM Loan_Validation  where Loan_Validation_ID=" + item["Loan_Validation_ID"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }

        }
    }


}