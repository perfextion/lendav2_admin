﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;

public partial class admin_LoanValidationNew : BasePage
{
    private static Dictionary<string, string> Tabs = new Dictionary<string, string>();

    public admin_LoanValidationNew()
    {
        Table_Name = "Loan_Validation";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        base.Page_Load(sender, e);
        LoanFullID.Value = Loan_Full_ID;

        try
        {
            string sql = "select tab_id, LOWER(tab_name) as [tab_name] from Ref_Tab";
            DataTable dt = gen_db_utils.gp_sql_get_datatable(sql, dbKey);

            Tabs = dt.AsEnumerable().ToDictionary(row => row[1].ToString(), row => row[0].ToString());
        }
        catch
        {
            Tabs = new Dictionary<string, string>();
        }
    }

    [WebMethod]
    public static string GetTableInfo(string Loan_Full_ID)
    {
        Count_Query = @"Declare @Count int;
                        select @Count = Count(loan_settings) from loan_master where Active_Ind = 1";

        if (!string.IsNullOrEmpty(Loan_Full_ID))
        {
            Count_Query += " and Loan_Full_ID like '%" + Loan_Full_ID + "%'";
        }

        return Get_Table_Info();
    }

    [WebMethod]
    public static string LoadTable(string Loan_Full_ID)
    {
        HttpContext.Current.Session["Loan_Full_ID"] = Loan_Full_ID;

        string sql_qry = @"select loan_settings, loan_full_id from loan_master where Active_Ind = 1";

        if (!string.IsNullOrEmpty(Loan_Full_ID))
        {
            sql_qry += " and Loan_Full_ID like '%" + Loan_Full_ID + "%'";
        }

        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);

        var loan_validations = new List<errormodel>();

        foreach (var item in dt.AsEnumerable())
        {
            var settings = item["loan_settings"].ToString();
            var loan_full_id = item["loan_full_id"].ToString();
            if (!string.IsNullOrEmpty(settings))
            {
                var json_settings = JsonConvert.DeserializeObject<Loansettings>(settings);
                var validations = json_settings.Validations;

                if (validations != null)
                {
                    foreach (var v in validations)
                    {
                        string tab_id = string.Empty;

                        Tabs.TryGetValue(v.tab, out tab_id);

                        v.loan_full_id = loan_full_id;
                        v.Status = "1";
                        v.tab_id = tab_id;
                        loan_validations.Add(v);
                    }
                }
            }
        }

        var res =  JsonConvert.SerializeObject(loan_validations);
        return res;
    }
}