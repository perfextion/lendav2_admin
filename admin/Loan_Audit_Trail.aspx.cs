﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;

public partial class admin_Loan_Audit_Trail : BasePage
{
    public admin_Loan_Audit_Trail()
    {
        Table_Name = "Loan_Audit_Trail";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        base.Page_Load(sender, e);
        LoanFullID.Value = Loan_Full_ID;
    }

    [WebMethod]
    public static string GetTableInfo(string Loan_Full_ID)
    {
        Count_Query = "DECLARE @Count int; Select @Count = Count(*) From Loan_Audit_Trail";

        if(!string.IsNullOrEmpty(Loan_Full_ID))
        {
            Count_Query += " Where Loan_Full_ID LIKE '%" + Loan_Full_ID + "%'";
        }
        return Get_Table_Info();
    }

    [WebMethod]
    public static Dictionary<string, string> GetLoanFullIds()
    {
        string sql_qry = "select Distinct Loan_Full_ID from Loan_Audit_Trail Where Loan_Full_ID is not null and Loan_Full_ID !=  '' order by Loan_Full_ID";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return dt.AsEnumerable().ToDictionary(row => row[0].ToString(), row => row[0].ToString());
    }

    [WebMethod]
    public static string LoadTable(string Loan_Full_ID)
    {
        HttpContext.Current.Session["Loan_Full_ID"] = Loan_Full_ID;

        string sql_qry = @"select TOP 1000 FORMAT(Date_Time, 'MM/dd/yyyy hh:mm:ss tt') as [Date_Time_Formatted],U.Username, LA.* from Loan_Audit_Trail LA 
                            LEFT JOIN [Users] U on U.[UserId] = LA.[User_ID]
                            Where LA.Loan_Full_ID is not null and LA.Loan_Full_ID !=  ''";

        if (!string.IsNullOrEmpty(Loan_Full_ID))
        {
            sql_qry += " AND LA.Loan_Full_ID LIKE '%" + Loan_Full_ID + "%'";
        }

        sql_qry += "Order by LA.Date_Time DESC";

        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return JsonConvert.SerializeObject(dt);
    }
}
