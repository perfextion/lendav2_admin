﻿<%@ Page Title="" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="Loan_Detail_New.aspx.cs" Inherits="admin_Loan_Detail_New" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style>
        .ui-jqgrid .ui-jqgrid-htable th div {
            height: auto;
            overflow: hidden;
            padding-right: 4px;
            padding-top: 2px;
            position: relative;
            vertical-align: text-top;
            white-space: normal !important;
        }

        .ui-jqgrid tr.jqgrow td {
            word-wrap: break-word; /* IE 5.5+ and CSS3 */
            white-space: pre-wrap; /* CSS3 */
            white-space: normal !important;
            height: auto;
            vertical-align: text-top;
            padding-top: 2px;
            padding-bottom: 3px;
        }

         .ui-jqgrid .ui-jqgrid-htable th {
            height: 45px;
        }

        .cvteste {
            padding-right: 14px !important;
        }

        #content {
            padding: 14px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="loan-transaction-table">
        <div class="row switch-controls">
            <div class="switch-control disabled" id="editControl">
                <label for="editBtn">Edit</label>
                <div class="toggle-btn small">            
                    <input type="checkbox" id="editBtn" class="cb-value" />
                    <span class="round-btn"></span>
                </div>
            </div>
            <div class="switch-control disabled" id="enableControl">
                <label for="enableBtn">Enable</label>
                <div class="toggle-btn small">            
                    <input type="checkbox" id="enableBtn" class="cb-value" />
                    <span class="round-btn"></span>
                </div>
            </div>
        </div>
        <div class="loader"></div>
        <div class="row main-content">
            <div>
                <table id="jqGrid"></table>
                <div id="jqGridPager"></div>
            </div>
        </div>
        <asp:HiddenField ID="LoanFullID" runat="server" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" Runat="Server">
    <script type="text/javascript">  
        var DeleteRows = [];
        var timer;
        var lastSelection;

        $(function () {
            var obj = {
                Init: function () {
                    $('.loader').show();
                    obj.bindGrid();

                    $('#navbtnadd').addClass('disabled');
                    $('#navbtnsave').addClass('disabled');

                    $("#navbtnrefresh").on('click', function () {
                        $('.loader').show();
                        $('#txtSearchBar').val('');
                        obj.bindGrid();
                    });

                    $("#txtSearchBar").on("keyup", function () {
                        var self = this;
                        obj.search(self);
                    });

                    $("#navbtndownload").on("click", function () {
                        $("#jqGrid").jqGrid("exportToExcel", {
                            includeLabels: true,
                            includeGroupHeader: true,
                            includeFooter: true,
                            fileName: "Loan Detail.xlsx",
                            maxlength: 40
                        });
                    });

                     $('#navbtncolumns').click(function () {
                        $("#jqGrid").columnChooser({});
                     });

                    obj.initLoanSelect();
                },
                initLoanSelect: function () {
                    var hiddenVal = $('#ContentPlaceHolder1_LoanFullID').val();

                    if (hiddenVal) {
                        $('#txtSearchLoan').val(hiddenVal);
                    }

                    $('.select-loan').show();

                    $('#selectLoanBtn').click(function () {
                        $('.loader').show();
                        var loan = $('#txtSearchLoan').val();
                        $('#ContentPlaceHolder1_LoanFullID').val(loan);

                        obj.bindGrid();
                    });
                },
                getTableInfo: function () {
                    var loan_full_id = $('#ContentPlaceHolder1_LoanFullID').val();
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "Loan_Detail_New.aspx/GetTableInfo",
                        data: JSON.stringify({ Loan_Full_ID: loan_full_id }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            var data = JSON.parse(res.d);

                            var table_info = data.Table_Info[0];
                            $('#txtTableName').html(table_info.Table_Name);
                            $('#txtLastUpdated').html(table_info.Last_Updated);
                            $('#txtRecordsCount').html(table_info.Records_Count);

                            if (data.CanEdit) {
                                $('#editControl').removeClass('disabled');
                            } else {
                                $('#editControl').addClass('disabled');
                            }

                            $('#enableControl').addClass('disabled');
                            
                        }
                    });
                },
                bindGrid: function () {
                    var loan_full_id = $('#ContentPlaceHolder1_LoanFullID').val();
                    $.ajax({
                        type: "POST",
                        url: "Loan_Detail_New.aspx/LoadTable",
                        data: JSON.stringify({ Loan_Full_ID: loan_full_id }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var resData = JSON.parse(data.d);
                            $('.loader').show();
                            for (var i = 0; i < resData.length; i++) {
                                resData[i].rowid = i + 1;
                            }
                            obj.loadgrid(resData);
                            obj.getTableInfo();
                            $('.loader').hide();
                        }
                    });
                },
                loadgrid: function (data) {
                    $.jgrid.gridUnload("#jqGrid");
                    $("#jqGrid").jqGrid({
                        datatype: "local",
                        styleUI: 'Bootstrap',
                        data: data,
                        colModel: [
                            { label: 'rowid', name: 'rowid', width: 75, editable: false, key: true, hidden: true,hidedlg: true },
                            { label: 'Loan Detail ID', name: 'Loan_CU_ID', width: 120, editable: false, align: 'center', hidden: false },
                            { label: 'Loan Full ID', name: 'Loan_Full_ID', width: 120, align: 'center', editable: false, hidden: false },
                            {
                                label: 'State Abbrev',
                                name: 'Farm_State_ID',
                                width: 120,
                                align: "center",
                                editable: false
                            },
                            {
                                label: 'County ID',
                                name: 'Farm_County_ID',
                                width: 120,
                                align: "center",
                                editable: false
                            },
                            {
                                label: 'Prod Share Pct',
                                name: 'Percent_Prod',
                                width: 120,
                                align: "right",
                                classes: 'right',
                                editable: false,
                                formatter: 'percentFormatter'
                            },
                            {
                                label: 'Landowner',
                                name: 'Landowner',
                                width: 180,
                                align: "left",
                                classes: 'left',
                                editable: false
                            },
                            {
                                label: 'FSN',
                                name: 'FSN',
                                width: 120,
                                align: "left",
                                editable: false
                            },
                            {
                                label: 'Section',
                                name: 'Section',
                                width: 120,
                                align: "left",
                                editable: false
                            },
                            {
                                label: 'Rated',
                                name: 'Rated',
                                width: 120,
                                align: "left",
                                editable: false
                            },
                            {
                                label: 'Col Cat Code',
                                name: 'Col_Cat_Code',
                                width: 120,
                                align: "center"
                            },
                            {
                                label: 'Crop Practice ID',
                                name: 'Crop_Practice_ID',
                                width: 120,
                                align: "center"
                            },
                            {
                                label: 'Crop Code',
                                name: 'Crop_Code',
                                width: 120,
                                align: "center"
                            },
                            {
                                label: 'Crop Type',
                                name: 'Crop_Type_Code',
                                width: 120,
                                align: "center"
                            },
                            {
                                label: 'Irr Prac',
                                name: 'Crop_Practice_Type_Code',
                                width: 120,
                                align: "center"
                            },
                            {
                                label: 'Crop Prac',
                                name: 'Crop_Practice_Type_Code',
                                width: 120,
                                align: "center"
                            },
                            {
                                label: 'Loan Farm ID',
                                name: 'Farm_ID',
                                width: 120,
                                align: "center",
                                editable: false
                            },
                            {
                                label: 'CU Acres',
                                name: 'CU_Acres',
                                width: 90,
                                align: "center",
                                editable: false,
                            },
                            {
                                label: 'Ins Value',
                                name: 'Ins_Value',
                                width: 120,
                                align: "right",
                                classes: 'right',
                                editable: false,
                                formatter: 'amountFormatter'
                            },
                            {
                                label: 'Disc Ins value',
                                name: 'Disc_Ins_value',
                                width: 120,
                                align: "right",
                                classes: 'right',
                                editable: false,
                                formatter: 'amountFormatter'
                            },
                            {
                                label: 'Mkt Value',
                                name: 'Mkt_Value',
                                width: 120,
                                align: "right",
                                classes: 'right',
                                editable: false,
                                formatter: 'amountFormatter'
                            },
                            {
                                label: 'Disc Mkt Value',
                                name: 'Disc_Mkt_Value',
                                width: 120,
                                align: "right",
                                classes: 'right',
                                editable: false,
                                formatter: 'amountFormatter'
                            },
                            {
                                label: 'CEI Value',
                                name: 'CEI_Value',
                                width: 120,
                                align: "right",
                                classes: 'right',
                                editable: false,
                                formatter: 'amountFormatter'
                            },
                            {
                                label: 'Disc CEI Value',
                                name: 'Disc_CEI_Value',
                                width: 120,
                                align: "right",
                                classes: 'right',
                                editable: false,
                                formatter: 'amountFormatter'
                            },
                            {
                                label: 'Price',
                                name: 'Z_Price',
                                width: 120,
                                align: "right",
                                classes: 'right',
                                editable: false,
                                formatter: 'priceFormatter'
                            },
                            {
                                label: 'Basis Adj',
                                name: 'Z_Basis_Adj',
                                width: 120,
                                align: "right",
                                classes: 'right',
                                editable: false,
                                formatter: 'priceFormatter'
                            },
                            {
                                label: 'Marketing Adj',
                                name: 'Z_Marketing_Adj',
                                width: 120,
                                align: "right",
                                classes: 'right',
                                editable: false,
                                formatter: 'priceFormatter'
                            },
                            {
                                label: 'Rebate Adj',
                                name: 'Z_Rebate_Adj',
                                width: 120,
                                align: "right",
                                classes: 'right',
                                editable: false,
                                formatter: 'priceFormatter'
                            },
                            {
                                label: 'Adj Price',
                                name: 'Z_Adj_Price',
                                width: 120,
                                align: "right",
                                classes: 'right',
                                editable: false,
                                formatter: 'priceFormatter'
                            },
                            {
                                label: 'Rate Yield',
                                name: 'Rate_Yield',
                                width: 120,
                                align: "right",
                                classes: 'right',
                                editable: false,
                                formatter: 'numberFormatter'
                            },
                            {
                                label: 'CU APH',
                                name: 'CU_APH',
                                width: 120,
                                align: "right",
                                classes: 'right',
                                editable: false,
                                formatter: 'numberFormatter'
                            },
                            {
                                label: 'Ins APH',
                                name: 'Ins_APH',
                                width: 120,
                                align: "right",
                                classes: 'right',
                                editable: false,
                                formatter: 'numberFormatter'
                            },
                            {
                                label: 'Verf Ins APH Status',
                                name: 'Verf_Ins_APH_Status',
                                width: 120,
                                align: "center",
                                editable: false
                            },
                            {
                                label: 'Verf AIP Acres Status',
                                name: 'Verf_AIP_Acres_Status',
                                width: 120,
                                align: "center",
                                editable: false
                            },
                            {
                                label: 'Verf FSA Acres Status',
                                name: 'Verf_FSA_Acres_Status',
                                width: 120,
                                align: "center",
                                editable: false
                            },
                            {
                                label: 'Booking Ind',
                                name: 'Booking_Ind',
                                width: 120,
                                align: "center",
                                editable: false
                            },
                            {
                                label: 'Added Land',
                                name: 'Added_Land',
                                width: 120,
                                align: "center",
                                editable: false
                            },
                            {
                                label: 'Status',
                                name: 'Status_Formatted',
                                align: "center",
                                width: 90,
                                editable: false
                            }
                        ],
                        viewrecords: true,
                        loadonce: true,
                        restoreAfterSelect: false,
                        saveAfterSelect: true,
                        sortable: true,
                        onPaging: obj.changePage,
                        height: 'auto',
                        rowNum: 100,
                        pager: "#jqGridPager"
                    });
                },
                search: function (self) {
                    if (timer) { clearTimeout(timer); }
                    timer = setTimeout(function () {
                        obj.changePage();
                        $("#jqGrid").jqGrid('filterInput', self.value);
                    }, 0);
                },
                changePage: function () {
                    lastSelection = "";
                    var grid = $("#jqGrid");
                    var id = $('.inline-edit-cell').parent().parent().prop('id');
                    if (id != undefined) {
                        grid.jqGrid('saveRow', id);
                        grid.jqGrid('restoreRow', id);
                        var row = grid.jqGrid('getRowData', id);
                        if (!jQuery.isEmptyObject(row)) {
                            var dataobj = jQuery("#jqGrid").jqGrid('getGridParam', 'data');
                            index = dataobj.findIndex(x => x.rowid == row.rowid);
                            if (row.rowid > 0 && row.Actionstatus != 1) {
                                row.Actionstatus = 2;
                                grid.jqGrid('getGridParam', 'data')[index] = row;
                            }
                        }
                    }
                }
            }
            obj.Init();
        });
    </script>
</asp:Content>
