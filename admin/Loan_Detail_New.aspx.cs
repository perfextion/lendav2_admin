﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_Loan_Detail_New : BasePage
{
    public admin_Loan_Detail_New()
    {
        Table_Name = "Loan_Crop_Unit";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        base.Page_Load(sender, e);
        LoanFullID.Value = Loan_Full_ID;
    }

    [WebMethod]
    public static string GetTableInfo(string Loan_Full_ID)
    {
        Count_Query = "Declare @count int; Select @Count = Count(*) From Loan_Crop_Unit";

        if (!string.IsNullOrEmpty(Loan_Full_ID))
        {
            Count_Query += " Where Loan_Full_ID like '%" + Loan_Full_ID + "%'";
        }

        return Get_Table_Info();
    }

    [WebMethod]
    public static string LoadTable(string Loan_Full_ID)
    {
        HttpContext.Current.Session["Loan_Full_ID"] = Loan_Full_ID;

        string sql_qry = @"select LCU.*, LF.Farm_State_ID, LF.Farm_County_ID, LF.Percent_Prod, LF.Landowner, LF.FSN, LF.Section, LF.Rated, 1 as Status_Formatted
                            from Loan_Crop_Unit LCU
                            JOIN Loan_Farm LF on LCU.Farm_ID  = LF.Farm_ID";

        if (!string.IsNullOrEmpty(Loan_Full_ID))
        {
            sql_qry += " Where LCU.Loan_Full_ID like '%" + Loan_Full_ID + "%'";
        }

        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return JsonConvert.SerializeObject(dt);
    }
}