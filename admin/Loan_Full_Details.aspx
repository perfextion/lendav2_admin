﻿<%@ Page Title="" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="Loan_Full_Details.aspx.cs" Inherits="Loan_Full_Details" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="https://rawgit.com/abodelot/jquery.json-viewer/master/json-viewer/jquery.json-viewer.js"></script>
    <link href="https://rawgit.com/abodelot/jquery.json-viewer/master/json-viewer/jquery.json-viewer.css" rel="stylesheet" />
     <style>
        .panel-body {
            max-height: 400px;
            overflow: auto;
        }

        .panel-body.exception {
            max-height: 700px;
            overflow: auto;
        }

         .panel-heading {
             cursor: pointer;
         }

        .panel-body table thead th {
            background: #ddd;
            position: sticky;
            top: -15px;
            z-index: 10;
        }

         .more-less {
             padding-right: 10px;
         }

         #content {
             padding: 14px !important;
         }

         .data-details > span {
             display: flex; 
         }

         .data-details > span > span:first-of-type {
             width: 50%;
             min-width: 232px;
         }

         .data-details > span > span:last-of-type {
             color: #00338d;
         }

         pre {
             white-space: pre-wrap; /* CSS3 */
             white-space: -moz-pre-wrap; /* Mozilla, post millennium */
             white-space: -pre-wrap; /* Opera 4-6 */
             white-space: -o-pre-wrap; /* Opera 7 */
             word-wrap: break-word; /* Internet Explorer 5.5+ */
         }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="loader"></div>
    <div class="row">
        <section id="widget-grid" class="">
            <div class="row">
                <article class="col-lg-12">
                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget" id="wid-id-0">
                        <!-- widget div-->
                        <!-- widget edit box -->
                        <div class="jarviswidget-editbox">
                            <!-- This area used as dropdown edit box -->
                        </div>
                        <!-- end widget edit box -->
                        <!-- widget content -->
                        <div class="widget-body">
                            <table style="width: 1000px">
                                <tr>
                                    <td style="width: 200px"></td>
                                </tr>
                            </table>
                            <div id="movieForm" class="form-horizontal">
                                <fieldset>
                                    <div class="col-md-12">
                                        <div class="col-md-8">
                                        </div>
                                        <div class="col-md-4">
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <asp:Label ID="lbl_table" runat="server" Text="Loan Summary Table"></asp:Label>
                            <asp:HiddenField ID="jsondata" runat="server" />
                        </div>
                    </div>
                </article>
            </div>
        </section>
        <asp:HiddenField ID="LoanFullID" runat="server" />
    </div>
    <script>         

        $(function () {
            $('.loader').show();
            bindGrid(); 
            initSelectLoan();
        });
        active = true;

        function bindGrid() {
            var loan_full_id = $('#ContentPlaceHolder1_LoanFullID').val();

            if (!loan_full_id) {
                alert('Please select a loan');
                $('.loader').hide();
            }
            else {
                $.ajax({
                    type: "POST",
                    url: "Loan_Full_Details.aspx/LoadTable",
                    data: JSON.stringify({ LoanFullID: loan_full_id }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (res) {
                        $('#ContentPlaceHolder1_lbl_table').html(res.d);
                        initJSON();
                        $('.loader').hide();
                    },
                    error: function (res) {
                    }
                });
            }            
        }

        function initJSON() {
            $('.setting-json-data').each(function () {
                try {
                    let data = $(this).html();
                    $('.preloanSetting').jsonViewer(JSON.parse(data));
                    $('.json-toggle').click();
                } catch (ex) {
                    
                }
            });
        }

        function initSelectLoan() {
            $('.select-loan').show();

            var hiddenVal = $('#ContentPlaceHolder1_LoanFullID').val();

            console.log(hiddenVal);

            if (hiddenVal) {
                $('#txtSearchLoan').val(hiddenVal);
            }

            $('.select-loan').show();

            $('#selectLoanBtn').click(function () {
                $('.loader').show();
                var loan = $('#txtSearchLoan').val();
                $('#ContentPlaceHolder1_LoanFullID').val(loan);

                bindGrid();
            });
        }

        $('#collapse-init').click(function () {
            if (active) {
                active = false;
                $('.panel-collapse.collapse').addClass('in');
                $('.panel-heading.accordion-toggle.collapsed').removeClass('collapsed');
                $(".panel-collapse.collapse.in").removeAttr("style");
                $(this).children('i').toggleClass('glyphicon-plus glyphicon-minus');
            } else {
                active = true;
                $('.panel-collapse.collapse.in').removeClass('in');
                $('.panel-heading.accordion-toggle').addClass('collapsed');
                $(this).children('i').toggleClass('glyphicon-plus glyphicon-minus');
            }
        });

        $('.panel-heading.accordion-toggle.collapsed').click(function () {
            if ($('.panel-heading.accordion-toggle.collapsed').length == 0) {
                active = true;
                $(this).toggleClass('glyphicon-plus glyphicon-minus');
            }
        });        

        function focus(id) {
            setTimeout(function () {
                $(id).removeClass('in');
                $('.panel-heading.accordion-toggle').addClass('collapsed');
            }, 500);
        }

        function Approve_Support(Role, Exception_ID, Region_ID) {
            $.ajax({
                type: "POST",
                url: "Loan_Full_Details.aspx/Approve_Support",
                data: JSON.stringify({ Role_Type_Code: Role, Loan_Exception_ID: Exception_ID, Region_Id: Region_ID}),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (res) {
                    location.reload(true);
                },
                error: function (res) {
                }
            });
        }
    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" runat="Server">
    <script>
        $('#navbtnadd').addClass('disabled');
        $('#navbtnsave').addClass('disabled');
        $('#navbtndownload').addClass('disabled');
        $('#navbtnrefresh').addClass('disabled');

        function toggleIcon(e) {
            $(e.target).prev('.panel-heading')
                .find(".more-less")
                .toggleClass('glyphicon-plus glyphicon-minus');
        }
        $('.panel-group').on('hidden.bs.collapse', toggleIcon);
        $('.panel-group').on('shown.bs.collapse', toggleIcon);
    </script>
</asp:Content>


