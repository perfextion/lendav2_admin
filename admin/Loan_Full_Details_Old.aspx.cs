﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_Loan_Full_Details_Old : System.Web.UI.Page
{
    private static string dbKey = "gp_conn";
    private string loan_full_id = "0";
    private string Loan_ID = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        loan_full_id = Request.QueryString["loan_full_id"];

        if (string.IsNullOrEmpty(loan_full_id))
        {
            string sql_query = "Select TOP 1 Loan_Full_Id From Loan_Master";
            var dt = gen_db_utils.gp_sql_get_datatable(sql_query, dbKey);
            var row = dt.Rows[0];
            loan_full_id = row["Loan_Full_Id"].ToString();
        }

        lbl_Loan_Full_ID.Text = loan_full_id;
        lbl_table.Text = "";
        lbl_table.Text += "<div class='panel-group' id='accordion'>";
        try { lbl_table.Text += Get_Loan_Master(loan_full_id); } catch (Exception) { }
        try { lbl_table.Text += Get_Affliliated_loans(loan_full_id); } catch (Exception) { }
        try { lbl_table.Text += Get_Loan_Associations(loan_full_id); } catch (Exception) { }
        try { lbl_table.Text += Get_Audit_Trails(loan_full_id); } catch (Exception) { }
        try { lbl_table.Text += Get_Borrower_Income_History(loan_full_id); } catch (Exception) { }
        try { lbl_table.Text += Get_Loan_Budget(loan_full_id); } catch (Exception) { }
        try { lbl_table.Text += Get_Loan_Collateral_Detail(loan_full_id); } catch (Exception) { }
        try { lbl_table.Text += Get_Loan_Comment(loan_full_id); } catch (Exception) { }
        try { lbl_table.Text += Get_Loan_Committee(loan_full_id); } catch (Exception) { }
        try { lbl_table.Text += Get_Loan_Marketing_Contract(loan_full_id); } catch (Exception) { }
        try { lbl_table.Text += Get_Loan_Crop_Practice(loan_full_id); } catch (Exception) { }
        try { lbl_table.Text += Get_Loan_Crop(loan_full_id); } catch (Exception) { }
        try { lbl_table.Text += Get_CoBorrower(loan_full_id); } catch (Exception) { }
        try { lbl_table.Text += Get_Cross_Collateral(loan_full_id); } catch (Exception) { }
        try { lbl_table.Text += Get_Loan_Crop_Unit(loan_full_id); } catch (Exception) { }
        try { lbl_table.Text += Get_Loan_Disburse(Loan_ID); } catch (Exception) { }
        try { lbl_table.Text += Get_Loan_Documents(loan_full_id); } catch (Exception) { }
        try { lbl_table.Text += Get_Loan_Exception(loan_full_id); } catch (Exception) { }
        try { lbl_table.Text += Get_Loan_Farm(loan_full_id); } catch (Exception) { }
        try { lbl_table.Text += Get_Loan_Other_Income(loan_full_id); } catch (Exception) { }
        try { lbl_table.Text += Get_Loan_Policies(loan_full_id); } catch (Exception) { }
        try { lbl_table.Text += Get_Loan_Wfrp(loan_full_id); } catch (Exception) { }
        try { lbl_table.Text += Get_Loan_Q_Response(loan_full_id); } catch (Exception) { }
        lbl_table.Text += "</div>";
    }

    public string Getcolor(string headname, int value)
    {
        string count = "";
        if (value != 0)
        {
            count = "<h4 class='panel-title' style='color:blue'> <i class='more-less glyphicon glyphicon-plus'></i>" + headname + "&nbsp;(" + value + ")</h4>";
        }
        else
        {
            count = "<h4 class='panel-title'> <i class='more-less glyphicon glyphicon-plus'></i>" + headname + " &nbsp;(" + value + ")</h4>";
        }
        return count;
    }

    public string Get_Loan_Master(string loan_full_id)
    {
        string stQry = "";
        string str_return = "";
        stQry = @"SELECT top 1 [Loan_Master_ID],[Loan_ID],[Loan_Seq_num],[Loan_Full_ID],[Crop_Year],[Region_ID],[Office_ID],
                [Loan_Officer_ID],[Farmer_ID],[Borrower_ID],[Loan_Type_Code],[Application_Date],[Borrower_ID_Type],[Borrower_SSN_Hash],
                [Borrower_Entity_Type_Code],[Borrower_Last_Name],[Borrower_First_Name],[Borrower_MI],[Borrower_Address],[Borrower_City],
                [Borrower_State_Abbrev],[Borrower_State],[Borrower_Zip],[Borrower_Phone],[Borrower_email],[Borrower_DL_state],[Borrower_Dl_Num],[Borrower_DOB],
                [Borrower_Preferred_Contact_Ind],[Borrower_County_ID],[Borrower_Lat],[Borrower_Long],[Spouse_SSN_Hash],[Spouse_Last_name],
                [Spouse_First_Name], [Spouse_MI],[Spouse_Address],[Spouse_City],[Spouse_State],[Spouse_Zip],[Spouse_Phone],[Spouse_Email],
                [Spouse_Preferred_Contact_Ind],[Farmer_ID_Type],[Farmer_SSN_Hash],[Farmer_Entity_Type],[Farmer_Last_Name],[Farmer_First_Name],
                [Farmer_MI],[Farmer_Address],[Farmer_City],[Farmer_State],[Farmer_Zip],[Farmer_Phone],[Farmer_Email],[Farmer_DL_State],
                [Farmer_DL_Num],[Farmer_DOB],[Farmer_Preferred_Contact_Ind],[Year_Begin_Farming],[Year_Begin_Client],[Referral_Type_Code] ,
                [Primary_AIP_ID],[Primary_AIP_Name],[Distributor_ID],[Distributor_Name],[Primary_Agency_ID],[Primary_Agency_Name],
                [Current_Bankruptcy_Status],[Original_Bankruptcy_Status],[Previous_Bankruptcy_Status],[Judgement_Ind],[Local_Ind],[Credit_Score]
                ,[Credit_Score_Date],[CPA_Prepared_Financials],[Financials_Date],[Current_Assets],[Inter_Assets],[Fixed_Assets],
                [Total_Assets],[Current_Assets_Disc_Percent],[Inter_Assets_Disc_Percent],[Fixed_Assets_Disc_Percent],[Current_Liabilities],
                [Inter_Liabilities],[Fixed_Liabilities],[Total_Liabilities],[Current_Disc_Net_Worth],[Inter_Disc_Net_Worth],
                [Fixed_Disc_Net_Worth],[Total_Disc_Net_Worth],[Borrower_Farm_Financial_Rating],[Borrower_Rating],[Requested_Credit]
                ,[Maturity_Date],[Estimated_Days],[Origination_Fee_Percent],[Service_Fee_Percent],[Extension_Fee_Percent],[Interest_Percent],
                [Origination_Fee_Amount],[Service_Fee_Amount],[Extension_Fee_Amount],[Interest_Est_Amount],[Total_Crop_Acres],[Total_Revenue],[ARM_Commitment],[Dist_Commitment],
                [Third_Party_Credit],[Total_Commitment],[Cash_Flow_Amount],[Break_Even_Percent],[Risk_Cushion_Amount],
                [Risk_Cushion_Percent],[Return_Percent],[Ag_Pro_Requested_Credit],[Delta_Crop_Acres],[Delta_Total_Revenue]
                ,[Delta_ARM_Commitment],[Delta_Dist_Commitment],[Delta_Third_Party_Credit],[Delta_Total_Commitment],
                [Delta_Origination_Fee_Amount],[Delta_Service_Fee_Amount],[Delta_Cash_Flow_Amount],[Delta_Risk_Cushion_Amount],
                [Delta_Risk_Cushion_Percent],[Delta_Return_Percent],[Net_Market_Value_Crops],[Net_Market_Value_Insurance]
                ,[Net_Market_Value_FSA],[Net_Market_Value_Livestock],[Net_Market_Value_Stored_Crops],[Net_Market_Value_Equipment]
                ,[Net_Market_Value_Real_Estate],[Net_Market_Value_Other],[Disc_value_Crops],[Disc_value_Insurance],[Disc_value_FSA],
                [Disc_value_Livestock],[Disc_value_Stored_Crops],[Disc_value_Equipment],[Disc_value_Real_Estate],[Disc_value_Other],
                [Disc_value_Total],[Prev_Addendum_Percent],[Current_Addendum_Percent],[Original_Application_Date] ,[Decision_Date],
                [Close_Date],[Cross_Col_ID],[Controlled_Disbursement_Ind],[Watch_List_ind],
                [Loan_Pending_Action_Type_Code],[Loan_Pending_Action_Level],[Loan_Status],[Prev_Loan_Status],
                [Nortridge_API_In_ID],[Third_Party_Total_Budget],[Dist_Total_Budget],[Arm_Total_Budget],[Loan_Total_Budget],
                [Arm_Extension_Fee],[Dist_Rate_Fee],[Arm_Rate_Fee],[Loan_Settings],[Loan_Comment],[IsDelete] FROM Loan_Master  where Loan_Full_ID = '" + loan_full_id + "'";

        DataTable dtLoan_Master = gen_db_utils.gp_sql_get_datatable(stQry, dbKey);

        str_return = " <div class='panel panel-default'><div class='panel-heading accordion-toggle collapsed' data-toggle='collapse'" +
            "  data-parent='#accordion' data-target='#collapseLoanMaster'>" + Getcolor("Loan Master", dtLoan_Master.Rows.Count) + "</div> ";

        if (dtLoan_Master != null && dtLoan_Master.Rows.Count > 0)
        {
            str_return += " <div id='collapseLoanMaster' class='panel-collapse collapse'><div class='panel-body' style='overflow-y:auto'>  <table id='datatable_fixed_column' class='table table-striped table-bordered'> "
                + " <thead> "
            + " <tr> "
             + "<th colspan=4;> Loan_Settings </th>"
             + "  </tr> "
             + "    </thead> "
             + "      <tbody>";
            foreach (DataRow dtrow in dtLoan_Master.Rows)
            {
                str_return += "<tr>"
                 + "<td colspan=4;> " +
                  "<pre id=preloanSetting style='padding-left: 20px;'></pre>"
                 + " </td>"
                + "</tr>";
            }
            str_return += " </tbody> "
                + " <thead> "
            + " <tr> "
            + "<th style='width: 200px'> Loan Details </th>"
             + "<th  style='width: 200px'> Borrower Details </th>"
             + "<th  style='width: 200px'> Spouse Details </th>"
             + "<th  style='width: 200px'> Farmer Details </th>"
             + "  </tr> "
             + "    </thead> "
             + "      <tbody>";
            foreach (DataRow dtrow in dtLoan_Master.Rows)
            {
                jsondata.Value = dtrow["Loan_Settings"].ToString();
                Loan_ID = dtrow["Loan_ID"].ToString();
                str_return += "<tr>"
                  + "<td>" +
                    " Loan_Master_ID:" + dtrow["Loan_Master_ID"].ToString() +
                    "<br>Loan_ID:" + dtrow["Loan_ID"].ToString() +
                    "<br>Loan_Seq_num:" + dtrow["Loan_Seq_num"].ToString() +
                    "<br>Loan_Full_ID:" + dtrow["Loan_Full_ID"].ToString() +
                    "<br>Loan_Type_Code:" + dtrow["Loan_Type_Code"].ToString() +
                    "<br>Loan_Pending_Action_Type_Code:" + dtrow["Loan_Pending_Action_Type_Code"].ToString() +
                    "<br>Loan_Pending_Action_Level:" + dtrow["Loan_Pending_Action_Level"].ToString() +
                "<br>Loan_Status:" + dtrow["Loan_Status"].ToString() +
                    "<br>Prev_Loan_Status:" + dtrow["Prev_Loan_Status"].ToString() +
                    "<br>Crop_Year:" + dtrow["Crop_Year"].ToString() +
                    "<br>Region_ID:" + dtrow["Region_ID"].ToString() +
                    "<br>Office_ID:" + dtrow["Office_ID"].ToString() +
                    "<br>Loan_Officer_ID:" + fc_gen_utils.sess_lookup("user_" + dtrow["Loan_Officer_ID"].ToString(), "-") +
                    "<br>Application_Date:" + dtrow["Application_Date"].ToString() +
                //"<br>Loan_Settings:<pre id=preloanSetting style='padding-left: 20px;'></pre>" +
                "</td>" +
                 "<td> " +
                     " Borrower_ID:" + dtrow["Borrower_ID"].ToString() +
                    "<br>Borrower_ID_Type:" + dtrow["Borrower_ID_Type"].ToString() +
                    "<br>Borrower_SSN_Hash:" + dtrow["Borrower_SSN_Hash"].ToString() +
                    "<br>Borrower_Entity_Type_Code:" + dtrow["Borrower_Entity_Type_Code"].ToString() +
                    "<br>Borrower_Last_Name:" + dtrow["Borrower_Last_Name"].ToString() +
                    "<br>Borrower_First_Name:" + dtrow["Borrower_First_Name"].ToString() +
                    "<br>Borrower_MI:" + dtrow["Borrower_MI"].ToString() +
                    "<br>Borrower_Address:" + dtrow["Borrower_Address"].ToString() +
                    "<br>Borrower_City:" + dtrow["Borrower_City"].ToString() +
                    //"<br>Borrower_State_Abbrev:" + dtrow["Borrower_State_Abbrev"].ToString() + "|" + fc_gen_utils.sess_lookup("st_" + dtrow["Borrower_State_Abbrev"].ToString(), "-") +
                    "<br>Borrower_State_Abbrev:" + dtrow["Borrower_State_Abbrev"].ToString() +
                    "<br>Borrower_State:" + dtrow["Borrower_State"].ToString() +
                    "<br>Borrower_Zip:" + dtrow["Borrower_Zip"].ToString() +
                    "<br>Borrower_Phone:" + dtrow["Borrower_Phone"].ToString() +
                    "<br>Borrower_email:" + dtrow["Borrower_email"].ToString() +
                    //"<br>Borrower_DL_state:" + dtrow["Borrower_DL_state"].ToString() + "|" + fc_gen_utils.sess_lookup("st_" + dtrow["Borrower_DL_state"].ToString(), "-") +
                    "<br>Borrower_DL_state:" + dtrow["Borrower_DL_state"].ToString() +
                    "<br>Borrower_Dl_Num:" + dtrow["Borrower_Dl_Num"].ToString() +
                    "<br>Borrower_DOB:" + dtrow["Borrower_DOB"].ToString() +
                    "<br>Borrower_Preferred_Contact_Ind:" + dtrow["Borrower_Preferred_Contact_Ind"].ToString() +
                    "<br>Borrower_County_ID:" + dtrow["Borrower_County_ID"].ToString() +
                    "<br>Borrower_Lat:" + dtrow["Borrower_Lat"].ToString() +
                    "<br>Borrower_Long:" + dtrow["Borrower_Long"].ToString() +
                    "<br>Borrower_Farm_Financial_Rating:" + dtrow["Borrower_Farm_Financial_Rating"].ToString() +
                    "<br>Borrower_Rating:" + dtrow["Borrower_Rating"].ToString() +
                   "</td>" +
                    "<td>" +
                        "Spouse_SSN_Hash:" + dtrow["Spouse_SSN_Hash"].ToString() +
                        "<br>Spouse_Last_name:" + dtrow["Spouse_Last_name"].ToString() +
                        "<br>Spouse_First_Name:" + dtrow["Spouse_First_Name"].ToString() +
                        "<br>Spouse_MI:" + dtrow["Spouse_MI"].ToString() +
                        "<br>Spouse_Address:" + dtrow["Spouse_Address"].ToString() +
                        "<br>Spouse_City:" + dtrow["Spouse_City"].ToString() +
                        "<br>Spouse_State:" + dtrow["Spouse_State"].ToString() +
                        "<br>Spouse_Zip:" + dtrow["Spouse_Zip"].ToString() +
                        "<br>Spouse_Phone:" + dtrow["Spouse_Phone"].ToString() +
                        "<br>Spouse_Email:" + dtrow["Spouse_Email"].ToString() +
                        "<br>Spouse_Preferred_Contact_Ind:" + dtrow["Spouse_Preferred_Contact_Ind"].ToString() +
                     "</td>" +
                      "<td> "
                   + "Farmer_ID:" + dtrow["Farmer_ID"].ToString() +
                    "<br>Farmer_ID_Type:" + dtrow["Farmer_ID_Type"].ToString() +
                    "<br>Farmer_SSN_Hash:" + dtrow["Farmer_SSN_Hash"].ToString() +
                    "<br>Farmer_Entity_Type:" + dtrow["Farmer_Entity_Type"].ToString() +
                    "<br>Farmer_Last_Name:" + dtrow["Farmer_Last_Name"].ToString() +
                    "<br>Farmer_First_Name:" + dtrow["Farmer_First_Name"].ToString() +
                    "<br>Farmer_MI:" + dtrow["Farmer_MI"].ToString() +
                    "<br>Farmer_Address:" + dtrow["Farmer_Address"].ToString() +
                    "<br>Farmer_City:" + dtrow["Farmer_City"].ToString() +
                    // "<br>Farmer_State:" + dtrow["Farmer_State"].ToString() +
                    "<br>Farmer_State:" + dtrow["Farmer_State"].ToString() + "|" + fc_gen_utils.sess_lookup("st_" + dtrow["Farmer_State"].ToString(), "-") +
                    "<br>Farmer_Zip:" + dtrow["Farmer_Zip"].ToString() +
                    "<br>Farmer_Phone:" + dtrow["Farmer_Phone"].ToString() +
                    "<br>Farmer_Email:" + dtrow["Farmer_Email"].ToString() +
                    //"<br>Farmer_DL_State:" + dtrow["Farmer_DL_State"].ToString() +
                    "<br>Farmer_DL_State:" + dtrow["Farmer_DL_State"].ToString() + "|" + fc_gen_utils.sess_lookup("st_" + dtrow["Farmer_DL_State"].ToString(), "-") +
                    "<br>Farmer_DL_Num:" + dtrow["Farmer_DL_Num"].ToString() +
                    "<br>Farmer_DOB:" + dtrow["Farmer_DOB"].ToString() +
                    "<br>Farmer_Preferred_Contact_Ind:" + dtrow["Farmer_Preferred_Contact_Ind"].ToString()
                 + " </td>"
                + "</tr>";
            }
            str_return += " </tbody> "
                + " <thead> "
            + " <tr> "
            + "<th  style='width: 200px'> Other Details </th>"
             + "<th  style='width: 200px'> Assets & Liabilities Details </th>"
             + "<th  style='width: 200px'> Ammounts, Commitment & Fee Details </th>"
            + "<th  style='width: 200px'> Disc Value Details </th>"
             + "  </tr> "
             + "    </thead> "
             + "      <tbody>";
            foreach (DataRow dtrow in dtLoan_Master.Rows)
            {
                str_return += "<tr>"
                + "<td> " +
                "Year_Begin_Farming:" + dtrow["Year_Begin_Farming"].ToString() +
                    "<br>Year_Begin_Client:" + dtrow["Year_Begin_Client"].ToString() +
                    "<br>Referral_Type_Code:" + dtrow["Referral_Type_Code"].ToString() +
                    "<br>Primary_AIP_ID:" + dtrow["Primary_AIP_ID"].ToString() +
                    "<br>Primary_AIP_Name:" + dtrow["Primary_AIP_Name"].ToString() +
                    "<br>Distributor_ID:" + dtrow["Distributor_ID"].ToString() +
                    "<br>Distributor_Name:" + dtrow["Distributor_Name"].ToString() +
                    "<br>Primary_Agency_ID:" + dtrow["Primary_Agency_ID"].ToString() +
                    "<br>Primary_Agency_Name:" + dtrow["Primary_Agency_Name"].ToString() +
                    "<br>Current_Bankruptcy_Status:" + dtrow["Current_Bankruptcy_Status"].ToString() +
                    "<br>Original_Bankruptcy_Status:" + dtrow["Original_Bankruptcy_Status"].ToString() +
                    "<br>Previous_Bankruptcy_Status:" + dtrow["Previous_Bankruptcy_Status"].ToString() +
                    "<br>Judgement_Ind:" + dtrow["Judgement_Ind"].ToString() +
                    "<br>Local_Ind:" + dtrow["Local_Ind"].ToString() +
                    "<br>Credit_Score:" + dtrow["Credit_Score"].ToString() +
                    "<br>Credit_Score_Date:" + dtrow["Credit_Score_Date"].ToString() +
                    "<br>CPA_Prepared_Financials:" + dtrow["CPA_Prepared_Financials"].ToString() +
                    "<br>Financials_Date:" + dtrow["Financials_Date"].ToString() +
                " </td>"
                 + "<td> " +
                    "Current_Assets:" + dtrow["Current_Assets"].ToString() +
                    "<br>Inter_Assets:" + dtrow["Inter_Assets"].ToString() +
                    "<br>Fixed_Assets:" + dtrow["Fixed_Assets"].ToString() +
                    "<br>Total_Assets:" + dtrow["Total_Assets"].ToString() +
                    "<br>Current_Assets_Disc_Percent:" + dtrow["Current_Assets_Disc_Percent"].ToString() +
                    "<br>Inter_Assets_Disc_Percent:" + dtrow["Inter_Assets_Disc_Percent"].ToString() +
                    "<br>Fixed_Assets_Disc_Percent:" + dtrow["Fixed_Assets_Disc_Percent"].ToString() +
                    "<br>Current_Liabilities:" + dtrow["Current_Liabilities"].ToString() +
                    "<br>Inter_Liabilities:" + dtrow["Inter_Liabilities"].ToString() +
                    "<br>Fixed_Liabilities:" + dtrow["Fixed_Liabilities"].ToString() +
                    "<br>Total_Liabilities:" + dtrow["Total_Liabilities"].ToString() +
                    "<br>Current_Disc_Net_Worth:" + dtrow["Current_Disc_Net_Worth"].ToString() +
                    "<br>Inter_Disc_Net_Worth:" + dtrow["Inter_Disc_Net_Worth"].ToString() +
                    "<br>Fixed_Disc_Net_Worth:" + dtrow["Fixed_Disc_Net_Worth"].ToString() +
                    "<br>Total_Disc_Net_Worth:" + dtrow["Total_Disc_Net_Worth"].ToString() +
                 " </td>"
                 + "<td> " +

                 "Total_Crop_Acres:" + dtrow["Total_Crop_Acres"].ToString() +
                    "<br>Total_Revenue:" + dtrow["Total_Revenue"].ToString() +
                    "<br>ARM_Commitment:" + dtrow["ARM_Commitment"].ToString() +
                    "<br>Dist_Commitment:" + dtrow["Dist_Commitment"].ToString() +
                    "<br>Third_Party_Credit:" + dtrow["Third_Party_Credit"].ToString() +
                    "<br>Total_Commitment:" + dtrow["Total_Commitment"].ToString() +
                    "<br>Cash_Flow_Amount:" + dtrow["Cash_Flow_Amount"].ToString() +
                    "<br>Break_Even_Percent:" + dtrow["Break_Even_Percent"].ToString() +
                    "<br>Risk_Cushion_Amount:" + dtrow["Risk_Cushion_Amount"].ToString() +
                    "<br>Risk_Cushion_Percent:" + dtrow["Risk_Cushion_Percent"].ToString() +
                    "<br>Return_Percent:" + dtrow["Return_Percent"].ToString() +
                    "<br>Ag_Pro_Requested_Credit:" + dtrow["Ag_Pro_Requested_Credit"].ToString() +
                    "<br>Requested_Credit:" + dtrow["Requested_Credit"].ToString() +
                    "<br>Maturity_Date:" + dtrow["Maturity_Date"].ToString() +
                    "<br>Estimated_Days:" + dtrow["Estimated_Days"].ToString() +
                    "<br>Origination_Fee_Percent:" + dtrow["Origination_Fee_Percent"].ToString() +
                    "<br>Service_Fee_Percent:" + dtrow["Service_Fee_Percent"].ToString() +
                    "<br>Extension_Fee_Percent:" + dtrow["Extension_Fee_Percent"].ToString() +
                    "<br>Interest_Percent:" + dtrow["Interest_Percent"].ToString() +
                    "<br>Origination_Fee_Amount:" + dtrow["Origination_Fee_Amount"].ToString() +
                    "<br>Service_Fee_Amount:" + dtrow["Service_Fee_Amount"].ToString() +
                    "<br>Extension_Fee_Amount:" + dtrow["Extension_Fee_Amount"].ToString() +
                    "<br>Interest_Est_Amount:" + dtrow["Interest_Est_Amount"].ToString() +
                 " </td>"
                  + "<td> " +
                 "Disc_value_Crops:" + dtrow["Disc_value_Crops"].ToString() +
                    "<br>Disc_value_Insurance:" + dtrow["Disc_value_Insurance"].ToString() +
                    "<br>Disc_value_FSA:" + dtrow["Disc_value_FSA"].ToString() +
                    "<br>Disc_value_Livestock:" + dtrow["Disc_value_Livestock"].ToString() +
                    "<br>Disc_value_Stored_Crops:" + dtrow["Disc_value_Stored_Crops"].ToString() +
                    "<br>Disc_value_Equipment:" + dtrow["Disc_value_Equipment"].ToString() +
                    "<br>Disc_value_Real_Estate:" + dtrow["Disc_value_Real_Estate"].ToString() +
                    "<br>Disc_value_Other:" + dtrow["Disc_value_Other"].ToString() +
                    "<br>Disc_value_Total:" + dtrow["Disc_value_Total"].ToString() +
                " </td>"
                + "</tr>";
            }
            str_return += " </tbody> "
                + " <thead> "
            + " <tr> "
             + "<th  style='width: 200px'> Budget Details </th>"
             + "<th  style='width: 200px'> Delta Details </th>"
             + "<th  style='width: 200px'> New Market Value Details </th>"
             + "  </tr> "
             + "    </thead> "
             + "      <tbody>";
            foreach (DataRow dtrow in dtLoan_Master.Rows)
            {
                str_return += "<tr>"

                 + "<td> " +
                 "Prev_Addendum_Percent:" + dtrow["Prev_Addendum_Percent"].ToString() +
                    "<br>Current_Addendum_Percent:" + dtrow["Current_Addendum_Percent"].ToString() +
                    //"<br>Rate_Yeild_Ref_Yeild_Percent:" + dtrow["Rate_Yeild_Ref_Yeild_Percent"].ToString() +
                    "<br>Original_Application_Date:" + dtrow["Original_Application_Date"].ToString() +
                    //"<br>Orginal_Maturity_Date:" + dtrow["Orginal_Maturity_Date"].ToString() +
                    //"<br>Orginal_Bankruptcy_Status:" + dtrow["Orginal_Bankruptcy_Status"].ToString() +
                    "<br>Decision_Date:" + dtrow["Decision_Date"].ToString() +
                    "<br>Close_Date:" + dtrow["Close_Date"].ToString() +
                    "<br>Cross_Collateral_ID:" + dtrow["Cross_Col_ID"].ToString() +
                    "<br>Controlled_Disbursement_Ind:" + dtrow["Controlled_Disbursement_Ind"].ToString() +
                    "<br>Watch_List_ind:" + dtrow["Watch_List_ind"].ToString() +
                    "<br>Nortridge_API_In_ID:" + dtrow["Nortridge_API_In_ID"].ToString() +
                    "<br>Third_Party_Total_Budget:" + dtrow["Third_Party_Total_Budget"].ToString() +
                    "<br>Dist_Total_Budget:" + dtrow["Dist_Total_Budget"].ToString() +
                    "<br>Arm_Total_Budget:" + dtrow["Arm_Total_Budget"].ToString() +
                    "<br>Loan_Total_Budget:" + dtrow["Loan_Total_Budget"].ToString() +
                    "<br>Arm_Extension_Fee:" + dtrow["Arm_Extension_Fee"].ToString() +
                    "<br>Dist_Rate_Fee:" + dtrow["Dist_Rate_Fee"].ToString() +
                    "<br>Arm_Rate_Fee:" + dtrow["Arm_Rate_Fee"].ToString() +
                 " </td>"
                 + "<td> " +
                    "Delta_Crop_Acres:" + dtrow["Delta_Crop_Acres"].ToString() +
                    "<br>Delta_Total_Revenue:" + dtrow["Delta_Total_Revenue"].ToString() +
                    "<br>Delta_ARM_Commitment:" + dtrow["Delta_ARM_Commitment"].ToString() +
                    "<br>Delta_Dist_Commitment:" + dtrow["Delta_Dist_Commitment"].ToString() +
                    "<br>Delta_Third_Party_Credit:" + dtrow["Delta_Third_Party_Credit"].ToString() +
                    "<br>Delta_Total_Commitment:" + dtrow["Delta_Total_Commitment"].ToString() +
                    "<br>Delta_Origination_Fee_Amount:" + dtrow["Delta_Origination_Fee_Amount"].ToString() +
                    "<br>Delta_Service_Fee_Amount:" + dtrow["Delta_Service_Fee_Amount"].ToString() +
                    "<br>Delta_Cash_Flow_Amount:" + dtrow["Delta_Cash_Flow_Amount"].ToString() +
                    "<br>Delta_Risk_Cushion_Amount:" + dtrow["Delta_Risk_Cushion_Amount"].ToString() +
                    "<br>Delta_Risk_Cushion_Percent:" + dtrow["Delta_Risk_Cushion_Percent"].ToString() +
                    "<br>Delta_Return_Percent:" + dtrow["Delta_Return_Percent"].ToString() +
                 " </td>"
                 + "<td> " +
                    "Net_Market_Value_Crops:" + dtrow["Net_Market_Value_Crops"].ToString() +
                    "<br>Net_Market_Value_Insurance:" + dtrow["Net_Market_Value_Insurance"].ToString() +
                    "<br>Net_Market_Value_FSA:" + dtrow["Net_Market_Value_FSA"].ToString() +
                    "<br>Net_Market_Value_Livestock:" + dtrow["Net_Market_Value_Livestock"].ToString() +
                    "<br>Net_Market_Value_Stored_Crops:" + dtrow["Net_Market_Value_Stored_Crops"].ToString() +
                    "<br>Net_Market_Value_Equipment:" + dtrow["Net_Market_Value_Equipment"].ToString() +
                    "<br>Net_Market_Value_Real_Estate:" + dtrow["Net_Market_Value_Real_Estate"].ToString() +
                    "<br>Net_Market_Value_Other:" + dtrow["Net_Market_Value_Other"].ToString() +
                 " </td>"
                + "</tr>";
            }
            foreach (DataRow dtrow in dtLoan_Master.Rows)
            {
                str_return += "<tr>"
                + "<td colspan='4' >[Loan_Comment]  :   " + dtrow["Loan_Comment"].ToString() + " </td>"
                + "</tr>";
            }
            str_return += " </tbody> "
           + " </table> "
           + " </div></div></div > ";
        }
        else
        {
            str_return += " <div id='collapseLoanMaster' class='panel-collapse collapse'><div class='panel-body' style='overflow-y:auto'>  <table id='datatable_fixed_column' class='table table-striped table-bordered'> "
                + " <thead> "
                + " <tr> "
                + "<th  style='width: 200px'> Loan Details </th>"
                 + "<th  style='width: 200px'> Borrower Details </th>"
                 + "<th  style='width: 200px'> Co Borrower Details </th>"
                 + "<th  style='width: 200px'> Spouse Details </th>"
                 + "  </tr> "
                 + "    </thead>"
                 + "      <tbody><td></td>";

            str_return += " </tbody> "
                + " <thead> "
            + " <tr> "
             + "<th  style='width: 200px'> Farmer Details </th>"
            + "<th  style='width: 200px'> Other Details </th>"
             + "<th  style='width: 200px'> Assets & Liabilities Details </th>"
             + "<th  style='width: 200px'> Ammounts, Commitment & Fee Details </th>"
             + "  </tr> "
             + "    </thead>"
             + "      <tbody><td></td>";

            str_return += " </tbody> "
                + " <thead> "
            + " <tr> "
            + "<th  style='width: 200px'> Disc Value Details </th>"
             + "<th  style='width: 200px'> Budget Details </th>"
             + "<th  style='width: 200px'> Delta Details </th>"
             + "<th  style='width: 200px'> New Market Value Details </th>"
             + "  </tr> <br><br> "
             + "    </thead>"
             + "      <tbody><td></td>";
            str_return += "<tr>"
               + "<td colspan='4' >[Loan_Comment]  : </td>"
               + "</tr>";
            str_return += " </tbody> "
           + " </table> "
           + " </div></div></div > ";
        }

        return str_return;
    }

    public string Get_Affliliated_loans(string loan_full_id)
    {
        string stQry = "";
        string str_return = "";

        stQry = "select loan_id, loan_full_id, farmer_id,Farmer_Last_name,Farmer_First_Name, Borrower_Id,Borrower_Last_name," +
            "Borrower_First_Name from loan_master where farmer_id in " +
            "(select farmer_id from loan_master where loan_full_Id='" + loan_full_id + "')and is_latest = 1";

        DataTable dtAffliliatedloans = gen_db_utils.gp_sql_get_datatable(stQry, dbKey);

        str_return = " <div class='panel panel-default'><div class='panel-heading accordion-toggle collapsed' data-toggle='collapse'" +
                  "  data-parent='#accordion' data-target='#collapseAffliliatedloans'>" + Getcolor("Affliliated loans", dtAffliliatedloans.Rows.Count) + "</div> ";

        str_return += " <div id='collapseAffliliatedloans' class='panel-collapse collapse'><div class='panel-body' style='overflow-y:auto'>  <table id='datatable_fixed_column' class='table table-striped table-bordered'> "
          + " <thead> "
           + " <tr> "
           + "<th> Loan_Id </th>"
           + "<th> Loan_Full_ID </th>"
           //  + "<th> Farmer_Id </th>"
           + "<th> Farmer_Name </th>"
        //   + "<th> Borrower_Id </th>"
           + "<th> Borrower_Name </th>"
            + "  </tr> "
            + "    </thead> "
            + "      <tbody>";
        if (dtAffliliatedloans != null && dtAffliliatedloans.Rows.Count > 0)
        {
            foreach (DataRow dtrow in dtAffliliatedloans.Rows)
            {
                str_return += "<tr>"
                + "<td> " + dtrow["loan_id"].ToString() + " </td>"
                + "<td> " + dtrow["Loan_Full_ID"].ToString() + " </td>"
                //  + "<td> " + dtrow["farmer_id"].ToString() + " </td>"
                + "<td> " + dtrow["Farmer_First_Name"].ToString() + " " + dtrow["Farmer_Last_name"].ToString() + " </td>"
                //  + "<td> " + dtrow["Borrower_Id"].ToString() + " </td>"
                + "<td> " + dtrow["Borrower_First_Name"].ToString() + "  " + dtrow["Borrower_Last_name"].ToString() + " </td>"
                + "</tr>";
            }
            str_return += " </tbody> "
           + " </table> "
           + " </div></div></div > ";
        }
        else
        {
            str_return += " <table id='datatable_fixed_column' class='table table-striped table-bordered'>";

            str_return += " </tbody> "
                                   + " </table> "
                                     + "</div></div></div >";
        }

        return str_return;
    }

    public string Get_Audit_Trails(string loan_full_id)
    {
        string stQry = "";
        string str_return = "";
        stQry = " select TOP 1000 Loan_Audit_Trail_ID,Loan_Full_ID,Loan_Seq_Num,User_ID,Date_Time,Loan_Status," +
            "  View_Update_Ind,Audit_Field_ID,Audit_Trail_Text, Audit_Trail_Type FROM Loan_Audit_Trail where Loan_Full_ID='" + loan_full_id + "' ";
        stQry += " order by Date_Time desc";
        DataTable dtLoanAuditTrail = gen_db_utils.gp_sql_get_datatable(stQry, dbKey);
        str_return = " <div class='panel panel-default'><div class='panel-heading accordion-toggle collapsed' data-toggle='collapse'" +
                  "  data-parent='#accordion' data-target='#collapseAuditList'>" + Getcolor("Audit List", dtLoanAuditTrail.Rows.Count) + " </div> ";

        str_return += " <div id='collapseAuditList' class='panel-collapse collapse'><div class='panel-body' style='overflow-y:auto'>  <table id='datatable_fixed_column' class='table table-striped table-bordered'> "
                             + "<thead> "
                              + "     <tr> "
                               + "      <th > Loan Audit Trail ID  </th> "
                               + "      <th style='width:10%;' > Loan Full ID  </th> "
                               + "      <th > Loan Seq Num  </th> "
                               + "      <th style='width:5%;' > User ID  </th> "
                               + "      <th style='width:15%;' > Date Time  </th> "
                               + "      <th > Loan Status  </th> "
                               + "      <th > View Update Ind  </th> "
                               + "      <th > Audit Field ID  </th> "
                               + "      <th > Audit Trail Text  </th> "
                               + "      <th > Audit Trail Type  </th> "
                              + "    </tr> "
                              + "      </thead> "
                              + "        <tbody> ";
        if (dtLoanAuditTrail != null && dtLoanAuditTrail.Rows.Count > 0)
        {
            foreach (DataRow dtrow in dtLoanAuditTrail.Rows)
            {
                str_return += "<tr>"
                                + "<td> " + dtrow["Loan_Audit_Trail_ID"].ToString() + " </td>"
                                + "<td> " + dtrow["Loan_Full_ID"].ToString() + " </td>"
                                + "<td> " + dtrow["Loan_Seq_Num"].ToString() + " </td>"
                                + "<td> " + dtrow["User_ID"].ToString() + " </td>"
                                + "<td> " + dtrow["Date_Time"].ToString() + " </td>"
                                + "<td> " + dtrow["Loan_Status"].ToString() + " </td>"
                                + "<td> " + dtrow["View_Update_Ind"].ToString() + " </td>"
                                + "<td> " + dtrow["Audit_Field_ID"].ToString() + " </td>"
                                + "<td> " + dtrow["Audit_Trail_Text"].ToString() + " </td>"
                                + "<td> " + dtrow["Audit_Trail_Type"].ToString() + " </td>"
                               + "</tr>";
            }

            str_return += " </tbody> "
                                    + " </table> "
                                      + "</div></div></div >";
        }
        else
        {
            str_return += " <table id='datatable_fixed_column' class='table table-striped table-bordered'>";

            str_return += " </tbody> "
                                   + " </table> "
                                     + "</div></div></div >";
        }

        return str_return;
    }

    public string Get_Cross_Collateral1(string loan_full_id)
    {
        string stQry = "";
        string str_return = "";
        stQry = "select Cross_Collateral_ID,cross_collateral_group_Id,Loan_ID from loan_cross_collateral where cross_collateral_group_Id " +
            "in (select cross_collateral_group_Id from loan_cross_collateral " +
            "where loan_Id=(select loan_Id from loan_master where loan_full_id='" + loan_full_id + "')) and status = 0";
        DataTable dtCrossCollateral = gen_db_utils.gp_sql_get_datatable(stQry, dbKey);
        str_return = " <div class='panel panel-default'><div class='panel-heading accordion-toggle collapsed' data-toggle='collapse'" +
           "  data-parent='#accordion' data-target='#collapseCrossCollateral'>" + Getcolor("Cross Collateral", dtCrossCollateral.Rows.Count) + "</div> ";
        str_return += " <div id='collapseCrossCollateral' class='panel-collapse collapse'><div class='panel-body' style='overflow-y:auto'>  <table id='datatable_fixed_column' class='table table-striped table-bordered'> "
          + " <thead> "
           + " <tr> "
           + "<th> Cross_Collateral_ID </th>"
           + "<th> cross_collateral_group_Id </th>"
           + "<th> Loan_ID </th>"
            + "  </tr> "
            + "    </thead> "
            + "      <tbody>";
        if (dtCrossCollateral != null && dtCrossCollateral.Rows.Count > 0)
        {
            foreach (DataRow dtrow in dtCrossCollateral.Rows)
            {
                str_return += "<tr>"
                + "<td> " + dtrow["Cross_Collateral_ID"].ToString() + " </td>"
                + "<td> " + dtrow["cross_collateral_group_Id"].ToString() + " </td>"
                + "<td> " + dtrow["Loan_ID"].ToString() + " </td>"
                + "</tr>";
            }
            str_return += " </tbody> "
           + " </table> "
           + " </div></div></div > ";
        }
        else
        {
            str_return += " <table id='datatable_fixed_column' class='table table-striped table-bordered'>";

            str_return += " </tbody> "
                                   + " </table> "
                                     + "</div></div></div >";
        }

        return str_return;
    }

    public string Get_Cross_Collateral(string loan_full_id)
    {
        string stQry = "";
        string str_return = "";

        stQry = @" select lcc.Loan_ID,loan_full_id, farmer_id,Farmer_Last_name,Farmer_First_Name, Borrower_Id,Borrower_Last_name,Borrower_First_Name  from loan_cross_collateral LCC
                     left join loan_master LM on LCC.Loan_ID=LM.Loan_ID
                     where cross_collateral_group_Id in (select cross_collateral_group_Id from loan_cross_collateral
                     where loan_Id=(select loan_Id from loan_master where loan_full_id='" + loan_full_id + "')) and status = 0";
        DataTable dtCrossCollateral = gen_db_utils.gp_sql_get_datatable(stQry, dbKey);
        str_return = " <div class='panel panel-default'><div class='panel-heading accordion-toggle collapsed' data-toggle='collapse'" +
           "  data-parent='#accordion' data-target='#collapseCrossCollateral'>" + Getcolor("Cross Collateral", dtCrossCollateral.Rows.Count) + "</div> ";
        str_return += " <div id='collapseCrossCollateral' class='panel-collapse collapse'><div class='panel-body' style='overflow-y:auto'>  <table id='datatable_fixed_column' class='table table-striped table-bordered'> "
          + " <thead> "
           + " <tr> "
           + "<th> Loan_ID </th>"
           + "<th> loan_full_id </th>"
           + "<th> Farmer_name </th>"
           + "<th> Borrower_Name </th>"
            + "  </tr> "
            + "    </thead> "
            + "      <tbody>";
        if (dtCrossCollateral != null && dtCrossCollateral.Rows.Count > 0)
        {
            foreach (DataRow dtrow in dtCrossCollateral.Rows)
            {
                str_return += "<tr>"
                + "<td> " + dtrow["Loan_ID"].ToString() + " </td>"
                + "<td> " + dtrow["loan_full_id"].ToString() + " </td>"
                + "<td> " + dtrow["Farmer_First_Name"].ToString() + " " + dtrow["Farmer_Last_name"].ToString() + "</td>"
                + "<td> " + dtrow["Borrower_First_Name"].ToString() + " " + dtrow["Borrower_Last_name"].ToString() + "</td>"
                + "</tr>";
            }
            str_return += " </tbody> "
           + " </table> "
           + " </div></div></div > ";
        }
        else
        {
            str_return += " <table id='datatable_fixed_column' class='table table-striped table-bordered'>";

            str_return += " </tbody> "
                                   + " </table> "
                                     + "</div></div></div >";
        }

        return str_return;
    }

    private string Get_Loan_Associations(string loan_full_id)
    {
        string str_return;

        string strSQL = "  SELECT [Assoc_ID] ,[Ref_Assoc_ID],[Z_Loan_ID],[Z_Loan_Seq_Num],[Loan_Full_ID],[Assoc_Type_Code] "
                      + " ,[Assoc_Name],[Contact],[Location],[Phone],[Email],[Amount],[Referred_Type],[Response] "
                      + " ,[Is_CoBorrower],[Preferred_Contact_Ind],[Assoc_Status],[Status],[IsDelete]  FROM[dbo].[Loan_Association] "
                      + "  where Loan_Full_ID = '" + loan_full_id + "'";

        DataTable dtLoan_Associations = gen_db_utils.gp_sql_get_datatable(strSQL, dbKey);
        str_return = " <div class='panel panel-default'><div class='panel-heading accordion-toggle collapsed' data-toggle='collapse'" +
            "  data-parent='#accordion' data-target='#collapseLoanAssociation'>" + Getcolor("Association", dtLoan_Associations.Rows.Count) + "</div> ";
        str_return += "<div id='collapseLoanAssociation' class='panel-collapse collapse'><div class='panel-body' style='overflow-y:auto'>  <table id='datatable_fixed_column' class='table table-striped table-bordered'> "
                  + "      <thead> "
                  + "     <tr> "
                + "       <th >Assoc_ID</th> "
               + "       <th >Ref_Assoc_ID</th> "
                + "       <th >Z_Loan_ID</th> "
               + "       <th >Z_Loan_Seq_Num</th> "
                + "       <th >Loan_Full_ID</th> "
               + "       <th >Assoc_Type_Code</th> "
                + "       <th >Assoc_Name</th> "
                + "       <th >Contact</th> "
               + "       <th >Location</th> "
                + "       <th >Phone</th> "
               + "       <th >Email</th> "
               + "       <th >Amount</th> "
               + "       <th >Referred_Type</th> "
               + "       <th >Response</th> "
               + "       <th >Is_CoBorrower</th> "
               + "       <th >Preferred_Contact_Ind</th> "
               + "       <th >Assoc_Status</th> "
                  + "    </tr> "
                  + "      </thead> "
                  + "        <tbody> ";
        if (dtLoan_Associations != null && dtLoan_Associations.Rows.Count > 0)
        {
            foreach (DataRow dtRow in dtLoan_Associations.Rows)
            {
                str_return += "<tr>"
                              + "<td>" + dtRow["Assoc_ID"].ToString() + "</td>"
                              + "<td>" + dtRow["Ref_Assoc_ID"].ToString() + "</td>"
                               + "<td>" + dtRow["Z_Loan_ID"].ToString() + "</td>"
                              + "<td>" + dtRow["Z_Loan_Seq_Num"].ToString() + "</td>"
                               + "<td>" + dtRow["Loan_Full_ID"].ToString() + "</td>"
                              + "<td>" + dtRow["Assoc_Type_Code"].ToString() + "</td>"
                               + "<td>" + dtRow["Assoc_Name"].ToString() + "</td>"
                               + "<td>" + dtRow["Contact"].ToString() + "</td>"
                              + "<td>" + dtRow["Location"].ToString() + "</td>"
                               + "<td>" + dtRow["Phone"].ToString() + "</td>"
                              + "<td>" + dtRow["Email"].ToString() + "</td>"
                              + "<td>" + dtRow["Amount"].ToString() + "</td>"
                              + "<td>" + dtRow["Referred_Type"].ToString() + "</td>"
                              + "<td>" + dtRow["Response"].ToString() + "</td>"
                              + "<td>" + dtRow["Is_CoBorrower"].ToString() + "</td>"
                              + "<td>" + dtRow["Preferred_Contact_Ind"].ToString() + "</td>"
                              + "<td>" + dtRow["Assoc_Status"].ToString() + "</td>"
                                 + "</tr>";
            }

            str_return += " </tbody> "
                                    + " </table>  "
                                    + "</div> </div ></div >";
        }
        else
        {
            str_return += " <table id='datatable_fixed_column' class='table table-striped table-bordered'>";
            str_return += " </tbody> "
                                   + " </table> "
                                     + "</div></div></div >";
        }
        return str_return;
    }

    public string Get_Loan_Budget(string loan_full_id)
    {
        string stQry = "";
        string str_return = "";

        stQry = "SELECT Loan_Budget_ID,Loan_Full_ID,Crop_Practice_ID,Expense_Type_ID,ARM_Budget_Acre,Distributor_Budget_Acre," +
                "Third_Party_Budget_Acre,Total_Budget_Acre,ARM_Budget_Crop,Distributor_Budget_Crop,Third_Party_Budget_Crop," +
                "Total_Budget_Crop_ET,Notes,Other_Description_Text,Budget_Type,Status,Z_Loan_ID,Z_Loan_Seq_num,isDelete FROM Loan_Budget "
                   + "  where Loan_Full_ID = '" + loan_full_id + "'";

        stQry += " Order by crop_practice_id,expense_type_id";

        DataTable dtLoanBudget = gen_db_utils.gp_sql_get_datatable(stQry, dbKey);
        str_return = " <div class='panel panel-default'><div class='panel-heading accordion-toggle collapsed' data-toggle='collapse'" +
                   "  data-parent='#accordion' data-target='#collapseLoanBudget'>" + Getcolor("Budget", dtLoanBudget.Rows.Count) + "</div> ";

        str_return += "<div id='collapseLoanBudget' class='panel-collapse collapse'><div class='panel-body' style='overflow-y:auto'>  <table id='datatable_fixed_column' class='table table-striped table-bordered'>"
                              + "<thead> "
                              + "     <tr> "
                               + "      <th > Loan Full ID </th> "
                              + "      <th > Loan Budget ID </th> "
                              + "      <th > Crop Practice ID </th> "
                              + "      <th > Expense Type ID </th> "
                             + "      <th > ARM Budget Acre </th> "
                             + "      <th > ARM Budget Crop </th> "
                             + "      <th > Notes </th> "
                             + "      <th > Budget_type </th> "
                             + "      <th > Status </th> "
                              + "    </tr> "
                              + "      </thead> "
                              + "        <tbody> ";

        if (dtLoanBudget != null && dtLoanBudget.Rows.Count > 0)
        {
            foreach (DataRow dtrow in dtLoanBudget.Rows)
            {
                str_return += "<tr>"
                                + "<td> " + dtrow["Loan_Full_ID"].ToString() + " </td>"
                                + "<td> " + dtrow["Loan_Budget_ID"].ToString() + " </td>"
                                + "<td> " + fc_gen_utils.sess_lookup("cp_" + dtrow["Crop_Practice_ID"].ToString(), "-") + " </td>"
                                + "<td> " + dtrow["Expense_Type_ID"].ToString() + " </td>"
                               + "<td> " + dtrow["ARM_Budget_Acre"].ToString() + "" +
                                         "<br>" + dtrow["Distributor_Budget_Acre"].ToString() +
                                        "<br>" + dtrow["Third_Party_Budget_Acre"].ToString() +
                                         "<br><b>" + dtrow["Total_Budget_Acre"].ToString() + "</b><br>" +
                                "</td>"
                                + "<td> " + dtrow["ARM_Budget_Crop"].ToString() + "" +
                                         "<br>" + dtrow["Distributor_Budget_Crop"].ToString() +
                                        "<br>" + dtrow["Third_Party_Budget_Crop"].ToString() +
                                         "<br><b>" + dtrow["Total_Budget_Crop_ET"].ToString() + "</b><br>" +
                                "</td>"
                                + "<td> " + dtrow["Notes"].ToString() + " </td>"
                                + "<td> " + dtrow["Budget_Type"].ToString() + " </td>"
                                 + "<td> " + dtrow["Status"].ToString() + "" +
                                         "<br>" + dtrow["isDelete"].ToString() +
                                "</td>"

                               + "</tr>";
            }

            str_return += " </tbody> "
                                    + " </table> "
                                      + "</div></div></div >";
        }
        else
        {
            str_return += " <table id='datatable_fixed_column' class='table table-striped table-bordered'>";

            str_return += " </tbody> "
                                   + " </table> "
                                     + "</div></div></div >";
        }

        return str_return;
    }

    public string Get_Loan_Collateral_Detail(string loan_full_id)
    {
        string stQry = "";
        string str_return = "";

        stQry = "SELECT Collateral_ID,Loan_ID,Loan_Full_ID,Loan_Seq_Num,Collateral_Category_Code,Collateral_Type_Code,Collateral_Sub_Type_Code,Crop_Detail,Crop_Type,Collateral_Description,Measure_Code,Insurance_Category_Code,Insurance_Type_Code,Insurance_Sub_Type_Code,Market_Value,Prior_Lien_Amount,Net_Market_Value,Disc_Pct,Insurance_Value,Insurance_Disc_Value,Disc_Mkt_Value,Lien_Holder,Status,IsDelete,Insured_Flag,Qty,Price from Loan_Collateral_Detail "
                   + "  where Loan_Full_ID = '" + loan_full_id + "'";

        // stQry += " Order by crop_practice_id,expense_type_id";

        DataTable dtLoanCollateralDetail = gen_db_utils.gp_sql_get_datatable(stQry, dbKey);

        str_return = " <div class='panel panel-default'><div class='panel-heading accordion-toggle collapsed' data-toggle='collapse'" +
    "  data-parent='#accordion' data-target='#collapseLoanCollateral'>" + Getcolor("Collateral", dtLoanCollateralDetail.Rows.Count) + "</div> ";
        str_return += " <div id='collapseLoanCollateral' class='panel-collapse collapse'><div class='panel-body' style='overflow-y:auto'>  <table id='datatable_fixed_column' class='table table-striped table-bordered'> "
            + " <thead> "
             + " <tr> "
             + "<th> Collateral_ID </th>"
             + "<th> Loan_ID </th>"
             + "<th> Loan_Full_ID </th>"
             + "<th> Loan_Seq_Num </th>"
             + "<th> Collateral_Category_Code </th>"
             + "<th> Collateral_Type_Code </th>"
             + "<th> Collateral_Sub_Type_Code </th>"
             + "<th> Crop_Detail </th>"
             + "<th> Crop_Type </th>"
             + "<th> Collateral_Description </th>"
             + "<th> Measure_Code </th>"
             + "<th> Insurance_Category_Code </th>"
             + "<th> Insurance_Type_Code </th>"
             + "<th> Insurance_Sub_Type_Code </th>"
             + "<th> Market_Value </th>"
             + "<th> Prior_Lien_Amount </th>"
             + "<th> Net_Market_Value </th>"
             + "<th> Disc_Pct </th>"
             + "<th> Insurance_Value </th>"
             + "<th> Insurance_Disc_Value </th>"
             + "<th> Disc_Mkt_Value </th>"
             + "<th> Lien_Holder </th>"
             + "<th> Status </th>"
             //+ "<th> IsDelete </th>"
             + "<th> Insured_Flag </th>"
             + "<th> Qty </th>"
             + "<th> Price </th>"
              + "  </tr> "
              + "    </thead> "
              + "      <tbody>";
        if (dtLoanCollateralDetail != null && dtLoanCollateralDetail.Rows.Count > 0)
        {
            foreach (DataRow dtrow in dtLoanCollateralDetail.Rows)
            {
                str_return += "<tr>"
                + "<td> " + dtrow["Collateral_ID"].ToString() + " </td>"
                 + "<td> " + dtrow["Loan_ID"].ToString() + " </td>"
                 + "<td> " + dtrow["Loan_Full_ID"].ToString() + " </td> "
                  + "<td> " + dtrow["Loan_Seq_Num"].ToString() + " </td> "
                  + "<td> " + dtrow["Collateral_Category_Code"].ToString() + " </td> "
                    + "<td> " + dtrow["Collateral_Type_Code"].ToString() + " </td> "
                     + "<td> " + dtrow["Collateral_Sub_Type_Code"].ToString() + " </td> "
                      + "<td> " + dtrow["Crop_Detail"].ToString() + " </td> "
                       + "<td> " + dtrow["Crop_Type"].ToString() + " </td> "
                        + "<td> " + dtrow["Collateral_Description"].ToString() + " </td> "
                         + "<td> " + dtrow["Measure_Code"].ToString() + " </td> "
                          + "<td> " + dtrow["Insurance_Category_Code"].ToString() + " </td> "
                           + "<td> " + dtrow["Insurance_Type_Code"].ToString() + " </td>"
                            + "<td> " + dtrow["Insurance_Sub_Type_Code"].ToString() + " </td>"
                             + "<td> " + dtrow["Market_Value"].ToString() + " </td>"
                              + "<td> " + dtrow["Prior_Lien_Amount"].ToString() + " </td>"
                               + "<td> " + dtrow["Net_Market_Value"].ToString() + " </td>"
                                + "<td> " + dtrow["Disc_Pct"].ToString() + " </td>"
                                 + "<td> " + dtrow["Insurance_Value"].ToString() + " </td>"
                                  + "<td> " + dtrow["Insurance_Disc_Value"].ToString() + " </td>"
                                   + "<td> " + dtrow["Disc_Mkt_Value"].ToString() + " </td>"
                                    + "<td> " + dtrow["Lien_Holder"].ToString() + " </td>"
                                     + "<td> " + dtrow["Status"].ToString() + " </td>"
                                       //+ "<td> " + dtrow["IsDelete"].ToString() + " </td>"
                                       + "<td> " + dtrow["Insured_Flag"].ToString() + " </td>"
                                        + "<td> " + dtrow["Qty"].ToString() + " </td>"
                                         + "<td> " + dtrow["Price"].ToString() + " </td>"
                                           + " </td>"
                                           + "</tr>";
            }
            str_return += " </tbody> "
                                   + " </table> "
                                    + "</div></div></div >";
        }
        else
        {
            str_return += " <table id='datatable_fixed_column' class='table table-striped table-bordered'>";

            str_return += " </tbody> "
                                   + " </table> "
                                     + "</div></div></div >";
        }

        return str_return;
    }

    public string Get_Loan_Comment(string loan_full_id)
    {
        string stQry = "";
        string str_return = "";

        stQry = "SELECT Loan_Comment_ID,Loan_Full_ID,Loan_ID,Loan_Seq_Num,Parent_Comment_ID,User_ID,Comment_Time,Comment_Type_Code,Comment_Type_Level_Code,Comment_Text,Parent_Emoji_ID,Comment_Emoji_ID,Comment_Read_Ind,Status FROM Loan_Comment "
                   + "  where Loan_Full_ID = '" + loan_full_id + "'";

        DataTable dtLoan_Comment = gen_db_utils.gp_sql_get_datatable(stQry, dbKey);

        str_return = " <div class='panel panel-default'><div class='panel-heading accordion-toggle collapsed' data-toggle='collapse'" +
    "  data-parent='#accordion' data-target='#collapseLoanComment'>" + Getcolor("Comment", dtLoan_Comment.Rows.Count) + "</div> ";

        str_return += " <div id='collapseLoanComment' class='panel-collapse collapse'><div class='panel-body' style='overflow-y:auto'>  <table id='datatable_fixed_column' class='table table-striped table-bordered'> "
          + " <thead> "
           + " <tr> "
           + "<th> Loan_Comment_ID </th>"
           + "<th> Loan_Full_ID </th>"
           + "<th> Loan_ID </th>"
           + "<th> Loan_Seq_Num </th>"
           + "<th> Parent_Comment_ID </th>"
           + "<th> User_ID </th>"
           + "<th> Comment_Time </th>"
           + "<th> Comment_Type_Code </th>"
           + "<th> Comment_Type_Level_Code </th>"
           + "<th> Comment_Text </th>"
           + "<th> Parent_Emoji_ID </th>"
           + "<th> Comment_Emoji_ID </th>"
           + "<th> Comment_Read_Ind </th>"
           + "<th> Status </th>"
            + "  </tr> "
            + "    </thead> "
            + "      <tbody>";

        if (dtLoan_Comment != null && dtLoan_Comment.Rows.Count > 0)
        {
            foreach (DataRow dtrow in dtLoan_Comment.Rows)
            {
                str_return += "<tr>"
                + "<td> " + dtrow["Loan_Comment_ID"].ToString() + " </td>"
                + "<td> " + dtrow["Loan_Full_ID"].ToString() + " </td>"
                + "<td> " + dtrow["Loan_ID"].ToString() + " </td>"
                + "<td> " + dtrow["Loan_Seq_Num"].ToString() + " </td>"
                + "<td> " + dtrow["Parent_Comment_ID"].ToString() + " </td>"
                //+ "<td> " + dtrow["User_ID"].ToString() + " </td>"
                + "<td> " + fc_gen_utils.sess_lookup("user_" + dtrow["User_ID"].ToString(), "-") + " </td>"
                + "<td> " + dtrow["Comment_Time"].ToString() + " </td>"
                + "<td> " + dtrow["Comment_Type_Code"].ToString() + " </td>"
                + "<td> " + dtrow["Comment_Type_Level_Code"].ToString() + " </td>"
                + "<td> " + dtrow["Comment_Text"].ToString() + " </td>"
                + "<td> " + dtrow["Parent_Emoji_ID"].ToString() + " </td>"
                + "<td> " + dtrow["Comment_Emoji_ID"].ToString() + " </td>"
                + "<td> " + dtrow["Comment_Read_Ind"].ToString() + " </td>"
                + "<td> " + dtrow["Status"].ToString() + " </td>"
                + " </td>"
                + "</tr>";
            }
            str_return += " </tbody> "
                                       + " </table> "
                                         + "</div></div></div >";
        }
        else
        {
            str_return += " <table id='datatable_fixed_column' class='table table-striped table-bordered'>";
            str_return += " </tbody> "
                                   + " </table> "
                                     + "</div></div></div >";
        }

        return str_return;
    }

    public string Get_Loan_Committee(string loan_full_id)
    {
        string stQry = "";
        string str_return = "";

        stQry = "SELECT CM_ID,Loan_Full_ID,User_ID,Role,Added_Date,Voted_Date,Vote,Vote_Emoji_Type,CM_Role,Status FROM Loan_Committee "
                   + "  where Loan_Full_ID = '" + loan_full_id + "'";

        DataTable dtLoan_Committee = gen_db_utils.gp_sql_get_datatable(stQry, dbKey);

        str_return = " <div class='panel panel-default'><div class='panel-heading accordion-toggle collapsed' data-toggle='collapse'" +
       "  data-parent='#accordion' data-target='#collapseLoanCommittee'>" + Getcolor("Committee ", dtLoan_Committee.Rows.Count) + "</div> ";
        str_return += " <div id='collapseLoanCommittee' class='panel-collapse collapse'><div class='panel-body' style='overflow-y:auto'>  <table id='datatable_fixed_column' class='table table-striped table-bordered'> "
          + " <thead> "
           + " <tr> "
           + "<th> CM_ID </th>"
           + "<th> Loan_Full_ID </th>"
           + "<th> User_ID </th>"
           + "<th> Role </th>"
           + "<th> Added_Date </th>"
           + "<th> Voted_Date </th>"
           + "<th> Vote </th>"
           + "<th> Vote_Emoji_Type </th>"
           + "<th> CM_Role </th>"
           + "<th> Status </th>"
            + "  </tr> "
            + "    </thead> "
            + "      <tbody>";
        if (dtLoan_Committee != null && dtLoan_Committee.Rows.Count > 0)
        {
            foreach (DataRow dtrow in dtLoan_Committee.Rows)
            {
                str_return += "<tr>"
                + "<td> " + dtrow["CM_ID"].ToString() + " </td>"
                + "<td> " + dtrow["Loan_Full_ID"].ToString() + " </td>"
                //+ "<td> " + dtrow["User_ID"].ToString() + " </td>"
                + "<td> " + fc_gen_utils.sess_lookup("user_" + dtrow["User_ID"].ToString(), "-") + " </td>"
                + "<td> " + dtrow["Role"].ToString() + " </td>"
                + "<td> " + dtrow["Added_Date"].ToString() + " </td>"
                + "<td> " + dtrow["Voted_Date"].ToString() + " </td>"
                + "<td> " + dtrow["Vote"].ToString() + " </td>"
                + "<td> " + dtrow["Vote_Emoji_Type"].ToString() + " </td>"
                + "<td> " + dtrow["CM_Role"].ToString() + " </td>"
                + "<td> " + dtrow["Status"].ToString() + " </td>"
                + " </td>"
                + "</tr>";
            }
            str_return += " </tbody> "
           + " </table> "
           + " </div></div></div > ";
        }
        else
        {
            str_return += " <table id='datatable_fixed_column' class='table table-striped table-bordered'>";
            str_return += " </tbody> "
                                   + " </table> "
                                     + "</div></div></div >";
        }
        return str_return;
    }

    public string Get_Loan_Documents(string loan_full_id)
    {
        string stQry = "";
        string str_return = "";
        stQry = @"SELECT LD.*,
                    dbo.GetUserNameByUserID(LD.Request_User_ID) as [Request_User_Name],
                    dbo.GetUserNameByUserID(LD.Upload_User_ID) as [Upload_User_Name]
                    FROM [dbo].[Loan_Document] LD
                    LEFT JOIN Ref_Document_Type RDT On LD.Document_Type_ID = RDT.Document_Type_ID
                    WHERE LD.[Loan_ID] = ( Select LM.Loan_ID from Loan_Master LM Where LM.Loan_Full_ID = '" + loan_full_id + @"') And LD.[Status] = 1";

        DataTable dtLoan_Condition = gen_db_utils.gp_sql_get_datatable(stQry, dbKey);

        str_return = " <div class='panel panel-default'><div class='panel-heading accordion-toggle collapsed' data-toggle='collapse'" +
            "  data-parent='#accordion' data-target='#collapseLoanCondition'>" + Getcolor("Documents", dtLoan_Condition.Rows.Count) + "</div> ";

        str_return += " <div id='collapseLoanCondition' class='panel-collapse collapse'><div class='panel-body' style='overflow-y:auto'>  " +
                        "<table id='datatable_fixed_column' class='table table-striped table-bordered'> "
                         + " <thead> "
                          + " <tr> "
                              + "<th> Loan Document ID </th>"
                              + "<th> Document Name </th>"
                              + "<th> Document Type ID </th>"
                              + "<th> Document Type Level </th>"
                              + "<th> Standard Ind </th>"
                              + "<th> Replication Association Type </th>"
                              + "<th> Replication Association ID </th>"
                              + "<th> Request User ID </th>"
                              + "<th> Request User Name </th>"
                              + "<th> Upload User ID </th>"
                              + "<th> Upload User Name </th>"
                              + "<th> Document Details </th>"
                              + "<th> Custom </th>"
                              + "<th> Status </th>"
                        + "  </tr> "
                        + " </thead> "
                        + "<tbody>";

        if (dtLoan_Condition != null && dtLoan_Condition.Rows.Count > 0)
        {
            foreach (DataRow dtrow in dtLoan_Condition.Rows)
            {
                str_return += "<tr>"
                                + "<td> " + dtrow["Loan_Document_ID"].ToString() + " </td>"
                                + "<td> " + dtrow["Document_Name"].ToString() + " </td>"
                                + "<td> " + dtrow["Document_Type_ID"].ToString() + " </td>"
                                + "<td> " + dtrow["Document_Type_Level"].ToString() + " </td>"
                                + "<td> " + dtrow["Standard_Document_Ind"].ToString() + " </td>"
                                + "<td> " + (dtrow["Replication_Association_Type"].ToString() == string.Empty ? "-" : dtrow["Replication_Association_Type"].ToString()) + " </td>"
                                + "<td> " + (dtrow["Replication_Association_ID"].ToString() == "0" ? "-" : dtrow["Replication_Association_ID"].ToString()) + " </td>"
                                + "<td> " + (dtrow["Request_User_ID"].ToString() == "0" ? "-" : dtrow["Request_User_ID"].ToString()) + " </td>"
                                + "<td> " + (dtrow["Request_User_Name"].ToString() == string.Empty ? "-" : dtrow["Request_User_Name"].ToString()) + " </td>"
                                + "<td> " + (dtrow["Upload_User_ID"].ToString() == "0" ? "-" : dtrow["Upload_User_ID"].ToString()) + " </td>"
                                + "<td> " + (dtrow["Upload_User_Name"].ToString() == string.Empty ? "-" : dtrow["Upload_User_Name"].ToString()) + " </td>"
                                + "<td> " + dtrow["Document_Details"].ToString() + " </td>"
                                + "<td> " + (dtrow["Is_Custom"].ToString() == "0" ? "No" : "Yes") + " </td>"
                                + "<td> " + dtrow["Status"].ToString() + " </td>"
                             + "</tr>";
            }

            str_return += " </tbody> "
                       + " </table> "
                    + " </div>" +
                    "</div>" +
                  "</div > ";
        }
        else
        {
            str_return += " <table id='datatable_fixed_column' class='table table-striped table-bordered'>";
            str_return += "   <tbody> </tbody> "
                         + " </table> "
                       + "</div>" +
                       "</div>" +
                      "</div >";
        }
        return str_return;
    }

    public string Get_Loan_Crop(string loan_full_id)
    {
        string stQry = "";
        string str_return = "";

        stQry = "SELECT Loan_Crop_ID,Loan_Full_ID,Crop_Code,Crop_ID,Crop_Type_Code,Crop_Price,Basic_Adj,Marketing_Adj,Rebate_Adj,Adj_Price,Contract_Qty,Contract_Price,Percent_booked,IsDelete,LC_Status,Acres,Revenue,W_Crop_Yield,LC_Share FROM Loan_Crop  "
                   + "  where Loan_Full_ID = '" + loan_full_id + "'";
        DataTable dtLoan_Crop = gen_db_utils.gp_sql_get_datatable(stQry, dbKey);

        str_return = " <div class='panel panel-default'><div class='panel-heading accordion-toggle collapsed' data-toggle='collapse'" +
      "  data-parent='#accordion' data-target='#collapseLoanCrop'>" + Getcolor("Crop", dtLoan_Crop.Rows.Count) + "</div> ";
        str_return += " <div id='collapseLoanCrop' class='panel-collapse collapse'><div class='panel-body' style='overflow-y:auto'>  <table id='datatable_fixed_column' class='table table-striped table-bordered'> "
          + " <thead> "
           + " <tr> "
           + "<th> Loan_Crop_ID </th>"
           + "<th> Loan_Full_ID </th>"
           + "<th> Crop_Code </th>"
           + "<th> Crop_ID </th>"
           + "<th> Crop_Type_Code </th>"
           + "<th> Crop_Price </th>"
           + "<th> Basic_Adj </th>"
           + "<th> Marketing_Adj </th>"
           + "<th> Rebate_Adj </th>"
           + "<th> Adj_Price </th>"
           + "<th> Contract_Qty </th>"
           + "<th> Contract_Price </th>"
           + "<th> Percent_booked </th>"
           //+ "<th> IsDelete </th>"
           + "<th> LC_Status </th>"
           + "<th> Acres </th>"
           + "<th> Revenue </th>"
           + "<th> W_Crop_Yield </th>"
           + "<th> LC_Share </th>"
            + "  </tr> "
            + "    </thead> "
            + "      <tbody>";
        if (dtLoan_Crop != null && dtLoan_Crop.Rows.Count > 0)
        {
            foreach (DataRow dtrow in dtLoan_Crop.Rows)
            {
                str_return += "<tr>"
                + "<td> " + dtrow["Loan_Crop_ID"].ToString() + " </td>"
                + "<td> " + dtrow["Loan_Full_ID"].ToString() + " </td>"
                + "<td> " + dtrow["Crop_Code"].ToString() + " </td>"
                + "<td> " + dtrow["Crop_ID"].ToString() + " </td>"
                + "<td> " + dtrow["Crop_Type_Code"].ToString() + " </td>"
                + "<td> " + dtrow["Crop_Price"].ToString() + " </td>"
                + "<td> " + dtrow["Basic_Adj"].ToString() + " </td>"
                + "<td> " + dtrow["Marketing_Adj"].ToString() + " </td>"
                + "<td> " + dtrow["Rebate_Adj"].ToString() + " </td>"
                + "<td> " + dtrow["Adj_Price"].ToString() + " </td>"
                + "<td> " + dtrow["Contract_Qty"].ToString() + " </td>"
                + "<td> " + dtrow["Contract_Price"].ToString() + " </td>"
                + "<td> " + dtrow["Percent_booked"].ToString() + " </td>"
                //+ "<td> " + dtrow["IsDelete"].ToString() + " </td>"
                + "<td> " + dtrow["LC_Status"].ToString() + " </td>"
                + "<td> " + dtrow["Acres"].ToString() + " </td>"
                + "<td> " + dtrow["Revenue"].ToString() + " </td>"
                + "<td> " + dtrow["W_Crop_Yield"].ToString() + " </td>"
                + "<td> " + dtrow["LC_Share"].ToString() + " </td>"
                + " </td>"
                + "</tr>";
            }
            str_return += " </tbody> "
           + " </table> "
           + " </div></div></div > ";
        }
        else
        {
            str_return += " <table id='datatable_fixed_column' class='table table-striped table-bordered'>";
            str_return += " </tbody> "
                                   + " </table> "
                                     + "</div></div></div >";
        }
        return str_return;
    }

    public string Get_Loan_Crop_Practice(string loan_full_id)
    {
        string stQry = "";
        string str_return = "";

        stQry = "SELECT Loan_Crop_Practice_ID,Loan_Full_ID,Crop_Practice_ID,LCP_APH,LCP_Acres,LCP_ARM_Budget,LCP_Distributer_Budget,LCP_Third_Party_Budget,Market_Value,Disc_Market_Value,LCP_Notes,LCP_Status,isDelete FROM Loan_Crop_Practice  "
                   + "  where Loan_Full_ID = '" + loan_full_id + "'";

        DataTable dtLoan_Crop_Practice = gen_db_utils.gp_sql_get_datatable(stQry, dbKey);

        str_return = " <div class='panel panel-default'><div class='panel-heading accordion-toggle collapsed' data-toggle='collapse'" +
            "  data-parent='#accordion' data-target='#collapseLoanCropPractice'>" + Getcolor("Crop Practice", dtLoan_Crop_Practice.Rows.Count) + "</div> ";
        str_return += " <div id='collapseLoanCropPractice' class='panel-collapse collapse'><div class='panel-body' style='overflow-y:auto'>  <table id='datatable_fixed_column' class='table table-striped table-bordered'> "
                  + " <thead> "
                   + " <tr> "
                   + "<th> Loan_Crop_Practice_ID </th>"
                   + "<th> Loan_Full_ID </th>"
                   + "<th> Crop_Practice_ID </th>"
                   + "<th> LCP_APH </th>"
                   + "<th> LCP_Acres </th>"
                   + "<th> LCP_ARM_Budget </th>"
                   + "<th> LCP_Distributer_Budget </th>"
                   + "<th> LCP_Third_Party_Budget </th>"
                   + "<th> Market_Value </th>"
                   + "<th> Disc_Market_Value </th>"
                   + "<th> LCP_Notes </th>"
                   + "<th> LCP_Status </th>"
                    //+ "<th> isDelete </th>"
                    + "  </tr> "
                    + "    </thead> "
                    + "      <tbody>";
        if (dtLoan_Crop_Practice != null && dtLoan_Crop_Practice.Rows.Count > 0)
        {
            foreach (DataRow dtrow in dtLoan_Crop_Practice.Rows)
            {
                str_return += "<tr>"
                + "<td> " + dtrow["Loan_Crop_Practice_ID"].ToString() + " </td>"
                + "<td> " + dtrow["Loan_Full_ID"].ToString() + " </td>"
                + "<td> " + fc_gen_utils.sess_lookup("cp_" + dtrow["Crop_Practice_ID"].ToString(), "-") + " </td>"
                + "<td> " + dtrow["LCP_APH"].ToString() + " </td>"
                + "<td> " + dtrow["LCP_Acres"].ToString() + " </td>"
                + "<td> " + dtrow["LCP_ARM_Budget"].ToString() + " </td>"
                + "<td> " + dtrow["LCP_Distributer_Budget"].ToString() + " </td>"
                + "<td> " + dtrow["LCP_Third_Party_Budget"].ToString() + " </td>"
                + "<td> " + dtrow["Market_Value"].ToString() + " </td>"
                + "<td> " + dtrow["Disc_Market_Value"].ToString() + " </td>"
                + "<td> " + dtrow["LCP_Notes"].ToString() + " </td>"
                + "<td> " + dtrow["LCP_Status"].ToString() + " </td>"
                //+ "<td> " + dtrow["isDelete"].ToString() + " </td>"
                + " </td>"
                + "</tr>";
            }
            str_return += " </tbody> "
           + " </table> "
           + " </div></div></div > ";
        }
        else
        {
            str_return += " <table id='datatable_fixed_column' class='table table-striped table-bordered'>";

            str_return += " </tbody> "
                                   + " </table> "
                                     + "</div></div></div >";
        }

        return str_return;
    }

    public string Get_Loan_Crop_Unit(string loan_full_id)
    {
        string stQry = "";
        string str_return = "";

        stQry = "SELECT Loan_CU_ID,Loan_ID,Farm_ID,Loan_Full_ID,CU_Acres,Crop_Code,Crop_Type_Code,Crop_Practice_Type_Code,Crop_Practice_ID,Z_Price,Crop_Full_Key,Ins_Value,Disc_Ins_value,Mkt_Value,Disc_Mkt_Value,CEI_Value,Disc_CEI_Value,Z_Basis_Adj,Z_Marketing_Adj,Z_Rebate_Adj,Z_Adj_Price,CU_APH,Ins_APH,Booking_Ind,Status,IsDelete FROM Loan_Crop_Unit  "
                   + "  where Loan_Full_ID = '" + loan_full_id + "'";
        DataTable dtLoan_Crop_Unit = gen_db_utils.gp_sql_get_datatable(stQry, dbKey);
        str_return = " <div class='panel panel-default'><div class='panel-heading accordion-toggle collapsed' data-toggle='collapse'" +
           "  data-parent='#accordion' data-target='#collapseLoanCropUnit'>" + Getcolor("Crop Unit", dtLoan_Crop_Unit.Rows.Count) + "</div> ";

        str_return += " <div id='collapseLoanCropUnit' class='panel-collapse collapse'><div class='panel-body' style='overflow-y:auto'>  <table id='datatable_fixed_column' class='table table-striped table-bordered'> "
          + " <thead> "
           + " <tr> "
           + "<th> Loan_CU_ID </th>"
           + "<th> Loan_ID </th>"
           + "<th> Farm_ID </th>"
           + "<th> Loan_Full_ID </th>"
           + "<th> CU_Acres </th>"
           + "<th> Crop_Code </th>"
           + "<th> Crop_Type_Code </th>"
           + "<th> Crop_Practice_Type_Code </th>"
           + "<th> Crop_Practice_ID </th>"
           + "<th> Z_Price </th>"
           + "<th> Crop_Full_Key </th>"
           + "<th> Ins_Value </th>"
           + "<th> Disc_Ins_value </th>"
           + "<th> Mkt_Value </th>"
           + "<th> Disc_Mkt_Value </th>"
           + "<th> CEI_Value </th>"
           + "<th> Disc_CEI_Value </th>"
           + "<th> Z_Basis_Adj </th>"
           + "<th> Z_Marketing_Adj </th>"
           + "<th> Z_Rebate_Adj </th>"
           + "<th> Z_Adj_Price </th>"
           + "<th> CU_APH </th>"
           + "<th> Ins_APH </th>"
           + "<th> Booking_Ind </th>"
           + "<th> Status </th>"
            //+ "<th> IsDelete </th>"
            + "  </tr> "
            + "    </thead> "
            + "      <tbody>";
        if (dtLoan_Crop_Unit != null && dtLoan_Crop_Unit.Rows.Count > 0)
        {
            foreach (DataRow dtrow in dtLoan_Crop_Unit.Rows)
            {
                str_return += "<tr>"
                + "<td> " + dtrow["Loan_CU_ID"].ToString() + " </td>"
                + "<td> " + dtrow["Loan_ID"].ToString() + " </td>"
                + "<td> " + dtrow["Farm_ID"].ToString() + " </td>"
                + "<td> " + dtrow["Loan_Full_ID"].ToString() + " </td>"
                + "<td> " + dtrow["CU_Acres"].ToString() + " </td>"
                + "<td> " + dtrow["Crop_Code"].ToString() + " </td>"
                + "<td> " + dtrow["Crop_Type_Code"].ToString() + " </td>"
                + "<td> " + dtrow["Crop_Practice_Type_Code"].ToString() + " </td>"
                + "<td> " + fc_gen_utils.sess_lookup("cp_" + dtrow["Crop_Practice_ID"].ToString(), "-") + " </td>"
                + "<td> " + dtrow["Z_Price"].ToString() + " </td>"
                + "<td> " + dtrow["Crop_Full_Key"].ToString() + " </td>"
                + "<td> " + dtrow["Ins_Value"].ToString() + " </td>"
                + "<td> " + dtrow["Disc_Ins_value"].ToString() + " </td>"
                + "<td> " + dtrow["Mkt_Value"].ToString() + " </td>"
                + "<td> " + dtrow["Disc_Mkt_Value"].ToString() + " </td>"
                + "<td> " + dtrow["CEI_Value"].ToString() + " </td>"
                + "<td> " + dtrow["Disc_CEI_Value"].ToString() + " </td>"
                + "<td> " + dtrow["Z_Basis_Adj"].ToString() + " </td>"
                + "<td> " + dtrow["Z_Marketing_Adj"].ToString() + " </td>"
                + "<td> " + dtrow["Z_Rebate_Adj"].ToString() + " </td>"
                + "<td> " + dtrow["Z_Adj_Price"].ToString() + " </td>"
                + "<td> " + dtrow["CU_APH"].ToString() + " </td>"
                + "<td> " + dtrow["Ins_APH"].ToString() + " </td>"
                + "<td> " + dtrow["Booking_Ind"].ToString() + " </td>"
                + "<td> " + dtrow["Status"].ToString() + " </td>"
                //+ "<td> " + dtrow["IsDelete"].ToString() + " </td>"
                + " </td>"
                + "</tr>";
            }
            str_return += " </tbody> "
           + " </table> "
           + " </div></div></div > ";
        }
        else
        {
            str_return += " <table id='datatable_fixed_column' class='table table-striped table-bordered'>";
            str_return += " </tbody> "
                                   + " </table> "
                                     + "</div></div></div >";
        }
        return str_return;
    }

    public string Get_Loan_Exception(string loan_full_id)
    {
        string stQry = "";
        string str_return = "";

        stQry = @"SELECT
	                LE.*,
                    LM.Region_ID,
	                REX.Sort_Order,
                    REX.Custom_Ind,
                    dbo.GetUserNameByUserID(LE.Support_User_ID_1) as [Support_User_Name_1],
                    dbo.GetUserNameByUserID(LE.Support_User_ID_2) as [Support_User_Name_2],
                    dbo.GetUserNameByUserID(LE.Support_User_ID_3) as [Support_User_Name_3],
                    dbo.GetUserNameByUserID(LE.Support_User_ID_4) as [Support_User_Name_4]
                FROM [dbo].[Loan_Exceptions] LE
                INNER JOIN Ref_Exception REX On REX.Exception_ID = LE.Exception_ID
                INNER JOIN Loan_Master LM On LM.Loan_Full_ID = LE.Loan_Full_Id
                WHERE LE.LOAN_FULL_ID = '" + loan_full_id + @"' AND LE.STATUS = 1";

        DataTable dtLoan_Exception = gen_db_utils.gp_sql_get_datatable(stQry, dbKey);

        str_return = " <div class='panel panel-default'><div class='panel-heading accordion-toggle collapsed' data-toggle='collapse'" +
        "  data-parent='#accordion' data-target='#collapseLoanException'>" + Getcolor("Exception", dtLoan_Exception.Rows.Count) + "</div> ";

        str_return += " <div id='collapseLoanException' class='panel-collapse collapse'>" +
                            "<div class='panel-body exception'>  " +
                                "<table id='datatable_fixed_column' class='table table-striped table-bordered'> "
                                  + " <thead> "
                                   + " <tr> "
                                       + "<th> Exception ID </th>"
                                       + "<th> Exception Text </th>"
                                       + "<th> Exception Level </th>"
                                       + "<th> Tab ID </th>"
                                       + "<th> Chev. ID </th>"
                                       + "<th> Mit. Ind </th>"
                                       + "<th> Mitigation Text </th>"
                                       + "<th> Support Ind </th>"
                                       + "<th style='width: 150px;'> Support Role 1 </th>"
                                       + "<th style='width: 150px;'> Support Role 2 </th>"
                                       + "<th style='width: 150px;'> Support Role 3 </th>"
                                       + "<th style='width: 150px;'> Support Role 4 </th>"
                                       + "<th> Document Type ID </th>"
                                       + "<th> Sort Order </th>"
                                    + "  </tr> "
                                    + " </thead> "
                                    + "<tbody>";

        if (dtLoan_Exception != null && dtLoan_Exception.Rows.Count > 0)
        {
            foreach (DataRow dtrow in dtLoan_Exception.Rows)
            {
                str_return += "<tr>"
                                + "<td> " + dtrow["Exception_ID"].ToString() + " </td>"
                                + "<td> " + dtrow["Exception_ID_Text"].ToString() + " </td>"
                                + "<td> " + dtrow["Exception_ID_Level"].ToString() + " </td>"
                                + "<td> " + dtrow["Tab_ID"].ToString() + " </td>"
                                + "<td> " + dtrow["Chevron_ID"].ToString() + " </td>"
                                + "<td> " + dtrow["Mitigation_Ind"].ToString() + " </td>"
                                + "<td> " + dtrow["Mitigation_Text"].ToString() + " </td>"
                                + "<td> " + dtrow["Support_Ind"].ToString() + " </td>"
                                + "<td> " + Support_UI(dtrow, dtrow["Support_Role_Type_Code_1"].ToString(), dtrow["Support_User_Name_1"].ToString(), dtrow["Support_Date_Time_1"].ToString(), 1) + " </td>"
                                + "<td> " + Support_UI(dtrow, dtrow["Support_Role_Type_Code_2"].ToString(), dtrow["Support_User_Name_2"].ToString(), dtrow["Support_Date_Time_2"].ToString(), 2) + " </td>"
                                + "<td> " + Support_UI(dtrow, dtrow["Support_Role_Type_Code_3"].ToString(), dtrow["Support_User_Name_3"].ToString(), dtrow["Support_Date_Time_3"].ToString(), 3) + " </td>"
                                + "<td> " + Support_UI(dtrow, dtrow["Support_Role_Type_Code_4"].ToString(), dtrow["Support_User_Name_4"].ToString(), dtrow["Support_Date_Time_4"].ToString(), 4) + " </td>"
                                + "<td> " + dtrow["Document_Type_ID"].ToString() + " </td>"
                                + "<td> " + dtrow["Sort_Order"].ToString() + " </td>"
                            + "</tr>";
            }
            str_return += " </tbody> "
                    + " </table> "
                    + "</div>" +
                    "</div>" +
                  "</div > ";
        }
        else
        {
            str_return += " <table id='datatable_fixed_column' class='table table-striped table-bordered'>";

            str_return += " </tbody> "
                                   + " </table> "
                                     + "</div></div></div >";
        }

        return str_return;
    }

    private string Support_UI(DataRow dtrow, string role, string user_name, string datetime, int role_type)
    {
        string UI = string.Empty;

        if (!string.IsNullOrEmpty(role))
        {
            UI += "<div class='support-ui'>";
            UI += "<p><strong>Role</strong>: </br>" + role + "</p>";

            if (!string.IsNullOrEmpty(user_name))
            {
                UI += "<p><strong>Approved By: </strong> </br>" + user_name + "</p>";
                UI += "<p><strong>Approved On: </strong> </br>" + datetime.ToString() + "</p>";
            }
            else
            {
                UI += @"<a href ='javascript:Approve_Support(" + role_type + "," + dtrow["Loan_Exception_ID"] + "," + dtrow["Region_ID"].ToString() + ")' > Approve </a> ";
            }

            UI += "</div>";
        }

        return UI;
    }

    [WebMethod]
    public static void Approve_Support(string Role_Type_Code, string Loan_Exception_ID, string Region_Id)
    {
        try
        {
            string sql = "SELECT TOP 1 * From Loan_Exceptions Where Loan_Exception_ID = " + Loan_Exception_ID;
            DataTable dtLoan_Exception = gen_db_utils.base_sql_get_datatable(sql, dbKey);
            var exception = dtLoan_Exception.Rows[0];

            var role = exception["Support_Role_Type_Code_" + Role_Type_Code].ToString();

            if (role.Contains("!"))
            {
                role = role.Replace("!", "").Trim();

                sql = @"Select TOP 1 * from Users U
                        JOIN Job_Title JT on U.Job_Title_ID = JT.Job_Title_ID
                        JOIN Ref_Office RO on U.Office_Id = RO.Office_Id
                    Where JT.Job_Title_Code = '" + role + @"' AND RO.Region_Id = '" + Region_Id + @"'
                    ORDER BY NEWID()";
            }
            else
            {
                sql = "Select TOP 1 * from Users U JOIN Job_Title JT on U.Job_Title_ID = JT.Job_Title_ID Where JT.Job_Title_Code = '" + role + "' ORDER BY NEWID()";
            }

            var dt = gen_db_utils.base_sql_get_datatable(sql, dbKey);
            DataRow userRow = null;

            if(dt.Rows.Count == 0)
            {
                sql = "Select TOP 1 * from Users U JOIN Job_Title JT on U.Job_Title_ID = JT.Job_Title_ID Where JT.Job_Title_Code = '" + role + "' ORDER BY NEWID()";
                dt = gen_db_utils.base_sql_get_datatable(sql, dbKey);
            }

            userRow = dt.Rows[0];

            switch (Role_Type_Code)
            {
                case "1":
                    sql = "UPDATE Loan_Exceptions " +
                            "SET Support_Date_Time_1 = GETDATE(), " +
                            "Support_User_ID_1 = '" + userRow["UserID"].ToString() + "' " +
                        "Where Loan_Exception_ID = " + Loan_Exception_ID;
                    break;

                case "2":
                    sql = "UPDATE Loan_Exceptions " +
                            "SET Support_Date_Time_2 = GETDATE(), " +
                            "Support_User_ID_2 = '" + userRow["UserID"].ToString() + "' " +
                        "Where Loan_Exception_ID = " + Loan_Exception_ID;
                    break;

                case "3":
                    sql = "UPDATE Loan_Exceptions " +
                            "SET Support_Date_Time_3 = GETDATE(), " +
                            "Support_User_ID_3 = '" + userRow["UserID"].ToString() + "' " +
                        "Where Loan_Exception_ID = " + Loan_Exception_ID;
                    break;

                case "4":
                    sql = "UPDATE Loan_Exceptions " +
                            "SET Support_Date_Time_4 = GETDATE(), " +
                            "Support_User_ID_4 = '" + userRow["UserID"].ToString() + "' " +
                        "Where Loan_Exception_ID = " + Loan_Exception_ID;
                    break;
            }

            gen_db_utils.base_sql_execute(sql, dbKey);
        }
        catch (Exception ex)
        {
        }
    }

    public string Get_Loan_Farm(string loan_full_id)
    {
        string stQry = "";
        string str_return = "";

        stQry = "SELECT Farm_ID,Loan_Full_ID,Farm_State_ID,Farm_County_ID,Percent_Prod,Landowner,FSN,Section,Rated,Owned,Share_Rent,Rent_UOM,Cash_Rent_Total,Cash_Rent_Per_Acre,Cash_Rent_Due_Date,Cash_Rent_Paid,Permission_To_Insure,Cash_Rent_Waived,Cash_Rent_Waived_Amount,Irr_Acres,NI_Acres,Crop_share_Detail_Indicator,Status,IsDelete FROM Loan_Farm "
                   + "  where Loan_Full_ID = '" + loan_full_id + "'";
        DataTable dtLoan_Farm = gen_db_utils.gp_sql_get_datatable(stQry, dbKey);
        str_return = " <div class='panel panel-default'><div class='panel-heading accordion-toggle collapsed' data-toggle='collapse'" +
           "  data-parent='#accordion' data-target='#collapseLoanFarm'>" + Getcolor("Farm", dtLoan_Farm.Rows.Count) + "</div> ";
        str_return += " <div id='collapseLoanFarm' class='panel-collapse collapse'><div class='panel-body' style='overflow-y: auto;'>  <table id='datatable_fixed_column' class='table table-striped table-bordered'> "
         + " <thead> "
          + " <tr> "
              + "<th> Farm_ID </th>"
              + "<th> Loan_Full_ID </th>"
              + "<th> Farm_State_ID </th>"
              + "<th> Farm_County </th>"
              + "<th> Percent_Prod </th>"
              + "<th> Landowner </th>"
              + "<th> FSN </th>"
              + "<th> Section </th>"
              + "<th> Rated </th>"
              + "<th> Owned </th>"
              + "<th> Share_Rent </th>"
              + "<th> Rent_UOM </th>"
              + "<th> Cash_Rent_Total </th>"
              + "<th> Cash_Rent_Per_Acre </th>"
              + "<th> Cash_Rent_Due_Date </th>"
              + "<th> Cash_Rent_Paid </th>"
              + "<th> Permission_To_Insure </th>"
              + "<th> Cash_Rent_Waived </th>"
              + "<th> Cash_Rent_Waived_Amount </th>"
              + "<th> Irr_Acres </th>"
              + "<th> NI_Acres </th>"
              + "<th> Crop_share_Detail_Indicator </th>"
              + "<th> Status </th>"
           //+ "<th> IsDelete </th>"
           + "  </tr> "
           + "  </thead> "
           + "<tbody>";
        if (dtLoan_Farm != null && dtLoan_Farm.Rows.Count > 0)
        {
            foreach (DataRow dtrow in dtLoan_Farm.Rows)
            {
                str_return += "<tr>"
                                + "<td> " + dtrow["Farm_ID"].ToString() + " </td>"
                                + "<td> " + dtrow["Loan_Full_ID"].ToString() + " </td>"
                                + "<td> " + dtrow["Farm_State_ID"].ToString() + "|" + fc_gen_utils.sess_lookup("st_" + dtrow["Farm_State_ID"].ToString(), "-") + " </td>"
                                //+ "<td> " + dtrow["Farm_County_ID"].ToString() + " </td>"
                                + "<td> " + fc_gen_utils.sess_lookup("county_" + dtrow["Farm_County_ID"].ToString(), "-") + " </td>"
                                + "<td> " + dtrow["Percent_Prod"].ToString() + " </td>"
                                + "<td> " + dtrow["Landowner"].ToString() + " </td>"
                                + "<td> " + dtrow["FSN"].ToString() + " </td>"
                                + "<td> " + dtrow["Section"].ToString() + " </td>"
                                + "<td> " + dtrow["Rated"].ToString() + " </td>"
                                + "<td> " + dtrow["Owned"].ToString() + " </td>"
                                + "<td> " + dtrow["Share_Rent"].ToString() + " </td>"
                                + "<td> " + dtrow["Rent_UOM"].ToString() + " </td>"
                                + "<td> " + dtrow["Cash_Rent_Total"].ToString() + " </td>"
                                + "<td> " + dtrow["Cash_Rent_Per_Acre"].ToString() + " </td>"
                                + "<td> " + dtrow["Cash_Rent_Due_Date"].ToString() + " </td>"
                                + "<td> " + dtrow["Cash_Rent_Paid"].ToString() + " </td>"
                                + "<td> " + dtrow["Permission_To_Insure"].ToString() + " </td>"
                                + "<td> " + dtrow["Cash_Rent_Waived"].ToString() + " </td>"
                                + "<td> " + dtrow["Cash_Rent_Waived_Amount"].ToString() + " </td>"
                                + "<td> " + dtrow["Irr_Acres"].ToString() + " </td>"
                                + "<td> " + dtrow["NI_Acres"].ToString() + " </td>"
                                + "<td> " + dtrow["Crop_share_Detail_Indicator"].ToString() + " </td>"
                                + "<td> " + dtrow["Status"].ToString() + " </td>"
                            //+ "<td> " + dtrow["IsDelete"].ToString() + " </td>"
                            + " </td>"
                            + "</tr>";
            }
            str_return += " </tbody> "
           + " </table> "
           + " </div></div></div > ";
        }
        else
        {
            str_return += " <table id='datatable_fixed_column' class='table table-striped table-bordered'>";

            str_return += " </tbody> "
                                   + " </table> "
                                     + "</div></div></div >";
        }

        return str_return;
    }

    public string Get_Loan_Marketing_Contract(string loan_full_id)
    {
        string stQry = "";
        string str_return = "";

        stQry = "SELECT Contract_ID,Z_Loan_ID,Z_Loan_Seq_Num,Loan_Full_ID,Category,Crop_Code,Crop_Type_Code,Assoc_Type_Code,Assoc_ID,Quantity,Price,Market_Value,Contract_Per,UoM,Description_Text,Status FROM Loan_Marketing_Contract "
                   + "  where Loan_Full_ID = '" + loan_full_id + "'";

        DataTable dtLoan_Marketing_Contract = gen_db_utils.gp_sql_get_datatable(stQry, dbKey);
        str_return = " <div class='panel panel-default'><div class='panel-heading accordion-toggle collapsed' data-toggle='collapse'" +
    "  data-parent='#accordion' data-target='#collapseLoanMarketingContract'>" + Getcolor("Marketing Contract", dtLoan_Marketing_Contract.Rows.Count) + "</div> ";
        str_return += " <div id='collapseLoanMarketingContract' class='panel-collapse collapse'><div class='panel-body' style='overflow-y:auto'>  <table id='datatable_fixed_column' class='table table-striped table-bordered'> "
       + " <thead> "
        + " <tr> "
        + "<th> Contract_ID </th>"
        + "<th> Z_Loan_ID </th>"
        + "<th> Z_Loan_Seq_Num </th>"
        + "<th> Loan_Full_ID </th>"
        + "<th> Category </th>"
        + "<th> Crop_Code </th>"
        + "<th> Crop_Type_Code </th>"
        + "<th> Assoc_Type_Code </th>"
        + "<th> Assoc_ID </th>"
        + "<th> Quantity </th>"
        + "<th> Price </th>"
        + "<th> Market_Value </th>"
        + "<th> Contract_Per </th>"
        + "<th> UoM </th>"
        + "<th> Description_Text </th>"
        + "<th> Status </th>"
         + "  </tr> "
         + "    </thead> "
         + "      <tbody>";
        if (dtLoan_Marketing_Contract != null && dtLoan_Marketing_Contract.Rows.Count > 0)
        {
            foreach (DataRow dtrow in dtLoan_Marketing_Contract.Rows)
            {
                str_return += "<tr>"
                + "<td> " + dtrow["Contract_ID"].ToString() + " </td>"
                + "<td> " + dtrow["Z_Loan_ID"].ToString() + " </td>"
                + "<td> " + dtrow["Z_Loan_Seq_Num"].ToString() + " </td>"
                + "<td> " + dtrow["Loan_Full_ID"].ToString() + " </td>"
                + "<td> " + dtrow["Category"].ToString() + " </td>"
                + "<td> " + dtrow["Crop_Code"].ToString() + " </td>"
                + "<td> " + dtrow["Crop_Type_Code"].ToString() + " </td>"
                + "<td> " + dtrow["Assoc_Type_Code"].ToString() + " </td>"
                + "<td> " + dtrow["Assoc_ID"].ToString() + " </td>"
                + "<td> " + dtrow["Quantity"].ToString() + " </td>"
                + "<td> " + dtrow["Price"].ToString() + " </td>"
                + "<td> " + dtrow["Market_Value"].ToString() + " </td>"
                + "<td> " + dtrow["Contract_Per"].ToString() + " </td>"
                + "<td> " + dtrow["UoM"].ToString() + " </td>"
                + "<td> " + dtrow["Description_Text"].ToString() + " </td>"
                + "<td> " + dtrow["Status"].ToString() + " </td>"
                + " </td>"
                + "</tr>";
            }
            str_return += " </tbody> "
           + " </table> "
           + " </div></div></div > ";
        }
        else
        {
            str_return += " <table id='datatable_fixed_column' class='table table-striped table-bordered'>";

            str_return += " </tbody> "
                                   + " </table> "
                                     + "</div></div></div >";
        }

        return str_return;
    }

    public string Get_Loan_Other_Income(string loan_full_id)
    {
        string stQry = "";
        string str_return = "";

        stQry = "SELECT Loan_Other_Income_ID,Loan_Full_ID,Other_Income_ID,Other_Description_Text,Amount,Status FROM Loan_Other_Income  "
                   + "  where Loan_Full_ID = '" + loan_full_id + "'";
        DataTable dtLoan_Other_Income = gen_db_utils.gp_sql_get_datatable(stQry, dbKey);
        str_return = " <div class='panel panel-default'><div class='panel-heading accordion-toggle collapsed' data-toggle='collapse'" +
    "  data-parent='#accordion' data-target='#collapseLoanOtherIncome'>" + Getcolor("Other Income", dtLoan_Other_Income.Rows.Count) + "</div> ";
        str_return += " <div id='collapseLoanOtherIncome' class='panel-collapse collapse'><div class='panel-body' style='overflow-y:auto'>  <table id='datatable_fixed_column' class='table table-striped table-bordered'> "
           + " <thead> "
            + " <tr> "
            + "<th> Loan Full ID </th>"
            + "<th> Other Income ID </th>"
            + "<th> Other Description Text </th>"
            + "<th> Amount </th>"
            + "<th> Status </th>"
             + "  </tr> "
             + "    </thead> "
             + "      <tbody>";
        if (dtLoan_Other_Income != null && dtLoan_Other_Income.Rows.Count > 0)
        {
            foreach (DataRow dtrow in dtLoan_Other_Income.Rows)
            {
                str_return += "<tr>"
                + "<td> " + dtrow["Loan_Full_ID"].ToString() + " </td>"
                + "<td> " + dtrow["Other_Income_ID"].ToString() + " </td>"
                + "<td> " + dtrow["Other_Description_Text"].ToString() + " </td>"
                + "<td> " + dtrow["Amount"].ToString() + " </td>"
                + "<td> " + dtrow["Status"].ToString() + " </td>"
                + " </td>"
                + "</tr>";
            }
            str_return += " </tbody> "
           + " </table> "
           + " </div></div></div > ";
        }
        else
        {
            str_return += " <table id='datatable_fixed_column' class='table table-striped table-bordered'>";

            str_return += " </tbody> "
                                   + " </table> "
                                     + "</div></div></div >";
        }

        return str_return;
    }

    public string Get_Loan_Policies1(string loan_full_id)
    {
        string stQry = "";
        string str_return = "";

        stQry = "SELECT Policy_id,Agent_Id,Agency_Id,State_Id,Crop_Practice_Id,County_Id,ProposedAIP,Loan_Full_Id,Rated,HighlyRated,Level,Price,Premium,Unit,MPCI_Subplan,HasSecondaryPlans,IsDeleted,CreatedDate,ModifiedDate FROM Loan_Policies "
                   + "  where Loan_Full_ID = '" + loan_full_id + "'";

        DataTable dtLoan_Policies = gen_db_utils.gp_sql_get_datatable(stQry, dbKey);

        str_return = " <div class='panel panel-default'><div class='panel-heading accordion-toggle collapsed' data-toggle='collapse'" +
            "  data-parent='#accordion' data-target='#collapseLoanPolicies'>" + Getcolor("Policies", dtLoan_Policies.Rows.Count) + "</div> ";
        str_return += " <div id='collapseLoanPolicies' class='panel-collapse collapse'><div class='panel-body' style='overflow-y:auto'>  <table id='datatable_fixed_column' class='table table-striped table-bordered'> "
                                   + " <thead> "
                                    + " <tr> "
                                    + "<th> Policy_id </th>"
                                    + "<th> Agent_Id </th>"
                                    + "<th> Agency_Id </th>"
                                    + "<th> State </th>"
                                    + "<th> Crop_Practice_Id </th>"
                                    + "<th> County </th>"
                                    + "<th> ProposedAIP </th>"
                                    + "<th> Loan_Full_Id </th>"
                                    + "<th> Rated </th>"
                                    + "<th> HighlyRated </th>"
                                    + "<th> Level </th>"
                                    + "<th> Price </th>"
                                    + "<th> Premium </th>"
                                    + "<th> Unit </th>"
                                    + "<th> MPCI_Subplan </th>"
                                    + "<th> HasSecondaryPlans </th>"
                                    + "<th> IsDeleted </th>"
                                    + "<th> CreatedDate </th>"
                                    + "<th> ModifiedDate </th>"
                                     + "  </tr> "
                                     + "    </thead> "
                                     + "      <tbody>";
        if (dtLoan_Policies != null && dtLoan_Policies.Rows.Count > 0)
        {
            foreach (DataRow dtrow in dtLoan_Policies.Rows)
            {
                str_return += "<tr>"
                + "<td> " + dtrow["Policy_id"].ToString() + " </td>"
                + "<td> " + dtrow["Agent_Id"].ToString() + " </td>"
                + "<td> " + dtrow["Agency_Id"].ToString() + " </td>"
                + "<td> " + dtrow["State_Id"].ToString() + "|" + fc_gen_utils.sess_lookup("st_" + dtrow["State_Id"].ToString(), "-") + " </td>"
                + "<td> " + fc_gen_utils.sess_lookup("cp_" + dtrow["Crop_Practice_Id"].ToString(), "-") + " </td>"
                + "<td> " + fc_gen_utils.sess_lookup("county_" + dtrow["County_Id"].ToString(), "-") + " </td>"
                + "<td> " + dtrow["ProposedAIP"].ToString() + " </td>"
                + "<td> " + dtrow["Loan_Full_Id"].ToString() + " </td>"
                + "<td> " + dtrow["Rated"].ToString() + " </td>"
                + "<td> " + dtrow["HighlyRated"].ToString() + " </td>"
                + "<td> " + dtrow["Level"].ToString() + " </td>"
                + "<td> " + dtrow["Price"].ToString() + " </td>"
                + "<td> " + dtrow["Premium"].ToString() + " </td>"
                + "<td> " + dtrow["Unit"].ToString() + " </td>"
                + "<td> " + dtrow["MPCI_Subplan"].ToString() + " </td>"
                + "<td> " + dtrow["HasSecondaryPlans"].ToString() + " </td>"
                + "<td> " + dtrow["IsDeleted"].ToString() + " </td>"
                + "<td> " + dtrow["CreatedDate"].ToString() + " </td>"
                + "<td> " + dtrow["ModifiedDate"].ToString() + " </td>"
                + " </td>"
                + "</tr>";

                string strSubpoliciesQry = "SELECT SubPolicy_Id,FK_Policy_Id,Upper_Limit,Lower_Limit,lsp.Agent_Id,lsp.Agency_Id,Aip,Price_Pct,Yield_Pct,Ins_SubType,Ins_Type,lsp.Premium,Prot_Factor,Yield,Wind,Liability,FCMC,lsp.[Unit],lsp.[Option],Deduct,CreatedOn,ModifiedOn "
                + "FROM Loan_Sub_Policies lsp join Loan_Policies lp on lsp.fk_policy_id = lp.policy_id"
                + "  where Loan_Full_ID = '" + loan_full_id + "' and FK_Policy_Id= '" + dtrow["Policy_id"].ToString() + "' ";

                DataTable dtLoan_Sub_Policies = gen_db_utils.gp_sql_get_datatable(strSubpoliciesQry, dbKey);

                if (dtLoan_Sub_Policies != null && dtLoan_Sub_Policies.Rows.Count > 0)
                {
                    foreach (DataRow dtrow1 in dtLoan_Sub_Policies.Rows)
                    {
                        //+ "FK_Policy_Id :" + dtrow1["FK_Policy_Id"].ToString()
                        //+ "FK_Policy_Id :" + dtrow1["Agent_Id"].ToString()
                        //+ "FK_Policy_Id :" + dtrow1["Agency_Id"].ToString() + " </td>"

                        str_return += "<tr><td></td>"
                        + "<td colspan='3'> <b>SubPolicy_Id:</b> " + dtrow1["SubPolicy_Id"].ToString() + "<br>"
                        + " Upper_Limit :" + dtrow1["Upper_Limit"].ToString() + "<br>"
                        + " Lower_Limit :" + dtrow1["Lower_Limit"].ToString() + " </td>"

                        + "<td colspan='2'> Aip:" + dtrow1["Aip"].ToString() + "  <br> "
                        + "Price_Pct: " + dtrow1["Price_Pct"].ToString() + "<br>"
                         + " Yield_Pct :" + dtrow1["Yield_Pct"].ToString() + " </td>"

                         + "<td colspan='3'> Ins_SubType :" + dtrow1["Ins_SubType"].ToString() + "<br>"
                         + " Ins_Type :" + dtrow1["Ins_Type"].ToString() + "<br>"
                        + " Premium: " + dtrow1["Premium"].ToString() + " </td>"

                        + "<td colspan='3'> Prot_Factor :" + dtrow1["Prot_Factor"].ToString() + "<br>"
                        + " Yield :" + dtrow1["Yield"].ToString() + "<br>"
                        + " Wind :" + dtrow1["Wind"].ToString() + " </td>"

                        + "<td colspan='3'> Liability :" + dtrow1["Liability"].ToString() + "<br>"
                        + " FCMC:" + dtrow1["FCMC"].ToString() + "<br>"
                        + " Deduct :" + dtrow1["Deduct"].ToString() + " </td>"

                       + "<td colspan='4'> CreatedOn :" + dtrow1["CreatedOn"].ToString() + "<br>"
                        + " ModifiedOn :" + dtrow1["ModifiedOn"].ToString()
                        + " </td>"
                        + "</tr>";
                    }
                }
            }
            str_return += " </tbody> "
           + " </table> "
           + " </div></div></div > ";
        }
        else
        {
            str_return += " <table id='datatable_fixed_column' class='table table-striped table-bordered'>";

            str_return += " </tbody> "
                                   + " </table> "
                                     + "</div></div></div >";
        }

        return str_return;
    }

    public string Get_Loan_Policies(string loan_full_id)
    {
        string stQry = "";
        string str_return = "";
        stQry = "select distinct loan_full_id,State_ID,County_ID,Crop_Code,Irr_Practice_Type_Code,Crop_Practice_Type_Code from Loan_Policy "
               + "  where Loan_Full_ID = '" + loan_full_id + "'";

        DataTable dtLoan_Policies = gen_db_utils.gp_sql_get_datatable(stQry, dbKey);

        string strSubpoliciesQry = "SELECT Loan_Policy_ID,Loan_Full_ID,State_ID,County_ID,Crop_Code,Crop_Type_Code," +
            "Irr_Practice_Type_Code,Crop_Practice_Type_Code,HR_Exclusion_Ind,Ins_Plan,Ins_Plan_Type,Eligible_Ind," +
            "Select_Ind,Ins_Unit_Type_Code,Area_Yield,Upper_Level,Lower_Level,Liability_Percent,[Option],Price,Yield_Percent," +
            "Price_Percent,Liability_Amount,Deductible_Units,Deductible_Percent,Late_Deductible,Actuarial_Hail,Actuarial_Wind," +
            "Premium,Ins_Value,Agent_ID,Agency_ID,AIP_ID,Policy_Verification_Ind,Policy_Verification_Doc_ID," +
            "Policy_Verification_User_ID,Policy_Verification_Date_Time,Other_Description_Text,Status,IsCustom,Policy_Key,Custom1 " +
            "FROM Loan_Policy where Loan_Full_ID = '" + loan_full_id + "'";

        DataTable dtLoan_Sub_Policies = gen_db_utils.gp_sql_get_datatable(strSubpoliciesQry, dbKey);

        str_return = " <div class='panel panel-default'><div class='panel-heading accordion-toggle collapsed' data-toggle='collapse'" +
            "  data-parent='#accordion' data-target='#collapseLoanPolicies'>" + Getcolor("Policies", dtLoan_Policies.Rows.Count) + "</div> ";
        str_return += " <div id='collapseLoanPolicies' class='panel-collapse collapse'><div class='panel-body' style='overflow-y:auto'>  <table id='datatable_fixed_column' class='table table-striped table-bordered'> "
                                   + " <thead> "
                                    + " <tr> "

                                    + "<th> Loan Full Id </th>"
                                    + "<th> Crop Code </th>"
                                    + "<th> Crop Practice Type Code </th>"
                                     + "<th> Irr Practice Type Code </th>"
                                     + "<th> State Name </th>"
                                    + "<th> County Name </th>"
                                     + "  </tr> "
                                     + "    </thead> "
                                     + "      <tbody>";
        if (dtLoan_Policies != null && dtLoan_Policies.Rows.Count > 0)
        {
            foreach (DataRow dtrow in dtLoan_Policies.Rows)
            {
                str_return += "<tr>"
                + "<td> " + dtrow["loan_full_id"].ToString() + " </td>"
                + "<td> " + dtrow["Crop_Code"].ToString() + " </td>"
                 + "<td> " + dtrow["Crop_Practice_Type_Code"].ToString() + " </td>"
                 + "<td> " + dtrow["Irr_Practice_Type_Code"].ToString() + " </td>"
                    + "<td> " + dtrow["State_Id"].ToString() + "|" + fc_gen_utils.sess_lookup("st_" + dtrow["State_Id"].ToString(), "-") + " </td>"
                + "<td> " + dtrow["County_Id"].ToString() + " | " + fc_gen_utils.sess_lookup("county_" + dtrow["County_Id"].ToString(), "-") + " </td>"

                + "</tr>";

                var resData = from s in dtLoan_Sub_Policies.AsEnumerable()
                              where s.Field<string>("Crop_Code") == Convert.ToString(dtrow["Crop_Code"])
                              && s.Field<string>("Crop_Practice_Type_Code") == Convert.ToString(dtrow["Crop_Practice_Type_Code"])
                              && s.Field<string>("Irr_Practice_Type_Code") == Convert.ToString(dtrow["Irr_Practice_Type_Code"])
                              && s.Field<Int32?>("State_Id") == Convert.ToInt32(dtrow["State_Id"])
                              && s.Field<Int32?>("County_Id") == Convert.ToInt32(dtrow["County_Id"])
                              select new
                              {
                                  Loan_Policy_ID = s.Field<Int32>("Loan_Policy_ID"),
                                  Loan_Full_ID = s.Field<string>("Loan_Full_ID"),

                                  State_ID = s.Field<Int32?>("State_ID"),
                                  County_ID = s.Field<Int32?>("County_ID"),
                                  Agent_ID = s.Field<Int32?>("Agent_ID"),
                                  AIP_ID = s.Field<Int32?>("AIP_ID"),
                                  Policy_Key = s.Field<string>("Policy_Key"),
                                  Crop_Code = s.Field<string>("Crop_Code"),
                                  Crop_Type_Code = s.Field<string>("Crop_Type_Code"),
                                  Irr_Practice_Type_Code = s.Field<string>("Irr_Practice_Type_Code"),
                                  Crop_Practice_Type_Code = s.Field<string>("Crop_Practice_Type_Code"),
                                  Ins_Plan = s.Field<string>("Ins_Plan"),
                                  Ins_Plan_Type = s.Field<string>("Ins_Plan_Type"),
                                  Ins_Unit_Type_Code = s.Field<string>("Ins_Unit_Type_Code"),
                                  Option = s.Field<string>("Option"),
                                  HR_Exclusion_Ind = s.Field<bool?>("HR_Exclusion_Ind"),
                                  Eligible_Ind = s.Field<bool?>("Eligible_Ind"),
                                  Select_Ind = s.Field<bool?>("Select_Ind"),
                                  Upper_Level = s.Field<Double?>("Upper_Level"),
                                  Lower_Level = s.Field<Double?>("Lower_Level"),
                                  Premium = s.Field<Double?>("Premium"),
                                  Price = s.Field<Double?>("Price"),
                                  Yield_Percent = s.Field<Double?>("Yield_Percent"),
                                  Price_Percent = s.Field<Double?>("Price_Percent"),
                                  Ins_Value = s.Field<Double?>("Ins_Value"),
                                  Area_Yield = s.Field<Double?>("Area_Yield"),
                                  Late_Deductible = s.Field<Double?>("Late_Deductible"),
                                  Liability_Percent = s.Field<Double?>("Liability_Percent"),
                                  Actuarial_Hail = s.Field<Double?>("Actuarial_Hail"),
                                  Actuarial_Wind = s.Field<Double?>("Actuarial_Wind"),
                                  Custom1 = s.Field<Double?>("Custom1"),
                                  IsCustom = s.Field<bool?>("IsCustom"),

                                  Policy_Verification_Ind = s.Field<Int32?>("Policy_Verification_Ind"),
                                  Policy_Verification_Doc_ID = s.Field<Int32?>("Policy_Verification_Doc_ID"),
                                  Policy_Verification_User_ID = s.Field<Int32?>("Policy_Verification_User_ID"),
                                  Policy_Verification_Date_Time = s.Field<DateTime?>("Policy_Verification_Date_Time"),
                                  Other_Description_Text = s.Field<string>("Other_Description_Text"),
                                  Status = s.Field<Int32?>("Status")
                              };

                if (resData != null)
                {
                    foreach (var subitems in resData)
                    {
                        str_return += "<tr><td></td>"
                        + "<td colspan='1'> State Id : " + subitems.State_ID + "<br>"
                        + " County Id :" + subitems.County_ID + "<br>"
                        + " Agent Id :" + subitems.Agent_ID + "<br>"
                        + " AIP Id :" + subitems.AIP_ID + "<br>"
                        + "<br>"
                        + " Policy_Key:" + subitems.Policy_Key + "<br>"
                        + " Crop_Code :" + subitems.Crop_Code + "<br>"
                        + "Crop_Type_Code :" + subitems.Crop_Type_Code + "<br>"
                        + "Irr_Practice_type_Code :" + subitems.Irr_Practice_Type_Code + "<br>"
                        + "Crop_Practice_Type_Code  :" + subitems.Crop_Practice_Type_Code + " </td>"

                        + "<td colspan='1'> Ins_Plan :" + subitems.Ins_Plan + "  <br> "
                        + "Ins_Plan_Type : " + subitems.Ins_Plan_Type + "<br>"
                        + "Ins_Unit_Type_Code : " + subitems.Ins_Unit_Type_Code + "<br>"
                         + " Option :" + subitems.Option + "<br>"
                         + "<br>"
                         + " HR_Exclusion_Ind :" + subitems.HR_Exclusion_Ind + "<br>"
                         + " Eligible_Ind :" + subitems.Eligible_Ind + "<br>"
                         + " Select_Ind :" + subitems.Select_Ind + " </td>"

                           + "<td colspan='1'> Upper_Level :" + subitems.Upper_Level + "<br>"
                          + " Lower_Level :" + subitems.Lower_Level + "<br>"
                          + "<br>"
                          + " Premium :" + subitems.Premium + "<br>"
                          + "<br>"
                          + " Yield_Percent :" + subitems.Yield_Percent + "<br>"
                          + " Price_Percent :" + subitems.Price_Percent + "<br>"
                          + "<br>"
                         + " Ins_Value: " + subitems.Ins_Value + " </td>"

                         + "<td colspan='1'> Area_Yield :" + subitems.Area_Yield + "<br>"
                         + " Late_Deductible :" + subitems.Late_Deductible + "<br>"
                         + " Liability_Percent :" + subitems.Liability_Percent + "<br>"
                         + " Actuarial_Hail :" + subitems.Actuarial_Hail + "<br>"
                         + " Actuarial_Wind :" + subitems.Actuarial_Wind + "<br>"
                         + " Custom1 :" + subitems.Custom1 + "<br>"
                         + " IsCustom :" + subitems.IsCustom + " </td>"

                         + "<td colspan='1'> Policy_Verification_Ind :" + subitems.Policy_Verification_Ind + "<br>"
                         + " Policy_Verification_Doc_ID :" + subitems.Policy_Verification_Doc_ID + "<br>"
                         + " Policy_Verification_User_ID :" + subitems.Policy_Verification_User_ID + "<br>"
                         + " Policy_Verification_Date_Time :" + subitems.Policy_Verification_Date_Time + "<br>"
                         + " Other_Description_Text :" + subitems.Other_Description_Text + "<br>"
                         + " Status :" + subitems.Status + " </td>"
                        + "</tr>";
                    }
                }
            }
            str_return += " </tbody> "
           + " </table> "
           + " </div></div></div > ";
        }
        else
        {
            str_return += " <table id='datatable_fixed_column' class='table table-striped table-bordered'>";

            str_return += " </tbody> "
                                   + " </table> "
                                     + "</div></div></div >";
        }

        return str_return;
    }

    public string Get_Loan_Policies_old(string loan_full_id)
    {
        string stQry = "";
        string str_return = "";

        stQry = "SELECT Policy_id,Agent_Id,Agency_Id,State_Id,Crop_Practice_Id,County_Id,ProposedAIP,Loan_Full_Id,Rated,HighlyRated,Level,Price,Premium,Unit,MPCI_Subplan,HasSecondaryPlans,IsDeleted,CreatedDate,ModifiedDate FROM Loan_Policies "
                   + "  where Loan_Full_ID = '" + loan_full_id + "'";

        DataTable dtLoan_Policies = gen_db_utils.gp_sql_get_datatable(stQry, dbKey);

        string strSubpoliciesQry = "SELECT SubPolicy_Id,FK_Policy_Id,Upper_Limit,Lower_Limit,lsp.Agent_Id,lsp.Agency_Id,Aip,Price_Pct,Yield_Pct,Ins_SubType,Ins_Type,lsp.Premium,Prot_Factor,Yield,Wind,Liability,FCMC,lsp.[Unit],lsp.[Option],Deduct,CreatedOn,ModifiedOn "
                + "FROM Loan_Sub_Policies lsp join Loan_Policies lp on lsp.fk_policy_id = lp.policy_id"
                + "  where Loan_Full_ID = '" + loan_full_id + "'";

        DataTable dtLoan_Sub_Policies = gen_db_utils.gp_sql_get_datatable(strSubpoliciesQry, dbKey);

        str_return = " <div class='panel panel-default'><div class='panel-heading accordion-toggle collapsed' data-toggle='collapse'" +
            "  data-parent='#accordion' data-target='#collapseLoanPolicies'>" + Getcolor("Policies", dtLoan_Policies.Rows.Count) + "</div> ";
        str_return += " <div id='collapseLoanPolicies' class='panel-collapse collapse'><div class='panel-body' style='overflow-y:auto'>  <table id='datatable_fixed_column' class='table table-striped table-bordered'> "
                                   + " <thead> "
                                    + " <tr> "
                                    + "<th> Policy_id </th>"
                                    + "<th> Agent_Id </th>"
                                    + "<th> Agency_Id </th>"
                                    + "<th> State </th>"
                                    + "<th> Crop_Practice_Id </th>"
                                    + "<th> County </th>"
                                    + "<th> ProposedAIP </th>"
                                    + "<th> Loan_Full_Id </th>"
                                    + "<th> Rated </th>"
                                    + "<th> HighlyRated </th>"
                                    + "<th> Level </th>"
                                    + "<th> Price </th>"
                                    + "<th> Premium </th>"
                                    + "<th> Unit </th>"
                                    + "<th> MPCI_Subplan </th>"
                                    + "<th> HasSecondaryPlans </th>"
                                    + "<th> IsDeleted </th>"
                                    + "<th> CreatedDate </th>"
                                    + "<th> ModifiedDate </th>"
                                     + "  </tr> "
                                     + "    </thead> "
                                     + "      <tbody>";
        if (dtLoan_Policies != null && dtLoan_Policies.Rows.Count > 0)
        {
            foreach (DataRow dtrow in dtLoan_Policies.Rows)
            {
                str_return += "<tr>"
                + "<td> " + dtrow["Policy_id"].ToString() + " </td>"
                + "<td> " + dtrow["Agent_Id"].ToString() + " </td>"
                + "<td> " + dtrow["Agency_Id"].ToString() + " </td>"
                + "<td> " + dtrow["State_Id"].ToString() + "|" + fc_gen_utils.sess_lookup("st_" + dtrow["State_Id"].ToString(), "-") + " </td>"
                + "<td> " + fc_gen_utils.sess_lookup("cp_" + dtrow["Crop_Practice_Id"].ToString(), "-") + " </td>"
                + "<td> " + fc_gen_utils.sess_lookup("county_" + dtrow["County_Id"].ToString(), "-") + " </td>"
                + "<td> " + dtrow["ProposedAIP"].ToString() + " </td>"
                + "<td> " + dtrow["Loan_Full_Id"].ToString() + " </td>"
                + "<td> " + dtrow["Rated"].ToString() + " </td>"
                + "<td> " + dtrow["HighlyRated"].ToString() + " </td>"
                + "<td> " + dtrow["Level"].ToString() + " </td>"
                + "<td> " + dtrow["Price"].ToString() + " </td>"
                + "<td> " + dtrow["Premium"].ToString() + " </td>"
                + "<td> " + dtrow["Unit"].ToString() + " </td>"
                + "<td> " + dtrow["MPCI_Subplan"].ToString() + " </td>"
                + "<td> " + dtrow["HasSecondaryPlans"].ToString() + " </td>"
                + "<td> " + dtrow["IsDeleted"].ToString() + " </td>"
                + "<td> " + dtrow["CreatedDate"].ToString() + " </td>"
                + "<td> " + dtrow["ModifiedDate"].ToString() + " </td>"
                + " </td>"
                + "</tr>";

                //string strSubpoliciesQry = "SELECT SubPolicy_Id,FK_Policy_Id,Upper_Limit,Lower_Limit,lsp.Agent_Id,lsp.Agency_Id,Aip,Price_Pct,Yield_Pct,Ins_SubType,Ins_Type,lsp.Premium,Prot_Factor,Yield,Wind,Liability,FCMC,lsp.[Unit],lsp.[Option],Deduct,CreatedOn,ModifiedOn "
                //+ "FROM Loan_Sub_Policies lsp join Loan_Policies lp on lsp.fk_policy_id = lp.policy_id"
                //+ "  where Loan_Full_ID = '" + loan_full_id + "' and FK_Policy_Id= '" + dtrow["Policy_id"].ToString() + "' ";

                //DataTable dtLoan_Sub_Policies = gen_db_utils.gp_sql_get_datatable(strSubpoliciesQry, dbKey);

                var resData = from s in dtLoan_Sub_Policies.AsEnumerable()
                              where s.Field<int>("FK_Policy_Id") == Convert.ToInt32(dtrow["Policy_id"])
                              select new
                              {
                                  SubPolicy_Id = s.Field<int?>("SubPolicy_Id"),
                                  FK_Policy_Id = s.Field<int?>("FK_Policy_Id"),
                                  Upper_Limit = s.Field<Double?>("Upper_Limit"),
                                  Lower_Limit = s.Field<Double?>("Lower_Limit"),
                                  Aip = s.Field<int?>("Aip"),
                                  //Agent_Id = s.Field<int>("Agent_Id"),
                                  //Agency_Id = s.Field<int>("Agency_Id"),
                                  Price_Pct = s.Field<Double?>("Price_Pct"),
                                  Yield_Pct = s.Field<Double?>("Yield_Pct"),
                                  Ins_SubType = s.Field<string>("Ins_SubType"),
                                  Ins_Type = s.Field<string>("Ins_Type"),
                                  Premium = s.Field<Double?>("Premium"),
                                  Prot_Factor = s.Field<Double?>("Prot_Factor"),
                                  Yield = s.Field<Double?>("Yield"),
                                  Wind = s.Field<int?>("Wind"),
                                  Liability = s.Field<Double?>("Liability"),
                                  FCMC = s.Field<Double?>("FCMC"),
                                  Unit = s.Field<string>("Unit"),
                                  Option = s.Field<string>("Option"),
                                  Deduct = s.Field<Double?>("Deduct"),
                                  CreatedOn = s.Field<DateTime?>("CreatedOn"),
                                  ModifiedOn = s.Field<DateTime?>("ModifiedOn")
                              };

                if (resData != null)
                {
                    foreach (var subitems in resData)
                    {
                        //+ "FK_Policy_Id :" + dtrow1["FK_Policy_Id"].ToString()
                        //+ "FK_Policy_Id :" + dtrow1["Agent_Id"].ToString()
                        //+ "FK_Policy_Id :" + dtrow1["Agency_Id"].ToString() + " </td>"

                        str_return += "<tr><td></td>"
                        + "<td colspan='3'> <b>SubPolicy_Id:</b> " + subitems.SubPolicy_Id + "<br>"
                        + " Upper_Limit :" + subitems.Upper_Limit + "<br>"
                        + " Lower_Limit :" + subitems.Lower_Limit + " </td>"

                        + "<td colspan='2'> Aip:" + subitems.Aip + "  <br> "
                        + "Price_Pct: " + subitems.Price_Pct + "<br>"
                         + " Yield_Pct :" + subitems.Yield_Pct + " </td>"

                         + "<td colspan='3'> Ins_SubType :" + subitems.Ins_SubType + "<br>"
                         + " Ins_Type :" + subitems.Ins_Type + "<br>"
                        + " Premium: " + subitems.Premium + " </td>"

                        + "<td colspan='3'> Prot_Factor :" + subitems.Prot_Factor + "<br>"
                        + " Yield :" + subitems.Yield + "<br>"
                        + " Wind :" + subitems.Wind + " </td>"

                        + "<td colspan='3'> Liability :" + subitems.Liability + "<br>"
                        + " FCMC:" + subitems.FCMC + "<br>"
                        + " Deduct :" + subitems.Deduct + " </td>"

                       + "<td colspan='4'> CreatedOn :" + subitems.CreatedOn + "<br>"
                        + " ModifiedOn :" + subitems.ModifiedOn
                        + " </td>"
                        + "</tr>";
                    }
                }
            }
            str_return += " </tbody> "
           + " </table> "
           + " </div></div></div > ";
        }
        else
        {
            str_return += " <table id='datatable_fixed_column' class='table table-striped table-bordered'>";

            str_return += " </tbody> "
                                   + " </table> "
                                     + "</div></div></div >";
        }

        return str_return;
    }

    public string Get_Loan_Q_Response(string loan_full_id)
    {
        string stQry = "";
        string str_return = "";

        stQry = "SELECT Loan_Q_response_ID,Loan_ID,Loan_Seq_Num,Loan_Full_ID,Chevron_ID,Question_ID,Question_Category_Code,Response_Detail,Response_Detail_Field_ID,Status FROM Loan_Q_Response  "
                   + "  where Loan_Full_ID = '" + loan_full_id + "'";

        DataTable dtLoan_Q_Response = gen_db_utils.gp_sql_get_datatable(stQry, dbKey);
        str_return = " <div class='panel panel-default'><div class='panel-heading accordion-toggle collapsed' data-toggle='collapse'" +
             "  data-parent='#accordion' data-target='#collapseLoan_Q_Response'>" + Getcolor("Q Response", dtLoan_Q_Response.Rows.Count) + "</div> ";
        str_return += " <div id='collapseLoan_Q_Response' class='panel-collapse collapse'><div class='panel-body' style='overflow-y:auto'>  <table id='datatable_fixed_column' class='table table-striped table-bordered'> "
               + " <thead> "
                + " <tr> "
                + "<th> Loan_Q_response_ID </th>"
                + "<th> Loan_ID </th>"
                + "<th> Loan_Seq_Num </th>"
                + "<th> Loan_Full_ID </th>"
                + "<th> Chevron_ID </th>"
                + "<th> Question_ID </th>"
                + "<th> Question_Category_Code </th>"
                + "<th> Response_Detail </th>"
                + "<th> Response_Detail_Field_ID </th>"
                + "<th> Status </th>"
                 + "  </tr> "
                 + "    </thead> "
                 + "      <tbody>";

        if (dtLoan_Q_Response != null && dtLoan_Q_Response.Rows.Count > 0)
        {
            foreach (DataRow dtrow in dtLoan_Q_Response.Rows)
            {
                str_return += "<tr>"
                + "<td> " + dtrow["Loan_Q_response_ID"].ToString() + " </td>"
                + "<td> " + dtrow["Loan_ID"].ToString() + " </td>"
                + "<td> " + dtrow["Loan_Seq_Num"].ToString() + " </td>"
                + "<td> " + dtrow["Loan_Full_ID"].ToString() + " </td>"
                + "<td> " + dtrow["Chevron_ID"].ToString() + " </td>"
                + "<td> " + dtrow["Question_ID"].ToString() + " </td>"
                + "<td> " + dtrow["Question_Category_Code"].ToString() + " </td>"
                + "<td> " + dtrow["Response_Detail"].ToString() + " </td>"
                + "<td> " + dtrow["Response_Detail_Field_ID"].ToString() + " </td>"
                + "<td> " + dtrow["Status"].ToString() + " </td>"
                + " </td>"
                + "</tr>";
            }
            str_return += " </tbody> "
           + " </table> "
           + " </div></div></div > ";
        }
        else
        {
            str_return += " <table id='datatable_fixed_column' class='table table-striped table-bordered'>";

            str_return += " </tbody> "
                                   + " </table> "
                                     + "</div></div></div >";
        }

        return str_return;
    }

    public string Get_Loan_Sub_Policies(string loan_full_id)
    {
        string stQry = "";
        string str_return = "";
        // stQry = "SELECT SubPolicy_Id,FK_Policy_Id,Upper_Limit,Lower_Limit,Agent_Id,Agency_Id,Aip,Price_Pct,Yield_Pct,Ins_SubType,Ins_Type,Premium,Prot_Factor,Yield,Wind,Liability,FCMC,Deduct,CreatedOn,ModifiedOn FROM Loan_Sub_Policies  ";
        stQry = "SELECT SubPolicy_Id,FK_Policy_Id,Upper_Limit,Lower_Limit,lsp.Agent_Id,lsp.Agency_Id,Aip,Price_Pct,Yield_Pct,Ins_SubType,Ins_Type,lsp.Premium,Prot_Factor,Yield,Wind,Liability,FCMC,Deduct,CreatedOn,ModifiedOn "
                + "FROM Loan_Sub_Policies lsp join Loan_Policies lp on lsp.fk_policy_id = lp.policy_id"
                + "  where Loan_Full_ID = '" + loan_full_id + "'";

        DataTable dtLoan_Sub_Policies = gen_db_utils.gp_sql_get_datatable(stQry, dbKey);
        str_return = " <div class='panel panel-default'><div class='panel-heading accordion-toggle collapsed' data-toggle='collapse'" +
    "  data-parent='#accordion' data-target='#collapseLoanSubPolicies'>" + Getcolor("Sub Policies", dtLoan_Sub_Policies.Rows.Count) + "</div> ";
        str_return += " <div id='collapseLoanSubPolicies' class='panel-collapse collapse'><div class='panel-body' style='overflow-y:auto'>  <table id='datatable_fixed_column' class='table table-striped table-bordered'> "
               + " <thead> "
                + " <tr> "
                + "<th> SubPolicy_Id </th>"
                + "<th> FK_Policy_Id </th>"
                + "<th> Upper_Limit </th>"
                + "<th> Lower_Limit </th>"
                + "<th> Agent_Id </th>"
                + "<th> Agency_Id </th>"
                + "<th> Aip </th>"
                + "<th> Price_Pct </th>"
                + "<th> Yield_Pct </th>"
                + "<th> Ins_SubType </th>"
                + "<th> Ins_Type </th>"
                + "<th> Premium </th>"
                + "<th> Prot_Factor </th>"
                + "<th> Yield </th>"
                + "<th> Wind </th>"
                + "<th> Liability </th>"
                + "<th> FCMC </th>"
                + "<th> Deduct </th>"
                + "<th> CreatedOn </th>"
                + "<th> ModifiedOn </th>"
                 + "  </tr> "
                 + "    </thead> "
                 + "      <tbody>";
        if (dtLoan_Sub_Policies != null && dtLoan_Sub_Policies.Rows.Count > 0)
        {
            foreach (DataRow dtrow in dtLoan_Sub_Policies.Rows)
            {
                str_return += "<tr>"
                + "<td> " + dtrow["SubPolicy_Id"].ToString() + " </td>"
                + "<td> " + dtrow["FK_Policy_Id"].ToString() + " </td>"
                + "<td> " + dtrow["Upper_Limit"].ToString() + " </td>"
                + "<td> " + dtrow["Lower_Limit"].ToString() + " </td>"
                + "<td> " + dtrow["Agent_Id"].ToString() + " </td>"
                + "<td> " + dtrow["Agency_Id"].ToString() + " </td>"
                + "<td> " + dtrow["Aip"].ToString() + " </td>"
                + "<td> " + dtrow["Price_Pct"].ToString() + " </td>"
                + "<td> " + dtrow["Yield_Pct"].ToString() + " </td>"
                + "<td> " + dtrow["Ins_SubType"].ToString() + " </td>"
                + "<td> " + dtrow["Ins_Type"].ToString() + " </td>"
                + "<td> " + dtrow["Premium"].ToString() + " </td>"
                + "<td> " + dtrow["Prot_Factor"].ToString() + " </td>"
                + "<td> " + dtrow["Yield"].ToString() + " </td>"
                + "<td> " + dtrow["Wind"].ToString() + " </td>"
                + "<td> " + dtrow["Liability"].ToString() + " </td>"
                + "<td> " + dtrow["FCMC"].ToString() + " </td>"
                + "<td> " + dtrow["Deduct"].ToString() + " </td>"
                + "<td> " + dtrow["CreatedOn"].ToString() + " </td>"
                + "<td> " + dtrow["ModifiedOn"].ToString() + " </td>"
                + " </td>"
                + "</tr>";
            }
            str_return += " </tbody> "
           + " </table> "
           + " </div></div></div > ";
        }
        else
        {
            str_return += " <table id='datatable_fixed_column' class='table table-striped table-bordered'>";
            str_return += " </tbody> "
                                   + " </table> "
                                     + "</div></div></div >";
        }

        return str_return;
    }

    public string Get_Loan_Wfrp(string loan_full_id)
    {
        string stQry = "";
        string str_return = "";

        stQry = "SELECT Wfrv_ID,Loan_Full_ID,Proposed_AIP,Approved_Revenue,Level,Premium,Created_On,Modified_On FROM Loan_Wfrp "
                   + "  where Loan_Full_ID = '" + loan_full_id + "'";

        DataTable dtLoan_Wfrp = gen_db_utils.gp_sql_get_datatable(stQry, dbKey);
        str_return = " <div class='panel panel-default'><div class='panel-heading accordion-toggle collapsed' data-toggle='collapse'" +
           "  data-parent='#accordion' data-target='#collapseLoanWfrp'>" + Getcolor("Wfrp", dtLoan_Wfrp.Rows.Count) + "</div> ";

        str_return += " <div id='collapseLoanWfrp' class='panel-collapse collapse'><div class='panel-body' style='overflow-y:auto'>  <table id='datatable_fixed_column' class='table table-striped table-bordered'> "
          + " <thead> "
           + " <tr> "
           + "<th> Wfrv_ID </th>"
           + "<th> Loan_Full_ID </th>"
           + "<th> Proposed_AIP </th>"
           + "<th> Approved_Revenue </th>"
           + "<th> Level </th>"
           + "<th> Premium </th>"
           + "<th> Created_On </th>"
           + "<th> Modified_On </th>"
            + "  </tr> "
            + "    </thead> "
            + "      <tbody>";

        if (dtLoan_Wfrp != null && dtLoan_Wfrp.Rows.Count > 0)
        {
            foreach (DataRow dtrow in dtLoan_Wfrp.Rows)
            {
                str_return += "<tr>"
                + "<td> " + dtrow["Wfrv_ID"].ToString() + " </td>"
                + "<td> " + dtrow["Loan_Full_ID"].ToString() + " </td>"
                + "<td> " + dtrow["Proposed_AIP"].ToString() + " </td>"
                + "<td> " + dtrow["Approved_Revenue"].ToString() + " </td>"
                + "<td> " + dtrow["Level"].ToString() + " </td>"
                + "<td> " + dtrow["Premium"].ToString() + " </td>"
                + "<td> " + dtrow["Created_On"].ToString() + " </td>"
                + "<td> " + dtrow["Modified_On"].ToString() + " </td>"
                + " </td>"
                + "</tr>";
            }
            str_return += " </tbody> "
           + " </table> "
           + " </div></div></div > ";
        }
        else
        {
            str_return += " <table id='datatable_fixed_column' class='table table-striped table-bordered'>";

            str_return += " </tbody> "
                                   + " </table> "
                                     + "</div></div></div >";
        }

        return str_return;
    }

    public string Get_CoBorrower(string loan_full_id)
    {
        string stQry = "";
        string str_return = "";
        stQry = "SELECT CoBorrower_ID,Loan_Full_ID,Borrower_ID_Type,Borrower_SSN_Hash,Borrower_Entity_Type_Code," +
            "Borrower_Last_Name,Borrower_First_Name,Borrower_MI,Borrower_Address,Borrower_City,Borrower_State_ID," +
            "Borrower_Zip,Borrower_Phone,Borrower_email,Borrower_DL_state,Borrower_Dl_Num,Borrower_DOB,Borrower_Preferred_Contact_Ind," +
            "Borrower_County_ID,Borrower_Lat,Borrower_Long,Spouse_SSN_Hash,Spouse_Last_name,Spouse_First_Name," +
            "Spouse__MI,Spouse__Address,Spouse_City,Spouse_State,Spouse_Zip,Spouse_Phone,Spouse_Email,Spouse_Preffered_Contact_Ind," +
            "IsDelete FROM CoBorrower"
                   + "  where Loan_Full_ID = '" + loan_full_id + "'";
        DataTable dtCoBorrower = gen_db_utils.gp_sql_get_datatable(stQry, dbKey);
        str_return = " <div class='panel panel-default'><div class='panel-heading accordion-toggle collapsed' data-toggle='collapse'" +
           "  data-parent='#accordion' data-target='#collapseCoBorrower'>" + Getcolor("Co Borrower", dtCoBorrower.Rows.Count) + "</div> ";
        str_return += " <div id='collapseCoBorrower' class='panel-collapse collapse'><div class='panel-body' style='overflow-y:auto'>  <table id='datatable_fixed_column' class='table table-striped table-bordered'> "
           + " <thead> "
            + " <tr> "
            + "<th> CoBorrower_ID </th>"
            + "<th> Loan_Full_ID </th>"
            + "<th> Borrower_ID_Type </th>"
            + "<th> Borrower_SSN_Hash </th>"
            + "<th> Borrower_Entity_Type_Code </th>"
            + "<th> Borrower_Last_Name </th>"
            + "<th> Borrower_First_Name </th>"
            + "<th> Borrower_MI </th>"
            + "<th> Borrower_Address </th>"
            + "<th> Borrower_City </th>"
            + "<th> Borrower_State_ID </th>"
            + "<th> Borrower_Zip </th>"
            + "<th> Borrower_Phone </th>"
            + "<th> Borrower_email </th>"
            + "<th> Borrower_DL_state </th>"
            + "<th> Borrower_Dl_Num </th>"
            + "<th> Borrower_DOB </th>"
            + "<th> Borrower_Preferred_Contact_Ind </th>"
            + "<th> Borrower_County_ID </th>"
            + "<th> Borrower_Lat </th>"
            + "<th> Borrower_Long </th>"
            + "<th> Spouse_SSN_Hash </th>"
            + "<th> Spouse_Last_name </th>"
            + "<th> Spouse_First_Name </th>"
            + "<th> Spouse__MI </th>"
            + "<th> Spouse__Address </th>"
            + "<th> Spouse_City </th>"
            + "<th> Spouse_State </th>"
            + "<th> Spouse_Zip </th>"
            + "<th> Spouse_Phone </th>"
            + "<th> Spouse_Email </th>"
            + "<th> Spouse_Preffered_Contact_Ind </th>"
             + "  </tr> "
             + "    </thead> "
             + "      <tbody>";
        if (dtCoBorrower != null && dtCoBorrower.Rows.Count > 0)
        {
            foreach (DataRow dtrow in dtCoBorrower.Rows)
            {
                str_return += "<tr>"
                + "<td> " + dtrow["CoBorrower_ID"].ToString() + " </td>"
                + "<td> " + dtrow["Loan_Full_ID"].ToString() + " </td>"
                + "<td> " + dtrow["Borrower_ID_Type"].ToString() + " </td>"
                + "<td> " + dtrow["Borrower_SSN_Hash"].ToString() + " </td>"
                + "<td> " + dtrow["Borrower_Entity_Type_Code"].ToString() + " </td>"
                + "<td> " + dtrow["Borrower_Last_Name"].ToString() + " </td>"
                + "<td> " + dtrow["Borrower_First_Name"].ToString() + " </td>"
                + "<td> " + dtrow["Borrower_MI"].ToString() + " </td>"
                + "<td> " + dtrow["Borrower_Address"].ToString() + " </td>"
                + "<td> " + dtrow["Borrower_City"].ToString() + " </td>"
                + "<td> " + dtrow["Borrower_State_ID"].ToString() + " </td>"
                + "<td> " + dtrow["Borrower_Zip"].ToString() + " </td>"
                + "<td> " + dtrow["Borrower_Phone"].ToString() + " </td>"
                + "<td> " + dtrow["Borrower_email"].ToString() + " </td>"
                + "<td> " + dtrow["Borrower_DL_state"].ToString() + " </td>"
                + "<td> " + dtrow["Borrower_Dl_Num"].ToString() + " </td>"
                + "<td> " + dtrow["Borrower_DOB"].ToString() + " </td>"
                + "<td> " + dtrow["Borrower_Preferred_Contact_Ind"].ToString() + " </td>"
                + "<td> " + dtrow["Borrower_County_ID"].ToString() + " </td>"
                + "<td> " + dtrow["Borrower_Lat"].ToString() + " </td>"
                + "<td> " + dtrow["Borrower_Long"].ToString() + " </td>"
                + "<td> " + dtrow["Spouse_SSN_Hash"].ToString() + " </td>"
                + "<td> " + dtrow["Spouse_Last_name"].ToString() + " </td>"
                + "<td> " + dtrow["Spouse_First_Name"].ToString() + " </td>"
                + "<td> " + dtrow["Spouse__MI"].ToString() + " </td>"
                + "<td> " + dtrow["Spouse__Address"].ToString() + " </td>"
                + "<td> " + dtrow["Spouse_City"].ToString() + " </td>"
                + "<td> " + dtrow["Spouse_State"].ToString() + " </td>"
                + "<td> " + dtrow["Spouse_Zip"].ToString() + " </td>"
                + "<td> " + dtrow["Spouse_Phone"].ToString() + " </td>"
                + "<td> " + dtrow["Spouse_Email"].ToString() + " </td>"
                + "<td> " + dtrow["Spouse_Preffered_Contact_Ind"].ToString() + " </td>"
                //+ "<td> " + dtrow["IsDelete"].ToString() + " </td>"
                + " </td>"
                + "</tr>";
            }
            str_return += " </tbody> "
           + " </table> "
           + " </div></div></div > ";
        }
        else
        {
            str_return += " <table id='datatable_fixed_column' class='table table-striped table-bordered'>";

            str_return += " </tbody> "
                                   + " </table> "
                                     + "</div></div></div >";
        }

        return str_return;
    }

    public string Get_Borrower_Income_History(string loan_full_id)
    {
        string stQry = "";
        string str_return = "";
        stQry = "SELECT BIH_ID,Borrower_ID,Loan_Full_ID,Borrower_Year,Borrower_Expense,Borrower_Revenue,FC_Borrower_Income,Status FROM Borrower_Income_History"
                   + "  where Loan_Full_ID = '" + loan_full_id + "'";
        DataTable dtBorrower_IH = gen_db_utils.gp_sql_get_datatable(stQry, dbKey);
        str_return = " <div class='panel panel-default'><div class='panel-heading accordion-toggle collapsed' data-toggle='collapse'" +
           "  data-parent='#accordion' data-target='#collapseBorrowerIH'>" + Getcolor("Borrower Income History", dtBorrower_IH.Rows.Count) + "</div> ";
        str_return += " <div id='collapseBorrowerIH' class='panel-collapse collapse'><div class='panel-body' style='overflow-y:auto'>  <table id='datatable_fixed_column' class='table table-striped table-bordered'> "
          + " <thead> "
           + " <tr> "
           + "<th> BIH_ID </th>"
           + "<th> Borrower_ID </th>"
           + "<th> Loan_Full_ID </th>"
           + "<th> Borrower_Year </th>"
           + "<th> Borrower_Expense </th>"
           + "<th> Borrower_Revenue </th>"
           + "<th> FC_Borrower_Income </th>"
           + "<th> Status </th>"
            + "  </tr> "
            + "    </thead> "
            + "      <tbody>";
        if (dtBorrower_IH != null && dtBorrower_IH.Rows.Count > 0)
        {
            foreach (DataRow dtrow in dtBorrower_IH.Rows)
            {
                str_return += "<tr>"
                + "<td> " + dtrow["BIH_ID"].ToString() + " </td>"
                + "<td> " + dtrow["Borrower_ID"].ToString() + " </td>"
                + "<td> " + dtrow["Loan_Full_ID"].ToString() + " </td>"
                + "<td> " + dtrow["Borrower_Year"].ToString() + " </td>"
                + "<td> " + dtrow["Borrower_Expense"].ToString() + " </td>"
                + "<td> " + dtrow["Borrower_Revenue"].ToString() + " </td>"
                + "<td> " + dtrow["FC_Borrower_Income"].ToString() + " </td>"
                + "<td> " + dtrow["Status"].ToString() + " </td>"
                + " </td>"
                + "</tr>";
            }
            str_return += " </tbody> "
           + " </table> "
           + " </div></div></div > ";
        }
        else
        {
            str_return += " <table id='datatable_fixed_column' class='table table-striped table-bordered'>";

            str_return += " </tbody> "
                                   + " </table> "
                                     + "</div></div></div >";
        }

        return str_return;
    }

    public string Get_Loan_Disburse(string loan_full_id)
    {
        string stQry = "";
        string str_return = "";

        //stQry = "SELECT Policy_id,Agent_Id,Agency_Id,State_Id,Crop_Practice_Id,County_Id,ProposedAIP,Loan_Full_Id,Rated,HighlyRated,Level,Price,Premium,Unit,MPCI_Subplan,HasSecondaryPlans,IsDeleted,CreatedDate,ModifiedDate FROM Loan_Policies "
        //           + "  where Loan_Full_ID = '" + loan_full_id + "'";
        stQry = "SELECT Loan_Disburse_ID ,Loan_ID ,Total_Disburse_Amount ,Request_User_ID ," +
            "Request_Status_Ind ,Request_Date_Time ,LO_Approval_User_ID ,LO_Approval_Status_Ind ,LO_Approval_Date_Time ," +
            "Risk_Approval_User_ID ,Risk_Approval_Status_Ind ,Risk_Approval_Date_Time ,Processor_User_ID ,Processor_Status_Ind ," +
            "Processor_Date_time ,Special_ACH_Ind,Special_CRP_Ind ,Special_Comment ,Special_Harvest_Comment," +
            "Status FROM Loan_Disburse"
                  + "  where Loan_ID = " + loan_full_id + "";

        DataTable dtLoan_Disburses = gen_db_utils.gp_sql_get_datatable(stQry, dbKey);

        string strDisburseDetailsQry = "SELECT ldis.Loan_Disburse_ID ,ldis.Loan_ID ,ldis.Total_Disburse_Amount ," +
            "ldis.Request_User_ID ,ldis.Request_Status_Ind ,ldis.Request_Date_Time ,ldis.LO_Approval_User_ID ," +
            "ldis.LO_Approval_Status_Ind ,ldis.LO_Approval_Date_Time ,ldis.Risk_Approval_User_ID ," +
            "ldis.Risk_Approval_Status_Ind ,ldis.Risk_Approval_Date_Time ,ldis.Processor_User_ID ,ldis.Processor_Status_Ind ," +
            "ldis.Processor_Date_time ,ldis.Special_ACH_Ind ,ldis.Special_CRP_Ind ," +
            "ldis.Special_Comment ,ldis.Special_Harvest_Comment ,ldis.Status ,ldisDet.Loan_Disburse_Detail_ID ," +
            "ldisDet.Loan_ID ,ldisDet.Loan_Disburse_ID ,ldisDet.Budget_Expense_ID ,ldisDet.Disburse_Detail_Commit_Amount ," +
            "ldisDet.Disburse_Detail_Used_Amount ,ldisDet.Disburse_Detail_Requested_Amount ,ldisDet.Status  "
                + "FROM Loan_Disburse_Detail ldisDet join Loan_Disburse ldis on ldisDet.Loan_Disburse_ID = ldis.Loan_Disburse_ID"
                + "  where ldis.Loan_ID = " + loan_full_id + "";

        DataTable dtLoan_Sub_Policies = gen_db_utils.gp_sql_get_datatable(strDisburseDetailsQry, dbKey);

        str_return = " <div class='panel panel-default'><div class='panel-heading accordion-toggle collapsed' data-toggle='collapse'" +
            "  data-parent='#accordion' data-target='#collapseLoanDisburse'>" + Getcolor("Disburse", dtLoan_Disburses.Rows.Count) + "</div> ";
        str_return += " <div id='collapseLoanDisburse' class='panel-collapse collapse'><div class='panel-body' style='overflow-y:auto'>  <table id='datatable_fixed_column' class='table table-striped table-bordered'> "
                                   + " <thead> "
                                     + "    </thead> "
                                     + "      <tbody>";
        if (dtLoan_Disburses != null && dtLoan_Disburses.Rows.Count > 0)
        {
            foreach (DataRow dtrow in dtLoan_Disburses.Rows)
            {
                str_return += "<tr>"
                     + "<td colspan='3'><b> Loan_Disburse_ID  </br> Loan_ID </br>Total_Disburse_Amount </b></br><b> Request_Status_Ind  </br> Request_Date_Time </br>Status</b> </br><b> LO_Approval_User_ID  </br> LO_Approval_Status_Ind </br>LO_Approval_Date_Time </b></td>"
                     + "<td colspan='2'> " + dtrow["Loan_Disburse_ID"].ToString() + "</br>" + dtrow["Loan_ID"].ToString() + " </br> " + dtrow["Total_Disburse_Amount"].ToString() + " </br> " + dtrow["Request_Status_Ind"].ToString() + "</br>" + dtrow["Request_Date_Time"].ToString() + "</br>" + dtrow["Status"].ToString() + " </br> " + dtrow["LO_Approval_User_ID"].ToString() + "</br>" + dtrow["LO_Approval_Status_Ind"].ToString() + " </br> " + dtrow["LO_Approval_Date_Time"].ToString() + "</td>"
                      + "<td colspan='2'><b> Risk_Approval_User_ID  </br> Risk_Approval_Status_Ind </br>Risk_Approval_Date_Time </b> </br><b> Processor_User_ID  </br> Processor_Status_Ind </br>Processor_Date_time </b> </br><b> Special_ACH_Ind  </br> Special_CRP_Ind </br>Special_Comment </br>Special_Harvest_Comment</b></td>"
                     + "<td colspan='2'> " + dtrow["Risk_Approval_User_ID"].ToString() + "</br>" + dtrow["Risk_Approval_Status_Ind"].ToString() + " </br> " + dtrow["Risk_Approval_Date_Time"].ToString() + " </br> " + dtrow["Processor_User_ID"].ToString() + "</br>" + dtrow["Processor_Status_Ind"].ToString() + " </br> " + dtrow["Processor_Date_time"].ToString() + "</br> " + dtrow["Special_ACH_Ind"].ToString() + "</br>" + dtrow["Special_CRP_Ind"].ToString() + " </br> " + dtrow["Special_Comment"].ToString() + " </br> " + dtrow["Special_Harvest_Comment"].ToString() + "</td>"
                     + "</tr>";
                var resData = from s in dtLoan_Sub_Policies.AsEnumerable()
                              where s.Field<int>("Loan_Disburse_ID") == Convert.ToInt32(dtrow["Loan_Disburse_ID"])
                              select new
                              {
                                  Loan_Disburse_Detail_ID = s.Field<int?>("Loan_Disburse_Detail_ID"),
                                  Loan_ID = s.Field<string>("Loan_ID"),
                                  Loan_Disburse_ID = s.Field<int?>("Loan_Disburse_ID"),
                                  Budget_Expense_ID = s.Field<int?>("Budget_Expense_ID"),
                                  Disburse_Detail_Commit_Amount = s.Field<Double?>("Disburse_Detail_Commit_Amount"),
                                  Disburse_Detail_Used_Amount = s.Field<Double?>("Disburse_Detail_Used_Amount"),
                                  Disburse_Detail_Requested_Amount = s.Field<Double?>("Disburse_Detail_Requested_Amount"),
                                  Status = s.Field<int?>("Status")
                              };

                if (resData != null)
                {
                    str_return += " <tr> "
                        + "<td>  </td>"
                                    + "<td> Loan_Disburse_ID </td>"
                                    + "<td> Loan_ID </td>"
                                    + "<td> Loan_Disburse_Detail_ID </td>"
                                    + "<td> Budget_Expense_ID </td>"
                                    + "<td> Disburse_Detail_Commit_Amount </td>"
                                    + "<td> Disburse_Detail_Used_Amount </td>"
                                    + "<td> Disburse_Detail_Requested_Amount </td>"
                                    + "<td> Status </td>"
                                     + "  </tr> ";
                    foreach (var subitems in resData)
                    {
                        str_return += "<tr>"
                             + "<td>  </td>"
                                   + "<td> " + subitems.Loan_Disburse_ID + " </td>"
                                   + "<td> " + subitems.Loan_ID + " </td>"
                                   + "<td> " + subitems.Loan_Disburse_Detail_ID + " </td>"
                                   + "<td> " + subitems.Budget_Expense_ID + " </td>"
                                   + "<td> " + subitems.Disburse_Detail_Commit_Amount + " </td>"
                                   + "<td> " + subitems.Disburse_Detail_Used_Amount + " </td>"
                                   + "<td> " + subitems.Disburse_Detail_Requested_Amount + " </td>"
                                   + "<td> " + subitems.Status + " </td>"
                                   + "</tr>";
                    }
                    str_return += " <tr></tr> ";
                }
            }
            str_return += " </tbody> "
           + " </table> "
           + " </div></div></div > ";
        }
        else
        {
            str_return += " <table id='datatable_fixed_column' class='table table-striped table-bordered'>";

            str_return += " </tbody> "
                                   + " </table> "
                                     + "</div></div></div >";
        }

        return str_return;
    }
}