﻿<%@ Page Title="" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="Loan_Purge.aspx.cs" Inherits="admin_Loan_Purge" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <link href='../css/fullcalendar.min.css' rel='stylesheet' />
    <link href='../css/fullcalendar.print.min.css' rel='stylesheet' media='print' />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" />
    <script src='../js/moment.min.js'></script>
    <script src='../js/fullcalendar/jquery.min.js'></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src='../js/fullcalendar/fullcalendar.min.js'></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <style>
        #spncheckedcount {
            display: block;
        }
    </style>

    <div style="width: 1500px; margin-top: 15px">
        <div>
            <div class="form-group" style="margin-left: 2.6%;">

                <label for="txtFromLoanID" style="float: left; margin-top: 7px;">From Loan ID:</label>
                <input type="text" class="form-control" name="search" id="txtFromLoanID" style="float: left; margin-left: 5px; width: 230px;" />

                <label for="txtToLoanID" style="float: left; margin-top: 7px; margin-left: 1%;">To Loan ID:</label>
                <input type="text" class="form-control" name="search" id="txtToLoanID" style="float: left; margin-left: 5px; width: 230px;" />

                <label for="ddlcropyear" style="float: left; margin-top: 7px; margin-left: 1%;">Crop Year:</label>
                <select class="form-control" id="ddlcropyear" style="float: left; margin-left: 5px; width: 230px;">
                    <option value="">Select</option>
                    <option value="2017">2017</option>
                    <option value="2018">2018</option>
                    <option value="2019">2019</option>
                </select>

                <div id="btnFilter" class="btn btn-md btn-primary" style="float: left; margin-left: 1%;">
                    Filter
                </div>
            </div>
            <div id="btnPurge" class="btn btn-md btn-default" style="float: left; margin-left: 1%;">
                Purge
            </div>
            <div class="loader" style="float: left; margin-top: 0rem; margin-left: 3rem;">
            </div>
        </div>
        <%-- <div>
            <div>
                <div class="form-group">
                    <label for="txtSearch1" style="float: left; margin-top: 7px;">Search :</label>
                    <input type="text" class="form-control" name="search" id="txtSearch1" style="float: left; margin-left: 5px; width: 230px;" />
                    <div id="btnSearch" class="btn btn-md btn-primary" style="float: left; margin-left: 5px;">
                        Search
                    </div>
                </div>
            </div>
        </div>--%>
    </div>
    <div class="row">
        <div class="col-md-11" style="padding-left: 51px; width: 1394px; padding-top: 8px;">
            <span id="spncheckedcount" style="color: green;"></span>
            <asp:Label ID="lbl_table" runat="server"></asp:Label>
        </div>
    </div>
    <div class="modal fade" id="myModal1" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <%--style="width:758px"--%>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Loan Details</h4>
                </div>
                <div class="modal-body">
                    <div class="row" style="margin-left: 5px">
                        <div id="divModelSave1">
                            <div class="col-md-12">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtSearch1">Loan Master id</label>
                                        <input type="text" id="txtloanMasterID" class="form-control" disabled="disabled" style="width: 200px" />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtSearch1">Loan Full id</label>
                                        <input type="text" id="txtloanfullid" disabled="disabled" class="form-control" style="width: 200px" />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtSearch1">Crop Year</label>
                                        <input type="text" id="txtcropyear" class="form-control" name="UserName" style="width: 200px" />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtSearch1">Pre Year Loan Id</label>
                                        <input type="text" id="txtpreyearid" class="form-control" name="UserName" style="width: 200px" />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtSearch1">Loan Officer ID</label>
                                        <input type="text" id="txtuserid" class="form-control" name="UserName" style="width: 200px" />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtSearch1">Farmer_ID</label>
                                        <input type="text" id="txtfarmerid" class="form-control" disabled="disabled" name="UserName" style="width: 200px" />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtSearch1">Loan_Status</label>
                                        <input type="text" id="txtloanstatus" class="form-control" name="UserName" style="width: 200px" />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtSearch1">is_Latest</label>
                                        <select id="ddlislatest" class="form-control" style="width: 80%;">
                                            <option value="1">Yes</option>
                                            <option value="0">No</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-8">
                                </div>
                                <div class="col-md-4 text-center">
                                    <div class="btn btn-primary" id="btnModalSave">Save</div>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <%--style="width:758px"--%>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Confirm Delete</h4>
                </div>
                <div class="modal-body">
                    <div class="row" style="margin-left: 5px">
                        <div id="divModelSave">
                            <div>
                                <input type="hidden" id="hdn_loan_master_id" />
                                <h4>Are you sure, want to delete this loan details ?</h4>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-8">
                                </div>
                                <div class="col-md-4 text-center">
                                    <div class="btn btn-primary" id="btnModalYes">YES</div>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">NO</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="myModal2" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <%--style="width:758px"--%>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Confirm Delete</h4>
                </div>
                <div class="modal-body">
                    <div class="row" style="margin-left: 5px">
                        <div>
                            <input type="hidden" />
                            <h4>Are you sure, want to delete this All loan details ?</h4>
                            <h4 id="h4loanids"></h4>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-8">
                            </div>
                            <div class="col-md-4 text-center">
                                <div class="btn btn-primary" id="btnModalYes1">YES</div>
                                <button type="button" class="btn btn-default" data-dismiss="modal">NO</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        var Loan_Master_IDs = [];
        var Loan_Full_Ids = [];
        $(function () {
            $("#txtSearch").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "Loan_Purge.aspx/GetLoanFullId",
                        data: "{searchVal:'" + $("#txtSearch").val() + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            response(data.d);
                        }
                    });
                },
                select: function (event, ui) {
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "Loan_Purge.aspx/LoadTable",
                        data: "{loan_id:'" + ui.item.value + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            $('#ContentPlaceHolder1_lbl_table').html(data.d);
                        }
                    });
                }
            });
            var obj = {
                Init: function () {
                    $('.loader').show();
                    obj.buttonsave();
                    obj.loadtable();

                    $("#txtSearch").on("keyup", function () {
                        var value = $('#txtSearch').val();
                        if (value == '') {
                            $('.loader').show();
                            var self = this;
                            obj.loadtable();
                        }
                    });

                    $("#btnPurge").addClass('disabled');

                    $('#btnModalYes').click(function () {
                        obj.buttonYes();
                    });

                    $('#btnModalYes1').click(function () {
                        obj.buttonYes1();
                    });

                    $("#btnFilter").click(function () {
                        $('.loader').show();
                        obj.loadtable();
                    });

                    $("#btnPurge").click(function () {
                        Loan_Full_Ids = [];
                        Loan_Master_IDs = [];
                        $.each($("input[name='checkedones']:checked"), function () {
                            Loan_Full_Ids.push($(this).val());
                            Loan_Master_IDs.push(this.id);
                        });
                        $('#h4loanids').text(Loan_Full_Ids.join(", "));
                        $('#myModal2').modal('show');
                    });

                },
                autosearch: function () {
                    var value = $('#txtSearch1').val();
                    $('.loader').show();
                    $.ajax({
                        type: "POST",
                        url: "Loan_Purge.aspx/LoadTable",
                        data: JSON.stringify({ keyword: value }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            $('#ContentPlaceHolder1_lbl_table').html(res.d);
                            $('.loader').hide();
                        }
                    });

                },
                buttonYes: function () {
                    $('.loader').show();
                    var divModelSave = $('#divModelSave').show();
                    $('#spnException').hide();
                    $.ajax({
                        type: "POST",
                        url: "Loan_Purge.aspx/DeleteLoanDetails",
                        data: JSON.stringify({ loan_master_id: $('#hdn_loan_master_id').val() }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            $('.loader').show();
                            //$('#txtSearch1').val('');
                            // obj.autosearch();
                            obj.loadtable();
                            $('#myModal').modal('hide');
                        },
                        failure: function (response) {
                        }
                    });

                },
                buttonYes1: function () {
                    $('.loader').show();
                    $.ajax({
                        type: "POST",
                        url: "Loan_Purge.aspx/UpdateLoans",
                        data: JSON.stringify({ Loanids: Loan_Master_IDs }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            $('#myModal2').modal('hide');
                            toastr.success(data.d + ' Loans Purged');
                            obj.loadtable();

                        },
                        failure: function (response) {
                            $('.loader').show();
                        }
                    });

                },
                buttonsave: function () {
                    $('#btnModalSave').click(function () {
                        $('.loader').show();
                        var dataobj = {}
                        dataobj.Loan_Master_Id = $('#txtloanMasterID').val();
                        dataobj.Loan_Full_Id = $('#txtloanfullid').val();
                        dataobj.Crop_Year = $('#txtcropyear').val();
                        dataobj.Prev_yr_Loan_ID = $('#txtpreyearid').val();
                        dataobj.Loan_Officer_ID = $('#txtuserid').val();
                        dataobj.Farmer_ID = $('#txtfarmerid').val();
                        dataobj.Loan_Status = $('#txtloanstatus').val();
                        dataobj.Is_Latest = $('#ddlislatest').val();
                        var divModelSave = $('#divModelSave').show();
                        $.ajax({
                            type: "POST",
                            url: "Loan_Purge.aspx/Updateloandetails",
                            data: JSON.stringify({ lst: dataobj }),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (data) {
                                $('.loader').show();
                                obj.loadtable();
                                $('#myModal1').modal('hide');
                            },
                            failure: function (response) {
                            }
                        });
                    });
                },
                enableDisablePurgeButton: function () {
                    var countCheckedCheckboxes = $(".checked1").filter(':checked').length;
                    $("#spncheckedcount").text('Selected Loans:  ' + countCheckedCheckboxes);

                    if (countCheckedCheckboxes > 0) {
                        $('#btnPurge').removeClass('disabled');
                    } else {
                        $('#btnPurge').addClass('disabled');
                    }
                },
                loadtable: function () {
                    $('.loader').show();
                    var FromLoanID = $('#txtFromLoanID').val();
                    var ToLoanID = $('#txtToLoanID').val();
                    var cropyear = $('#ddlcropyear').val();
                    if (FromLoanID == "" && ToLoanID == "" && cropyear == "") {
                        $("#btnPurge").hide();
                    } else {
                        $("#btnPurge").show();
                    }
                    $('#ContentPlaceHolder1_lbl_table').text('');
                    $.ajax({
                        type: "POST",
                        url: "Loan_Purge.aspx/LoadTable",
                        data: JSON.stringify({ FromID: FromLoanID, ToID: ToLoanID, CropYear: cropyear }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {

                            $('#ContentPlaceHolder1_lbl_table').html(res.d);

                            $(".checked1").change(function (e) {
                                obj.enableDisablePurgeButton();
                            }); 
                            
                            if (FromLoanID == "" && ToLoanID == "" && cropyear == "") {
                                $("#spncheckedcount").hide();
                            } else {
                                $("#spncheckedcount").show();
                                obj.enableDisablePurgeButton();
                            }

                            $("tr td :checkbox").bind("click", function () {
                                var $this = $(this);
                                if ($this.is(':checked')) {
                                    var $row = $this.closest('tr');
                                    $row.css('color', 'red');
                                    //$this.siblings(':text').attr("disabled", "disabled");
                                }
                                else {
                                    var $row = $this.closest('tr');
                                    $row.css('color', 'black');
                                }

                                var countCheckedCheckboxes = $(".checked1").filter(':checked').length;
                                if (countCheckedCheckboxes == $('#tblLoanlist > tbody > tr').length) {
                                    $("#selectAllLoans").prop("checked", true);
                                } else {
                                    $("#selectAllLoans").prop("checked", false);
                                }
                            });

                            $("#selectAllLoans").bind("click", function () {
                                var $this = $(this);
                                if ($this.is(':checked')) {
                                    $('.checked1').prop( "checked", true );
                                }
                                else {
                                    $('.checked1').prop( "checked", false );
                                }

                                obj.enableDisablePurgeButton();
                            });
                            $('.loader').hide();
                        }
                    });
                }
            }
            obj.Init();
        });
        function manageRecord(id) {
            $('#hdn_loan_master_id').val(id);
            $('#divModelSave').show();
            $('#myModal').modal('show');
        }
        function manage(id) {
            $('#divUserID').show();
            $.ajax({
                type: "POST",
                url: "Loan_Purge.aspx/GetLoanFullIDs",
                data: JSON.stringify({ MasterId: id }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (res) {
                    var objdata = JSON.parse(res.d);
                    var data = objdata[0];
                    $('#txtloanMasterID').val(data.loan_master_id);
                    $('#txtloanfullid').val(data.loan_full_id);
                    $('#txtcropyear').val(data.Crop_Year);
                    $('#txtpreyearid').val(data.Prev_yr_Loan_ID);
                    $('#txtuserid').val(data.Loan_Officer_ID);
                    $('#txtfarmerid').val(data.Farmer_ID);
                    $('#txtloanstatus').val(data.Loan_Status);
                    $('#ddlislatest').val(data.is_Latest);

                    if (data.is_Latest) {
                        $('#ddlislatest').val('1');
                    }
                    else
                        $('#ddlislatest').val('0');
                }
            });
            $('#divModelSave1').show();
            $('#myModal1').modal('show');
        }
    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" runat="Server">
</asp:Content>



