﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Text;
using System.Web.UI.WebControls;

public partial class admin_Loan_Purge : System.Web.UI.Page
{
    static string dbKey = "gp_conn";
    static int count = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //lbl_table.Text = LoadTable("", "", "");
            count = 0;
        }
    }
    [WebMethod]
    public static string LoadTable(string FromID, string ToID, string CropYear)
    {
        string strQry = @"SELECT 
	                        Loan_Full_ID,
	                        Loan_Master_ID,
	                        Loan_ID,
	                        Legacy_Loan_ID,
	                        Crop_Year,
	                        Application_Date,
	                        Borrower_First_Name,
	                        Borrower_Last_Name,
	                        Total_Commitment,
	                        Loan_Status,
	                        Archived_Loan_Ind
                        FROM Loan_Master WHERE Test_Loan_Ind = 0 And Archived_Loan_Ind = 0";
        
        if (string.IsNullOrEmpty(FromID.Trim()) && string.IsNullOrEmpty(ToID.Trim()) && string.IsNullOrEmpty(CropYear.Trim()))
        {
            count = 0;
        } 
        else
        {
            if (!string.IsNullOrEmpty(FromID.Trim()) && !string.IsNullOrEmpty(ToID.Trim()))
            {
                strQry += " and Loan_ID between '" + FromID + "' and '" + ToID + "' ";
                count = 1;
            }

            if (!string.IsNullOrEmpty(CropYear.Trim()))
            {
                strQry += " and Crop_Year=" + CropYear + "";
                count = 1;
            }
        }        

        strQry += " order by Loan_Full_ID";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(strQry, dbKey);
        return GeneteateGrid(dt);
    }
    [WebMethod]
    public static List<string> GetLoanFullId(string searchVal)
    {
        string sql_qry = "select distinct Loan_Full_ID from Loan_Master where Loan_Full_ID like '%" + searchVal + "%'";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        List<string> emp = new List<string>();

        return (from DataRow row in dt.Rows
                select row["Loan_Full_ID"].ToString()
               ).ToList();
    }

    [WebMethod]
    public static string GetLoanByID(string loan_id)
    {
        string sql_qry = "select * from Loan_Master  where Loan_Full_ID='" + loan_id + "'";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
    }
    [WebMethod]
    public static string GetLoanFullIDs(string MasterId)
    {
        string sql_qry = "select loan_master_id,loan_full_id,Crop_Year, Prev_yr_Loan_ID, Loan_Officer_ID, Farmer_ID, Loan_Status, is_Latest from loan_master where loan_master_id='" + MasterId + "'";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
    }
    [WebMethod]
    public static void Updateloandetails(Dictionary<string, string> lst)
    {
        loanmaster objmanag = new loanmaster();
        string json = JsonConvert.SerializeObject(lst);
        loanmaster loandetails = JsonConvert.DeserializeObject<loanmaster>(json);
        string qry = "";
        qry = "update loan_master set Crop_Year='" + loandetails.Crop_Year + "',Prev_yr_Loan_ID='" + loandetails.Prev_yr_Loan_ID + "'," +
            "Loan_Officer_ID='" + loandetails.Loan_Officer_ID + "',Farmer_ID='" + loandetails.Farmer_ID + "',Loan_Status='" + loandetails.Loan_Status + "'," +
            "is_Latest='" + loandetails.Is_Latest + "' where loan_master_id='" + loandetails.Loan_Master_Id + "'";

        gen_db_utils.gp_sql_execute(qry, dbKey);
    }
    [WebMethod]
    public static string GeneteateGrid(DataTable dt)
    {
        StringBuilder sb = new StringBuilder();
        string str_return = "";
        if (dt != null && dt.Rows.Count > 0)
        {
            string head = "";
            string head2 = "";
            if (count != 0)
            {
                head = "<th style='text-align:center;'>" +
                           "<input type='checkbox' style='width:20px;height:17px;' id='selectAllLoans' name='checkedselectAllLoans'>" +
                        "</th>";
                head2 = "";
            }
            else
            {
                head2 = "<th></th>";
            }
            str_return += "<span style='color: green;'>Count of records : " + dt.Rows.Count + "</span><div> <table id='tblLoanlist' class='table table-striped table-bordered' >  "
                                 + "<thead> "
                                 + "     <tr> "
                                      + head
                                      + "       <th >Loan Full ID</th> "
                                      + "       <th >Loan ID</th> "
                                      + "       <th >Legacy Loan ID</th> "
                                      + "       <th >Crop Year</th> "
                                      + "       <th >Application Date</th> "
                                      + "       <th >Borrower</th> "
                                      + "       <th >Total Commitment</th> "
                                      + "       <th >Status</th> "
                                     + head2
                                 + "    </tr> "
                                 + "      </thead> "
                                 + "        <tbody> ";
            string checkbox = "";
            string colortr = "";
            string links = "";

            foreach (DataRow dtr1 in dt.Rows)
            {
                if (count != 0)
                {
                    if (dtr1["Archived_Loan_Ind"].ToString() == "0")
                    {
                        checkbox = "<td style='text-align:center;'><input type='checkbox' style='width:20px;height:17px;' class='checked1' id='" + dtr1["Loan_Master_ID"].ToString() + "' value='" + dtr1["Loan_Full_ID"].ToString() + "' name='checkedones'></td>";
                        colortr = "<tr style='color:red;'>";
                        links = "";
                    }
                    else if (dtr1["Archived_Loan_Ind"].ToString() == "1")
                    {
                        checkbox = "<td style='text-align:center;'><input type='checkbox' style='width:20px;height:17px;' class='checked1' id='" + dtr1["Loan_Master_ID"].ToString() + "' value='" + dtr1["Loan_Full_ID"].ToString() + "' name='checkedones'></td>";
                        colortr = "<tr style='color:black;'>";
                        links = "";
                    }
                }
                else
                {
                    colortr = "<tr>";
                    links = "<td><a href =' javascript:manageRecord(" + dtr1["Loan_Master_ID"] + ")' > Delete</a> " +
                                 "||<a href ='../admin/Loan_Sequence_Details.aspx?loan_full_id=" + dtr1["Loan_Full_ID"].ToString() + "'> Seq</a> " +
                                 "|| <a href =' javascript:manage(" + dtr1["Loan_Master_ID"] + ")' > Manage</a></td>"
                               + "</tr>"; ;
                }
                str_return += colortr
                               + checkbox
                               + "<td><a href ='../admin/Loan_Full_Details.aspx?loan_full_id=" + dtr1["Loan_Full_ID"].ToString() + "' > " + dtr1["Loan_Full_ID"].ToString() + "</a></td>"

                               + "<td>" + dtr1["Loan_ID"].ToString() + "</td>"

                               + "<td>" + dtr1["Legacy_Loan_ID"].ToString() + "</td>"

                               + "<td>" + dtr1["Crop_Year"].ToString() + "</td>"

                               + "<td>" + dtr1["Application_Date"].ToString() + "</td>"

                               + "<td>" + dtr1["Borrower_First_Name"].ToString() + " " + dtr1["Borrower_Last_Name"].ToString() + "</td>"

                               + "<td>" + dtr1["Total_Commitment"].ToString() + "</td>"
                               + "<td>" + dtr1["Loan_Status"].ToString() + "</td>"

                             + links;

            }

            str_return += "</tbody> "
                                    + " </table> "
                                      + "</div></div></div >";
            sb.Append(str_return);

        }
        else
        {
            str_return = "    No data available...";
        }
        return sb.ToString();
    }

    [WebMethod]
    public static void DeleteLoanDetails(string loan_master_id)
    {
        string qry = "Delete from Loan_Master where Loan_Master_ID='" + loan_master_id + "'";
        gen_db_utils.gp_sql_execute(qry, dbKey);

        //string qry = "Delete from Loan_Association where Loan_Full_Id = '" + loan_master_id + "'";
        //string qry = "Delete from Loan_Budget where Loan_Full_Id = '" + loan_master_id + "'";
        //string qry = "Delete from Loan_Collateral_Detail where Loan_Full_Id = '" + loan_master_id + "'";
        //string qry = "Delete from Loan_Comment where Loan_Full_Id = '" + loan_master_id + "'";
        //string qry = "Delete from Loan_Committee where Loan_Full_Id = '" + loan_master_id + "'";
        //string qry = "Delete from Loan_Condition where Loan_Full_Id = '" + loan_master_id + "'";
        //string qry = "Delete from Loan_Crop where Loan_Full_Id = '" + loan_master_id + "'";
        //string qry = "Delete from Loan_Crop_Practice where Loan_Full_Id = '" + loan_master_id + "'";
        //string qry = "Delete from Loan_Crop_Unit where Loan_Full_Id = '" + loan_master_id + "'";
        //string qry = "Delete from Loan_Exception where Loan_Full_Id = '" + loan_master_id + "'";
        //string qry = "Delete from Loan_Farm where Loan_Full_Id = '" + loan_master_id + "'";
        //string qry = "Delete from Loan_Marketing_Contract where Loan_Full_Id = '" + loan_master_id + "'";
        //string qry = "Delete from Loan_Other_Income where Loan_Full_Id = '" + loan_master_id + "'";
        //string qry = "Delete from Loan_Policies where Loan_Full_Id = '" + loan_master_id + "'";
        //string qry = "Delete from Loan_Q_Response where Loan_Full_Id = '" + loan_master_id + "'";
        //string qry = "Delete from Loan_Wfrp where Loan_Full_Id = '000104-00099'
        //--delete from Loan_Sub_Policies where Loan_Full_Id = '000104-00099'

    }

    [WebMethod]
    public static int UpdateLoans(List<string> Loanids)
    {
        int value = 0;
        if (Loanids.Count > 0)
        {
            string result = string.Join(", ", Loanids);
            string qry = "Update Loan_Master set Archived_Loan_Ind = 1 where Loan_Master_ID IN (" + result + ")";
            value = gen_db_utils.count_base_sql_execute(qry, dbKey);
        }
        else
        {
            return 0;
        }
        return value;
    }
}
public class loanmaster
{
    public string Loan_Master_Id { set; get; }
    public string Loan_Full_Id { set; get; }
    public string Crop_Year { set; get; }
    public string Prev_yr_Loan_ID { set; get; }
    public string Loan_Officer_ID { set; get; }
    public string Farmer_ID { set; get; }
    public string Loan_Status { set; get; }
    public string Is_Latest { set; get; }


    public loanmaster()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}