﻿<%@ Page Title="" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="Loan_Purge_New.aspx.cs" Inherits="admin_Loan_Purge_New" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style>
        .ui-jqgrid .ui-jqgrid-htable th div {
            height: auto;
            overflow: hidden;
            padding-right: 4px;
            padding-top: 2px;
            position: relative;
            vertical-align: text-top;
            white-space: normal !important;
        }

        .cvteste {
            padding-right: 14px !important;
        }

        .left {
            padding-left: 14px !important;
        }

        .right {
            padding-right: 14px !important;
        }

        #content {
            padding: 20px 14px !important;
        }

        .switch-controls {
            margin: 0 15px 0px !important;
            display: inline-flex;
        }

        .select-all-icon {
            text-align: center;
            height: 17px;
            position: relative;
            top: 3px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="system-admin-default">
        <div class="row">
            <div class="ddl-menu col-md-12">
                <div class="form-inline col-md-5">
                    <label for="ddlcropyear">Crop Year:</label>
                    <select class="form-control" id="ddlcropyear" style="margin-left: 5px; width: 230px;">
                        <option value="">Select</option>
                        <option value="2017">2017</option>
                        <option value="2018">2018</option>
                        <option value="2019">2019</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row switch-controls">
            <div class="switch-control disabled" id="editControl">
                <label for="editBtn">Edit</label>
                <div class="toggle-btn small">            
                    <input type="checkbox" id="editBtn" class="cb-value" />
                    <span class="round-btn"></span>
                </div>
            </div>
            <div class="switch-control disabled" id="enableControl">
                <label for="enableBtn">Enable</label>
                <div class="toggle-btn small">            
                    <input type="checkbox" id="enableBtn" class="cb-value" />
                    <span class="round-btn"></span>
                </div>
            </div>
        </div>
        <div class="loader"></div>
        <div class="row" style="position: relative;">
            <div id="truncateTable" style="position: absolute; top: -25px; display: none">
                <a href="javascript:truncatetable()" class='glyphicon glyphicon-trash' style='color:red'></a>
            </div>
            <div style="margin-left: 30px;">                
                <table id="jqGrid"></table>
                <div id="jqGridPager"></div>
            </div>
        </div>
    </div>
    <br />
    <br />
    <br />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" Runat="Server">
    <script type="text/javascript">  
        var DeleteRows = [];
        var selectedRows = [];
        var timer;
        var lastSelection;
        var canEdit = false;

        var isAllSelected = false;
        $(function () {
            var obj = {
                Init: function () {
                    $('.loader').show();
                    obj.bindGrid();
                    obj.getTableInfo();

                    $("#navbtnadd").addClass('disabled');

                    $("#navbtnsave").click(function () {
                        $('.loader').show();
                        obj.save();
                    });

                    $("#navbtnrefresh").click(function () {
                        $('.loader').show();
                        obj.bindGrid();
                    });

                    $("#navbtndownload").click(function () {
                        $("#jqGrid").jqGrid("exportToExcel", {
                            includeLabels: true,
                            includeGroupHeader: true,
                            includeFooter: true,
                            fileName: "Risk Other Queue.xlsx",
                            maxlength: 40 // maxlength for visible string data 
                        })
                    });

                    $("#txtSearchBar").on("keyup", function () {
                        var self = this;
                        obj.search(self);
                    });

                    $('#navbtncolumns').click(function () {
                        $("#jqGrid").columnChooser({});
                    });

                    $('#editBtn').change(function () {
                        canEdit = $(this).is(':checked');
                        if (!canEdit) {
                            if (lastSelection != null) {
                                var grid = $("#jqGrid");
                                grid.jqGrid('saveRow', lastSelection);
                                grid.jqGrid('restoreRow', lastSelection);
                                lastSelection = null;
                            }
                        }
                    });

                    $('#ddlcropyear').change(function () {
                        $('.loader').show();
                        obj.bindGrid();
                    });                    
                },
                getTableInfo: function () {
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "Loan_Purge_New.aspx/GetTableInfo",
                        data: JSON.stringify({}),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            var data = JSON.parse(res.d);

                            var table_info = data.Table_Info[0];
                            $('#txtTableName').html(table_info.Table_Name);
                            $('#txtLastUpdated').html(table_info.Last_Updated);
                            $('#txtRecordsCount').html(table_info.Records_Count);

                            if (data.CanEdit) {
                                $('#editControl').removeClass('disabled');
                            } else {
                                $('#editControl').addClass('disabled');
                            }

                            $('#enableControl').addClass('disabled');

                        }
                    });
                },
                bindGrid: function () {
                    var cropyear = $('#ddlcropyear').val();
                    $.ajax({
                        type: "POST",
                        url: "Loan_Purge_New.aspx/LoadTable",
                        data: JSON.stringify({ CropYear: cropyear || '' }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            DeleteRows = [];
                            selectedRows = [];

                            var resData = JSON.parse(data.d);
                            $('.loader').show();
                            for (var i = 0; i < resData.length; i++) {
                                resData[i].rowid = i + 1;
                            }
                            obj.loadgrid(resData);
                            $('.loader').hide();
                        }
                    });
                },
                loadgrid: function (data) {
                    $.jgrid.gridUnload("#jqGrid");
                    $("#jqGrid").jqGrid({
                        datatype: "local",
                        styleUI: 'Bootstrap',
                        data: data,
                        colModel: [
                            { label: 'rowid', name: 'rowid', width: 75, editable: false, key: true, hidden: true, hidedlg: true },
                            { label: '', name: '', align: "center", sortable: false, classes: "center", width: 90, formatter: obj.checkbox, exportcol: false, hidedlg: true },
                            { label: 'Loan_Master_ID', name: 'Loan_Master_ID', align: "left", classes: "left", width: 150, editable: false, hidden: true, hidedlg: true },
                            {
                                label: 'Loan Full ID',
                                name: 'Loan_Full_ID',
                                width: 120,
                                align: "center",
                                classes: "center",
                                editable: false
                            },
                            { label: 'Loan ID', name: 'Loan_ID', align: "center", classes: "center", width: 90, editable: false },
                            { label: 'Legacy Loan ID', name: 'Legacy_Loan_ID', align: "center", classes: "center", width: 120, editable: false },
                            { label: 'Crop Year', name: 'Crop_Year', align: "center", classes: "center", width: 90, editable: false },
                            { label: 'Application Date', name: 'Application_Date', align: "left", classes: "left", width: 120, editable: false },
                            { label: 'Borrower', name: 'Borrower_Name', align: "left", classes: "left", width: 240, editable: false },
                            { label: 'Total Commitment', name: 'Total_Commitment', align: "right", classes: "right", width: 150, editable: false, formatter: 'amountFormatter' },
                            { label: 'Status', name: 'Loan_Status', align: "center", classes: "center", width: 90, editable: false },
                            { label: 'Actionstatus', name: 'Actionstatus', width: 70, editable: false, hidden: true, exportcol: false, hidedlg: true },
                            { label: '', name: '', width: 60, align: 'center', formatter: obj.deleteLink, exportcol: false, hidedlg: true },
                        ],
                        viewrecords: true,
                        loadonce: true,
                        restoreAfterSelect: false,
                        saveAfterSelect: true,
                        sortable: true,
                        onSelectRow: obj.edit,
                        onPaging: obj.changePage,
                        height: 'auto',
                        rowNum: 100,
                        pager: "#jqGridPager",
                        loadComplete: function () {
                            var width = $('#gbox_jqGrid').width();
                            $('#truncateTable').css('left', width - 3 + 'px');
                            $('#truncateTable').show();

                            var isAdded = false;
                            var $grid = $("#jqGrid");
                            $grid.closest("div.ui-jqgrid-view")
                                .find("div.ui-jqgrid-hdiv table.ui-jqgrid-htable tr.ui-jqgrid-labels > th.ui-th-column > div.ui-jqgrid-sortable")
                                .each(function (index) {
                                    var idPrefix = "jqgh_" + $grid[0].id + "_";

                                    var id = $(this).attr('id');

                                    if (id == idPrefix && !isAdded) {
                                        $(this).find('i').remove();

                                        $('<i>')
                                            .addClass("fa")
                                            .addClass("fa-square-o")
                                            .addClass("fa-lg")
                                            .addClass('select-all-icon')
                                            .attr('title', 'Select/Deselect All Loans')
                                            .appendTo(this)
                                            .click(function (e) {
                                                selectAll();
                                                return false;
                                            });

                                        isAdded = true;
                                    }
                                });

                            $('.addCheckbox').change(function () {
                                toggleCheckbox();
                                return false;
                            });
                        }
                    });
                },
                save: function () {
                    $('.loader').show();
                    var allrows = [];

                    for (var i = 0; i < DeleteRows.length; i++) {
                        allrows.push(DeleteRows[i].Loan_Master_ID);
                    }

                    $.ajax({
                        type: "POST",
                        url: "Loan_Purge_New.aspx/UpdateLoans",
                        data: JSON.stringify({ Loanids: allrows }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            toastr.success("Saved Sucessful");
                            if (timer) { clearTimeout(timer); }
                            timer = setTimeout(function () {
                                location.reload();
                            }, 1000);
                        },
                        failure: function (response) {
                            $('.loader').hide();
                            var val = response.d;
                            toastr.warning(val);
                        }
                    });
                },
                search: function (self) {
                    if (timer) { clearTimeout(timer); }
                    timer = setTimeout(function () {
                        obj.changePage();
                        $("#jqGrid").jqGrid('filterInput', self.value);
                        var length = jQuery("#jqGrid").jqGrid('getGridParam', 'records');
                        $('#txtRecordsCount').html('Records Count: ' + length);
                    }, 0);
                },
                changePage: function () {
                    lastSelection = "";
                    var grid = $("#jqGrid");
                    var id = $('.inline-edit-cell').parent().parent().prop('id');
                    if (id != undefined) {
                        grid.jqGrid('saveRow', id);
                        grid.jqGrid('restoreRow', id);
                        var row = grid.jqGrid('getRowData', id);
                        if (!jQuery.isEmptyObject(row)) {
                            var dataobj = jQuery("#jqGrid").jqGrid('getGridParam', 'data');
                            index = dataobj.findIndex(x => x.rowid == row.rowid);
                            if (row.rowid > 0 && row.Actionstatus != 1) {
                                row.Actionstatus = 2;
                                grid.jqGrid('getGridParam', 'data')[index] = row;
                            }
                        }
                    }
                },
                checkbox: function (cellValue, options, rowdata, action) {
                    return "<input type='checkbox' class='addCheckbox' data-master-id='" + rowdata.Loan_Master_ID + "' /> ";
                },
                deleteLink: function (cellValue, options, rowdata, action) {
                    return "<a href='javascript:deleteRecord(" + options.rowId + ")' class='glyphicon glyphicon-trash' style='color:red'></a>";
                }
            }
            obj.Init();

        });

        function toggleCheckbox() {
            selectedRows = [];
            $('.addCheckbox:checked').each(function () {
                var checkbox = $(this);
                var masterId = checkbox.data('masterId');
                selectedRows.push(masterId);
            });           

            updateSelectAll();
        }

        function updateSelectAll() {
            var total = $('.jqgrow.ui-row-ltr').length;
            var totalCheckboxes = $('.addCheckbox:checked').length;

            if (total == totalCheckboxes) {
                if (!isAllSelected) {
                    var icon = $('.select-all-icon');

                    isAllSelected = true;
                    icon.addClass('fa-check-square');
                    icon.removeClass('fa-square-o');
                }
            } else {
                if (isAllSelected) {
                    var icon = $('.select-all-icon');

                    isAllSelected = false;
                    icon.addClass('fa-square-o');
                    icon.removeClass('fa-check-square');
                }
            }
        }

        function selectAll () {
            if (canEdit) {
                var icon = $('.select-all-icon');
                if (!isAllSelected) {
                    isAllSelected = true;
                    icon.addClass('fa-check-square');
                    icon.removeClass('fa-square-o');
                    $('.addCheckbox').prop('checked', true);
                } else {
                    isAllSelected = false;
                    icon.addClass('fa-square-o');
                    icon.removeClass('fa-check-square');
                    $('.addCheckbox').prop('checked', false);
                }

                selectedRows = [];
                $('.addCheckbox:checked').each(function () {
                    var checkbox = $(this);
                    var masterId = checkbox.data('masterId');
                    selectedRows.push(masterId);
                });
            }
        }

        function truncatetable() {
            if (canEdit) {
                if (selectedRows.length > 0) {
                    var result = confirm("Are you sure you want to purge the selected Loan(s)?");

                    if (result == true) {
                        $('.loader').show();
                        $.ajax({
                            type: "POST",
                            url: "Loan_Purge_New.aspx/UpdateLoans",
                            data: JSON.stringify({ Loanids: selectedRows }),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (data) {
                                toastr.success("Saved Sucessful");
                                if (timer) { clearTimeout(timer); }
                                timer = setTimeout(function () {
                                    location.reload();
                                }, 1000);
                            },
                            failure: function (response) {
                                $('.loader').hide();
                                var val = response.d;
                                toastr.warning(val);
                            }
                        });
                    }
                } else {
                    alert('Please select Loan First');
                }
            }
        }

        function deleteRecord(id) {
            if (canEdit) {
                var grid = $('#jqGrid');
                var result = confirm("Are you sure you want to purge this Loan?");

                if (result == true) {
                    var ediId = $('.inline-edit-cell').parent().parent().prop('id');
                    grid.jqGrid('saveRow', ediId);
                    var rowData = grid.jqGrid('getRowData', ediId);
                    if (rowData.rowid > 0 && rowData.Actionstatus != 1) {
                        rowData.Actionstatus = 2;
                        grid.jqGrid('getGridParam', 'data')[ediId - 1] = rowData;
                    }

                    var row = grid.jqGrid('getRowData', id);
                    grid.jqGrid('delRowData', id);
                    if (row.Actionstatus != 1) {
                        DeleteRows.push(row);
                    }
                    var data = jQuery("#jqGrid").jqGrid('getGridParam', 'data');
                    for (var i = 0; i < data.length; i++) {
                        data[i].rowid = i + 1;
                    }
                    var curpage = parseInt($(".ui-pg-input").val());
                    jQuery('#jqGrid').jqGrid('clearGridData');
                    jQuery('#jqGrid').jqGrid('setGridParam', { data: data });
                    $("#jqGrid").trigger("reloadGrid", [{ page: curpage }]);
                    $("#jqGrid").trigger("reloadGrid", [{ page: curpage }]);
                    lastSelection = id;
                }
            }
        }
        function unique(list) {
            var result = [];
            $.each(list, function (i, e) {
                if ($.inArray(e, result) == -1) result.push(e);
            });
            return result;
        }
    </script>
</asp:Content>
