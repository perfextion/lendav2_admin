﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_Loan_Purge_New : BasePage
{
    public admin_Loan_Purge_New()
    {
        Table_Name = "Loan_Purge";
    }

    [WebMethod]
    public static string GetTableInfo()
    {
        Count_Query = @"DECLARE @Count int; SELECT @Count= Count(*) from Loan_Master WHERE Test_Loan_Ind = 0 And Archived_Loan_Ind = 0";
        return Get_Table_Info();
    }

    [WebMethod]
    public static string LoadTable(string CropYear)
    {
        string strQry = @"SELECT 
	                        Loan_Full_ID,
	                        Loan_Master_ID,
	                        Loan_ID,
	                        Legacy_Loan_ID,
	                        Crop_Year,
	                        CONVERT(varchar(20), Application_Date, 101) as [Application_Date],
	                        [dbo].[Get_Entity_Name](Borrower_Entity_Type_Code, Borrower_First_Name, Borrower_MI, Borrower_Last_Name) as [Borrower_Name],
	                        Total_Commitment,
	                        Loan_Status,
	                        Archived_Loan_Ind
                        FROM Loan_Master WHERE Test_Loan_Ind = 0 And Archived_Loan_Ind = 0";

        if (!string.IsNullOrEmpty(CropYear.Trim()))
        {
            strQry += " and Crop_Year=" + CropYear + "";
        }

        strQry += " order by Loan_Full_ID";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(strQry, dbKey);
        return JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static int UpdateLoans(List<string> Loanids)
    {
        int value = 0;
        if (Loanids.Count > 0)
        {
            string result = string.Join(", ", Loanids);
            string qry = "Update Loan_Master set Archived_Loan_Ind = 1 where Loan_Master_ID IN (" + result + ")";
            value = gen_db_utils.count_base_sql_execute(qry, dbKey);

            Update_Table_Audit();
        }
        else
        {
            return 0;
        }
        return value;
    }
}