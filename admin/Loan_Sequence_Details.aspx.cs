﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_Loan_Sequence_Details : System.Web.UI.Page
{
    static string dbKey = "gp_conn";
    string loan_full_id = "0";
    protected void Page_Load(object sender, EventArgs e)
    {
        string loan_full_id = Request.QueryString.GetValues("loan_full_id") == null ? "000101-000" : (Request.QueryString.GetValues("loan_full_id")[0].ToString());
        lbl_table.Text = LoadTable(loan_full_id);
    }
    public static string LoadTable(string loan_full_id)
    {
        string str_sql = "";
        string[] loandid = loan_full_id.Split('-');
        string loandsplid = loandid[0];        
        str_sql = "select loan_full_id,Crop_Year, Prev_yr_Loan_ID, Loan_Officer_ID, Farmer_ID, Borrower_ID, Loan_Type_Code, Loan_Status, is_Latest from loan_master where loan_full_id like '%" + loandsplid + "%' order by loan_full_id";

        DataTable dt = gen_db_utils.gp_sql_get_datatable(str_sql, dbKey);
        return GeneteateGrid(dt);
    }
    [WebMethod]
    public static string GeneteateGrid(DataTable dt1)
    {
        string str_return = "";
        if (dt1 != null && dt1.Rows.Count > 0)
        {
            str_return += "<div> <table id='tblActiveUsers' class='table table-striped table-bordered' >  "
                                 + "<thead> "
                                 + "     <tr> "
                                      + "<th>loan_full_id</th> "
                                      + "<th>Crop_Year</th> "
                                      + "<th>Prev_yr_Loan_ID</th> "
                                      + "<th>Loan_Officer_ID</th> "
                                      + "<th>Farmer_ID</th> "
                                      + "<th>Borrower_ID</th> "
                                      + "<th>Loan_Type_Code</th> "
                                      + "<th>Loan_Status</th> "
                                      + "<th>is_Latest</th> "
                                 + "    </tr> "
                                 + "      </thead> "
                                 + "        <tbody> ";

            foreach (DataRow dtr1 in dt1.Rows)
            {//,, , , , , , , 
                str_return += "<tr>"
                             + "<td>" + dtr1["loan_full_id"].ToString() + "</td>"
                             + "<td>" + dtr1["Crop_Year"].ToString() + "</td>"
                             + "<td>" + dtr1["Prev_yr_Loan_ID"].ToString() + "</td>"
                             + "<td>" + dtr1["Loan_Officer_ID"].ToString() + "</td>"
                             + "<td>" + dtr1["Farmer_ID"].ToString() + "</td>"
                             + "<td>" + dtr1["Borrower_ID"].ToString() + "</td>"
                             + "<td>" + dtr1["Loan_Type_Code"].ToString() + "</td>"
                             + "<td>" + dtr1["Loan_Status"].ToString() + "</td>"
                             + "<td>" + dtr1["is_Latest"].ToString() + "</td>"
                               + "</tr>";
            }

            str_return += " </tbody> "
                                    + " </table> "
                                      + "</div></div></div >";
        }
        else
        {
            str_return = "    No data available...";
        }
        return str_return;
    }
}