﻿<%@ Page Title="" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="Loans_List.aspx.cs" Inherits="admin_ReferenceAssociation" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href='../css/fullcalendar.min.css' rel='stylesheet' />
    <link href='../css/fullcalendar.print.min.css' rel='stylesheet' media='print' />
    <script src='../js/moment.min.js'></script>
    <div class="loader"></div>
    <div style="width: 1500px; margin-top: 15px">
        <div style="margin-left: 66.7%">
            <div class="form-inline">
                <div class="form-group">
                    <label for="txtSearch1" style="float: left; margin-top: 7px;">Search :</label>
                    <input type="text" class="form-control" name="search" id="txtSearch1" style="float: left; margin-left: 5px; width: 230px;" />
                    <%--<label for="txtSearch">Loan Full ID :</label>
                <input type="text" class="form-control" name="search" id="txtSearch" style="width: 200px" />--%>
                    <div id="btnSearch" class="btn btn-md btn-primary" style="float: left; margin-left: 5px;">
                        Search
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-11" style="padding-left: 51px; width: 1394px; padding-top: 8px;">
            <asp:Label ID="lbl_table" runat="server" Text="lbl_table"></asp:Label>
        </div>
    </div>
    <div class="modal fade" id="myModal1" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <%--style="width:758px"--%>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Loan Details</h4>
                </div>
                <div class="modal-body">
                    <div class="row" style="margin-left: 5px">
                        <div id="divModelSave1">
                            <div class="col-md-12">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtSearch1">Loan Master id</label>
                                        <input type="text" id="txtloanMasterID" class="form-control" disabled="disabled" style="width: 200px" />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtSearch1">Loan Full id</label>
                                        <input type="text" id="txtloanfullid" disabled="disabled" class="form-control" style="width: 200px" />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtSearch1">Crop Year</label>
                                        <input type="text" id="txtcropyear" class="form-control" name="UserName" style="width: 200px" />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtSearch1">Pre Year Loan Id</label>
                                        <input type="text" id="txtpreyearid" class="form-control" name="UserName" style="width: 200px" />
                                    </div>
                                </div>


                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtSearch1">Loan Officer ID</label>
                                        <input type="text" id="txtuserid" class="form-control" name="UserName" style="width: 200px" />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtSearch1">Farmer_ID</label>
                                        <input type="text" id="txtfarmerid" class="form-control" disabled="disabled" name="UserName" style="width: 200px" />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtSearch1">Loan_Status</label>
                                        <input type="text" id="txtloanstatus" class="form-control" name="UserName" style="width: 200px" />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtSearch1">is_Latest</label>
                                        <select id="ddlislatest" class="form-control" style="width: 80%;">
                                            <option value="1">Yes</option>
                                            <option value="0">No</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-8">
                                </div>
                                <div class="col-md-4 text-center">
                                    <div class="btn btn-primary" id="btnModalSave">Save</div>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <%--style="width:758px"--%>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Confirm Delete</h4>
                </div>
                <div class="modal-body">
                    <div class="row" style="margin-left: 5px">
                        <div id="divModelSave">
                            <div>
                                <input type="hidden" id="hdn_loan_master_id" />
                                <h4>Are you sure, want to delete this loan details ?</h4>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-8">
                                </div>
                                <div class="col-md-4 text-center">
                                    <div class="btn btn-primary" id="btnModalYes">YES</div>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">NO</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(function () {
            $("#txtSearch").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "Loans_List.aspx/GetLoanFullId",
                        data: "{searchVal:'" + $("#txtSearch").val() + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            response(data.d);
                        }
                    });
                },
                select: function (event, ui) {
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "Loans_List.aspx/LoadTable",
                        data: "{loan_id:'" + ui.item.value + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            $('#ContentPlaceHolder1_lbl_table').html(data.d);
                        }
                    });
                }
            });
            var obj = {
                Init: function () {
                    obj.buttonsave();
                    $('.loader').hide();
                    $("#txtSearch").on("keyup", function () {
                        var value = $('#txtSearch').val();
                        if (value == '') {
                            $('.loader').show();
                            var self = this;
                            obj.loadtable();
                        }
                    });
                    $("#btnSearch").on("click", function () {
                        var self = this;
                        obj.autosearch(self);
                    });
                    //$("#txtSearch1").on("keyup", function () {
                    //    var self = this;
                    //    obj.autosearch(self);
                    //});
                    $('#btnModalYes').click(function () {
                        obj.buttonYes();
                    });
                },
                autosearch: function () {
                    var value = $('#txtSearch1').val();
                    $('.loader').show();
                    $.ajax({
                        type: "POST",
                        url: "Loans_List.aspx/LoadTable",
                        data: JSON.stringify({ keyword: value }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            $('#ContentPlaceHolder1_lbl_table').html(res.d);
                            $('.loader').hide();
                        }
                    });

                },
                buttonYes: function () {
                    $('.loader').show();
                    var divModelSave = $('#divModelSave').show();
                    $('#spnException').hide();
                    $.ajax({
                        type: "POST",
                        url: "Loans_List.aspx/DeleteLoanDetails",
                        data: JSON.stringify({ loan_master_id: $('#hdn_loan_master_id').val() }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            $('.loader').show();
                            //$('#txtSearch1').val('');
                            // obj.autosearch();
                            obj.loadtable();
                            $('#myModal').modal('hide');
                        },
                        failure: function (response) {
                        }
                    });

                },
                buttonsave: function () {
                    $('#btnModalSave').click(function () {
                        $('.loader').show();
                        var dataobj = {}
                        dataobj.Loan_Master_Id = $('#txtloanMasterID').val();
                        dataobj.Loan_Full_Id = $('#txtloanfullid').val();
                        dataobj.Crop_Year = $('#txtcropyear').val();
                        dataobj.Prev_yr_Loan_ID = $('#txtpreyearid').val();
                        dataobj.Loan_Officer_ID = $('#txtuserid').val();
                        dataobj.Farmer_ID = $('#txtfarmerid').val();
                        dataobj.Loan_Status = $('#txtloanstatus').val();
                        dataobj.Is_Latest = $('#ddlislatest').val();
                        var divModelSave = $('#divModelSave').show();
                        $.ajax({
                            type: "POST",
                            url: "Loans_List.aspx/Updateloandetails",
                            data: JSON.stringify({ lst: dataobj }),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (data) {
                                $('.loader').show();
                                obj.loadtable();
                                $('#myModal1').modal('hide');
                            },
                            failure: function (response) {
                            }
                        });
                    });
                },
                loadtable: function () {
                    $('.loader').show();
                    var loanid = $('#txtSearch1').val();
                    $('#ContentPlaceHolder1_lbl_table').text('');
                    $.ajax({
                        type: "POST",
                        url: "Loans_List.aspx/LoadTable",
                        data: JSON.stringify({ keyword: loanid }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            $('.loader').show();
                            $('#ContentPlaceHolder1_lbl_table').html(res.d);
                            $('.loader').hide();
                        }
                    });

                }
            }
            obj.Init();
        });
        function manageRecord(id) {
            $('#hdn_loan_master_id').val(id);
            $('#divModelSave').show();
            $('#myModal').modal('show');
        }
        function manage(id) {
            $('#divUserID').show();
            $.ajax({
                type: "POST",
                url: "Loans_List.aspx/GetLoanFullIDs",
                data: JSON.stringify({ MasterId: id }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (res) {
                    var objdata = JSON.parse(res.d);
                    var data = objdata[0];

                    $('#txtloanMasterID').val(data.loan_master_id);
                    $('#txtloanfullid').val(data.loan_full_id);
                    $('#txtcropyear').val(data.Crop_Year);
                    $('#txtpreyearid').val(data.Prev_yr_Loan_ID);
                    $('#txtuserid').val(data.Loan_Officer_ID);
                    $('#txtfarmerid').val(data.Farmer_ID);
                    $('#txtloanstatus').val(data.Loan_Status);
                    $('#ddlislatest').val(data.is_Latest);

                    if (data.is_Latest) {
                        $('#ddlislatest').val('1');
                    }
                    else
                        $('#ddlislatest').val('0');
                }
            });
            $('#divModelSave1').show();
            $('#myModal1').modal('show');
        }
    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" runat="Server">
    <script>
        $('#navbtnadd').addClass('disabled');
        $('#navbtnsave').addClass('disabled');
        $('#navbtndownload').addClass('disabled');
        $('#navbtnrefresh').addClass('disabled');
    </script>
</asp:Content>



