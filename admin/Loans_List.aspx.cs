﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Text;
using System.Web.UI.WebControls;

public partial class admin_ReferenceAssociation : System.Web.UI.Page
{
    static string dbKey = "gp_conn";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            lbl_table.Text = LoadTable("");
        }
    }
    [WebMethod]
    public static string LoadTable(string keyword)
    {
        string strQry = "";
        strQry = "select top 100 * from Loan_Master ";
        if (!string.IsNullOrEmpty(keyword.Trim()))
            strQry += " Where (Legacy_Loan_ID like '%" + keyword + "%' OR Loan_Full_ID like '%" + keyword + "%' " +
                "OR Borrower_First_Name like '%" + keyword + "%'OR Borrower_Last_Name like '%" + keyword + "%') ";
        strQry += " order by Loan_Full_ID";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(strQry, dbKey);
        return GeneteateGrid(dt);
    }
    [WebMethod]
    public static List<string> GetLoanFullId(string searchVal)
    {
        string sql_qry = "select distinct Loan_Full_ID from Loan_Master where Loan_Full_ID like '%" + searchVal + "%'";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        List<string> emp = new List<string>();

        return (from DataRow row in dt.Rows
                select row["Loan_Full_ID"].ToString()
               ).ToList();
    }

    [WebMethod]
    public static string GetLoanByID(string loan_id)
    {
        string sql_qry = "select * from Loan_Master  where Loan_Full_ID='" + loan_id + "'";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
    }
    [WebMethod]
    public static string GetLoanFullIDs(string MasterId)
    {
        string sql_qry = "select loan_master_id,loan_full_id,Crop_Year, Prev_yr_Loan_ID, Loan_Officer_ID, Farmer_ID, Loan_Status, is_Latest from loan_master where loan_master_id='" + MasterId + "'";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
    }
    [WebMethod]
    public static void Updateloandetails(Dictionary<string, string> lst)
    {
        loanmaster1 objmanag = new loanmaster1();
        string json = JsonConvert.SerializeObject(lst);
        loanmaster1 loandetails = JsonConvert.DeserializeObject<loanmaster1>(json);
        string qry = "";
        qry = "update loan_master set Crop_Year='" + loandetails.Crop_Year + "',Prev_yr_Loan_ID='" + loandetails.Prev_yr_Loan_ID + "'," +
            "Loan_Officer_ID='" + loandetails.Loan_Officer_ID + "',Farmer_ID='" + loandetails.Farmer_ID + "',Loan_Status='" + loandetails.Loan_Status + "'," +
            "is_Latest='" + loandetails.Is_Latest + "' where loan_master_id='" + loandetails.Loan_Master_Id + "'";

        gen_db_utils.gp_sql_execute(qry, dbKey);
    }
    [WebMethod]
    public static string GeneteateGrid(DataTable dt)
    {
        StringBuilder sb = new StringBuilder();
        string str_return = "";
        if (dt != null && dt.Rows.Count > 0)
        {
            str_return += "<div> <table id='tblLoanlist' class='table table-striped table-bordered' >  "
                                 + "<thead> "
                                 + "     <tr> "
                                      + "       <th >Loan Full ID</th> "
                                      + "       <th >Legacy Loan ID</th> "
                                      + "       <th >Application Date</th> "
                                      + "       <th >Borrower</th> "
                                      + "       <th >Total Commitment</th> "
                                      + "       <th >Status</th> "
                                     + "      <th >  </th> "
                                 + "    </tr> "
                                 + "      </thead> "
                                 + "        <tbody> ";

            foreach (DataRow dtr1 in dt.Rows)
            {
                str_return += "<tr>"
                               + "<td>" + dtr1["Loan_Full_ID"].ToString() + "</td>"

                               + "<td>" + dtr1["Legacy_Loan_ID"].ToString() + "</td>"

                               + "<td>" + dtr1["Application_Date"].ToString() + "</td>"

                               + "<td>" + dtr1["Borrower_First_Name"].ToString() + " " + dtr1["Borrower_Last_Name"].ToString() + "</td>"

                               + "<td>" + dtr1["Total_Commitment"].ToString() + "</td>"
                               + "<td>" + dtr1["Loan_Status"].ToString() + "</td>"

                             + "<td><a href ='../admin/Loan_Full_Details_Old.aspx?loan_full_id=" + dtr1["Loan_Full_ID"].ToString() + "' > view</a>        " +
                                 "||      <a href =' javascript:manageRecord(" + dtr1["Loan_Master_ID"] + ")' > delete</a> " +
                                 "||<a href ='../admin/Loan_Sequence_Details.aspx?loan_full_id=" + dtr1["Loan_Full_ID"].ToString() + "' > Seq</a> " +
                                 "|| <a href =' javascript:manage(" + dtr1["Loan_Master_ID"] + ")' > Manage</a></td>"
                               + "</tr>";

            }

            str_return += "</tbody> "
                                    + " </table> "
                                      + "</div></div></div >";
            sb.Append(str_return);

        }
        else
        {
            str_return = "    No data available...";
        }
        return sb.ToString();
    }

    [WebMethod]
    public static void DeleteLoanDetails(string loan_master_id)
    {
        string qry = "Delete from Loan_Master where Loan_Master_ID='" + loan_master_id + "'";
       gen_db_utils.gp_sql_execute(qry, dbKey);

        //string qry = "Delete from Loan_Association where Loan_Full_Id = '" + loan_master_id + "'";
        //string qry = "Delete from Loan_Budget where Loan_Full_Id = '" + loan_master_id + "'";
        //string qry = "Delete from Loan_Collateral_Detail where Loan_Full_Id = '" + loan_master_id + "'";
        //string qry = "Delete from Loan_Comment where Loan_Full_Id = '" + loan_master_id + "'";
        //string qry = "Delete from Loan_Committee where Loan_Full_Id = '" + loan_master_id + "'";
        //string qry = "Delete from Loan_Condition where Loan_Full_Id = '" + loan_master_id + "'";
        //string qry = "Delete from Loan_Crop where Loan_Full_Id = '" + loan_master_id + "'";
        //string qry = "Delete from Loan_Crop_Practice where Loan_Full_Id = '" + loan_master_id + "'";
        //string qry = "Delete from Loan_Crop_Unit where Loan_Full_Id = '" + loan_master_id + "'";
        //string qry = "Delete from Loan_Exception where Loan_Full_Id = '" + loan_master_id + "'";
        //string qry = "Delete from Loan_Farm where Loan_Full_Id = '" + loan_master_id + "'";
        //string qry = "Delete from Loan_Marketing_Contract where Loan_Full_Id = '" + loan_master_id + "'";
        //string qry = "Delete from Loan_Other_Income where Loan_Full_Id = '" + loan_master_id + "'";
        //string qry = "Delete from Loan_Policies where Loan_Full_Id = '" + loan_master_id + "'";
        //string qry = "Delete from Loan_Q_Response where Loan_Full_Id = '" + loan_master_id + "'";
        //string qry = "Delete from Loan_Wfrp where Loan_Full_Id = '000104-00099'
        //--delete from Loan_Sub_Policies where Loan_Full_Id = '000104-00099'

    }
}
public class loanmaster1
{
    public string Loan_Master_Id { set; get; }
    public string Loan_Full_Id { set; get; }
    public string Crop_Year { set; get; }
    public string Prev_yr_Loan_ID { set; get; }
    public string Loan_Officer_ID { set; get; }
    public string Farmer_ID { set; get; }
    public string Loan_Status { set; get; }
    public string Is_Latest { set; get; }


    public loanmaster1()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}