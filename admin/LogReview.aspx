﻿<%@ Page Title="" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="LogReview.aspx.cs" Inherits="admin_LogReview" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container" style="padding-top: 25px;">
        <div class="form-horizontal">

            <div class="row" style="padding-left: 18px;">
                <div class="col-md-9">
                    <div class="form-group">
                        <label for="txtQuery">Query :</label>
                        <asp:TextBox runat="server" CssClass="form-control" ID="txtQuery" TextMode="MultiLine" Rows="5" cols="100" />
                    </div>
                    <div class="form-group">
                        <asp:Button Text="execute" runat="server" ID="btnGo" OnClick="btnGo_Click" CssClass="btn btn-primary" />
                        <%-- <asp:Label ID="lblchecking" Style="color: forestgreen" runat="server"></asp:Label>--%>
                        <asp:Label Text="" ID="lblError" runat="server" Style="color: red; font-size: 16px; font-weight: 500;" />

                    </div>
                </div>
                <div class="col-md-3">
                 <%--   <div id="divloader" class="loader"></div>--%>
                </div>

            </div>
            <div class="row">
                <div class="col-md-12">
                    <asp:Label ID="lbl_table" runat="server" Text="lbl_table"></asp:Label>
                </div>
            </div>
        </div>
    </div>
    <script>
        //$(function () {
        //    $('#divloader').hide();
        //    $('#btnGo_Click').click(function () {
        //        $('#divloader').show();
        //    })
        //})
    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" runat="Server">
</asp:Content>

