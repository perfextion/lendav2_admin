﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_LogReview : System.Web.UI.Page
{
    string dbKey = "gp_conn";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["UserID"] == null)
            {
                Response.Redirect("../Login.aspx");
            }
            else
            {

                txtQuery.Text = "select top 1000 * from logs order by log_id desc";
                //lbl_table.Text = LoadTable();
                lbl_table.Text = LoadTableByQuery(txtQuery.Text);

            }
        }
    }
    protected string LoadTable()
    {
        string str_return = "";
        string loggedInUser = Session["UserID"].ToString();


        string str_sql_1 = "select top 1000  Log_Id,Log_Section,Log_Message,Log_datetime,Exec_Time,Severity,Sql_Error,userID, "
                          + " Loan_Full_ID,Batch_ID,SourceName,SourceDetail from logs order by log_id desc ";
        DataTable dt1 = gen_db_utils.gp_sql_get_datatable(str_sql_1, dbKey);

        str_return += "<div> <table id='tblActiveUsers' class='table table-striped table-bordered' >  "
                              + "<thead> "
                              + "     <tr> "
                              + "       <th >Log_Id</th> "
                              + "       <th >Log_Section</th> "
                              + "       <th >Log_Message</th> "
                              + "       <th >Log_datetime</th> "
                              + "       <th >Exec_Time</th> "
                              + "       <th >Severity</th> "
                              + "       <th >Sql_Error</th> "
                              + "       <th >userID</th> "
                              + "       <th >Loan_Full_ID</th> "
                              + "       <th >Batch_ID</th> "
                              + "       <th >SourceName</th> "
                              + "       <th >SourceDetail</th> "
                              + "    </tr> "
                              + "      </thead> "
                              + "        <tbody> ";

        foreach (DataRow dtr1 in dt1.Rows)
        {

            str_return += "<tr>"

                                + "<td>" + dtr1["Log_Id"].ToString() + "</td>"
                                + "<td>" + dtr1["Log_Section"].ToString() + "</td>"
                                + "<td>" + dtr1["Log_Message"].ToString() + "</td>"
                                + "<td>" + dtr1["Log_datetime"].ToString() + "</td>"
                                + "<td>" + dtr1["Exec_Time"].ToString() + "</td>"
                                + "<td>" + dtr1["Severity"].ToString() + "</td>"
                                + "<td>" + dtr1["Sql_Error"].ToString() + "</td>"
                                + "<td>" + dtr1["userID"].ToString() + "</td>"
                                + "<td>" + dtr1["Loan_Full_ID"].ToString() + "</td>"
                                + "<td>" + dtr1["Batch_ID"].ToString() + "</td>"
                                + "<td>" + dtr1["SourceName"].ToString() + "</td>"
                                + "<td>" + dtr1["SourceDetail"].ToString() + "</td>"
                             + "</tr>";

        }

        str_return += " </tbody> "
                                + " </table> "
                                  + "</div></div></div >";

        return str_return;

    }
    protected string LoadTableByQuery(string strQry)
    {
        string str_return = "";
        lblError.Text = "";
        try
        {
            DataTable dt1 = gen_db_utils.gp_sql_get_datatable(strQry, dbKey);

            str_return += "<div> <table id='tblActiveUsers' class='table table-striped table-bordered' >  "
                                  + "<thead> "
                                  + "     <tr> "
                                  + "       <th >Log_Id</th> "
                                  + "       <th >Log_Section</th> "
                                  + "       <th >Log_Message</th> "
                                  + "       <th >Log_datetime</th> "
                                  + "       <th >Exec_Time</th> "
                                  + "       <th >Severity</th> "
                                  + "       <th >Sql_Error</th> "
                                  + "       <th >userID</th> "
                                  + "       <th >Loan_Full_ID</th> "
                                  + "       <th >Batch_ID</th> "
                                  + "       <th >SourceName</th> "
                                  + "       <th >SourceDetail</th> "
                                  + "    </tr> "
                                  + "      </thead> "
                                  + "        <tbody> ";

            foreach (DataRow dtr1 in dt1.Rows)
            {
                str_return += "<tr>"
                                    + "<td>" + dtr1["Log_Id"].ToString() + "</td>"
                                    + "<td>" + dtr1["Log_Section"].ToString() + "</td>"
                                    + "<td>" + dtr1["Log_Message"].ToString() + "</td>"
                                    + "<td>" + dtr1["Log_datetime"].ToString() + "</td>"
                                    + "<td>" + dtr1["Exec_Time"].ToString() + "</td>"
                                    + "<td>" + dtr1["Severity"].ToString() + "</td>"
                                    + "<td>" + dtr1["Sql_Error"].ToString() + "</td>"
                                    + "<td>" + dtr1["userID"].ToString() + "</td>"
                                    + "<td>" + dtr1["Loan_Full_ID"].ToString() + "</td>"
                                    + "<td>" + dtr1["Batch_ID"].ToString() + "</td>"
                                    + "<td>" + dtr1["SourceName"].ToString() + "</td>"
                                    + "<td>" + dtr1["SourceDetail"].ToString() + "</td>"
                                 + "</tr>";
            }

            str_return += " </tbody> "
                                    + " </table> "
                                      + "</div></div></div >";

        }
        catch (Exception)
        {
            lblError.Text = "Enter valid query";
        }
        return str_return;
    }

    protected void btnGo_Click(object sender, EventArgs e)
    {
        lbl_table.Text = LoadTableByQuery(txtQuery.Text);
        // ClientScript.RegisterStartupScript(this.GetType(), "alert", "$('#divloader').hide();", true);
    }
}