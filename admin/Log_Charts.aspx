﻿<%@ Page Title="" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="Log_Charts.aspx.cs" Inherits="admin_Log_Charts" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/series-label.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>

    <style>
        #container {
            min-width: 310px;
            max-width: 800px;
            height: 400px;
            margin: 0 auto
        }
      
    </style>

    <div class="row">
        <div class="form-inline">
            <div class="col-md-4">
            </div>
            <div class="col-md-6 text-align-right">

                <div class="form-group">
                    <label for="txtSearch">Search :</label>
                    <input type="text" class="form-control" name="search" id="txtSearch" style="width: 200px" />
                </div>

                <div class="form-group">
                    <select id="ddlDays" class="form-control" style="width: 200px;">
                        <option value="7">7 days</option>
                        <option value="30">30 days</option>
                        <option value="90">90 days</option>
                    </select>
                </div>
                <div class="form-group">
                    <input type="button" class="btn" id="btnSearch" value="Search" />
                </div>
            </div>
        </div>
    </div>
    <div class="loader"></div>

    <div class="row" id="container">
    </div>

    <script type="text/javascript">

        var localdata;
        var days = ['0', '1', '2', '3', '4', '5', '6'];
        var thirtydays = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30'];

        GetChartData();


        $("#btnSearch").on("click", function () {
            
            $('.loader').show();
            var data = $("#ddlDays").val();
            var d = [];
            for (var i = 1; i <= parseInt(data); i++) {
                d.push(i);
            }
            days = d;
            GetChartData();
        });

        function GetChartData() {

            var searchValue = $('#txtSearch').val().replace(/"/g, '\\"').replace(/'/g, "\\'");
            var weeks = getDates();

            $.ajax({
                type: "POST",
                url: "Log_Charts.aspx/GetLogDetails",
                data: "{days:" + $('#ddlDays').val() + ", searchText:'" + searchValue + "',weekDays:'" + weeks + "' }",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    
                    if (data != null && data.d != undefined) {
                        var dataSplit = data.d.split('&&&&');
                        var datesArry = dataSplit[0].split(",");
                        var valuesArry = dataSplit[1];
                        var localdata = [{
                            name: 'logs',
                            data: JSON.parse(valuesArry)
                        }];
                        loadCharts(localdata, $("#ddlDays").val(), datesArry);
                    }
                    $('.loader').hide();
                }
            });
        }
        function loadCharts(data, noOfdays, datesArry) {
            Highcharts.chart('container', {

                title: {
                    text: 'Logs, last ' + noOfdays + ' days'
                },
                xAxis: {
                    //tickInterval: 7,
                    categories: datesArry
                },
                yAxis: {
                    title: {
                        text: 'Number of Logs'
                    }
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle'
                },
                plotOptions: {
                    series: {
                        label: {
                            connectorAllowed: false
                        },
                        pointStart: 0

                    }
                },
                series: data,
                responsive: {
                    rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            }
                        }
                    }]
                }
            });
        }





        function getDates() {
            
            var result = [];
            var oldendDate;

            for (i = 0; i < 13; i++) {
                if (i == 0) {
                    var today = new Date();
                    var newdate = new Date();
                    newdate.setDate(newdate.getDate() - 7);
                } else {
                    today = oldendDate;
                    var newdate = new Date(today);
                    newdate.setDate(newdate.getDate() - 7);
                }

                oldendDate = newdate;
                var dat = {};
                dat.startDate = (newdate.getMonth() + 1) + '/' + newdate.getDate() + '/' + newdate.getFullYear()
                dat.endDate = (today.getMonth() + 1) + '/' + today.getDate() + '/' + today.getFullYear();

                result.push(dat)
            }

            return JSON.stringify(result.reverse());
        }
    </script>


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" runat="Server">
</asp:Content>

