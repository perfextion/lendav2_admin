﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;

public partial class admin_Log_Charts : System.Web.UI.Page
{
    static string dbKey = "gp_conn";
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    //[WebMethod]
    //public static string GetLogDetails(int days, string searchText)
    //{
    //    if (searchText.Contains("'"))
    //    {
    //        searchText = searchText.Replace("'", "''");
    //    }
    //    string sql_qry = "";
    //    DataTable dt = new DataTable();
    //    if (days == 90)
    //    {
    //        string sql = "declare @sDate datetime, "
    //                 + "@eDate datetime; "
    //                + " select @sDate = CONVERT(char(10),DATEADD(day, -90, CONVERT(char(10), GetDate(), 101)),101),"
    //                + "     @eDate = CONVERT(char(10), GetDate(),101) ;"
    //                + " ; with cte as "
    //                + "  ( "
    //                + "  select @sDate StartDate,  "
    //                + " DATEADD(wk, DATEDIFF(wk, 0, @sDate), 6) EndDate "
    //                + "union all "
    //                + "select dateadd(ww, 1, StartDate), "
    //                + " dateadd(ww, 1, EndDate) "
    //                + " from cte "
    //                + "where dateadd(ww, 1, StartDate) <= @eDate "
    //                + ") "
    //                + "select * "
    //                + "from cte";

    //        DataTable dtRecords = gen_db_utils.gp_sql_get_datatable(sql, dbKey);
    //        DataTable dtNew = new DataTable();

    //        if (dtRecords.Rows.Count > 0)
    //        {
    //            dt.Columns.Add("logCount", typeof(string));
    //            dt.Columns.Add("logDate", typeof(string));
    //            for (int i = 0; i < dtRecords.Rows.Count - 1; i++)
    //            {
    //                string date = Convert.ToDateTime(dtRecords.Rows[i][1]).ToString("MM/dd/yyyy");                    
    //                sql_qry = "select coalesce(sum(logCount),0) logCount from ( select count(log_Message) logCount,logDate from(select log_Message, CONVERT(char(10), Log_datetime, 101) as logDate from Logs   where Log_Section like '%" + searchText + "%' or Log_Message like '%" + searchText + "%' and Log_datetime between DATEADD(week, -1, CONVERT(char(10), '" + date + "', 101)) and CONVERT(char(10), '" + date + "',101) ) x  group by logDate 	)  z";
    //                dtNew = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);

    //                DataRow dr = dt.NewRow();
    //                dr[0] = dtNew.Rows[0][0].ToString();
    //                dr[1] = date;
    //                dt.Rows.Add(dr);
    //            }
    //        }
    //    }
    //    else
    //    {
    //        if (!String.IsNullOrEmpty(searchText))
    //            sql_qry = " select count(log_Message) logCount,logDate from ( select log_Message, CONVERT(char(10), Log_datetime, 101) as logDate from Logs   where Log_Section like '%" + searchText + "%' or Log_Message like '%" + searchText + "%' and Log_datetime between DATEADD(day, -" + days + ", CONVERT(char(10), GetDate(), 101)) and CONVERT(char(10), GetDate(),101) ) x   group by logDate";
    //        else
    //            sql_qry = " select count(log_Message) logCount,logDate from ( select log_Message, CONVERT(char(10), Log_datetime, 101) as logDate from Logs   where Log_datetime between DATEADD(day, -" + days + ", CONVERT(char(10), GetDate(), 101)) and CONVERT(char(10), GetDate(),101) ) x   group by logDate";
    //        dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);

    //    }

    //    return DataTableToJsonstring(dt);
    //}

    [WebMethod]
    public static string GetLogDetails(int days, string searchText, string weekDays)
    {
        if (searchText.Contains("'"))
        {
            searchText = searchText.Replace("'", "''");
        }
        string sql_qry = "";
        DataTable dt = new DataTable();
        if (days == 90)
        {
            DataTable dtNew = new DataTable();
            dynamic lst = JsonConvert.DeserializeObject<dynamic>(weekDays);
            dt.Columns.Add("logCount", typeof(string));
            dt.Columns.Add("logDate", typeof(string));
            foreach (var item in lst)
            {
                string startDate = item["startDate"];
                string endDate = item["endDate"];

                //sql_qry = "select coalesce(sum(logCount),0) logCount from ( select count(log_Message) logCount,logDate from(select log_Message, CONVERT(char(10), Log_datetime, 101) as logDate from Logs   where Log_Section like '%" + searchText + "%' or Log_Message like '%" + searchText + "%' and Log_datetime between CONVERT(char(10), '" + startDate + "',101)  and CONVERT(char(10), '" + endDate + "',101) ) x  group by logDate 	)  z";
                if (!String.IsNullOrEmpty(searchText))
                    sql_qry = " select coalesce(sum(logCount),0) logCount from ( select count(log_Message) logCount,logDate from(select log_Message, CONVERT(char(10), Log_datetime, 101) as logDate from Logs   where (Log_Section like '%" + searchText + "%' or Log_Message like '%" + searchText + "%') and Log_datetime between CONVERT(char(10), '" + startDate + "',101)  and CONVERT(char(10), '" + endDate + "',101) ) x  group by logDate 	)  z";
                else
                    sql_qry = " select coalesce(sum(logCount),0) logCount from ( select count(log_Message) logCount,logDate from(select log_Message, CONVERT(char(10), Log_datetime, 101) as logDate from Logs   where  Log_datetime between CONVERT(char(10), '" + startDate + "',101)  and CONVERT(char(10), '" + endDate + "',101) ) x  group by logDate 	)  z";
                
                dtNew = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);

                DataRow dr = dt.NewRow();
                dr[0] = dtNew.Rows[0][0].ToString();
                dr[1] = startDate + " - " + endDate;
                dt.Rows.Add(dr);
            }
        }
        else
        {
            if (!String.IsNullOrEmpty(searchText))
                sql_qry = " select count(log_Message) logCount,logDate from ( select log_Message, CONVERT(char(10), Log_datetime, 101) as logDate from Logs   where Log_Section like '%" + searchText + "%' or Log_Message like '%" + searchText + "%' and Log_datetime between DATEADD(day, -" + days + ", CONVERT(char(10), GetDate(), 101)) and CONVERT(char(10), GetDate(),101) ) x   group by logDate";
            else
                sql_qry = " select count(log_Message) logCount,logDate from ( select log_Message, CONVERT(char(10), Log_datetime, 101) as logDate from Logs   where Log_datetime between DATEADD(day, -" + days + ", CONVERT(char(10), GetDate(), 101)) and CONVERT(char(10), GetDate(),101) ) x   group by logDate";
            dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);

        }

        return DataTableToJsonstring(dt);
    }


    public static string DataTableToJsonstring(DataTable dt)
    {
        DataSet ds = new DataSet();
        ds.Merge(dt);
        StringBuilder JsonValues = new StringBuilder();
        StringBuilder JsonDates = new StringBuilder();
        StringBuilder JsonRes = new StringBuilder();
        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            JsonValues.Append("[");
            //JsonDates.Append("[");
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                JsonValues.Append(ds.Tables[0].Rows[i][0].ToString() + ",");
                JsonDates.Append(ds.Tables[0].Rows[i][1].ToString() + ",");
            }
            JsonValues = JsonValues.Remove(JsonValues.Length - 1, 1);
            JsonDates = JsonDates.Remove(JsonDates.Length - 1, 1);
            JsonValues.Append("]");
            //JsonDates.Append("]");

            //JsonRes.Append("[");
            JsonRes.Append(JsonDates + "&&&&" + JsonValues);
            //JsonRes.Append("]");

            return JsonRes.ToString();


        }
        else
        {
            return null;
        }
    }
}