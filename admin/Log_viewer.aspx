﻿<%@ Page Title="" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="Log_viewer.aspx.cs" Inherits="admin_Log_viewer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" />
    <script src='../js/moment.min.js'></script>

    <script src='../js/fullcalendar/jquery.min.js'></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src='../js/fullcalendar/fullcalendar.min.js'></script>

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://rawgit.com/abodelot/jquery.json-viewer/master/json-viewer/jquery.json-viewer.js"></script>
    <link href="https://rawgit.com/abodelot/jquery.json-viewer/master/json-viewer/jquery.json-viewer.css" rel="stylesheet" />


    <div class="col-md-12 col-sm-4" style="margin-top: 15px">
        <div class="loader" style="margin-top: 10px;">
        </div>
        <div class="col-md-10">
            <div class="form-inline">
                <div class="form-group" style="float: left; margin-left: 1%;">
                    <label for="txtSearch1">Loan Full Id :</label>
                    <input type="text" class="form-control" name="search" id="txtSearch1" style="width: 230px;" />
                </div>
                <div class="form-group" style="float: left; margin-left: 1%;">
                    <div id="btnSearch" class="btn btn-md btn-primary" style="float: left;">
                        Search
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12" style="padding-left: 51px; padding-top: 8px;">
            <asp:Label ID="lbl_table" runat="server" Text="lbl_table"></asp:Label>
        </div>
    </div>
    <script>
        $(function () {
            $('.loader').show();
            var obj = {
                Init: function () {
                    $("#btnSearch").on("click", function () {
                        $('.loader').show();
                        obj.autosearch();
                        toastr.success("Please Wait Getting The Data");
                    });
                    $('.loader').hide();
                },
                autosearch: function () {
                    var value = $('#txtSearch1').val();
                    $.ajax({
                        type: "POST",
                        url: "Log_viewer.aspx/LoadTable",
                        data: JSON.stringify({ keyword: value }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            $('#ContentPlaceHolder1_lbl_table').html(res.d);
                            loadJsonViewer()
                            $('.loader').hide();
                        }
                    });

                },
            }
            obj.Init();
        });
        function loadJsonViewer() {
            $('.loader').show();
            $('.loadJsonViewer').each(function (index) {
                let data = '';
                data = $(this).text();
                if (data != undefined && data != "") {
                    $(this).jsonViewer(JSON.parse(data));
                    $(this).find('.json-toggle').click();
                }
            });
            $('.loader').hide();
        }
    </script>
    <div onload="loadJsonViewer()"></div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" runat="Server">
</asp:Content>

