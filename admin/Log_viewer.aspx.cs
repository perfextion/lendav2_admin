﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_Log_viewer : System.Web.UI.Page
{
    static string dbKey = "gp_conn";
    protected void Page_Load(object sender, EventArgs e)
    {
        string jobid = string.Empty;
        string officeid = string.Empty;
        if (!IsPostBack)
        {
            lbl_table.Text = LoadTable("002038-001");
            ClientScript.RegisterStartupScript(this.GetType(), "UpdateTime", "loadJsonViewer()", true);
        }

        //lbl_table.Text = LoadTable("", "");
    }
    [WebMethod]
    public static string GeneteateGrid(DataTable dt1)
    {
        string str_return = "";
        if (dt1 != null && dt1.Rows.Count > 0)
        {
            str_return += "<div> <table id='tblActiveUsers' class='table table-striped table-bordered' >  "
                                 + "<thead> "
                                 + "     <tr> "
                                      + "       <th >Log Id</th> "
                                      + "       <th >Loan Full ID</th> "
                                       + "       <th >Sql Error</th> "
                                       + "       <th >Count</th> "
                                      + "       <th >Log Section</th> "
                                      + "       <th >Log Message</th> "
                                      + "       <th >Log datetime</th> "
                                      + "       <th >User ID</th> "
                                 + "    </tr> "
                                 + "      </thead> "
                                 + "        <tbody> ";



            foreach (DataRow dtr1 in dt1.Rows)
            {
                dynamic deserialized = JsonConvert.DeserializeObject(dtr1["Sql_Error"].ToString());
                JObject rss = JObject.Parse(dtr1["Sql_Error"].ToString());

                //JArray assoc = (JArray)rss["Association"];

                int Assocnulls = (from p in rss["Association"]
                                  where (string)p["ActionStatus"] == null
                                  select (string)p["ActionStatus"]).Count();
                int Assoczero = (from p in rss["Association"]
                                 where (int?)p["ActionStatus"] == 0
                                 select (int?)p["ActionStatus"]).Count();
                int Assocone = (from p in rss["Association"]
                                where (int?)p["ActionStatus"] == 1
                                select (int?)p["ActionStatus"]).Count();
                string jsonval = "";
                if (dtr1["Sql_Error"].ToString() != "")
                { jsonval = "<td > <pre class='loadJsonViewer' id=preloanSetting style='padding-left: 20px;' jsondata=''>" + dtr1["Sql_Error"].ToString() + "</pre> </td>"; }
                else
                    jsonval = "<td></td>";
                str_return += "<tr>"
                             + "<td>" + dtr1["Log_Id"].ToString() + "</td>"
                             + "<td>" + dtr1["Loan_Full_ID"].ToString() + "</td>"
                                + jsonval
                             + "<td> <b>Association:</b><br/>Nulls :" + Assocnulls + "<br/>Zero's :" + Assoczero + "<br/>One's :" + Assocone + "</td>"
                             + "<td>" + dtr1["Log_Section"].ToString() + "</td>"
                             + "<td>" + dtr1["Log_Message"].ToString() + "</td>"
                             + "<td>" + dtr1["Log_datetime"].ToString() + "</td>"
                             + "<td>" + dtr1["userID"].ToString() + "</td>"
                               + "</tr>";


            }

            str_return += " </tbody> "
                                    + " </table> "
                                      + "</div></div></div >";

        }
        else
        {
            str_return = "    No data available...";
        }
        return str_return;
    }


    [WebMethod]
    public static string LoadTable(string keyword)
    {
        string str_sql = "";
        str_sql = " SELECT [Log_Id],[Log_Section],[Log_Message],[Log_datetime],[Sql_Error]," +
        "[userID],[Loan_Full_ID] FROM [dbo].[Logs] where [Log_Message]='sync object' ";

        if (!string.IsNullOrEmpty(keyword))
        {
            str_sql += " and  Loan_Full_ID ='" + keyword + "' ";
        }
        str_sql += " order by Log_Id";
        DataTable dt1 = gen_db_utils.gp_sql_get_datatable(str_sql, dbKey);
        return GeneteateGrid(dt1);
    }
}

