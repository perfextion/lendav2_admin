﻿<%@ Page Title="" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="Logs.aspx.cs" Inherits="admin_Logs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
   <style>

       .ui-jqgrid tr.jqgrow td {
           white-space: nowrap;
           overflow: hidden;
           text-overflow: ellipsis;
           vertical-align: text-top;
           padding-top: 2px;
           padding-bottom: 3px;
       }

       .example-element-detail {
           display: block;
       }

       .example-element-description {
           padding: 5px;
       }

       #content {
           padding: 14px !important;
       }

       .ui-jqgrid .ui-subgrid {
           cursor: text !important;
       }
   </style>
    <div class="">
		
        <div class="panel-body row-padding">          
            <div class="row">
                <div class="form-inline">
                    <div class="col-md-4">
                    </div>
                    <div class="col-md-6 text-align-right">
                        <div class="form-group">
                            <label for="ddlColNames">Search By :</label>
                            <select id="ddlColNames" class="form-control" style="width: 200px">                               
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="txtSearch">Enter :</label>
                            <input type="text" class="form-control" name="search" id="txtSearch" style="width: 200px" />
                        </div>
                        <div class="form-group">
                            <input type="button" class="btn" id="btnSearch" value="Search" />
                        </div>
                    </div>
                </div>
            </div>
			<div class="loader"></div>
            <br />
            <div class="row">
                <div style="margin-left: 15px">
                    <table id="jqGrid"></table>
                    <div id="jqGridPager"></div>
                </div>
            </div>
			 
        </div>
        <br />
        <br />
        <br />
    </div>

    <script type="text/javascript">           

        $(document).ready(function () {

            var last_detail_row;


            $('#ddlDBNames option[value="gp_conn"]').attr("selected", "selected");
            $('#lbldatabasename').text('LendaPlus_v1');

            $.ajax({
                type: "POST",
                url: "Logs.aspx/GetLogColumnNames",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (res) {
                    
                    $.each(res.d, function (data, value) {
                        $("#ddlColNames").append($("<option></option>").val(value).html(value));
                    })
                }
            });
            $.ajax({
                type: "POST",
                url: "Logs.aspx/GetLogDetails",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    BindGrid(data.d);
                }
            });     

            $("#btnSearch").on("click", function () {
                $('.loader').show();
                //var search = this.value;
                var search = $("#txtSearch").val().replace(/"/g, '\\"').replace(/'/g, "\\'");
                var ddlName = $('#ddlColNames').val();

                if (ddlName == "") {
                    alert("Please Select dropDown");
                    return false;
                    $('#ddlColNames').focus()
                }

                $.ajax({
                    type: "POST",
                    url: "Logs.aspx/GetLogDetailsbyID",
                    data: "{colName:'" + ddlName + "',serchText:'" + search + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        BindGrid(data.d);
						$('.loader').hide();
                    }
                });
            });

            function BindGrid(data) {
				$('.loader').show();
                $.jgrid.gridUnload("#jqGrid");
                $("#jqGrid").jqGrid({
                    datatype: "local",
                    styleUI: 'Bootstrap',
                    data: JSON.parse(data),
                    colModel: [
                        { label: 'Log Id', name: 'Log_Id', width: 90, key: true, sorttype: 'number' },
                        { label: 'Log Section', name: 'Log_Section', width: 200 },
                        { label: 'Log Message', name: 'Log_Message', width: 350 },
                        {
                            label: 'Log Date Time', name: 'Log_datetime', width: 150                            
                        },
                        { label: 'Exec Time', name: 'Exec_Time', width: 90 },
                        { label: 'Severity', name: 'Severity', width: 70, sorttype: 'number' },
                        { label: 'Sql Error', name: 'Sql_Error', width: 150 },
                        { label: 'User Id', name: 'userID', width: 70, sorttype: 'number' },
                        { label: 'Loan Full ID', name: 'Loan_Full_ID', width: 90 },
                        { label: 'Batch ID', name: 'Batch_ID', width: 120 },
                        { label: 'Source Name', name: 'SourceName', width: 120 },
                        { label: 'Source Detail', name: 'SourceDetail', width: 120 },
                        { label: 'Status', name: 'Status', width: 70 , sorttype: 'number'}
                    ],
                    viewrecords: true,
                    loadonce: true,
                    restoreAfterSelect: false,
                    saveAfterSelect: true,
                    sortable: true,
                    height: 'auto',
                    rowNum: 100,
                    pager: "#jqGridPager",
                    subGrid: true, // set the subGrid property to true to show expand buttons for each row
                    subGridRowExpanded: showChildGrid, // javascript function that will take care of showing the child grid
                });
				$('.loader').hide();
            }

            function showChildGrid(parentRowID, parentRowKey) {
                $('.loader').show();
                if (last_detail_row && parentRowKey != last_detail_row) {
                    $("#jqGrid").jqGrid('collapseSubGridRow', last_detail_row);
                }
                 $.ajax({
                    type: "POST",
                    url: "Logs.aspx/GetMoreDetails",
                    data: "{logId:'" + parentRowKey + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                     success: function (html) {                        
                        
                         last_detail_row = parentRowKey;

                         var div = $("#" + parentRowID);
                         div.append(html.d);
                         $('.loader').hide();
                    }
                });
            }
        });

    </script>


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" runat="Server">
</asp:Content>

