﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;

public partial class admin_Logs : System.Web.UI.Page
{
    static string dbKey = "gp_conn";
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [WebMethod]
    public static List<string> GetLogColumnNames()
    {
        string sql_qry = "SELECT Column_Name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Logs'";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);

        List<string> emp = new List<string>();
        emp = (from DataRow row in dt.Rows
               select row["Column_Name"].ToString()
               ).ToList();
        return emp;
    }

    [WebMethod]
    public static string GetLogDetails()
    {
        string sql_qry = @"select top 1000 
                              [Log_Id]
                              ,[Log_Section]
                              ,LEFT([Log_Message], 100) as [Log_Message]
                              ,FORMAT([Log_datetime], 'MM/dd/yyyy hh:mm:ss tt') as [Log_datetime]
                              ,[Exec_Time]
                              ,[Severity]
                              ,LEFT([Sql_Error], 100) as [Sql_Error]
                              ,[userID]
                              ,[Loan_Full_ID]
                              ,[Batch_ID]
                              ,[SourceName]
                              ,[SourceDetail]
                              ,[Status]  
                        from [dbo].[Logs] order by Log_Id desc";

        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static string GetLogDetailsbyID(string colName, string serchText)
    {
        if (serchText.Contains("'"))
        {
            serchText = serchText.Replace("'", "''");
        }

        string sql_qry = @"select top 1000  
                               [Log_Id]
                              ,[Log_Section]
                              ,LEFT([Log_Message], 100) as [Log_Message]
                              ,FORMAT([Log_datetime], 'MM/dd/yyyy hh:mm:ss tt') as [Log_datetime]
                              ,[Exec_Time]
                              ,[Severity]
                              ,LEFT([Sql_Error], 100) as [Sql_Error]
                              ,[userID]
                              ,[Loan_Full_ID]
                              ,[Batch_ID]
                              ,[SourceName]
                              ,[SourceDetail]
                              ,[Status] 
                        from [dbo].[Logs] ";

        if (!string.IsNullOrEmpty(serchText))
        {
            sql_qry += " where " + colName + " like '%" + serchText + "%' order by Log_Id desc";
        }

        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);

        return JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static string GetMoreDetails(string logId)
    {
        string sql_query = "SELECT Log_Id, Log_Section, Sql_Error, Log_Message FROM LOGS Where Log_Id = '" + logId + "'";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_query, dbKey);

        string details_html = string.Empty;

        details_html = @"<div class='example-element-detail'>
                            <div class='example-element-description'>
                              <strong>Log Id:</strong> {0}
                            </div>
                            <div class='example-element-description'>
                              <strong>Log Section:</strong> {1}
                            </div>
                            <div class='example-element-description'>
                              <strong>SQL Error:</strong> {2}
                            </div>
                            <div class='example-element-description'>
                              <strong>Log Message:</strong> {3}
                            </div>
                      </div>";

        DataRow dr = dt.Rows[0];

        details_html = string.Format(details_html, dr["Log_Id"], dr["Log_Section"], dr["Sql_Error"], dr["Log_Message"]);
        return details_html;
    }
}