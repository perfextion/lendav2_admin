﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Services;

public partial class admin_MasterBorrowerNew : BasePage
{
    public admin_MasterBorrowerNew()
    {
        Table_Name = "Master_Borrower";
    }

    [WebMethod]
    public static string GetTableInfo()
    {
        return Get_Table_Info();
    }

    [WebMethod]
    public static string GetListItems()
    {
        string sql_qry = @"select List_Item_Value, List_Item_Code, List_Item_Name, List_Group_Code from ref_list_item
                            where list_group_code in ( 'ENTITY_ID_TYPE', 'BORROWER_ENTITY_TYPE', 'PREF_CONTACT')
                            UNION
                            Select State_Abbrev as List_Item_Value, '' as List_Item_Code, State_Abbrev as List_Item_Name, 'STATE' as List_Group_Code From Ref_State";

        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static string GetMasterBorrowerData()
    {
        string sql_qry = @"select *, 0 as Action_Status from Master_Borrower";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static void SaveAssocType(dynamic ListItems)
    {
        foreach (var item in ListItems)
        {
            string qry = "";
            int val = Convert.ToInt32(item["Actionstatus"]);

            if (val == 1)
            {
                qry = @"INSERT INTO [dbo].[Master_Borrower]
                                   ([Borrower_ID_Type]
                                   ,[Borrower_SSN_Hash]
                                   ,[Borrower_Entity_Type_Code]
                                   ,[Borrower_First_Name]
                                   ,[Borrower_Last_Name]
                                   ,[Borrower_MI]
                                   ,[Borrower_Address]
                                   ,[Borrower_City]
                                   ,[Borrower_State_ID]
                                   ,[Borrower_Zip]
                                   ,[Borrower_Phone]
                                   ,[Borrower_email]
                                   ,[Borrower_DL_state]
                                   ,[Borrower_Dl_Num]
                                   ,[Borrower_DOB]
                                   ,[Borrower_Preferred_Contact_Ind]
                                   ,[Borrower_County_ID]
                                   ,[Borrower_Lat]
                                   ,[Borrower_Long]
                                   ,[Status])
                             VALUES
                               (
                                    '" + item["Borrower_ID_Type"] + @"'" +
                                   ",'" + item["Borrower_SSN_Hash"] + @"'" +
                                   ",'" + item["Borrower_Entity_Type_Code"] + @"'" +
                                   ",'" + item["Borrower_First_Name"].Replace("'", "''") + @"'" +
                                   ",'" + item["Borrower_Last_Name"].Replace("'", "''") + @"'" +
                                   ",'" + item["Borrower_MI"].Replace("'", "''") + @"'" +
                                   ",'" + item["Borrower_Address"].Replace("'", "''") + @"'" +
                                   ",'" + item["Borrower_City"].Replace("'", "''") + @"'" +
                                   ",'" + item["Borrower_State_ID"] + @"'" +
                                   ",'" + item["Borrower_Zip"] + @"'" +
                                   ",'" + item["Borrower_Phone"] + @"'" +
                                   ",'" + item["Borrower_email"].Replace("'", "''") + @"'" +
                                   ",'" + item["Borrower_DL_state"] + @"'" +
                                   ",'" + item["Borrower_Dl_Num"] + @"'" +
                                   ",'" + item["Borrower_DOB"] + @"'" +
                                   ",'" + item["Borrower_Preferred_Contact_Ind"] + @"'" +
                                   ",'" + item["Borrower_County_ID"] + @"'" +
                                   ",'" + item["Borrower_Lat"] + @"'" +
                                   ",'" + item["Borrower_Long"] + @"'" +
                                   ",'" + item["Status"] + @"'
                                )  ";

                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 2)
            {
                qry = @"UPDATE [dbo].[Master_Borrower]
                           SET  [Borrower_ID_Type] = '" + item["Borrower_ID_Type"] + @"' " +
                              ",[Borrower_SSN_Hash] = '" + item["Borrower_SSN_Hash"] + @"' " +
                              ",[Borrower_Entity_Type_Code] = '" + item["Borrower_Entity_Type_Code"] + @"' " +
                              ",[Borrower_First_Name] = '" + item["Borrower_First_Name"].Replace("'", "''") + @"' " +
                              ",[Borrower_Last_Name] = '" + item["Borrower_Last_Name"].Replace("'", "''") + @"' " +
                              ",[Borrower_MI] = '" + item["Borrower_MI"].Replace("'", "''") + @"' " +
                              ",[Borrower_Address] = '" + item["Borrower_Address"].Replace("'", "''") + @"' " +
                              ",[Borrower_City] = '" + item["Borrower_City"].Replace("'", "''") + @"' " +
                              ",[Borrower_State_ID] = '" + item["Borrower_State_ID"] + @"' " +
                              ",[Borrower_Zip] = '" + item["Borrower_Zip"] + @"' " +
                              ",[Borrower_Phone] = '" + item["Borrower_Phone"] + @"' " +
                              ",[Borrower_email] = '" + item["Borrower_email"].Replace("'", "''") + @"' " +
                              ",[Borrower_DL_state] = '" + item["Borrower_DL_state"] + @"' " +
                              ",[Borrower_Dl_Num] = '" + item["Borrower_Dl_Num"] + @"' " +
                              ",[Borrower_DOB] = '" + item["Borrower_DOB"].Replace("'", "''") + @"' " +
                              ",[Borrower_Preferred_Contact_Ind] = '" + item["Borrower_Preferred_Contact_Ind"] + @"' " +
                              ",[Borrower_County_ID] = '" + item["Borrower_County_ID"] + @"' " +
                              ",[Borrower_Lat] = '" + item["Borrower_Lat"] + @"' " +
                              ",[Borrower_Long] = '" + item["Borrower_Long"] + @"' " +
                              ",[Status] = '" + item["Status"] + @"' " +
                         "WHERE Master_Borrower_ID = '" + item["Master_Borrower_ID"] + "' ";

                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 3)
            {
                qry = "DELETE FROM Master_Borrower  where Master_Borrower_ID = '" + item["Master_Borrower_ID"] + "' ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
        }

        Update_Table_Audit();
    }
}