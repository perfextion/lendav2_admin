﻿<%@ Page Title="" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="Master_Borrower.aspx.cs" Inherits="admin_Master_Borrower" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .modal-body {
            border-bottom: 1px solid #e5e5e5;
        }
    </style>
    <div class="loader"></div>
    
    <div class="modal fade" id="myModal" style="margin-top: 5px" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <%--style="width:758px"--%>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Borrower Details</h4>
                </div>
                <div class="modal-body">
                    <div class="row" style="margin-left: 5px">
                        <div id="divModelSave">
                            <div class="col-md-12">
                                <div id="divBorrowerID" class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtFarmerId">Borrower ID</label>
                                        <input type="text" id="txtbid" class="form-control" disabled="disabled" name="Farmer Type ID" style="width: 200px" />
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtSearch">Borrower SSN Hash</label>
                                        <input type="text" id="txtBorrowerSSNHash" class="form-control" name="FarmerSSNHash" style="width: 200px" />
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtFarmerId">Borrower Entity Type Code</label>
                                        <input type="text" id="txtBorrowerentitytype" class="form-control" name="FarmerEntityType" style="width: 200px" />
                                    </div>
                                </div>


                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtFarmerfn">Borrower First Name</label>
                                        <input type="text" id="txtBorrowerfirstname" class="form-control" name="Farmer First Name" style="width: 200px" />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtSearch">Borrower MI</label>
                                        <input type="text" id="txtBorrowermi" class="form-control" name="Farmermi" style="width: 200px" />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtSearch">Borrower Last Name</label>
                                        <input type="text" id="txtBorrowerlastname" class="form-control" name="Farmerlastname" style="width: 200px" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row" style="margin-left: 10px; margin-top: 5px;">
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="txtSearch">Borrower Address</label>
                                    <textarea id="txtBorrowerfarmeraddress" class="form-control" style="width: 200px; height: 107px" name="Farmerfarmeraddress"></textarea>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="txtFarmerId">Borrower City</label>
                                    <input type="text" id="txtBorrowercity" class="form-control" name="Farmer City" style="width: 200px" />
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="txtSearch">Borrower State ID</label>
                                    <select id="ddlBorrowerstate" class="form-control" style="width: 80%;">
                                        <option value=" ">Select</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="txtSearch">Borrower Zip</label>
                                    <input type="text" id="txtBorrowerzip" class="form-control" name="Farmerzip" style="width: 200px;" />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="txtFarmerId">Borrower Phone</label>
                                    <input type="text" id="txtBorrowerphone" class="form-control" name="farmerphone" style="width: 200px" />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="txtSearch">Borrower Email</label>
                                    <input type="text" id="txtBorroweremail" class="form-control" name="farmeremail" style="width: 200px" />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="txtSearch">Borrower DL State</label>
                                    <select id="ddlBorrowerdlstate" class="form-control" style="width: 80%;">
                                        <option value=" ">Select</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="txtSearch">Borrower Dl Num</label>
                                    <input type="text" id="txtBorrowerdlnum" class="form-control" name="txtfarmerprefcontactind" style="width: 200px;" />
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="txtSearch">Borrower DOB</label>
                                    <input type="text" id="txtBorrowerdob" class="form-control datepicker" name="farmerdob" style="width: 200px" />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="txtSearch">Borrower Preferred Contact Ind</label>
                                    <input type="text" id="txtBprefind" class="form-control" name="txtyearbeginfarming" style="width: 200px;" />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="txtSearch">Borrower County ID</label>
                                    <input type="text" id="txtBCounid" class="form-control" name="yearbeginclient" style="width: 200px" />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="txtSearch">Borrower Lat</label>
                                    <input type="text" id="txtBlat" class="form-control" name="yearbeginclient" style="width: 200px" />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="txtSearch">Borrower Long</label>
                                    <input type="text" id="txtBLong" class="form-control" name="yearbeginclient" style="width: 200px" />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="txtSearch">Borrower ID Type</label>
                                    <input type="text" id="txtBorroweridtype" class="form-control" name="FarmerIDType" style="width: 200px" />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="txtSearch">Status</label>
                                    <select id="ddlstatus" class="form-control" style="width: 80%;">
                                        <option value="1">Active</option>
                                        <option value="0">InActive</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="txtSearch">IsDelete</label>
                                    <select id="ddlisdelete" class="form-control" style="width: 80%;">
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-8">
                            </div>
                            <div class="col-md-4 text-center">
                                <div class="btn btn-primary" id="btnModalSave">Save</div>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-11" style="padding-left: 51px; padding-top: 8px;">
            <asp:Label ID="lbl_table" runat="server" Text="lbl_table"></asp:Label>
        </div>
    </div>
    <script>
        $(function () {
            $(".datepicker").datepicker({ dateFormat: 'yy-mm-dd' });
        });
        $(function () {

            var obj = {
                Init: function () {
                    obj.buttonsave();
                    // obj.loadtable();
                    obj.getallstates();
                    $('#navbtnadd').on("click", function () {
                        obj.ClearModelData();
                        $('#divBorrowerID').hide();
                        $('#divModelSave').show();
                        $('#myModal').modal('show');

                    });
                    $("#navbtnrefresh").click(function () {
                        obj.loadtable();
                    });
                    $('.loader').hide();
                    $(document).keydown(function (event) {
                        if (event.keyCode == 27) {
                            $('#myModal').modal('hide');
                        }
                    });
                    $("#navbtndownload").click(function () {
                        exportTableToExcel('tblActiveUsers', 'Master_Borrower');
                    });
                },
                ClearModelData: function () {
                    $('#txtbid').val('');
                    $('#txtBorroweridtype').val('');
                    $('#txtBorrowerSSNHash').val('');
                    $('#txtBorrowerentitytype').val('');
                    $('#txtBorrowerfirstname').val('');
                    $('#txtBorrowerlastname').val('');
                    $('#txtBorrowermi').val('');
                    $('#txtBorrowerfarmeraddress').val('');
                    $('#txtBorrowercity').val('');
                    $('#ddlBorrowerstate').val('');
                    $('#txtBorrowerzip').val('');
                    $('#txtBorrowerphone').val('');
                    $('#txtBorroweremail').val('');
                    $('#ddlBorrowerdlstate').val('');
                    $('#txtBorrowerdlnum').val('');
                    $('#txtBorrowerdob').val('');
                    $('#txtBprefind').val('');
                    $('#txtBCounid').val('');
                    $('#txtBlat').val('');
                    $('#txtBLong').val('');
                    $('#ddlstatus').val('');
                    $('#ddlisdelete').val('');
                },
                buttonsave: function () {
                    $('#btnModalSave').click(function () {
                        $('.loader').show();
                        var dataobj = {};
                        dataobj.Master_Borrower_ID = $('#txtbid').val();
                        dataobj.Borrower_ID_Type = $('#txtBorroweridtype').val();
                        dataobj.Borrower_SSN_Hash = $('#txtBorrowerSSNHash').val();
                        dataobj.Borrower_Entity_Type_Code = $('#txtBorrowerentitytype').val();
                        dataobj.Borrower_First_Name = $('#txtBorrowerfirstname').val();
                        dataobj.Borrower_Last_Name = $('#txtBorrowerlastname').val();
                        dataobj.Borrower_MI = $('#txtBorrowermi').val();
                        dataobj.Borrower_Address = $('#txtBorrowerfarmeraddress').val();
                        dataobj.Borrower_City = $('#txtBorrowercity').val();
                        dataobj.Borrower_State_ID = $('#ddlBorrowerstate').val();
                        dataobj.Borrower_Zip = $('#txtBorrowerzip').val();
                        dataobj.Borrower_Phone = $('#txtBorrowerphone').val();
                        dataobj.Borrower_Email = $('#txtBorroweremail').val();
                        dataobj.Borrower_DL_State = $('#ddlBorrowerdlstate').val();
                        dataobj.Borrower_Dl_Num = $('#txtBorrowerdlnum').val();
                        dataobj.Borrower_DOB = $('#txtBorrowerdob').val();
                        dataobj.Borrower_Preferred_Contact_Ind = $('#txtBprefind').val();
                        dataobj.Borrower_County_ID = $('#txtBCounid').val();
                        dataobj.Borrower_Lat = $('#txtBlat').val();
                        dataobj.Borrower_Long = $('#txtBLong').val();
                        dataobj.Status = $('#ddlstatus').val();
                        dataobj.IsDelete = $('#ddlisdelete').val();
                        var divModelSave = $('#divModelSave').show();
                        $('#spnException').hide();
                        $.ajax({
                            type: "POST",
                            url: "Master_Borrower.aspx/UpdateBorrowerDetails",
                            data: JSON.stringify({ lst: dataobj }),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (data) {
                                $('.loader').show();
                                obj.loadtable();
                                $('#myModal').modal('hide');
                            },
                            failure: function (response) {
                            }
                        });
                    });
                },
                loadtable: function () {
                    $('#ContentPlaceHolder1_lbl_table').text('');
                    $.ajax({
                        type: "POST",
                        url: "Master_Borrower.aspx/LoadTable",
                        //  data: JSON.stringify({ }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            $('#ContentPlaceHolder1_lbl_table').html(res.d);
                            $('.loader').hide();
                        }
                    });
                },
                getallstates: function () {
                    $.ajax({
                        type: "POST",
                        url: "Master_Borrower.aspx/Getallstates",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            $.each(res.d, function (data, value) {
                                $("#ddlBorrowerstate").append($("<option></option>").val(data).html(value));
                                $("#ddlBorrowerdlstate").append($("<option></option>").val(data).html(value));
                            });
                        }
                    });
                }
            }
            obj.Init();
        });
        function manageRecord(id) {
            $('#divBorrowerID').show();
            $.ajax({
                type: "POST",
                url: "Master_Borrower.aspx/GetRefMasterBorrowerByID",
                data: JSON.stringify({ Borrower_ID: id }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (res) {
                    var objdata = JSON.parse(res.d);
                    var data = objdata[0];
                    $('#txtbid').val(data.Master_Borrower_ID);
                    $('#txtBorroweridtype').val(data.Borrower_ID_Type);
                    $('#txtBorrowerSSNHash').val(data.Borrower_SSN_Hash);
                    $('#txtBorrowerentitytype').val(data.Borrower_Entity_Type_Code);
                    $('#txtBorrowerfirstname').val(data.Borrower_First_Name);
                    $('#txtBorrowerlastname').val(data.Borrower_Last_Name);
                    $('#txtBorrowermi').val(data.Borrower_MI);
                    $('#txtBorrowerfarmeraddress').val(data.Borrower_Address);
                    $('#txtBorrowercity').val(data.Borrower_City);
                    $('#ddlBorrowerstate').val(data.Borrower_State_ID);
                    $('#txtBorrowerzip').val(data.Borrower_Zip);
                    $('#txtBorrowerphone').val(data.Borrower_Phone);
                    $('#txtBorroweremail').val(data.Borrower_Email);
                    $('#ddlBorrowerdlstate').val(data.Borrower_DL_State);
                    $('#txtBorrowerdlnum').val(data.Borrower_Dl_Num);
                    $('#txtBorrowerdob').val(data.Borrower_DOB);
                    $('#txtBprefind').val(data.Borrower_Preferred_Contact_Ind);
                    $('#txtBCounid').val(data.Borrower_County_ID);
                    $('#txtBlat').val(data.Borrower_Lat);
                    $('#txtBLong').val(data.Borrower_Long);
                    $('#ddlstatus').val(data.Status);
                    $('#ddlisdelete').val(data.IsDelete);
                    $('#divModelSave').show();
                    $('#myModal').modal('show');
                }
            });
        }
        $('#navbtnsave').addClass('disabled');
        $('#navbtncolumns').addClass('disabled');
    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" runat="Server">
</asp:Content>

