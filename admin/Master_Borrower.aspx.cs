﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_Master_Borrower : System.Web.UI.Page
{
    static string dbKey = "gp_conn";

    protected void Page_Load(object sender, EventArgs e)
    {
        lbl_table.Text = LoadTable();
    }

    [WebMethod]
    public static void UpdateBorrowerDetails(Dictionary<string, string> lst)
    {
        BorrowerMaster objmanag = new BorrowerMaster();
        string json = JsonConvert.SerializeObject(lst);
        BorrowerMaster objborrower = JsonConvert.DeserializeObject<BorrowerMaster>(json);
        string qry = "";
        if (objborrower.Master_Borrower_ID != "")
        {
            qry = "UPDATE Master_Borrower SET Borrower_ID_Type='" + objborrower.Borrower_ID_Type + "',[Borrower_SSN_Hash]= '" + objborrower.Borrower_SSN_Hash + "'," +
                "[Borrower_Entity_Type_Code]='" + objborrower.Borrower_Entity_Type_Code + "',[Borrower_First_Name]='" + objborrower.Borrower_First_Name + "'," +
                "[Borrower_Last_Name]='" + objborrower.Borrower_Last_Name + "',[Borrower_MI]='" + objborrower.Borrower_MI + "'," +
                "[Borrower_Address]='" + objborrower.Borrower_Address + "',[Borrower_City]='" + objborrower.Borrower_City + "'," +
                "[Borrower_State_ID]='" + objborrower.Borrower_State_ID + "',[Borrower_Zip]='" + objborrower.Borrower_Zip + "'," +
                "[Borrower_Phone]='" + objborrower.Borrower_Phone + "',[Borrower_Email]='" + objborrower.Borrower_Email + "'," +
                "[Borrower_DL_State]='" + objborrower.Borrower_DL_State + "',[Borrower_Dl_Num]='" + objborrower.Borrower_Dl_Num + "'," +
                "[Borrower_DOB]='" + objborrower.Borrower_DOB + "',[Borrower_Preferred_Contact_Ind]='" + objborrower.Borrower_Preferred_Contact_Ind + "'," +
                "[Borrower_County_ID]='" + objborrower.Borrower_County_ID + "',[Borrower_Lat]='" + objborrower.Borrower_Lat + "'," +
                "[Borrower_Long]='" + objborrower.Borrower_Long + "',[Status]='" + objborrower.Status + "',[IsDelete]='" + objborrower.IsDelete + "'" +
                "WHERE Master_Borrower_ID='" + objborrower.Master_Borrower_ID + "'";
        }
        else
        {
            qry = "insert into Master_Borrower (Borrower_ID_Type,Borrower_SSN_Hash,Borrower_Entity_Type_Code," +
                "Borrower_First_Name,Borrower_Last_Name,Borrower_MI,Borrower_Address,Borrower_City,Borrower_State_ID,Borrower_Zip," +
                "Borrower_Phone,Borrower_Email,Borrower_DL_State,Borrower_Dl_Num,Borrower_DOB,Borrower_Preferred_Contact_Ind," +
                "Borrower_County_ID,Borrower_Lat,Borrower_Long,Status,IsDelete) values('" + objborrower.Borrower_ID_Type + "'," +
                "'" + objborrower.Borrower_SSN_Hash + "','" + objborrower.Borrower_Entity_Type_Code + "','" + objborrower.Borrower_First_Name + "'," +
                "'" + objborrower.Borrower_Last_Name + "','" + objborrower.Borrower_MI + "','" + objborrower.Borrower_Address + "'," +
                "'" + objborrower.Borrower_City + "','" + objborrower.Borrower_State_ID + "','" + objborrower.Borrower_Zip + "'," +
                "'" + objborrower.Borrower_Phone + "','" + objborrower.Borrower_Email + "','" + objborrower.Borrower_DL_State + "'," +
                "'" + objborrower.Borrower_Dl_Num + "','" + objborrower.Borrower_DOB + "','" + objborrower.Borrower_Preferred_Contact_Ind + "'," +
                "'" + objborrower.Borrower_County_ID + "','" + objborrower.Borrower_Lat + "','" + objborrower.Borrower_Long + "'," +
                "'" + objborrower.Status + "','" + objborrower.IsDelete + "')";

        }
        gen_db_utils.gp_sql_execute(qry, dbKey);
    }

    [WebMethod]
    public static string LoadTable()
    {
        string str_sql = "";
        string str_return = "";

        //str_sql = "SELECT Master_Borrower_ID,Borrower_ID_Type,Borrower_SSN_Hash,Borrower_Entity_Type_Code," +
        //    "Borrower_First_Name,Borrower_Last_Name,Borrower_MI,Borrower_Address,Borrower_City,Borrower_State_ID," +
        //    "Borrower_Zip,Borrower_Phone,Borrower_Email,Borrower_DL_State,Borrower_Dl_Num,Borrower_DOB," +
        //    "Borrower_Preferred_Contact_Ind,Borrower_County_ID,Borrower_Lat,Borrower_Long,Status,IsDelete FROM Master_Borrower";

        str_sql = "SELECT a.Master_Borrower_ID,b.loan_full_id,a.Borrower_ID_Type,a.Borrower_SSN_Hash," +
            "a.Borrower_Entity_Type_Code,a.Borrower_First_Name,a.Borrower_Last_Name,a.Borrower_MI,a.Borrower_Address," +
            "a.Borrower_City,a.Borrower_State_ID,a.Borrower_Zip,a.Borrower_Phone,a.Borrower_Email,a.Borrower_DL_State," +
            "a.Borrower_Dl_Num,a.Borrower_DOB,a.Borrower_Preferred_Contact_Ind,a.Borrower_County_ID,a.Borrower_Lat," +
            "a.Borrower_Long,a.Status,a.IsDelete FROM Master_Borrower a LEFT JOIN(SELECT Borrower_ID, " +
            "loan_full_id = STUFF((SELECT ',' + loan_full_id FROM loan_master bb WHERE bb.Borrower_ID = aa.Borrower_ID " +
            "FOR XML PATH('')), 1, 1, '') FROM loan_master aa GROUP BY Borrower_ID)	b on " +
            "a.Master_Borrower_ID = b.Borrower_ID ";


        DataTable dt1 = gen_db_utils.gp_sql_get_datatable(str_sql, dbKey);
        if (dt1 != null && dt1.Rows.Count > 0)
        {
            str_return += "<span style='color: green;'>Count of records : " + dt1.Rows.Count + "</span><div> <table id='tblActiveUsers' class='table table-striped table-bordered' >  "
                              + "<thead> "
                              + "     <tr> "
                             + "      <th style='width: 238px;' > Borrower Details </th> "
                                         + "      <th > Loan Full Id </th> "
                             + "      <th > ID Type Details </th> "
                             + "      <th > Address Details </th> "
                             + "      <th > Contact Details </th> "
                              + "      <th  style='text-align:center;'> Action </th> "
                              + "    </tr> "
                              + "      </thead> "
                              + "        <tbody> ";
            string datetime = "";
            foreach (DataRow dtr1 in dt1.Rows)
            {
                //datetime = (string.IsNullOrEmpty(dtr1["Borrower_DOB"])) ? "" : Convert.ToDateTime(dtr1["Borrower_DOB"]).ToString("yyyy-MM-dd");

                if (dtr1["Borrower_DOB"].ToString() != "")
                {
                    datetime = Convert.ToDateTime(dtr1["Borrower_DOB"]).ToString("yyyy-MM-dd");
                }
                else
                {
                    datetime = "";
                }
                str_return += "<tr>"
                            + "<td> (" + dtr1["Master_Borrower_ID"].ToString() + "" +
                              ") <br>" + dtr1["Borrower_ID_Type"].ToString() + " " + dtr1["Borrower_SSN_Hash"].ToString() + "<br>" +
                              dtr1["Borrower_First_Name"].ToString() + " " + dtr1["Borrower_MI"].ToString() + " " + dtr1["Borrower_Last_Name"].ToString() + "<br>" +
                              datetime +
                              "</td>"
                              + "<td>" + dtr1["loan_full_id"].ToString() + "</td>"
                              + "<td>Entity :" + dtr1["Borrower_Entity_Type_Code"].ToString() +
                              //"<br>MI : " + dtr1["Borrower_MI"].ToString() +
                              "<br>DL Num : " + dtr1["Borrower_Dl_Num"].ToString() +
                              "<br>DL State : " + dtr1["Borrower_DL_State"].ToString() +
                              "</td>"

                             + "<td>" + dtr1["Borrower_Address"].ToString() +
                             "<br>" + dtr1["Borrower_City"].ToString() + " , " + dtr1["Borrower_State_ID"].ToString() + " , " + dtr1["Borrower_Zip"].ToString() +
                             "<br>" + dtr1["Borrower_Phone"].ToString() + "<br>" + dtr1["Borrower_Email"].ToString() +
                             "</td>"

                             + "<td>Contact_Ind : " + dtr1["Borrower_Preferred_Contact_Ind"].ToString() + "<br>" +
                             "County : " + dtr1["Borrower_County_ID"].ToString() + "<br>" +
                             "Long : " + dtr1["Borrower_Long"].ToString() + "<br>" +
                             "Status : " + dtr1["Status"].ToString() + "<br>" +
                             "IsDelete : " + dtr1["IsDelete"].ToString() + "<br>" +
                             "Lat : " + dtr1["Borrower_Lat"].ToString() + "</td>"

                            + "<td style='text-align:center;'><a href =' javascript:manageRecord(" + dtr1["Master_Borrower_ID"] + ")' > Manage</a></td>"

                              + "</tr>";
            }

            str_return += " </tbody> "
                                    + " </table> "
                                      + "</div></div></div >";
        }
        else
        {
            str_return = "    No data available...";
        }

        return str_return;

    }

    [WebMethod]
    public static string GetRefMasterBorrowerByID(string Borrower_ID)
    {
        string sql_qry = "SELECT Master_Borrower_ID,Borrower_ID_Type,Borrower_SSN_Hash,Borrower_Entity_Type_Code," +
            "Borrower_First_Name,Borrower_Last_Name,Borrower_MI,Borrower_Address,Borrower_City,Borrower_State_ID," +
            "Borrower_Zip,Borrower_Phone,Borrower_Email,Borrower_DL_State,Borrower_Dl_Num,Borrower_DOB," +
            "Borrower_Preferred_Contact_Ind,Borrower_County_ID,Borrower_Lat,Borrower_Long,Status,IsDelete " +
            "FROM Master_Borrower where Master_Borrower_ID='" + Borrower_ID + "'";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);

    }
    [WebMethod]
    public static Dictionary<string, string> Getallstates()
    {
        string sql_qry = "select State_Abbrev,CONCAT( State_ID,'.',State_Name) as State_Name,State_Abbrev from Ref_State order by State_ID";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);

        return dt.AsEnumerable().ToDictionary<DataRow, string, string>(row => row[2].ToString(),
                                       row => row[1].ToString());
    }
}
public class BorrowerMaster
{
    public string Master_Borrower_ID { set; get; }
    public string Borrower_ID_Type { set; get; }
    public string Borrower_SSN_Hash { set; get; }
    public string Borrower_Entity_Type_Code { set; get; }
    public string Borrower_First_Name { set; get; }
    public string Borrower_Last_Name { set; get; }
    public string Borrower_MI { set; get; }
    public string Borrower_Address { set; get; }
    public string Borrower_City { set; get; }
    public string Borrower_State_ID { set; get; }
    public string Borrower_Zip { set; get; }
    public string Borrower_Phone { set; get; }
    public string Borrower_Email { set; get; }
    public string Borrower_DL_State { set; get; }
    public string Borrower_Dl_Num { set; get; }
    public string Borrower_DOB { set; get; }
    public string Borrower_Preferred_Contact_Ind { set; get; }
    public string Borrower_County_ID { set; get; }
    public string Borrower_Lat { set; get; }
    public string Borrower_Long { set; get; }
    public string Status { set; get; }
    public string IsDelete { set; get; }
    public BorrowerMaster()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}

