﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_MaxUserPendingActions : System.Web.UI.Page
{
    static string dbKey = "gp_conn";
    protected void Page_Load(object sender, EventArgs e)
    {
        lbl_table.Text = LoadTable();
    }
    public static string LoadTable()
    {
        string str_sql = "";
        str_sql = "select  A.MAX_PA, LM.loan_full_id, LM.Loan_Officer_ID from loan_master " +
            "LM left join(select Max(sort_order) as MAX_PA, Loan_Full_ID from user_pending_action UP" +
            " join Ref_Pending_Action RP on RP.Ref_PA_ID = UP.Ref_PA_ID group by Loan_Full_ID) A on A.Loan_Full_ID = LM.Loan_Full_ID order by MAX_PA desc";

        DataTable dt1 = gen_db_utils.gp_sql_get_datatable(str_sql, dbKey);
        return GeneteateGrid(dt1);
    }
    [WebMethod]
    public static string GeneteateGrid(DataTable dt1)
    {
        string str_return = "";
        if (dt1 != null && dt1.Rows.Count > 0)
        {
            str_return += "<span style='color: green;'>Count of records : " + dt1.Rows.Count + "</span><div> <table id='tblActiveUsers' class='table table-striped table-bordered' >  "
                                 + "<thead> "
                                 + "     <tr> "
                                      + "<th>Max Pending Actions</th> "
                                      + "<th>Loan Full ID</th> "
                                      + "<th>Loan Officer ID</th> "
                                 + "    </tr> "
                                 + "      </thead> "
                                 + "        <tbody> ";

            foreach (DataRow dtr1 in dt1.Rows)
            {
                str_return += "<tr>"
                             + "<td>" + dtr1["MAX_PA"].ToString() + "</td>"
                             + "<td>" + dtr1["loan_full_id"].ToString() + "</td>"
                             + "<td>" + dtr1["Loan_Officer_ID"].ToString() + "</td>"
                               + "</tr>";
            }

            str_return += " </tbody> "
                                    + " </table> "
                                      + "</div></div></div >";

        }
        else
        {
            str_return = "    No data available...";
        }
        return str_return;
    }
}
