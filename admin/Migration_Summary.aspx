﻿<%@ Page Title="" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="Migration_Summary.aspx.cs" Inherits="admin_Migration_Summary" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .ddl.form-control {
            width: 200px;
            margin-left: 10px;
        }

        .form-group {
            display: flex;
        }

        table {
            font-size: 12px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <div style="padding: 0px 30px;">
            <div class="row">
                <div class="form-group">
                    <label for="">Status</label>
                    <asp:DropDownList CssClass="form-control ddl" AutoPostBack="True"  OnSelectedIndexChanged="ddlstatus_SelectedIndexChanged" ID="ddlstatus" runat="server" DataTextField="old_loan_status" DataValueField="old_loan_status"></asp:DropDownList>
                </div>
            </div>
            <div class="row">
                <asp:Label ID="lbl_table" runat="server" Text="lbl_table"></asp:Label>
            </div>
        </div>
    </div>
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-lg" style="width: 64rem;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Summary Details</h4>
                </div>
                <div class="modal-body" style="margin-bottom: -29px;">
                    <div class="row" style="margin-left: 5px">
                        <div id="divModelSave">
                            <div class="col-md-12" id="detailsview">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="col-md-12">
                        <div class="col-md-8">
                        </div>
                        <div class="col-md-4 text-center">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(function () {
            var obj = {
                Init: function () {
                    $('.loader').hide();
                },
            }
            obj.Init();
        });
        function viewDetails(id) {
            $('#divUserID').hide();
            $('#detailsview').html('');
            $.ajax({
                type: "POST",
                url: "Migration_Summary.aspx/GetMigrationDetailsByID",
                data: JSON.stringify({ ID: id }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (res) {
                    $('#detailsview').append(res.d);
                    $('#myModal').modal('show');
                }
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" runat="Server">
</asp:Content>

