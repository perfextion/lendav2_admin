﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_Migration_Summary : System.Web.UI.Page
{
    static string dbKey = "gp_conn";
    static string color = "";    

    protected void Page_Load(object sender, EventArgs e)
    {
        if(!Page.IsPostBack)
        {
            BindDropdown();
            lbl_table.Text = LoadTable(150);
        }
    }

    public void BindDropdown()
    {
        string str_sql = "select distinct old_loan_status from migration_summary where old_loan_status > 0";
        var dt = gen_db_utils.gp_sql_get_datatable(str_sql, dbKey);

        ddlstatus.DataSource = dt;
        ddlstatus.DataBind();
    }

    [WebMethod]
    public static string GetMigrationDetailsByID(string ID)
    {
        string sql_qry = "SELECT [mig_id],[old_loan_id],[child_id],[parent_id],[is_latest],[old_parent_loan],[old_child_loan]," +
            "[upload_status],[old_loan_status],[nortridge_status],[new_loan_full_id],[nortridge_loan_id],[old_cnt_farms]," +
            "[new_cnt_farms],[old_arm_commitment],[new_arm_commitment],[nort_arm_commitment],[old_dist_commitment]," +
            "[new_dist_commitment],[nort_dist_commitment],[old_third_party_commitment],[new_third_party_commitment]," +
            "[nort_third_party_commitment],[old_crop_year],[new_crop_year],[nort_crop_year],[old_total_acres]," +
            "[new_total_acres],[nort_total_acres],[old_total_crop_acres],[new_total_crop_acres],[nort_total_crop_acres]," +
            "[old_total_assets],[new_total_assets],[nort_total_assets],[old_total_revenue],[new_total_revenue]," +
            "[nort_total_revenue],[old_cash_flow_amount],[new_cash_flow_amount],[nort_cash_flow_amount],[old_margin]," +
            "[new_margin],[nort_margin],[old_market_value_insurance],[new_market_value_insurance],[old_insurance_value_crops]," +
            "[new_insurance_value_crops],[old_arm_total_budget],[new_arm_total_budget],[nort_arm_total_budget]," +
            "[old_dist_total_budget],[new_dist_total_budget],[old_third_party_total_budget],[new_third_party_total_budget]," +
            "[old_rent_budget],[new_rent_budget],[old_ins_budget],[new_ins_budget],[old_closed_date],[old_cash_flow]," +
            "[old_risk_cushion],[old_borrower_rating],[new_borrower_rating],[old_return_percent],[new_return_percent]," +
            "[old_risk_cushion_percent],[new_risk_cushion_percent],[old_cnt_collateral_items],[new_cnt_collateral_items]," +
            "[old_cnt_collateral_FSA],[old_cnt_collateral_stored],[old_cnt_collateral_equip],[old_cnt_collateral_realestate]," +
            "[old_cnt_collateral_other],[old_cnt_collateral_FSA_value],[old_cnt_collateral_stored_value] FROM [Migration_Summary] " +
            "where mig_id='" + ID + "'";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return generatedetails(dt);
    }

    [WebMethod]
    public static string LoadTable(int status)
    {
        string stQry = "";
        string str_return = "";
        stQry = "SELECT [mig_id],[old_loan_id],[new_loan_full_id],[old_cnt_farms],[new_cnt_farms]," +
            "[old_arm_commitment],[new_arm_commitment],[old_dist_commitment],[new_dist_commitment]," +
            "[old_third_party_commitment],[new_third_party_commitment],[old_crop_year],[new_crop_year]," +
            "[old_total_acres],[new_total_acres],[old_total_assets],[new_total_assets],[old_total_crop_acres]," +
            "[new_total_crop_acres],[old_total_revenue],[new_total_revenue],[old_cash_flow_amount],[new_cash_flow_amount]," +
            "[old_market_value_insurance],[new_market_value_insurance],[old_insurance_value_crops],[new_insurance_value_crops]," +
            "[old_arm_total_budget],[new_arm_total_budget],[old_dist_total_budget],[new_dist_total_budget]," +
            "[old_third_party_total_budget],[new_third_party_total_budget], old_cash_flow, old_risk_cushion, new_risk_cushion_amount FROM [Migration_Summary] " +
            "where [new_loan_full_id] is not null ";

        stQry += " and old_loan_status = " + status;

        DataTable dtLoanBudget = gen_db_utils.gp_sql_get_datatable(stQry, dbKey);

        string str_sql = "select legacy_loan_id, migrated_loan_Adjustments from loan_master where legacy_loan_id in (select old_loan_id from migration_summary where old_loan_status > 0)";
        var dt = gen_db_utils.gp_sql_get_datatable(str_sql, dbKey);
        var migrationSummarydt = dt.AsEnumerable().ToDictionary(r => r[0].ToString(), r => r[1].ToString());

        if (dtLoanBudget != null && dtLoanBudget.Rows.Count > 0)
        {
            str_return += "<span>Total count is:" + dtLoanBudget.Rows.Count;
            str_return += "<div> <table id='tblActiveUsers' class='table table-striped table-bordered' >  "
                           + "<thead> "
                           + "     <tr> "
                            + "      <th> Mig ID </th> "
                            + "      <th> Loan Details </th> "
                            + "      <th> Farm Details  </th> "
                            + "      <th> Arm Details  </th> "
                            + "      <th> Arm Budget  </th> "
                            + "      <th> Cash Flow  </th> "
                            + "      <th> Total Assets  </th> "
                            + "      <th> Migration Adujustments  </th> "
                            + "      <th>  View  </th> "
                           + "    </tr> "
                           + " </thead> "
                           + " <tbody> ";

            foreach (DataRow dtrow in dtLoanBudget.Rows)
            {
                string armcommit, ARMBudget, Cropyear, Farms, Farmsacres, insvalcrops, totalrevenue, marketvalueins, CropAcres, totalassets, DistBudget, cashflow, Dist_Commit, thirdPartybudget, thirdPartyCommit = "";

                armcommit = ValidTable(dtrow["old_arm_commitment"].ToString(), dtrow["new_arm_commitment"].ToString());

                Farms = ValidTable(dtrow["old_cnt_farms"].ToString(), dtrow["new_cnt_farms"].ToString());

                Cropyear = ValidTable(dtrow["old_crop_year"].ToString(), dtrow["new_crop_year"].ToString());

                Farmsacres = ValidTable(dtrow["old_total_acres"].ToString(), dtrow["new_total_acres"].ToString());

                CropAcres = ValidTable(dtrow["old_total_crop_acres"].ToString(), dtrow["new_total_crop_acres"].ToString());

                Dist_Commit = ValidTable(dtrow["old_dist_commitment"].ToString(), dtrow["new_dist_commitment"].ToString());

                thirdPartyCommit = ValidTable(dtrow["old_third_party_commitment"].ToString(), dtrow["new_third_party_commitment"].ToString());

                ARMBudget = ValidTable(dtrow["old_arm_total_budget"].ToString(), dtrow["new_arm_total_budget"].ToString());

                DistBudget = ValidTable(dtrow["old_dist_total_budget"].ToString(), dtrow["new_dist_total_budget"].ToString());

                thirdPartybudget = ValidTable(dtrow["old_third_party_total_budget"].ToString(), dtrow["old_third_party_total_budget"].ToString());

                cashflow = ValidTable(dtrow["old_cash_flow"].ToString(), dtrow["new_cash_flow_amount"].ToString());

                string risk_cushion = ValidTable(dtrow["old_risk_cushion"].ToString(), dtrow["new_risk_cushion_amount"].ToString());

                totalassets = ValidTable(dtrow["old_total_assets"].ToString(), dtrow["new_total_assets"].ToString());

                totalrevenue = ValidTable(dtrow["old_total_revenue"].ToString(), dtrow["new_total_revenue"].ToString());

                marketvalueins = ValidTable(dtrow["old_market_value_insurance"].ToString(), dtrow["new_market_value_insurance"].ToString());

                insvalcrops = ValidTable(dtrow["old_insurance_value_crops"].ToString(), dtrow["new_insurance_value_crops"].ToString());

                var adj_json = fc_lookup_json(migrationSummarydt, dtrow["old_loan_id"].ToString());

                string adjustments = string.Empty;

                if(!string.IsNullOrEmpty(adj_json.ToString()))
                {
                    adjustments = "<div> ARM Commit : " + adj_json["ARM_Commitment"] + " </div>";
                    adjustments += "<div> Dist Commit : " + adj_json["Dist_Commitment"] + " </div>";
                    adjustments += "<div> Mkt Value : " + adj_json["Net_Market_Value_Crops"] + " </div>";
                    adjustments += "<div> Cash Flow : " + adj_json["Cash_Flow_Amount"] + " </div>";
                    adjustments += "<div> Risk Cushion : " + adj_json["Risk_Cushion_Amount"] + " </div>";
                    adjustments += "<div> Org Fee : " + adj_json["Origination_Fee_Amount"] + " </div>";
                    adjustments += "<div> Srvc Fee : " + adj_json["Service_Fee_Amount"] + " </div>";
                    adjustments += "<div> ARM Fee : " + adj_json["ARM_Rate_Fee"] + " </div>";
                    adjustments += "<div> Dist Fee : " + adj_json["Dist_Rate_Fee"] + " </div>";
                    adjustments += "<div> Cash Rent : " + adj_json["Cash_Rent_Budget"] + " </div>";
                }

                str_return += "<tr>"
                                + "<td> " + dtrow["mig_id"].ToString() + " </td>"
                                + "<td> Loan ID: " + dtrow["old_loan_id"].ToString() + " | " + dtrow["new_loan_full_id"].ToString() + " <br/>" +
                                "Crop Year: " + Cropyear + "  </td>"
                                + "<td> Farms: " + Farms + " <br/>" +
                                " Farm Acres: " + Farmsacres + " <br/>" +
                                " Crop Acres: " + CropAcres + " <br/>" +
                                "</td>"
                                 + "<td> ARM Commit: " + armcommit + " <br/>" +
                                " Dist Commit: " + Dist_Commit + " <br/>" +
                                " 3rd Party Commit: " + thirdPartyCommit + " <br/>" +
                                "</td>"
                                + "<td>ARM Bgt: " + ARMBudget + " <br/>" +
                                " Dist Bgt: " + DistBudget + " <br/>" +
                                " 3rd_Party Bgt: " + thirdPartybudget + " <br/>" +
                                "</td>"
                                + "<td>Cash Flow:" + cashflow + " <br/>" +
                                " Risk Cushion: " + risk_cushion + " <br/>" +
                                "</td>"
                                + "<td>Total Assets: " + totalassets + " <br/>" +
                                " Total Revenue: " + totalrevenue + " <br/>" +
                                " Mkt Value Ins: " + marketvalueins + " <br/>" +
                                " Ins Value Crops: " + insvalcrops + " <br/>" +
                                "</td>"
                                + "<td>" + adjustments + "</td>"
                                 + "<td><a href ='javascript:viewDetails(" + dtrow["mig_id"] + ")' > Details</a></td>"
                               + "</tr>";
            }
            str_return += " </tbody> "
                                    + " </table> "
                                      + "</div></div></div >";
        }
        else
        {
            str_return = "    No data available...";
        }
        return str_return;
    }
    public static string ValidTable(string oldvalue, string newvalue)
    {
        string data = "";
        if (!string.IsNullOrEmpty(oldvalue) && !string.IsNullOrEmpty(newvalue))
        {
            data = percentDiff(Convert.ToDouble(oldvalue), Convert.ToDouble(newvalue), 0.01);
        }
        else
        {
            oldvalue = (string.IsNullOrEmpty(oldvalue)) ? "" : Math.Round(Convert.ToDouble(oldvalue), 2).ToString();
            newvalue = (string.IsNullOrEmpty(newvalue)) ? "" : Math.Round(Convert.ToDouble(newvalue), 2).ToString();
            data = oldvalue + "|" + newvalue;
        }
        return data;
    }
    public static string generatedetails(DataTable dtLoanBudget)
    {
        string str_return = "";
        if (dtLoanBudget != null && dtLoanBudget.Rows.Count > 0)
        {
            str_return += "<div> <table id='tblActiveUsers' class='table table-striped table-bordered' >  "
                              + "<thead> "
                              + "     <tr> "
                               + "      <th> Names </th> "
                               + "      <th style='width: 10rem;text-align:center;'> Old </th> "
                               + "      <th style='width: 10rem;text-align:center;'> New </th> "
                               + "      <th style='width: 10rem;text-align:center;'> Nortridge  </ th> "
                              + "    </tr> "
                              + "      </thead> "
                              + "        <tbody> ";

            foreach (DataRow dtrow in dtLoanBudget.Rows)
            {
                string Loan_id, Cnt_Farms, Arm_Commitment, Dist_commitment, Third_party_Commitment,
                    Crop_year, Total_Acres, Total_Crop_Acres, Total_Assets, Total_Revenue, Cash_Flow_Amount,
                    Margin, Market_Value_Insurance, Insurance_Value_Crops, Arm_Total_Budget, Dist_Total_Budget,
                    Third_party_total_budget, Rent_Budget, Ins_Budget, Borrower_Rating, Return_Percent,
                    Risk_Cushion_Percent, Cnt_Collateral_items = "";

                //Loan_id = Valid(dtrow["old_loan_id"].ToString(), dtrow["new_loan_full_id"].ToString());
                Loan_id = "<td style='text-align:center;'>" + dtrow["old_loan_id"].ToString() + "</td>" +
                           "<td style='text-align:center;'>" + dtrow["new_loan_full_id"].ToString() + "</td>";
                str_return += TableReturn("Loan_id", Loan_id, dtrow["nortridge_loan_id"].ToString());

                Cnt_Farms = Valid(dtrow["old_cnt_farms"].ToString(), dtrow["new_cnt_farms"].ToString());
                str_return += TableReturn("Cnt_Farms", Cnt_Farms, "--");

                Arm_Commitment = Valid(dtrow["old_arm_commitment"].ToString(), dtrow["new_arm_commitment"].ToString());
                str_return += TableReturn("Arm_Commitment", Arm_Commitment, dtrow["nort_arm_commitment"].ToString());

                Dist_commitment = Valid(dtrow["old_dist_commitment"].ToString(), dtrow["new_dist_commitment"].ToString());
                str_return += TableReturn("Dist_commitment", Dist_commitment, dtrow["nort_dist_commitment"].ToString());


                Third_party_Commitment = Valid(dtrow["old_third_party_commitment"].ToString(), dtrow["new_third_party_commitment"].ToString());
                str_return += TableReturn("Third_party_Commitment", Third_party_Commitment, dtrow["nort_third_party_commitment"].ToString());

                Crop_year = Valid(dtrow["old_crop_year"].ToString(), dtrow["new_crop_year"].ToString());
                str_return += TableReturn("Crop_year", Crop_year, dtrow["nort_crop_year"].ToString());

                Total_Acres = Valid(dtrow["old_total_acres"].ToString(), dtrow["new_total_acres"].ToString());
                str_return += TableReturn("Total_Acres", Total_Acres, dtrow["nort_total_acres"].ToString());

                Total_Crop_Acres = Valid(dtrow["old_total_crop_acres"].ToString(), dtrow["new_total_crop_acres"].ToString());
                str_return += TableReturn("Total_Crop_Acres", Total_Crop_Acres, dtrow["nort_total_crop_acres"].ToString());

                Total_Assets = Valid(dtrow["old_total_assets"].ToString(), dtrow["new_total_assets"].ToString());
                str_return += TableReturn("Total_Assets", Total_Assets, dtrow["nort_total_assets"].ToString());

                Total_Revenue = Valid(dtrow["old_total_revenue"].ToString(), dtrow["new_total_revenue"].ToString());
                str_return += TableReturn("Total_Revenue", Total_Revenue, dtrow["nort_total_revenue"].ToString());

                Cash_Flow_Amount = Valid(dtrow["old_cash_flow_amount"].ToString(), dtrow["new_cash_flow_amount"].ToString());
                str_return += TableReturn("Cash_Flow_Amount", Cash_Flow_Amount, dtrow["nort_cash_flow_amount"].ToString());

                Margin = Valid(dtrow["old_margin"].ToString(), dtrow["new_margin"].ToString());
                str_return += TableReturn("Margin", Margin, dtrow["nort_margin"].ToString());

                Market_Value_Insurance = Valid(dtrow["old_market_value_insurance"].ToString(), dtrow["new_market_value_insurance"].ToString());
                str_return += TableReturn("Market_Value_Insurance", Market_Value_Insurance, "--");

                Insurance_Value_Crops = Valid(dtrow["old_insurance_value_crops"].ToString(), dtrow["new_insurance_value_crops"].ToString());
                str_return += TableReturn("Insurance_Value_Crops", Insurance_Value_Crops, "--");

                Arm_Total_Budget = Valid(dtrow["old_arm_total_budget"].ToString(), dtrow["new_arm_total_budget"].ToString());
                str_return += TableReturn("Arm_Total_Budget", Arm_Total_Budget, dtrow["nort_arm_total_budget"].ToString());

                Dist_Total_Budget = Valid(dtrow["old_dist_total_budget"].ToString(), dtrow["new_dist_total_budget"].ToString());
                str_return += TableReturn("Dist_Total_Budget", Dist_Total_Budget, "--");

                Third_party_total_budget = Valid(dtrow["old_third_party_total_budget"].ToString(), dtrow["new_third_party_total_budget"].ToString());
                str_return += TableReturn("Third_party_total_budget", Third_party_total_budget, "--");

                Rent_Budget = Valid(dtrow["old_rent_budget"].ToString(), dtrow["new_rent_budget"].ToString());
                str_return += TableReturn("Rent_Budget", Rent_Budget, "--");

                Ins_Budget = Valid(dtrow["old_ins_budget"].ToString(), dtrow["new_ins_budget"].ToString());
                str_return += TableReturn("Ins_Budget", Ins_Budget, "--");

                Borrower_Rating = Valid(dtrow["old_borrower_rating"].ToString(), dtrow["new_borrower_rating"].ToString());
                str_return += TableReturn("Borrower_Rating", Borrower_Rating, "--");

                Return_Percent = Valid(dtrow["old_return_percent"].ToString(), dtrow["new_return_percent"].ToString());
                str_return += TableReturn("Return_Percent", Return_Percent, "--");

                Risk_Cushion_Percent = Valid(dtrow["old_risk_cushion_percent"].ToString(), dtrow["new_risk_cushion_percent"].ToString());
                str_return += TableReturn("Risk_Cushion_Percent", Risk_Cushion_Percent, "--");

                Cnt_Collateral_items = Valid(dtrow["old_cnt_collateral_items"].ToString(), dtrow["new_cnt_collateral_items"].ToString());
                str_return += TableReturn("Cnt_Collateral_items", Cnt_Collateral_items, "--");
            }
            str_return += " </tbody> "
                                    + " </table> "
                                      + "</div></div></div >";
        }
        else
        {
            str_return = "    No data available...";
        }
        return str_return;
    }

    public static string percentDiff(Double value1, Double value2, Double tolerance)
    {
        string Data = "";
        if (Math.Abs(value1 - value2) / value1 > tolerance)
        {
            Data = "<span style='color:red'>" + Math.Round(value1, 2) + " | " + Math.Round(value2, 2) + "</span>";
        }
        else
        {
            Data = Math.Round(value1, 2) + " | " + Math.Round(value2, 2);
        }
        return Data;
    }
    public static string abs_Diff(Double value1, Double value2, Double tolerance)
    {
        string Data = "";
        if (Math.Abs(value1 - value2) > tolerance)
        {
            Data = "<td style='text-align:center;'>" + Math.Round(value1, 2) + "</td>" +
                    "<td style='text-align:center;'>" + Math.Round(value2, 2) + "</td>";
            color = "style='color:red'";
        }
        else
        {
            Data = "<td style='text-align:center;'>" + Math.Round(value1, 2) + "</td>" +
                     "<td style='text-align:center;'>" + Math.Round(value2, 2) + "</td>";
            color = "style='color:black'";
        }
        return Data;
    }
    public static string Valid(string oldvalue, string newvalue)
    {
        string data = "";
        if (!string.IsNullOrEmpty(oldvalue) && !string.IsNullOrEmpty(newvalue))
        {
            data = abs_Diff(Convert.ToDouble(oldvalue), Convert.ToDouble(newvalue), 1);
        }
        else
        {
            oldvalue = (string.IsNullOrEmpty(oldvalue)) ? "" : Math.Round(Convert.ToDouble(oldvalue), 2).ToString();
            newvalue = (string.IsNullOrEmpty(newvalue)) ? "" : Math.Round(Convert.ToDouble(newvalue), 2).ToString();
            data = "<td style='text-align:center;'>" + oldvalue + "</td>" +
                        "<td style='text-align:center;'>" + newvalue + "</td>";
            color = "style='color:black'";
        }
        return data;
    }

    public static string TableReturn(string Head, string Loan_id, string nortridge)
    {
        string table = "";
        var value = nortridge.GetType();
        table = "<tr " + color + ">"
                + "<td> " + Head + " </td>"
                + Loan_id
                + "<td style='text-align:center;'> " + nortridge + " </td>"
               + "</tr>";
        return table;
    }

    public static dynamic fc_lookup_json(Dictionary<string, string> dict, string key)
    {
        if (dict != null && dict.ContainsKey(key))
        {
            return JsonConvert.DeserializeObject<dynamic>(dict.FirstOrDefault(a => a.Key == key).Value);
        }
        else
        {
            return string.Empty;
        }
    }

    protected void ddlstatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        var selectedItem = ddlstatus.SelectedValue;
         lbl_table.Text = LoadTable(Convert.ToInt32(selectedItem));
    }
}
