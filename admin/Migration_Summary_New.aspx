﻿<%@ Page Title="" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="Migration_Summary_New.aspx.cs" Inherits="admin_Migration_Summary_New" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .scroll-table {
            max-height: 65vh;
            overflow: auto;
        }

        .scroll-table table thead th {
            background: #ddd;
            position: sticky;
            top: 0px;
        }

        w-10 {
            width: 10%;
        }

        #content {
            padding: 14px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <div class="col-md-12" style="padding-left: 30px; padding-top: 8px;">
            <asp:Label ID="lbl_table" runat="server" Text="lbl_table"></asp:Label>
        </div>
    </div>
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-lg" style="width: 64rem;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Summary Details</h4>
                </div>
                <div class="modal-body" style="margin-bottom: -29px; max-height: 70vh; overflow: auto;">
                    <div class="row" style="margin-left: 5px">
                        <div id="divModelSave">
                            <div class="col-md-12" id="detailsview">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="col-md-12">
                        <div class="col-md-8">
                        </div>
                        <div class="col-md-4 text-center">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(function () {
            var obj = {
                Init: function () {
                    $('.loader').hide();
                    $('#navbtnsave').addClass('disabled');
                    $('#navbtnadd').addClass('disabled');
                    $('#navbtnrefresh').addClass('disabled');
                    $('#navbtncolumns').addClass('disabled');
                },
            }
            obj.Init();
        });
        function viewDetails(id) {
            $('#divUserID').hide();
            $('#detailsview').html('');
            $.ajax({
                type: "POST",
                url: "Migration_Summary_New.aspx/GetMigrationDetailsByID",
                data: JSON.stringify({ ID: id }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (res) {
                    $('#detailsview').append(res.d);
                    $('#myModal').modal('show');
                }
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" runat="Server">
    <script>
        $("#navbtndownload").click(function () {
            exportTableToExcel_Exclude('tblActiveUsers', 'Loan Migration');
        });
    </script>
</asp:Content>

