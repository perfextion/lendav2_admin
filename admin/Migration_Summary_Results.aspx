﻿<%@ Page Title="" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="Migration_Summary_Results.aspx.cs" Inherits="admin_Migration_Summary_Results" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="migration-summary-result" style="padding-left: 50px;">
        <div class="loader"></div>
        <table id="migrationSummaryResults" class="table table-bordered table-hover" style="width: 600px; display: none">
            <thead>
                <tr>
                    <th>Column name</th>
                    <th>Count</th>
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" Runat="Server">
    <script>
        $(function () {
            var obj = {
                Init: function () {
                    $('.loader').show();
                    obj.bindGrid();

                    $('#navbtnadd').addClass('disabled');
                    $('#navbtnsave').addClass('disabled');
                    $('#navbtncolumns').addClass('disabled');
                    $('#navbtnrefresh').addClass('disabled');

                    $('#navbtndownload').click(function () {
                        $('.loader').show();
                        var ws = XLSX.utils.table_to_sheet(document.getElementById('migrationSummaryResults'));
                        var wb = XLSX.utils.book_new();

                        ws['!cols'] = [
                            { wch: 30 },
                            { wch: 15 }
                        ];

                        XLSX.utils.book_append_sheet(wb, ws, 'Migration Summary Results');
                        XLSX.writeFile(wb, `Migration Summary Results.xlsx`);

                        $('.loader').hide();
                    });
                },
                bindGrid: function () {
                    $.ajax({
                        type: "POST",
                        url: "Migration_Summary_Results.aspx/LoadTable",
                        data: JSON.stringify({ }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var resData = JSON.parse(data.d);
                            var tbody = $('#migrationSummaryResults tbody')
                            var tr = '';

                            Object.keys(resData[0]).forEach(function (key) {
                                var k = key.split(' ');
                                k.splice(0, 1);

                                tr += '<tr><td>' + k.join(' ') + '</td><td>' + resData[0][key] + '</td></tr>';
                            });


                            tbody.html(tr);
                            $('.loader').hide();
                            $('#migrationSummaryResults').show();
                        }
                    });
                }
            }

             obj.Init();
        });
    </script>
</asp:Content>

