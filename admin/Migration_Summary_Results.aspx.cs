﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_Migration_Summary_Results : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        base.Page_Load(sender, e);
        LoadTable();
    }

    public static List<ColumnList> GetColumnNames()
    {
        string sql = @"select Column_Name, dbo.GetColumnName(COLUMN_NAME) as [Translated_Column_Name] from Information_Schema.Columns where Table_Name = 'Migration_Summary_Excel_Report_View' and Column_Name Like 'd_%'";
        var list = gen_db_utils.ExcecuteAndGetList<ColumnList>(sql, dbKey);
        return list;
    }

    [WebMethod]
    public static string LoadTable()
    {
        string sql = "Select";
        sql += Environment.NewLine;

        sql += "(Select Count(*) from Migration_Summary_Excel_Report_View) as [Total Count],";
        sql += Environment.NewLine;

        var columnList = GetColumnNames();
        sql += string.Join(",\n", columnList.Select(a => GetSQL(a.Column_Name, a.Translated_Column_Name)));

        var dt = gen_db_utils.gp_sql_get_datatable(sql, dbKey);

        return JsonConvert.SerializeObject(dt);
    }

    private static string GetSQL(string columnName, string Translated_Column_Name,  int MatchInd = 1)
    {

        if (MatchInd == 1)
        {
            return "(Select COUNT(*) From Migration_Summary_Excel_Report_View where [" + columnName + "] = '-') as [" + Translated_Column_Name + "]";
        }
        else
        {
            return "(Select COUNT(*) From Migration_Summary_Excel_Report_View where [" + columnName + "] != '-')  as [" + Translated_Column_Name + "]";
        }
    }
}

public  class ColumnList
{
    public string Column_Name { get; set; }
    public string Translated_Column_Name { get; set; }
}