﻿<%@ Page Title="" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="Migration_Summary_V.2.aspx.cs" Inherits="admin_Migration_Summary_V_2" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style>
         .ui-jqgrid .ui-jqgrid-htable th div {
            height: auto;
            overflow: hidden;
            padding-right: 4px;
            padding-top: 2px;
            position: relative;
            vertical-align: text-top;
            word-wrap: break-word; /* IE 5.5+ and CSS3 */
            white-space: pre-wrap; /* CSS3 */
            white-space: normal !important;
        }

        .ui-jqgrid tr.jqgrow td {
            word-wrap: break-word; /* IE 5.5+ and CSS3 */
            white-space: pre-wrap; /* CSS3 */
            white-space: normal !important;
            height: auto;
            vertical-align: text-top;
            padding-top: 2px;
            padding-bottom: 3px;
        }

        .ui-jqgrid tr.jqgrow td[aria-describeby=jqGrid_Loan_Comment] {
            text-overflow: ellipsis;
            -o-text-overflow: ellipsis;
            word-break: unset;
            white-space:unset;
            white-space: unset;
            height: 100px;
        }

        .ui-jqgrid .ui-jqgrid-htable th {
            height: 50px;
        }

        .ui-jqgrid-bdiv {
            max-height: 400px;
        }

        .cvteste {
            padding-right: 14px !important;
        }

        #content {
            padding: 25px 14px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="loan-migration-summary">
        <div class="loader"></div>
        <div class="row main-content">
            <div style="margin-left: 35px">
                <table id="jqGrid"></table>
                <div id="jqGridPager"></div>
            </div>
        </div>
        <asp:HiddenField ID="LoanFullID" runat="server" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" Runat="Server">
    <script type="text/javascript">  
        var DeleteRows = [];
        var timer;
        var lastSelection;

        var entity_types = {};
        var pref_contacts = {};

        $(function () {
            var obj = {
                Init: function () {
                    $('.loader').show();
                    $('#navbtnexceldownload').show();

                    obj.getListItems();
                    obj.bindGrid();

                    $('#navbtnadd').addClass('disabled');
                    $('#navbtnsave').addClass('disabled');

                    $("#navbtnrefresh").on('click', function () {
                        $('.loader').show();
                        $('#txtSearchBar').val('');
                        obj.bindGrid();
                    });

                    $("#txtSearchBar").on("keyup", function () {
                        var self = this;
                        obj.search(self);
                    });

                    $("#navbtndownload").on("click", function () {
                        $("#jqGrid").jqGrid("exportToExcel", {
                            includeLabels: true,
                            includeGroupHeader: true,
                            includeFooter: true,
                            fileName: "Migration Summary Comparison.xlsx",
                            maxlength: 40
                        });
                    });

                    $('#navbtncolumns').click(function () {
                        $("#jqGrid").columnChooser({});
                    });

                    $('#navbtnexceldownload').click(function () {
                        $('.loader').show();
                        obj.downloadExcel();
                    });
                },
                downloadExcel: function () {
                    $.ajax({
                        type: "POST",
                        url: "Migration_Summary_V.2.aspx/DownloadExcel",
                        data: JSON.stringify({}),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var exportData = JSON.parse(data.d);
                            var ws = XLSX.utils.json_to_sheet(exportData);
                            var wb = XLSX.utils.book_new();

                            if (exportData && exportData.length > 0) {
                                var wscols = []

                                Object.keys(exportData[0]).forEach((key, index) => {
                                    var length = key ? key.length : 14;

                                    //var maxLength = obj.getMaxLength(exportData, key);
                                    //length = Math.max(length, maxLength);

                                    if (key.toLowerCase().includes('email')) {
                                        length = 30;
                                    }
                                    
                                    wscols.push({ wch: length });
                                    ws[XLSX.utils.encode_col(index) + '1'].v = obj.getHeader(key);
                                });

                                ws['!cols'] = wscols;

                                XLSX.utils.book_append_sheet(wb, ws, 'Migration Summary');
                                XLSX.writeFile(wb, `Migration Summary.xlsx`);
                            }

                            $('.loader').hide();
                        }
                    });
                },
                getMaxLength(obj, key) {
                    var values = obj.map(a => (a[key] || '').length);
                    return Math.max(...values);
                },
                getHeader: function (x) {
                    if (x) {
                        let array = x.split('_');
                        array = array.map(s => {
                            return s.charAt(0).toUpperCase() + s.slice(1);
                        });
                        return array.join(' ');
                    } else {
                        return x;
                    }
                },
                getListItems: function () {
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "LoanMaster.aspx/GetListItems",
                        data: JSON.stringify({}),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            var data = JSON.parse(res.d);

                            entity_types = {};
                            pref_contacts = {};

                            for (var i of data) {
                                if (i.List_Group_Code == 'BORROWER_ENTITY_TYPE') {
                                    entity_types[i.List_Item_Code] = i.List_Item_Name;
                                }

                                if (i.List_Group_Code == 'PREF_CONTACT') {
                                    pref_contacts[i.List_Item_Value] = i.List_Item_Name;
                                }
                            }
                        }

                    });
                },
                getTableInfo: function () {
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "Migration_Summary_V.2.aspx/GetTableInfo",
                        data: JSON.stringify({ }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            var data = JSON.parse(res.d);

                            var table_info = data.Table_Info[0];
                            $('#txtTableName').html(table_info.Table_Name);
                            $('#txtLastUpdated').html(table_info.Last_Updated);
                            $('#txtRecordsCount').html(table_info.Records_Count);
                            
                        }
                    });
                },
                bindGrid: function () {
                    $.ajax({
                        type: "POST",
                        url: "Migration_Summary_V.2.aspx/LoadTable",
                        data: JSON.stringify({ }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var resData = JSON.parse(data.d);
                            $('.loader').show();
                            for (var i = 0; i < resData.length; i++) {
                                resData[i].rowid = i + 1;
                            }
                            obj.loadgrid(resData);
                            obj.getTableInfo();
                            $('.loader').hide();
                        }
                    });
                },
                loadgrid: function (data, showSynthesis = true, showOld = true, showDiff = true ) {
                    $.jgrid.gridUnload("#jqGrid");
                    $("#jqGrid").jqGrid({
                        datatype: "local",
                        styleUI: 'Bootstrap',
                        data: data,
                        colModel: [
                            { label: 'rowid', name: 'rowid', width: 75, editable: false, key: true, hidden: true, hidedlg: true },
                            {
                                label: 'Migration Id',
                                name: 'mig_id',
                                width: 150,
                                align: 'center',
                                classes: 'center',
                                editable: false,
                                hidden: false,
                                sorttype: 'number'
                            },
                            {
                                label: 'Old Loan Id',
                                name: 'old_loan_id',
                                width: 150,
                                align: 'center',
                                classes: 'center',
                                editable: false,
                                hidden: false,
                                sorttype: 'number'
                            },
                            {
                                label: 'New Loan Full Id',
                                name: 'new_loan_full_id',
                                width: 150,
                                align: 'center',
                                classes: 'center',
                                editable: false,
                                hidden: false,
                                sorttype: 'string'
                            },
                            {
                                label: 'Child Id',
                                name: 'child_id',
                                width: 150,
                                align: 'center',
                                classes: 'center',
                                editable: false,
                                hidden: !showSynthesis,
                                sorttype: 'number'
                            },
                            {
                                label: 'Parent Id',
                                name: 'parent_id',
                                width: 150,
                                align: 'center',
                                classes: 'center',
                                editable: false,
                                hidden: !showSynthesis,
                                sorttype: 'number'
                            },
                            {
                                label: 'Nortridge Status',
                                name: 'nortridge_status',
                                width: 150,
                                align: 'center',
                                classes: 'center',
                                editable: false,
                                hidden: !showSynthesis,
                                sorttype: 'string'
                            },
                            { label: 'Nortridge Loan Id', name: 'nortridge_loan_id', width: 150, align: 'center', classes: 'center', editable: false, hidden: !showSynthesis, sorttype: 'number' },
                            { label: 'Old Borrower Id', name: 'old_borrower_id', width: 150, align: 'center', classes: 'center', editable: false, hidden: !showOld, sorttype: 'number' },
                            { label: 'New Borrower Id', name: 'new_borrower_id', width: 150, align: 'center', classes: 'center', editable: false, hidden: !showSynthesis, sorttype: 'number' },
                            { label: 'Old Borrower ID Type', name: 'old_Borrower_ID_Type', width: 150, align: 'center', classes: 'center', editable: false, hidden: !showOld, sorttype: 'number' },
                            { label: 'Old Borrower County ID', name: 'Old_Borrower_County_ID', width: 150, align: 'center', classes: 'center', editable: false, hidden: !showOld, sorttype: 'number' },
                            { label: 'Old Borrower CIFid', name: 'Old_Borrower_CIFid', width: 150, align: 'center', classes: 'center', editable: false, hidden: !showOld, sorttype: 'number' },
                            { label: 'Old Farmer Id', name: 'old_farmer_id', width: 150, align: 'center', classes: 'center', editable: false, hidden: !showOld, sorttype: 'number' },
                            { label: 'New Farmer Id', name: 'new_farmer_id', width: 150, align: 'center', classes: 'center', editable: false, hidden: !showSynthesis, sorttype: 'number' },
                            { label: 'Is Latest', name: 'is_latest', width: 150, align: 'center', classes: 'center', editable: false, hidden: !showSynthesis, sorttype: 'number' },
                            { label: 'Old Parent Loan', name: 'old_parent_loan', width: 150, align: 'center', classes: 'center', editable: false, hidden: !showOld, sorttype: 'number' },
                            { label: 'Old Child Loan', name: 'old_child_loan', width: 150, align: 'center', classes: 'center', editable: false, hidden: !showOld, sorttype: 'number' },
                            { label: 'Upload Status', name: 'upload_status', width: 150, align: 'center', classes: 'center', editable: false, hidden: !showSynthesis, sorttype: 'number' },
                            { label: 'Old Loan Status', name: 'old_loan_status', width: 150, align: 'center', classes: 'center', editable: false, hidden: !showOld, sorttype: 'number' },
                            { label: 'Old Borrower Last Name', name: 'old_Borrower_Last_Name', width: 150, align: 'left', classes: 'left', editable: false, hidden: !showOld, sorttype: 'string' },
                            { label: 'Old Borrower First Name', name: 'old_Borrower_First_Name', width: 150, align: 'left', classes: 'left', editable: false, hidden: !showOld, sorttype: 'string' },
                            { label: 'Old Borrower MI', name: 'old_Borrower_MI', width: 150, align: 'left', classes: 'left', editable: false, hidden: !showOld, sorttype: 'string' },
                            { label: 'Old Borrower Address', name: 'Old_Borrower_Address', width: 150, align: 'left', classes: 'left', editable: false, hidden: !showOld, sorttype: 'string' },
                            { label: 'Old Borrower City', name: 'Old_Borrower_City', width: 150, align: 'left', classes: 'left', editable: false, hidden: !showOld, sorttype: 'string' },
                            { label: 'Old Borrower State Abbrev', name: 'Old_Borrower_State_Abbrev', width: 150, align: 'left', classes: 'left', editable: false, hidden: !showOld, sorttype: 'string' },
                            { label: 'Old Borrower State', name: 'Old_Borrower_State', width: 150, align: 'left', classes: 'left', editable: false, hidden: !showOld, sorttype: 'string' },
                            { label: 'Old Borrower Zip', name: 'Old_Borrower_Zip', width: 150, align: 'center', classes: 'center', editable: false, hidden: !showOld, sorttype: 'number' },
                            { label: 'Old Borrower Phone', name: 'Old_Borrower_Phone', width: 150, align: 'left', classes: 'left', editable: false, hidden: !showOld, sorttype: 'string' },
                            { label: 'Old Borrower Email', name: 'Old_Borrower_Email', width: 150, align: 'left', classes: 'left', editable: false, hidden: !showOld, sorttype: 'string' },
                            { label: 'Old Borrower DL State Abbrev', name: 'Old_Borrower_DL_State_Abbrev', width: 150, align: 'left', classes: 'left', editable: false, hidden: !showOld, sorttype: 'string' },
                            { label: 'Old Borrower DL State', name: 'Old_Borrower_DL_State', width: 150, align: 'left', classes: 'left', editable: false, hidden: !showOld, sorttype: 'string' },
                            { label: 'Old Borrower DL Num', name: 'Old_Borrower_DL_Num', width: 150, align: 'left', classes: 'left', editable: false, hidden: !showOld, sorttype: 'string' },
                            { label: 'Old Borrower DOB', name: 'Old_Borrower_DOB', width: 150, align: 'left', classes: 'left', editable: false, hidden: !showOld, sorttype: 'string' },
                            { label: 'Old Borrower Preferred Contact Ind', name: 'Old_Borrower_Preferred_Contact_Ind', width: 150, align: 'center', classes: 'center', editable: false, hidden: !showOld, sorttype: 'number' },
                            { label: 'Old Borrower County Name', name: 'Old_Borrower_County_Name', width: 150, align: 'left', classes: 'left', editable: false, hidden: !showOld, sorttype: 'string' },
                            { label: 'Old Borrower Lat', name: 'Old_Borrower_Lat', width: 150, align: 'right', classes: 'right', editable: false, hidden: !showOld, sorttype: 'number' },
                            { label: 'Old Borrower Long', name: 'Old_Borrower_Long', width: 150, align: 'right', classes: 'right', editable: false, hidden: !showOld, sorttype: 'number' },
                            { label: 'Old Co Borrower Count', name: 'Old_Co_Borrower_Count', width: 150, align: 'right', classes: 'right', editable: false, hidden: !showOld, sorttype: 'number' },
                            { label: 'Old Spouse SSN Hash', name: 'Old_Spouse_SSN_Hash', width: 150, align: 'left', classes: 'left', editable: false, hidden: !showOld, sorttype: 'string' },
                            { label: 'Old Spouse Last Name', name: 'Old_Spouse_Last_name', width: 150, align: 'left', classes: 'left', editable: false, hidden: !showOld, sorttype: 'string' },
                            { label: 'Old Spouse First Name', name: 'Old_Spouse_First_Name', width: 150, align: 'left', classes: 'left', editable: false, hidden: !showOld, sorttype: 'string' },
                            { label: 'Old Spouse MI', name: 'Old_Spouse_MI', width: 150, align: 'left', classes: 'left', editable: false, hidden: !showOld, sorttype: 'string' },
                            { label: 'Old Spouse Address', name: 'Old_Spouse_Address', width: 150, align: 'left', classes: 'left', editable: false, hidden: !showOld, sorttype: 'string' },
                            { label: 'Old Spouse City', name: 'Old_Spouse_City', width: 150, align: 'left', classes: 'left', editable: false, hidden: !showOld, sorttype: 'string' },
                            { label: 'Old Spouse State', name: 'Old_Spouse_State', width: 150, align: 'left', classes: 'left', editable: false, hidden: !showOld, sorttype: 'string' },
                            { label: 'Old Spouse State Abbrev', name: 'Old_Spouse_State_Abbrev', width: 150, align: 'left', classes: 'left', editable: false, hidden: !showOld, sorttype: 'string' },
                            { label: 'Old Spouse Phone', name: 'Old_Spouse_Phone', width: 150, align: 'left', classes: 'left', editable: false, hidden: !showOld, sorttype: 'string' },
                            { label: 'Old Spouse Email', name: 'Old_Spouse_Email', width: 150, align: 'left', classes: 'left', editable: false, hidden: !showOld, sorttype: 'string' },
                            { label: 'Old Spouse Preferred Contact Ind', name: 'Old_Spouse_Preferred_Contact_Ind', width: 150, align: 'center', classes: 'center', editable: false, hidden: !showOld, sorttype: 'number' },
                            { label: 'Old Spouse Zip', name: 'Old_Spouse_Zip', width: 150, align: 'center', classes: 'center', editable: false, hidden: !showOld, sorttype: 'number' },
                            { label: 'Old Loan Type', name: 'old_loan_type', width: 150, align: 'left', classes: 'left', editable: false, hidden: !showOld, sorttype: 'string' },
                            { label: 'New Loan Type', name: 'new_loan_type', width: 150, align: 'left', classes: 'left', editable: false, hidden: !showSynthesis, sorttype: 'string' },
                            { label: 'Old Cnt Farms', name: 'old_cnt_farms', width: 150, align: 'center', classes: 'center', editable: false, hidden: !showOld, sorttype: 'number' },
                            { label: 'New Cnt Farms', name: 'new_cnt_farms', width: 150, align: 'center', classes: 'center', editable: false, hidden: !showSynthesis, sorttype: 'number' },
                            { label: 'Old Arm Commitment', name: 'old_arm_commitment', width: 150, align: 'right', classes: 'right', editable: false, hidden: !showOld, sorttype: 'number' },
                            { label: 'New Arm Commitment', name: 'new_arm_commitment', width: 150, align: 'right', classes: 'right', editable: false, hidden: !showSynthesis, sorttype: 'number' },
                            { label: 'Nort Arm Commitment', name: 'nort_arm_commitment', width: 150, align: 'right', classes: 'right', editable: false, hidden: !showSynthesis, sorttype: 'number' },
                            { label: 'Old Dist Commitment', name: 'old_dist_commitment', width: 150, align: 'right', classes: 'right', editable: false, hidden: !showOld, sorttype: 'number' },
                            { label: 'New Dist Commitment', name: 'new_dist_commitment', width: 150, align: 'right', classes: 'right', editable: false, hidden: !showSynthesis, sorttype: 'number' },
                            { label: 'Nort Dist Commitment', name: 'nort_dist_commitment', width: 150, align: 'right', classes: 'right', editable: false, hidden: !showSynthesis, sorttype: 'number' },
                            { label: 'Old Third Party Commitment', name: 'old_third_party_commitment', width: 150, align: 'right', classes: 'right', editable: false, hidden: !showOld, sorttype: 'number' },
                            { label: 'New Third Party Commitment', name: 'new_third_party_commitment', width: 150, align: 'right', classes: 'right', editable: false, hidden: !showSynthesis, sorttype: 'number' },
                            { label: 'Nort Third Party Commitment', name: 'nort_third_party_commitment', width: 150, align: 'right', classes: 'right', editable: false, hidden: !showSynthesis, sorttype: 'number' },
                            { label: 'Old Crop Year', name: 'old_crop_year', width: 150, align: 'center', classes: 'center', editable: false, hidden: !showOld, sorttype: 'number' },
                            { label: 'New Crop Year', name: 'new_crop_year', width: 150, align: 'center', classes: 'center', editable: false, hidden: !showSynthesis, sorttype: 'number' },
                            { label: 'Nort Crop Year', name: 'nort_crop_year', width: 150, align: 'center', classes: 'center', editable: false, hidden: !showSynthesis, sorttype: 'number' },
                            { label: 'Old Total Acres', name: 'old_total_acres', width: 150, align: 'right', classes: 'right', editable: false, hidden: !showOld, sorttype: 'number' },
                            { label: 'New Total Acres', name: 'new_total_acres', width: 150, align: 'right', classes: 'right', editable: false, hidden: !showSynthesis, sorttype: 'number' },
                            { label: 'Nort Total Acres', name: 'nort_total_acres', width: 150, align: 'center', classes: 'center', editable: false, hidden: !showSynthesis, sorttype: 'number' },
                            { label: 'Old Total Crop Acres', name: 'old_total_crop_acres', width: 150, align: 'right', classes: 'right', editable: false, hidden: !showOld, sorttype: 'number' },
                            { label: 'New Total Crop Acres', name: 'new_total_crop_acres', width: 150, align: 'right', classes: 'right', editable: false, hidden: !showSynthesis, sorttype: 'number' },
                            { label: 'Nort Total Crop Acres', name: 'nort_total_crop_acres', width: 150, align: 'center', classes: 'center', editable: false, hidden: !showSynthesis, sorttype: 'number' },
                            { label: 'Old Total Assets', name: 'old_total_assets', width: 150, align: 'right', classes: 'right', editable: false, hidden: !showOld, sorttype: 'number' },
                            { label: 'New Total Assets', name: 'new_total_assets', width: 150, align: 'right', classes: 'right', editable: false, hidden: !showSynthesis, sorttype: 'number' },
                            { label: 'Nort Total Assets', name: 'nort_total_assets', width: 150, align: 'right', classes: 'right', editable: false, hidden: !showSynthesis, sorttype: 'number' },
                            { label: 'Old Total Revenue', name: 'old_total_revenue', width: 150, align: 'right', classes: 'right', editable: false, hidden: !showOld, sorttype: 'number' },
                            { label: 'New Total Revenue', name: 'new_total_revenue', width: 150, align: 'right', classes: 'right', editable: false, hidden: !showSynthesis, sorttype: 'number' },
                            { label: 'Nort Total Revenue', name: 'nort_total_revenue', width: 150, align: 'right', classes: 'right', editable: false, hidden: !showSynthesis, sorttype: 'number' },
                            { label: 'Old Cash Flow Amount', name: 'old_cash_flow_amount', width: 150, align: 'right', classes: 'right', editable: false, hidden: !showOld, sorttype: 'number' },
                            { label: 'New Cash Flow Amount', name: 'new_cash_flow_amount', width: 150, align: 'right', classes: 'right', editable: false, hidden: !showSynthesis, sorttype: 'number' },
                            { label: 'Nort Cash Flow Amount', name: 'nort_cash_flow_amount', width: 150, align: 'right', classes: 'right', editable: false, hidden: !showSynthesis, sorttype: 'number' },
                            { label: 'Old Margin', name: 'old_margin', width: 150, align: 'right', classes: 'right', editable: false, hidden: !showOld, sorttype: 'number' },
                            { label: 'New Margin', name: 'new_margin', width: 150, align: 'right', classes: 'right', editable: false, hidden: !showSynthesis, sorttype: 'number' },
                            { label: 'Nort Margin', name: 'nort_margin', width: 150, align: 'right', classes: 'right', editable: false, hidden: !showSynthesis, sorttype: 'number' },
                            { label: 'Old Market Value Insurance', name: 'old_market_value_insurance', width: 150, align: 'right', classes: 'right', editable: false, hidden: !showOld, sorttype: 'number' },
                            { label: 'New Market Value Insurance', name: 'new_market_value_insurance', width: 150, align: 'right', classes: 'right', editable: false, hidden: !showSynthesis, sorttype: 'number' },
                            { label: 'Old Insurance Value Crops', name: 'old_insurance_value_crops', width: 150, align: 'right', classes: 'right', editable: false, hidden: !showOld, sorttype: 'number' },
                            { label: 'New Insurance Value Crops', name: 'new_insurance_value_crops', width: 150, align: 'right', classes: 'right', editable: false, hidden: !showSynthesis, sorttype: 'number' },
                            { label: 'Old Arm Total Budget', name: 'old_arm_total_budget', width: 150, align: 'right', classes: 'right', editable: false, hidden: !showOld, sorttype: 'number' },
                            { label: 'New Arm Total Budget', name: 'new_arm_total_budget', width: 150, align: 'right', classes: 'right', editable: false, hidden: !showSynthesis, sorttype: 'number' },
                            { label: 'Nort Arm Total Budget', name: 'nort_arm_total_budget', width: 150, align: 'right', classes: 'right', editable: false, hidden: !showSynthesis, sorttype: 'number' },
                            { label: 'Old Dist Total Budget', name: 'old_dist_total_budget', width: 150, align: 'right', classes: 'right', editable: false, hidden: !showOld, sorttype: 'number' },
                            { label: 'New Dist Total Budget', name: 'new_dist_total_budget', width: 150, align: 'right', classes: 'right', editable: false, hidden: !showSynthesis, sorttype: 'number' },
                            { label: 'Old Third Party Total Budget', name: 'old_third_party_total_budget', width: 150, align: 'right', classes: 'right', editable: false, hidden: !showOld, sorttype: 'number' },
                            { label: 'New Third Party Total Budget', name: 'new_third_party_total_budget', width: 150, align: 'right', classes: 'right', editable: false, hidden: !showSynthesis, sorttype: 'number' },
                            { label: 'Old Rent Budget', name: 'old_rent_budget', width: 150, align: 'right', classes: 'right', editable: false, hidden: !showOld, sorttype: 'number' },
                            { label: 'New Rent Budget', name: 'new_rent_budget', width: 150, align: 'right', classes: 'right', editable: false, hidden: !showSynthesis, sorttype: 'number' },
                            { label: 'Old Ins Budget', name: 'old_ins_budget', width: 150, align: 'right', classes: 'right', editable: false, hidden: !showOld, sorttype: 'number' },
                            { label: 'New Ins Budget', name: 'new_ins_budget', width: 150, align: 'right', classes: 'right', editable: false, hidden: !showSynthesis, sorttype: 'number' },
                            { label: 'Old Closed Date', name: 'old_closed_date', width: 150, align: 'left', classes: 'left', editable: false, hidden: !showOld, sorttype: 'string' },
                            { label: 'Old Cash Flow', name: 'old_cash_flow', width: 150, align: 'right', classes: 'right', editable: false, hidden: !showOld, sorttype: 'number' },
                            { label: 'Old Risk Cushion', name: 'old_risk_cushion', width: 150, align: 'right', classes: 'right', editable: false, hidden: !showOld, sorttype: 'number' },
                            { label: 'Old Borrower Rating', name: 'old_borrower_rating', width: 150, align: 'center', classes: 'center', editable: false, hidden: !showOld, sorttype: 'number' },
                            { label: 'New Borrower Rating', name: 'new_borrower_rating', width: 150, align: 'center', classes: 'center', editable: false, hidden: !showSynthesis, sorttype: 'number' },
                            { label: 'Old Return Percent', name: 'old_return_percent', width: 150, align: 'right', classes: 'right', editable: false, hidden: !showOld, sorttype: 'number' },
                            { label: 'New Return Percent', name: 'new_return_percent', width: 150, align: 'right', classes: 'right', editable: false, hidden: !showSynthesis, sorttype: 'number' },
                            { label: 'Old Risk Cushion Percent', name: 'old_risk_cushion_percent', width: 150, align: 'right', classes: 'right', editable: false, hidden: !showOld, sorttype: 'number' },
                            { label: 'New Risk Cushion Percent', name: 'new_risk_cushion_percent', width: 150, align: 'right', classes: 'right', editable: false, hidden: !showSynthesis, sorttype: 'number' },
                            { label: 'Old Cnt Collateral Items', name: 'old_cnt_collateral_items', width: 150, align: 'center', classes: 'center', editable: false, hidden: !showOld, sorttype: 'number' },
                            { label: 'New Cnt Collateral Items', name: 'new_cnt_collateral_items', width: 150, align: 'center', classes: 'center', editable: false, hidden: !showSynthesis, sorttype: 'number' },
                            { label: 'Old Cnt Collateral FSA', name: 'old_cnt_collateral_FSA', width: 150, align: 'center', classes: 'center', editable: false, hidden: !showOld, sorttype: 'number' },
                            { label: 'Old Cnt Collateral FSA Value', name: 'old_cnt_collateral_FSA_value', width: 150, align: 'center', classes: 'right', editable: false, hidden: !showOld, sorttype: 'number' },
                            { label: 'Old Net Market Value Crops', name: 'old_Net_Market_Value_Crops', width: 150, align: 'right', classes: 'right', editable: false, hidden: !showOld, sorttype: 'number' },
                            { label: 'New Net Market Value Crops', name: 'new_Net_Market_Value_Crops', width: 150, align: 'right', classes: 'right', editable: false, hidden: !showSynthesis, sorttype: 'number' },
                            { label: 'Old Net Market Value FSA', name: 'old_Net_Market_Value_FSA', width: 150, align: 'right', classes: 'right', editable: false, hidden: !showOld, sorttype: 'number' },
                            { label: 'New Net Market Value FSA', name: 'new_Net_Market_Value_FSA', width: 150, align: 'right', classes: 'right', editable: false, hidden: !showSynthesis, sorttype: 'number' },
                            { label: 'Old Net Market Value Livestock', name: 'old_Net_Market_Value_Livestock', width: 150, align: 'right', classes: 'right', editable: false, hidden: !showOld, sorttype: 'number' },
                            { label: 'New Net Market Value Livestock', name: 'new_Net_Market_Value_Livestock', width: 150, align: 'right', classes: 'right', editable: false, hidden: !showSynthesis, sorttype: 'number' },
                            { label: 'Old Cnt Collateral Stored', name: 'old_cnt_collateral_stored', width: 150, align: 'center', classes: 'center', editable: false, hidden: !showOld, sorttype: 'number' },
                            { label: 'Old Cnt Collateral Stored Value', name: 'old_cnt_collateral_stored_value', width: 150, align: 'center', classes: 'right', editable: false, hidden: !showOld, sorttype: 'number' },
                            { label: 'Old Net Market Value Stored Crops', name: 'old_Net_Market_Value_Stored_Crops', width: 150, align: 'right', classes: 'right', editable: false, hidden: !showOld, sorttype: 'number' },
                            { label: 'New Net Market Value Stored Crops', name: 'new_Net_Market_Value_Stored_Crops', width: 150, align: 'right', classes: 'right', editable: false, hidden: !showSynthesis, sorttype: 'number' },
                            { label: 'Old Cnt Collateral Equip', name: 'old_cnt_collateral_equip', width: 150, align: 'center', classes: 'center', editable: false, hidden: !showOld, sorttype: 'number' },
                            { label: 'Old Net Market Value Equipment', name: 'old_Net_Market_Value_Equipment', width: 150, align: 'right', classes: 'right', editable: false, hidden: !showOld, sorttype: 'number' },
                            { label: 'New Net Market Value Equipment', name: 'new_Net_Market_Value_Equipment', width: 150, align: 'right', classes: 'right', editable: false, hidden: !showSynthesis, sorttype: 'number' },
                            { label: 'Old Cnt Collateral Realestate', name: 'old_cnt_collateral_realestate', width: 150, align: 'center', classes: 'center', editable: false, hidden: !showOld, sorttype: 'number' },
                            { label: 'Old Net Market Value Real Estate', name: 'old_Net_Market_Value_Real_Estate', width: 150, align: 'right', classes: 'right', editable: false, hidden: !showOld, sorttype: 'number' },
                            { label: 'New Net Market Value Real Estate', name: 'new_Net_Market_Value_Real_Estate', width: 150, align: 'right', classes: 'right', editable: false, hidden: !showSynthesis, sorttype: 'number' },
                            { label: 'Old Cnt Collateral Other', name: 'old_cnt_collateral_other', width: 150, align: 'center', classes: 'center', editable: false, hidden: !showOld, sorttype: 'number' },
                            { label: 'Old Net Market Value Other', name: 'old_Net_Market_Value_Other', width: 150, align: 'right', classes: 'right', editable: false, hidden: !showOld, sorttype: 'number' },
                            { label: 'New Net Market Value Other', name: 'new_Net_Market_Value_Other', width: 150, align: 'right', classes: 'right', editable: false, hidden: !showSynthesis, sorttype: 'number' },
                            { label: 'Old Status', name: 'old_status', width: 150, align: 'center', classes: 'center', editable: false, hidden: !showOld, sorttype: 'number' },
                            { label: 'Old Deleted', name: 'old_deleted', width: 150, align: 'center', classes: 'center', editable: false, hidden: !showOld, sorttype: 'number' },
                            { label: 'Old Archived', name: 'old_archived', width: 150, align: 'center', classes: 'center', editable: false, hidden: !showOld, sorttype: 'number' }
                        ],
                        viewrecords: true,
                        loadonce: true,
                        restoreAfterSelect: false,
                        saveAfterSelect: true,
                        sortable: true,
                        onPaging: obj.changePage,
                        height: 'auto',
                        rowNum: 100,
                        pager: "#jqGridPager"
                    });
                },
                formatPhone: function (phone) {
                    if (phone) {
                        phone = phone.replace(/[^\d]/g, "");

                        if (phone.length == 10) {
                            return phone.replace(/(\d{3})(\d{3})(\d{4})/, "($1) $2-$3");
                        }

                        return phone;
                    }
                    return '';
                },
                formatSSN: function (ssn) {
                    if (ssn) {
                        ssn = ssn.replace(/[^\d]/g, "");

                        if (ssn.length == 9) {
                            return ssn.replace(/(\d{3})(\d{2})(\d{4})$/, "$1-$2-$3");
                        }

                        return ssn;
                    }
                    return '';
                },
                search: function (self) {
                    if (timer) { clearTimeout(timer); }
                    timer = setTimeout(function () {
                        obj.changePage();
                        $("#jqGrid").jqGrid('filterInput', self.value);
                    }, 0);
                },
                changePage: function () {
                    lastSelection = "";
                    var grid = $("#jqGrid");
                    var id = $('.inline-edit-cell').parent().parent().prop('id');
                    if (id != undefined) {
                        grid.jqGrid('saveRow', id);
                        grid.jqGrid('restoreRow', id);
                        var row = grid.jqGrid('getRowData', id);
                        if (!jQuery.isEmptyObject(row)) {
                            var dataobj = jQuery("#jqGrid").jqGrid('getGridParam', 'data');
                            index = dataobj.findIndex(x => x.rowid == row.rowid);
                            if (row.rowid > 0 && row.Actionstatus != 1) {
                                row.Actionstatus = 2;
                                grid.jqGrid('getGridParam', 'data')[index] = row;
                            }
                        }
                    }
                }
            }
            obj.Init();
        });
    </script>
</asp:Content>
