﻿using Newtonsoft.Json;
using System;
using System.Data;
using System.Web;
using System.Web.Services;

public partial class admin_Migration_Summary_V_2 : BasePage
{
    public admin_Migration_Summary_V_2()
    {
        Table_Name = "Migration_Summary";
    }

    [WebMethod]
    public static string GetTableInfo()
    {
        Count_Query = @"Declare @Count int;
                        select @Count = Count(*) FROM [Migration_Summary] where [new_loan_full_id] is not null and old_loan_status = 150";

        return Get_Table_Info();
    }

    [WebMethod]
    public static string GetListItems()
    {
        string sql_qry = @"select * from ref_list_item
                        where list_group_code in ( 'BORROWER_ENTITY_TYPE', 'PFC')";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static string LoadTable()
    {
        string sql_qry = @"SELECT * FROM [Migration_Summary] where [new_loan_full_id] is not null and [new_loan_full_id] != '' and old_loan_status = 150";

        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static string DownloadExcel()
    {
        string sql_qry = @"SELECT * FROM [Migration_Summary_Excel_Report_View]";

        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return JsonConvert.SerializeObject(dt);
    }
}