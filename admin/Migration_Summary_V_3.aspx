﻿<%@ Page Title="" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="Migration_Summary_V_3.aspx.cs" Inherits="admin_Migration_Summary_V_3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style>
         .ui-jqgrid .ui-jqgrid-htable th div {
            height: auto;
            overflow: hidden;
            padding-right: 4px;
            padding-top: 2px;
            position: relative;
            vertical-align: text-top;
            word-wrap: break-word; /* IE 5.5+ and CSS3 */
            white-space: pre-wrap; /* CSS3 */
            white-space: normal !important;
        }

        @media screen and (max-width: 1280px) {
            .ui-jqgrid-bdiv {
                max-height: 60vh;
            }
        }

        @media screen and (max-width: 1024px) {
            .ui-jqgrid-bdiv {
                max-height: 55vh;
            }
        }

        @media screen and (min-width: 1280px) {
            .ui-jqgrid-bdiv {
                max-height: 65vh;
            }
        }

        .cvteste {
            padding-right: 14px !important;
        }

        #content {
            padding: 14px 14px !important;
        }

        .switch-controls {
            margin: 0 20px 0px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="loan-migration-summary">
        <div class="row switch-controls">
            <div class="switch-control" id="diffOnly" style="width: 180px !important;">
                <label for="enableBtn">Include No Diff</label>
                <div class="toggle-btn small">            
                    <input type="checkbox" id="diffOnlyBtn" class="cb-value"/>
                    <span class="round-btn"></span>
                </div>
            </div>
        </div>
        <div class="loader"></div>
        <div class="row main-content">
            <div style="margin-left: 35px">
                <table id="jqGrid"></table>
                <div id="jqGridPager"></div>
            </div>
        </div>
        <asp:HiddenField ID="LoanFullID" runat="server" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" Runat="Server">
    <script type="text/javascript">  
        var timer;
        var default_diff_columns = [
            'diff_ARM_commit',
            'diff_dist_commit',
            'diff_CF',
            'diff_RC',
            'diff_Market_Value_Crops',
            'diff_market_value_total'
        ];

        var COLUMNS = [
            { label: 'Old Loan ID', name: 'old_loan_ID', width: 150, align: 'center', classes: 'center', editable: false, hidden: false, sorttype: 'number' },
            { label: 'New Loan Full ID', name: 'new_Loan_Full_ID', width: 150, align: 'center', classes: 'center', editable: false, hidden: false, sorttype: 'string' },
            { label: 'New Loan Status', name: 'new_loan_status', width: 150, align: 'center', editable: false, hidden: false, sorttype: 'string' },
            { label: 'New Crop Year', name: 'new_crop_year', width: 150, align: 'center', classes: 'center', editable: false, hidden: false, sorttype: 'number' },
            { label: 'New Office Name', name: 'new_office_name', width: 150, align: 'left', classes: 'left', editable: false, hidden: false, sorttype: 'string' },
            { label: 'Old ARM Commit', name: 'old_ARM_commit', width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, hidden: false, sorttype: 'number' },
            { label: 'New ARM Commit', name: 'new_ARM_commit', width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, hidden: false, sorttype: 'number' },
            { label: 'Diff ARM Commit', name: 'diff_ARM_commit', width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, hidden: false, sorttype: 'number' },
            { label: 'Old Dist Commit', name: 'old_dist_commit', width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, hidden: false, sorttype: 'number' },
            { label: 'New Dist Commit', name: 'new_dist_commit', width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, hidden: false, sorttype: 'number' },
            { label: 'Diff Dist Commit', name: 'diff_dist_commit', width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, hidden: false, sorttype: 'number' },
            { label: 'Old CF', name: 'old_CF', width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, hidden: false, sorttype: 'number' },
            { label: 'New CF', name: 'new_CF', width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, hidden: false, sorttype: 'number' },
            { label: 'Diff CF', name: 'diff_CF', width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, hidden: false, sorttype: 'number' },
            { label: 'Old RC', name: 'old_RC', width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, hidden: false, sorttype: 'number' },
            { label: 'New RC', name: 'new_RC', width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, hidden: false, sorttype: 'number' },
            { label: 'Diff RC', name: 'diff_RC', width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, hidden: false, sorttype: 'number' },
            { label: 'Old Mkt Value Crops', name: 'old_market_value_crops', width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, hidden: false, sorttype: 'number' },
            { label: 'New Mkt Value Crops', name: 'new_Market_Value_Crops', width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, hidden: false, sorttype: 'number' },
            { label: 'Diff Mkt Value Crops', name: 'diff_Market_Value_Crops', width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, hidden: false, sorttype: 'number' },
            { label: 'Old Ins Value Crops', name: 'old_ins_value_crops', width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, hidden: false, sorttype: 'number' },
            { label: 'New Ins Value Crops', name: 'new_ins_value_crops', width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, hidden: false, sorttype: 'number' },
            { label: 'Diff Ins Value Crops', name: 'diff_ins_value_crops', width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, hidden: false, sorttype: 'number' },
            { label: 'Old Mkt Value Total', name: 'old_market_value_total', width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, hidden: false, sorttype: 'number' },
            { label: 'New Mkt Value Total', name: 'new_market_value_total', width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, hidden: false, sorttype: 'number' },
            { label: 'Diff Mkt Value Total', name: 'diff_market_value_total', width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, hidden: false, sorttype: 'number' }
        ];

        $(function () {
            var obj = {
                Init: function () {
                    $('.loader').show();
                    $('#navbtnexceldownload').show();

                    obj.bindGrid();

                    $('#navbtnadd').addClass('disabled');
                    $('#navbtnsave').addClass('disabled');

                    $("#navbtnrefresh").on('click', function () {
                        $('.loader').show();
                        $('#txtSearchBar').val('');
                        obj.bindGrid();
                    });

                    $("#txtSearchBar").on("keyup", function () {
                        var self = this;
                        obj.search(self);
                    });

                    $("#navbtndownload").on("click", function () {
                        $("#jqGrid").jqGrid("exportToExcel", {
                            includeLabels: true,
                            includeGroupHeader: true,
                            includeFooter: true,
                            fileName: "Migration Summary Comparison.xlsx",
                            maxlength: 40
                        });
                    });

                    $('#navbtncolumns').click(function () {
                        var diff_columns = [];

                        $("#jqGrid").columnChooser({
                            done: function (event) {
                                if (event) {
                                    var cols = $('#jqGrid').jqGrid('getGridParam', "colModel");
                                    var colChosen = [];

                                    for (i = 0; i < cols.length; i++) {                                        
                                        if (!cols[i]['hidden']) {
                                            colChosen.push(cols[i]);

                                            if (cols[i].name.toLowerCase().includes('diff')) {
                                                diff_columns.push(cols[i].name);
                                            }
                                        }
                                    }

                                    COLUMNS = cols;
                                    default_diff_columns = diff_columns;

                                    $('.loader').show();
                                    obj.bindGrid($(this).is(':checked'), diff_columns);
                                }
                            }
                        });
                    });

                    $('#navbtnexceldownload').click(function () {
                        $('.loader').show();
                        obj.downloadExcel();
                    });

                    $('#diffOnlyBtn').change(function () {
                        var includeNoDiff = $(this).is(':checked'); 
                        $('.loader').show();
                        obj.bindGrid(includeNoDiff);
                    });
                },
                downloadExcel: function () {
                    $.ajax({
                        type: "POST",
                        url: "Migration_Summary_V_3.aspx/DownloadExcel",
                        data: JSON.stringify({}),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var exportData = JSON.parse(data.d);
                            var ws = XLSX.utils.json_to_sheet(exportData);
                            var wb = XLSX.utils.book_new();

                            if (exportData && exportData.length > 0) {
                                var wscols = []

                                Object.keys(exportData[0]).forEach((key, index) => {
                                    var length = key ? key.length : 14;

                                    if (key.toLowerCase().includes('email')) {
                                        length = 30;
                                    }
                                    
                                    wscols.push({ wch: length });
                                    ws[XLSX.utils.encode_col(index) + '1'].v = obj.getHeader(key);
                                });

                                ws['!cols'] = wscols;

                                XLSX.utils.book_append_sheet(wb, ws, 'Migration Summary');
                                XLSX.writeFile(wb, `Migration Summary.xlsx`);
                            }

                            $('.loader').hide();
                        }
                    });
                },
                getMaxLength(obj, key) {
                    var values = obj.map(a => (a[key] || '').length);
                    return Math.max(...values);
                },
                getHeader: function (x) {
                    if (x) {
                        let array = x.split('_');
                        array = array.map(s => {
                            return s.charAt(0).toUpperCase() + s.slice(1);
                        });
                        return array.join(' ');
                    } else {
                        return x;
                    }
                },
                getTableInfo: function (includeNoDiff = false, diff_columns = []) {
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "Migration_Summary_V_3.aspx/GetTableInfo",
                        data: JSON.stringify({ includeNoDiff: includeNoDiff, diff_columns: diff_columns }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            var data = JSON.parse(res.d);

                            var table_info = data.Table_Info[0];
                            $('#txtTableName').html(table_info.Table_Name);
                            $('#txtLastUpdated').html(table_info.Last_Updated);
                            $('#txtRecordsCount').html(table_info.Records_Count);
                            
                        }
                    });
                },
                bindGrid: function (includeNoDiff = false, diff_columns = default_diff_columns) {
                    $.ajax({
                        type: "POST",
                        url: "Migration_Summary_V_3.aspx/LoadTable",
                        data: JSON.stringify({ includeNoDiff: includeNoDiff, diff_columns: diff_columns }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var resData = JSON.parse(data.d);

                            $('.loader').show();
                            for (var i = 0; i < resData.length; i++) {
                                resData[i].rowid = i + 1;
                            }

                            obj.loadgrid(resData);
                            obj.getTableInfo(includeNoDiff, diff_columns);
                            $('.loader').hide();
                        }
                    });
                },
                loadgrid: function (data) {
                    $.jgrid.gridUnload("#jqGrid");
                    $("#jqGrid").jqGrid({
                        datatype: "local",
                        styleUI: 'Bootstrap',
                        data: data,
                        colModel: obj.getColModels(COLUMNS),
                        viewrecords: true,
                        loadonce: true,
                        restoreAfterSelect: false,
                        saveAfterSelect: true,
                        sortable: true,
                        onPaging: obj.changePage,
                        height: 'auto',
                        rowNum: 100,
                        pager: "#jqGridPager"
                    });
                },
                 getColModels: function (columns) {
                    return [
                        { label: 'rowid', name: 'rowid', width: 75, editable: false, key: true, hidden: true, hidedlg: true },
                        ...columns
                    ]
                },
                search: function (self) {
                    if (timer) { clearTimeout(timer); }
                    timer = setTimeout(function () {
                        obj.changePage();
                        $("#jqGrid").jqGrid('filterInput', self.value);
                        var length = jQuery("#jqGrid").jqGrid('getGridParam', 'records');
                        $('#txtRecordsCount').html('Records Count: ' + length);
                    }, 0);
                },
                changePage: function () {
                    lastSelection = "";
                    var grid = $("#jqGrid");
                    var id = $('.inline-edit-cell').parent().parent().prop('id');
                    if (id != undefined) {
                        grid.jqGrid('saveRow', id);
                        grid.jqGrid('restoreRow', id);
                        var row = grid.jqGrid('getRowData', id);
                        if (!jQuery.isEmptyObject(row)) {
                            var dataobj = jQuery("#jqGrid").jqGrid('getGridParam', 'data');
                            index = dataobj.findIndex(x => x.rowid == row.rowid);
                            if (row.rowid > 0 && row.Actionstatus != 1) {
                                row.Actionstatus = 2;
                                grid.jqGrid('getGridParam', 'data')[index] = row;
                            }
                        }
                    }
                }
            }
            obj.Init();
        });
    </script>
</asp:Content>
