﻿using Newtonsoft.Json;
using System.Data;
using System.Linq;
using System.Web.Services;

public partial class admin_Migration_Summary_V_3 : BasePage
{
    public admin_Migration_Summary_V_3()
    {
        Table_Name = "Migration_Summary";
    }

    [WebMethod]
    public static string GetTableInfo(string[] diff_columns, bool includeNoDiff = false)
    {
        Count_Query = @"Declare @Count int;
                        select @Count = Count(1) FROM Migration_Summary_Excel_Report_View ";

        string diff_query = string.Empty;

        if (diff_columns != null && diff_columns.Length > 0)
        {
            diff_query = string.Join(" OR ", diff_columns.Select(a => "[" + a + "] != '0'"));
        }

        if (!includeNoDiff && diff_columns != null && diff_columns.Length > 0)
        {
            Count_Query += @"Where " + diff_query;
        }

        return Get_Table_Info();
    }

    [WebMethod]
    public static string LoadTable(string[] diff_columns, bool includeNoDiff = false)
    {
        string sql_qry = @"SELECT
	                         [old_loan_ID]
	                        ,[new_Loan_Full_ID]
	                        ,[new_crop_year]
	                        ,[new_office_name]
	                        ,[new_loan_status]
	                        ,[old_ARM_commit]
	                        ,[new_ARM_commit]
	                        ,[diff_ARM_commit]
	                        ,[old_dist_commit]
	                        ,[new_dist_commit]
	                        ,[diff_dist_commit]	
	                        ,[old_CF]
	                        ,[new_CF]
	                        ,[diff_CF]
	                        ,[old_RC]
	                        ,[new_RC]
	                        ,[diff_RC]
	                        ,[old_market_value_crops]
	                        ,[new_Market_Value_Crops]
	                        ,[diff_Market_Value_Crops]
	                        ,[old_ins_value_crops]
	                        ,[new_ins_value_crops]
	                        ,[diff_ins_value_crops]
	                        ,[old_market_value_total]
	                        ,[new_market_value_total]
	                        ,[diff_market_value_total]
                        FROM Migration_Summary_Excel_Report_View ";

        string diff_query = string.Empty;

        if(diff_columns != null && diff_columns.Length > 0)
        {
            diff_query = string.Join(" OR ", diff_columns.Select(a => "[" + a + "] != '0'"));
        }
        
        if(!includeNoDiff && diff_columns != null && diff_columns.Length > 0)
        {
            sql_qry += @"Where " + diff_query;
        }

        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static string DownloadExcel()
    {
        string sql_qry = @"SELECT * FROM [Migration_Summary_Excel_Report_View]";

        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return JsonConvert.SerializeObject(dt);
    }
}