﻿<%@ Page Title="Migration Summary" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="Migration_Summary_V_4.aspx.cs" Inherits="admin_Migration_Summary_V_4" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style>
         .ui-jqgrid .ui-jqgrid-htable th div {
            height: auto;
            overflow: hidden;
            padding-right: 4px;
            padding-top: 2px;
            position: relative;
            vertical-align: text-top;
            word-wrap: break-word; /* IE 5.5+ and CSS3 */
            white-space: pre-wrap; /* CSS3 */
            white-space: normal !important;
        }

        @media screen and (max-width: 1280px) {
            .ui-jqgrid-bdiv {
                max-height: 60vh;
            }
        }

        @media screen and (max-width: 1024px) {
            .ui-jqgrid-bdiv {
                max-height: 55vh;
            }
        }

        @media screen and (min-width: 1280px) {
            .ui-jqgrid-bdiv {
                max-height: 65vh;
            }
        }

        .cvteste {
            padding-right: 14px !important;
        }

        #content {
            padding: 14px 14px !important;
        }

        .switch-controls {
            margin: 0 20px 0px !important;
        }

        .fc-head-container thead tr, .table thead tr {
            height: 50px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="loan-migration-summary">
        <div class="row switch-controls">
            <div class="switch-control" id="diffOnly" style="width: 180px !important;">
                <label for="enableBtn">Include No Diff</label>
                <div class="toggle-btn small">            
                    <input type="checkbox" id="diffOnlyBtn" class="cb-value"/>
                    <span class="round-btn"></span>
                </div>
            </div>
        </div>
        <div class="loader"></div>
        <div class="row main-content">
            <div style="margin-left: 35px">
                <table id="jqGrid"></table>
                <div id="jqGridPager"></div>
            </div>
        </div>
        <asp:HiddenField ID="LoanFullID" runat="server" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" Runat="Server">
    <script type="text/javascript">  
        var timer;
        var default_diff_columns = [
            'diff_arm_commitment',
            'diff_dist_commitment',
            'diff_third_party_commitment',
            'diff_cash_rent_budget',
            'diff_cash_flow_amount',
            'diff_total_other_income',
            'diff_crop_mkt_value',
            'diff_crop_ins_value',
            'diff_estimated_interest',
        ];

        var COLUMNS = [
            { label: 'Old Loan ID', name: 'old_loan_id', width: 150, align: 'center', classes: 'center', editable: false, sorttype: 'number' },
            { label: 'New Loan Full ID', name: 'new_loan_full_id', width: 150, align: 'center', classes: 'center', editable: false, sorttype: 'string' },
            { label: 'New Crop Year', name: 'crop_year', width: 150, align: 'center', classes: 'center', editable: false, sorttype: 'number' },
            { label: 'New Office Name', name: 'office_name', width: 150, align: 'left', classes: 'left', editable: false, sorttype: 'number' },
            { label: 'Adj Old Arm Commitment', name: 'adj_old_arm_commitment', width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, sorttype: 'number' },
            { label: 'New Arm Commitment', name: 'new_arm_commitment', width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, sorttype: 'number' },
            { label: 'Diff Arm Commitment', name: 'diff_arm_commitment', width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, sorttype: 'number' },
            { label: 'Adj Old Dist Commitment', name: 'adj_old_dist_commitment', width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, sorttype: 'number' },
            { label: 'New Dist Commitment', name: 'new_dist_commitment', width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, sorttype: 'number' },
            { label: 'Diff Dist Commitment', name: 'diff_dist_commitment', width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, sorttype: 'number' },
            { label: 'Adj Old Third Party Commitment', name: 'adj_old_third_party_commitment', width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, sorttype: 'number' },
            { label: 'New Third Party Commitment', name: 'new_third_party_commitment', width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, sorttype: 'number' },
            { label: 'Diff Third Party Commitment', name: 'diff_third_party_commitment', width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, sorttype: 'number' },
            { label: 'Adj Old Cash Rent Budget', name: 'adj_old_cash_rent_budget', hidden: true, width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, sorttype: 'number' },
            { label: 'New Cash Rent Budget', name: 'new_cash_rent_budget', hidden: true, width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, sorttype: 'number' },
            { label: 'Diff Cash Rent Budget', name: 'diff_cash_rent_budget', hidden: true, width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, sorttype: 'number' },
            { label: 'Adj Old Cash Flow Amount', name: 'adj_old_cash_flow_amount', width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, sorttype: 'number' },
            { label: 'New Cash Flow Amount', name: 'new_cash_flow_amount', width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, sorttype: 'number' },
            { label: 'Diff Cash Flow Amount', name: 'diff_cash_flow_amount', width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, sorttype: 'number' },
            { label: 'Adj Old Total Other Income', name: 'adj_old_total_other_income', width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, sorttype: 'number' },
            { label: 'New Total Other Income', name: 'new_total_other_income', width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, sorttype: 'number' },
            { label: 'Diff Total Other Income', name: 'diff_total_other_income', width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, sorttype: 'number' },
            { label: 'Adj Old Crop Mkt Value', name: 'adj_old_crop_mkt_value', width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, sorttype: 'number' },
            { label: 'New Crop Mkt Value', name: 'new_crop_mkt_value', width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, sorttype: 'number' },
            { label: 'Diff Crop Mkt Value', name: 'diff_crop_mkt_value', width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, sorttype: 'number' },
            { label: 'Adj Old Crop Disc Mkt Value', name: 'adj_old_crop_disc_mkt_value', hidden: true, width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, sorttype: 'number' },
            { label: 'New Crop Disc Mkt Value', name: 'new_crop_disc_mkt_value', hidden: true, width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, sorttype: 'number' },
            { label: 'Diff Crop Disc Mkt Value', name: 'diff_crop_disc_mkt_value', hidden: true, width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, sorttype: 'number' },
            { label: 'Adj Old Crop Ins Value', name: 'adj_old_crop_ins_value', width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, sorttype: 'number' },
            { label: 'New Crop Ins Value', name: 'new_crop_ins_value', width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, sorttype: 'number' },
            { label: 'Diff Crop Ins Value', name: 'diff_crop_ins_value', width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, sorttype: 'number' },
            { label: 'Adj Old Crop Disc Ins Value', name: 'adj_old_crop_disc_ins_value', hidden: true, width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, sorttype: 'number' },
            { label: 'New Crop Disc Ins Value', name: 'new_crop_disc_ins_value', hidden: true, width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, sorttype: 'number' },
            { label: 'Diff Crop Disc Ins Value', name: 'diff_crop_disc_ins_value', hidden: true, width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, sorttype: 'number' },
            { label: 'Adj Old Collateral Mkt Value', name: 'adj_old_mkt_collateral_value', hidden: true, width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, sorttype: 'number' },
            { label: 'New Collateral Mkt Value', name: 'new_mkt_collateral_value', hidden: true, width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, sorttype: 'number' },
            { label: 'Diff Collateral Mkt Value', name: 'diff_mkt_collateral_value', hidden: true, width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, sorttype: 'number' },
            { label: 'Adj Old Disc Collateral Mkt Value', name: 'adj_old_disc_mkt_collateral_value', hidden: true, width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, sorttype: 'number' },
            { label: 'New Disc Collateral Mkt Value', name: 'new_disc_mkt_collateral_value', hidden: true, width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, sorttype: 'number' },
            { label: 'Diff Disc Collateral Mkt Value', name: 'diff_disc_mkt_collateral_value', hidden: true, width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, sorttype: 'number' },
            { label: 'Adj Old Collateral Ins Value', name: 'adj_old_ins_collateral_value', hidden: true, width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, sorttype: 'number' },
            { label: 'New Collateral Ins Value', name: 'new_ins_collateral_value', hidden: true, width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, sorttype: 'number' },
            { label: 'Diff Collateral Ins Value', name: 'diff_ins_collateral_value', hidden: true, width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, sorttype: 'number' },
            { label: 'Adj Old Disc Collateral Ins Value', name: 'adj_old_disc_ins_collateral_value', hidden: true, width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, sorttype: 'number' },
            { label: 'New Disc Collateral Ins Value', name: 'new_disc_ins_collateral_value', hidden: true, width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, sorttype: 'number' },
            { label: 'Diff Disc Collateral Ins Value', name: 'diff_disc_ins_collateral_value', hidden: true, width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, sorttype: 'number' },
            { label: 'Adj Old Estimated Interest', name: 'adj_old_estimated_interest', width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, sorttype: 'number' },
            { label: 'New Estimated Interest', name: 'new_estimated_interest', width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, sorttype: 'number' },
            { label: 'Diff Estimated Interest', name: 'diff_estimated_interest', width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, sorttype: 'number' },
            { label: 'Adj Old Ag Pro Requested Credit', name: 'adj_old_ag_pro_requested_credit', hidden: true, width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, sorttype: 'number' },
            { label: 'New Ag Pro Requested Credit', name: 'new_ag_pro_requested_credit', hidden: true, width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, sorttype: 'number' },
            { label: 'Diff Ag Pro Requested Credit', name: 'diff_ag_pro_requested_credit', hidden: true, width: 150, align: 'right', formatter: 'migrationAmountFormatter', classes: 'right', editable: false, sorttype: 'number' }
        ];

        $(function () {
            var obj = {
                Init: function () {
                    $('.loader').show();
                    $('#navbtnexceldownload').show();

                    obj.bindGrid();

                    $('#navbtnadd').addClass('disabled');
                    $('#navbtnsave').addClass('disabled');

                    $("#navbtnrefresh").on('click', function () {
                        $('.loader').show();
                        $('#txtSearchBar').val('');
                        obj.bindGrid();
                    });

                    $("#txtSearchBar").on("keyup", function () {
                        var self = this;
                        obj.search(self);
                    });

                    $("#navbtndownload").on("click", function () {
                        $("#jqGrid").jqGrid("exportToExcel", {
                            includeLabels: true,
                            includeGroupHeader: true,
                            includeFooter: true,
                            fileName: "Migration Summary Comparison.xlsx",
                            maxlength: 40
                        });
                    });

                    $('#navbtncolumns').click(function () {
                        var diff_columns = [];

                        $("#jqGrid").columnChooser({
                            done: function (event) {
                                if (event) {
                                    var cols = $('#jqGrid').jqGrid('getGridParam', "colModel");
                                    var colChosen = [];

                                    for (i = 0; i < cols.length; i++) {                                        
                                        if (!cols[i]['hidden']) {
                                            colChosen.push(cols[i]);

                                            if (cols[i].name.toLowerCase().includes('diff')) {
                                                diff_columns.push(cols[i].name);
                                            }
                                        }
                                    }

                                    COLUMNS = cols;
                                    default_diff_columns = diff_columns;

                                    $('.loader').show();
                                    obj.bindGrid($(this).is(':checked'), diff_columns);
                                }
                            }
                        });
                    });

                    $('#navbtnexceldownload').click(function () {
                        $('.loader').show();
                        obj.downloadExcel();
                    });

                    $('#diffOnlyBtn').change(function () {
                        var includeNoDiff = $(this).is(':checked'); 
                        $('.loader').show();
                        obj.bindGrid(includeNoDiff);
                    });
                },
                downloadExcel: function () {
                    $.ajax({
                        type: "POST",
                        url: "Migration_Summary_V_4.aspx/DownloadExcel",
                        data: JSON.stringify({}),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var exportData = JSON.parse(data.d);
                            var ws = XLSX.utils.json_to_sheet(exportData);
                            var wb = XLSX.utils.book_new();

                            if (exportData && exportData.length > 0) {
                                var wscols = []

                                Object.keys(exportData[0]).forEach((key, index) => {
                                    var length = key ? key.length : 14;

                                    if (key.toLowerCase().includes('email')) {
                                        length = 30;
                                    }
                                    
                                    wscols.push({ wch: length });
                                    ws[XLSX.utils.encode_col(index) + '1'].v = obj.getHeader(key);
                                });

                                ws['!cols'] = wscols;

                                XLSX.utils.book_append_sheet(wb, ws, 'Migration Summary');
                                XLSX.writeFile(wb, `Migration Summary.xlsx`);
                            }

                            $('.loader').hide();
                        }
                    });
                },
                getMaxLength(obj, key) {
                    var values = obj.map(a => (a[key] || '').length);
                    return Math.max(...values);
                },
                getHeader: function (x) {
                    if (x) {
                        let array = x.split('_');
                        array = array.map(s => {
                            return s.charAt(0).toUpperCase() + s.slice(1);
                        });
                        return array.join(' ');
                    } else {
                        return x;
                    }
                },
                getTableInfo: function (includeNoDiff = false, diff_columns = []) {
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "Migration_Summary_V_4.aspx/GetTableInfo",
                        data: JSON.stringify({ includeNoDiff: includeNoDiff, diff_columns: diff_columns }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            var data = JSON.parse(res.d);

                            var table_info = data.Table_Info[0];
                            $('#txtTableName').html(table_info.Table_Name);
                            $('#txtLastUpdated').html(table_info.Last_Updated);
                            $('#txtRecordsCount').html(table_info.Records_Count);
                            
                        }
                    });
                },
                bindGrid: function (includeNoDiff = false, diff_columns = default_diff_columns) {
                    $.ajax({
                        type: "POST",
                        url: "Migration_Summary_V_4.aspx/LoadTable",
                        data: JSON.stringify({ includeNoDiff: includeNoDiff, diff_columns: diff_columns }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var resData = JSON.parse(data.d);

                            $('.loader').show();
                            for (var i = 0; i < resData.length; i++) {
                                resData[i].rowid = i + 1;
                            }

                            obj.loadgrid(resData);
                            obj.getTableInfo(includeNoDiff, diff_columns);
                            $('.loader').hide();
                        }
                    });
                },
                loadgrid: function (data) {
                    $.jgrid.gridUnload("#jqGrid");
                    $("#jqGrid").jqGrid({
                        datatype: "local",
                        styleUI: 'Bootstrap',
                        data: data,
                        colModel: obj.getColModels(COLUMNS),
                        viewrecords: true,
                        loadonce: true,
                        restoreAfterSelect: false,
                        saveAfterSelect: true,
                        sortable: true,
                        onPaging: obj.changePage,
                        height: 'auto',
                        rowNum: 100,
                        pager: "#jqGridPager"
                    });
                },
                 getColModels: function (columns) {
                    return [
                        { label: 'rowid', name: 'rowid', width: 75, editable: false, key: true, hidden: true, hidedlg: true },
                        ...columns
                    ]
                },
                search: function (self) {
                    if (timer) { clearTimeout(timer); }
                    timer = setTimeout(function () {
                        obj.changePage();
                        $("#jqGrid").jqGrid('filterInput', self.value);
                        var length = jQuery("#jqGrid").jqGrid('getGridParam', 'records');
                        $('#txtRecordsCount').html('Records Count: ' + length);
                    }, 0);
                },
                changePage: function () {
                    lastSelection = "";
                    var grid = $("#jqGrid");
                    var id = $('.inline-edit-cell').parent().parent().prop('id');
                    if (id != undefined) {
                        grid.jqGrid('saveRow', id);
                        grid.jqGrid('restoreRow', id);
                        var row = grid.jqGrid('getRowData', id);
                        if (!jQuery.isEmptyObject(row)) {
                            var dataobj = jQuery("#jqGrid").jqGrid('getGridParam', 'data');
                            index = dataobj.findIndex(x => x.rowid == row.rowid);
                            if (row.rowid > 0 && row.Actionstatus != 1) {
                                row.Actionstatus = 2;
                                grid.jqGrid('getGridParam', 'data')[index] = row;
                            }
                        }
                    }
                }
            }
            obj.Init();
        });
    </script>
</asp:Content>
