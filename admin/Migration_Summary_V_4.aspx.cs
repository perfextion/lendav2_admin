﻿using Newtonsoft.Json;
using System.Data;
using System.Linq;
using System.Web.Services;

public partial class admin_Migration_Summary_V_4 : BasePage
{
    public admin_Migration_Summary_V_4()
    {
        Table_Name = "Migration_Summary";
    }

    [WebMethod]
    public static string GetTableInfo(string[] diff_columns, bool includeNoDiff = false)
    {
        Count_Query = @"Declare @Count int;
                        select @Count = Count(1) FROM vMigrationSummary ";

        string diff_query = string.Empty;

        if (diff_columns != null && diff_columns.Length > 0)
        {
            diff_query = string.Join(" OR ", diff_columns.Select(a => "( [" + a + "] != '0' OR [" + a + "] IS NULL)"));
        }

        if (!includeNoDiff && diff_columns != null && diff_columns.Length > 0)
        {
            Count_Query += @"Where " + diff_query;
        }

        return Get_Table_Info();
    }

    [WebMethod]
    public static string LoadTable(string[] diff_columns, bool includeNoDiff = false)
    {
        string sql_qry = @"SELECT
	                         *	                        
                        FROM vMigrationSummary ";

        string diff_query = string.Empty;

        if (diff_columns != null && diff_columns.Length > 0)
        {
            diff_query = string.Join(" OR ", diff_columns.Select(a => "( [" + a + "] != '0' OR [" + a + "] IS NULL)"));
        }

        if (!includeNoDiff && diff_columns != null && diff_columns.Length > 0)
        {
            sql_qry += @"Where " + diff_query;
        }

        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static string DownloadExcel()
    {
        string sql_qry = @"SELECT * FROM vMigrationSummaryExcel";

        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return JsonConvert.SerializeObject(dt);
    }
}