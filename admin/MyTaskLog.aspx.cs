﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_MyTaskLog : System.Web.UI.Page
{
    string dbKey = "gp_conn";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["UserID"] == null)
            {
                Response.Redirect("../Login.aspx");
            }
            else
            {
                lbl_table.Text = LoadTable();
            }
        }
    }
    protected string LoadTable()
    {
        string str_return = "";
        string loggedInUser = Session["UserID"].ToString();
        lbltaskdesc.Text = Session["Username"].ToString().ToUpper() + " Task Details :";

        //string str_sql_1 = " select chkin_time, chkout_time, Task_desc, loggedInUser, username,CONVERT(VARCHAR(20),DateAdd(hh,Datediff(ss,chkin_time, chkout_time)%(60*60*24),0),108) as duration from Z_Team_Status ts"
        //                 + " left join hr_users hu on ts.loggedInUser = hu.userid " +
        //                 "where chkin_time >= DATEADD(day,-2, GETDATE()) and loggedinuser='" + loggedInUser + "' ORDER BY chkin_time desc  ";

        string str_sql_1 = " SELECT ckkintime,CONVERT(VARCHAR, chkin_time, 111) +' ' +"
                + "  CONVERT(VARCHAR, DATEPART(hh, chkin_time)) + ':' +"
                + " RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, chkin_time)), 2) AS chkin_time,"
                + "  CONVERT(VARCHAR, chkout_time, 111) +' ' +"
                + " CONVERT(VARCHAR, DATEPART(hh, chkout_time)) + ':' +"
                + " RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, chkout_time)), 2) AS chkout_time,"
                + " Task_desc, CASE WHEN LEN(H) = 1"
                + " THEN '0' + h"
                + " ELSE h END  + ':' +"
                + " CASE WHEN LEN(m) = 1"
                + " THEN '0' + m"
                + " ELSE m END as duration"
                + "  FROM ("
                + "  select chkin_time as ckkintime ,chkin_time, chkout_time, Task_desc,loggedInUser,"
                + "  CAST(datediff([second], chkin_time, chkout_time) / 3600 AS VARCHAR(3)) as [h],"
                + "  CAST(datediff([second], chkin_time, chkout_time) % 3600 / 60 AS VARCHAR(3)) as [m]"
                + "  from Z_Team_Status ) as status"
                + "  where chkin_time >= DATEADD(day,-4, GETDATE()) and loggedinuser='" + loggedInUser + "' ORDER BY ckkintime desc";


        DataTable dt1 = gen_db_utils.gp_sql_get_datatable(str_sql_1, dbKey);


        str_return += "<div> <table id='tblActiveUsers' class='table table-striped table-bordered' >  "
                                      + "      <thead> "
                              + "     <tr> "
                              + "       <th >Checkin Time</th> "
                              + "       <th >Checkout Time</th> "
                              + "       <th >Duration</th> "
                              + "       <th >Task Description</th> "
                              + "    </tr> "
                              + "      </thead> "
                              + "        <tbody> ";

        foreach (DataRow dtr1 in dt1.Rows)
        {

            str_return += "<tr>"

                              + "<td>" + dtr1["chkin_time"].ToString() + "</td>"
                               + "<td>" + dtr1["chkout_time"].ToString() + "</td>"
                               + "<td>" + dtr1["duration"].ToString() + "</td>"
                               + "<td>" + dtr1["Task_desc"].ToString() + "</td>"
                             + "</tr>";

        }

        str_return += " </tbody> "
                                + " </table> "
                                  + "</div></div></div >";

        return str_return;

    }
}