﻿<%@ Page Title="" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="RMA_Insurance_Offer.aspx.cs" Inherits="admin_RMA_Insurance_Offer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="loader"></div>
    <div style="width: 1500px; margin-top: 18px">
        <div style="margin-left: 2.7%">
            <div class="form-inline">
                <div class="form-group">
                    <label for="txtcropyear" style="float: left; margin-top: 7px;">Crop Year :</label>
                    <input type="text" class="form-control" id="txtcropyear" style="float: left; margin-left: 5px; width: 150px;" />
                </div>
                <div class="form-group">
                    <label for="txtcropyear" style="float: left; margin-top: 7px;">RMA Price Key : </label>
                    <input type="text" class="form-control" id="txtRMA_Price_Key" style="float: left; margin-left: 5px; width: 230px;" />
                </div>
                <div class="form-group">
                    <label for="lblstate" style="float: left; margin-top: 7px;">State</label>
                    <select id="ddlstate" class="form-control" style="float: left; margin-left: 5px; width: 200px;">
                        <option value="">Select</option>
                        <option value="0">0:Arm Default</option>
                    </select>
                </div>
                <div class="form-group">
                    <div id="btnSearch" class="btn btn-md btn-primary" style="float: left; margin-left: 5px;">
                        Search
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="RMACrop_Year" runat="server" />
    <asp:HiddenField ID="RMAPriceKey" runat="server" />
    <asp:HiddenField ID="RMAState" runat="server" />
    <div class="row">
        <div class="col-md-11" style="padding-left: 51px; width: 1394px; padding-top: 8px;">
            <asp:Label ID="lbl_table" runat="server" Text="lbl_table"></asp:Label>
        </div>
    </div>
    <script>
        $(function () {
            $(".datepicker").datepicker({ dateFormat: 'yy-mm-dd' });
        });
        $(function () {

            var obj = {
                Init: function () {
                    $('.loader').hide();
                    obj.getallstates();
                    if ($('#ContentPlaceHolder1_RMACrop_Year').val() != '') {
                        $('#txtcropyear').val($('#ContentPlaceHolder1_RMACrop_Year').val());
                    }
                    if ($('#ContentPlaceHolder1_RMAPriceKey').val() != '') {
                        $('#txtRMA_Price_Key').val($('#ContentPlaceHolder1_RMAPriceKey').val());
                    }
                    if ($('#ContentPlaceHolder1_RMACrop_RMAState').val() != '') {
                        $('#ddlstate').val($('#ContentPlaceHolder1_RMACrop_RMAState').val());
                    }
                    obj.loadtable();
                    $("#btnSearch").on("click", function () {
                        $('.loader').show();
                        obj.loadtable();
                    });
                    $("#navbtnrefresh").click(function () {
                        $('.loader').show();
                        obj.loadtable();
                    });
                    $("#navbtndownload").click(function () {
                       //
                    });
                },
                loadtable: function () {
                    $('#ContentPlaceHolder1_lbl_table').text('');
                    var cropyear = $('#txtcropyear').val();
                    var Pricekey = $('#txtRMA_Price_Key').val();
                    var state = $('#ddlstate').val();
                    $.ajax({
                        type: "POST",
                        url: "RMA_Insurance_Offer.aspx/LoadTable",
                        data: JSON.stringify({ Crop_Year: cropyear, PriceKey: Pricekey, State: state }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            $('#ContentPlaceHolder1_lbl_table').html(res.d);
                            $('.loader').hide();
                        }
                    });
                },
                getallstates: function () {
                    $.ajax({
                        type: "POST",
                        url: "RMA_Insurance_Offer.aspx/Getallstates",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            $.each(res.d, function (data, value) {
                                $("#ddlstate").append($("<option></option>").val(data).html(value));
                            });
                        }

                    });
                }
            }
            obj.Init();
        });
        $('#navbtnsave').addClass('disabled');
        $('#navbtnadd').addClass('disabled');
        $('#navbtncolumns').addClass('disabled');
       
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" runat="Server">
</asp:Content>

