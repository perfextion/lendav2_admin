﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
public partial class admin_RMA_Insurance_Offer : System.Web.UI.Page
{

    static string dbKey = "gp_conn";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["RMACrop_Year"] != null)
        {
            RMACrop_Year.Value = Session["RMACrop_Year"].ToString();
        }
        if (Session["RMAPriceKey"] != null)
        {
            RMAPriceKey.Value = Session["RMAPriceKey"].ToString();
        }
        if (Session["RMAState"] != null)
        {
            RMAState.Value = Session["RMAState"].ToString();
        }
        //lbl_table.Text = LoadTable("", "", "");
    }
    [WebMethod]
    public static string LoadTable(string Crop_Year, string PriceKey, string State)
    {
        string stQry = "";
        string str_return = "";
        HttpContext.Current.Session["RMACrop_Year"] = Crop_Year;
        HttpContext.Current.Session["RMAPriceKey"] = PriceKey;
        HttpContext.Current.Session["RMAState"] = State;
        stQry = " SELECT [RMA_Price_ID],[Crop_Year],[RMA_Price_Key],[Ins_Plan_Code],[Ins_Plan_Type_Code]," +
            "[Crop_Code],[Crop_Type],[Irr_Prac],[Crop_Prac],[State_ID],[County_ID],[MPCI_Base_Price]," +
            "[MPCI_Harvest_Price],[Price_Established] FROM Ref_RMA_Insurance_Offer where 1=1  ";
        if (Crop_Year != "")
        {
            stQry += " and Crop_Year= '" + Crop_Year + "' ";
        }
        if (PriceKey != "")
        {
            stQry += " and RMA_Price_Key like '%" + PriceKey + "%' ";
        }
        if (State != "")
        {
            stQry += " and State_ID= '" + State + "' ";
        }
        DataTable dtLoanBudget = gen_db_utils.gp_sql_get_datatable(stQry, dbKey);
        if (dtLoanBudget != null && dtLoanBudget.Rows.Count > 0)
        {
            str_return += "<div> <table id='tblActiveUsers' class='table table-striped table-bordered' >  "
                              + "<thead> "
                              + "     <tr> "
                               + "      <th>RMA Price ID</th> "
                               + "      <th>Crop_Year</th> "
                               + "      <th>RMA_Price_Key</th> "
                               + "      <th>Insurence Details</th> "
                               + "      <th>Crop Details</th> "
                               + "      <th>State ID</th> "
                               + "      <th>County ID</th> "
                               + "      <th>MPCI_Base_Price</th> "
                               + "      <th>MPCI_Harvest_Price</th> "
                               + "      <th>Price_Established</th> "
                              + "    </tr> "
                              + "      </thead> "
                              + "        <tbody> ";

            foreach (DataRow dtrow in dtLoanBudget.Rows)
            {
                str_return += "<tr>"
                                + "<td> " + dtrow["RMA_Price_ID"].ToString() + " </td>"
                                + "<td> " + dtrow["Crop_Year"].ToString() + " </td>"
                                + "<td> " + dtrow["RMA_Price_Key"].ToString() + " </td>"
                                + "<td> Ins_Plan_Code : " + dtrow["Ins_Plan_Code"].ToString() + "" +
                                "<br/>Ins_Plan_Type_Code : " + dtrow["Ins_Plan_Type_Code"].ToString() + " </td>"
                                + "<td> Crop_Code : " + dtrow["Crop_Code"].ToString() + "" +
                                "<br/>Crop_Type : " + dtrow["Crop_Type"].ToString() +
                                "<br/>Irr_Prac : " + dtrow["Irr_Prac"].ToString() +
                                "<br/>Crop_Prac : " + dtrow["Crop_Prac"].ToString() + " </td>"
                                + "<td> " + dtrow["State_ID"].ToString() + " </td>"
                                + "<td> " + dtrow["County_ID"].ToString() + " </td>"
                                + "<td> " + dtrow["MPCI_Base_Price"].ToString() + " </td>"
                                + "<td> " + dtrow["MPCI_Harvest_Price"].ToString() + " </td>"
                                + "<td> " + dtrow["Price_Established"].ToString() + " </td>"
                               + "</tr>";
            }
            str_return += " </tbody> "
                                    + " </table> "
                                      + "</div></div></div >";
        }
        else
        {
            str_return = "    No data available...";
        }
        return str_return;
    }
    [WebMethod]
    public static Dictionary<string, string> Getallstates()
    {
        string sql_qry = "select State_ID,CONCAT( State_ID,'.',State_Name) as State_Name from Ref_State order by State_ID";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);

        return dt.AsEnumerable().ToDictionary<DataRow, string, string>(row => row[0].ToString(),
                                       row => row[1].ToString());
    }
}
