﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_RMA_Insurance_Offer_New : BasePage
{
    public admin_RMA_Insurance_Offer_New()
    {
        Table_Name = "Ref_RMA_Insurance_Offer";
    }

    [WebMethod]
    public static string GetTableInfo()
    {
        return Get_Table_Info();
    }

    [WebMethod]
    public static string GetRMAData()
    {
        string sql_qry = @"SELECT * ,0 as [ActionStatus] FROM [dbo].[Ref_RMA_Insurance_Offer]";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static void SaveRMAData(dynamic RMAData)
    {
        foreach (var item in RMAData)
        {
            string qry = "";
            int val = Convert.ToInt32(item["Actionstatus"]);

            if (val == 1)
            {
                qry = @"INSERT INTO [dbo].[Ref_RMA_Insurance_Offer]
                                   ([Crop_Year]
                                   ,[RMA_Price_Key]
                                   ,[Crop_Code]
                                   ,[Crop_Type]
                                   ,[State_ID]
                                   ,[Reference_Yield]
                                   ,[County_ID]
                                   ,[UOM]
                                   ,[MPCI_Base_Price]
                                   ,[MPCI_Harvest_Price]
                                   ,[Price_Established]
                                   ,[Status])
                             VALUES
                               (
                                    '" + item["Crop_Year"] + @"'" +
                                   ",'" + item["RMA_Price_Key"].Replace("'", "''") + @"'" +
                                   ",'" + item["Crop_Code"].Replace("'", "''") + @"'" +
                                   ",'" + item["Crop_Type"].Replace("'", "''") + @"'" +
                                   ",'" + item["State_ID"] + @"'" +
                                   ",'" + item["County_ID"] + @"'" +
                                   "," + Get_ID(item["Reference_Yield"].ToString()) + @"" +
                                   ",'" + item["UOM"] + @"'" +
                                   ",'" + item["MPCI_Base_Price"] + @"'" +
                                   ",'" + item["MPCI_Harvest_Price"] + @"'" +
                                   ",'" + item["Price_Established"] + @"'" +
                                   ",'" + item["Status"] + @"'
                                )  ";

                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 2)
            {
                qry = @"UPDATE [dbo].[Ref_RMA_Insurance_Offer]
                           SET  [Crop_Year] = '" + item["Crop_Year"] + @"' " +
                              ",[RMA_Price_Key] = '" + item["RMA_Price_Key"].Replace("'", "''") + @"' " +
                              ",[Crop_Code] = '" + item["Crop_Code"].Replace("'", "''") + @"' " +
                              ",[Crop_Type] = '" + item["Crop_Type"].Replace("'", "''") + @"' " +
                              ",[State_ID] = '" + item["State_ID"] + @"' " +
                              ",[County_ID] = '" + item["County_ID"] + @"' " +
                              ",[Reference_Yield] = " + Get_ID(item["Reference_Yield"].ToString()) + @" " +
                              ",[UOM] = '" + item["UOM"] + @"' " +
                              ",[MPCI_Base_Price] = '" + item["MPCI_Base_Price"] + @"' " +
                              ",[MPCI_Harvest_Price] = '" + item["MPCI_Harvest_Price"] + @"' " +
                              ",[Price_Established] = '" + item["Price_Established"] + @"' " +
                              ",[Status] = '" + item["Status"] + @"' " +
                         "WHERE RMA_Price_ID = '" + item["RMA_Price_ID"] + "' ";

                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 3)
            {
                qry = "DELETE FROM [Ref_RMA_Insurance_Offer]  where RMA_Price_ID = '" + item["RMA_Price_ID"] + "' ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
        }

        gen_db_utils.Update_Table_Audit_Trail("Ref_RMA_Insurance_Offer", userid, dbKey);
    }

    [WebMethod]
    public static void TruncateTable()
    {
        string qry = "TRUNCATE TABLE Ref_RMA_Insurance_Offer";
        gen_db_utils.gp_sql_execute(qry, dbKey);
        Update_Table_Audit();
    }

    [WebMethod]
    public static void UploadExcel(dynamic budgets)
    {
        string qry = string.Empty;

        foreach (var item in budgets)
        {
            if (!string.IsNullOrEmpty(item["Status"]))
            {
                var crop_year = TrimSQL(Get_Value(item, "Crop Year"));
                string crop_code = TrimSQL(Get_Value(item, "Crop Code"));
                var crop_type = Get_Value(item, "Crop Type");

                var state_id = Get_Value(item, "State Abbrev");
                try
                {
                    state_id = Convert.ToInt32(state_id);
                }
                catch
                {
                    state_id = 0;
                }

                var county_id = Get_Value(item, "County ID");
                try
                {
                    county_id = Convert.ToInt32(county_id);
                }
                catch
                {
                    county_id = 0;
                }

                var ref_yield = Get_Value(item, "Reference Yield");
                var UOM = Get_Value(item, "UOM");
                var base_price = Get_Value(item, "MPCI Base Price");
                var harvest_price = Get_Value(item, "MPCI Harvest Price");
                var est_pct = Get_Value(item, "Price Established Pct");
                var Status = Get_Value(item, "Status");
                var RMA_Price_Key = Get_Value(item, "RMA Price Key");

                qry += @"IF NOT EXISTS
                            (
                                SELECT 1 FROM [dbo].[Ref_RMA_Insurance_Offer] 
                                    WHERE RMA_Price_Key = '" + RMA_Price_Key + @"' 
                                    AND State_ID = '" + state_id + @"'
                                    AND County_ID = '" + county_id + @"'
                                    AND STATUS = 1
                            )   
                            BEGIN    
                                INSERT INTO [dbo].[Ref_RMA_Insurance_Offer]
                                   (
                                        [Crop_Year]
                                       ,[RMA_Price_Key]
                                       ,[Crop_Code]
                                       ,[Crop_Type]
                                       ,[State_ID]
                                       ,[County_ID]
                                       ,[Reference_Yield]
                                       ,[UOM]
                                       ,[MPCI_Base_Price]
                                       ,[MPCI_Harvest_Price]
                                       ,[Price_Established]
                                       ,[Status]
                                    )
                                VALUES
                                   (
                                        '" + crop_year + @"'
                                       ,'" + RMA_Price_Key + @"'
                                       ,'" + crop_code + @"'
                                       ,'" + crop_type + @"'
                                       ,'" + state_id + @"'
                                       ,'" + county_id + @"'
                                       ,'" + ref_yield + @"'
                                       ,'" + UOM + @"'
                                       ,'" + base_price + @"'
                                       ,'" + harvest_price + @"'
                                       ,'" + est_pct + @"'
                                       ,'" + Status + @"'
                                    )
                                END
                            ELSE
                                BEGIN
                                    UPDATE Ref_RMA_Insurance_Offer SET
                                        MPCI_Base_Price = '" + base_price + @"', 
                                        MPCI_Harvest_Price = '" + harvest_price + @"', 
                                        Reference_Yield = '" + ref_yield + @"', 
                                        UOM = '" + UOM + @"', 
                                        Price_Established = '" + est_pct + @"'
                                    WHERE RMA_Price_Key = '" + RMA_Price_Key + @"' 
                                    AND State_ID = '" + state_id + @"'
                                    AND County_ID = '" + county_id + @"'
                                    AND STATUS = 1
                                END";

                qry += Environment.NewLine;
            }
        }

        if (!string.IsNullOrEmpty(qry))
        {
            gen_db_utils.gp_sql_execute(qry, dbKey);
            Update_Table_Audit();
        }
    }
}