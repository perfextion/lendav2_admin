﻿<%@ Page Title="Association Default" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="RefAssocDefault.aspx.cs" Inherits="admin_RefAssocDefault" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style>
        .ui-jqgrid .ui-jqgrid-htable th div {
            height: auto;
            overflow: hidden;
            padding-right: 4px;
            padding-top: 2px;
            position: relative;
            vertical-align: text-top;
            white-space: normal !important;
        }

        /*Navbar Specific*/
        .navbar {
            margin-bottom: -10px;
           
        }

        #content {
            padding: 25px 14px !important;
        }

        .ref-assoc-default .col-md-3:first-child {
            width: 300px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">   
    <div class="ref-assoc-default reference-table">
        <div class="row" >
            <div class="col-md-12" style="padding-left: 35px;">
                <div class="form-inline col-md-3">
                    <label for="ddlofficename">Office:</label>
                    <select id="ddlofficename" class="form-control" style="width: 200px">
                        <option value="">Select</option>
                    </select>
                </div>
                <div class="form-inline col-md-3">
                    <label for="ddlAssocType">Assoc Type:</label>
                    <select class="form-control" id="ddlAssocType" style="width: 200px">
                        <option value="">Select</option>
                    </select>
                </div>  
            </div>          
        </div>
        <div class="row switch-controls">
            <div class="switch-control disabled" id="editControl">
                <label for="editBtn">Edit</label>
                <div class="toggle-btn small">            
                    <input type="checkbox" id="editBtn" class="cb-value" />
                    <span class="round-btn"></span>
                </div>
            </div>
            <div class="switch-control disabled" id="enableControl">
                <label for="enableBtn">Enable</label>
                <div class="toggle-btn small">            
                    <input type="checkbox" id="enableBtn" class="cb-value" />
                    <span class="round-btn"></span>
                </div>
            </div>
        </div>
        <div class="loader"></div>
        <div class="row main-content" style="position: relative">
            <div id="truncateTable" style="position: absolute; top: -25px; display: none">
                <a href="javascript:truncatetable()" class='glyphicon glyphicon-trash' style='color:red'></a>
            </div>
            <div>
                <table id="jqGrid"></table>
                <div id="jqGridPager"></div>
            </div>
        </div>
    </div>
    <br />
    <br />
    <br />
    <asp:HiddenField ID="Collateral" runat="server" />   
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" Runat="Server">
    <script type="text/javascript">  
        var DeleteRows = [];
        var timer;
        var lastSelection;
        var offices = {};
        var assocTypes = {};
        var canEdit = false;
        $(function () {
            var obj = {
                Init: function () {
                    $('.loader').show();

                    obj.getoffices();
                    obj.getAssocTypes();
                    obj.bindGrid();

                    $("#navbtnadd").click(function () {
                        obj.add();
                    });

                    $("#navbtnsave").click(function () {
                        $('.loader').show();
                        obj.save();
                    });

                    $("#navbtnrefresh").click(function () {
                        $('.loader').show();
                        obj.bindGrid();
                        $('#navbtnsave').removeClass('syncItems');
                        $('#navbtnsave>i').removeClass('syncItems');
                    });

                    $("#navbtndownload").click(function () {
                        $("#jqGrid").jqGrid("exportToExcel", {
                            includeLabels: true,
                            includeGroupHeader: true,
                            includeFooter: true,
                            fileName: "Association Default.xlsx",
                            maxlength: 40
                        })
                    });

                    $("#txtSearchBar").on("keyup", function () {
                        var self = this;
                        obj.search(self);
                    });

                    $("#ddlAssocType").on("change", function () {
                        $('.loader').show();
                        obj.bindGrid();
                    });

                    $("#ddlofficename").on("change", function () {
                        $('.loader').show();
                        obj.bindGrid();
                    });

                    $('#navbtncolumns').click(function () {
                        $("#jqGrid").columnChooser({});
                    });

                    $('#editBtn').change(function () {
                        canEdit = $(this).is(':checked');
                        if (!canEdit) {
                            if (lastSelection != null) {
                                var grid = $("#jqGrid");
                                grid.jqGrid('saveRow', lastSelection);
                                grid.jqGrid('restoreRow', lastSelection);
                                lastSelection = null;
                            }
                        }
                    });
                },
                getTableInfo: function () {
                    var Assoc_Type = $("#ddlAssocType").val();
                    var officename = $('#ddlofficename').val();
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "RefAssocDefault.aspx/GetTableInfo",
                        data: JSON.stringify({ Assoc_Type: Assoc_Type, Office_ID: officename }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            var data = JSON.parse(res.d);

                            var table_info = data.Table_Info[0];
                            $('#txtTableName').html(table_info.Table_Name);
                            $('#txtLastUpdated').html(table_info.Last_Updated);
                            $('#txtRecordsCount').html(table_info.Records_Count);

                            if (data.CanEdit) {
                                $('#editControl').removeClass('disabled');
                            } else {
                                $('#editControl').addClass('disabled');
                            }

                            $('#enableControl').addClass('disabled');
                            
                        },
                        failure: function (err) {
                            console.log(err);
                        }
                    });
                },
                getoffices: function () {
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "RefAssocDefault.aspx/GetAllofficenames",
                        data: JSON.stringify({  }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            offices = {};
                            $.each(res.d, function (data, value) {
                                offices[data] = data;
                                $("#ddlofficename").append($("<option></option>").val(data).html(value));
                            });
                        }
                    });
                },
                getAssocTypes: function () {
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "RefAssocDefault.aspx/GetAllAssocTypes",
                        data: JSON.stringify({  }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            assocTypes = {};
                            $.each(res.d, function (data, value) {
                                assocTypes[data] = value;
                                $("#ddlAssocType").append($("<option></option>").val(data).html(value));
                            });
                        }
                    });
                },
                bindGrid: function () {
                    var Assoc_Type = $("#ddlAssocType").val();
                    var officename = $('#ddlofficename').val();
                    $.ajax({
                        type: "POST",
                        url: "RefAssocDefault.aspx/GetAssociationDefault",
                        data: JSON.stringify({ Assoc_Type: Assoc_Type, Office_ID: officename }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var resData = JSON.parse(data.d);
                            $('#lblcount').html('Count of records: ' + resData.length);
                            $('.loader').show();
                            for (var i = 0; i < resData.length; i++) {
                                resData[i].rowid = i + 1;
                            }
                            obj.loadgrid(resData);
                            obj.getTableInfo();
                            $('.loader').hide();
                        }
                    });
                },
                loadgrid: function (data) {
                    $.jgrid.gridUnload("#jqGrid");
                    $("#jqGrid").jqGrid({
                        datatype: "local",
                        styleUI: 'Bootstrap',
                        data: data,
                        colModel: [
                            { label: 'rowid', name: 'rowid', width: 75, editable: false, key: true, hidden: true, hidedlg: true  },
                            { label: 'Assoc Default ID', name: 'Assoc_Def_ID', align: 'center', width: 120, editable: false, sorttype: 'integer' },
                            {
                                label: 'Assoc Type Code',
                                name: 'Assoc_Type_Code',
                                width: 120,
                                align: "center",
                                editable: true,
                                edittype: 'select',
                                editoptions: {
                                   value: assocTypes
                                },
                                formatter: 'select'
                            },
                            {
                                label: 'Assoc ID',
                                name: 'Assoc_ID',
                                width: 120,
                                align: "center",
                                classes: "center",
                                editable: true
                            },
                            {
                                label: 'Assoc Name',
                                name: 'Assoc_Name',
                                width: 150,
                                align: "left",
                                classes: "left",
                                editable: true,
                                hidden: true,
                                hidedlg: true
                            },                            
                            {
                                label: 'Office ID',
                                name: 'Office_ID',
                                width: 120,
                                align: "center",
                                editable: true,
                                hidden: false,
                                edittype: 'select',
                                editoptions: {
                                    value: offices
                                },
                                formatter: 'select',
                                sorttype: 'integer'
                            },
                            {
                                label: 'Contact',
                                name: 'Contact',
                                width: 120,
                                align: "left",
                                editable: false,
                                hidden: true,
                                hidedlg: true
                            },
                            {
                                label: 'Location',
                                name: 'Location',
                                width: 120,
                                align: "left",
                                editable: false,
                                hidden: true,
                                hidedlg: true
                            },
                            {
                                label: 'Email',
                                align: "left",
                                name: 'Email',
                                width: 180,
                                editable: false,
                                hidden: true,
                                hidedlg: true
                            },
                            {
                                label: 'Phone',
                                align: "left",
                                name: 'Phone',
                                width: 120,
                                editable: false,
                                hidden: true,
                                hidedlg: true
                            },
                            {
                                label: 'Preferred Contact Ind',
                                name: 'Preferred_Contact_Ind',
                                align: "left",
                                classes: "left",
                                width: 180,
                                editable: false,
                                hidden: true,
                                hidedlg: true,
                                edittype: 'select',
                                editoptions:
                                    {
                                        value: {
                                            1: 'Phone',
                                            2: 'Email',
                                            3: 'Text',
                                            4: 'Email & Text',
                                            5: 'Mail'
                                        }
                                    },
                                formatter: 'select'
                            },
                            {
                                label: 'Status',
                                name: 'Status',
                                align: "center",
                                classes: "center",
                                width: 90,
                                editable: true,
                                hidden: false,
                                editoptions: {
                                    maxlength: 1,
                                    dataEvents: [
                                        {
                                            type: 'keypress',
                                            fn: function (e) {
                                                return NumberOnly(e, this, false);
                                            }
                                        }
                                    ]
                                }
                            },
                            { label: 'Actionstatus', name: 'Actionstatus', width: 70, editable: false, hidden: true, hidedlg: true  },
                            { label: '', name: '', width: 60, align: 'center', formatter: obj.deleteLink, exportcol: false, hidedlg: true  },
                        ],
                        viewrecords: true,
                        loadonce: true,
                        restoreAfterSelect: false,
                        saveAfterSelect: true,
                        sortable: true,
                        onSelectRow: obj.edit,
                        onPaging: obj.changePage,
                        height: 'auto',
                        rowNum: 100,
                        pager: "#jqGridPager",
                        loadComplete: function () {
                            obj.showDeleteButton();
                        }
                    });                   
                },
                showDeleteButton: function () {
                    var width = $('#gbox_jqGrid').width();
                    $('#truncateTable').css('left', width + 15 + 'px');
                    $('#truncateTable').show();
                },
                add: function () {

                    var grid = $("#jqGrid");
                    var rowsperPage = parseInt(grid.getGridParam('rowNum'));
                    var gridlength = grid.jqGrid('getGridParam', 'data').length;
                    var curpage = parseInt($(".ui-pg-input").val());
                    var totPages = Math.ceil(gridlength / rowsperPage);


                    if (rowsperPage * totPages == gridlength) {
                        var id = $('.inline-edit-cell').parent().parent().prop('id');
                        grid.jqGrid('saveRow', id);
                        var row = obj.newrow();

                        var newRowId = row.rowid;
                        grid.jqGrid('addRowData', newRowId, row,"first");
                        grid.trigger('reloadGrid');
                        lastSelection = newRowId;
                        grid.jqGrid('saveRow', lastSelection);
                        grid.jqGrid('restoreRow', lastSelection);
                        $('.glyphicon-step-forward').trigger('click');
                    } else {
                        $('.glyphicon-step-forward').trigger('click');
                        var row = obj.newrow();

                        var newRowId = row.rowid;
                        grid.jqGrid('addRowData', newRowId, row, "first");
                    }


                    lastSelection = newRowId;
                    grid.jqGrid('saveRow', lastSelection);
                    grid.jqGrid('restoreRow', lastSelection);

                    var eid = $('.inline-edit-cell').parent().parent().prop('id')
                    grid.jqGrid('saveRow', eid);
                    grid.jqGrid('restoreRow', eid);
                    grid.jqGrid('editRow', newRowId);
                    $('#navbtnsave').addClass('syncItems');
                    $('#navbtnsave>i').addClass('syncItems');
                },
                edit: function (id) {

                    if (id && id !== lastSelection && canEdit) {
                        var grid = $("#jqGrid");
                        grid.jqGrid('saveRow', lastSelection);
                        grid.jqGrid('restoreRow', lastSelection);
                        var row = grid.jqGrid('getRowData', lastSelection);

                        if (!jQuery.isEmptyObject(row)) {
                            var dataobj = jQuery("#jqGrid").jqGrid('getGridParam', 'data');
                            index = dataobj.findIndex(x => x.rowid == row.rowid);
                            if (row.rowid > 0 && row.Actionstatus != 1) {
                                row.Actionstatus = 2;
                                grid.jqGrid('getGridParam', 'data')[index] = row;

                                $('#navbtnsave').addClass('syncItems');
                                $('#navbtnsave>i').addClass('syncItems');
                            }
                        }

                        grid.jqGrid('editRow', id);
                        lastSelection = id;
                    }
                },
                newrow: function () {
                    var newid = 0;

                    var grid = $("#jqGrid");
                    var length = jQuery("#jqGrid").jqGrid('getGridParam', 'records');
                    var dataobj = grid.jqGrid('getGridParam', 'data');
                    var id = [];
                    ids = dataobj.map(function (e) { return e.rowid });
                    if (ids.length > 0) {
                        newid = ids.reduce(function (a, b) { return Math.max(a, b); });
                    }

                    var row = {
                        Assoc_Def_ID: '',
                        Assoc_Name: '',
                        Assoc_Type_Code: '',
                        Office_ID: 1,
                        Contact: '',
                        Location: '',
                        Phone: '',
                        Email: '',
                        Preferred_Contact_Ind: 1,
                        Status: 1,
                        rowid: newid + 1
                    };
                    return row;
                },
                save: function () {

                    $('.loader').show();
                    var grid = $("#jqGrid");
                    var allrows = [];
                    var id = $('.inline-edit-cell').parent().parent().prop('id');
                    grid.jqGrid('saveRow', id);
                    var rowData = grid.jqGrid('getRowData', id);
                    if (rowData.rowid > 0) {
                        rowData.Actionstatus = 2;
                        grid.jqGrid('getGridParam', 'data')[id] = rowData;
                    }

                    for (var i = 0; i < DeleteRows.length; i++) {
                        DeleteRows[i].Actionstatus = 3
                        allrows.push(DeleteRows[i]);
                    }
                    var dataobj = grid.jqGrid('getGridParam', 'data');
                    var newRows = $.grep(dataobj, function (e) {
                        if (e != undefined) {
                            return e.Actionstatus == 1 || e.Actionstatus == 2
                        }
                    });
                    for (var i = 0; i < newRows.length; i++) {
                        allrows.push(newRows[i]);
                    }

                    $.ajax({
                        type: "POST",
                        url: "RefAssocDefault.aspx/SaveAssocDefaultList",
                        data: JSON.stringify({ AssocDefaultList: allrows }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            toastr.success("Saved Sucessful");
                            if (timer) { clearTimeout(timer); }
                            timer = setTimeout(function () {
                                location.reload();
                            }, 1000);
                            $('#navbtnsave').removeClass('syncItems');
                            $('#navbtnsave>i').removeClass('syncItems');
                        },
                        failure: function (response) {
                            $('.loader').hide();
                            var val = response.d;
                            toastr.warning(val);
                        }
                    });
                },
                search: function (self) {
                    if (timer) { clearTimeout(timer); }
                    timer = setTimeout(function () {
                        obj.changePage();
                        $("#jqGrid").jqGrid('filterInput', self.value);
                        var length = jQuery("#jqGrid").jqGrid('getGridParam', 'records');
                        $('#txtRecordsCount').html('Records Count: ' + length);
                    }, 0);
                },
                changePage: function () {
                    lastSelection = "";
                    var grid = $("#jqGrid");
                    var id = $('.inline-edit-cell').parent().parent().prop('id');
                    if (id != undefined) {
                        grid.jqGrid('saveRow', id);
                        grid.jqGrid('restoreRow', id);
                        var row = grid.jqGrid('getRowData', id);
                        if (!jQuery.isEmptyObject(row)) {
                            var dataobj = jQuery("#jqGrid").jqGrid('getGridParam', 'data');
                            index = dataobj.findIndex(x => x.rowid == row.rowid);
                            if (row.rowid > 0 && row.Actionstatus != 1) {
                                row.Actionstatus = 2;
                                grid.jqGrid('getGridParam', 'data')[index] = row;
                            }
                        }
                    }
                },
                deleteLink: function (cellValue, options, rowdata, action) {
                    return "<a href='javascript:deleteRecord(" + options.rowId + ")' class='glyphicon glyphicon-trash' style='color:red'></a>";
                }
            }
            obj.Init();
        });

        function deleteRecord(id) {
            if (canEdit) {
                var grid = $('#jqGrid');
                var result = confirm("Are you sure you Want to delete?");

                if (result == true) {
                    var ediId = $('.inline-edit-cell').parent().parent().prop('id');
                    grid.jqGrid('saveRow', ediId);
                    var rowData = grid.jqGrid('getRowData', ediId);
                    if (rowData.rowid > 0 && rowData.Actionstatus != 1) {
                        rowData.Actionstatus = 2;
                        grid.jqGrid('getGridParam', 'data')[ediId - 1] = rowData;
                    }

                    var row = grid.jqGrid('getRowData', id);
                    grid.jqGrid('delRowData', id);
                    if (row.Actionstatus != 1) {
                        DeleteRows.push(row);
                    }
                    var data = jQuery("#jqGrid").jqGrid('getGridParam', 'data');
                    for (var i = 0; i < data.length; i++) {
                        data[i].rowid = i + 1;
                    }
                    var curpage = parseInt($(".ui-pg-input").val());
                    jQuery('#jqGrid').jqGrid('clearGridData');
                    jQuery('#jqGrid').jqGrid('setGridParam', { data: data });
                    $("#jqGrid").trigger("reloadGrid", [{ page: curpage }]);
                    $("#jqGrid").trigger("reloadGrid", [{ page: curpage }]);
                    lastSelection = id;
                }
            }
        }

        function truncatetable() {
            if (canEdit) {
                var result = confirm('Are you sure you want to delete all record?');
                if (result) {
                    $('.loader').show();
                    $.ajax({
                        type: "POST",
                        url: "RefAssocDefault.aspx/TruncateTable",
                        data: JSON.stringify({}),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            toastr.success("Sucessful");
                            if (timer) { clearTimeout(timer); }
                            timer = setTimeout(function () {
                                location.reload();
                            }, 1000);
                        },
                        failure: function (response) {
                            $('.loader').hide();
                            var val = response.d;
                            toastr.warning(val);
                        }
                    });
                }
            }
        }
    </script>
</asp:Content>

