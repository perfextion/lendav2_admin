﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_RefAssocDefault : BasePage
{
    public admin_RefAssocDefault()
    {
        Table_Name = "Ref_Assoc_Default";
    }

    [WebMethod]
    public static string GetTableInfo(string Assoc_Type, string Office_ID)
    {
        Count_Query = @"DECLARE @Count int;
            Select @Count = Count(*)  From Ref_Assoc_Default";

        if (!string.IsNullOrEmpty(Assoc_Type))
        {
            Count_Query += " Where Assoc_Type_Code = '" + Assoc_Type + "'";

            if (!string.IsNullOrEmpty(Office_ID) && Convert.ToInt32(Office_ID) > 0)
            {
                Count_Query += " AND OFFICE_ID = '" + Office_ID + "'";
            }
        }
        else
        {
            if (!string.IsNullOrEmpty(Office_ID) && Convert.ToInt32(Office_ID) > 0)
            {
                Count_Query += " WHERE OFFICE_ID = '" + Office_ID + "'";
            }
        }

        return Get_Table_Info();
    }

    [WebMethod]
    public static string GetAssociationDefault(string Assoc_Type, string Office_ID)
    {
        string sql_qry = @"SELECT *, 0 as [Actionstatus] from [Ref_Assoc_Default]";

        if (!string.IsNullOrEmpty(Assoc_Type))
        {
            sql_qry += " Where Assoc_Type_Code = '" + Assoc_Type + "'";

            if (!string.IsNullOrEmpty(Office_ID) && Convert.ToInt32(Office_ID) > 0)
            {
                sql_qry += " AND OFFICE_ID = '" + Office_ID + "'";
            }
        }
        else
        {
            if (!string.IsNullOrEmpty(Office_ID) && Convert.ToInt32(Office_ID) > 0)
            {
                sql_qry += " WHERE OFFICE_ID = '" + Office_ID + "'";
            }
        }
        

        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static Dictionary<string, string> GetAllofficenames()
    {
        string sql_qry = "select Office_ID,CONCAT( Office_ID,'.',Office_Name) as Office_Name from Ref_Office ";        
        sql_qry += " order by Office_ID";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return dt.AsEnumerable().ToDictionary(row => row[0].ToString(), row => row[1].ToString());
    }

    [WebMethod]
    public static Dictionary<string, string> GetAllAssocTypes()
    {
        string sql_qry = @"select distinct assoc_type_code , assoc_type_name from Ref_association_type where [status]  = 1";        
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return dt.AsEnumerable().ToDictionary(row => row[0].ToString(), row => row[0].ToString());
    }

    [WebMethod]
    public static void SaveAssocDefaultList(dynamic AssocDefaultList)
    {
        foreach (var item in AssocDefaultList)
        {
            string qry = "";
            int val = Convert.ToInt32(item["Actionstatus"]);

            if (val == 1)
            {
                qry = @"INSERT INTO [dbo].[Ref_Assoc_Default]
                                   ([Assoc_Name]
                                   ,[Assoc_Type_Code]
                                   ,[Assoc_ID]
                                   ,[Office_ID]
                                   ,[Contact]
                                   ,[Location]
                                   ,[Phone]
                                   ,[Email]
                                   ,[Preferred_Contact_Ind]
                                   ,[Status])
                             VALUES
                               (
                                    '" + item["Assoc_Name"].Replace("'", "''") + @"'" +
                                   ",'" + item["Assoc_Type_Code"].Replace("'", "''") + @"'" +
                                   ",'" + item["Assoc_ID"] + @"'" +
                                   ",'" + item["Office_ID"] + @"'" +
                                   ",'" + item["Contact"].Replace("'", "''") + @"'" +
                                   ",'" + item["Location"].Replace("'", "''") + @"'" +
                                   ",'" + item["Phone"].Replace("'", "''") + @"'" +
                                   ",'" + item["Email"].Replace("'", "''") + @"'" +
                                   ",'" + item["Preferred_Contact_Ind"] + @"'" +
                                   ",'" + item["Status"] + @"'
                                )  ";

                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 2)
            {
                qry = @"UPDATE [dbo].[Ref_Assoc_Default]
                           SET  [Office_ID] = '" + item["Office_ID"] + @"' " +
                              ",[Preferred_Contact_Ind] = '" + item["Preferred_Contact_Ind"] + @"' " +
                              ",[Assoc_ID] = '" + item["Assoc_ID"] + @"' " +
                              ",[Assoc_Name] = '" + item["Assoc_Name"].Replace("'", "''") + @"' " +
                              ",[Assoc_Type_Code] = '" + item["Assoc_Type_Code"].Replace("'", "''") + @"' " +
                              ",[Contact] = '" + item["Contact"].Replace("'", "''") + @"' " +
                              ",[Location] = '" + item["Location"].Replace("'", "''") + @"' " +
                              ",[Phone] = '" + item["Phone"].Replace("'", "''") + @"' " +
                              ",[Email] = '" + item["Email"].Replace("'", "''") + @"' " +
                              ",[Status] = '" + item["Status"] + @"' " +
                         "WHERE Assoc_Def_ID = '" + item["Assoc_Def_ID"] + "' ";

                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 3)
            {
                qry = "DELETE FROM Ref_Assoc_Default  where Assoc_Def_ID = '" + item["Assoc_Def_ID"] + "' ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
        }

        if (AssocDefaultList != null)
        {
            Update_Table_Audit();
        }
    }

    [WebMethod]
    public static void TruncateTable()
    {
        string qry = "TRUNCATE TABLE Ref_Assoc_Default";
        gen_db_utils.gp_sql_execute(qry, dbKey);
        Update_Table_Audit();
    }
}