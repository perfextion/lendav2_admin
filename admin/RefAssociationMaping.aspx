﻿<%@ Page Title="" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="RefAssociationMaping.aspx.cs" Inherits="admin_RefAssociationMaping" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href='../css/fullcalendar.min.css' rel='stylesheet' />
    <link href='../css/fullcalendar.print.min.css' rel='stylesheet' media='print' />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" />
    <script src='../js/moment.min.js'></script>
    <script src='../js/fullcalendar/jquery.min.js'></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src='../js/fullcalendar/fullcalendar.min.js'></script>

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <div class="loader"></div>
    <div class="col-md-12 col-sm-4" style="margin-top: 15px">
        <div class="row">
            <div>
                <asp:HiddenField ID="fieldhdn" runat="server" />
                <asp:HiddenField ID="assocnamehdn" runat="server" />
                <asp:HiddenField ID="contacthdn" runat="server" />
                <asp:HiddenField ID="locationhdn" runat="server" />
                <asp:HiddenField ID="phonehdn" runat="server" />
                <asp:HiddenField ID="emailhdn" runat="server" />
            </div>
            <div class="col-md-3" style="margin-left: 18px">
                <div class="form-inline">
                    <div class="form-group">
                        <label for="lblofficenames">Assoc Type Code :</label>
                        <select id="ddlAssocTypeCode" class="form-control" style="width: 200px">
                            <option value="">Select</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-inline" style="margin-left: 20px; margin-top: 10px;">
                    <span><b>Find A Match For : </b></span>
                    <span id="lblname">Find the</span>

                    <%--  <span><b>; </b></span>
                    <span id="lblcontact">contact</span>--%>

                    <%--<span><b>; </b></span>
                    <span id="lblphone">Phone</span>

                    <span><b>; </b></span>
                    <span id="lbllocation">location</span>

                    <span><b>; </b></span>
                    <span id="lblemail">Find the</span>--%>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-inline">
                    <div class="btn btn-primary" id="btnadd">Add</div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-11" style="padding-left: 51px; padding-top: 8px;">
            <asp:Label ID="lbl_table" runat="server" Text="lbl_table"></asp:Label>
        </div>
    </div>


    <script>
        $(function () {
            $(".datepicker").datepicker({ dateFormat: 'yy-mm-dd' });
        });
        $(function () {

            var obj = {
                Init: function () {

                    $('.loader').hide();
                    obj.getAssociationCodes();
                    $('#ddlAssocTypeCode').change(function () {
                        var assocTypecode = $('#ddlAssocTypeCode').val();
                        $('#ContentPlaceHolder1_lbl_table').text('');
                        obj.changeAssocTypeCode(assocTypecode);
                    });
                    $('#btnadd').click(function () {
                        obj.buttonsave();
                    });
                    var fldname = $('#ContentPlaceHolder1_fieldhdn').val();
                    var assocname = $('#ContentPlaceHolder1_assocnamehdn').val();
                    var contact = $('#ContentPlaceHolder1_contacthdn').val();
                    var location = $('#ContentPlaceHolder1_locationhdn').val();
                    var phone = $('#ContentPlaceHolder1_phonehdn').val();
                    var email = $('#ContentPlaceHolder1_emailhdn').val();
                    $('#ddlAssocTypeCode').val(fldname).trigger('change');
                    $('#lblname').text(assocname + '; ' + contact + '; ' + location + '; ' + phone + '; ' + email + ';');
                },
                buttonsave: function () {
                    $('.loader').show();
                    var assocTypecode = $('#ddlAssocTypeCode').val();
                    $.ajax({
                        type: "POST",
                        url: "RefAssociationMaping.aspx/SaveIds",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            window.location.href = "RiskOtherQueue.aspx?Field_ID=" + assocTypecode;
                        },
                        failure: function (response) {
                        }
                    });

                },
                getAssociationCodes: function () {
                    $.ajax({
                        type: "POST",
                        url: "RefAssociationMaping.aspx/GetAssocTypeCode",
                        async: false,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            $.each(res.d, function (data, value) {
                                var htmlval;
                                var valid;
                                if ('AGY' == value) {
                                    valid = 'AGY'
                                    htmlval = "Association: Agency"
                                }
                                if ('AIP' == value) {
                                    valid = 'AIP'
                                    htmlval = "Association: AIP"
                                }
                                if ('BUY' == value) {
                                    valid = 'BUY'
                                    htmlval = "Association: Buyer"
                                }
                                if ('DIS' == value) {
                                    valid = 'DIS'
                                    htmlval = "Association: Distributor"
                                }
                                if (value == 'THR') {
                                    valid = 'THR';
                                    htmlval = "Association: Third Party";
                                }
                                if ('Expense' == value) {
                                    valid = 'Expense'
                                    htmlval = "Budget: Budget Expense"
                                }
                                if (valid != null && valid != '') {
                                    $("#ddlAssocTypeCode").append($("<option></option>").val(valid).html(htmlval));
                                }

                                //$.each(res.d, function (data, value) {
                                //    
                                //    $("#ddlAssocTypeCode").append($("<option></option>").val(value).html(value));
                            });
                        }

                    });
                },
                changeAssocTypeCode: function (assocTypecode) {
                    $('.loader').show();
                    $.ajax({
                        type: "POST",
                        url: "RefAssociationMaping.aspx/LoadTable",
                        data: JSON.stringify({ AssocTypeCode: assocTypecode }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            $('#ContentPlaceHolder1_lbl_table').html(res.d);
                            $('.loader').hide();
                        }
                    });
                },
                loadtable: function () {
                    var assocTypecode = $('#ddlAssocTypeCode').val();
                    $('#ContentPlaceHolder1_lbl_table').text('');
                    $.ajax({
                        type: "POST",
                        url: "RefAssociationMaping.aspx/LoadTable",
                        data: JSON.stringify({ AssocTypeCode: assocTypecode }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            $('.loader').show();
                            $('#ContentPlaceHolder1_lbl_table').html(res.d);
                            $('.loader').hide();
                        }
                    });

                }
            }
            obj.Init();
        });
        function usethis(id) {
            var assocTypecode = $('#ddlAssocTypeCode').val();
            $.ajax({
                type: "POST",
                url: "RefAssociationMaping.aspx/UpdateRiskother",
                data: JSON.stringify({ ref_asso_id: id }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (res) {
                    window.location.href = "RiskOtherQueue.aspx?Field_ID=" + assocTypecode;
                }
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" runat="Server">
</asp:Content>

