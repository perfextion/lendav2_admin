﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Text;
using System.Web.UI.WebControls;

public partial class admin_RefAssociationMaping : System.Web.UI.Page
{
    static string dbKey = "gp_conn";
    static Globalvar.Preferred_Contacts precont;
    static Globalvar.fieldids fieldids;
    static string Field_id, assocname;
    static string ROQ_ID, Ref_Column_ID;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ROQ_ID = Request.QueryString.GetValues("ROQ_ID") == null ? "0" : (Request.QueryString.GetValues("ROQ_ID")[0].ToString());

            string strQry = "SELECT ROQ_ID,Loan_ID,Loan_Seq_ID,Loan_Full_ID,Field_ID,Ref_Database_ID,Ref_Column_ID," +
           "User_ID,Data_Time_Entered,Date_Time_Action,Other_Text,Risk_Admin_ID,Suggested_Ref_ID," +
           "Response_Ind,Status FROM Risk_Other_Queue ";
            if (!string.IsNullOrEmpty(ROQ_ID))
            {
                strQry += " where ROQ_ID='" + ROQ_ID + "'";
            }
            DataTable dt = gen_db_utils.gp_sql_get_datatable(strQry, dbKey);
            if (dt != null && dt.Rows.Count > 0)
            {
                Field_id = dt.Rows[0][4].ToString();
                assocname = dt.Rows[0][10].ToString();
                assocnamehdn.Value = assocname.ToString();
                fieldhdn.Value = Field_id;
                lbl_table.Text = LoadTable(Field_id);
            }
            string sql_qry = "";
            sql_qry = "SELECT ROQ_ID,Field_ID,Ref_Column_ID,Assoc_ID,Contact,Location,Phone,Email " +
                "from Risk_Other_Queue ROQ inner join Loan_Association LA on ROQ.Ref_Column_ID=LA.Assoc_ID where ROQ_ID='" + ROQ_ID + "'";
            DataTable dtdetails = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
            if (dtdetails != null && dtdetails.Rows.Count > 0)
            {
                Ref_Column_ID = Convert.ToString(dtdetails.Rows[0][2]);
                contacthdn.Value = Convert.ToString(dtdetails.Rows[0][4]);
                locationhdn.Value = Convert.ToString(dtdetails.Rows[0][5]);
                phonehdn.Value = Convert.ToString(dtdetails.Rows[0][6]);
                emailhdn.Value = Convert.ToString(dtdetails.Rows[0][7]);
            }

        }
    }
    [WebMethod]
    public static string LoadTable(string AssocTypeCode)
    {
        string strQry = "";

        strQry = "SELECT Ref_Association_ID,ROQ_ID,Assoc_Type_Code,Assoc_Name,Assoc_Code,Contact,Location,Phone,Email,Referred_Type,Response, " +
                 "Preferred_Contact_Ind,Assoc_Status,IsOther,Date_Added,Status FROM dbo.Ref_Association where 1=1  ";

        if (!string.IsNullOrEmpty(AssocTypeCode))
            strQry += "  and Assoc_Type_Code='" + AssocTypeCode + "'";

        DataTable dt = gen_db_utils.gp_sql_get_datatable(strQry, dbKey);

        return GeneteateGrid(dt);
    }
    [WebMethod]
    public static string UpdateRiskother(string ref_asso_id)
    {
        string sql_qry = "";
        if (!string.IsNullOrEmpty(ROQ_ID) && !string.IsNullOrEmpty(ref_asso_id))
        {

            //sql_qry = "select * from Ref_Association where Ref_Association_ID= '" + ref_asso_id + "'";
            //DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
            //string EntryData = dt.Rows[0][3].ToString();
            sql_qry = "";
            //sql_qry = "update Risk_Other_Queue  set Suggested_Ref_ID='" + ref_asso_id + "',Other_Text='" + EntryData + "' where ROQ_ID='" + ROQ_ID + "'";
            sql_qry = "update Risk_Other_Queue  set Suggested_Ref_ID='" + ref_asso_id + "'  where ROQ_ID='" + ROQ_ID + "'";
            gen_db_utils.gp_sql_execute(sql_qry, dbKey);
            sql_qry = "";
            //sql_qry = "update Risk_Other_Queue  set Suggested_Ref_ID='" + ref_asso_id + "',Other_Text='" + EntryData + "' where Field_ID='" + Field_id + "' and Other_Text='" + assocname + "'";
            sql_qry = "update Risk_Other_Queue  set Suggested_Ref_ID='" + ref_asso_id + "'  where Field_ID='" + Field_id + "' and Other_Text='" + assocname + "'";
            gen_db_utils.gp_sql_execute(sql_qry, dbKey);
            sql_qry = "";
            sql_qry = "update Ref_Association set ROQ_ID='" + ROQ_ID + "' where Ref_Association_ID= '" + ref_asso_id + "'";
            gen_db_utils.gp_sql_execute(sql_qry, dbKey);
        }
        else
        {
            return "failure";
        }
        return "Sucess";
    }
    [WebMethod]
    public static string SaveIds()
    {
        string refasscode = "";
        string sql_qry = "";
        if (!string.IsNullOrEmpty(ROQ_ID))
        {
            string Contact, Location, Phone, Email;
            sql_qry = "SELECT ROQ_ID,Field_ID,Ref_Column_ID,Assoc_ID,Contact,Location,Phone,Email " +
                "from Risk_Other_Queue ROQ inner join Loan_Association LA on ROQ.Ref_Column_ID=LA.Assoc_ID where ROQ_ID='" + ROQ_ID + "'";
            DataTable dtdetails = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
            if (dtdetails != null && dtdetails.Rows.Count > 0)
            {
                Contact = Convert.ToString(dtdetails.Rows[0][4]);
                Location = Convert.ToString(dtdetails.Rows[0][5]);
                Phone = Convert.ToString(dtdetails.Rows[0][6]);
                Email = Convert.ToString(dtdetails.Rows[0][7]);
                sql_qry = "insert into Ref_Association (Assoc_Type_Code,Assoc_Name,ROQ_ID,Contact,Location,Phone,Email) " +
                    "values('" + Field_id + "','" + assocname + "','" + ROQ_ID + "','" + Contact + "','" + Location + "','" + Phone + "','" + Email + "')";
                gen_db_utils.gp_sql_execute(sql_qry, dbKey);
            }
            sql_qry = "";
            sql_qry = "select * from Ref_Association where Assoc_Type_Code='" + Field_id + "'and Assoc_Name='" + assocname + "' order by Ref_Association_ID desc";
            DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
            if (dt != null && dt.Rows.Count > 0)
            {
                refasscode = dt.Rows[0][0].ToString();
            }
        }
        sql_qry = "";
        if (!string.IsNullOrEmpty(ROQ_ID) && !string.IsNullOrEmpty(refasscode))
        {
            sql_qry = "update Risk_Other_Queue  set Suggested_Ref_ID='" + refasscode + "' where ROQ_ID='" + ROQ_ID + "'";
            gen_db_utils.gp_sql_execute(sql_qry, dbKey);
            sql_qry = "";
            sql_qry = "update Risk_Other_Queue  set Suggested_Ref_ID='" + refasscode + "' where Field_ID='" + Field_id + "' and Other_Text='" + assocname + "' ";
            gen_db_utils.gp_sql_execute(sql_qry, dbKey);
        }
        else
        {
            return "Failure";
        }
        //sql_qry = "";
        //sql_qry = "update Loan_Association set Ref_Assoc_ID='" + refasscode + "' where Assoc_ID='" + Ref_Column_ID + "'";
        //gen_db_utils.gp_sql_execute(sql_qry, dbKey);

        return "Sucess";
    }
    [WebMethod]
    public static List<string> GetAssocTypeCode()
    {
        string sql_qry = "select Distinct(Field_ID) from Risk_Other_Queue";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        List<string> emp = new List<string>();
        return (from DataRow row in dt.Rows
                select row["Field_ID"].ToString()
               ).ToList();
    }
    [WebMethod]
    public static List<string> GetAssocName()
    {
        string sql_qry = "select Distinct(Other_Text) from Risk_Other_Queue";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        List<string> emp = new List<string>();
        return (from DataRow row in dt.Rows
                select row["Other_Text"].ToString()
               ).ToList();
    }
    [WebMethod]
    public static string GeneteateGrid(DataTable dt)
    {
        string str_return = "";
        if (dt != null && dt.Rows.Count > 0)
        {
            str_return += "<div> <table id='tblActiveUsers' class='table table-striped table-bordered' >  "
                                 + "<thead> "
                                 + "     <tr> "
                                      + "       <th >Assoc ID</th> "
                                      + "       <th >ROQ_ID</th> "
                                      + "       <th >Assoc Type Details</th> "
                                      + "       <th >Details</th> "
                                      + "       <th >Referred_Type</th> "
                                      + "       <th >Assoc_Status</th> "
                                      + "       <th >IsOther</th> "
                                      + "       <th >Status</th> "
                                     //+ "      <th >IsDelete</th> "
                                     + "      <th >  </th> "
                                 + "    </tr> "
                                 + "      </thead> "
                                 + "        <tbody> ";

            foreach (DataRow dtr1 in dt.Rows)
            {
                Enum.TryParse<Globalvar.Preferred_Contacts>(dtr1["Preferred_Contact_Ind"].ToString(), out precont);
                Enum.TryParse<Globalvar.Preferred_Contacts>(dtr1["Preferred_Contact_Ind"].ToString(), out precont);
                string value = precont.ToString();

                string Fieldid = dtr1["Assoc_Type_Code"].ToString();
                if ("AGY" == dtr1["Assoc_Type_Code"].ToString())
                {
                    Fieldid = "Agency";
                }
                if ("BUY" == dtr1["Assoc_Type_Code"].ToString())
                {
                    Fieldid = "Buyer";
                }
                if ("DIS" == dtr1["Assoc_Type_Code"].ToString())
                {
                    Fieldid = "Distributor";
                }
                if ("Expense" == dtr1["Assoc_Type_Code"].ToString())
                {
                    Fieldid = "Budget Expense";
                }

                str_return += "<tr>"
                               + "<td>" + dtr1["Ref_Association_ID"].ToString() + "</td>"
                               + "<td>" + dtr1["ROQ_ID"].ToString() + "</td>"

                               //+ "<td>" + dtr1["Assoc_Type_Code"].ToString() + "<br>" + dtr1["Assoc_Name"].ToString() + "<br>" + dtr1["Assoc_Code"].ToString() + "</td>"
                               + "<td>" + Fieldid + "<br>" + dtr1["Assoc_Name"].ToString() + ";" + dtr1["Assoc_Code"].ToString() + "</td>"

                               + "<td>" + dtr1["Contact"].ToString() + "," + dtr1["Location"].ToString() +
                                 "<br>" + dtr1["Phone"].ToString() + "," + dtr1["Email"].ToString() + "</td>"

                               + "<td>" + dtr1["Referred_Type"].ToString() +
                                 "<br>" + dtr1["Response"].ToString() +
                                 //"<br>" + dtr1["Preferred_Contact_Ind"].ToString() + "</td>"
                                 "<br>" + value + "</td>"

                               + "<td>" + dtr1["Assoc_Status"].ToString() + "</td>"
                               + "<td>" + dtr1["IsOther"].ToString() + "<br>" + dtr1["Date_Added"].ToString() + "</td>"

                               + "<td>" + dtr1["Status"].ToString() + "</td>"
                             //+ "<td>" + dtr1["IsDelete"].ToString() + "</td>"

                             + "<td ><a href =' javascript:usethis(" + dtr1["Ref_Association_ID"] + ")' >Use This</a></td>"

                               + "</tr>";

            }

            str_return += "</tbody> "
                                    + " </table> "
                                      + "</div></div></div >";


        }
        else
        {
            str_return = "   ...No data available...";
        }
        return str_return;
    }
    [WebMethod]
    public static void UpdateAssocDetails(Dictionary<string, string> lst)
    {

        ReferenceAssociation objmanag = new ReferenceAssociation();
        string json = JsonConvert.SerializeObject(lst);
        ReferenceAssociation Ref = JsonConvert.DeserializeObject<ReferenceAssociation>(json);
        string qry = "";
        if (Ref.RefAssociationID != string.Empty)
        {
            qry = "update Ref_Association set ROQ_ID='" + Ref.ROQID + "'" +
            ",Assoc_Name='" + Ref.AssocName + "',Contact='" + Ref.Contact + "'," +
            "Location='" + Ref.Location + "',Phone='" + Ref.Phone + "',Email='" + Ref.Email + "',Referred_Type='" + Ref.ReferredType + "'," +
            "Response='" + Ref.Response + "',Preferred_Contact_Ind='" + Ref.PreferredContactInd + "',Assoc_Status='" + Ref.AssocStatus + "'," +
            "IsOther='" + Ref.IsOther + "',Status='" + Ref.Status + "'  where Ref_Association_ID = '" + Ref.RefAssociationID + "'";
        }
        else
        {
            qry = "insert into Ref_Association (ROQ_ID,Assoc_Type_Code,Assoc_Name,Contact,Location,Phone,Email,Referred_Type,Response," +
                "Preferred_Contact_Ind,Assoc_Status,IsOther,Status) values('" + Ref.ROQID + "','" + Ref.AssocTypeCode + "','" + Ref.AssocName + "'," +
                "'" + Ref.Contact + "','" + Ref.Location + "','" + Ref.Phone + "','" + Ref.Email + "','" + Ref.ReferredType + "','" + Ref.Response + "','" + Ref.PreferredContactInd + "'," +
                "'" + Ref.AssocStatus + "','" + Ref.IsOther + "','" + Ref.Status + "')";
        }
        gen_db_utils.gp_sql_execute(qry, dbKey);
    }

    public class ReferenceAssociation
    {
        public string RefAssociationID { set; get; }
        public string ROQID { set; get; }
        public string AssocTypeCode { set; get; }
        public string AssocName { set; get; }
        public string Assoc_Code { set; get; }
        public string Contact { set; get; }
        public string Location { set; get; }
        public string Phone { set; get; }
        public string Email { set; get; }
        public string ReferredType { set; get; }
        public string Response { set; get; }
        public string PreferredContactInd { set; get; }
        public string AssocStatus { set; get; }
        public string IsOther { set; get; }
        public string Date_Added { set; get; }
        public string Status { set; get; }

        public ReferenceAssociation()
        {
            //
            // TODO: Add constructor logic here
            //
        }

    }

}