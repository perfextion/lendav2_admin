﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_RefAssociationReferralType : BasePage
{
    public admin_RefAssociationReferralType()
    {
        Table_Name = "Assoc_Referral_Type";        
    }

    [WebMethod]
    public static string GetTableInfo()
    {
        Count_Query = @"DECLARE @Count int;
                        Select @Count = Count(*)  from [Ref_List_Item] where list_group_code = 'ASSOC_REFERRAL_TYPE'";
        return Get_Table_Info();
    }

    [WebMethod]
    public static string GetAssocTypes()
    {
        string sql_qry = @"SELECT *, 0 as [Actionstatus] from [Ref_List_Item] where list_group_code = 'ASSOC_REFERRAL_TYPE'";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static void SaveAssocType(dynamic ListItems)
    {
        foreach (var item in ListItems)
        {
            string qry = "";
            int val = Convert.ToInt32(item["Actionstatus"]);

            if (val == 1)
            {
                qry = @"INSERT INTO [dbo].[Ref_List_Item]
                               ([List_Item_Code]
                               ,[List_Item_Name]
                               ,[List_Group_Code]
                               ,[List_Item_Value]
                               ,[Status])
                         VALUES
                               (
                                    '" + item["List_Item_Code"].Replace("'", "''") + @"'" +
                                   ",'" + item["List_Item_Name"].Replace("'", "''") + @"'" +
                                   ",'ASSOC_REFERRAL_TYPE'" +
                                   ",'" + item["List_Item_Code"].Replace("'", "''") + @"'" +
                                   ",'" + item["Status"] + @"'
                                )  ";

                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 2)
            {
                qry = @"UPDATE [dbo].[Ref_List_Item]
                           SET  [List_Item_Code] = '" + item["List_Item_Code"].Replace("'", "''") + @"' " +
                              ",[List_Item_Name] = '" + item["List_Item_Name"].Replace("'", "''") + @"' " +
                              ",[List_Group_Code] = 'ASSOC_REFERRAL_TYPE' " +
                              ",[List_Item_Value] = '" + item["List_Item_Code"].Replace("'", "''") + @"' " +
                              ",[Status] = '" + item["Status"] + @"' " +
                         "WHERE List_Item_ID = '" + item["List_Item_ID"] + "' ";

                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 3)
            {
                qry = "DELETE FROM Ref_List_Item  where List_Item_ID = '" + item["List_Item_ID"] + "' ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
        }

        Update_Table_Audit();
    }
}