﻿using Newtonsoft.Json;
using System;
using System.Data;
using System.Web.Services;

public partial class admin_RefAssociationType : BasePage
{
    public admin_RefAssociationType()
    {
        Table_Name = "Ref_Association_Type";
    }

    [WebMethod]
    public static string GetTableInfo()
    {
        return Get_Table_Info();
    }

    [WebMethod]
    public static string GetAssocTypes()
    {
        string sql_qry = @"SELECT *, 0 as [Actionstatus] from Ref_Association_Type";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static void SaveAssocType(dynamic ListItems)
    {
        foreach (var item in ListItems)
        {
            string qry = "";
            int val = Convert.ToInt32(item["Actionstatus"]);

            if (val == 1)
            {
                qry = @"INSERT INTO [dbo].[Ref_Association_Type]
                               ([Assoc_Type_Code]
                               ,[Assoc_Type_Name]
                               ,[Contact_Req_Ind]
                               ,[Location_Req_Ind]
                               ,[Email_Req_Ind]
                               ,[Preffered_Contact_Req_Ind]
                               ,[Value_Req_Ind]
                               ,[Status])
                         VALUES
                               (
                                    '" + item["Assoc_Type_Code"].Replace("'", "''") + @"'" +
                                   ",'" + item["Assoc_Type_Name"].Replace("'", "''") + @"'" +
                                   "," + Get_ID(item["Contact_Req_Ind"].ToString()) + @"" +
                                   "," + Get_ID(item["Location_Req_Ind"].ToString()) + @"" +
                                   "," + Get_ID(item["Email_Req_Ind"].ToString()) + @"" +
                                   "," + Get_ID(item["Preffered_Contact_Req_Ind"].ToString()) + @"" +
                                   "," + Get_ID(item["Value_Req_Ind"].ToString()) + @"" +
                                   ",'" + item["Status"] + @"'
                                )  ";

                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 2)
            {
                qry = @"UPDATE [dbo].[Ref_Association_Type]
                           SET  [Assoc_Type_Code] = '" + item["Assoc_Type_Code"].Replace("'", "''") + @"' " +
                              ",[Assoc_Type_Name] = '" + item["Assoc_Type_Name"].Replace("'", "''") + @"' " +
                              ",[Contact_Req_Ind] = " + Get_ID(item["Contact_Req_Ind"].ToString()) + @" " +
                              ",[Location_Req_Ind] = " + Get_ID(item["Location_Req_Ind"].ToString()) + @" " +
                              ",[Email_Req_Ind] = " + Get_ID(item["Email_Req_Ind"].ToString()) + @" " +
                              ",[Preffered_Contact_Req_Ind] = " + Get_ID(item["Preffered_Contact_Req_Ind"].ToString()) + @" " +
                              ",[Value_Req_Ind] = " + Get_ID(item["Value_Req_Ind"].ToString()) + @" " +
                              ",[Status] = '" + item["Status"] + @"' " +
                         "WHERE Ref_Assoc_Type_ID = '" + item["Ref_Assoc_Type_ID"] + "' ";

                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 3)
            {
                qry = "DELETE FROM Ref_Association_Type  where Ref_Assoc_Type_ID = '" + item["Ref_Assoc_Type_ID"] + "' ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }            
        }

        if(ListItems != null)
        {
            Update_Table_Audit();
        }
    }
}