﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_RefBorrowerRating : BasePage
{
    public admin_RefBorrowerRating()
    {
        Table_Name = "Ref_Borrower_Rating";
    }

    [WebMethod]
    public static string GetTableInfo()
    {
        return Get_Table_Info();
    }

    [WebMethod]
    public static string LoadTable()
    {
        string sql_query = @"Select * from Ref_Borrower_Rating";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_query, dbKey);
        return JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static void SaveBorrowerRating(dynamic List)
    {
        string qry = string.Empty;

        foreach (var item in List)
        {
            int val = Convert.ToInt32(item["Actionstatus"]);

            if (val == 1)
            {
                qry += @"INSERT INTO [dbo].[Ref_Borrower_Rating]
                                   ([Borrower_Rating_Code]
                                   ,[Borrower_Rating_Name]
                                   ,[Value_1]
                                   ,[Value_2]
                                   ,[Value_3]
                                   ,[Value_4]
                                   ,[Value_5]
                                   ,[Constant_Ind]
                                   ,[Status])
                             VALUES
                               (
                                    '" + item["Borrower_Rating_Code"].Replace("'", "''") + @"'" +
                                   ",'" + item["Borrower_Rating_Name"].Replace("'", "''") + @"'" +
                                   ",'" + item["Value_1"].ToString().Replace("'", "''") + @"'" +
                                   ",'" + item["Value_2"].ToString().Replace("'", "''") + @"'" +
                                   ",'" + item["Value_3"].ToString().Replace("'", "''") + @"'" +
                                   ",'" + item["Value_4"].ToString().Replace("'", "''") + @"'" +
                                   ",'" + item["Value_5"].ToString().Replace("'", "''") + @"'" +
                                   ",'" + item["Constant_Ind"] + @"'" +
                                   ",'" + item["Status"] + @"'
                                )  ";

                qry += Environment.NewLine;
            }
            else if (val == 2)
            {
                qry += @"UPDATE [dbo].[Ref_Borrower_Rating]
                           SET  [Borrower_Rating_Code] = '" + item["Borrower_Rating_Code"].Replace("'", "''") + @"' " +
                              ",[Borrower_Rating_Name] = '" + item["Borrower_Rating_Name"].Replace("'", "''") + @"' " +
                              ",[Value_1] = '" + item["Value_1"].ToString().Replace("'", "''") + @"' " +
                              ",[Value_2] = '" + item["Value_2"].ToString().Replace("'", "''") + @"' " +
                              ",[Value_3] = '" + item["Value_3"].ToString().Replace("'", "''") + @"' " +
                              ",[Value_4] = '" + item["Value_4"].ToString().Replace("'", "''") + @"' " +
                              ",[Value_5] = '" + item["Value_5"].ToString().Replace("'", "''") + @"' " +
                              ",[Constant_Ind] = '" + item["Constant_Ind"] + @"' " +
                              ",[Status] = '" + item["Status"] + @"' " +
                         "WHERE Borrower_Rating_ID = '" + item["Borrower_Rating_ID"] + "' ";

                qry += Environment.NewLine;
            }
            else if (val == 3)
            {
                qry += "DELETE FROM Ref_Borrower_Rating  where Borrower_Rating_ID = '" + item["Borrower_Rating_ID"] + "' ";
                qry += Environment.NewLine;
            }
        }

        if(!string.IsNullOrEmpty(qry))
        {
            gen_db_utils.gp_sql_execute(qry, dbKey);
            gen_db_utils.Update_Table_Audit_Trail("Ref_Borrower_Rating", userid, dbKey);
        }
    }
}