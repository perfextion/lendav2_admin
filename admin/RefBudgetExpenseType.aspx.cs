﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_RefBudgetExpenseType : BasePage
{
    public admin_RefBudgetExpenseType()
    {
        Table_Name = "Ref_Budget_Expense_Type";
    }

    [WebMethod]
    public static string GetTableInfo()
    {
        return Get_Table_Info();
    }

    [WebMethod]
    public static string BindBudgetExceptionstypeDetails()
    {
        string sql_qry = @"SELECT 
                            Budget_Expense_Type_ID,
                            Budget_Expense_Name,
                            Standard_Ind,
                            Crop_Input_Ind,
                            Sort_order,
                            Translated_Expense_ID,
                            Validation_ID,
                            Exception_ID,
                            Documentation_Trigger_Ind,
                            Documentation_ID,
                            Financial_Budget_Code,
                            Status 
                        FROM Ref_Budget_Expense_Type  order by Sort_order";

        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static Dictionary<string, string> GetEnumsBudgetExpence()
    {
        DataTable dt = new DataTable();
        dt = EnumToDataTable(typeof(Globalvar.Budget_Expences));
        return dt.AsEnumerable().ToDictionary<DataRow, string, string>(row => row[1].ToString(),
                                     row => row[0].ToString());
    }

    public static DataTable EnumToDataTable(Type enumType)
    {
        DataTable table = new DataTable();
        table.Columns.Add("Key", typeof(string));
        table.Columns.Add("Value", Enum.GetUnderlyingType(enumType));
        foreach (string name in Enum.GetNames(enumType))
        {
            table.Rows.Add(name.Replace('_', ' '), Enum.Parse(enumType, name));
        }
        return table;
    }

    [WebMethod]
    public static void SaveBudgetExceptionstypeDetails(dynamic lst)
    {
        Log_utility log = new Log_utility();
        foreach (var item in lst)
        {
            string qry = "";
            int val = Convert.ToInt32(item["Actionstatus"]);
            var Validation_Exception_Ind = 0;

            if(!string.IsNullOrEmpty(item["Validation_ID"]) || !string.IsNullOrEmpty(item["Exception_ID"]))
            {
                Validation_Exception_Ind = 1;
            }

            if (val == 1)
            {
                qry = "INSERT INTO Ref_Budget_Expense_Type " +
                        "(" +
                            "Budget_Expense_Name," +
                            "Standard_Ind," +
                            "Crop_Input_Ind," +
                            "Translated_Expense_ID," +
                            "Validation_ID," +
                            "Exception_ID," +
                            "Sort_order," +
                            "Financial_Budget_Code," +
                            "Validation_Exception_Ind," +
                            "Status" +
                        ") " +
                  " VALUES" +
                      "(" +
                          "'" + ManageQuotes(item["Budget_Expense_Name"]) + "'," +
                          "'" + ManageQuotes(item["Standard_Ind"]) + "'," +
                          "'" + ManageQuotes(item["Crop_Input_Ind"]) + "'," +
                          "" + Get_ID(item["Translated_Expense_ID"]) + "," +
                          "" + Get_ID(item["Validation_ID"]) + "," +
                          "" + Get_ID(item["Exception_ID"]) + "," +
                          "'" + ManageQuotes(item["Sort_order"]) + "'," +
                          "'" + ManageQuotes(item["Financial_Budget_Code"]) + "'," +
                          "'" + Validation_Exception_Ind + "'," +
                          "'" + ManageQuotes(item["Status"]) + "'" +
                      ")";
                gen_db_utils.gp_sql_execute(qry, dbKey);
                log.Log_Section = "BudgetExceptionstype";
                log.Log_Message = "Added the New Budget Expences " + ManageQuotes(item["Budget_Expense_Name"]) + "";
                Log_utilitys.Log_utilit(log);
            }
            else if (val == 2)
            {
                qry = "UPDATE Ref_Budget_Expense_Type " +
                    "SET Budget_Expense_Name = '" + ManageQuotes(item["Budget_Expense_Name"]) + "'," +
                        "[Standard_Ind] = '" + ManageQuotes(item["Standard_Ind"]) + "'," +
                        "[Validation_ID] = " + Get_ID(item["Validation_ID"]) + "," +
                        "[Exception_ID] = " + Get_ID(item["Exception_ID"]) + "," +
                        "[Translated_Expense_ID] = " + Get_ID(item["Translated_Expense_ID"]) + "," +
                        "[Crop_Input_Ind] = '" + ManageQuotes(item["Crop_Input_Ind"]) + "'," +
                        "[Sort_order] = '" + ManageQuotes(item["Sort_order"]) + "'," +
                        "[Financial_Budget_Code] = '" + ManageQuotes(item["Financial_Budget_Code"]) + "'," +
                        "[Validation_Exception_Ind] = '" + Validation_Exception_Ind + "'," +
                        "[Status] = '" + ManageQuotes(item["Status"]) + "' " +
                    "where Budget_Expense_Type_ID=" + item["Budget_Expense_Type_ID"] + "  ";

                gen_db_utils.gp_sql_execute(qry, dbKey);
                log.Log_Section = "BudgetExceptionstype";
                log.Log_Message = "Updated the the Budget Expences Id is " + item["Budget_Expense_Type_ID"] + "";
                Log_utilitys.Log_utilit(log);
            }
            else if (val == 3)
            {
                qry = "DELETE FROM Ref_Budget_Expense_Type where Budget_Expense_Type_ID=" + item["Budget_Expense_Type_ID"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
                log.Log_Section = "BudgetExceptionstype";
                log.Log_Message = "Deleted the Budget Expences Id is " + item["Budget_Expense_Type_ID"] + "";
                Log_utilitys.Log_utilit(log);
            }
        }

        if (lst != null)
        {
            Update_Table_Audit();
        }
    }
    public static string ManageQuotes(string value)
    {
        if (!String.IsNullOrEmpty(value) && value.Contains("'"))
        {
            value = value.Replace("'", "''");
        }
        return value;
    }
}