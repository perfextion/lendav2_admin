﻿<%@ Page Title="" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="RefBudgetValues.aspx.cs" Inherits="admin_RefBudgetValues" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div style="width: 100%; margin: 1%; margin-left: 3%!important;">
        <div class="row">
            <div class="form-inline">
                <div style="float: left;">
                    <label for="txtcropfullkey">Crop Full Key :</label>
                    <input type="text" id="txtcropfullkey" class="form-control" />
                </div>
                <div style="float: left; margin-left: 23px;">
                    <label for="txtofficeid">Select Type :</label>
                    <select class="form-control" id="ddlselecttype" style="width: 200px;">
                        <option value="0">Select</option>
                        <option value="1">ARM</option>
                        <option value="2">Z_Region_ID</option>
                        <option value="3">Z_Office_ID</option>
                    </select>
                </div>
                <div id="divid" style="display: none; float: left; margin-left: 23px;">
                    <label for="txtofficeid" id="lblname">Z Office ID :</label>
                    <input type="text" id="txtofficeid" class="form-control" />
                </div>
                <div class="btn btn-primary" id="btninsert" style="margin-left: 23px;">
                    Insert Values
                </div>
            </div>
        </div>
        <div class="row" style="padding-top: 20px">
            <div class="form-inline">
                <div class="form-group">
                    <label for="txtFromcropfullkey">From Crop Full Key :</label>
                    <input type="text" id="txtFromcropfullkey" class="form-control" />
                </div>
                 <div class="form-group">
                    <label for="txtTocropfullkey">To Crop Full Key :</label>
                    <input type="text" id="txtTocropfullkey" class="form-control" />
                </div>
                <div class="btn btn-primary" id="btnCopy" style="margin-left: 23px;">
                    Copy Values
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="loader"></div>
        <div class="col-md-11" style="padding-left: 51px; color: green; font-weight: 700; padding-top: 8px;">
            <asp:Label ID="lbl_table" runat="server" Text=""> </asp:Label>
        </div>
    </div>
    <div class="row">
        <div class="col-md-11" style="padding-left: 51px; color: red; font-weight: 700; padding-top: 8px;">
            <asp:Label ID="lbl_table1" runat="server" Text=""></asp:Label>
        </div>
    </div>

    <script type="text/javascript">  
        var timer;

        $(function () {
            var obj = {
                Init: function () {
                    $('.loader').hide();
                    $('#btninsert').click(function () {
                        $('.loader').show();
                        var cropfullkey, regionid, officeid;
                        var ddlval = $('#ddlselecttype').val();
                        cropfullkey = $('#txtcropfullkey').val();
                        if (cropfullkey != "" && ddlval != "0") {
                            if (ddlval == 1) {
                                cropfullkey = $('#txtcropfullkey').val();
                                regionid = 0;
                                officeid = 0;
                                if (cropfullkey != "") {
                                    obj.DeleteRecords(cropfullkey, officeid);
                                }
                                else {
                                    alert("Please Enter cropfullkey");
                                }

                            }
                            else if (ddlval == 2) {
                                cropfullkey = $('#txtcropfullkey').val();
                                officeid = '$';
                                regionid = $('#txtofficeid').val();
                                if (cropfullkey != "") {
                                    var office = 0;
                                    obj.DeleteRecords(cropfullkey, office);
                                }
                                else {
                                    alert("Please Enter cropfullkey");
                                }
                            }
                            else if (ddlval == 3) {
                                cropfullkey = $('#txtcropfullkey').val();
                                officeid = $('#txtofficeid').val();
                                regionid = '$';
                                if (cropfullkey != "" && officeid != "") {
                                    obj.DeleteRecords(cropfullkey, officeid);
                                }
                                else {
                                    alert("Please Enter officeid");
                                }
                            }
                            else {
                                alert("Please Enter Values");
                            }

                            obj.loadtable(cropfullkey, regionid, officeid);
                        }
                        else {
                            alert("Please Enter Values");
                        }
                        $('.loader').hide();

                    })
                    $('#ddlselecttype').change(function () {
                        var ddlval = $('#ddlselecttype').val();
                        if (ddlval == 1) {
                            $("#divid").css({ "display": "none" });
                        }
                        else if (ddlval == 2) {
                            $('#txtofficeid').val('');
                            $("#divid").css({ "display": "" });
                            $('#lblname').text('Z Region ID');
                        }
                        else if (ddlval == 3) {
                            $('#txtofficeid').val('');
                            $("#divid").css({ "display": "" });
                            $('#lblname').text('Z Office ID');
                        }
                        else {
                            $("#divid").css({ "display": "none" });
                        }

                    });

                    $('#btnCopy').click(function () {
                        var from_Crop_key = $('#txtFromcropfullkey').val();
                        var to_Crop_key = $('#txtTocropfullkey').val();

                        if (!from_Crop_key || !to_Crop_key) {
                            alert('Please enter both From Crop key and to crop key');
                            return;
                        } else {
                            $('.loader').show();
                            $.ajax({
                                type: "POST",
                                async: false,
                                url: "RefBudgetValues.aspx/CopyBudgets",
                                data: JSON.stringify({ To_Crop_Key: to_Crop_key, From_Crop_Key: from_Crop_key }),
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                success: function (res) {
                                    toastr.success("Saved Sucessful");

                                    if (timer) { clearTimeout(timer); }
                                    timer = setTimeout(function () {
                                        window.location.href = window.location.origin + '/../admin/BudgetDefalutSummery.aspx';
                                    }, 2000);
                                },
                                failure: function (e) {
                                    toastr.error(e.d);
                                    $('.loader').hide();
                                }
                            });
                        }
                    });
                },

                loadtable: function (cropfullkey, regionid, officeid) {
                    $.ajax({
                        type: "POST",
                        url: "RefBudgetValues.aspx/LoadTable",
                        data: JSON.stringify({ cropfullkey: cropfullkey, regionid: regionid, officeid: officeid }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            $('#ContentPlaceHolder1_lbl_table').html(res.d);
                            $('.loader').hide();
                        }
                    });
                },
                DeleteRecords: function (cropfullkey, officeid) {
                    $.ajax({
                        type: "POST",
                        url: "RefBudgetValues.aspx/Deletevalues",
                        data: JSON.stringify({ cropfullkey: cropfullkey, officeid: officeid }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            $('#ContentPlaceHolder1_lbl_table1').html(res.d);
                            $('.loader').hide();
                        }
                    });
                },
            }
            obj.Init();

        });


    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" runat="Server">
</asp:Content>

