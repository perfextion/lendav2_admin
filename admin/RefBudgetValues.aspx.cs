﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_RefBudgetValues : System.Web.UI.Page
{
    static string dbKey = "gp_conn";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            //lbl_table.Text = LoadTable("CRN_0_IRR_IRR", "0");
        }
    }
    [WebMethod]
    public static string LoadTable(string cropfullkey, string regionid, string officeid)
    {
        string strQry = "";
        string sqlval = "";
        if (regionid == "0" && officeid == "0")
        {
            sqlval = " z_office_id = '" + officeid + "' and Z_Region_ID = '" + regionid + "'";
        }
        if (regionid == "$")
        {
            sqlval = " z_office_id = '" + officeid.Trim() + "'";
            regionid = "0";
        }
        if (officeid == "$")
        {
            sqlval = " Z_Region_ID = '" + regionid.Trim() + "' and  Z_Office_ID='0'";
            officeid = "0";
        }
        strQry = " select Budget_Expense_Name,Budget_Expense_Type_ID from Ref_Budget_Expense_Type where standard_ind = 1 and Budget_Expense_Type_ID not in " +
            "(select Budget_Expense_Type_ID from Ref_Budget_Default where crop_full_key = '" + cropfullkey.Trim() + "' and " + sqlval + ")";

        DataTable dt = gen_db_utils.gp_sql_get_datatable(strQry, dbKey);
        if (dt != null && dt.Rows.Count > 0)
        {
            return insertBudgets(dt, cropfullkey.Trim(), officeid.Trim(), regionid.Trim());
        }
        return "Creating the " + cropfullkey.Trim() + " is '0' Records Inserted";
        //return GeneteateGrid(dt);
    }
    [WebMethod]
    public static string Deletevalues(string cropfullkey, string officeid)
    {
        string strQry = "";

        strQry = "  select * from Ref_Budget_Default where crop_full_key = '" + cropfullkey.Trim() + "' and z_office_id =  '" + officeid.Trim() + "' " +
            "and Budget_Expense_Type_ID not in (select Budget_Expense_Type_ID from Ref_Budget_Expense_Type where standard_ind = 1)";

        DataTable dt = gen_db_utils.gp_sql_get_datatable(strQry, dbKey);
        if (dt != null && dt.Rows.Count > 0)
        {
            return DeleteBudgets(dt, cropfullkey.Trim());
        }
        return cropfullkey.Trim() + " is '0' Records Deleted";
    }

    [WebMethod]
    public static string insertBudgets(DataTable dt, string cropfullkey, string officeid, string regionid)
    {
        string qry = "";
        string officeqry = "";
        string returncount = "";
        int count;
        DataTable dtoffice = new DataTable();
        try
        {
            if (officeid != "0")
            {
                officeqry = "select Office_ID,Region_ID from Ref_Office where Office_ID ='" + officeid + "'";
                dtoffice = gen_db_utils.gp_sql_get_datatable(officeqry, dbKey);
                if (dtoffice != null && dtoffice.Rows.Count > 0)
                {
                    regionid = dtoffice.Rows[0][1].ToString();
                }
            }
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dtr in dt.Rows)
                {
                    qry += " insert into Ref_Budget_Default (Crop_Full_Key,Budget_Expense_Type_ID,Z_Region_ID,Z_Office_ID,ref_key)" +
                        "values('" + cropfullkey + "','" + dtr["Budget_Expense_Type_ID"].ToString() + "','" + regionid + "','" + officeid + "','" + regionid + "_" + officeid + "');  ";
                }
                count = gen_db_utils.count_gp_sql_execute(qry, dbKey);
                returncount = "Creating the " + cropfullkey + " is '" + count.ToString() + "' Records Inserted";
            }
            else
            {
                returncount = "Creating the " + cropfullkey + " is '0' Records Inserted";
            }

        }
        catch (Exception ex)
        {
            returncount = ex.Message;
        }
        return returncount;
    }
    [WebMethod]
    public static string DeleteBudgets(DataTable dt, string cropfullkey)
    {
        string qry = "";
        string returncount = "";
        int count;
        try
        {
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dtr in dt.Rows)
                {
                    qry += " delete Ref_Budget_Default where Budget_Default_ID='" + dtr["Budget_Default_ID"].ToString() + "';   ";
                }
                count = gen_db_utils.count_gp_sql_execute(qry, dbKey);
                returncount = cropfullkey + " is '" + count.ToString() + "' Records Deleted";
            }
            else
            {
                returncount = cropfullkey + " is '0' Records Deleted";
            }
        }
        catch (Exception ex)
        {
            returncount = ex.Message;
        }
        return returncount;
    }

    public static string ManageQuotes(string value)
    {
        if (!String.IsNullOrEmpty(value) && value.Contains("'"))
        {
            value = value.Replace("'", "''");
        }
        return value;
    }

    [WebMethod]
    public static string GeneteateGrid(DataTable dt)
    {
        string str_return = "";
        if (dt != null && dt.Rows.Count > 0)
        {
            str_return += "<div> <table id='tblLoanlist' class='table table-striped table-bordered' style='width:40%;' >  "
                                             + "<thead> "
                                 + "     <tr> "
                                      + "       <th style='text-align:center;' >Budget Expense Type ID</th> "
                                      + "       <th >Budget Expense Name</th> "
                                     + "       <th style='text-align:center;'>Action</th> "
                                 + "    </tr> "
                                 + "      </thead> "
                                 + "        <tbody> ";

            foreach (DataRow dtr1 in dt.Rows)
            {
                str_return += "<tr>"
                             + "<td style='text-align:center;' >" + dtr1["Budget_Expense_Type_ID"].ToString() + "</td>" +

                             "<td >" + dtr1["Budget_Expense_Name"].ToString() + "</td>"

                               + "<td></td>"

                              + "</tr>";

            }

            str_return += "</tbody> "
                                    + " </table> "
                                      + "</div></div></div >";
            ;

        }
        else
        {
            str_return = "    No data available...";
        }
        return str_return;
    }

    [WebMethod]
    public static void CopyBudgets(string To_Crop_Key, string From_Crop_Key)
    {
        string qry = @"IF NOT EXISTS(Select 1 From Ref_Budget_Default Where Crop_Full_Key = '" + To_Crop_Key + @"' And Z_Region_ID = 0 And Z_Office_ID = 0)
                        BEGIN
                             INSERT INTO [dbo].[Ref_Budget_Default]
                                   (
                                        [Crop_Practice_ID]
                                       ,[Crop_Full_Key]
                                       ,[Budget_Expense_Type_ID]
                                       ,[ARM_Budget]
                                       ,[Distributor_Budget]
                                       ,[Third_Party_Budget]
                                       ,[Z_Crop_Code]
                                       ,[Z_Practice_Type_Code]
                                       ,[Z_Region_ID]
                                       ,[Z_Office_ID]
                                       ,[Status]
                                       ,[ref_key]
                                    )
                                SELECT 
	                                VC.Crop_And_Practice_ID,
	                                '" + To_Crop_Key + @"',
	                                RB.[Budget_Expense_Type_ID],
	                                [ARM_Budget],
	                                0,
	                                0,
	                                VC.Crop_Code,
	                                VC.Irr_Prac_Code,
	                                0,
	                                0,
	                                1,
	                                '0_0'
                                FROM Ref_Budget_Default RB
                                JOIN Ref_Budget_Expense_Type RBET ON RB.Budget_Expense_Type_ID = RBET.Budget_Expense_Type_ID
                                LEFT JOIN V_Crop_Price_Details VC ON VC.Crop_Full_Key = '" + To_Crop_Key + @"'
                                Where RB.Crop_Full_Key = '" + From_Crop_Key + @"'
                                AND Ref_Key = '0_0'
                                AND (RBET.Standard_Ind = 1 OR RBET.Standard_Ind = 2)
                        END";

        qry += Environment.NewLine;

        gen_db_utils.base_sql_execute(qry, dbKey);
    }
}