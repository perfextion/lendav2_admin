﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Services;

public partial class admin_RefCollateralActionAdjDisc : BasePage
{
    public admin_RefCollateralActionAdjDisc()
    {
        Table_Name = "Ref_Collateral_Action_Disc_Adj";
    }

    [WebMethod]
    public static string GetTableInfo()
    {
        return Get_Table_Info();
    }

    [WebMethod]
    public static string GetCollateralActionAdjDisc()
    {
        string sql_qry = @"SELECT *, 0 as [Actionstatus] from [Ref_Collateral_Action_Disc_Adj]";

        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static void SaveCollateralActionAdjDisc(dynamic lst)
    {
        foreach (var item in lst)
        {

            string qry = string.Empty;
            int val = Convert.ToInt32(item["Actionstatus"]);

            var Validation_Exception_Ind = 0;

            if(!string.IsNullOrEmpty(item["Validation_ID"]) || !string.IsNullOrEmpty(item["Exception_ID"]))
            {
                Validation_Exception_Ind = 1;
            }

            if (val == 1)
            {
                qry = @"INSERT INTO [dbo].[Ref_Collateral_Action_Disc_Adj]
                               ([Collateral_Action_Type_Code]
                               ,[Collateral_Action_Name]
                               ,[Collateral_Mkt_Disc_Adj_Percent]
                               ,[Collateral_Ins_Disc_Adj_Percent]
                               ,[Validation_Exception_Ind]
                               ,[Validation_ID]
                               ,[Exception_ID]
                               ,[Status])
                         VALUES
                               (
                                    '" + item["Collateral_Action_Type_Code"].Replace("'", "''") + @"'" +
                                   ",'" + TrimSQL(item["Collateral_Action_Name"]) + @"'" +
                                   ",'" + item["Collateral_Mkt_Disc_Adj_Percent"] + @"'" +
                                   ",'" + item["Collateral_Ins_Disc_Adj_Percent"] + @"'" +
                                   ",'" + Validation_Exception_Ind + @"'" +
                                   "," + Get_ID(item["Validation_ID"]) + @"" +
                                   "," + Get_ID(item["Exception_ID"]) + @"" +
                                   ",'" + item["Status"] + @"'
                                )  ";

                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 2)
            {
                qry = @"UPDATE [dbo].[Ref_Collateral_Action_Disc_Adj]
                           SET  [Collateral_Mkt_Disc_Adj_Percent] = '" + item["Collateral_Mkt_Disc_Adj_Percent"] + @"' " +
                              ",[Collateral_Ins_Disc_Adj_Percent] = '" + item["Collateral_Ins_Disc_Adj_Percent"] + @"' " +
                              ",[Validation_Exception_Ind] = '" + Validation_Exception_Ind + @"' " +
                              ",[Validation_ID] = " + Get_ID(item["Validation_ID"]) + @" " +
                              ",[Exception_ID] = " + Get_ID(item["Exception_ID"]) + @" " +
                              ",[Collateral_Action_Name] = '" + TrimSQL(item["Collateral_Action_Name"]) + @"' " +
                              ",[Collateral_Action_Type_Code] = '" + TrimSQL(item["Collateral_Action_Type_Code"]) + @"' " +
                              ",[Status] = '" + item["Status"] + @"' " +
                         "WHERE Collateral_Action_Disc_Adj_ID = '" + item["Collateral_Action_Disc_Adj_ID"] + "' ";

                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 3)
            {
                qry = "DELETE FROM Ref_Collateral_Action_Disc_Adj  where Collateral_Action_Disc_Adj_ID = '" + item["Collateral_Action_Disc_Adj_ID"] + "' ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
        }

        Update_Table_Audit();
    }    
}