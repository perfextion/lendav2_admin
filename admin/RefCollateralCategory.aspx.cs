﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Services;

public partial class admin_RefCollateralCategory : BasePage
{
    public admin_RefCollateralCategory()
    {
        Table_Name = "Ref_Collateral_Category";
    }

    [WebMethod]
    public static string GetTableInfo()
    {
        return Get_Table_Info();
    }

    [WebMethod]
    public static string GetCollateralCategories()
    {
        string sql_qry = "SELECT *, 0 as [Actionstatus] from [Ref_Collateral_Category] Order By [Sort_Order]";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static void SaveCollateralCategories(dynamic lst)
    {
        foreach (var item in lst)
        {
            string qry = "";
            int val = Convert.ToInt32(item["Actionstatus"]);

            if (val == 1)
            {
                qry = @"INSERT INTO [dbo].[Ref_Collateral_Category]
                               ([Collateral_Category_Code]
                               ,[Collateral_Category_Name]
                               ,[Sort_Order]
                               ,[Prior_Lien_Max_Disc_Ind]
                               ,[Protect_Insured_Ind]
                               ,[Default_Insured_Ind]
                               ,[Status])
                         VALUES
                               (
                                    '" + item["Collateral_Category_Code"].Replace("'", "''") + @"'" +
                                   ",'" + item["Collateral_Category_Name"].Replace("'", "''") + @"'" +
                                   ",'" + item["Sort_Order"] + @"'" +
                                   ",'" + item["Prior_Lien_Max_Disc_Ind"] + @"'" +
                                   ",'" + item["Protect_Insured_Ind"] + @"'" +
                                   ",'" + item["Default_Insured_Ind"] + @"'" +
                                   ",'" + item["Status"] + @"'
                                )  ";

                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 2)
            {
                qry = @"UPDATE [dbo].[Ref_Collateral_Category]
                           SET [Collateral_Category_Name] = '" + item["Collateral_Category_Name"].Replace("'", "''") + @"' " +
                              ",[Collateral_Category_Code] = '" + item["Collateral_Category_Code"].Replace("'", "''") + @"' " +
                              ",[Sort_Order] = '" + item["Sort_Order"] + @"' " +
                              ",[Prior_Lien_Max_Disc_Ind] = '" + item["Prior_Lien_Max_Disc_Ind"] + @"' " +
                              ",[Protect_Insured_Ind] = '" + item["Protect_Insured_Ind"] + @"' " +
                              ",[Default_Insured_Ind] = '" + item["Default_Insured_Ind"] + @"' " +
                              ",[Status] = '" + item["Status"] + @"' " +
                         "WHERE Collateral_Category_ID = '" + item["Collateral_Category_ID"] + "' ";

                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 3)
            {
                qry = "DELETE FROM Ref_Collateral_Category  where Collateral_Category_ID = '" + item["Collateral_Category_ID"] + "' ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
        }

        Update_Table_Audit();
    }
}