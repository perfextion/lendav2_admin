﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Services;

public partial class admin_RefCollateralContractType : BasePage
{
    public admin_RefCollateralContractType()
    {
        Table_Name = "Ref_Contract_Type";
    }

    [WebMethod]
    public static string GetTableInfo()
    {
        return Get_Table_Info();
    }

    [WebMethod]
    public static string GetContractTypes()
    {
        string sql_qry = "SELECT *, 0 as [Actionstatus] from [Ref_Contract_Type]";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static void SaveCollateralContractTypes(dynamic lst)
    {
        foreach (var item in lst)
        {
            string qry = "";
            int val = Convert.ToInt32(item["Actionstatus"]);

            var Validation_Exception_Ind = 0;

            if (!string.IsNullOrEmpty(item["Validation_ID"]) || !string.IsNullOrEmpty(item["Exception_ID"]))
            {
                Validation_Exception_Ind = 1;
            }

            if (val == 1)
            {
                qry = @"INSERT INTO [dbo].[Ref_Contract_Type]
                               ([Contract_Type_Code]
                               ,[Contract_Type_Name]
                               ,[Disc_Adj]
                               ,[Validation_ID]
                               ,[Exception_ID]
                               ,[Validation_Exception_Ind]
                               ,[Status])
                         VALUES
                               (
                                    '" + item["Contract_Type_Code"].Replace("'", "''") + @"'" +
                                   ",'" + item["Contract_Type_Name"].Replace("'", "''") + @"'" +
                                   ",'" + item["Disc_Adj"] + @"'" +
                                   "," + Get_ID(item["Validation_ID"]) + @"" +
                                   "," + Get_ID(item["Exception_ID"]) + @"" +
                                   "," + Validation_Exception_Ind + @"" +
                                   ",'" + item["Status"] + @"'
                                )  ";

                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 2)
            {
                qry = @"UPDATE [dbo].[Ref_Contract_Type]
                           SET [Contract_Type_Name] = '" + item["Contract_Type_Name"].Replace("'", "''") + @"' " +
                              ",[Contract_Type_Code] = '" + item["Contract_Type_Code"].Replace("'", "''") + @"' " +
                              ",[Disc_Adj] = '" + item["Disc_Adj"] + @"' " +
                              ",[Validation_ID] = " + Get_ID(item["Validation_ID"]) + @" " +
                              ",[Exception_ID] = " + Get_ID(item["Exception_ID"]) + @" " +
                              ",[Validation_Exception_Ind] = " + Validation_Exception_Ind + @" " +
                              ",[Status] = '" + item["Status"] + @"' " +
                         "WHERE Contract_Type_ID = '" + item["Contract_Type_ID"] + "' ";

                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 3)
            {
                qry = "DELETE FROM Ref_Contract_Type  where Contract_Type_ID = '" + item["Contract_Type_ID"] + "' ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
        }

        Update_Table_Audit();
    }
}