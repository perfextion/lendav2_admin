﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Services;

public partial class admin_RefCollateralInsDisc : BasePage
{
    public admin_RefCollateralInsDisc()
    {
        Table_Name = "Ref_Collateral_Ins_Disc";
    }

    [WebMethod]
    public static string GetTableInfo()
    {
        return Get_Table_Info();
    }

    [WebMethod]
    public static Dictionary<string, string> GetCollateralCategories()
    {
        string sql_qry = "select Collateral_Category_Code, Collateral_Category_Name from Ref_Collateral_Category order by [sort_order]";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return dt.AsEnumerable().ToDictionary(row => row[0].ToString(), row => row[0].ToString());
    }

    [WebMethod]
    public static string GetCollateralInsDisc()
    {
        string sql_qry = @"SELECT RC.*, 0 as [Actionstatus], ISNULL(RCC.Collateral_Category_Name, RC.Collateral_Category_Code) as [Collateral_Category_Name] from [Ref_Collateral_Ins_Disc] RC
                            LEFT JOIN [Ref_Collateral_Category] RCC ON RC.Collateral_Category_Code = RCC.Collateral_Category_Code
                            ORDER BY RCC.[Sort_Order]";

        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static void SaveCollateralInsDisc(dynamic lst)
    {
        foreach (var item in lst)
        {
            string qry = "";
            int val = Convert.ToInt32(item["Actionstatus"]);

            var Validation_Exception_Ind = 0;

            if (!string.IsNullOrEmpty(item["Validation_ID"]) || !string.IsNullOrEmpty(item["Exception_ID"]))
            {
                Validation_Exception_Ind = 1;
            }

            if (val == 1)
            {
                qry = @"INSERT INTO [dbo].[Ref_Collateral_Ins_Disc]
                               ([Collateral_Category_Code]
                               ,[Disc_Percent]
                               ,[Validation_ID]
                               ,[Exception_ID]
                               ,[Validation_Exception_Ind]
                               ,[Ins_Plan_Code]
                               ,[Ins_Type_Code]
                               ,[Collateral_Code]
                               ,[Collateral_Type_Code]
                               ,[Irr_Practice_Type_Code]
                               ,[Crop_Practice_Type_Code]
                               ,[Status])
                         VALUES
                               (
                                    '" + item["Collateral_Category_Code"].Replace("'", "''") + @"'" +
                                   ",'" + item["Disc_Percent"] + @"'" +
                                   "," + Get_ID(item["Validation_ID"]) + @"" +
                                   "," + Get_ID(item["Exception_ID"]) + @"" +
                                   "," + Validation_Exception_Ind + @"" +
                                   ",'" + item["Ins_Plan_Code"].Replace("'", "''") + @"'" +
                                   ",'" + item["Ins_Type_Code"].Replace("'", "''") + @"'" +
                                   ",'" + item["Collateral_Code"].Replace("'", "''") + @"'" +
                                   ",'" + item["Collateral_Type_Code"].Replace("'", "''") + @"'" +
                                   ",'" + item["Irr_Practice_Type_Code"].Replace("'", "''") + @"'" +
                                   ",'" + item["Crop_Practice_Type_Code"].Replace("'", "''") + @"'" +
                                   ",'" + item["Status"] + @"'
                                )  ";

                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 2)
            {
                qry = @"UPDATE [dbo].[Ref_Collateral_Ins_Disc]
                           SET  [Disc_Percent] = '" + item["Disc_Percent"] + @"' " +
                              ",[Validation_ID] = " + Get_ID(item["Validation_ID"]) + @" " +
                              ",[Exception_ID] = " + Get_ID(item["Exception_ID"]) + @" " +
                              ",[Validation_Exception_Ind] = " + Validation_Exception_Ind + @" " +
                              ",[Ins_Plan_Code] = '" + item["Ins_Plan_Code"].Replace("'", "''") + @"' " +
                              ",[Ins_Type_Code] = '" + item["Ins_Type_Code"].Replace("'", "''") + @"' " +
                              ",[Collateral_Category_Code] = '" + item["Collateral_Category_Code"].Replace("'", "''") + @"' " +
                              ",[Collateral_Code] = '" + item["Collateral_Code"].Replace("'", "''") + @"' " +
                              ",[Collateral_Type_Code] = '" + item["Collateral_Type_Code"].Replace("'", "''") + @"' " +
                              ",[Irr_Practice_Type_Code] = '" + item["Irr_Practice_Type_Code"].Replace("'", "''") + @"' " +
                              ",[Crop_Practice_Type_Code] = '" + item["Crop_Practice_Type_Code"].Replace("'", "''") + @"' " +
                              ",[Status] = '" + item["Status"] + @"' " +
                         "WHERE Collateral_Ins_Disc_ID = '" + item["Collateral_Ins_Disc_ID"] + "' ";

                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 3)
            {
                qry = "DELETE FROM Ref_Collateral_Ins_Disc  where Collateral_Ins_Disc_ID = '" + item["Collateral_Ins_Disc_ID"] + "' ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
        }

        Update_Table_Audit();
    }
}