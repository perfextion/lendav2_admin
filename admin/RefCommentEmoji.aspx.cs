﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_RefCommentEmoji : BasePage
{
    public admin_RefCommentEmoji()
    {
        Table_Name = "Ref_Comment_Emoji";
    }

    [WebMethod]
    public static string GetTableInfo()
    {
        return Get_Table_Info();
    }

    [WebMethod]
    public static string GetCommentEmoji()
    {
        string sql_qry = @"Select * from Ref_Comment_Emoji";

        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static void SaveCommentTypes(dynamic lst)
    {
        foreach (var item in lst)
        {
            string qry = "";
            int val = Convert.ToInt32(item["Actionstatus"]);

            if (val == 1)
            {
                qry = @"INSERT INTO [dbo].[Ref_Comment_Emoji]
                                       ([Comment_Emoji_Text]
                                       ,[Emoji_Reference]
                                       ,[Vote_Ind]
                                       ,[Status])
                                 VALUES
                               (
                                    '" + item["Comment_Emoji_Text"].Replace("'", "''") + @"'" +
                                   ",'" + item["Emoji_Reference"].Replace("'", "''") + @"'" +
                                   ",'" + item["Vote_Ind"] + @"'" +
                                   ",'" + item["Status"] + @"'
                                )  ";

                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 2)
            {
                qry = @"UPDATE [dbo].[Ref_Comment_Emoji]
                           SET [Comment_Emoji_Text] = '" + item["Comment_Emoji_Text"].Replace("'", "''") + @"' " +
                              ",[Emoji_Reference] = '" + item["Emoji_Reference"].Replace("'", "''") + @"' " +
                              ",[Vote_Ind] = '" + item["Vote_Ind"] + @"' " +
                              ",[Status] = '" + item["Status"] + @"' " +
                         "WHERE Comment_Emoji_ID = '" + item["Comment_Emoji_ID"] + "' ";

                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 3)
            {
                qry = "DELETE FROM Ref_Comment_Emoji  where Comment_Emoji_ID = '" + item["Comment_Emoji_ID"] + "' ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
        }

        Update_Table_Audit();
    }
}