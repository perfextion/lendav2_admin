﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Services;
using System.Web.UI.WebControls;

public partial class admin_RefCommentType : BasePage
{
    public admin_RefCommentType()
    {
        Table_Name = "Ref_Comment_Type";
    }

    [WebMethod]
    public static string GetTableInfo()
    {
        return Get_Table_Info();
    }

    [WebMethod]
    public static Dictionary<string, string> GetPendingActionList()
    {
        string sql_qry = "Select PA_Code, PA_Name from Ref_Pending_Action";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return dt.AsEnumerable().ToDictionary(row => row[0].ToString(), row => row[1].ToString());
    }

    [WebMethod]
    public static string GetCommentTypes()
    {
        string sql_qry = @"SELECT *, 0 as [Actionstatus], RA.PA_Name from [Ref_Comment_Type] RC
                        LEFT JOIN Ref_Pending_Action RA On RC.Pending_Action_Code = RA.PA_Code ";

        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static void SaveCommentTypes(dynamic lst)
    {
        foreach (var item in lst)
        {
            string qry = "";
            int val = Convert.ToInt32(item["Actionstatus"]);

            if (val == 1)
            {
                qry = @"INSERT INTO [dbo].[Ref_Comment_Type]
                                   ([Comment_Type_Code]
                                   ,[Comment_Type_Name]
                                   ,[Notification_Ind]
                                   ,[Read_Required_Ind]
                                   ,[Pending_Action_Code]
                                   ,[Status])
                             VALUES
                               (
                                    '" + item["Comment_Type_Code"].Replace("'", "''") + @"'" +
                                   ",'" + item["Comment_Type_Name"].Replace("'", "''") + @"'" +
                                   ",'" + item["Notification_Ind"] + @"'" +
                                   ",'" + item["Read_Required_Ind"] + @"'" +
                                   ",'" + item["Pending_Action_Code"] + @"'" +
                                   ",'" + item["Status"] + @"'
                                )  ";

                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 2)
            {
                qry = @"UPDATE [dbo].[Ref_Comment_Type]
                           SET [Comment_Type_Code] = '" + item["Comment_Type_Code"].Replace("'", "''") + @"' " +
                              ",[Comment_Type_Name] = '" + item["Comment_Type_Name"].Replace("'", "''") + @"' " +
                              ",[Notification_Ind] = '" + item["Notification_Ind"] + @"' " +
                              ",[Read_Required_Ind] = '" + item["Read_Required_Ind"] + @"' " +
                              ",[Pending_Action_Code] = '" + item["Pending_Action_Code"] + @"' " +
                              ",[Status] = '" + item["Status"] + @"' " +
                         "WHERE Comment_Type_ID = '" + item["Comment_Type_ID"] + "' ";

                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 3)
            {
                qry = "DELETE FROM Ref_Comment_Type  where Comment_Type_ID = '" + item["Comment_Type_ID"] + "' ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
        }

        Update_Table_Audit();
    }
}