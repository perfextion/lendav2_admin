﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_RefCommitteeLevel : BasePage
{
    public admin_RefCommitteeLevel()
    {
        Table_Name = "Ref_Committee_Level";
    }

    [WebMethod]
    public static string GetTableInfo()
    {
        return Get_Table_Info();
    }

    [WebMethod]
    public static string GetLoancommiteelevel()
    {
        string sql_qry = " SELECT 0 as Actionstatus,CM_Level_ID, Level_Code, Level_Name, Approval, Status FROM Ref_Committee_Level ";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
    }


    [WebMethod]
    public static void Savecommitee(dynamic lst)
    {
        foreach (var item in lst)
        {
            string qry = "";
            int val = Convert.ToInt32(item["Actionstatus"]);
            if (val == 1)
            {
                qry = "INSERT INTO Ref_Committee_Level( Level_Code,Level_Name,Approval,Status)" +
                    " VALUES('" + item["Level_Code"].Replace("'", "''") + "','" + item["Level_Name"].Replace("'", "''") + "'," +
                    "'" + item["Approval"] + "','" + item["Status"].Replace("'", "''") + "')";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 2)
            {
                qry = "UPDATE Ref_Committee_Level SET Level_Code = '" + item["Level_Code"].Replace("'", "''") + "' ," +
                    "Level_Name = '" + item["Level_Name"].Replace("'", "''") + "' ," +
                    "Approval = '" + item["Approval"] + "' ,Status = '" + item["Status"].Replace("'", "''") + "' " +
                    "where CM_Level_ID=" + item["CM_Level_ID"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 3)
            {
                qry = "DELETE FROM Ref_Committee_Level  where CM_Level_ID=" + item["CM_Level_ID"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
        }

        Update_Table_Audit();
    }
}