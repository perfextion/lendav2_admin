﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
public partial class admin_RefCommitteeLevelDetail : BasePage
{
    public admin_RefCommitteeLevelDetail()
    {
        Table_Name = "Ref_Committee_Level_Detail";
    }

    [WebMethod]
    public static string GetTableInfo()
    {
        return Get_Table_Info();
    }

    [WebMethod]
    public static string GetLoancommiteelevel()
    {
        string sql_qry = @" SELECT [CM_Level_Detail_ID]
                                  ,[Level_Code]
                                  ,[Pool_Set_ID]
                                  ,LTRIM(RTRIM([Pool_Set_Name])) as [Pool_Set_Name]
                                  ,[Count]
                                  ,[Role_1]
                                  ,[Role_2]
                                  ,[Role_3]
                                  ,[Role_4]
                                  ,LTRIM(RTRIM([Voting])) as [Voting]
                                  ,[Status]
                                  ,0 as [ActionStatus]
                              FROM[dbo].[Ref_Committee_Level_Detail]";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
    }


    [WebMethod]
    public static void Savecommitee(dynamic lst)
    {
        foreach (var item in lst)
        {
            string qry = "";
            int val = Convert.ToInt32(item["Actionstatus"]);
            if (val == 1)
            {//CM_Level_Detail_ID,Level_Code,Role_Set_Code,Count,Role_1,Role_2,Role_3,Role_4,Voting,Status
                qry = "INSERT INTO Ref_Committee_Level_Detail(Level_Code,Count,Role_1,Role_2,Role_3,Role_4,Voting,Status)" +
                    " VALUES('" + item["Level_Code"].Replace("'", "''") + "'," +
                    "'" + item["Count"] + "','" + item["Role_1"].Replace("'", "''") + "'," +
                    "'" + item["Role_2"].Replace("'", "''") + "','" + item["Role_3"].Replace("'", "''") + "'," +
                    "'" + item["Role_4"].Replace("'", "''") + "','" + item["Voting"].Replace("'", "''") + "'," +
                    "'" + item["Status"].Replace("'", "''") + "')";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 2)
            {//CM_Level_Detail_ID,Level_Code,Role_Set_Code,Count,Role_1,Role_2,Role_3,Role_4,Voting,Status
                qry = "UPDATE Ref_Committee_Level_Detail SET Level_Code = '" + item["Level_Code"].Replace("'", "''") + "' ," +
                    "Count = '" + item["Count"] + "' ,Role_1 = '" + item["Role_1"].Replace("'", "''") + "'," +
                    "Role_2 = '" + item["Role_2"].Replace("'", "''") + "',Role_3 = '" + item["Role_3"].Replace("'", "''") + "'," +
                    "Role_4 = '" + item["Role_4"].Replace("'", "''") + "',Voting = '" + item["Voting"].Replace("'", "''") + "'," +
                    "Pool_Set_ID = '" + item["Pool_Set_ID"] + "',Pool_Set_Name = '" + item["Pool_Set_Name"].Replace("'", "''") + "'," +
                    "Status = '" + item["Status"].Replace("'", "''") + "'" +
                    "where CM_Level_Detail_ID=" + item["CM_Level_Detail_ID"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 3)
            {
                qry = "DELETE FROM Ref_Committee_Level_Detail  where CM_Level_Detail_ID=" + item["CM_Level_Detail_ID"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
        }

        Update_Table_Audit();
    }
}