﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class admin_Reference_RefDocumentDetail : BasePage
{
    public admin_Reference_RefDocumentDetail()
    {
        Table_Name = "Ref_Document_Detail";
    }

    [WebMethod]
    public static string GetTableInfo()
    {
        return Get_Table_Info();
    }

    [WebMethod]
    public static string GetRefDocumentDetail()
    {
        string sql_qry = "SELECT 0 as Actionstatus,Document_Detail_ID,Document_Type_ID,Field_ID,Critical_Ind,Status FROM Ref_Document_Detail ";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
    }
    [WebMethod]
    public static void SaveRefDocumentDetail(dynamic lst)
    {
        foreach (var item in lst)
        {
            string qry = "";
            int val = Convert.ToInt32(item["Actionstatus"]);
            if (val == 1)
            {
                qry = "INSERT INTO Ref_Document_Detail(Document_Type_ID,Field_ID,Critical_Ind,Status)" +
                    " VALUES('" + item["Document_Type_ID"].Replace("'", "''") + "','" + item["Field_ID"].Replace("'", "''") + "'" +
                    ",'" + item["Critical_Ind"].Replace("'", "''") + "','" + item["Status"].Replace("'", "''") + "')";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 2)
            {
                qry = "UPDATE Ref_Document_Detail SET Document_Type_ID = '" + item["Document_Type_ID"].Replace("'", "''") + "' ," +
                    "Field_ID = '" + item["Field_ID"].Replace("'", "''") + "',Critical_Ind = '" + item["Critical_Ind"].Replace("'", "''") + "'," +
                    "Status = '" + item["Status"].Replace("'", "''") + "' where Document_Detail_ID=" + item["Document_Detail_ID"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 3)
            {
                qry = "DELETE FROM Ref_Document_Detail  where Document_Detail_ID=" + item["Document_Detail_ID"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
        }

        Update_Table_Audit();
    }
}