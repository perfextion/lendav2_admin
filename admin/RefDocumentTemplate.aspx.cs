﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_Reference_RefDocumentTemplate : System.Web.UI.Page
{
    static string dbKey = "gp_conn";
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    [WebMethod]
    public static string GetRefDocumentTemplate()
    {
        string sql_qry = "SELECT 0 as Actionstatus,Document_Template_ID,State_ID,Docuument_Type_ID,Document_Template_Name,Document_Content,Status FROM Ref_Document_Template";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
    }
    [WebMethod]
    public static void SaveRefDocumentTemplate(dynamic lst)
    {
        foreach (var item in lst)
        {
            string qry = "";
            int val = Convert.ToInt32(item["Actionstatus"]);
            if (val == 1)
            {
                qry = "INSERT INTO Ref_Document_Template(State_ID,Docuument_Type_ID,Document_Template_Name,Document_Content,Status)" +
                    " VALUES('" + item["State_ID"].Replace("'", "''") + "','" + item["Docuument_Type_ID"].Replace("'", "''") + "','" + item["Document_Template_Name"].Replace("'", "''") + "','" + item["Document_Content"].Replace("'", "''") + "','" + item["Status"].Replace("'", "''") + "')";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 2)
            {
                qry = "UPDATE Ref_Document_Template SET State_ID = '" + item["State_ID"].Replace("'", "''") + "' " +
                    ",Docuument_Type_ID = '" + item["Docuument_Type_ID"].Replace("'", "''") + "'," +
                    "Document_Template_Name = '" + item["Document_Template_Name"].Replace("'", "''") + "'," +
                    "Document_Content = '" + item["Document_Content"].Replace("'", "''") + "',Status = '" + item["Status"].Replace("'", "''") + "' " +
                    "where Document_Template_ID=" + item["Document_Template_ID"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 3)
            {
                qry = "DELETE FROM Ref_Document_Template  where Document_Template_ID=" + item["Document_Template_ID"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }



        }
    }


}