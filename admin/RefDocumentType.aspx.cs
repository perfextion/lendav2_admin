﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_Reference_RefDocumentType : BasePage
{
    public admin_Reference_RefDocumentType()
    {
        Count_Query = string.Empty;
        Table_Name = "Ref_Document_Type";
    }

    [WebMethod]
    public static string GetTableInfo()
    {
        return Get_Table_Info();
    }

    [WebMethod]
    public static Dictionary<string, string> GetAllAssocTypes()
    {
        string sql_qry = @"select Assoc_Type_Code from Ref_Association_Type Where [Status] = 1 order by Assoc_Type_Code";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return dt.AsEnumerable().ToDictionary(row => row[0].ToString(), row => row[0].ToString());
    }

    [WebMethod]
    public static string GetRefDocumentType()
    {
        string sql_qry = "SELECT 0 as Actionstatus," +
                            "Document_Type_ID," +
                            "Document_Type_Name," +
                            "Document_Name_Override," +
                            "Document_Type_Level," +
                            "Sort_Order," +
                            "Standard_Condition_Ind," +
                            "State_Variation_Ind," +
                            "Replication_Association_Type_Code," +
                            "Table_Name," +
                            "Assoc_Replication_Code," +
                            "Status " +
                         "FROM Ref_Document_Type Order by Document_Type_Level, Sort_Order";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
    }


    [WebMethod]
    public static void SaveRefDocumentType(dynamic lst)
    {
        foreach (var item in lst)
        {
            string qry = "";
            int val = Convert.ToInt32(item["Actionstatus"]);
            if (val == 1)
            {
                qry = "INSERT INTO Ref_Document_Type" +
                    "(" +
                        "Document_Type_Name," +
                        "Document_Name_Override," +
                        "Document_Type_Level," +
                        "Sort_Order," +
                        "Standard_Condition_Ind," +
                        "State_Variation_Ind," +
                        "Replication_Association_Type_Code," +
                        "Assoc_Replication_Code," +
                        "Status" +
                    ")" +
                    " VALUES(" +
                        "'" + item["Document_Type_Name"].Replace("'", "''") + "'," +
                        "'" + item["Document_Name_Override"].Replace("'", "''") + "'," +
                        "'" + item["Document_Type_Level"].Replace("'", "''") + "'," +
                        "'" + item["Sort_Order"].Replace("'", "''") + "'," +
                        "'" + item["Standard_Condition_Ind"].Replace("'", "''") + "'," +
                        "'" + item["State_Variation_Ind"].Replace("'", "''") + "'," +
                        "'" + item["Replication_Association_Type_Code"].Replace("'", "''") + "'," +
                        "'" + item["Assoc_Replication_Code"].Replace("'", "''") + "'," +
                        "'" + item["Status"].Replace("'", "''") + "'" +
                    ")";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 2)
            {
                qry = "UPDATE Ref_Document_Type SET " +
                        "Document_Type_Name = '" + item["Document_Type_Name"].Replace("'", "''") + "' ," +
                        "Document_Name_Override = '" + item["Document_Name_Override"].Replace("'", "''") + "' ," +
                        "Document_Type_Level = '" + item["Document_Type_Level"].Replace("'", "''") + "'," +
                        "Sort_Order = '" + item["Sort_Order"].Replace("'", "''") + "'," +
                        "Standard_Condition_Ind = '" + item["Standard_Condition_Ind"].Replace("'", "''") + "'," +
                        "State_Variation_Ind = '" + item["State_Variation_Ind"].Replace("'", "''") + "'," +
                        "Replication_Association_Type_Code = '" + item["Replication_Association_Type_Code"].Replace("'", "''") + "'," +
                        "Assoc_Replication_Code = '" + item["Assoc_Replication_Code"].Replace("'", "''") + "'," +
                        "Status = '" + item["Status"].Replace("'", "''") + "' " +
                    "where Document_Type_ID=" + item["Document_Type_ID"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 3)
            {
                qry = "DELETE FROM Ref_Document_Type  where Document_Type_ID = '" + item["Document_Type_ID"] + "'  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
        }
        Update_Table_Audit();
    }
}