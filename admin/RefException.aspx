﻿<%@ Page Title="Risk Exception" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="RefException.aspx.cs" Inherits="admin_RefException" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .ui-jqgrid tr.jqgrow td {
            word-wrap: break-word; /* IE 5.5+ and CSS3 */
            white-space: pre-wrap; /* CSS3 */
            white-space: normal !important;
            height: auto;
            vertical-align: text-top;
            padding-top: 2px;
            padding-bottom: 3px;
        }

        .ui-jqgrid .ui-jqgrid-htable th {
            height: 65px;
        }

        .cvteste {
            padding-right: 14px !important;
        }

        #content {
            padding: 14px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="risk-exceptions reference-table">
        <div class="row switch-controls">
            <div class="switch-control disabled" id="editControl">
                <label for="editBtn">Edit</label>
                <div class="toggle-btn small">            
                    <input type="checkbox" id="editBtn" class="cb-value" />
                    <span class="round-btn"></span>
                </div>
            </div>
            <div class="switch-control disabled" id="enableControl">
                <label for="enableBtn">Enable</label>
                <div class="toggle-btn small">            
                    <input type="checkbox" id="enableBtn" class="cb-value" />
                    <span class="round-btn"></span>
                </div>
            </div>
        </div>
        <div class="loader"></div>
        <div class="row main-content">
            <div>
                <table id="jqGrid"></table>
                <div id="jqGridPager"></div>
            </div>
        </div>
    </div>
    <br />
    <br />
    <br />
    <br />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" runat="Server">
    <script type="text/javascript">
        var DeleteRows = [];
        var timer;
        var lastSelection;
        var canEdit = false;

        $(function () {
            var Chervonslist = {};
            var obj = {
                Init: function () {
                    obj.getChevronList();

                    $("#navbtnadd").click(function () {
                        obj.add();
                    });

                    $("#navbtnsave").click(function () {
                        $('.loader').show();
                        obj.save();
                    });

                    $("#txtSearchBar").on("keyup", function () {
                        var self = this;
                        obj.search(self);
                    });

                    $("#navbtnrefresh").click(function () {
                        $('.loader').show();
                        obj.bindGrid();
                    });

                    $("#navbtndownload").click(function () {
                        $("#jqGrid").jqGrid("exportToExcel", {
                            includeLabels: true,
                            includeGroupHeader: true,
                            includeFooter: true,
                            fileName: "Risk Exception.xlsx",
                            maxlength: 40 // maxlength for visible string data 
                        })
                    });

                    $('#navbtncolumns').click(function () {
                        $("#jqGrid").columnChooser({});
                    });

                    $('#editBtn').change(function () {
                        canEdit = $(this).is(':checked');
                        if (!canEdit) {
                            if (lastSelection != null) {
                                var grid = $("#jqGrid");
                                grid.jqGrid('saveRow', lastSelection);
                                grid.jqGrid('restoreRow', lastSelection);
                                lastSelection = null;
                            }
                        }
                    });
                },
                getChevronList: function () {
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "RefException.aspx/Getchervonid",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            Chervonslist = data.d;
                            obj.bindGrid();
                        }
                    });
                },
                getTableInfo: function () {
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "RefException.aspx/GetTableInfo",
                        data: JSON.stringify({ }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            var data = JSON.parse(res.d);

                            var table_info = data.Table_Info[0];
                            $('#txtTableName').html(table_info.Table_Name);
                            $('#txtLastUpdated').html(table_info.Last_Updated);
                            $('#txtRecordsCount').html(table_info.Records_Count);

                            if (data.CanEdit) {
                                $('#editControl').removeClass('disabled');
                            } else {
                                $('#editControl').addClass('disabled');
                            }

                            $('#enableControl').addClass('disabled');
                            
                        }
                    });
                },
                bindGrid: function () {
                    $.ajax({
                        type: "POST",
                        url: "RefException.aspx/GetRefException",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var resData = JSON.parse(data.d);
                            $('.loader').show();
                            for (var i = 0; i < resData.length; i++) {
                                resData[i].rowid = i + 1;
                            }
                            obj.loadgrid(resData);
                            obj.getTableInfo();
                            $('.loader').hide();
                        }
                    });
                },
                loadgrid: function (data) {
                    $.jgrid.gridUnload("#jqGrid");
                    $("#jqGrid").jqGrid({
                        datatype: "local",
                        styleUI: 'Bootstrap',
                        data: data,
                        colModel: [
                            { label: 'rowid', name: 'rowid', width: 75, editable: false, key: true, hidden: true, hidedlg: true },
                            { label: 'Exception ID', name: 'Exception_ID', align: 'center', width: 120, editable: false, sorttype: 'integer' },
                            { label: 'Exception ID Text', name: 'Exception_ID_Text', width: 300, editable: true },
                            {
                                label: 'Tab ID', name: 'Tab_Id', width: 90, align: 'center', editable: true, hidden: false,
                                editoptions: {
                                    maxlength: 3,
                                    dataEvents: [
                                        {
                                            type: 'keypress',
                                            fn: function (e) {
                                                return NumberOnly(e, this, false);
                                            }
                                        }
                                    ]
                                },
                                sorttype: 'integer'
                            },
                            { label: 'Level 1', name: 'Level_1_Hyper_Code', width: 300, editable: true, hidden: false },
                            { label: 'Level 2', name: 'Level_2_Hyper_Code', width: 300, editable: true, hidden: false },
                            {
                                label: 'Mitigation Text Req Ind', name: 'Mitigation_Text_Required_Ind', align: 'center', width: 90, editable: true,
                                editoptions: {
                                    maxlength: 1,
                                    dataEvents: [
                                        {
                                            type: 'keypress',
                                            fn: function (e) {
                                                return NumberOnly(e, this, false);
                                            }
                                        }
                                    ]
                                },
                                hidden: false
                            },
                            {
                                label: 'Support Req Ind', name: 'Support_Required_Ind', align: 'center', width: 90, editable: true,
                                editoptions: {
                                    maxlength: 1,
                                    dataEvents: [
                                        {
                                            type: 'keypress',
                                            fn: function (e) {
                                                return NumberOnly(e, this, false);
                                            }
                                        }
                                    ]
                                },
                                hidden: false
                            },
                            {
                                label: 'Support Role Type Code 1', name: 'Support_Role_Type_Code_1', width: 90, editable: true, editoptions: {
                                    maxlength: 50
                                },
                                hidden: false
                            },
                            {
                                label: 'Support Role Type Code 2', name: 'Support_Role_Type_Code_2', width: 90, editable: true, editoptions: {
                                    maxlength: 50
                                },
                                hidden: false
                            },
                            {
                                label: 'Support Role Type Code 3', name: 'Support_Role_Type_Code_3', width: 90, editable: true, editoptions: {
                                    maxlength: 50
                                },
                                hidden: false
                            },
                            {
                                label: 'Support Role Type Code 4', name: 'Support_Role_Type_Code_4', width: 90, editable: true, editoptions: {
                                    maxlength: 50
                                },
                                hidden: false
                            },
                            {
                                label: 'Pending Action Required Ind', name: 'Pending_Action_Required_Ind', align: 'center', width: 90, editable: true,
                                editoptions: {
                                    maxlength: 1,
                                    dataEvents: [
                                        {
                                            type: 'keypress',
                                            fn: function (e) {
                                                return NumberOnly(e, this, false);
                                            }
                                        }
                                    ]
                                },
                                hidden: false
                            },
                            {
                                label: 'Pending Action Code', name: 'Pending_Action_Code', width: 90, align: 'center', editable: true, editoptions: {
                                    maxlength: 50
                                },
                                hidden: false
                            },
                            {
                                label: 'Doc Req Ind', name: 'Document_Required_Ind', align: 'center', width: 90, editable: true,
                                editoptions: {
                                    maxlength: 1,
                                    dataEvents: [
                                        {
                                            type: 'keypress',
                                            fn: function (e) {
                                                return NumberOnly(e, this, false);
                                            }
                                        }
                                    ]
                                }
                            },
                            {
                                label: 'Doc ID', name: 'Document_Type_ID', width: 90, align: 'center', editable: true, hidden: false,
                                editoptions: {
                                    maxlength: 10,
                                    dataEvents: [
                                        {
                                            type: 'keypress',
                                            fn: function (e) {
                                                return NumberOnly(e, this, false);
                                            }
                                        }
                                    ]
                                },
                                sorttype: 'integer'
                            },
                            {
                                label: 'Sort Order', name: 'Sort_Order', sorttype: 'number', width: 60, align: 'center', editable: true, hidden: false,
                                editoptions: {
                                    maxlength: 3,
                                    dataEvents: [
                                        {
                                            type: 'keypress',
                                            fn: function (e) {
                                                return NumberOnly(e, this, false);
                                            }
                                        }
                                    ]
                                }
                            },
                            {
                                label: 'Status', name: 'Status', width: 90, editable: true, align: 'center', hidden: false,
                                editoptions: {
                                    maxlength: 1, dataEvents: [
                                        {
                                            type: 'keypress',
                                            fn: function (e) {
                                                return NumberOnly(e, this, false);
                                            }
                                        }
                                    ]
                                }
                            },
                            { label: 'Actionstatus', name: 'Actionstatus', width: 20, editable: false, hidden: true, hidedlg: true },
                            { label: '', name: '', width: 60, align: 'center', formatter: obj.deleteLink, hidedlg: true  },
                        ],
                        viewrecords: true,
                        loadonce: true,
                        restoreAfterSelect: false,
                        saveAfterSelect: true,
                        sortable: true,
                        onSelectRow: obj.edit,
                        onPaging: obj.changePage,
                        height: 'auto',
                        rowNum: 100,
                        pager: "#jqGridPager"
                    });
                },
                gettab: function (chervonid) {
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "RefException.aspx/Gettabidbychervon",
                        data: JSON.stringify({ ChervonId: chervonid }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var id = $('.inline-edit-cell').parent().parent().prop('id');
                            chervonlist = JSON.parse(data.d);
                            if (chervonid != '' && chervonid != null) {
                                if (id != undefined) {
                                    $("#jqGrid").jqGrid("setCell", id, "Tab_Id", chervonlist[0].Tab_Id);
                                }
                            }
                        }
                    });
                },
                add: function () {

                    var grid = $("#jqGrid");
                    var rowsperPage = parseInt(grid.getGridParam('rowNum'));
                    var gridlength = grid.jqGrid('getGridParam', 'data').length;
                    var curpage = parseInt($(".ui-pg-input").val());
                    var totPages = Math.ceil(gridlength / rowsperPage);


                    if (rowsperPage * totPages == gridlength) {
                        var id = $('.inline-edit-cell').parent().parent().prop('id');
                        grid.jqGrid('saveRow', id);
                        var row = obj.newrow();

                        var newRowId = row.rowid;
                        grid.jqGrid('addRowData', newRowId, row, "first");
                        grid.trigger('reloadGrid');
                        lastSelection = newRowId;
                        grid.jqGrid('saveRow', lastSelection);
                        grid.jqGrid('restoreRow', lastSelection);
                        $('.glyphicon-step-forward').trigger('click');
                    } else {
                        $('.glyphicon-step-forward').trigger('click');
                        var row = obj.newrow();

                        var newRowId = row.rowid;
                        grid.jqGrid('addRowData', newRowId, row, "first");
                    }


                    lastSelection = newRowId;
                    grid.jqGrid('saveRow', lastSelection);
                    grid.jqGrid('restoreRow', lastSelection);

                    var eid = $('.inline-edit-cell').parent().parent().prop('id')
                    grid.jqGrid('saveRow', eid);
                    grid.jqGrid('restoreRow', eid);
                    grid.jqGrid('editRow', newRowId);
                },
                edit: function (id) { 

                    if (id && id !== lastSelection && canEdit) {
                        var grid = $("#jqGrid");
                        grid.jqGrid('saveRow', lastSelection);
                        grid.jqGrid('restoreRow', lastSelection);
                        var row = grid.jqGrid('getRowData', lastSelection);

                        if (!jQuery.isEmptyObject(row)) {
                            var dataobj = jQuery("#jqGrid").jqGrid('getGridParam', 'data');
                            index = dataobj.findIndex(x => x.rowid == row.rowid);
                            if (row.rowid > 0 && row.Actionstatus != 1) {
                                row.Actionstatus = 2;
                                grid.jqGrid('getGridParam', 'data')[index] = row;
                            }
                        }

                        grid.jqGrid('editRow', id);
                        lastSelection = id;
                    }
                },
                newrow: function () {
                    var newid = 0;
                    var sortorder = 0;

                    var grid = $("#jqGrid");
                    var length = jQuery("#jqGrid").jqGrid('getGridParam', 'records');
                    var dataobj = grid.jqGrid('getGridParam', 'data');
                    var dataobj = grid.jqGrid('getGridParam', 'data');
                    var ids = [];

                    ids = dataobj.map(function (e) { return e.rowid });
                    if (ids.length > 0) {
                        newid = ids.reduce(function (a, b) { return Math.max(a, b); });
                        sortorder = dataobj.map(function (e) { return e.Sort_Order }).reduce(function (a, b) { return Math.max(a, b); })
                    }

                    var row = {
                        Exception_ID: 0,
                        Level_1_Hyper_Code: '',
                        Level_2_Hyper_Code: '',
                        Mitigation_Text_Required_Ind: 0,
                        Pending_Action_Required_Ind: 0,
                        Pending_Action_Code: '',
                        Support_Required_Ind: 0,
                        Support_Role_Type_Code_1: '',
                        Support_Role_Type_Code_2: '',
                        Support_Role_Type_Code_3: '',
                        Support_Role_Type_Code_4: '',
                        Document_Required_Ind: '',
                        Document_Type_ID: 0,
                        Actionstatus: 1,
                        Status: 1,
                        Sort_Order: sortorder + 1,
                        rowid: newid + 1
                    };
                    return row;
                },
                save: function () {

                    $('.loader').show();
                    var grid = $("#jqGrid");
                    var allrows = [];
                    var id = $('.inline-edit-cell').parent().parent().prop('id');
                    grid.jqGrid('saveRow', id);
                    var rowData = grid.jqGrid('getRowData', id);
                    if (rowData.rowid > 0) {
                        rowData.Actionstatus = 2;
                        grid.jqGrid('getGridParam', 'data')[id] = rowData;
                    }

                    for (var i = 0; i < DeleteRows.length; i++) {
                        DeleteRows[i].Actionstatus = 3
                        allrows.push(DeleteRows[i]);
                    }
                    var dataobj = grid.jqGrid('getGridParam', 'data');
                    var newRows = $.grep(dataobj, function (e) {
                        if (e != undefined) {
                            return e.Actionstatus == 1 || e.Actionstatus == 2
                        }
                    });
                    for (var i = 0; i < newRows.length; i++) {
                        allrows.push(newRows[i]);
                    }

                    $.ajax({
                        type: "POST",
                        url: "RefException.aspx/SaveRefException",
                        data: JSON.stringify({ lst: allrows }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            toastr.success("Updated Sucessful");
                            if (timer) { clearTimeout(timer); }
                            timer = setTimeout(function () {
                                location.reload();
                            }, 2000);

                        },
                        failure: function (response) {
                            var val = console.log(response.d);
                            toastr.warning(val);
                        }

                    });
                },
                search: function (self) {
                    if (timer) { clearTimeout(timer); }
                    timer = setTimeout(function () {
                        obj.changePage();
                        $("#jqGrid").jqGrid('filterInput', self.value);
                        var length = jQuery("#jqGrid").jqGrid('getGridParam', 'records');
                        $('#txtRecordsCount').html('Records Count: ' + length);
                    }, 0);
                },
                changePage: function () {

                    lastSelection = "";
                    var grid = $("#jqGrid");
                    var id = $('.inline-edit-cell').parent().parent().prop('id');
                    if (id != undefined) {
                        grid.jqGrid('saveRow', id);
                        grid.jqGrid('restoreRow', id);
                        var row = grid.jqGrid('getRowData', id);
                        if (!jQuery.isEmptyObject(row)) {
                            var dataobj = jQuery("#jqGrid").jqGrid('getGridParam', 'data');
                            index = dataobj.findIndex(x => x.rowid == row.rowid);
                            if (row.rowid > 0 && row.Actionstatus != 1) {
                                row.Actionstatus = 2;
                                grid.jqGrid('getGridParam', 'data')[index] = row;
                            }
                        }
                    }
                },
                deleteLink: function (cellValue, options, rowdata, action) {
                    return "<a href='javascript:deleteRecord(" + options.rowId + ")' class='glyphicon glyphicon-trash' style='color:red'></a>";
                }
            }
            obj.Init();

        });

        function deleteRecord(id) {

            if (canEdit) {
                var grid = $('#jqGrid');
                var result = confirm("Are you sure you Want to delete?");

                if (result == true) {
                    var ediId = $('.inline-edit-cell').parent().parent().prop('id');
                    grid.jqGrid('saveRow', ediId);
                    var rowData = grid.jqGrid('getRowData', ediId);
                    if (rowData.rowid > 0 && rowData.Actionstatus != 1) {
                        rowData.Actionstatus = 2;
                        grid.jqGrid('getGridParam', 'data')[ediId - 1] = rowData;
                    }

                    var row = grid.jqGrid('getRowData', id);
                    grid.jqGrid('delRowData', id);
                    if (row.Actionstatus != 1) {
                        DeleteRows.push(row);
                    }
                    var data = jQuery("#jqGrid").jqGrid('getGridParam', 'data');
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].Exception_ID == 0) {
                            data[i].rowid = i + 1;
                            data[i].id = i + 1;
                        } else
                            data[i].rowid = i + 1;
                    }
                    var curpage = parseInt($(".ui-pg-input").val());
                    jQuery('#jqGrid').jqGrid('clearGridData');
                    jQuery('#jqGrid').jqGrid('setGridParam', { data: data });
                    $("#jqGrid").trigger("reloadGrid", [{ page: curpage }]);
                    $("#jqGrid").trigger("reloadGrid", [{ page: curpage }]);
                    lastSelection = id;
                }
            }
        }
        function unique(list) {
            var result = [];
            $.each(list, function (i, e) {
                if ($.inArray(e, result) == -1) result.push(e);
            });
            return result;
        }
    </script>
</asp:Content>

