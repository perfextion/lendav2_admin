﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_RefException : BasePage
{
    public admin_RefException()
    {
        Table_Name = "Ref_Exception";
    }

    [WebMethod]
    public static string GetTableInfo()
    {
        return Get_Table_Info();
    }

    [WebMethod]
    public static string GetRefException()
    {
        string sql_qry = @"SELECT 
	                        0 as Actionstatus,
	                        Exception_ID,
	                        Exception_ID_Text,
	                        Chevron_ID,
	                        Tab_Id,
	                        Sort_Order,
	                        Level_1_Hyper_Code,
	                        Level_2_Hyper_Code,
	                        Mitigation_Text_Required_Ind,
	                        Support_Required_Ind,
	                        Support_Role_Type_Code_1,
	                        Support_Role_Type_Code_2,
	                        Support_Role_Type_Code_3,
	                        Support_Role_Type_Code_4,
	                        Pending_Action_Required_Ind,
	                        Pending_Action_Code,
	                        Document_Required_Ind,
	                        Document_Type_ID,
	                        [Status]
                        FROM Ref_Exception";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static Dictionary<string, string> Getchervonid()
    {
        string sql_qry = "select CAST(Chevron_id as Varchar) as Chevron_id, chevron_Name from [dbo].[Ref_Chevron]";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return dt.AsEnumerable().ToDictionary(a => a.Field<string>("Chevron_id"), a => a.Field<string>("chevron_Name"));
    }

    [WebMethod]
    public static string Gettabidbychervon(string ChervonId)
    {
        string sql_qry = "select Chevron_Name,Tab_Id from Ref_Chevron where Chevron_ID='" + ChervonId + "'";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
        //return DataTableToJsonstrings(dt);
    }
    
    [WebMethod]
    public static void SaveRefException(dynamic lst)
    {
        foreach (var item in lst)
        {
            string qry = "";
            int val = Convert.ToInt32(item["Actionstatus"]);

            var IsCustomException = 0;

            if (string.IsNullOrEmpty(item["Level_1_Hyper_Code"]) && string.IsNullOrEmpty(item["Level_2_Hyper_Code"])) {
                IsCustomException = 1;
            }

            if (val == 1)
            {
                qry = @"INSERT INTO Ref_Exception
                                (
                                    Exception_ID_Text,
                                    Tab_Id,
                                    Sort_Order,
                                    Level_1_Hyper_Code,
                                    Level_2_Hyper_Code,
                                    Mitigation_Text_Required_Ind,
                                    Support_Required_Ind,
                                    Support_Role_Type_Code_1,
                                    Support_Role_Type_Code_2,
                                    Support_Role_Type_Code_3,
                                    Support_Role_Type_Code_4,
                                    Pending_Action_Required_Ind,
                                    Pending_Action_Code,
                                    Document_Required_Ind,
                                    Document_Type_ID,
                                    Custom_Ind,
                                    Status
                                ) 
                            values
                                (
                                    '" + ManageQuotes(item["Exception_ID_Text"]) + "'," +
                                    "'" + item["Tab_Id"] + "'," +
                                    "'" + ManageQuotes(item["Sort_Order"]) + "'," +
                                    "'" + ManageQuotes(item["Level_1_Hyper_Code"]) + "'," +
                                    "'" + ManageQuotes(item["Level_2_Hyper_Code"]) + "'," +
                                    "'" + ManageQuotes(item["Mitigation_Text_Required_Ind"]) + "'," +
                                    "'" + ManageQuotes(item["Support_Required_Ind"]) + "'," +
                                    "'" + ManageQuotes(item["Support_Role_Type_Code_1"]) + "'," +
                                    "'" + ManageQuotes(item["Support_Role_Type_Code_2"]) + "'," +
                                    "'" + ManageQuotes(item["Support_Role_Type_Code_3"]) + "'," +
                                    "'" + ManageQuotes(item["Support_Role_Type_Code_4"]) + "'," +
                                    "'" + ManageQuotes(item["Pending_Action_Required_Ind"]) + "'," +
                                    "'" + ManageQuotes(item["Pending_Action_Code"]) + "'," +
                                    "'" + ManageQuotes(item["Document_Required_Ind"]) + "'," +
                                    "'" + ManageQuotes(item["Document_Type_ID"]) + "'," +
                                    "'" + IsCustomException + "'," +
                                    "'" + ManageQuotes(item["Status"]) + "'" +
                              ")";

                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 2)
            {
                qry = "UPDATE Ref_Exception " +
                    "SET Exception_ID_Text = '" + item["Exception_ID_Text"].Replace("'", "''") + "'," +
                        "Tab_Id = '" + item["Tab_Id"].Replace("'", "''") + "' ," +
                        "Sort_Order = '" + item["Sort_Order"].Replace("'", "''") + "' ," +
                        "Level_1_Hyper_Code = '" + item["Level_1_Hyper_Code"].Replace("'", "''") + "' ," +
                        "Level_2_Hyper_Code = '" + item["Level_2_Hyper_Code"].Replace("'", "''") + "' ," +
                        "Mitigation_Text_Required_Ind = '" + item["Mitigation_Text_Required_Ind"].Replace("'", "''") + "' ," +
                        "Support_Required_Ind = '" + item["Support_Required_Ind"].Replace("'", "''") + "' ," +
                        "Support_Role_Type_Code_1 = '" + item["Support_Role_Type_Code_1"].Replace("'", "''") + "' ," +
                        "Support_Role_Type_Code_2 = '" + item["Support_Role_Type_Code_2"].Replace("'", "''") + "' ," +
                        "Support_Role_Type_Code_3 = '" + item["Support_Role_Type_Code_3"].Replace("'", "''") + "' ," +
                        "Support_Role_Type_Code_4 = '" + item["Support_Role_Type_Code_4"].Replace("'", "''") + "' ," +
                        "Pending_Action_Required_Ind = '" + item["Pending_Action_Required_Ind"].Replace("'", "''") + "' ," +
                        "Pending_Action_Code = '" + item["Pending_Action_Code"].Replace("'", "''") + "'," +
                        "Document_Required_Ind = '" + item["Document_Required_Ind"].Replace("'", "''") + "'," +
                        "Document_Type_ID = '" + item["Document_Type_ID"].Replace("'", "''") + "'," +
                        "Custom_Ind = '" + IsCustomException + "'," +
                        "Status = '" + item["Status"].Replace("'", "''") + "' " +
                    " where Exception_ID=" + item["Exception_ID"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 3)
            {
                qry = "DELETE FROM Ref_Exception  where Exception_ID=" + item["Exception_ID"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
        }

        gen_db_utils.Update_Table_Audit_Trail("Ref_Exception", userid, dbKey);
    }
    public static string ManageQuotes(string value)
    {
        if (value.Contains("'"))
        {
            value = value.Replace("'", "''");
        }
        return value;
    }

}