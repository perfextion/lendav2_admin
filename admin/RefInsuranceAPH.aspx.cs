﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_RefInsuranceAPH : BasePage
{
    public admin_RefInsuranceAPH()
    {
        Table_Name = "Ref_Ins_APH";
    }

    [WebMethod]
    public static string GetTableInfo()
    {
        return Get_Table_Info();
    }

    [WebMethod]
    public static string LoadTable()
    {
        string sql_qry = @"SELECT  [Ref_APH_ID]
                                  ,[Policy_Holder]
                                  ,[Crop_Year]
                                  ,[State_ID]
                                  ,[County_ID]
                                  ,[Crop_Code]
                                  ,[Crop_Type]
                                  ,[Irr_Prac]
                                  ,[Crop_Prac]
                                  ,[Landowner]
                                  ,[Share]
                                  ,[FSN]
                                  ,[Rated]
                                  ,[APH]
                                  ,[Verify_APH_Ind]
                                  ,[Verify_APH_Doc_ID]
                                  ,[Verify_APH_User_ID]
                                  ,FORMAT([Verify_APH_Date_Time], 'MM-dd-yyyy') as [Verify_APH_Date_Time]
                                  ,[Rate_Yield]
                                  ,[Added_Land_Ind]
                                  ,[Status]
                                  ,0 as [Actionstatus]
                              FROM [dbo].[Ref_Ins_APH]";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static void SaveTable(dynamic ListItems)
    {
        foreach (var item in ListItems)
        {
            string qry = string.Empty;
            int val = Convert.ToInt32(item["Actionstatus"]);

            if (val == 1)
            {
                qry = @"INSERT INTO [dbo].[Ref_Ins_APH]
                                   ([Policy_Holder]
                                   ,[Crop_Year]
                                   ,[State_ID]
                                   ,[County_ID]
                                   ,[Crop_Code]
                                   ,[Crop_Type]
                                   ,[Irr_Prac]
                                   ,[Crop_Prac]
                                   ,[Landowner]
                                   ,[Share]
                                   ,[FSN]
                                   ,[Rated]
                                   ,[APH]
                                   ,[Verify_APH_Ind]
                                   ,[Verify_APH_Doc_ID]
                                   ,[Verify_APH_User_ID]
                                   ,[Verify_APH_Date_Time]
                                   ,[Rate_Yield]
                                   ,[Added_Land_Ind]
                                   ,[Status])
                             VALUES
                               (
                                    '" + TrimSQL(item["Policy_Holder"]) + @"'" +
                                   ",'" + item["Crop_Year"] + @"'" +
                                   ",'" + TrimSQL(item["State_ID"]) + @"'" +
                                   ",'" + TrimSQL(item["County_ID"]) + @"'" +
                                   ",'" + TrimSQL(item["Crop_Code"]) + @"'" +
                                   ",'" + TrimSQL(item["Crop_Type"]) + @"'" +
                                   ",'" + TrimSQL(item["Irr_Prac"]) + @"'" +
                                   ",'" + TrimSQL(item["Crop_Prac"]) + @"'" +
                                   ",'" + TrimSQL(item["Landowner"]) + @"'" +
                                   ",'" + item["Share"] + @"'" +
                                   ",'" + TrimSQL(item["FSN"]) + @"'" +
                                   ",'" + TrimSQL(item["Rated"]) + @"'" +
                                   "," + Get_ID(item["APH"]) + @"" +
                                   "," + Get_ID(item["Verify_APH_Ind"]) + @"" +
                                   "," + Get_ID(item["Verify_APH_Doc_ID"]) + @"" +
                                   "," + Get_ID(item["Verify_APH_User_ID"]) + @"" +
                                   "," + Get_ID(item["Verify_APH_Date_Time"]) + @"" +
                                   "," + Get_ID(item["Rate_Yield"]) + @"'" +
                                   "," + Get_ID(item["Added_Land_Ind"]) + @"" +
                                   ",'" + item["Status"] + @"'
                                )  ";

                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 2)
            {
                qry = @"UPDATE [dbo].[Ref_Ins_APH]
                        SET   [Policy_Holder] = '" + item["Policy_Holder"] + @"' " +
                            ",[Crop_Year] = '" + item["Crop_Year"] + @"' " +
                            ",[State_ID] = '" + item["State_ID"] + @"' " +
                            ",[County_ID] = '" + item["County_ID"] + @"' " +
                            ",[Crop_Code] = '" + item["Crop_Code"] + @"' " +
                            ",[Crop_Type] = '" + item["Crop_Type"] + @"' " +
                            ",[Irr_Prac] = '" + item["Irr_Prac"] + @"' " +
                            ",[Crop_Prac] = '" + item["Crop_Prac"] + @"' " +
                            ",[Landowner] = '" + item["Landowner"] + @"' " +
                            ",[Share] = '" + item["Share"] + @"' " +
                            ",[FSN] = '" + item["FSN"] + @"' " +
                            ",[Rated] = '" + item["Rated"] + @"' " +
                            ",[APH] = '" + item["APH"] + @"' " +
                            ",[Verify_APH_Ind] = " + Get_ID(item["Verify_APH_Ind"]) + @" " +
                            ",[Verify_APH_Doc_ID] = " + Get_ID(item["Verify_APH_Doc_ID"]) + @" " +
                            ",[Verify_APH_User_ID] = " + Get_ID(item["Verify_APH_User_ID"]) + @" " +
                            ",[Verify_APH_Date_Time] = " + Get_ID(item["Verify_APH_Date_Time"]) + @" " +
                            ",[Rate_Yield] = " + Get_ID(item["Rate_Yield"]) + @"' " +
                            ",[Added_Land_Ind] = " + Get_ID(item["Added_Land_Ind"]) + @" " +
                            ",[Status] = '" + item["Status"] + @"'
                        WHERE Ref_APH_ID = '" + item["Ref_APH_ID"] + "' ";

                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 3)
            {
                qry = "DELETE FROM [Ref_Ins_APH]  where Ref_APH_ID = '" + item["Ref_APH_ID"] + "' ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
        }

        if (ListItems != null)
        {
            Update_Table_Audit();
        }
    }
}