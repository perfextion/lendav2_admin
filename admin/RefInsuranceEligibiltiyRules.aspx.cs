﻿using Newtonsoft.Json;
using System;
using System.Data;
using System.Web.Services;

public partial class admin_RefInsuranceEligibiltiyRules : BasePage
{
    public admin_RefInsuranceEligibiltiyRules()
    {
        Table_Name = "Ref_Ins_Eligibility_Rules";
    }

    [WebMethod]
    public static string GetTableInfo()
    {
        return Get_Table_Info();
    }

    [WebMethod]
    public static string LoadTable()
    {
        string sql_qry = @"SELECT *, 0 as [Actionstatus] from Ref_Ins_Eligibility_Rules order by [Ins_Plan], [Ins_Plan_Type]";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static void SaveTable(dynamic ListItems)
    {
        foreach (var item in ListItems)
        {
            string qry = string.Empty;
            int val = Convert.ToInt32(item["Actionstatus"]);

            if (val == 1)
            {
                qry = @"INSERT INTO [dbo].[Ref_Ins_Eligibility_Rules]
                               ([Ins_Plan]
                               ,[Ins_Plan_Type]
                               ,[Eligible_Crop_Code]
                               ,[Eligible_State_Abbrev]
                               ,[Ineligible_Crop_Prac]
                               ,[Status])
                         VALUES
                               (
                                    '" + TrimSQL(item["Ins_Plan"]) + @"'" +
                                   ",'" + TrimSQL(item["Ins_Plan_Type"]) + @"'" +
                                   ",'" + TrimSQL(item["Eligible_Crop_Code"]) + @"'" +
                                   ",'" + TrimSQL(item["Eligible_State_Abbrev"]) + @"'" +
                                   ",'" + TrimSQL(item["Ineligible_Crop_Prac"]) + @"'" +
                                   ",'" + item["Status"] + @"'
                                )  ";

                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 2)
            {
                qry = @"UPDATE [dbo].[Ref_Ins_Eligibility_Rules]
                           SET  [Ins_Plan] = '" + TrimSQL(item["Ins_Plan"]) + @"' " +
                              ",[Ins_Plan_Type] = '" + TrimSQL(item["Ins_Plan_Type"]) + @"' " +
                              ",[Eligible_Crop_Code] = '" + TrimSQL(item["Eligible_Crop_Code"]) + @"' " +
                              ",[Eligible_State_Abbrev] = '" + TrimSQL(item["Eligible_State_Abbrev"]) + @"' " +
                              ",[Ineligible_Crop_Prac] = '" + TrimSQL(item["Ineligible_Crop_Prac"]) + @"' " +
                              ",[Status] = '" + item["Status"] + @"' " +
                         "WHERE Ins_Eligibility_Rule_ID = '" + item["Ins_Eligibility_Rule_ID"] + "' ";

                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 3)
            {
                qry = "DELETE FROM Ref_Ins_Eligibility_Rules  where Ins_Eligibility_Rule_ID = '" + item["Ins_Eligibility_Rule_ID"] + "' ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
        }

        if (ListItems != null)
        {
            Update_Table_Audit();
        }
    }
}