﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_RefInsurancePolicyPlan : BasePage
{
    public admin_RefInsurancePolicyPlan()
    {
        Table_Name = "Ref_Ins_Policy_Plan";
    }

    [WebMethod]
    public static string GetTableInfo()
    {
        return Get_Table_Info();
    }

    [WebMethod]
    public static Dictionary<string, string> GetAIPList()
    {
        string sql_qry = @"select Ref_Association_Id, Assoc_Name from Ref_Association Where Assoc_Type_Code = 'AIP' union select '0' ,'' ";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return dt.AsEnumerable().ToDictionary(row => row[0].ToString(), row => row[1].ToString());
    }

    [WebMethod]
    public static string LoadTable()
    {
        string sql_qry = @"SELECT *, 0 as [Actionstatus] from Ref_Ins_Policy_Plan";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static void SaveTable(dynamic ListItems)
    {
        foreach (var item in ListItems)
        {
            string qry = string.Empty;
            int val = Convert.ToInt32(item["Actionstatus"]);

            if (val == 1)
            {
                qry = @"INSERT INTO [dbo].[Ref_Ins_Policy_Plan]
                               ([Ins_Plan]
                               ,[Ins_Plan_Name]
                               ,[Federal_Ind]
                               ,[Unique_AIP_Ind]
                               ,[Unique_AIP_ID]
                               ,[Status])
                         VALUES
                               (
                                    '" + TrimSQL(item["Ins_Plan"]) + @"'" +
                                   ",'" + TrimSQL(item["Ins_Plan_Name"]) + @"'" +
                                   "," + Get_ID(item["Federal_Ind"]) + @"" +
                                   "," + Get_ID(item["Unique_AIP_Ind"]) + @"" +
                                   ",'" + TrimSQL(item["Unique_AIP_ID"]) + @"'" +
                                   ",'" + item["Status"] + @"'
                                )  ";

                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 2)
            {
                qry = @"UPDATE [dbo].[Ref_Ins_Policy_Plan]
                           SET  [Ins_Plan] = '" + TrimSQL(item["Ins_Plan"]) + @"' " +
                              ",[Ins_Plan_Name] = '" + TrimSQL(item["Ins_Plan_Name"]) + @"' " +
                              ",[Federal_Ind] = " + Get_ID(item["Federal_Ind"]) + @" " +
                              ",[Unique_AIP_Ind] = " + Get_ID(item["Unique_AIP_Ind"]) + @" " +
                              ",[Unique_AIP_ID] = '" + TrimSQL(item["Unique_AIP_ID"]) + @"' " +
                              ",[Status] = '" + item["Status"] + @"' " +
                         "WHERE Ref_Ins_Plan_ID = '" + item["Ref_Ins_Plan_ID"] + "' ";

                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 3)
            {
                qry = "DELETE FROM Ref_Ins_Policy_Plan  where Ref_Ins_Plan_ID = '" + item["Ref_Ins_Plan_ID"] + "' ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
        }

        if (ListItems != null)
        {
            Update_Table_Audit();
        }
    }
}