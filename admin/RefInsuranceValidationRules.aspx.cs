﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_RefInsuranceValidationRules : BasePage
{
    public admin_RefInsuranceValidationRules()
    {
        Table_Name = "Ref_Ins_Validation_Rules";
    }

    [WebMethod]
    public static string GetTableInfo()
    {
        return Get_Table_Info();
    }

    [WebMethod]
    public static string LoadTable()
    {
        string sql_qry = @"SELECT *, 0 as [Actionstatus] from [Ref_Ins_Validation_Rules]";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static void SaveTable(dynamic ListItems)
    {
        foreach (var item in ListItems)
        {
            string qry = string.Empty;
            int val = Convert.ToInt32(item["Actionstatus"]);

            if (val == 1)
            {
                qry = @"INSERT INTO [dbo].[Ref_Ins_Validation_Rules]
                                   ([Ins_Plan]
                                   ,[Ins_Plan_Type]
                                   ,[Unit]
                                   ,[Option_Type]
                                   ,[Upper_Pct]
                                   ,[Lower_Pct]
                                   ,[Range_Max]
                                   ,[Area_Yield]
                                   ,[Yield_Pct]
                                   ,[Price_Pct]
                                   ,[Liability_Max_Upper_Pct_75]
                                   ,[Liability_Max_Upper_Pct_85]
                                   ,[Deduct_Percent_Max]
                                   ,[Deduct_Unit_CRN]
                                   ,[Deduct_Unit_SOY]
                                   ,[Deduct_Unit_RIC]
                                   ,[Deduct_Unit_WHT]
                                   ,[Late_Deduct_Unit]
                                   ,[FCMC]
                                   ,[AIP]
                                   ,[Status])
                             VALUES
                               (
                                    '" + TrimSQL(item["Ins_Plan"]) + @"'" +
                                   ",'" + TrimSQL(item["Ins_Plan_Type"]) + @"'" +
                                   ",'" + TrimSQL(item["Unit"]) + @"'" +
                                   ",'" + TrimSQL(item["Option_Type"]) + @"'" +
                                   ",'" + TrimSQL(item["Upper_Pct"]) + @"'" +
                                   ",'" + TrimSQL(item["Lower_Pct"]) + @"'" +
                                   "," + Get_ID(item["Range_Max"]) + @"" +
                                   "," + Get_ID(item["Area_Yield"]) + @"" +
                                   ",'" + TrimSQL(item["Yield_Pct"]) + @"'" +
                                   ",'" + TrimSQL(item["Price_Pct"]) + @"'" +
                                   ",'" + TrimSQL(item["Liability_Max_Upper_Pct_75"]) + @"'" +
                                   ",'" + TrimSQL(item["Liability_Max_Upper_Pct_85"]) + @"'" +
                                   ",'" + TrimSQL(item["Deduct_Percent_Max"]) + @"'" +
                                   "," + Get_ID(item["Deduct_Unit_CRN"]) + @"" +
                                   "," + Get_ID(item["Deduct_Unit_SOY"]) + @"" +
                                   "," + Get_ID(item["Deduct_Unit_RIC"]) + @"" +
                                   "," + Get_ID(item["Deduct_Unit_WHT"]) + @"" +
                                   "," + Get_ID(item["Late_Deduct_Unit"]) + @"" +
                                   "," + Get_ID(item["FCMC"]) + @"" +
                                   ",'" + TrimSQL(item["AIP"]) + @"'" +
                                   ",'" + item["Status"] + @"'
                                )  ";

                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 2)
            {
                qry = @"UPDATE [dbo].[Ref_Ins_Validation_Rules]
                           SET  [Ins_Plan] = '" + TrimSQL(item["Ins_Plan"]) + @"' " +
                              ",[Ins_Plan_Type] = '" + TrimSQL(item["Ins_Plan_Type"]) + @"' " +
                              ",[Unit] = '" + TrimSQL(item["Unit"]) + @"' " +
                              ",[Option_Type] = '" + TrimSQL(item["Option_Type"]) + @"' " +
                              ",[Upper_Pct] = '" + TrimSQL(item["Upper_Pct"]) + @"' " +
                              ",[Lower_Pct] = '" + TrimSQL(item["Lower_Pct"]) + @"' " +
                              ",[Range_Max] = " + Get_ID(item["Range_Max"]) + @" " +
                              ",[Area_Yield] = " + Get_ID(item["Area_Yield"]) + @" " +
                              ",[Yield_Pct] = '" + TrimSQL(item["Yield_Pct"]) + @"' " +
                              ",[Price_Pct] = '" + TrimSQL(item["Price_Pct"]) + @"' " +
                              ",[Liability_Max_Upper_Pct_75] = '" + TrimSQL(item["Liability_Max_Upper_Pct_75"]) + @"' " +
                              ",[Liability_Max_Upper_Pct_85] = '" + TrimSQL(item["Liability_Max_Upper_Pct_85"]) + @"' " +
                              ",[Deduct_Percent_Max] = '" + TrimSQL(item["Deduct_Percent_Max"]) + @"' " +
                              ",[Deduct_Unit_CRN] = " + Get_ID(item["Deduct_Unit_CRN"]) + @" " +
                              ",[Deduct_Unit_SOY] = " + Get_ID(item["Deduct_Unit_SOY"]) + @" " +
                              ",[Deduct_Unit_RIC] = " + Get_ID(item["Deduct_Unit_RIC"]) + @" " +
                              ",[Deduct_Unit_WHT] = " + Get_ID(item["Deduct_Unit_WHT"]) + @" " +
                              ",[Late_Deduct_Unit] = " + Get_ID(item["Late_Deduct_Unit"]) + @" " +
                              ",[FCMC] = " + Get_ID(item["FCMC"]) + @" " +
                              ",[AIP] = '" + TrimSQL(item["AIP"]) + @"' " +
                              ",[Status] = '" + item["Status"] + @"' " +
                         "WHERE Ins_Validation_Rule_ID = '" + item["Ins_Validation_Rule_ID"] + "' ";

                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 3)
            {
                qry = "DELETE FROM [Ref_Ins_Validation_Rules] where Ins_Validation_Rule_ID = '" + item["Ins_Validation_Rule_ID"] + "' ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
        }

        if (ListItems != null)
        {
            Update_Table_Audit();
        }
    }
}