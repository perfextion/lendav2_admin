﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Services;

public partial class admin_RefListItem : BasePage
{
    private static List<string> ExclusionList = null;
    public admin_RefListItem()
    {
        Table_Name = "Ref_List_Item";

        ExclusionList = new List<string>
        {
            "'ASSOC_REFERRAL_TYPE'",
            "'ASSOC_TYPE'",
            "'BORROWER_ENTITY_TYPE'",
            "'SCALE'"
        };
    }

    [WebMethod]
    public static string GetTableInfo(string List_Group_Code)
    {
        Count_Query = @"DECLARE @Count int;
                        Select @Count = Count(*)  From Ref_List_Item Where List_Group_Code NOT IN (" + string.Join(",", ExclusionList) + ")";

        if (!string.IsNullOrEmpty(List_Group_Code))
        {
            Count_Query += " AND List_Group_Code = '" + List_Group_Code + "'";
        }

        return Get_Table_Info();
    }

    [WebMethod]
    public static string GetListItems(string List_Group_Code)
    {
        string sql_qry = @"SELECT *, 0 as [Actionstatus] from [Ref_List_Item] Where List_Group_Code NOT IN (" + string.Join(",", ExclusionList) + ")";

        if(!string.IsNullOrEmpty(List_Group_Code))
        {
            sql_qry += " AND List_Group_Code = '" + List_Group_Code + "'";
        }

        sql_qry += " ORDER BY List_Group_Code";

        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static void SyncListItems(dynamic ListItems)
    {
        foreach (var item in ListItems)
        {
            string qry = "";
            int val = Convert.ToInt32(item["Actionstatus"]);

            if (val == 1)
            {
                qry = @"INSERT INTO [dbo].[Ref_List_Item]
                               ([List_Item_Code]
                               ,[List_Item_Name]
                               ,[List_Group_Code]
                               ,[List_Item_Value]
                               ,[Status])
                         VALUES
                               (
                                    '" + item["List_Item_Code"].Replace("'", "''") + @"'" +
                                   ",'" + item["List_Item_Name"].Replace("'", "''")  + @"'" +
                                   ",'" + item["List_Group_Code"].Replace("'", "''") + @"'" +
                                   ",'" + item["List_Item_Value"].Replace("'", "''") + @"'" +
                                   ",'" + item["Status"] + @"'
                                )  ";

                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 2)
            {
                qry = @"UPDATE [dbo].[Ref_List_Item]
                           SET  [List_Item_Code] = '" + item["List_Item_Code"].Replace("'", "''") + @"' " +
                              ",[List_Item_Name] = '" + item["List_Item_Name"].Replace("'", "''") + @"' " +
                              ",[List_Group_Code] = '" + item["List_Group_Code"].Replace("'", "''") + @"' " +
                              ",[List_Item_Value] = '" + item["List_Item_Value"].Replace("'", "''") + @"' " +
                              ",[Status] = '" + item["Status"] + @"' " +
                         "WHERE List_Item_ID = '" + item["List_Item_ID"] + "' ";

                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 3)
            {
                qry = "DELETE FROM Ref_List_Item  where List_Item_ID = '" + item["List_Item_ID"] + "' ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
        }

        Update_Table_Audit();
    }

    [WebMethod]
    public static Dictionary<string, string> GetAllListGroupCodes()
    {
        string sql_qry = "select distinct List_Group_Code From Ref_List_Item Where List_Group_Code NOT IN (" + string.Join(",", ExclusionList) + ") Order by List_Group_Code";

        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return dt.AsEnumerable().ToDictionary(row => row[0].ToString(), row => row[0].ToString());
    }
}