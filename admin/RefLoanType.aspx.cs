﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_RefLoanType : BasePage
{
    public admin_RefLoanType()
    {
        Table_Name = "Ref_Loan_Type";
    }

    [WebMethod]
    public static string GetTableInfo()
    {
        return Get_Table_Info();
    }

    [WebMethod]
    public static string LoadTable()
    {
        string sql_query = @"Select *, 0 as [Actionstatus] from Ref_Loan_Type";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_query, dbKey);

        return JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static void SaveTable(dynamic LoanTypes)
    {
        string sql = string.Empty;

        foreach (var item in LoanTypes)
        {
            int val = Convert.ToInt32(item["Actionstatus"]);

            if (val == 1)
            {
                sql += @"INSERT INTO [dbo].[Ref_Loan_Type]
                                   ([Loan_Type_Code]
                                   ,[Loan_Type_Name]
                                   ,[Origination_Fee_Percent]
                                   ,[Service_Fee_Percent]
                                   ,[Extension_Fee_Percent]
                                   ,[Rate_Percent]
                                   ,[Status])
                             VALUES
                               (
                                    '" + item["Loan_Type_Code"].Replace("'", "''") + @"'" +
                                   ",'" + item["Loan_Type_Name"].Replace("'", "''") + @"'" +
                                   ",'" + item["Origination_Fee_Percent"] + @"'" +
                                   ",'" + item["Service_Fee_Percent"] + @"'" +
                                   ",'" + item["Extension_Fee_Percent"] + @"'" +
                                   ",'" + item["Rate_Percent"] + @"'" +
                                   ",'" + item["Status"] + @"'
                                )  ";
                sql += Environment.NewLine;
            }
            else if (val == 2)
            {
                sql += @"UPDATE [dbo].[Ref_Loan_Type]
                           SET  [Loan_Type_Code] = '" + item["Loan_Type_Code"].Replace("'", "''") + @"' " +
                              ",[Loan_Type_Name] = '" + item["Loan_Type_Name"].Replace("'", "''") + @"' " +
                              ",[Origination_Fee_Percent] = '" + item["Origination_Fee_Percent"] + @"' " +
                              ",[Service_Fee_Percent] = '" + item["Service_Fee_Percent"] + @"' " +
                              ",[Extension_Fee_Percent] = '" + item["Extension_Fee_Percent"] + @"' " +
                              ",[Rate_Percent] = '" + item["Rate_Percent"] + @"' " +
                              ",[Status] = '" + item["Status"] + @"' " +
                         "WHERE Loan_Type_ID = '" + item["Loan_Type_ID"] + "' ";
                sql += Environment.NewLine;
            }
            else if (val == 3)
            {
                sql += "DELETE FROM Ref_Loan_Type  where Loan_Type_ID = '" + item["Loan_Type_ID"] + "' ";
                sql += Environment.NewLine;

            }
        }

        if(!string.IsNullOrEmpty(sql))
        {
            gen_db_utils.gp_sql_execute(sql, dbKey);
            gen_db_utils.Update_Table_Audit_Trail("Ref_Loan_Type", userid, dbKey);
        }
    }
}