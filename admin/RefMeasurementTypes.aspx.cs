﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;

public partial class admin_RefMeasurementTypes : BasePage
{
    public admin_RefMeasurementTypes()
    {
        Table_Name = "Ref_Measurement_Type";
    }

    [WebMethod]
    public static string GetTableInfo()
    {
        return Get_Table_Info();
    }

    [WebMethod]
    public static Dictionary<string, string> GetCollateralCategories()
    {
        string sql_qry = "select Collateral_Category_Code, Collateral_Category_Name from Ref_Collateral_Category order by [sort_order]";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return dt.AsEnumerable().ToDictionary(row => row[0].ToString(), row => row[1].ToString());
    }

    [WebMethod]
    public static string GetMeasurementTypes()
    {
        string sql_qry = @"SELECT 
                            REM.*, 
                            ISNULL(RCC.Collateral_Category_Name,Collateral_Category) as [Collateral_Category_Name] 
                          FROM Ref_Measurement_Type REM 
                          LEFT JOIN Ref_Collateral_Category RCC on REM.Collateral_Category = RCC.Collateral_Category_Code
                          Order by ISNULL(RCC.Sort_Order, REM.Measurement_Type_Id)";

        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static void SaveMeasurementTypes(dynamic lst)
    {
        foreach (var item in lst)
        {
            string qry = string.Empty;
            int val = Convert.ToInt32(item["Actionstatus"]);
            if (val == 1)
            {
                qry = @"INSERT INTO [dbo].[Ref_Measurement_Type]
                               ([Measurement_Type_Code]
                               ,[Measurement_Type_Name]
                               ,[Collateral_Category]
                               ,[Disc_Percent]
                               ,[Validation_ID]
                               ,[Exception_ID]
                               ,[Status])
                         VALUES
                               (
                                    '" + item["Measurement_Type_Code"].Replace("'", "''") + @"'" +
                                   ",'" + item["Measurement_Type_Name"].Replace("'", "''") + @"'" +
                                   ",'" + item["Collateral_Category"].Replace("'", "''") + @"'" +
                                   ",'" + item["Disc_Percent"] + @"'" +
                                   "," + Get_ID(item["Validation_ID"]) + @"" +
                                   "," + Get_ID(item["Exception_ID"]) + @"" +
                                   ",'" + item["Status"] + @"'
                                )  ";

                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 2)
            {
                qry = @"UPDATE [dbo].[Ref_Measurement_Type]
                           SET [Measurement_Type_Name] = '" + item["Measurement_Type_Name"].Replace("'", "''") + @"' " +
                              ",[Measurement_Type_Code] = '" + item["Measurement_Type_Code"].Replace("'", "''") + @"' " +
                              ",[Collateral_Category] = '" + item["Collateral_Category"].Replace("'", "''") + @"' " +
                              ",[Disc_Percent] = '" + item["Disc_Percent"] + @"' " +
                              ",[Validation_ID] = " + Get_ID(item["Validation_ID"]) + @" " +
                              ",[Exception_ID] = " + Get_ID(item["Exception_ID"]) + @" " +
                              ",[Status] = '" + item["Status"] + @"' " +
                         "WHERE Measurement_Type_ID = '" + item["Measurement_Type_ID"] + "' ";

                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 3)
            {
                qry = "DELETE FROM Ref_Measurement_Type  where Measurement_Type_ID = '" + item["Measurement_Type_ID"] + "' ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
        }

        Update_Table_Audit();
    }
}