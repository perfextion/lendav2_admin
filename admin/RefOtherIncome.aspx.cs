﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Services;

public partial class admin_RefOtherIncome : System.Web.UI.Page
{
    private static string dbKey = "gp_conn";
    static string userid;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserID"] == null)
        {
            Response.Redirect("~/Login.aspx");
        }
        else
        {
            userid = Session["UserID"].ToString();
        }
    }

    [WebMethod]
    public static string GetTableInfo()
    {
        try
        {
            string sql_query = @"DECLARE @Count int;
                            Select @Count = Count(*)  From Ref_Other_Income;

                            SELECT
	                            Table_UI_Name as [Table_Name],
	                            CONCAT('Records Count: ', @Count) as [Records_Count],
	                            CASE 
		                            WHEN Modified_By IS NOT NULL THEN CONCAT('Last Updated by: ', DBO.GetUserNameByUserID(Modified_By), ' ', FORMAT( Modified_On, 'MM/dd/yyyy hh:mm:ss tt', 'en-US' ))
		                            ELSE ''
	                            END AS [Last_Updated]
                            FROM Table_Audit_Trail
                            WHERE Table_Name = 'Ref_Other_Income' ";

            DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_query, dbKey);
            return JsonConvert.SerializeObject(dt);
        }
        catch
        {
            return JsonConvert.SerializeObject(new List<object> { new object() });
        }
    }

    [WebMethod]
    public static string GetRefOtherIncomes()
    {
        string sql_qry = "SELECT *, 0 as [ActionStatus] from Ref_Other_Income Order By [Sort_Order]";        
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static void SaveOtherIncomes(dynamic lst)
    {
        foreach (var item in lst)
        {
            string qry = "";
            int val = Convert.ToInt32(item["Actionstatus"]);

            if (val == 1)
            {
                qry = @"INSERT INTO [dbo].[Ref_Other_Income]
                               (
                                    [Other_Income_Name]
                                   ,[Sort_Order]
                                   ,[Status]
                                )
                         VALUES
                               (
                                    '" + item["Other_Income_Name"].Replace("'", "''") + @"'" +
                                   ",'" + item["Sort_Order"] + @"'" +
                                   ",'" + item["Status"] + @"'
                                )  ";

                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 2)
            {
                qry = @"UPDATE [dbo].[Ref_Other_Income]
                           SET [Other_Income_Name] = '" + item["Other_Income_Name"].Replace("'", "''") + @"' " +
                              ",[Sort_Order] = '" + item["Sort_Order"] + @"' " +
                              ",[Status] = '" + item["Status"] + @"' " +
                         "WHERE Other_Income_ID = '" + item["Other_Income_ID"] + "' ";

                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 3)
            {
                qry = "DELETE FROM Ref_Other_Income  where Other_Income_ID = '" + item["Other_Income_ID"] + "' ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
        }

        gen_db_utils.Update_Table_Audit_Trail("Ref_Other_Income", userid, dbKey);
    }
}