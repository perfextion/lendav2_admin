﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
public partial class admin_Reference_RefValidation : BasePage
{
    public admin_Reference_RefValidation()
    {
        Table_Name = "Ref_Validation";
    }

    [WebMethod]
    public static string GetTableInfo()
    {
        return Get_Table_Info();
    }

    [WebMethod]
    public static string GetRefValidation()
    {
        string sql_qry = "SELECT 0 as Actionstatus,Validation_ID,Validation_ID_Text,Tab_ID,Sort_Order,Level_1_Hyper_Code,Level_2_Hyper_Code,Pending_Action_Required_Ind,Pending_Action_Code,Status FROM Ref_Validation ";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
    }


    [WebMethod]
    public static void SaveRefValidation(dynamic lst)
    {
        foreach (var item in lst)
        {
            string qry = "";
            int val = Convert.ToInt32(item["Actionstatus"]);
            if (val == 1)
            {
                qry = "INSERT INTO Ref_Validation(Validation_ID_Text,Tab_ID,Sort_Order,Level_1_Hyper_Code,Level_2_Hyper_Code,Pending_Action_Required_Ind,Pending_Action_Code,Status)" +
                    " VALUES('" + item["Validation_ID_Text"].Replace("'", "''") + "','" + item["Tab_ID"].Replace("'", "''") + "'," +
                    "'" + item["Sort_Order"].Replace("'", "''") + "','" + item["Level_1_Hyper_Code"].Replace("'", "''") + "'," +
                    "'" + item["Level_2_Hyper_Code"].Replace("'", "''") + "','" + item["Pending_Action_Required_Ind"].Replace("'", "''") + "'," +
                    "'" + item["Pending_Action_Code"].Replace("'", "''") + "','" + item["Status"].Replace("'", "''") + "')";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 2)
            {
                qry = "UPDATE Ref_Validation SET Validation_ID_Text = '" + item["Validation_ID_Text"].Replace("'", "''") + "' ," +
                    "Tab_ID = '" + item["Tab_ID"].Replace("'", "''") + "'," +
                    "Sort_Order = '" + item["Sort_Order"].Replace("'", "''") + "',Level_1_Hyper_Code = '" + item["Level_1_Hyper_Code"].Replace("'", "''") + "'," +
                    "Level_2_Hyper_Code = '" + item["Level_2_Hyper_Code"].Replace("'", "''") + "',Pending_Action_Required_Ind = '" + item["Pending_Action_Required_Ind"].Replace("'", "''") + "'," +
                    "Pending_Action_Code = '" + item["Pending_Action_Code"].Replace("'", "''") + "',Status = '" + item["Status"].Replace("'", "''") + "' " +
                    " where Validation_ID=" + item["Validation_ID"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 3)
            {
                qry = "DELETE FROM Ref_Validation  where Validation_ID=" + item["Validation_ID"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
        }

        gen_db_utils.Update_Table_Audit_Trail("Ref_Validation", userid, dbKey);
    }
}