﻿<%@ Page Title="Borrower Farm Financial Ratio" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="Ref_Farm_Financial_Rating.aspx.cs" Inherits="admin_Ref_Farm_Financial_Rating" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <style>
        input[type="text"] {
            width: 150px;
            text-align: center;
        }

        table {
            border-collapse: collapse;
        }

        table, td, th {
            border: 1px solid black;
        }

        tr {
            text-align: center;
        }

        table.farm-financial {
            width: 1030px;
        }

        .farm-financial tbody tr td {
            cursor: pointer;
        }

        .farm-financial tbody tr td  input {
            cursor: text;
        }

        .farm-financial tbody tr td:not(:first-child) {
            width: 150px;
            padding: 5px;
        }

        .farm-financial tbody tr td:first-child {
            text-align: left;
        }

        #content {
            padding: 14px !important;
        }
    </style>
     <div class="row switch-controls">
        <div class="switch-control disabled" id="editControl">
            <label for="editBtn">Edit</label>
            <div class="toggle-btn small">            
                <input type="checkbox" checked="checked" id="editBtn" class="cb-value" />
                <span class="round-btn"></span>
            </div>
        </div>
        <div class="switch-control disabled" id="enableControl">
            <label for="enableBtn">Enable</label>
            <div class="toggle-btn small">            
                <input type="checkbox" id="enableBtn" class="cb-value" />
                <span class="round-btn"></span>
            </div>
        </div>
    </div>
    <div class="loader"></div>
    <table id="ffrtable" class="table table-bordered table-hover farm-financial" style="margin: 20px;    margin-left: 35px">
        <thead>
            <tr style="height: 40px;">
                <th style="text-align: center; width: 200px;" class="secure-code">Farm Finanical Ratio Name</th>
                <th style="text-align: center;">Owned Strong</th>
                <th style="text-align: center;">Owned Stable</th>
                <th style="text-align: center;">Leased Strong</th>
                <th style="text-align: center;">Leased Stable</th>
                <th style="text-align: center;">Weight</th>
            </tr>
        </thead>
        <tbody>
            <tr>
            <td style="min-width: 50px">Current Ratio</td>
            <td style="min-width: 50px">
                <input type="text" id="CR_OWNED_STRONG" class="form-control savetbox" />
            </td>
            <td style="min-width: 50px">
                <input type="text" id="CR_OWNED_STABLE" class="form-control savetbox" /></td>
            <td style="min-width: 50px">
                <input type="text" id="CR_LEASED_STRONG" class="form-control savetbox" /></td>
            <td style="min-width: 50px">
                <input type="text" id="CR_LEASED_STABLE" class="form-control savetbox" /></td>
            <td style="min-width: 50px">
                <input type="text" id="CR_WEIGHT" class="form-control savetbox" /></td>
        </tr>
        <tr>
            <td style="min-width: 50px">Working Capital</td>
            <td style="min-width: 50px">
                <input type="text" id="WC_OWNED_STRONG" class="form-control savetbox" /></td>
            <td style="min-width: 50px">
                <input type="text" id="WC_OWNED_STABLE" class="form-control savetbox" /></td>
            <td style="min-width: 50px">
                <input type="text" id="WC_LEASED_STRONG" class="form-control savetbox" /></td>
            <td style="min-width: 50px">
                <input type="text" id="WC_LEASED_STABLE" class="form-control savetbox" /></td>
            <td style="min-width: 50px">
                <input type="text" id="WC_WEIGHT" class="form-control savetbox" /></td>
        </tr>
        <tr>
            <td style="min-width: 50px">Debt / Assets</td>
            <td style="min-width: 50px">
                <input type="text" id="DA_OWNED_STRONG" class="form-control savetbox" /></td>
            <td style="min-width: 50px">
                <input type="text" id="DA_OWNED_STABLE" class="form-control savetbox" /></td>
            <td style="min-width: 50px">
                <input type="text" id="DA_LEASED_STRONG" class="form-control savetbox" /></td>
            <td style="min-width: 50px">
                <input type="text" id="DA_LEASED_STABLE" class="form-control savetbox" /></td>
            <td style="min-width: 50px">
                <input type="text" id="DA_WEIGHT" class="form-control savetbox" /></td>
        </tr>
        <tr>
            <td style="min-width: 50px">Equity / Assets</td>
            <td style="min-width: 50px">
                <input type="text" id="EA_OWNED_STRONG" class="form-control savetbox" /></td>
            <td style="min-width: 50px">
                <input type="text" id="EA_OWNED_STABLE" class="form-control savetbox" /></td>
            <td style="min-width: 50px">
                <input type="text" id="EA_LEASED_STRONG" class="form-control savetbox" /></td>
            <td style="min-width: 50px">
                <input type="text" id="EA_LEASED_STABLE" class="form-control savetbox" /></td>
            <td style="min-width: 50px">
                <input type="text" id="EA_WEIGHT" class="form-control savetbox" /></td>
        </tr>
        <tr>
            <td style="min-width: 50px">Debt / Equity</td>
            <td style="min-width: 50px">
                <input type="text" id="DE_OWNED_STRONG" class="form-control savetbox" /></td>
            <td style="min-width: 50px">
                <input type="text" id="DE_OWNED_STABLE" class="form-control savetbox" /></td>
            <td style="min-width: 50px">
                <input type="text" id="DE_LEASED_STRONG" class="form-control savetbox" /></td>
            <td style="min-width: 50px">
                <input type="text" id="DE_LEASED_STABLE" class="form-control savetbox" /></td>
            <td style="min-width: 50px">
                <input type="text" id="DE_WEIGHT" class="form-control savetbox" /></td>
        </tr>
        <tr>
            <td style="min-width: 50px">ROA</td>
            <td style="min-width: 50px">
                <input type="text" id="ROA_OWNED_STRONG" class="form-control savetbox" /></td>
            <td style="min-width: 50px">
                <input type="text" id="ROA_OWNED_STABLE" class="form-control savetbox" /></td>
            <td style="min-width: 50px">
                <input type="text" id="ROA_LEASED_STRONG" class="form-control savetbox" /></td>
            <td style="min-width: 50px">
                <input type="text" id="ROA_LEASED_STABLE" class="form-control savetbox" /></td>
            <td style="min-width: 50px">
                <input type="text" id="ROA_WEIGHT" class="form-control savetbox" /></td>
        </tr>
        <tr>
            <td style="min-width: 50px">Operating Profit</td>
            <td style="min-width: 50px">
                <input type="text" id="OP_OWNED_STRONG" class="form-control savetbox" /></td>
            <td style="min-width: 50px">
                <input type="text" id="OP_OWNED_STABLE" class="form-control savetbox" /></td>
            <td style="min-width: 50px">
                <input type="text" id="OP_LEASED_STRONG" class="form-control savetbox" /></td>
            <td style="min-width: 50px">
                <input type="text" id="OP_LEASED_STABLE" class="form-control savetbox" /></td>
            <td style="min-width: 50px">
                <input type="text" id="OP_WEIGHT" class="form-control savetbox" /></td>
        </tr>
        <tr>
            <td style="min-width: 50px">Operating Exp / Rev</td>
            <td style="min-width: 50px">
                <input type="text" id="OPER_OWNED_STRONG" class="form-control savetbox" /></td>
            <td style="min-width: 50px">
                <input type="text" id="OPER_OWNED_STABLE" class="form-control savetbox" /></td>
            <td style="min-width: 50px">
                <input type="text" id="OPER_LEASED_STRONG" class="form-control savetbox" /></td>
            <td style="min-width: 50px">
                <input type="text" id="OPER_LEASED_STABLE" class="form-control savetbox" /></td>
            <td style="min-width: 50px">
                <input type="text" id="OPER_WEIGHT" class="form-control savetbox" /></td>
        </tr>
        <tr>
            <td style="min-width: 50px">Interest / Cash flow</td>
            <td style="min-width: 50px">
                <input type="text" id="CF_OWNED_STRONG" class="form-control savetbox" /></td>
            <td style="min-width: 50px">
                <input type="text" id="CF_OWNED_STABLE" class="form-control savetbox" /></td>
            <td style="min-width: 50px">
                <input type="text" id="CF_LEASED_STRONG" class="form-control savetbox" /></td>
            <td style="min-width: 50px">
                <input type="text" id="CF_LEASED_STABLE" class="form-control savetbox" /></td>
            <td style="min-width: 50px">
                <input type="text" id="CF_WEIGHT" class="form-control savetbox" /></td>
        </tr>
        </tbody>
    </table>
    <script>       
        $(function () {            
            $('.disabled').prop('disabled', 'disabled');
            var update_obj = [];
            var timer;
            var canEdit = false;
            var obj = {
                Init: function () {
                    $('.loader').show();
                    obj.Getdata();                    

                    $('#navbtnsave').addClass('disabled');

                    $(".savetbox").blur(function () {
                        var value = this.value;
                        var id = this.id;

                        let obj = update_obj.find(a => a.id == id);
                        if (obj) {
                            obj.value = value;
                        } else {
                            obj = {
                                id: id,
                                value: value
                            };
                            update_obj.push(obj);
                        }

                        if (!$('#navbtnsave').hasClass('syncItems')) {
                            $('#navbtnsave').addClass('syncItems');
                            $('#navbtnsave').removeClass('disabled');
                            $('#navbtnsave>i').addClass('syncItems');
                        }
                        
                    });

                    obj.EnableDisableForm();

                    $('.savetbox').prop('disabled', 'disabled');

                    $('#navbtnsave').click(function () {
                        $('.loader').show();
                        obj.save(update_obj);
                    });

                    $('#navbtnrefresh').click(function () {
                        $('.loader').show();
                        obj.Getdata();
                        $('#navbtnsave').removeClass('syncItems');
                        $('#navbtnsave>i').removeClass('syncItems');
                        $('#navbtnsave').addClass('disabled');
                    });
                },
                EnableDisableForm: function () {
                     $('#enableBtn').change(function () {
                         var value = $(this).is(':checked');
                         if (value) {
                             $('.savetbox').prop('disabled', '');
                         } else {
                            $('.savetbox').prop('disabled', 'disabled');
                         }
                     });

                    $('#editBtn').change(function () {
                        canEdit = $(this).is(':checked');
                        if (!canEdit) {
                            $('#enableBtn').parent().parent().addClass('disabled');
                            $('#enableBtn').prop('checked', false);
                            $('#enableBtn').parent().removeClass('active');
                            $('.savetbox').prop('disabled', 'disabled');
                        } else {
                            $('#enableBtn').parent().parent().removeClass('disabled');
                        }
                    });
                },
                getTableInfo: function () {
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "Ref_Farm_Financial_Rating.aspx/GetTableInfo",
                        data: JSON.stringify({  }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            var data = JSON.parse(res.d);

                            var table_info = data.Table_Info[0];
                            $('#txtTableName').html(table_info.Table_Name);
                            $('#txtLastUpdated').html(table_info.Last_Updated);
                            $('#txtRecordsCount').html(table_info.Records_Count);

                            if (data.CanEdit) {
                                $('#editControl').removeClass('disabled');
                            } else {
                                $('#editControl').addClass('disabled');
                                $('#enableControl').addClass('disabled');
                            }
                            
                        },
                        failure: function (err) {
                            console.log(err);
                        }
                    });
                },
                Getdata: function () {
                    $.ajax({
                        type: "POST",
                        url: "Ref_Farm_Financial_Rating.aspx/GetBRWDetails",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var objdata = JSON.parse(data.d);
                            for (var i = 0; i < objdata.length; i++) {
                                $('#' + objdata[i].Discount_Key).val(objdata[i].Discount_Value);
                            }
                            obj.getTableInfo();
                            $('.loader').hide();
                        },
                        failure: function (response) {
                        }
                    });
                },
                save: function (data) {
                    $.ajax({
                        type: "POST",
                        url: "Ref_Farm_Financial_Rating.aspx/UpdateData",
                        data: JSON.stringify({ financial_data: data }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            toastr.success("Saved Sucessful");
                            $('#navbtnsave').removeClass('syncItems');
                            $('#navbtnsave>i').removeClass('syncItems');
                            if (timer) { clearTimeout(timer); }
                            timer = setTimeout(function () {
                                location.reload();
                            }, 1000);
                        },
                        failure: function (response) {
                            var val = response.d;
                            toastr.warning(val);
                            $('.loader').hide();
                        }
                    });
                }
            }
            obj.Init();
        });

        $('#navbtnadd').addClass('disabled');
        $('#navbtncolumns').addClass('disabled');
        $('#navbtndownload').click(function () {
            exportTableToExcel('ffrtable','Borrower Farm Financial Ratio')
        });
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" runat="Server">
    
</asp:Content>

