﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_Ref_Farm_Financial_Rating : BasePage
{
    public admin_Ref_Farm_Financial_Rating()
    {
        Table_Name = "Ref_Farm_Financial_Rating";
    }

    [WebMethod]
    public static string GetTableInfo()
    {
        Count_Query = @"DECLARE @Count int; Select @Count = 9;";
        return Get_Table_Info();
    }

    [WebMethod]
    public static string GetBRWDetails()
    {
        string sql_qry = "select * from Ref_discounts where Disc_Group_Code = 'BRW'";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static void UpdateKeysDetails(string VValue, string VKey)
    {
        string qry = "";
        qry = "select * from Ref_discounts where Discount_key='" + VKey + "'";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(qry, dbKey);
        if (dt != null && dt.Rows.Count > 0)
        {
       //     Discount_key,Discount_Value
               qry = "update Ref_discounts set Discount_Value='" + VValue + "' where Discount_key='" + VKey + "'";
        }
        gen_db_utils.gp_sql_execute(qry, dbKey);

        gen_db_utils.Update_Table_Audit_Trail("Ref_Farm_Financial_Rating", userid, dbKey);
    }

    [WebMethod]
    public static void UpdateData(dynamic financial_data)
    {
        string qry = string.Empty;

        foreach (var item in financial_data)
        {
            qry += "update Ref_discounts set Discount_Value = '" + item["value"] + "' where Discount_key = '" + item["id"] + "'"; ;
            qry += Environment.NewLine;
        }
        
        if(!string.IsNullOrEmpty(qry))
        {
            gen_db_utils.gp_sql_execute(qry, dbKey);
            Update_Table_Audit();
        }
    }
}
