﻿using Newtonsoft.Json;
using System;
using System.Data;
using System.Web.Services;

public partial class admin_Ref_Farm_Financial_Rating_New : BasePage
{
    public admin_Ref_Farm_Financial_Rating_New()
    {
        Table_Name = "Ref_Farm_Financial_Rating";
    }

    [WebMethod]
    public static string GetTableInfo()
    {
        return Get_Table_Info();
    }

    [WebMethod]
    public static string LoadTable()
    {
        string sql_qry = @"SELECT *, 0 as [Actionstatus] from Ref_Farm_Financial_Rating";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static void SaveTable(dynamic ListItems)
    {
        foreach (var item in ListItems)
        {
            string qry = string.Empty;
            int val = Convert.ToInt32(item["Actionstatus"]);

            if (val == 1)
            {
                qry = @"INSERT INTO [dbo].[Ref_Farm_Financial_Rating]
                               ([Farm_Financial_Code]
                               ,[Farm_Financial_Name]
                               ,[Owned_Strong]
                               ,[Owned_Stable]
                               ,[Leased_Strong]
                               ,[Leased_Stable]
                               ,[Weight]
                               ,[Status])
                         VALUES
                               (
                                    '" + TrimSQL(item["Farm_Financial_Code"]) + @"'" +
                                   ",'" + TrimSQL(item["Farm_Financial_Name"]) + @"'" +
                                   "," + Get_ID(item["Owned_Strong"]) + @"" +
                                   "," + Get_ID(item["Owned_Stable"]) + @"" +
                                   "," + Get_ID(item["Leased_Strong"]) + @"" +
                                   "," + Get_ID(item["Leased_Stable"]) + @"" +
                                   "," + Get_ID(item["Weight"]) + @"" +
                                   ",'" + item["Status"] + @"'
                                )  ";

                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 2)
            {
                qry = @"UPDATE [dbo].[Ref_Farm_Financial_Rating]
                           SET  [Farm_Financial_Code] = '" + TrimSQL(item["Farm_Financial_Code"]) + @"' " +
                              ",[Farm_Financial_Name] = '" + TrimSQL(item["Farm_Financial_Name"]) + @"' " +
                              ",[Owned_Strong] = " + Get_ID(item["Owned_Strong"]) + @" " +
                              ",[Owned_Stable] = " + Get_ID(item["Owned_Stable"]) + @" " +
                              ",[Leased_Strong] = " + Get_ID(item["Leased_Strong"]) + @" " +
                              ",[Leased_Stable] = " + Get_ID(item["Leased_Stable"]) + @" " +
                              ",[Weight] = " + Get_ID(item["Weight"]) + @" " +
                              ",[Status] = '" + item["Status"] + @"' " +
                         "WHERE Farm_Financial_Rating_ID = '" + item["Farm_Financial_Rating_ID"] + "' ";

                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 3)
            {
                qry = "DELETE FROM Ref_Farm_Financial_Rating  where Farm_Financial_Rating_ID = '" + item["Farm_Financial_Rating_ID"] + "' ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
        }

        if (ListItems != null)
        {
            Update_Table_Audit();
        }
    }
}