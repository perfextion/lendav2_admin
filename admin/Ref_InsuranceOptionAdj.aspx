﻿<%@ Page Title="Insurance Option Adj" Language="C#" AutoEventWireup="true" MasterPageFile="~/gp_Master.master" CodeFile="Ref_InsuranceOptionAdj.aspx.cs" Inherits="admin_Ref_InsuranceOptionAdj" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        #content {
            padding: 14px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="ref-ins-option-adj reference-table">
        <div class="row switch-controls">
            <div class="switch-control disabled" id="editControl">
                <label for="editBtn">Edit</label>
                <div class="toggle-btn small">            
                    <input type="checkbox" id="editBtn" class="cb-value" />
                    <span class="round-btn"></span>
                </div>
            </div>
            <div class="switch-control disabled" id="enableControl">
                <label for="enableBtn">Enable</label>
                <div class="toggle-btn small">            
                    <input type="checkbox" id="enableBtn" class="cb-value" />
                    <span class="round-btn"></span>
                </div>
            </div>
        </div>
        <div class="loader"></div>
        <div class="row main-content">
            <div>
                <table id="jqGrid"></table>
                <div id="jqGridPager"></div>
            </div>
        </div>
    </div>

    <br />
    <asp:HiddenField ID="OptionAdjKey" runat="server" />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <script type="text/javascript">  
        var DeleteRows = [];
        var timer;
        var lastSelection;
        var ddlval;
        var canEdit = false;
        $.urlParam = function (name) {
            var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
            if (results == null) {
                return null;
            }
            return decodeURI(results[1]) || 0;
        }
        $(function () {
            var obj = {
                Init: function () {
                    if ($('#ContentPlaceHolder1_OptionAdjKey').val() != '') {
                        $('#txtSearch').val($('#ContentPlaceHolder1_OptionAdjKey').val());
                        var DiscGroupCode = $('#ContentPlaceHolder1_OptionAdjKey').val();
                    }
                    $('.loader').show();
                    obj.bindgrid();

                    $("#navbtnadd").click(function () {
                        obj.add();
                    });

                    $("#navbtnrefresh").click(function () {
                        $('.loader').show();
                        lastSelection = null;
                        obj.bindgrid();
                    });

                    $("#navbtndownload").click(function () {
                        $("#jqGrid").jqGrid("exportToExcel", {
                            includeLabels: true,
                            includeGroupHeader: true,
                            includeFooter: true,
                            fileName: "RefOptionAdjustments.xlsx",
                            maxlength: 40 // maxlength for visible string data 
                        })
                    });

                    $("#navbtnsave").click(function () {
                        obj.save();
                    });

                    $('#navbtncolumns').click(function () {
                        $("#jqGrid").columnChooser({});
                    });

                    $("#txtSearchBar").on("keyup", function () {
                        var self = this;
                        obj.search(self);
                    });

                    $('#editBtn').change(function () {
                        canEdit = $(this).is(':checked');
                        if (!canEdit) {
                            if (lastSelection != null) {
                                var grid = $("#jqGrid");
                                grid.jqGrid('saveRow', lastSelection);
                                grid.jqGrid('restoreRow', lastSelection);
                                lastSelection = null;
                            }
                        }
                    });
                },
                getTableInfo: function () {
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "Ref_InsuranceOptionAdj.aspx/GetTableInfo",
                        data: JSON.stringify({ }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            var data = JSON.parse(res.d);

                            var table_info = data.Table_Info[0];
                            $('#txtTableName').html(table_info.Table_Name);
                            $('#txtLastUpdated').html(table_info.Last_Updated);
                            $('#txtRecordsCount').html(table_info.Records_Count);

                            if (data.CanEdit) {
                                $('#editControl').removeClass('disabled');
                            } else {
                                $('#editControl').addClass('disabled');
                            }

                            $('#enableControl').addClass('disabled');
                            
                        }
                    });
                },
                bindgrid: function () {
                    var optionkey = $("#txtSearchBar").val();
                    $.ajax({
                        type: "POST",
                        url: "Ref_InsuranceOptionAdj.aspx/BindReferenceAdjustmentDetails",
                        async: false,
                        data: JSON.stringify({ Optionkey: optionkey }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                             var resData = JSON.parse(data.d);
                            $('.loader').show();
                            for (var i = 0; i < resData.length; i++) {
                                resData[i].rowid = i + 1;
                            }
                            obj.loadgrid(resData);
                            obj.getTableInfo();
                            $('.loader').hide();
                        }
                    });
                   
                },
                loadgrid: function (data) {
                    $.jgrid.gridUnload("#jqGrid");
                    $("#jqGrid").jqGrid({
                        datatype: "local",
                        styleUI: 'Bootstrap',
                        data: data,
                        colModel: [
                            { label: 'rowid', name: 'rowid', width: 75, editable: false, key: true, hidden: true, hidedlg: true },
                            {
                                label: 'Option Adj ID', align: 'center', name: 'Id', width: 120, editable: false
                            },
                            {
                                label: 'Crop Code', align: 'center', name: 'Crop_Code', width: 120, editable: true
                            },
                            {
                                label: 'Ins Plan', align: 'center', name: 'Ins_Plan', width: 120, editable: true
                            },
                            {
                                label: 'Ins Plan Type', align: 'center', name: 'Ins_Plan_Type', width: 120, editable: true
                            },
                            {
                                label: 'Option Type Code', align: 'center', name: 'Option_Type_Code', width: 120, editable: true
                            },
                            {
                                label: 'Option Adj Key', align: 'left', name: 'Option_Adj_Key', width: 240, editable: true
                            },
                            {
                                label: 'Option Adj Price', align:'right', classes: 'right', name: 'Option_Adj_Price', width: 150, editable: true, editoptions: {
                                    maxlength: 10, dataEvents: [
                                        {
                                            type: 'keypress', fn: function (e) {
                                                var charCode = (e.which) ? e.which : e.keyCode;
                                                if (((charCode != 46 || (charCode == 46 && $(this).val() == '')) ||
                                                    $(this).val().indexOf('.') != -1) && (charCode < 48 || charCode > 57)) {
                                                    return false;
                                                }
                                            }
                                        }
                                    ]
                                },
                                formatter: 'priceFormatter'
                            },
                            {
                                label: 'Option Adj Percent', align: 'center', classes: 'center', name: 'Option_Adj_Percent', width: 150, editable: true, editoptions: {
                                    maxlength: 10, dataEvents: [
                                        {
                                            type: 'keypress', fn: function (e) {
                                                var charCode = (e.which) ? e.which : e.keyCode;
                                                if (((charCode != 46 || (charCode == 46 && $(this).val() == '')) ||
                                                    $(this).val().indexOf('.') != -1) && (charCode < 48 || charCode > 57)) {
                                                    return false;
                                                }
                                            }
                                        }
                                    ]
                                },
                                formatter: 'percentFormatter'
                            },
                            {
                                label: 'Status',
                                align: 'center',
                                editoptions: {
                                    maxlength: 1,
                                    dataEvents: [
                                        {
                                            type: 'keypress',
                                            fn: function (e) {
                                                return NumberOnly(e, this, false);
                                            }
                                        }
                                    ]
                                },
                                name: 'Status',
                                width: 90,
                                editable: true
                            },
                            { label: 'Actionstatus', name: 'Actionstatus', width: 70, editable: false, hidden: true, hidedlg: true  },
                            { label: '', name: '', width: 60, align: 'center', formatter: obj.deleteLink, hidedlg: true  },
                        ],
                        viewrecords: true,
                        loadonce: true,
                        restoreAfterSelect: false,
                        saveAfterSelect: true,
                        sortable: true,
                        onSelectRow: obj.edit,
                        onPaging: obj.changePage,
                        height: 'auto',
                        rowNum: 100,
                        pager: "#jqGridPager"
                    });
                },
                add: function () {
                    var grid = $("#jqGrid");
                    var rowsperPage = parseInt(grid.getGridParam('rowNum'));
                    var gridlength = grid.jqGrid('getGridParam', 'data').length;
                    var curpage = parseInt($(".ui-pg-input").val());
                    var totPages = Math.ceil(gridlength / rowsperPage);
                    //if (rowsperPage * curpage == gridlength) {
                    if (rowsperPage * totPages == gridlength) {
                        var id = $('.inline-edit-cell').parent().parent().prop('id');
                        grid.jqGrid('saveRow', id);
                        var row = obj.newrow();
                        // var newRowId = grid.jqGrid('getGridParam', 'data').length + 1;
                        var newRowId = row.rowid;
                        grid.jqGrid('addRowData', newRowId, row, "first");
                        grid.trigger('reloadGrid');
                        lastSelection = newRowId;
                        grid.jqGrid('saveRow', lastSelection);
                        grid.jqGrid('restoreRow', lastSelection);
                        $('.glyphicon-step-forward').trigger('click');

                    } else {

                        $('.glyphicon-step-forward').trigger('click');
                        var row = obj.newrow();
                        // var newRowId = grid.jqGrid('getGridParam', 'data').length + 1;
                        var newRowId = row.rowid;
                        grid.jqGrid('addRowData', newRowId, row, "first");
                    }

                    lastSelection = newRowId;
                    grid.jqGrid('saveRow', lastSelection);
                    grid.jqGrid('restoreRow', lastSelection);

                    var eid = $('.inline-edit-cell').parent().parent().prop('id')
                    grid.jqGrid('saveRow', eid);
                    grid.jqGrid('restoreRow', eid);
                    grid.jqGrid('editRow', newRowId);
                    $('#navbtnsave').addClass('syncItems');
                    $('#navbtnsave>i').addClass('syncItems');
                    
                },
                edit: function (id) { 

                    if (id && id !== lastSelection && canEdit) {
                        var grid = $("#jqGrid");
                        grid.jqGrid('saveRow', lastSelection);
                        grid.jqGrid('restoreRow', lastSelection);
                        var row = grid.jqGrid('getRowData', lastSelection);

                        if (!jQuery.isEmptyObject(row)) {
                            var dataobj = jQuery("#jqGrid").jqGrid('getGridParam', 'data');
                            index = dataobj.findIndex(x => x.rowid == row.rowid);
                            if (row.rowid > 0 && row.Actionstatus != 1) {
                                row.Actionstatus = 2;
                                grid.jqGrid('getGridParam', 'data')[index] = row;
                            }
                        }

                        grid.jqGrid('editRow', id);
                        lastSelection = id;

                        $('#navbtnsave').addClass('syncItems');
                        $('#navbtnsave>i').addClass('syncItems');
                    }
                },
                newrow: function () {
                    var newid = 0;
                    var grid = $("#jqGrid");
                    var length = jQuery("#jqGrid").jqGrid('getGridParam', 'records');
                    var dataobj = grid.jqGrid('getGridParam', 'data');
                    var dataobj = grid.jqGrid('getGridParam', 'data');

                    IDs = dataobj.map(function (e) { return e.rowid });
                    if (IDs.length > 0) {
                        newid = IDs.reduce(function (a, b) { return Math.max(a, b); });
                    }
                    var row = {
                        Actionstatus: 1,
                        rowid: 0,
                        Option_Adj_Key: '',
                        Option_Adj_Price: 0,
                        Option_Adj_Percent:0
                    };
                    return row;
                },
                save: function () {
                    $('.loader').show();
                    var grid = $("#jqGrid");
                    var allrows = [];
                    var id = $('.inline-edit-cell').parent().parent().prop('id');
                    grid.jqGrid('saveRow', id);
                    var rowData = grid.jqGrid('getRowData', id);
                    if (rowData.rowid > 0) {
                        rowData.Actionstatus = 2;
                        grid.jqGrid('getGridParam', 'data')[id] = rowData;
                    }
                    for (var i = 0; i < DeleteRows.length; i++) {
                        DeleteRows[i].Actionstatus = 3
                        allrows.push(DeleteRows[i]);
                    }
                    var dataobj = grid.jqGrid('getGridParam', 'data');
                    var newRows = $.grep(dataobj, function (e) {
                        if (e != undefined) {
                            return e.Actionstatus == 1 || e.Actionstatus == 2
                        }
                    });
                    //newRows = $.distinct(newRows);
                    for (var i = 0; i < newRows.length; i++) {
                        allrows.push(newRows[i]);
                    }
                    $.ajax({
                        type: "POST",
                        url: "Ref_InsuranceOptionAdj.aspx/SaveReferenceAdjustmentDetails",
                        data: JSON.stringify({ lst: allrows }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            //location.reload();
                            //$('#ddldiskgroupcode').val(ddlval).trigger('change');
                            toastr.success("Saved Sucessful");
                            if (timer) { clearTimeout(timer); }
                            timer = setTimeout(function () {
                                window.location.href = "Ref_InsuranceOptionAdj.aspx?ddlval=" + ddlval;
                            }, 2000);
                            $('#navbtnsave').removeClass('syncItems');
                            $('#navbtnsave>i').removeClass('syncItems');
                        },
                        failure: function (response) {
                            var val = console.log(response.d);
                            toastr.warning(val);
                        }
                    });
                },
                search: function (self) {
                    if (timer) { clearTimeout(timer); }
                    timer = setTimeout(function () {
                        obj.changePage();
                        $("#jqGrid").jqGrid('filterInput', self.value);
                        var length = jQuery("#jqGrid").jqGrid('getGridParam', 'records');
                        $('#txtRecordsCount').html('Records Count: ' + length);
                    }, 0);
                },
                filedSearch: function () {
                    var postData = $("#jqGrid").jqGrid("getGridParam", "postData"),
                        colModel = $("#jqGrid").jqGrid("getGridParam", "colModel"),
                        rules = [],
                        searchText = $("#ddldiskgroupcode :selected").text();
                    if (searchText != "Select") {
                        l = colModel.length;
                        if (searchText != "") {
                            rules.push({ field: "Disc_Group_Code", op: "eq", data: searchText });
                        }
                        postData.filters = JSON.stringify({
                            groupOp: "OR",
                            rules: rules
                        });

                        $("#jqGrid").jqGrid("setGridParam", { search: true });
                        $("#jqGrid").trigger("reloadGrid", [{ page: 1, current: true }]);
                        // return false;
                    }
                    else {
                        obj.bindgrid();
                    }
                },
                changePage: function () {
                    lastSelection = "";
                    var grid = $("#jqGrid");
                    var id = $('.inline-edit-cell').parent().parent().prop('id');
                    if (id != undefined) {
                        grid.jqGrid('saveRow', id);
                        grid.jqGrid('restoreRow', id);
                        var row = grid.jqGrid('getRowData', id);
                        if (!jQuery.isEmptyObject(row)) {
                            var dataobj = jQuery("#jqGrid").jqGrid('getGridParam', 'data');
                            index = dataobj.findIndex(x => x.rowid == row.rowid);
                            if (row.rowid > 0 && row.Actionstatus != 1) {
                                row.Actionstatus = 2;
                                grid.jqGrid('getGridParam', 'data')[index] = row;
                            }
                        }
                    }
                },
                deleteLink: function (cellValue, options, rowdata, action) {
                    return "<a href='javascript:deleteRecord(" + options.rowId + ")' class='glyphicon glyphicon-trash' style='color:red'></a>";
                }
            }
            obj.Init();
        });
        function deleteRecord(id) {
            if (canEdit) {
                var grid = $('#jqGrid');
                var result = confirm("Are you sure you Want to delete?");
                if (result == true) {
                    var ediId = $('.inline-edit-cell').parent().parent().prop('id');
                    grid.jqGrid('saveRow', ediId);
                    var rowData = grid.jqGrid('getRowData', ediId);
                    if (rowData.rowid > 0 && rowData.Actionstatus != 1) {
                        rowData.Actionstatus = 2;
                        grid.jqGrid('getGridParam', 'data')[ediId - 1] = rowData;
                    }
                    var row = grid.jqGrid('getRowData', id);
                    grid.jqGrid('delRowData', id);
                    if (row.Actionstatus != 1) {
                        DeleteRows.push(row);
                    }
                    var data = jQuery("#jqGrid").jqGrid('getGridParam', 'data');
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].Discount_Id == 0) {
                            data[i].rowid = i + 1;
                            data[i].id = i + 1;
                        } else
                            data[i].rowid = i + 1;
                    }
                    var curpage = parseInt($(".ui-pg-input").val());
                    jQuery('#jqGrid').jqGrid('clearGridData');
                    jQuery('#jqGrid').jqGrid('setGridParam', { data: data });
                    $("#jqGrid").trigger("reloadGrid", [{ page: curpage }]);
                    $("#jqGrid").trigger("reloadGrid", [{ page: curpage }]);
                    lastSelection = id;
                    $('#navbtnsave').addClass('syncItems');
                    $('#navbtnsave>i').addClass('syncItems');
                }
            }
        }

        $(".navbar a.disabled").click(function () {
            return false;
        })
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" runat="Server">
</asp:Content>

