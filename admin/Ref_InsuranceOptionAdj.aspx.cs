﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;

public partial class admin_Ref_InsuranceOptionAdj : BasePage
{
    public admin_Ref_InsuranceOptionAdj()
    {
        Table_Name = "Ref_Option_Adj_Lookup";
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["OptionAdjKey"] != null)
        {
            OptionAdjKey.Value = Session["OptionAdjKey"].ToString();
        }

        base.Page_Load(sender, e);
    }

    [WebMethod]
    public static string GetTableInfo()
    {
        return Get_Table_Info();
    }    

    [WebMethod]
    public static string BindReferenceAdjustmentDetails(string Optionkey)
    {
        string stQry = string.Empty;
        HttpContext.Current.Session["OptionAdjKey"] = Optionkey;

        stQry = @" SELECT * FROM Ref_Option_Adj_Lookup ";

        if (Optionkey != "")
        {
            stQry += " Where Option_Adj_Key like '%" + Optionkey + "%' ";
        }

        DataTable dt = gen_db_utils.gp_sql_get_datatable(stQry, dbKey);
        return JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static string SearchOptionkey(string Optionkey)
    {

        string sql_qry = @" SELECT * FROM Ref_Option_Adj_Lookup where Option_Adj_Key='" + Optionkey + "'";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static void SaveReferenceAdjustmentDetails(dynamic lst)
    {
        foreach (var item in lst)
        {
            string qry = "";
            int val = Convert.ToInt32(item["Actionstatus"]);
            if (val == 1)
            {
                qry = @"INSERT INTO [dbo].[Ref_Option_Adj_Lookup]
                               (
                                    [Crop_Code]
                                   ,[Ins_Plan]
                                   ,[Ins_Plan_Type]
                                   ,[Option_Type_Code]
                                   ,[Option_Adj_Key]
                                   ,[Option_Adj_Price]
                                   ,[Option_Adj_Percent]
                                   ,[Status]
                                )
                         VALUES
                               (
                                   '" + item["Crop_Code"] + "'," + @" 
                                   '" + item["Ins_Plan"] + "'," + @" 
                                   '" + item["Ins_Plan_Type"] + "'," + @" 
                                   '" + item["Option_Type_Code"] + "'," + @" 
                                   '" + item["Option_Adj_Key"] + "'," + @" 
                                   '" + item["Option_Adj_Price"] + "'," + @"  
                                   '" + item["Option_Adj_Percent"] + "'," + @" 
                                   '" + item["Status"] + "'" +
                               ");";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 2)
            {
                qry = @"UPDATE [dbo].[Ref_Option_Adj_Lookup]
                       SET [Crop_Code] = '" + item["Crop_Code"] + @"'
                          ,[Ins_Plan] = '" + item["Ins_Plan"] + @"'
                          ,[Ins_Plan_Type] = '" + item["Ins_Plan_Type"] + @"'
                          ,[Option_Type_Code] = '" + item["Option_Type_Code"] + @"'
                          ,[Option_Adj_Key] = '" + item["Option_Adj_Key"] + @"'
                          ,[Option_Adj_Price] = '"+ item["Option_Adj_Price"]+ @"'
                          ,[Option_Adj_Percent]  = '" + item["Option_Adj_Percent"] + @"'
                          ,[Status]  ='" + item["Status"] + @"'
                     WHERE Id = " +item["Id"];
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 3)
            {
                qry = "DELETE FROM Ref_Option_Adj_Lookup where id = " + item["Id"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
        }

        gen_db_utils.Update_Table_Audit_Trail("Ref_Option_Adj_Lookup", userid, dbKey);
    }
}