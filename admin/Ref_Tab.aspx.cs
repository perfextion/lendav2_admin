﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
public partial class admin_Ref_Tab : BasePage
{
    public admin_Ref_Tab()
    {
        Table_Name = "Ref_Tab";
    }

    [WebMethod]
    public static string GetTableInfo()
    {
        return Get_Table_Info();
    }

    [WebMethod]
    public static string GetReftab()
    {
        string sql_qry = "SELECT 0 as Actionstatus,tab_id,Tab_Name,Tab_Code,Sort_Order,Status FROM Ref_Tab ";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
    }


    [WebMethod]
    public static void SaveTab(dynamic lst)
    {
        foreach (var item in lst)
        {
            string qry = "";
            int val = Convert.ToInt32(item["Actionstatus"]);
            if (val == 1)
            {
                qry = "INSERT INTO Ref_Tab(Tab_Name,Tab_Code,Sort_Order,Status)" +
                    " VALUES('" + item["Tab_Name"].Replace("'", "''") + "','" + item["Tab_Code"].Replace("'", "''") + "'," +
                    "'" + item["Sort_Order"].Replace("'", "''") + "','" + item["Status"].Replace("'", "''") + "')";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 2)
            {
                qry = "UPDATE Ref_Tab SET Tab_Name = '" + item["Tab_Name"].Replace("'", "''") + "' ,Tab_Code = '" + item["Tab_Code"].Replace("'", "''") + "',Sort_Order = '" + item["Sort_Order"].Replace("'", "''") + "',Status = '" + item["Status"].Replace("'", "''") + "' where tab_id=" + item["tab_id"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 3)
            {
                qry = "DELETE FROM Ref_Tab  where tab_id=" + item["tab_id"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
        }

        gen_db_utils.Update_Table_Audit_Trail("Ref_Tab", userid, dbKey);
    }
}