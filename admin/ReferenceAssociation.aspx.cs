﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Text;
using System.Web.UI.WebControls;


public partial class admin_ReferenceAssociation : BasePage
{
    static Globalvar.Preferred_Contacts precont;
    string Field_ID;

    public admin_ReferenceAssociation()
    {
        Table_Name = "Ref_Association";
    }

    protected void Page_Load(object sender, EventArgs e)
    {       
        if (Session["REFASSOCAssocTypeCode"] != null)
        {
            REFASSOCAssocTypeCode.Value = Session["REFASSOCAssocTypeCode"].ToString();
        }

        base.Page_Load(sender, e);
    }

    [WebMethod]
    public static string GetTableInfo()
    {
        Count_Query = @"DECLARE @Count int;
                        Select @Count = Count(*)  From Ref_Association";
        return Get_Table_Info();
    }

    [WebMethod]
    public static string LoadTable(string AssocTypeCode)
    {

        HttpContext.Current.Session["REFASSOCAssocTypeCode"] = AssocTypeCode;
        string strQry = "";

        strQry = "SELECT Ref_Association_ID,Assoc_Type_Code,Assoc_Name,Assoc_Code,Contact,Location,Phone,Email,Referred_Type,Response, " +
                 "Preferred_Contact_Ind,Assoc_Status,IsOther,Date_Added,Status, Validation_ID, Exception_ID FROM dbo.Ref_Association ";
        if (!string.IsNullOrEmpty(AssocTypeCode))
            strQry += "Where Assoc_Type_Code='" + AssocTypeCode + "'";

        DataTable dt = gen_db_utils.gp_sql_get_datatable(strQry, dbKey);

        return JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static string GetRefAssociationByID(string ref_asso_id)
    {
        string sql_qry = "SELECT Ref_Association_ID,Assoc_Type_Code,Assoc_Name,Assoc_Code,Contact,Location,Phone,Email,Referred_Type,Response, " +
                 "Preferred_Contact_Ind,Assoc_Status,IsOther,Date_Added,Status, Validation_ID, Exception_ID FROM dbo.Ref_Association  where Ref_Association_ID='" + ref_asso_id + "'";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);

    }

    [WebMethod]
    public static List<string> GetAssocTypeCode()
    {
        string sql_qry = "select Distinct(Assoc_Type_Code) from Ref_Association";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        List<string> emp = new List<string>();
        return (from DataRow row in dt.Rows
                select row["Assoc_Type_Code"].ToString()
               ).ToList();
    }
   

    [WebMethod]
    public static void UpdateAssocDetails(dynamic lst)
    {
        ReferenceAssociation objmanag = new ReferenceAssociation();
        foreach (var item in lst)
        {
            string json = JsonConvert.SerializeObject(item);
            ReferenceAssociation Ref = JsonConvert.DeserializeObject<ReferenceAssociation>(json);
            string qry = "";
            if (Ref.Actionstatus == "3")
            {
                qry = "DELETE FROM Ref_Association  where Ref_Association_ID = '" + item["Ref_Association_ID"] + "' ";

            }
            else
            if (Ref.Ref_Association_ID != string.Empty)
            {
                qry = "update Ref_Association set " +
                        "Assoc_Name ='" + Ref.Assoc_Name + "'," +
                        "Contact ='" + Ref.Contact + "'," +
                        "Location ='" + Ref.Location + "'," +
                        "Phone ='" + Ref.Phone + "'," +
                        "Email ='" + Ref.Email + "'," +
                        "Preferred_Contact_Ind ='" + Ref.PreferredContactInd + "'," +
                        "Validation_ID =" + Get_ID(Ref.Validation_ID) + "," +
                        "Exception_ID =" + Get_ID(Ref.Exception_ID) + "," +
                        "Assoc_Status ='" + Ref.Status + "'," +
                        "Status ='" + Ref.Status + "'  " +
                    "where Ref_Association_ID = '" + Ref.Ref_Association_ID + "'";
            }
            else
            {
                qry = "insert into Ref_Association " +
                    "(" +
                        "Assoc_Type_Code," +
                        "Assoc_Name," +
                        "Contact," +
                        "Location," +
                        "Phone," +
                        "Email," +
                        "Preferred_Contact_Ind," +
                        "Assoc_Status," +
                        "Status," +
                        "Validation_ID," +
                        "Exception_ID" +
                    ") " +
                    "values(" +
                        "'" + Ref.Assoc_Type_Code + "'," +
                        "'" + Ref.Assoc_Name + "'," +
                        "'" + Ref.Contact + "'," +
                        "'" + Ref.Location + "'," +
                        "'" + Ref.Phone + "'," +
                        "'" + Ref.Email + "'," +
                        "'" + Ref.PreferredContactInd + "'," +
                        "'" + Ref.Status +  "'," +
                        "'" + Ref.Status + "'," +
                        "" + Get_ID(Ref.Validation_ID) + "," +
                        "" + Get_ID(Ref.Exception_ID) + "" +
                    ")";
            }
            gen_db_utils.gp_sql_execute(qry, dbKey);
        }

        gen_db_utils.Update_Table_Audit_Trail("Ref_Association", userid, dbKey);

    }

    public class ReferenceAssociation
    {
        public string Ref_Association_ID { set; get; }
        //public string ROQID { set; get; }
        public string Assoc_Type_Code { set; get; }
        public string Assoc_Name { set; get; }
        public string Contact { set; get; }
        public string Location { set; get; }
        public string Phone { set; get; }
        public string Email { set; get; }
        public string PreferredContactInd { set; get; }
        public string Validation_ID { get; set; }
        public string Exception_ID { get; set; }
        public string Status { set; get; }
        public string Actionstatus { set; get; }
        public ReferenceAssociation()
        {
            //
            // TODO: Add constructor logic here
            //
        }

    }

}