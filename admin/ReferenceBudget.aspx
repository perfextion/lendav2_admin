﻿<%@ Page Title="Budget Default" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="ReferenceBudget.aspx.cs" Inherits="admin_ReferenceBudget" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .fc-head-container thead tr, .table thead tr {
            height: 50px;
        }

        .ddlcolor {
            background-color: #C3BEBC;
        }

        .right {
            padding-right: 14px !important;
        }

        #content {
            padding: 14px !important;
        }

        .ddl-menu {
            float: left;
            margin-top: 10px;
            padding-left: 35px;
        }

        .ref-budget-default .ddl-menu .col-md-3:first-child {
            width: 300px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="ref-budget-default reference-table">
        <div class="row">
            <div class="ddl-menu col-md-12">
                <div class="form-inline col-md-3">
                    <label for="ddlofficename">Office:</label>
                    <select id="ddlofficename" class="form-control" style="width: 200px">
                        <option value="0_0">0:ARM Default</option>
                    </select>                    
                </div>
                <div class="form-inline col-md-3" style="padding: 0px">
                    <label style="margin-left:25px" for="ddlColNames">Crop Key:</label>
                    <select id="ddlColNames" class="form-control" style="width: 200px">
                    </select>
                </div>
            </div>
        </div>
        <div class="row switch-controls">
            <div class="switch-control disabled" id="editControl">
                <label for="editBtn">Edit</label>
                <div class="toggle-btn small">            
                    <input type="checkbox" id="editBtn" class="cb-value" />
                    <span class="round-btn"></span>
                </div>
            </div>
            <div class="switch-control disabled" id="enableControl">
                <label for="enableBtn">Enable</label>
                <div class="toggle-btn small">            
                    <input type="checkbox" id="enableBtn" class="cb-value" />
                    <span class="round-btn"></span>
                </div>
            </div>
        </div>
        <div class="loader"></div>
        <div class="row main-content" style="position: relative">
            <div id="truncateTable" style="position: absolute; top: -25px; display: none">
                <a href="javascript:truncatetable()" class='glyphicon glyphicon-trash' style='color:red'></a>
            </div>
            <div>
                <table id="jqGrid"></table>
                <div id="jqGridPager"></div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="cropfullkey" runat="server" />
    <asp:HiddenField ID="officeid" runat="server" />
    

    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Crop Key Into Budget</h4>
                </div>
                <div class="modal-body" style="margin-bottom: -10px;">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <label for="txtCropKey">Crop Key</label>
                                <input type="text" id="txtCropKey" class="form-control" name="Crop Key"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="col-md-12">
                        <div class="col-md-8">
                        </div>
                        <div class="col-md-4 text-right">
                            <div class="btn btn-primary" id="btnModalSave">Save</div>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br />
    <br />
    <br />
    <script type="text/javascript">  
        //var DeleteRows = [];
        var timer;
        var lastSelection;
        var DeleteRows = [];
        var canEdit = false;
        $(function () {
            var cropFillkeyList = {};
            var expenseList = {};
            var cropkeylist = {};
            var cropkey = [];
            $.urlParam = function (name) {
                var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
                if (results == null) {
                    return null;
                }
                return decodeURI(results[1]) || 0;
            }
            var obj = {
                Init: function () {
                    $('.loader').show();                    

                    if ($('#ContentPlaceHolder1_cropfullkey').val() != '') {
                        $('#ddlColNames').val($('#ContentPlaceHolder1_cropfullkey').val());
                        var cropfullkey = $('#ContentPlaceHolder1_cropfullkey').val();

                    }
                    if ($('#ContentPlaceHolder1_officeid').val() != '') {
                        $('#ddlofficename').val($('#ContentPlaceHolder1_officeid').val());
                        var officeid = $('#ContentPlaceHolder1_officeid').val();
                    }
                    obj.regionName();
                    obj.officename();
                    $.ajax({
                        type: "POST",
                        url: "ReferenceBudget.aspx/GetCropFullKeys",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            $.each(res.d, function (data, value) {
                                $("#ddlColNames").append($("<option></option>").val(data).html(value));
                            });
                            if ((cropfullkey || "") != "" && (officeid || "") != "") {
                                $('#ddlColNames').val(cropfullkey);
                                $('#ddlofficename').val(officeid);
                                obj.bindGrid();
                            }
                            else {
                                obj.bindGrid();
                            }
                        }
                    });

                    $("#navbtnadd").click(function () {
                        obj.addCropKey();
                    });

                    obj.getAllExpences();
                    obj.getAllcropkeys();

                    $('#ddlofficename').change(function () {
                        obj.bindGrid();
                    })

                    $("#navbtnsave").click(function () {
                        $('.loader').show();
                        obj.save();
                    });

                    $("#navbtnrefresh").click(function () {
                        $('.loader').show();
                        obj.bindGrid();
                    });

                    $('#navbtncolumns').click(function () {
                        $("#jqGrid").columnChooser({});
                    });

                    $('#btnModalSave').click(function () {
                        obj.saveCropKey();
                    });

                    $("#navbtndownload").click(function () {
                        $("#jqGrid").jqGrid("exportToExcel", {
                            includeLabels: true,
                            includeGroupHeader: true,
                            includeFooter: true,
                            fileName: "Budget Default.xlsx",
                            maxlength: 40, // maxlength for visible string data 
                            replaceStr: function _replStrFunc(v) {
                                return v.replace('&#160;', '');
                            }
                        }) 
                    });

                    $("#txtSearchBar").on("keyup", function () {
                        var self = this;
                        obj.search(self);
                    });

                    $('#editBtn').change(function () {
                        canEdit = $(this).is(':checked');
                        if (!canEdit) {
                            if (lastSelection != null) {
                                var grid = $("#jqGrid");
                                grid.jqGrid('saveRow', lastSelection);
                                grid.jqGrid('restoreRow', lastSelection);
                                lastSelection = null;
                            }
                            $('#navbtnupload').addClass('disabled');
                        } else {
                            $('#navbtnupload').removeClass('disabled');
                        }
                    });

                    $('a:not(#navbtnsave)').on("click", function (ev) {
                        var grid = $("#jqGrid");
                        var id = $('.inline-edit-cell').parent().parent().prop('id');
                        grid.jqGrid('saveRow', id);
                        var rowData = grid.jqGrid('getRowData', id);
                        if (rowData.Budget_Default_ID > 0) {
                            rowData.Actionstatus = 2;
                            grid.jqGrid('getGridParam', 'data')[id] = rowData;
                        }
                        var dataobj = grid.jqGrid('getGridParam', 'data');
                        var newRows = $.grep(dataobj, function (e) {
                            if (e != undefined) {
                                return e.Actionstatus == 2
                            }
                        });
                        if (newRows.length > 0) {
                            var result = confirm("Are you sure you Want to leave without Saving ?");
                            if (result == false) {
                                ev.preventDefault();
                            }
                        }

                    });
                    var previous;
                    $("#ddlColNames").on('focus', function () {
                        previous = this.value;
                    }).change(function () {
                        lastSelection = undefined;
                        var grid = $("#jqGrid");
                        var id = $('.inline-edit-cell').parent().parent().prop('id');
                        grid.jqGrid('saveRow', id);
                        var rowData = grid.jqGrid('getRowData', id);
                        if (rowData.Budget_Default_ID > 0) {
                            rowData.Actionstatus = 2;
                            grid.jqGrid('getGridParam', 'data')[id] = rowData;
                        }
                        var dataobj = grid.jqGrid('getGridParam', 'data');
                        var newRows = $.grep(dataobj, function (e) {
                            if (e != undefined) {
                                return e.Actionstatus == 2
                            }
                        });

                        if (newRows.length > 0) {
                            var result = confirm("Are you sure you Want to leave without Saving ?");
                            if (result == true) {
                                $('.loader').show();
                                obj.bindGrid();
                            } else {
                                $("#ddlColNames").val(previous)
                            }
                        } else {
                            $('.loader').show();
                            obj.bindGrid();
                        }
                    });
                },
                addCropKey: function () {
                    $('#divModelSave').show();
                    $('#txtCropKey').val('');
                    $('#myModal').modal('show');
                },
                saveCropKey: function () {
                    $('.loader').show();

                    var cropKey = $('#txtCropKey').val();

                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "ReferenceBudget.aspx/AddNewCropKey",
                        data: JSON.stringify({ Crop_Full_Key: cropKey }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            $('#myModal').modal('hide');
                            toastr.success("Saved Sucessful");

                            if (timer) { clearTimeout(timer); }
                            timer = setTimeout(function () {
                                location.reload();
                            }, 2000);
                        },
                        failure: function (e) {
                            $('.loader').hide();
                        }
                    });
                },
                getTableInfo: function () {
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "ReferenceBudget.aspx/GetTableInfo",
                        data: JSON.stringify({  }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            var data = JSON.parse(res.d);

                            var table_info = data.Table_Info[0];
                            $('#txtTableName').html(table_info.Table_Name);
                            $('#txtLastUpdated').html(table_info.Last_Updated);
                            $('#txtRecordsCount').html(table_info.Records_Count);

                            if (data.CanEdit) {
                                $('#editControl').removeClass('disabled');
                            } else {
                                $('#editControl').addClass('disabled');
                            }

                            $('#enableControl').addClass('disabled');
                            
                        }
                    });
                },
                loadgrid: function (data) {
                    $('.loader').show();
                    $.jgrid.gridUnload("#jqGrid");
                    $("#jqGrid").jqGrid({
                        datatype: "local",
                        styleUI: 'Bootstrap',
                        data: data,
                        colModel: [
                            { label: 'rowid', name: 'rowid', width: 70, editable: false, key: true, hidden: true, hidedlg: true },
                            { label: 'Budget ID', name: 'Budget_Default_ID', align: 'center', sorttype: 'number', width: 120, editable: false, key: false },
                            {
                                label: 'Expense ID', name: 'Budget_Expense_Type_ID', align: 'center', width: 120, editable: false
                            },
                            {
                                label: 'Expense Name', name: 'Budget_Expense_Name', width: 300, editable: false
                            },
                            { label: 'Crop Key', name: 'Crop_Full_Key', width: 180, editable: false, hidden: false },
                            { label: 'Region ID', name: 'Z_Region_ID', align: 'center', width: 120, editable: false },
                            { label: 'Region Name', name: 'Region_Name', align: 'center', width: 90, editable: false, hidden: true, hidedlg: true },
                            { label: 'Office ID', name: 'Z_Office_ID', align: 'center', width: 120, editable: false },
                            { label: 'Office Name', name: 'Office_Name', align: 'center', width: 90, editable: false, hidden: true, hidedlg: true },
                            {
                                label: 'Crop Practice ID', name: 'Crop_Practice_ID', width: 240, editable: false, hidden: true, edittype: "select", hidedlg: true , formatter: 'select',
                                editoptions: {
                                    value: cropkeylist,
                                    dataInit: function (elem) {
                                        setTimeout(function () {
                                            $(elem).change();
                                        }, 0);
                                    },
                                    dataEvents: [
                                        {
                                            type: 'change',
                                            fn: function (e) {

                                                var rowId = $('.inline-edit-cell').parent().parent().prop('id');
                                                $("#jqGrid").jqGrid("setCell", rowId, "Crop_Full_Key", $('#' + rowId + '_Crop_Practice_ID :selected').text());
                                            }
                                        }]
                                }
                            },
                            {
                                label: 'Budget Amt', name: 'ARM_Budget', width: 120, align: 'right', classes: 'right', editable: true,
                                editoptions: {
                                    maxlength: 10,
                                    dataEvents: [
                                        {
                                            type: 'keypress',
                                            fn: function (e) {
                                                return NumberOnly(e, this);
                                            }
                                        }
                                    ]
                                },
                                formatter: 'amountFormatter'
                            },
                            {
                                label: 'Financial Budget Code', name: 'Financial_Budget_Code', hidedlg: true, align: 'center', width: 90, hidden: true, editable: false,
                                editoptions: {
                                    maxlength: 20,
                                    dataEvents: [
                                        {
                                            type: 'keypress',
                                            fn: function (e) {
                                                return NumberOnly(e, this, false);
                                            }
                                        }
                                    ]
                                }
                            },
                            {
                                label: 'Status',
                                name: 'Status',
                                align: "center",
                                width: 90,
                                editable: true,
                                hidden: false,
                                editoptions: {
                                    maxlength: 1,
                                    dataEvents: [
                                        {
                                            type: 'keypress',
                                            fn: function (e) {
                                                return NumberOnly(e, this, false);
                                            }
                                        }
                                    ]
                                }
                            },
                            { label: 'Actionstatus', name: 'Actionstatus', width: 70, editable: false, hidden: true, hidedlg: true  },
                            { label: '', name: '', width: 60, align: 'center', formatter: obj.deleteLink, hidedlg: true, exportcol: false },
                        ],
                        viewrecords: true,
                        loadonce: true,
                        restoreAfterSelect: false,
                        saveAfterSelect: true,
                        sortable: true,
                        onSelectRow: obj.edit,
                        onPaging: obj.changePage,
                        height: 'auto',
                        rowNum: 100,
                        pager: "#jqGridPager",
                        loadComplete: function () {
                            obj.showDeleteButton();
                        }
                    });
                    $('.loader').hide();
                },
                showDeleteButton: function () {
                    var width = $('#gbox_jqGrid').width();
                    $('#truncateTable').css('left', width + 17 + 'px');
                    $('#truncateTable').show();
                },
                bindGrid: function () {
                    var ddlofficename = $('#ddlofficename').val();
                    var ddlName = $('#ddlColNames :selected').text();
                    //var ddlName = $('#ddlColNames').val();
                    if (ddlName == "") {
                        alert("Please Select dropDown");
                        $('#ddlColNames').focus();
                    }
                    $.ajax({
                        type: "POST",
                        url: "ReferenceBudget.aspx/GetReferenceBudgetbyCropID",
                        data: "{Crop_Full_Key:'" + ddlName + "',office_id:'" + ddlofficename + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {

                            var resData = JSON.parse(data.d);
                            $('.loader').show();
                            for (var i = 0; i < resData.length; i++) {
                                resData[i].rowid = i + 1;
                            }
                            obj.loadgrid(resData);
                            $('.loader').hide();
                            obj.getTableInfo();
                        }
                    });
                },
                officename: function () {
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "ReferenceBudget.aspx/GetAllofficenames",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            $.each(res.d, function (data, value) {
                                $("#ddlofficename").append($("<option></option>").val(data).html(value));
                            });
                        }

                    });
                },
                regionName: function () {
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "ReferenceBudget.aspx/GetAllRegionnames",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            $.each(res.d, function (data, value) {
                                $("#ddlofficename").append($("<option class='ddlcolor' ></option>").val(data).html(value));
                            });
                        }
                    });
                },
                edit: function (id) {
                    if (id && id !== lastSelection && canEdit) {
                        var grid = $("#jqGrid");
                        grid.jqGrid('saveRow', lastSelection);
                        grid.jqGrid('restoreRow', lastSelection);
                        var row = grid.jqGrid('getRowData', lastSelection);

                        if (!jQuery.isEmptyObject(row)) {
                            var dataobj = jQuery("#jqGrid").jqGrid('getGridParam', 'data');
                            index = dataobj.findIndex(x => x.rowid == row.rowid);
                            if (row.rowid > 0 && row.Actionstatus != 1) {
                                row.Actionstatus = 2;
                                grid.jqGrid('getGridParam', 'data')[index] = row;
                            }
                        }

                        grid.jqGrid('editRow', id);
                        lastSelection = id;
                    }
                },
                getAllExpences: function () {
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "ReferenceBudget.aspx/GetExpencesTypes",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            expenseList = JSON.parse(data.d);
                        }
                    });
                },
                getAllcropkeys: function () {
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "ReferenceBudget.aspx/GetCropKeys",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            cropkeylist = JSON.parse(data.d);
                        }
                    });
                },
                save: function () {
                    $('.loader').show();
                    var grid = $("#jqGrid");
                    var allrows = [];
                    var id = $('.inline-edit-cell').parent().parent().prop('id');
                    grid.jqGrid('saveRow', id);
                    var rowData = grid.jqGrid('getRowData', id);
                    if (rowData.rowid > 0) {
                        rowData.Actionstatus = 2;
                        grid.jqGrid('getGridParam', 'data')[id] = rowData;
                    }
                    for (var i = 0; i < DeleteRows.length; i++) {
                        DeleteRows[i].Actionstatus = 3
                        allrows.push(DeleteRows[i]);
                    }
                    var dataobj = grid.jqGrid('getGridParam', 'data');
                    var newRows = $.grep(dataobj, function (e) {
                        if (e != undefined) {
                            return e.Actionstatus == 1 || e.Actionstatus == 2
                        }
                    });
                    for (var i = 0; i < newRows.length; i++) {
                        allrows.push(newRows[i]);
                    }
                    var ddlofficename = $('#ddlofficename').val();
                    var ddlName = $('#ddlColNames :selected').text();
                    $.ajax({
                        type: "POST",
                        url: "ReferenceBudget.aspx/SaveReferenceBudget",
                        data: JSON.stringify({ lst: allrows }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {

                            toastr.success("Saved Sucessful");

                            if (timer) { clearTimeout(timer); }
                            timer = setTimeout(function () {
                                window.location.href = "ReferenceBudget.aspx?Crop_Full_Key=" + ddlName + "!" + ddlofficename;
                            }, 2000);
                            $('.loader').hide();
                        },
                        failure: function (response) {
                            var val = console.log(response.d);
                            toastr.warning(val);
                        }
                    });
                },
                search: function (self) {
                    if (timer) { clearTimeout(timer); }
                    timer = setTimeout(function () {
                        obj.changePage();
                        $("#jqGrid").jqGrid('filterInput', self.value);
                        var length = jQuery("#jqGrid").jqGrid('getGridParam', 'records');
                        $('#txtRecordsCount').html('Records Count: ' + length);
                    }, 0);
                },
                changePage: function () {
                    lastSelection = "";
                    var grid = $("#jqGrid");

                    var id = $('.inline-edit-cell').parent().parent().prop('id');
                    if (id != undefined) {
                        grid.jqGrid('saveRow', id);
                        grid.jqGrid('restoreRow', id);
                        var row = grid.jqGrid('getRowData', id);
                        if (!jQuery.isEmptyObject(row)) {
                            var dataobj = jQuery("#jqGrid").jqGrid('getGridParam', 'data');
                            index = dataobj.findIndex(x => x.rowid == row.rowid);
                            if (row.rowid > 0 && row.Actionstatus != 1) {
                                row.Actionstatus = 2;
                                grid.jqGrid('getGridParam', 'data')[index] = row;
                            }
                        }
                    }
                },
                deleteLink: function (cellValue, options, rowdata, action) {
                    return "<a href='javascript:deleteRecord(" + options.rowId + ")' class='glyphicon glyphicon-trash' style='color:red'></a>";
                },

                loadBudgetExpenseType: function (expencetype) {
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "ReferenceBudget.aspx/GetExpencesTypesvalues",
                        data: JSON.stringify({ expencetypeid: expencetype }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var id = $('.inline-edit-cell').parent().parent().prop('id');
                            ExpentionList = JSON.parse(data.d);
                            if (id != undefined) {
                                $("#jqGrid").jqGrid("setCell", id, "Crop_Input_Ind", ExpentionList[0].Crop_Input_Ind);
                                $("#jqGrid").jqGrid("setCell", id, "Documentation_ID", ExpentionList[0].Documentation_ID);
                                $("#jqGrid").jqGrid("setCell", id, "Documentation_Trigger_Ind", ExpentionList[0].Documentation_Trigger_Ind);
                                $("#jqGrid").jqGrid("setCell", id, "Financial_Budget_Code", ExpentionList[0].Financial_Budget_Code);
                                $("#jqGrid").jqGrid("setCell", id, "Status", ExpentionList[0].Status);

                                // $("#jqGrid").jqGrid("setCell", id, "Status",  $('#' + id + '_Budget_Expense_Type_ID :selected').text());
                            }
                        }
                    });
                }
            }
            obj.Init();

        });
        function isValidNumber(e) {

            var charCode = (e.which) ? e.which : e.keyCode;
            if (((charCode != 46 || (charCode == 46 && $(this).val() == '')) ||
                $(this).val().indexOf('.') != -1) && (charCode < 48 || charCode > 57)) {
                return false;
            }
        }
        function deleteRecord(id) {

            if (canEdit) {
                var grid = $('#jqGrid');
                var result = confirm("Are you sure you Want to delete?");

                if (result == true) {
                    var ediId = $('.inline-edit-cell').parent().parent().prop('id');
                    grid.jqGrid('saveRow', ediId);
                    var rowData = grid.jqGrid('getRowData', ediId);
                    if (rowData.rowid > 0 && rowData.Actionstatus != 1) {
                        rowData.Actionstatus = 2;
                        grid.jqGrid('getGridParam', 'data')[ediId - 1] = rowData;
                    }
                    var row = grid.jqGrid('getRowData', id);
                    grid.jqGrid('delRowData', id);
                    if (row.Actionstatus != 1) {
                        DeleteRows.push(row);
                    }

                    var data = jQuery("#jqGrid").jqGrid('getGridParam', 'data');
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].Budget_Default_ID == 0) {
                            data[i].rowid = i + 1;
                            data[i].id = i + 1;
                        } else
                            data[i].rowid = i + 1;
                    }
                    var curpage = parseInt($(".ui-pg-input").val());
                    jQuery('#jqGrid').jqGrid('clearGridData');
                    jQuery('#jqGrid').jqGrid('setGridParam', { data: data });
                    $("#jqGrid").trigger("reloadGrid", [{ page: curpage }]);
                    $("#jqGrid").trigger("reloadGrid", [{ page: curpage }]);
                    lastSelection = id;
                }
            }
        }
        function unique(list) {
            var result = [];
            $.each(list, function (i, e) {
                if ($.inArray(e, result) == -1) result.push(e);
            });
            return result;
        }

        function Upload_Excel_Data(data) {
            if (canEdit) {
                $.ajax({
                    type: "POST",
                    url: "ReferenceBudget.aspx/UploadExcel",
                    data: JSON.stringify({ budgets: data }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (res) {
                        toastr.success("Data Uploaded Sucessfully");
                        if (timer) { clearTimeout(timer); }
                        timer = setTimeout(function () {
                            location.reload();
                        }, 1000);
                    },
                    failure: function (response) {
                        $('.loader').hide();
                        var val = response.d;
                        toastr.warning(val);
                    }
                });
            }
        }

        function truncatetable() {
            if (canEdit) {
                var result = confirm('Are you sure you want to delete all record?');
                if (result) {
                    $('.loader').show();
                    $.ajax({
                        type: "POST",
                        url: "ReferenceBudget.aspx/TruncateTable",
                        data: JSON.stringify({}),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            toastr.success("Sucessful");
                            if (timer) { clearTimeout(timer); }
                            timer = setTimeout(function () {
                                location.reload();
                            }, 1000);
                        },
                        failure: function (response) {
                            $('.loader').hide();
                            var val = response.d;
                            toastr.warning(val);
                        }
                    });
                }
            }
        }

    </script>


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" runat="Server">
</asp:Content>

