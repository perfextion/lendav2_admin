﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_ReferenceBudget : BasePage
{
    static string cropkey;
    static int count = 0;

    public admin_ReferenceBudget()
    {
        Table_Name = "Ref_Budget_Default";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        cropkey = Request.QueryString.GetValues("Crop_Full_Key") == null ? null : (Request.QueryString.GetValues("Crop_Full_Key")[0].ToString());
        count = 0;

        if (cropkey != null)
        {
            string cropvalu = Request.QueryString.GetValues("Crop_Full_Key")[0].ToString();
            string[] cropvalue = cropvalu.Split('!');
            cropfullkey.Value = cropvalue[0];
            officeid.Value = cropvalue[1];
        }
        else
        {
            if (Session["Crop_Full_Key"] != null)
            {
                cropfullkey.Value = Session["Crop_Full_Key"].ToString();
            }
            if (Session["office_id"] != null)
            {
                officeid.Value = Session["office_id"].ToString();
            }
        }

        base.Page_Load(sender, e);
    }

    [WebMethod]
    public static string GetTableInfo()
    {
        dynamic Crop_Full_Key = HttpContext.Current.Session["Crop_Full_Key"];
        dynamic office_id = HttpContext.Current.Session["office_id"];

        string sql = @"DECLARE @Count int
                       Select @Count = Count(*)  From Ref_Budget_Default";

        if(!string.IsNullOrEmpty(Crop_Full_Key))
        {
            sql += " Where Crop_Full_Key = '" + Crop_Full_Key + "'";

            if(!string.IsNullOrEmpty(office_id))
            {
                string[] officeid = office_id.Split('_');

                //if (office_id == "1_0" || office_id == "2_0" || office_id == "3_0" || office_id == "0_0")
                if (officeid.Length >= 2)
                {
                    sql += " and ref_key='" + office_id + "'";
                }
                else
                {
                    sql += " and Z_Office_ID='" + office_id + "'";
                }
            }
        }
        else
        {
            if (!string.IsNullOrEmpty(office_id))
            {
                string[] officeid = office_id.Split('_');

                //if (office_id == "1_0" || office_id == "2_0" || office_id == "3_0" || office_id == "0_0")
                if (officeid.Length >= 2)
                {
                    sql += " Where ref_key='" + office_id + "'";
                }
                else
                {
                    sql += " Where Z_Office_ID='" + office_id + "'";
                }
            }
        }

        Count_Query = sql;
        return Get_Table_Info();
    }

    [WebMethod]
    public static Dictionary<string, string> GetCropFullKeys()
    {
        string sql_qry = "select  Distinct [Crop_Full_Key] from Ref_Budget_Default where crop_full_key != '0_0_0_0' order by [Crop_Full_Key]";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return dt.AsEnumerable().ToDictionary<DataRow, string, string>(row => row[0].ToString(),
                                       row => row[0].ToString());

    }
    [WebMethod]
    public static Dictionary<string, string> GetAllRegionnames()
    {
        Log_utilitys.update_ref_key();
        string sql_qry = "select  concat([Region_ID],'_0') as Region_ID,CONCAT([Region_ID],'.',[Region_Name]) from Ref_Region Where [Status] = 1";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return dt.AsEnumerable().ToDictionary<DataRow, string, string>(row => row[0].ToString(),
                                       row => row[1].ToString());
    }
    [WebMethod]
    public static Dictionary<string, string> GetAllofficenames()
    {
        string sql_qry = "select Office_ID,CONCAT( Office_ID,'.',Office_Name) as Office_Name from Ref_Office  Where [Status] = 1 order by Office_ID";

        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);

        return dt.AsEnumerable().ToDictionary<DataRow, string, string>(row => row[0].ToString(),
                                       row => row[1].ToString());
    }
    [WebMethod(EnableSession = true)]
    public static string GetReferenceBudgetbyCropID(string Crop_Full_Key, string office_id)
    {
        HttpContext.Current.Session["Crop_Full_Key"] = Crop_Full_Key;
        HttpContext.Current.Session["office_id"] = office_id;
        string sql_qry = @"SELECT
	                            ISNULL(RO.Office_Name,'ARM') as [Office_Name], 
	                            ISNULL(RR.Region_Name,'ARM') as Region_Name, 
	                            0 as Actionstatus,
	                            Budget_Default_ID,
	                            Crop_Practice_ID,
	                            Crop_Full_Key,
                                BD.Budget_Expense_Type_ID,
	                            BET.Budget_Expense_Name, 
	                            ARM_Budget,
	                            BD.Z_Office_ID,
	                            Z_Region_ID,
	                            BET.Financial_Budget_Code,
	                            BET.Status 
                            FROM Ref_Budget_Default BD 
                            LEFT JOIN Ref_Budget_Expense_Type BET on BD.Budget_Expense_Type_ID = BET.Budget_Expense_Type_ID
                            LEFT JOIN REF_OFFICE RO ON RO.Office_ID = BD.Z_Office_ID
                            LEFT JOIN Ref_Region RR on RR.Region_ID = BD.Z_Region_ID
                            WHERE Crop_Full_Key = '" + Crop_Full_Key + "' And BET.Standard_Ind = 1 ";

        if (!string.IsNullOrEmpty(office_id))
        {
            string[] officeid = office_id.Split('_');

            //if (office_id == "1_0" || office_id == "2_0" || office_id == "3_0" || office_id == "0_0")
            if (officeid.Length >= 2)
            {
                sql_qry += " and ref_key='" + office_id + "'";
            }
            else
            {
                sql_qry += " and Z_Office_ID='" + office_id + "'";
            }
        }
        sql_qry += " order by BET.Sort_order";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static string GetExpencesTypes()
    {
        string sql_qry = " select Distinct Budget_Expense_Type_ID,Budget_Expense_Name from  [Ref_Budget_Expense_Type]";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return DataTableToJsonstring(dt);
    }

    [WebMethod]
    public static string GetCropKeys()
    {
        string sql_qry = "SELECT Distinct Crop_And_Practice_ID,Crop_Full_Key from V_Crop_Price_Details";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return DataTableToJsonstring(dt);
    }

    [WebMethod]
    public static string GetExpencesTypesvalues(string expencetypeid)
    {
        string sql_qry = " select Crop_Input_Ind,Documentation_Trigger_Ind,Documentation_ID,Financial_Budget_Code,Status from  Ref_Budget_Expense_Type where Budget_Expense_Type_ID='" + expencetypeid + "' ";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
    }
    [WebMethod]
    public static void SaveReferenceBudget(dynamic lst)
    {
        foreach (var item in lst)
        {
            string qry = "";
            int val = Convert.ToInt32(item["Actionstatus"]);
            if (val == 1)
            {
                qry = "insert into Ref_Budget_Default (Crop_Practice_ID,Crop_Full_Key,Budget_Expense_Type_ID,ARM_Budget) values('" + item["Crop_Practice_ID"].Replace("'", "''") + "','" + item["Crop_Full_Key"].Replace("'", "''") + "'," +
                 "'" + item["Budget_Expense_Type_ID"].Replace("'", "''") + "','" + item["ARM_Budget"].Replace("'", "''") + "')";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 2)
            {
                qry = "UPDATE Ref_Budget_Default SET Crop_Practice_ID='" + item["Crop_Practice_ID"] + "',Crop_Full_Key='" + item["Crop_Full_Key"] + "'," +
                    "Budget_Expense_Type_ID='" + item["Budget_Expense_Type_ID"] + "'," +
                    "ARM_Budget = '" + (item["ARM_Budget"]) + "' where Budget_Default_ID='" + item["Budget_Default_ID"] + "'  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 3)
            {
                qry = "DELETE FROM Ref_Budget_Default  where Budget_Default_ID='" + item["Budget_Default_ID"] + "'  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }

        }

        Update_Table_Audit();
    }

    [WebMethod]
    public static void AddNewCropKey(string Crop_Full_Key)
    {
        string qry =  @"IF NOT EXISTS(Select 1 From Ref_Budget_Default Where Crop_Full_Key = '" + Crop_Full_Key + @"' And Z_Region_ID = 0 And Z_Office_ID = 0)
                        BEGIN
                             INSERT INTO [dbo].[Ref_Budget_Default]
                                   (
                                        [Crop_Practice_ID]
                                       ,[Crop_Full_Key]
                                       ,[Budget_Expense_Type_ID]
                                       ,[ARM_Budget]
                                       ,[Distributor_Budget]
                                       ,[Third_Party_Budget]
                                       ,[Z_Crop_Code]
                                       ,[Z_Practice_Type_Code]
                                       ,[Z_Region_ID]
                                       ,[Z_Office_ID]
                                       ,[Status]
                                       ,[ref_key]
                                    )
                                SELECT 
	                                VC.Crop_And_Practice_ID,
	                                '" + Crop_Full_Key + @"',
	                                RB.[Budget_Expense_Type_ID],
	                                [ARM_Budget],
	                                0,
	                                0,
	                                VC.Crop_Code,
	                                VC.Irr_Prac_Code,
	                                0,
	                                0,
	                                1,
	                                '0_0'
                                FROM Ref_Budget_Default RB
                                JOIN Ref_Budget_Expense_Type RBET ON RB.Budget_Expense_Type_ID = RBET.Budget_Expense_Type_ID
                                LEFT JOIN V_Crop_Price_Details VC ON VC.Crop_Full_Key = '" + Crop_Full_Key + @"'
                                Where RB.Crop_Full_Key = 'CRN_GRAIN'
                                AND Ref_Key = '0_0'
                                AND (RBET.Standard_Ind = 1 OR RBET.Standard_Ind = 2)
                        END";

        qry += Environment.NewLine;

        gen_db_utils.base_sql_execute(qry, dbKey);
        Update_Table_Audit();
    }

    public static string DataTableToJsonstring(DataTable dt)
    {
        DataSet ds = new DataSet();
        ds.Merge(dt);
        StringBuilder JsonStr = new StringBuilder();
        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            JsonStr.Append("{");
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                JsonStr.Append("\"" + ds.Tables[0].Rows[i][0].ToString() + "\":" + "\"" + ds.Tables[0].Rows[i][1].ToString() + "\",");
            }
            JsonStr = JsonStr.Remove(JsonStr.Length - 1, 1);
            JsonStr.Append("}");
            return JsonStr.ToString();
        }
        else
        {
            return null;
        }
    }

    [WebMethod]
    public static void TruncateTable()
    {
        string qry = "TRUNCATE TABLE Ref_Budget_Default";
        qry += Environment.NewLine;

        qry += @"INSERT INTO [dbo].[Ref_Budget_Default]
                            (
                                [Crop_Practice_ID]
                                ,[Crop_Full_Key]
                                ,[Budget_Expense_Type_ID]
                                ,[ARM_Budget]
                                ,[Distributor_Budget]
                                ,[Third_Party_Budget]
                                ,[Z_Crop_Code]
                                ,[Z_Practice_Type_Code]
                                ,[Z_Region_ID]
                                ,[Z_Office_ID]
                                ,[Status]
                                ,[ref_key]
                            )
                        SELECT 
	                        NULL,
	                        '0_0_0_0',
	                        RBET.[Budget_Expense_Type_ID],
	                        0,
	                        0,
	                        0,
	                        NULL,
	                        NULL,
	                        0,
	                        0,
	                        1,
	                        '0_0'
                        FROM Ref_Budget_Expense_Type RBET
                        Where (RBET.Standard_Ind = 1 OR RBET.Standard_Ind = 2)";

        gen_db_utils.gp_sql_execute(qry, dbKey);
        Update_Table_Audit();
    }

    [WebMethod]
    public static void UploadExcel(dynamic budgets)
    {
        string qry = string.Empty;

        foreach (var item in budgets)
        {
            if (!string.IsNullOrEmpty(item["Status"]))
            {
                var expense_type_id = TrimSQL(Get_Value(item, "Expense ID"));
                string crop_key = TrimSQL(Get_Value(item, "Crop Key"));
                var region_id = Get_Value(item, "Region ID");
                var office_id = Get_Value(item, "Office ID");
                var bgt = Get_Value(item, "Budget Amt");
                var Status = Get_Value(item, "Status");

                qry += @"EXEC [dbo].[usp_AddBudgetDefault] 
                                    @Crop_Full_Key = '" + crop_key + @"', 
                                    @Z_OFFICE_ID = '" + office_id + @"',  
                                    @Z_REGION_ID = '" + region_id + @"',  
                                    @Budget_Expense_Type_ID = '" + expense_type_id + @"',  
                                    @Budget_AMT = '" + bgt + @"'";

                qry += Environment.NewLine;
            }
        }

        if (!string.IsNullOrEmpty(qry))
        {
            qry += "EXEC usp_verify_budget";
            qry += Environment.NewLine;

            gen_db_utils.gp_sql_execute(qry, dbKey);
            Update_Table_Audit();
        }
    }
    public static string ManageQuotes(string value)
    {
        if (value.Contains("'"))
        {
            value = value.Replace("'", "''");
        }
        return value;
    }
}