﻿<%@ Page Title="" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="ReferenceBudgetViewArms.aspx.cs" Inherits="admin_ReferenceBudgetViewArms" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .ui-jqgrid .ui-jqgrid-htable th div {
            height: auto;
            overflow: hidden;
            padding-right: 4px;
            padding-top: 2px;
            position: relative;
            vertical-align: text-top;
            white-space: normal !important;
        }

        .fc-head-container thead tr, .table thead tr {
            height: 45px;
        }
    </style>
    <div class="">
        <div style="width: 1500px">
            <div style="width: 11%; margin-left: 27px; float: left;">
                <div style="margin-top: 5%; margin-bottom: 1%;">
                    <%--<div id="btnAddRow" class="btn btn-info" style="width: 70px;">
                        add
                    </div>--%>
                    <div id="btnSave" class="btn btn-success" style="width: 70px;">Save</div>
                    <div id="btnreload" class="btn btn-primary fa fa-refresh" title="Reload" style="width: 80px;"></div>
                </div>
            </div>
            <div style="width: 62%; float: left;">
                <div class="form-inline" style="margin-top: 1%; margin-left: 3%; margin-bottom: 2%;">
                    <label for="ddlRegions">Regions :</label>
                    <select id="ddlRegions" class="form-control" style="width: 200px">
                        <option value="4">All Locations</option>
                        <option value="0">ARM</option>
                    </select>
                    <label for="ddlCropkeys">Crop Practice ID :</label>
                    <select id="ddlCropkeys" class="form-control" style="width: 200px">
                        <%--<option value="0">Select</option>--%>
                    </select>
                    <label for="txtSearch">Search :</label>
                    <input type="text" class="form-control" name="search" id="txtSearch" style="width: 200px" />
                </div>
            </div>
        </div>
        <div class="loader" style="margin-top: 1%; margin-left: 12%; margin-bottom: 0%"></div>
        <div class="col-md-12" style="margin-bottom: 7%;">
            <div class="row">
                <div style="margin-left: 24px;">
                    <table id="jqGrid"></table>
                    <div id="jqGridPager"></div>
                </div>
            </div>
        </div>
        <br />
        <br />
        <br />
    </div>

    <script type="text/javascript">  

        var DeleteRows = [];
        var colmodel = [];
        var gridwidth = 130;
        var timer;
        var colid;
        var rowidno;
        var lastSelection;
        var execute = false;
        $(function () {
            $.urlParam = function (name) {
                var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
                if (results == null) {
                    return null;
                }
                return decodeURI(results[1]) || 0;
            }
            var obj = {
                Init: function () {

                    //$.jgrid.defaults.responsive = true;
                    //$.jgrid.defaults.styleUI = 'Bootstrap';

                    var crop = $.urlParam('Crop_Full_Key');
                    obj.getcropkeys();
                    obj.getregions();
                    obj.loadgridcols();
                    if (crop || 0 > 0 && crop.length > 0) {
                        var cropkey = crop.split('!');
                        var cropfullkey = cropkey[0];
                        var region = cropkey[1];
                        if (cropfullkey != undefined || "") {
                            $('#ddlCropkeys').val(cropfullkey);
                        }
                        if (region != undefined || "") {
                            $('#ddlRegions').val(region);
                        }
                    }
                    obj.bindGrid();
                    $("#btnSave").click(function () {
                        obj.save();
                    });
                    $("#btnreload").click(function () {
                        obj.Reload();
                    });
                    $("#txtSearch").on("keyup", function () {
                        var self = this;
                        obj.search(self);
                    });
                    $('a').on("click", function (ev) {

                        var grid = $("#jqGrid");
                        var id = $('.inline-edit-cell').parent().parent().prop('id');
                        grid.jqGrid('saveRow', id);
                        var rowData = grid.jqGrid('getRowData', id);
                        if (rowData.Budget_Default_ID > 0) {
                            rowData.Actionstatus = 2;
                            grid.jqGrid('getGridParam', 'data')[id] = rowData;
                        }
                        var dataobj = grid.jqGrid('getGridParam', 'data');
                        var newRows = $.grep(dataobj, function (e) {
                            if (e != undefined) {
                                return e.Actionstatus == 2
                            }
                        });
                        if (newRows.length > 0) {
                            var result = confirm("Are you sure you Want to leave without Saving ?");
                            if (result == false) {
                                ev.preventDefault();
                            }
                        }

                    });
                    var previous;
                    $("#ddlCropkeys").on('focus', function () {
                        previous = this.value;
                    }).change(function () {
                        lastSelection = undefined;
                        var grid = $("#jqGrid");
                        var id = $('.inline-edit-cell').parent().parent().prop('id');
                        grid.jqGrid('saveRow', id);
                        var rowData = grid.jqGrid('getRowData', id);
                        if (rowData.Budget_Default_ID > 0) {
                            rowData.Actionstatus = 2;
                            grid.jqGrid('getGridParam', 'data')[id] = rowData;
                        }
                        var dataobj = grid.jqGrid('getGridParam', 'data');
                        var newRows = $.grep(dataobj, function (e) {
                            if (e != undefined) {
                                return e.Actionstatus == 2
                            }
                        });

                        if (newRows.length > 0) {
                            var result = confirm("Are you sure you Want to leave without Saving ?");
                            if (result == true) {
                                $('.loader').show();
                                obj.bindGrid();
                            } else {
                                $("#ddlColNames").val(previous)
                            }
                        } else {
                            $('.loader').show();
                            obj.bindGrid();
                        }
                    });
                    $("#ddlRegions").on('focus', function () {
                        previous = this.value;
                    }).change(function () {
                        var regionid = $('#ddlRegions').val();
                        lastSelection = undefined;
                        var grid = $("#jqGrid");
                        var id = $('.inline-edit-cell').parent().parent().prop('id');
                        grid.jqGrid('saveRow', id);
                        var rowData = grid.jqGrid('getRowData', id);
                        if (rowData.Budget_Default_ID > 0) {
                            rowData.Actionstatus = 2;
                            grid.jqGrid('getGridParam', 'data')[id] = rowData;
                        }
                        var dataobj = grid.jqGrid('getGridParam', 'data');
                        var newRows = $.grep(dataobj, function (e) {
                            if (e != undefined) {
                                return e.Actionstatus == 2
                            }
                        });

                        if (newRows.length > 0) {
                            var result = confirm("Are you sure you Want to leave without Saving ?");
                            if (result == true) {
                                $('.loader').show();
                                obj.bindGrid();
                            } else {
                                $("#ddlRegions").val(previous)
                            }
                        } else {
                            $('.loader').show();
                            obj.bindGrid();
                        }
                    });

                },
                bindGrid: function () {
                    var CropFullKey = $('#ddlCropkeys :selected').text();
                    var Regionid = $('#ddlRegions').val();
                    $.ajax({
                        type: "POST",
                        url: "ReferenceBudgetViewArms.aspx/GetReferenceBudgetViewArms",
                        data: "{Crop_Full_Key:'" + CropFullKey + "',Region_id:'" + Regionid + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {

                            var resData = JSON.parse(data.d);
                            $('.loader').show();
                            for (var i = 0; i < resData.length; i++) {
                                resData[i].rowid = i + 1;
                            }
                            obj.loadgrid(resData);
                            for (var i = 0; i < resData.length; i++) {
                                if (Regionid == 0) {
                                    $.each(resData[i], function (obj, values) {
                                        if (obj == 0) {
                                            if ($.isNumeric(obj)) {
                                                $("#jqGrid").showCol(obj)
                                                //if (i == 0) {
                                                //    gridwidth += 70;
                                                //}
                                            }
                                        }
                                    });
                                }
                                else {
                                    $.each(resData[i], function (obj, values) {
                                        if ($.isNumeric(obj)) {
                                            $("#jqGrid").showCol(obj)
                                            //if (i == 0) {
                                            //    gridwidth += 70;
                                            //}
                                        }
                                    });
                                }
                            }
                            if (Regionid == 0) {
                                $("#jqGrid").jqGrid('setLabel', '5', 'ARM ');
                                $.jgrid.defaults.width = 1050;
                            }
                            else if (Regionid == 1) {
                                $("#jqGrid").jqGrid('setLabel', '5', 'Central ARM');
                                $.jgrid.defaults.width = 600;
                            }
                            else if (Regionid == 2) {
                                $("#jqGrid").jqGrid('setLabel', '5', 'East ARM');
                                $.jgrid.defaults.width = 500;
                            }
                            else if (Regionid == 3) {
                                $("#jqGrid").jqGrid('setLabel', '5', 'West ARM');
                                $.jgrid.defaults.width = 750;
                            }
                            $('.loader').hide();

                        }
                    });
                },
                loadgrid: function (data) {
                    $.jgrid.gridUnload("#jqGrid");
                    $("#jqGrid").jqGrid({
                        datatype: "local",
                        styleUI: 'Bootstrap',
                        data: data,
                        colModel: colmodel,
                        //[
                        //{ label: 'ARM', name: '0', width: 70, editable: true, hidden: true, align: 'center', editoptions: { maxlength: 20, dataEvents: [{ type: 'keypress', fn: function (event) { return isNumber(event, this) } }] } },
                        //{ label: 'Cleveland', name: '1', width: 70, editable: true, hidden: true, align: 'center', editoptions: { maxlength: 20, dataEvents: [{ type: 'keypress', fn: function (event) { return isNumber(event, this) } }] } },
                        //{ label: 'Crowley', name: '2', width: 70, editable: true, hidden: true, align: 'center', editoptions: { maxlength: 20, dataEvents: [{ type: 'keypress', fn: function (event) { return isNumber(event, this) } }] } },
                        //{ label: 'Jonesboro', name: '3', width: 70, editable: true, hidden: true, align: 'center', editoptions: { maxlength: 20, dataEvents: [{ type: 'keypress', fn: function (event) { return isNumber(event, this) } }] } },
                        //{ label: 'Rayville', name: '4', width: 70, editable: true, hidden: true, align: 'center', editoptions: { maxlength: 20, dataEvents: [{ type: 'keypress', fn: function (event) { return isNumber(event, this) } }] } },
                        //{ label: 'Dexter', name: '5', width: 70, editable: true, hidden: true, align: 'center', editoptions: { maxlength: 20, dataEvents: [{ type: 'keypress', fn: function (event) { return isNumber(event, this) } }] } },
                        //{ label: 'Amarillo', name: '6', width: 70, editable: true, hidden: true, align: 'center', editoptions: { maxlength: 20, dataEvents: [{ type: 'keypress', fn: function (event) { return isNumber(event, this) } }] } },
                        //{ label: 'Victoria', name: '7', width: 70, editable: true, hidden: true, align: 'center', editoptions: { maxlength: 20, dataEvents: [{ type: 'keypress', fn: function (event) { return isNumber(event, this) } }] } },
                        //{ label: 'Lubbock', name: '8', width: 70, editable: true, hidden: true, align: 'center', editoptions: { maxlength: 20, dataEvents: [{ type: 'keypress', fn: function (event) { return isNumber(event, this) } }] } },
                        //{ label: 'Iowa Falls', name: '9', width: 70, editable: true, hidden: true, align: 'center', editoptions: { maxlength: 20, dataEvents: [{ type: 'keypress', fn: function (event) { return isNumber(event, this) } }] } },
                        //{ label: 'Hiawatha', name: '10', width: 70, editable: true, hidden: true, align: 'center', editoptions: { maxlength: 20, dataEvents: [{ type: 'keypress', fn: function (event) { return isNumber(event, this) } }] } },
                        //{ label: 'Fort Wayne', name: '11', width: 70, editable: true, hidden: true, align: 'center', editoptions: { maxlength: 20, dataEvents: [{ type: 'keypress', fn: function (event) { return isNumber(event, this) } }] } },
                        //{ label: 'Pella', name: '12', width: 70, editable: true, hidden: true, align: 'center', editoptions: { maxlength: 20, dataEvents: [{ type: 'keypress', fn: function (event) { return isNumber(event, this) } }] } },
                        //{ label: 'Avon', name: '13', width: 70, editable: true, hidden: true, align: 'center', editoptions: { maxlength: 20, dataEvents: [{ type: 'keypress', fn: function (event) { return isNumber(event, this) } }] } },
                        //{ label: 'Wichita', name: '14', width: 70, editable: true, hidden: true, align: 'center', editoptions: { maxlength: 20, dataEvents: [{ type: 'keypress', fn: function (event) { return isNumber(event, this) } }] } },
                        //{ label: 'Hastings', name: '15', width: 70, editable: true, hidden: true, align: 'center', editoptions: { maxlength: 20, dataEvents: [{ type: 'keypress', fn: function (event) { return isNumber(event, this) } }] } },
                        //{ label: 'Garner', name: '16', width: 70, editable: true, hidden: true, align: 'center', editoptions: { maxlength: 20, dataEvents: [{ type: 'keypress', fn: function (event) { return isNumber(event, this) } }] } },
                        //{ label: 'Fremont', name: '17', width: 70, editable: true, hidden: true, align: 'center', editoptions: { maxlength: 20, dataEvents: [{ type: 'keypress', fn: function (event) { return isNumber(event, this) } }] } },
                        //{ label: 'Springfield', name: '18', width: 75, editable: true, hidden: true, align: 'center', editoptions: { maxlength: 20, dataEvents: [{ type: 'keypress', fn: function (event) { return isNumber(event, this) } }] } },
                        //{ label: 'Rochester', name: '19', width: 70, editable: true, hidden: true, align: 'center', editoptions: { maxlength: 20, dataEvents: [{ type: 'keypress', fn: function (event) { return isNumber(event, this) } }] } },
                        //{ label: 'Thomasville', name: '20', width: 80, editable: true, hidden: true, align: 'center', editoptions: { maxlength: 20, dataEvents: [{ type: 'keypress', fn: function (event) { return isNumber(event, this) } }] } },
                        //{ label: 'Richland', name: '21', width: 70, editable: true, hidden: true, align: 'center', editoptions: { maxlength: 20, dataEvents: [{ type: 'keypress', fn: function (event) { return isNumber(event, this) } }] } },
                        //{ label: 'Bellefontaine', name: '22', width: 85, editable: true, hidden: true, align: 'center', editoptions: { maxlength: 20, dataEvents: [{ type: 'keypress', fn: function (event) { return isNumber(event, this) } }] } },
                        //{ label: 'Nashville', name: '23', width: 70, editable: true, hidden: true, align: 'center', editoptions: { maxlength: 20, dataEvents: [{ type: 'keypress', fn: function (event) { return isNumber(event, this) } }] } },
                        //{ label: 'Saint Joseph', name: '24', width: 100, editable: true, hidden: true, align: 'center', editoptions: { maxlength: 20, dataEvents: [{ type: 'keypress', fn: function (event) { return isNumber(event, this) } }] } },

                        // { label: 'Actionstatus', name: 'Actionstatus', width: 70, editable: false, hidden: true },
                        // ],
                        viewrecords: true,
                        loadonce: true,
                        restoreAfterSelect: false,
                        saveAfterSelect: true,
                        sortable: true,
                        onSelectRow: obj.edit,
                        onCellSelect: function (row, col, content, event) {
                            var cm = jQuery("#jqGrid").jqGrid("getGridParam", "colModel");
                            colid = cm[col].name;
                            rowidno = row;
                        },
                        onPaging: obj.changePage,
                        height: 'auto',
                        rowNum: 100,
                        width: 1450,
                        shrinkToFit: false,
                        pager: "#jqGridPager",
                        gridComplete: function () {

                            var txtid = rowidno + '_' + colid;
                            $('#' + txtid + '').focus();
                        },
                        //shrinkToFit: true,
                        //autowidth: true
                    });
                    $("#jqGrid").jqGrid("setFrozenColumns");
                    $("#jqGrid").trigger("resize");

                },
                loadgridcols: function () {
                    var colvalue = "";
                    var widthgrid = "";
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "ReferenceBudgetViewArms.aspx/GetAllofficenames",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            colmodel.push({ label: 'rowid', name: 'rowid', width: 75, editable: false, key: true, hidden: true, frozen: true, });
                            colmodel.push({ label: 'Sort Order', name: 'Sort_order', align: 'center', sorttype: 'number', width: 60, frozen: true, editable: false });
                            colmodel.push({ label: 'Expense ID', name: 'Budget_Expense_Type_ID', align: 'center', hidden: true, sorttype: 'number', frozen: true, width: 70, editable: false });
                            colmodel.push({ label: 'Crop Full Key', name: 'Crop_Full_Key', width: 130, hidden: true, editable: false, frozen: true });
                            colmodel.push({ label: 'Expense Name', name: 'Budget_Expense_Name', width: 170, editable: false, frozen: true });
                            colmodel.push({ label: 'ARM', name: '0', width: 70, editable: true, hidden: true, align: 'center', editoptions: { maxlength: 20, dataEvents: [{ type: 'keypress', fn: function (event) { return isNumber(event, this) } }] } });
                            $.each(res.d, function (data, value) {
                                colmodel.push({ label: value, name: data, width: 70, editable: true, hidden: true, align: 'center', editoptions: { maxlength: 20, dataEvents: [{ type: 'keypress', fn: function (event) { return isNumber(event, this) } }] } });
                            });
                            colmodel.push({ label: 'Actionstatus', name: 'Actionstatus', width: 70, editable: false, hidden: true });
                        }
                    });

                },
                getcropkeys: function () {
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "ReferenceBudgetViewArms.aspx/GetCropFullKeys",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            $.each(res.d, function (data, value) {
                                $("#ddlCropkeys").append($("<option></option>").val(data).html(value));
                            });
                        }
                    })
                },
                getregions: function () {
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "ReferenceBudgetViewArms.aspx/GetRegions",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            $.each(res.d, function (data, value) {
                                $("#ddlRegions").append($("<option></option>").val(data).html(value));
                            });
                        }
                    })
                },
                add: function () {

                    var grid = $("#jqGrid");
                    var rowsperPage = parseInt(grid.getGridParam('rowNum'));
                    var gridlength = grid.jqGrid('getGridParam', 'data').length;
                    var curpage = parseInt($(".ui-pg-input").val());
                    var totPages = Math.ceil(gridlength / rowsperPage);


                    if (rowsperPage * totPages == gridlength) {
                        var id = $('.inline-edit-cell').parent().parent().prop('id');
                        grid.jqGrid('saveRow', id);
                        var row = obj.newrow();

                        var newRowId = row.rowid;
                        grid.jqGrid('addRowData', newRowId, row);
                        grid.trigger('reloadGrid');
                        lastSelection = newRowId;
                        grid.jqGrid('saveRow', lastSelection);
                        grid.jqGrid('restoreRow', lastSelection);
                        $('.glyphicon-step-forward').trigger('click');
                    } else {
                        $('.glyphicon-step-forward').trigger('click');
                        var row = obj.newrow();

                        var newRowId = row.rowid;
                        grid.jqGrid('addRowData', newRowId, row);
                    }


                    lastSelection = newRowId;
                    grid.jqGrid('saveRow', lastSelection);
                    grid.jqGrid('restoreRow', lastSelection);

                    var eid = $('.inline-edit-cell').parent().parent().prop('id')
                    grid.jqGrid('saveRow', eid);
                    grid.jqGrid('restoreRow', eid);
                    grid.jqGrid('editRow', newRowId);
                },
                edit: function (id) {
                    execute = true;
                    if (id && id !== lastSelection) {
                        var grid = $("#jqGrid");
                        grid.jqGrid('saveRow', lastSelection);
                        grid.jqGrid('restoreRow', lastSelection);
                        var row = grid.jqGrid('getRowData', lastSelection);

                        if (!jQuery.isEmptyObject(row)) {
                            var dataobj = jQuery("#jqGrid").jqGrid('getGridParam', 'data');
                            index = dataobj.findIndex(x => x.rowid == row.rowid);
                            if (row.rowid > 0 && row.Actionstatus != 1) {
                                row.Actionstatus = 2;
                                grid.jqGrid('getGridParam', 'data')[index] = row;
                            }
                        }
                        grid.jqGrid('editRow', id);
                        lastSelection = id;
                        obj.focus();
                    }
                },
                focus: function () {
                    setTimeout(function () {
                        var txtid = rowidno + '_' + colid;
                        $('#' + txtid + '').focus();
                    }, 1);

                },
                newrow: function () {
                    var newid = 0;

                    var grid = $("#jqGrid");
                    var length = jQuery("#jqGrid").jqGrid('getGridParam', 'records');
                    var dataobj = grid.jqGrid('getGridParam', 'data');
                    var dataobj = grid.jqGrid('getGridParam', 'data');
                    var id = [];
                    ids = dataobj.map(function (e) { return e.rowid });
                    if (ids.length > 0) {
                        newid = ids.reduce(function (a, b) { return Math.max(a, b); });
                    }
                    var row = {
                        Sort_order: 0,
                        Actionstatus: 1,
                        rowid: newid + 1
                    };
                    return row;
                },
                save: function () {
                    $('.loader').show();
                    var grid = $("#jqGrid");
                    var allrows = [];
                    var id = $('.inline-edit-cell').parent().parent().prop('id');
                    grid.jqGrid('saveRow', id);
                    var rowData = grid.jqGrid('getRowData', id);
                    if (rowData.rowid > 0) {
                        rowData.Actionstatus = 2;
                        grid.jqGrid('getGridParam', 'data')[id] = rowData;
                    }

                    for (var i = 0; i < DeleteRows.length; i++) {
                        DeleteRows[i].Actionstatus = 3
                        allrows.push(DeleteRows[i]);
                    }
                    var dataobj = grid.jqGrid('getGridParam', 'data');
                    var newRows = $.grep(dataobj, function (e) {
                        if (e != undefined) {
                            return e.Actionstatus == 1 || e.Actionstatus == 2
                        }
                    });
                    for (var i = 0; i < newRows.length; i++) {
                        allrows.push(newRows[i]);
                    }
                    var ddlName = $('#ddlCropkeys :selected').text();
                    var Regionid = $('#ddlRegions').val();
                    $.ajax({
                        type: "POST",
                        url: "ReferenceBudgetViewArms.aspx/SaveReferenceBudgetViewArms",
                        data: JSON.stringify({ lst: allrows, Region_id: Regionid }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {

                            toastr.success("Saved Sucessful");
                            if (timer) { clearTimeout(timer); }
                            timer = setTimeout(function () {
                                window.location.href = "ReferenceBudgetViewArms.aspx?Crop_Full_Key=" + ddlName + "!" + Regionid;;
                            }, 2000);
                        },
                        failure: function (response) {
                            var val = console.log(response.d);
                            toastr.warning(val);
                        }
                    });
                },
                Reload: function () {
                    $('.loader').show();
                    var ddlName = $('#ddlCropkeys :selected').text();
                    var Regionid = $('#ddlRegions').val();
                    window.location.href = "ReferenceBudgetViewArms.aspx?Crop_Full_Key=" + ddlName + "!" + Regionid;;
                    $('.loader').hide();
                },
                search: function (self) {
                    if (timer) { clearTimeout(timer); }
                    timer = setTimeout(function () {
                        obj.changePage();
                        $("#jqGrid").jqGrid('filterInput', self.value);
                    }, 0);
                },
                changePage: function () {

                    lastSelection = "";
                    var grid = $("#jqGrid");
                    var id = $('.inline-edit-cell').parent().parent().prop('id');
                    if (id != undefined) {
                        grid.jqGrid('saveRow', id);
                        grid.jqGrid('restoreRow', id);
                        var row = grid.jqGrid('getRowData', id);
                        if (!jQuery.isEmptyObject(row)) {
                            var dataobj = jQuery("#jqGrid").jqGrid('getGridParam', 'data');
                            index = dataobj.findIndex(x => x.rowid == row.rowid);
                            if (row.rowid > 0 && row.Actionstatus != 1) {
                                row.Actionstatus = 2;
                                grid.jqGrid('getGridParam', 'data')[index] = row;
                            }
                        }
                    }
                },
                deleteLink: function (cellValue, options, rowdata, action) {
                    return "<a href='javascript:deleteRecord(" + options.rowId + ")' class='glyphicon glyphicon-trash' style='color:red'></a>";
                }
            }
            obj.Init();

        });
        function isNumber(evt, element) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (
                (charCode != 45 || $(element).val().indexOf('-') != -1) &&      // “-” CHECK MINUS, AND ONLY ONE.
                (charCode != 46 || $(element).val().indexOf('.') != -1) &&      // “.” CHECK DOT, AND ONLY ONE.
                (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
        function editRow(rowId) {

            var grid = $('#jqGrid');
            grid.editRow(rowId, true, function () {
                var colModel = grid.getGridParam('colMode');
                var colName = colModel[colIndex].name;
                var input = $('#' + rowId + '_' + colName);
                input.get(0).focus();
            });
        }
        function deleteRecord(id) {

            var grid = $('#jqGrid');
            var result = confirm("Are you sure you Want to delete?");

            if (result == true) {
                var ediId = $('.inline-edit-cell').parent().parent().prop('id');
                grid.jqGrid('saveRow', ediId);
                var rowData = grid.jqGrid('getRowData', ediId);
                if (rowData.rowid > 0 && rowData.Actionstatus != 1) {
                    rowData.Actionstatus = 2;
                    grid.jqGrid('getGridParam', 'data')[ediId - 1] = rowData;
                }

                var row = grid.jqGrid('getRowData', id);
                grid.jqGrid('delRowData', id);
                if (row.Actionstatus != 1) {
                    DeleteRows.push(row);
                }
                var data = jQuery("#jqGrid").jqGrid('getGridParam', 'data');
                for (var i = 0; i < data.length; i++) {
                    if (data[i].Sort_order == 0) {
                        data[i].rowid = i + 1;
                        data[i].id = i + 1;
                    } else
                        data[i].rowid = i + 1;
                }
                var curpage = parseInt($(".ui-pg-input").val());
                jQuery('#jqGrid').jqGrid('clearGridData');
                jQuery('#jqGrid').jqGrid('setGridParam', { data: data });
                $("#jqGrid").trigger("reloadGrid", [{ page: curpage }]);
                $("#jqGrid").trigger("reloadGrid", [{ page: curpage }]);
                lastSelection = id;
            }
        }
        function unique(list) {
            var result = [];
            $.each(list, function (i, e) {
                if ($.inArray(e, result) == -1) result.push(e);
            });
            return result;
        }
    </script>


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" runat="Server"></asp:Content>

