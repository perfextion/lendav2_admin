﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
public partial class admin_ReferenceBudgetViewArms : System.Web.UI.Page
{
    static string dbKey = "gp_conn";
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    [WebMethod]
    public static string GetReferenceBudgetViewArms(string Crop_Full_Key, string Region_id)
    {
        string regionid = "";
        string regionname = "z_region_id,";
        string regioncondition = "";
        if (Region_id == "0")
        {
            regionid = "";
            regioncondition = "and z_region_id=''" + Region_id + "''";
        }
        else if (Region_id == "4")
        {
            regionid = "";
            regionname = "";
            regioncondition = " and not (z_region_id <>0 and z_office_id = 0)";
        }
        else
        {
            regionid = "where region_id='" + Region_id + @"'";
            regioncondition = "and z_region_id=''" + Region_id + "''";
        }

        string sql_qry = @"DECLARE @cols AS NVARCHAR(MAX),
    @query  AS NVARCHAR(MAX)

select @cols = STUFF((SELECT ',' + QUOTENAME(Office_ID) 
                    from Ref_Office " + regionid + @"
                    group by Office_ID
                    order by Office_ID
            FOR XML PATH(''), TYPE
            ).value('.', 'NVARCHAR(MAX)') 
        ,1,1,'')

set @query = 'SELECT 0 as Actionstatus,Sort_order,Budget_Expense_Type_ID,Crop_Full_Key,Budget_Expense_Name," + regionname + @"[0],' + @cols + ' from 
             (
               select [Sort_order], RBE.[Budget_Expense_Type_ID],[Crop_Full_Key],[Budget_Expense_Name],Z_Office_ID," + regionname + @"[ARM_Budget]
                  from[Ref_Budget_Default] RBD join[Ref_Budget_Expense_Type] RBE on RBE.[Budget_Expense_Type_ID] = RBD.[Budget_Expense_Type_ID]
                  where[Crop_Full_Key] = ''" + Crop_Full_Key + @"'' and[Standard_Ind] = ''1'' " + regioncondition + @"
            ) x 
            pivot 
            (
                sum(ARM_Budget)
                for Z_Office_ID in ([0],' + @cols + ')
            ) p order by sort_order;'
execute(@query);";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
    }
    [WebMethod]
    public static Dictionary<string, string> GetCropFullKeys()
    {
        string sql_qry = "select  Distinct [Crop_Full_Key] from Ref_Budget_Default";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        //return DataTableToJsonstring(dt);
        return dt.AsEnumerable().ToDictionary<DataRow, string, string>(row => row[0].ToString(),
                                       row => row[0].ToString());

    }
    [WebMethod]
    public static Dictionary<string, string> GetAllofficenames()
    {
        string sql_qry = "select Office_ID,Office_Name from Ref_Office ";
        sql_qry += " order by Office_ID";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return dt.AsEnumerable().ToDictionary<DataRow, string, string>(row => row[0].ToString(),
                                       row => row[1].ToString());
    }
    [WebMethod]
    public static Dictionary<string, string> GetRegions()
    {
        string sql_qry = "select  Distinct [Region_ID],[Region_Name] from Ref_Region";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        //return DataTableToJsonstring(dt);
        return dt.AsEnumerable().ToDictionary<DataRow, string, string>(row => row[0].ToString(),
                                       row => row[1].ToString());

    }

    [WebMethod]
    public static void SaveReferenceBudgetViewArms(dynamic lst, string Region_id)
    {
        DataTable dtoffice = new DataTable();
        string regionid = "";
        if (Region_id == "4")
        {
            regionid = "0";
        }
        else
        {
            regionid = Region_id;
        }
        foreach (var item in lst)
        {
            string qry = "";
            int val = Convert.ToInt32(item["Actionstatus"]);
            if (val == 2)
            {
                foreach (KeyValuePair<string, dynamic> item1 in item)
                {
                    string Key = item1.Key;
                    dynamic Value = item1.Value;
                    if (Key == "0" || Key == "1" || Key == "2" || Key == "3" || Key == "4" ||
                        Key == "5" || Key == "6" || Key == "7" || Key == "8" || Key == "9" || Key == "10" ||
                        Key == "11" || Key == "12" || Key == "13" || Key == "14" || Key == "15" || Key == "16" || Key == "17" ||
                        Key == "18" || Key == "19" || Key == "20" || Key == "21" || Key == "22" || Key == "23" || Key == "24")
                    {

                        Value = (string.IsNullOrEmpty(Value)) ? "null" : "'" + Value + "'";
                        //if (string.IsNullOrEmpty(Value))
                        //{
                        //    Value = "null";
                        //}
                        //else
                        //{
                        //    Value = "'" + Value + "'";
                        //}
                        if (Key != "0")
                        {
                            string officeqry = "select Office_ID,Region_ID from Ref_Office where Office_ID ='" + Key + "'";
                            dtoffice = gen_db_utils.gp_sql_get_datatable(officeqry, dbKey);
                            if (dtoffice != null && dtoffice.Rows.Count > 0)
                            {
                                regionid = dtoffice.Rows[0][1].ToString();
                            }
                        }
                        qry = "update Ref_Budget_Default set ARM_Budget=" + Value +
                    " where Crop_Full_Key='" + item["Crop_Full_Key"].Replace("'", "''") + "' and Z_Office_ID='" + Key + "' and Z_Region_ID='" + regionid + "'" +
                    " and Budget_Expense_Type_ID='" + item["Budget_Expense_Type_ID"].Replace("'", "''") + "';";
                        int count = gen_db_utils.count_gp_sql_execute(qry, dbKey);
                        if (count == 0 && Value != "null")
                        {
                            //if (Key != "0")
                            //{
                            //    string officeqry = "select Office_ID,Region_ID from Ref_Office where Office_ID ='" + Key + "'";
                            //    dtoffice = gen_db_utils.gp_sql_get_datatable(officeqry, dbKey);
                            //    if (dtoffice != null && dtoffice.Rows.Count > 0)
                            //    {
                            //        regionid = dtoffice.Rows[0][1].ToString();
                            //    }
                            //}
                            string qry1 = " insert into Ref_Budget_Default (Crop_Full_Key,Budget_Expense_Type_ID,Z_Region_ID,Z_Office_ID,ARM_Budget)" +
                        "values('" + item["Crop_Full_Key"].Replace("'", "''") + "','" + item["Budget_Expense_Type_ID"].Replace("'", "''") + "'," +
                        "'" + regionid + "','" + Key + "'," + Value + "); ";
                            gen_db_utils.gp_sql_execute(qry1, dbKey);
                        }
                    }
                }
            }
        }
    }
}