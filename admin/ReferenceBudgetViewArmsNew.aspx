﻿<%@ Page Title="Budget Default 3D Maintainance" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="ReferenceBudgetViewArmsNew.aspx.cs" Inherits="admin_ReferenceBudgetViewArmsNew" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .ui-jqgrid .ui-jqgrid-htable th {
            height: 55px;
        }

        div.header-div {
            padding-top: 10px !important;
            display: flex;
        }

        div.header-div > *:first-child {
            left: 13px;
        }

        div.header-div > *:nth-child(2) {
            left: 20px;
        }

        div.header-div > *:last-child {
            left: 22px;
        }

        div.header-div.button-2 > *:first-child {
            left: 22px;
        }

        div.header-div.button-2 > *:last-child {
            left: 28px;
        }

        td.cvteste {
            background-color: #D0E4F5 !important;
            border: 1px solid rgb(159, 196, 230) !important;
        }

        tr.success td.cvteste,
        tr.success td.arm
        {
            background-color: #dff0d8 !important;
        }

        tr.active:not(.success) td.cvteste,
        tr.active:not(.success) td.arm{
            background-color: #f5f5f5 !important;
        }

        .bghead {
            background-image: -webkit-linear-gradient(top,#9f4c4c 0,#912c2c 100%) !important;
        }

        .ui-jqgrid tr.jqgrow td {
            height: 35px;
        }

        #jqGrid_0_0, #jqGrid_1_0, #jqGrid_2_0, #jqGrid_3_0, #jqGrid_4_0, #jqGrid_5_0, #jqGrid_6_0, #jqGrid_7_0 {
            background: #a4cff3 !important;
            border: 1px solid rgb(125, 175, 221);
        }

        .arm {
            background: #a4cff3 !important;
        }

        #content {
            padding: 25px 14px !important;
        }

        .copy-icon {
            float: right;
            height: 17px;
            color: rgba(63, 63, 161, 0.78);
            padding-right: 6px;
            font-size: 14px;
            top: -1px;
        }

        .ref-key-icon {
            float: left;
            height: 17px;
            position: relative;
            top: 1.6px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="reference-table">
        <div class="row" style="margin-left: 35px;">
            <div class="form-inline">                    
                <label for="ddlCropkeys">Crop Key :</label>
                <select id="ddlCropkeys" class="form-control" style="width: 200px">
                </select>
            </div>
        </div>
        <div class="row switch-controls">
            <div class="switch-control disabled" id="editControl">
                <label for="editBtn">Edit</label>
                <div class="toggle-btn small">            
                    <input type="checkbox" id="editBtn" class="cb-value" />
                    <span class="round-btn"></span>
                </div>
            </div>
            <div class="switch-control disabled" id="enableControl">
                <label for="enableBtn">Enable</label>
                <div class="toggle-btn small">            
                    <input type="checkbox" id="enableBtn" class="cb-value" />
                    <span class="round-btn"></span>
                </div>
            </div>
        </div>
        <div class="loader"></div>
        <div class="row main-content">
            <div>
                <table id="jqGrid"></table>
                <div id="jqGridPager"></div>
            </div>
        </div>
        <br />
        <br />
        <br />
    </div>
    <asp:HiddenField ID="Crop_Full_Key" runat="server" />

    <script type="text/javascript">  

        var DeleteRows = [];
        var colmodel = [];
        var gridwidth = 130;
        var timer;
        var colid;
        var rowidno;
        var lastSelection;
        var execute = false;
        var canEdit = false;

        var selectedRefKey = "";

        var regionNameAbbrev = {
            '1_0': '1. W',
            '3_0': '3. C',
            '4_0': '4. NE',
            '5_0': '5. E',
            '6_0': '6. SC',
            '7_0': '7. SE'
        }

        var deleteButtons = [];
        $(function () {
            $.urlParam = function (name) {
                var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
                if (results == null) {
                    return null;
                }
                return decodeURI(results[1]) || 0;
            }
            var obj = {
                Init: function () {
                    //$.jgrid.defaults.responsive = true;
                    //$.jgrid.defaults.styleUI = 'Bootstrap';
                    $('.loader').show();

                    var crop = $.urlParam('Crop_Full_Key');

                    obj.getcropkeys();
                       if ($('#ContentPlaceHolder1_Crop_Full_Key').val() != '') {
                        $('#ddlCropkeys').val($('#ContentPlaceHolder1_Crop_Full_Key').val());
                        var Crop_Full_Key = $('#ContentPlaceHolder1_Crop_Full_Key').val();
                    }

                    deleteButtons = [];

                    obj.getregions();
                    obj.loadgridcols();

                    if (crop || 0 > 0 && crop.length > 0) {
                        var cropkey = crop.split('!');
                        var cropfullkey = cropkey[0];
                        var region = cropkey[1];
                        if (cropfullkey != undefined || "") {
                            $('#ddlCropkeys').val(cropfullkey);
                        }
                        if (region != undefined || "") {
                            $('#ddlRegions').val(region);
                        }
                    }

                    obj.bindGrid();

                    $("#navbtnsave").click(function () {
                        $('.loader').show();
                        obj.save();
                    });

                    $('#navbtncolumns').click(function () {
                        $("#jqGrid").columnChooser({ height:300});
                    });

                    $('#editBtn').change(function () {
                        canEdit = $(this).is(':checked');
                        if (!canEdit) {
                            if (lastSelection != null) {
                                var grid = $("#jqGrid");
                                grid.jqGrid('saveRow', lastSelection);
                                grid.jqGrid('restoreRow', lastSelection);
                                lastSelection = null;
                            }
                        }
                    });

                    $("#navbtnadd").addClass('disabled');

                    $("#navbtndownload").click(function () {
                        $("#jqGrid").jqGrid("exportToExcel", {
                            includeLabels: true,
                            includeGroupHeader: true,
                            includeFooter: true,
                            fileName: "Budget Default 3D Maintainance.xlsx",
                            maxlength: 40 // maxlength for visible string data 
                        })
                    });

                    $("#navbtnrefresh").click(function () {
                        obj.Reload();
                    });

                    $("#txtSearchBar").on("keyup", function () {
                        var self = this;
                        obj.search(self);
                    });

                    $('a:not(#navbtnsave)').on("click", function (ev) {
                        var grid = $("#jqGrid");
                        var id = $('.inline-edit-cell').parent().parent().prop('id');
                        grid.jqGrid('saveRow', id);
                        var rowData = grid.jqGrid('getRowData', id);
                        if (rowData.Budget_Default_ID > 0) {
                            rowData.Actionstatus = 2;
                            grid.jqGrid('getGridParam', 'data')[id] = rowData;
                        }
                        var dataobj = grid.jqGrid('getGridParam', 'data');
                        var newRows = $.grep(dataobj, function (e) {
                            if (e != undefined) {
                                return e.Actionstatus == 2
                            }
                        });
                        if (newRows.length > 0) {
                            var result = confirm("Are you sure you Want to leave without Saving ?");
                            if (result == false) {
                                ev.preventDefault();
                            }
                        }

                    });
                    var previous;
                    $("#ddlCropkeys").on('focus', function () {
                        previous = this.value;
                    }).change(function () {
                        lastSelection = undefined;
                        var grid = $("#jqGrid");
                        var id = $('.inline-edit-cell').parent().parent().prop('id');
                        grid.jqGrid('saveRow', id);
                        var rowData = grid.jqGrid('getRowData', id);
                        if (rowData.Budget_Default_ID > 0) {
                            rowData.Actionstatus = 2;
                            grid.jqGrid('getGridParam', 'data')[id] = rowData;
                        }
                        var dataobj = grid.jqGrid('getGridParam', 'data');
                        var newRows = $.grep(dataobj, function (e) {
                            if (e != undefined) {
                                return e.Actionstatus == 2
                            }
                        });

                        if (newRows.length > 0) {
                            var result = confirm("Are you sure you Want to leave without Saving ?");
                            if (result == true) {
                                $('.loader').show();
                                obj.bindGrid();
                            } else {
                                $("#ddlColNames").val(previous)
                            }
                        } else {
                            $('.loader').show();
                            obj.bindGrid();
                        }
                    });
                    $("#ddlRegions").on('focus', function () {
                        previous = this.value;
                    }).change(function () {
                        var regionid = $('#ddlRegions').val();
                        lastSelection = undefined;
                        var grid = $("#jqGrid");
                        var id = $('.inline-edit-cell').parent().parent().prop('id');
                        grid.jqGrid('saveRow', id);
                        var rowData = grid.jqGrid('getRowData', id);
                        if (rowData.Budget_Default_ID > 0) {
                            rowData.Actionstatus = 2;
                            grid.jqGrid('getGridParam', 'data')[id] = rowData;
                        }
                        var dataobj = grid.jqGrid('getGridParam', 'data');
                        var newRows = $.grep(dataobj, function (e) {
                            if (e != undefined) {
                                return e.Actionstatus == 2
                            }
                        });

                        if (newRows.length > 0) {
                            var result = confirm("Are you sure you Want to leave without Saving ?");
                            if (result == true) {
                                $('.loader').show();
                                obj.bindGrid();
                            } else {
                                $("#ddlRegions").val(previous)
                            }
                        } else {
                            $('.loader').show();
                            obj.bindGrid();
                        }
                    });
                },
                getTableInfo: function () {
                    var CropFullKey = $('#ddlCropkeys :selected').text();
                    var Regionid = $('#ddlRegions').val();
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "ReferenceBudgetViewArmsNew.aspx/GetTableInfo",
                        data: "{Crop_Full_Key:'" + CropFullKey + "',Region_id:'" + Regionid + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            var data = JSON.parse(res.d);

                            var table_info = data.Table_Info[0];
                            $('#txtTableName').html(table_info.Table_Name);
                            $('#txtLastUpdated').html(table_info.Last_Updated);
                            $('#txtRecordsCount').html(table_info.Records_Count);

                            if (data.CanEdit) {
                                $('#editControl').removeClass('disabled');
                            } else {
                                $('#editControl').addClass('disabled');
                            }

                            $('#enableControl').addClass('disabled');
                            
                        }
                    });
                },
                bindGrid: function () {
                    var CropFullKey = $('#ddlCropkeys :selected').text();
                    var Regionid = $('#ddlRegions').val();
                    $.ajax({
                        type: "POST",
                        url: "ReferenceBudgetViewArmsNew.aspx/GetReferenceBudgetViewArms",
                        data: "{Crop_Full_Key:'" + CropFullKey + "',Region_id:'" + Regionid + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var resData = JSON.parse(data.d);
                            $('.loader').show();
                            for (var i = 0; i < resData.length; i++) {
                                resData[i].rowid = i + 1;
                            }
                            obj.loadgrid(resData);
                            obj.getTableInfo();
                            $('.loader').hide();
                        }
                    });
                },
                loadgrid: function (data) {
                    $.jgrid.gridUnload("#jqGrid");
                    $("#jqGrid").jqGrid({
                        datatype: "local",
                        styleUI: 'Bootstrap',
                        data: data,
                        colModel: colmodel,
                        viewrecords: true,
                        loadonce: true,
                        restoreAfterSelect: false,
                        saveAfterSelect: true,
                        sortable: true,
                        onSelectRow: obj.edit,
                        onCellSelect: function (row, col, content, event) {
                            var cm = jQuery("#jqGrid").jqGrid("getGridParam", "colModel");
                            colid = cm[col].name;
                            rowidno = row;
                        },
                        onPaging: obj.changePage,
                        height: 'auto',
                        rowNum: 100,
                        shrinkToFit: false,
                        pager: "#jqGridPager",
                        gridComplete: function () {

                            var txtid = rowidno + '_' + colid;
                            $('#' + txtid + '').focus();
                        },
                        loadComplete: function () {
                            var $grid = $("#jqGrid");
                            $grid.closest("div.ui-jqgrid-view")
                                .find("div.ui-jqgrid-hdiv table.ui-jqgrid-htable tr.ui-jqgrid-labels > th.ui-th-column > div.ui-jqgrid-sortable")
                                .each(function (index) {
                                    var idPrefix = "jqgh_" + $grid[0].id + "_";

                                    var id = $(this).attr('id');
                                    var ua = window.navigator.userAgent;
                                    var isIE = /MSIE|Trident|Edge\//.test(ua);

                                    var refKey = deleteButtons.find(a => idPrefix + a == id);
                                    if (!!refKey) {
                                        $(this).find('div').remove();

                                        var div = $('<div>').addClass('header-div');

                                        if (refKey == '0_0') {
                                            div.addClass('button-2');
                                        }

                                        div.appendTo(this);

                                        $('<i>')
                                            .addClass("fa")
                                            .addClass("fa-circle-o")
                                            .addClass('fa-lg')
                                            .addClass('ref-key-icon')
                                            .attr('aria-hidden', true)
                                            .attr('data-ref-key', refKey)
                                            .appendTo(div)
                                            .click(function (e) {
                                                obj.setSelectedRefKey(refKey);
                                                return false;
                                            });

                                        if (id != idPrefix + '0_0') {
                                            var aTag = $('<a>')
                                                .addClass("glyphicon")
                                                .addClass("glyphicon-copy")
                                                .addClass('copy-icon')
                                                .attr('title', 'Copy Selected Officer/Region or ARM budget to this region/office');                                            

                                            if (isIE) {
                                                aTag.addClass('ie');
                                            }
                                        
                                            aTag
                                            .appendTo(div)
                                            .click(function (e) {
                                                var thId = $(e.target).closest('div.ui-jqgrid-sortable')[0].id;
                                                if (thId.substr(0, idPrefix.length) === idPrefix) {
                                                    var headerId = thId.substr(idPrefix.length);
                                                    obj.copyARMData(headerId);
                                                    return false;
                                                }
                                            });
                                        } 

                                        $('<a>')
                                            .addClass("glyphicon")
                                            .addClass("glyphicon-trash")
                                            .attr('title', 'Delete budget for this Region/Office')
                                            .css({ float: "right", height: "17px", color: 'red' })
                                            .appendTo(div)
                                            .click(function (e) {
                                                var thId = $(e.target).closest('div.ui-jqgrid-sortable')[0].id;
                                                if (thId.substr(0, idPrefix.length) === idPrefix) {
                                                    var headerId = thId.substr(idPrefix.length);
                                                    obj.truncateColumn(headerId);
                                                    return false;
                                                }
                                            });
                                                                                
                                    }                              

                                });
                        }
                    });
                    $("#jqGrid").jqGrid("setFrozenColumns");
                    $("#jqGrid").trigger("resize");
                },
                setSelectedRefKey: function (ref_key) {
                    if (canEdit) {
                        selectedRefKey = ref_key;
                        $('.ref-key-icon').each(function () {
                            var val = $(this).data('ref-key');
                            if (val == selectedRefKey) {
                                $(this).addClass('fa-check-circle');
                                $(this).removeClass('fa-circle-o');
                            } else {
                                $(this).removeClass('fa-check-circle');
                                $(this).addClass('fa-circle-o');
                            }
                        });
                    }
                },
                copyARMData: function (id) {
                    if (canEdit) {
                        if (selectedRefKey) {
                            var res = confirm('Are you sure you want to copy ARM budget to this region/office?(It will override existing data.)');
                            if (res) {
                                $('.loader').show();
                                var ref_key = id;
                                var crop_key = $('#ddlCropkeys :selected').text();

                                $.ajax({
                                    type: "POST",
                                    async: false,
                                    url: "ReferenceBudgetViewArmsNew.aspx/CopyARMData",
                                    data: "{crop_key:'" + crop_key + "',ref_key:'" + ref_key + "',from_ref_key:'" + selectedRefKey + "'}",
                                    contentType: "application/json; charset=utf-8",
                                    dataType: "json",
                                    success: function (data) {
                                        toastr.success("Data Copied Sucessfully.");
                                        if (timer) { clearTimeout(timer); }
                                        timer = setTimeout(function () {
                                            $('.loader').hide();
                                            window.location.reload();
                                        }, 2000);
                                    },
                                    failure: function (response) {
                                        $('.loader').hide();
                                        var val = response.d;
                                        toastr.warning(val);
                                    }
                                });
                            }
                        } else {
                            alert("Please select an Office/Region Or ARM.")
                        }
                    }
                },
                truncateColumn: function (id) {
                    if (canEdit) {
                        var res = confirm('Are you sure you want to delete this region/office budget?');
                        if (res) {
                            $('.loader').show();
                            var ref_key = id;
                            var crop_key = $('#ddlCropkeys :selected').text();

                            $.ajax({
                                type: "POST",
                                async: false,
                                url: "ReferenceBudgetViewArmsNew.aspx/TruncateColumn",
                                data: "{crop_key:'" + crop_key + "',ref_key:'" + ref_key + "'}",
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                success: function (data) {
                                    toastr.success("Data Deleted Sucessfully.");
                                    if (timer) { clearTimeout(timer); }
                                    timer = setTimeout(function () {
                                        $('.loader').hide();
                                        window.location.reload();
                                    }, 2000);
                                },
                                failure: function (response) {
                                    $('.loader').hide();
                                    var val = response.d;
                                    toastr.warning(val);
                                }
                            });
                        }
                    }
                },
                loadgridcols: function () {
                    var colvalue = "";
                    var widthgrid = "";
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "ReferenceBudgetViewArmsNew.aspx/GetAllofficenames",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            colmodel.push({ label: 'rowid', name: 'rowid', width: 75, editable: false, key: true, hidden: true, frozen: true, hidedlg: true });
                            colmodel.push({ label: 'Sort Order', name: 'Sort_order', align: 'center', sorttype: 'number', width: 60, frozen: true, editable: false, hidden: true, hidedlg: true });
                            colmodel.push({ label: 'Expense ID', name: 'Budget_Expense_Type_ID', align: 'center', hidden: true, sorttype: 'number', frozen: true, width: 70, editable: false, hidedlg: true  });
                            colmodel.push({ label: 'Crop Full Key', name: 'Crop_Full_Key', width: 130, hidden: true, editable: false, frozen: true, hidedlg: true });
                            colmodel.push({ label: '', name: '', formatter: obj.deleteLink, align: 'center', width: 60, editable: false, frozen: true });
                            colmodel.push({ label: 'Expense Name', name: 'Budget_Expense_Name', width: 170, editable: false, frozen: true });
                            colmodel.push({
                                label: 'ARM', name: '0_0', width: 90, editable: true, hidden: false, align: 'right', classes: 'arm right',
                                editoptions: { maxlength: 20, dataEvents: [{ type: 'keypress', fn: function (event) { return isNumber(event, this) } }] },
                                formatter: 'amountFormatter'
                            });

                            deleteButtons.push('0_0');
                            obj.loadregions();

                            $.each(res.d, function (data, value) {
                                colmodel.push({
                                    label: value, name: data, width: 90, editable: true, hidden: false, align: 'right', classes: 'right',
                                    editoptions: { maxlength: 20, dataEvents: [{ type: 'keypress', fn: function (event) { return isNumber(event, this) } }] },
                                    formatter: 'amountFormatter'
                                });

                                deleteButtons.push(data);
                            });
                            colmodel.push({ label: 'Actionstatus', name: 'Actionstatus', width: 70, editable: false, hidden: true, hidedlg: true });
                        }
                    });
                },
                loadregions: function () {
                    var colvalue = "";
                    var widthgrid = "";
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "ReferenceBudgetViewArmsNew.aspx/GetAllRegionnames",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            $.each(res.d, function (data, value) {
                                colmodel.push({
                                    label: value, name: data, width: 90, editable: true, hidden: false, classes: 'cvteste right', align: 'right',
                                    editoptions: { maxlength: 20, dataEvents: [{ type: 'keypress', fn: function (event) { return isNumber(event, this) } }] },
                                    formatter: 'amountFormatter'
                                });

                                deleteButtons.push(data);
                            });
                        }
                    });
                },
                getcropkeys: function () {
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "ReferenceBudgetViewArmsNew.aspx/GetCropFullKeys",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            $.each(res.d, function (data, value) {
                                $("#ddlCropkeys").append($("<option></option>").val(data).html(value));
                            });
                        }
                    })
                },
                getregions: function () {
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "ReferenceBudgetViewArmsNew.aspx/GetRegions",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            $.each(res.d, function (data, value) {
                                $("#ddlRegions").append($("<option></option>").val(data).html(value));
                            });
                        }
                    })
                },
                add: function () {

                    var grid = $("#jqGrid");
                    var rowsperPage = parseInt(grid.getGridParam('rowNum'));
                    var gridlength = grid.jqGrid('getGridParam', 'data').length;
                    var curpage = parseInt($(".ui-pg-input").val());
                    var totPages = Math.ceil(gridlength / rowsperPage);


                    if (rowsperPage * totPages == gridlength) {
                        var id = $('.inline-edit-cell').parent().parent().prop('id');
                        grid.jqGrid('saveRow', id);
                        var row = obj.newrow();

                        var newRowId = row.rowid;
                        grid.jqGrid('addRowData', newRowId, row,"first");
                        grid.trigger('reloadGrid');
                        lastSelection = newRowId;
                        grid.jqGrid('saveRow', lastSelection);
                        grid.jqGrid('restoreRow', lastSelection);
                        $('.glyphicon-step-forward').trigger('click');
                    } else {
                        $('.glyphicon-step-forward').trigger('click');
                        var row = obj.newrow();

                        var newRowId = row.rowid;
                        grid.jqGrid('addRowData', newRowId, row,"first");
                    }


                    lastSelection = newRowId;
                    grid.jqGrid('saveRow', lastSelection);
                    grid.jqGrid('restoreRow', lastSelection);

                    var eid = $('.inline-edit-cell').parent().parent().prop('id')
                    grid.jqGrid('saveRow', eid);
                    grid.jqGrid('restoreRow', eid);
                    grid.jqGrid('editRow', newRowId);
                },
                edit: function (id) {
                    execute = true;
                    if (id && id !== lastSelection && canEdit) {
                        var grid = $("#jqGrid");
                        grid.jqGrid('saveRow', lastSelection);
                        grid.jqGrid('restoreRow', lastSelection);
                        var row = grid.jqGrid('getRowData', lastSelection);

                        if (!jQuery.isEmptyObject(row)) {
                            var dataobj = jQuery("#jqGrid").jqGrid('getGridParam', 'data');
                            index = dataobj.findIndex(x => x.rowid == row.rowid);
                            if (row.rowid > 0 && row.Actionstatus != 1) {
                                row.Actionstatus = 2;
                                grid.jqGrid('getGridParam', 'data')[index] = row;
                            }
                        }
                        grid.jqGrid('editRow', id);
                        lastSelection = id;
                        obj.focus();
                    }
                },
                focus: function () {
                    setTimeout(function () {
                        var txtid = rowidno + '_' + colid;
                        $('#' + txtid + '').focus();
                    }, 1);

                },
                newrow: function () {
                    var newid = 0;

                    var grid = $("#jqGrid");
                    var length = jQuery("#jqGrid").jqGrid('getGridParam', 'records');
                    var dataobj = grid.jqGrid('getGridParam', 'data');
                    var dataobj = grid.jqGrid('getGridParam', 'data');
                    var id = [];
                    ids = dataobj.map(function (e) { return e.rowid });
                    if (ids.length > 0) {
                        newid = ids.reduce(function (a, b) { return Math.max(a, b); });
                    }
                    var row = {
                        Sort_order: 0,
                        Actionstatus: 1,
                        rowid: newid + 1
                    };
                    return row;
                },
                save: function () {
                    $('.loader').show();
                    var grid = $("#jqGrid");
                    var allrows = [];
                    var id = $('.inline-edit-cell').parent().parent().prop('id');
                    grid.jqGrid('saveRow', id);
                    var rowData = grid.jqGrid('getRowData', id);
                    if (rowData.rowid > 0) {
                        rowData.Actionstatus = 2;
                        grid.jqGrid('getGridParam', 'data')[id] = rowData;
                    }

                    for (var i = 0; i < DeleteRows.length; i++) {
                        DeleteRows[i].Actionstatus = 3
                        allrows.push(DeleteRows[i]);
                    }
                    var dataobj = grid.jqGrid('getGridParam', 'data');
                    var newRows = $.grep(dataobj, function (e) {
                        if (e != undefined) {
                            return e.Actionstatus == 1 || e.Actionstatus == 2
                        }
                    });
                    for (var i = 0; i < newRows.length; i++) {
                        allrows.push(newRows[i]);
                    }
                    var ddlName = $('#ddlCropkeys :selected').text();
                    //var Regionid = $('#ddlRegions').val();
                    $.ajax({
                        type: "POST",
                        url: "ReferenceBudgetViewArmsNew.aspx/SaveReferenceBudgetViewArms",
                        data: JSON.stringify({ lst: allrows }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            toastr.success("Saved Sucessful");
                            $('.loader').hide();
                            if (timer) { clearTimeout(timer); }
                            timer = setTimeout(function () {
                                window.location.reload();
                            }, 2000);
                        },
                        failure: function (response) {
                            $('.loader').hide();
                            var val = response.d;
                            toastr.warning(val);
                        }
                    });
                },
                Reload: function () {
                    $('.loader').show();
                    var ddlName = $('#ddlCropkeys :selected').text();
                    var Regionid = $('#ddlRegions').val();
                    window.location.href = "ReferenceBudgetViewArmsNew.aspx?Crop_Full_Key=" + ddlName + "!" + Regionid;;
                    $('.loader').hide();
                },
                search: function (self) {
                    if (timer) { clearTimeout(timer); }
                    timer = setTimeout(function () {
                        obj.changePage();
                        $("#jqGrid").jqGrid('filterInput', self.value);
                        var length = jQuery("#jqGrid").jqGrid('getGridParam', 'records');
                        $('#txtRecordsCount').html('Records Count: ' + length);
                    }, 0);
                },
                changePage: function () {

                    lastSelection = "";
                    var grid = $("#jqGrid");
                    var id = $('.inline-edit-cell').parent().parent().prop('id');
                    if (id != undefined) {
                        grid.jqGrid('saveRow', id);
                        grid.jqGrid('restoreRow', id);
                        var row = grid.jqGrid('getRowData', id);
                        if (!jQuery.isEmptyObject(row)) {
                            var dataobj = jQuery("#jqGrid").jqGrid('getGridParam', 'data');
                            index = dataobj.findIndex(x => x.rowid == row.rowid);
                            if (row.rowid > 0 && row.Actionstatus != 1) {
                                row.Actionstatus = 2;
                                grid.jqGrid('getGridParam', 'data')[index] = row;
                            }
                        }
                    }
                },
                deleteLink: function (cellValue, options, rowdata, action) {
                    return "<a href='javascript:truncateRow(" + rowdata.Budget_Expense_Type_ID + ")' class='glyphicon glyphicon-trash' style='color:red'></a>";
                }
            }
            obj.Init();

        });
        function isNumber(evt, element) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (
                (charCode != 45 || $(element).val().indexOf('-') != -1) &&      // “-” CHECK MINUS, AND ONLY ONE.
                (charCode != 46 || $(element).val().indexOf('.') != -1) &&      // “.” CHECK DOT, AND ONLY ONE.
                (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
        function editRow(rowId) {

            var grid = $('#jqGrid');
            grid.editRow(rowId, true, function () {
                var colModel = grid.getGridParam('colMode');
                var colName = colModel[colIndex].name;
                var input = $('#' + rowId + '_' + colName);
                input.get(0).focus();
            });
        }

        function truncateRow(id) {
            if (canEdit) {
                var res = confirm('Are you sure you want to delete?');
                if (res) {
                    $('.loader').show();
                    var expense_id = id;
                    var crop_key = $('#ddlCropkeys :selected').text();

                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "ReferenceBudgetViewArmsNew.aspx/TruncateRow",
                        data: "{crop_key:'" + crop_key + "',expense_id:'" + expense_id + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            toastr.success("Data Deleted Sucessfully.");
                            $('.loader').hide();
                            if (timer) { clearTimeout(timer); }
                            timer = setTimeout(function () {
                                window.location.reload();
                            }, 2000);
                        },
                        failure: function (response) {
                            $('.loader').hide();
                            var val = response.d;
                            toastr.warning(val);
                        }
                    });
                }
            }
        }

        function deleteRecord(id) {

            if (canEdit) {
                var grid = $('#jqGrid');
                var result = confirm("Are you sure you Want to delete?");

                if (result == true) {
                    var ediId = $('.inline-edit-cell').parent().parent().prop('id');
                    grid.jqGrid('saveRow', ediId);
                    var rowData = grid.jqGrid('getRowData', ediId);
                    if (rowData.rowid > 0 && rowData.Actionstatus != 1) {
                        rowData.Actionstatus = 2;
                        grid.jqGrid('getGridParam', 'data')[ediId - 1] = rowData;
                    }

                    var row = grid.jqGrid('getRowData', id);
                    grid.jqGrid('delRowData', id);
                    if (row.Actionstatus != 1) {
                        DeleteRows.push(row);
                    }
                    var data = jQuery("#jqGrid").jqGrid('getGridParam', 'data');
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].Sort_order == 0) {
                            data[i].rowid = i + 1;
                            data[i].id = i + 1;
                        } else
                            data[i].rowid = i + 1;
                    }
                    var curpage = parseInt($(".ui-pg-input").val());
                    jQuery('#jqGrid').jqGrid('clearGridData');
                    jQuery('#jqGrid').jqGrid('setGridParam', { data: data });
                    $("#jqGrid").trigger("reloadGrid", [{ page: curpage }]);
                    $("#jqGrid").trigger("reloadGrid", [{ page: curpage }]);
                    lastSelection = id;
                }
            }
        }
        function unique(list) {
            var result = [];
            $.each(list, function (i, e) {
                if ($.inArray(e, result) == -1) result.push(e);
            });
            return result;
        }
       // $('#`').addClass('disabled');
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" runat="Server">
</asp:Content>

