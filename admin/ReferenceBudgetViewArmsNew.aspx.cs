﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;

public partial class admin_ReferenceBudgetViewArmsNew : BasePage
{
    private static DataTable dtoffices = new DataTable();
    private static DataTable ref_key_datatable = new DataTable();

    public admin_ReferenceBudgetViewArmsNew()
    {
        Table_Name = "Ref_Budget_3D";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Crop_Full_Key"] != null)
        {
            Crop_Full_Key.Value = Session["Crop_Full_Key"].ToString();
        }

        base.Page_Load(sender, e);

        if(!IsPostBack)
        {
            string query = @"select concat(Region_ID,'_',office_id) as Office_ID from Ref_Office
                             union
                             select  concat([Region_ID],'_0') as Office_ID from Ref_Region
                             union 
                            select '0_0' as Office_ID";

            ref_key_datatable = gen_db_utils.gp_sql_get_datatable(query, dbKey);
        }
    }

    [WebMethod]
    public static string GetTableInfo(string Crop_Full_Key, string Region_id)
    {
        Count_Query = "Declare @Count int; Set @Count = 10;";
        return Get_Table_Info();
    }

    [WebMethod]
    public static string GetReferenceBudgetViewArms(string Crop_Full_Key, string Region_id)
    {
        HttpContext.Current.Session["Crop_Full_Key"] = Crop_Full_Key;
        string sql_qry = @"DECLARE @cols AS NVARCHAR(MAX),
                            @query  AS NVARCHAR(MAX)

                        select @cols = STUFF((SELECT ',' + QUOTENAME(concat(Region_ID, '_' ,Office_ID))
                                            from Ref_Office
                                            --group by Office_ID
                                            order by Office_ID
                                    FOR XML PATH(''), TYPE
                                    ).value('.', 'NVARCHAR(MAX)')
                                ,1,1,'')

                        set @query = 'SELECT 0 as Actionstatus,Sort_order,Budget_Expense_Type_ID,Crop_Full_Key,Budget_Expense_Name,[0_0],[1_0],[2_0],[3_0],[4_0],[5_0],[6_0],[7_0],'+@cols+' from
                                     (
                                       select [Sort_order], RBE.[Budget_Expense_Type_ID],[Crop_Full_Key],ref_key,[Budget_Expense_Name],[ARM_Budget]
                                          from[Ref_Budget_Default] RBD join[Ref_Budget_Expense_Type] RBE on RBE.[Budget_Expense_Type_ID] = RBD.[Budget_Expense_Type_ID]
                                          where[Crop_Full_Key] =  ''" + Crop_Full_Key + @"'' and[Standard_Ind] = ''1''
                                    ) x
                                    pivot
                                    (
                                        sum(ARM_Budget)
                                        for ref_key in ([0_0],[1_0],[2_0],[3_0],[4_0],[5_0],[6_0],[7_0],'+@cols+' )
                                    ) p order by sort_order;'
                        execute(@query);";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static Dictionary<string, string> GetCropFullKeys()
    {
        string sql_qry = "select  Distinct [Crop_Full_Key] from Ref_Budget_Default  where crop_full_key != '0_0_0_0' order by [Crop_Full_Key]";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        //return DataTableToJsonstring(dt);
        return dt.AsEnumerable().ToDictionary<DataRow, string, string>(row => row[0].ToString(),
                                       row => row[0].ToString());
    }

    [WebMethod]
    public static Dictionary<string, string> GetAllofficenames()
    {
        string sql_qry = "select concat(Region_ID, '_' ,Office_ID) as OfficeID,Office_Name from Ref_Office  Where [Status] = 1";
        sql_qry += " order by Office_ID";
        dtoffices = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return dtoffices.AsEnumerable().ToDictionary<DataRow, string, string>(row => row[0].ToString(),
                                       row => row[1].ToString());
    }

    [WebMethod]
    public static Dictionary<string, string> GetAllRegionnames()
    {
        string sql_qry = "select  concat([Region_ID],'_0') as Region_ID,CONCAT([Region_ID],'. ',[Region_Name]) from Ref_Region Where [Status] = 1";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return dt.AsEnumerable().ToDictionary(row => row[0].ToString(), row => row[1].ToString());
    }

    [WebMethod]
    public static Dictionary<string, string> GetRegions()
    {
        string sql_qry = "select  Distinct [Region_ID],[Region_Name] from Ref_Region Where [Status] = 1";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        //return DataTableToJsonstring(dt);
        return dt.AsEnumerable().ToDictionary<DataRow, string, string>(row => row[0].ToString(),
                                       row => row[1].ToString());
    }

    [WebMethod]
    public static void SaveReferenceBudgetViewArms(dynamic lst)
    {
        string qry = @"DECLARE @ARM_BUDGET VARCHAR(100);";

        foreach (var item in lst)
        {
            int val = Convert.ToInt32(item["Actionstatus"]);

            if (val == 2)
            {
                foreach (KeyValuePair<string, dynamic> item1 in item)
                {
                    string Ref_Key = item1.Key;

                    if(Ref_Key != "ActionStatus" && Ref_Key != "Sort_order" && Ref_Key != "rowid")
                    {
                        dynamic ARM_Budget = item1.Value;
                        ARM_Budget = string.IsNullOrEmpty(ARM_Budget.ToString()) ? "NULL" : "'" + ARM_Budget + "'";

                        dynamic Expense_Type_ID = item["Budget_Expense_Type_ID"].Replace("'", "''");
                        dynamic Crop_Full_Key = item["Crop_Full_Key"].Replace("'", "''");

                        var row = ref_key_datatable.AsEnumerable().SingleOrDefault(a => a.Field<string>("Office_ID") == Ref_Key);
                        if (row != null)
                        {
                            string[] array = Ref_Key.Split('_');
                            var region_id = array[0];
                            var office_id = array[1];

                            qry += Environment.NewLine;
                            qry += Get_Budget_Query(Crop_Full_Key, Ref_Key, Expense_Type_ID, ARM_Budget, region_id, office_id);
                            qry += Environment.NewLine;
                        }
                    }                    
                }
            }            
        }

        if (!string.IsNullOrEmpty(qry))
        {
            gen_db_utils.gp_sql_execute(qry, dbKey);
            gen_db_utils.Base_sql_execute_sp("usp_verify_budget", dbKey);
            Update_Table_Audit();
        }        
    }

    private static string Get_Budget_Query(dynamic crop_key, dynamic ref_key, dynamic expense_id, dynamic arm_budget, dynamic region_id, dynamic office_id)
    {
        string query = @"IF EXISTS(SELECT 1 From Ref_Budget_Default Where Crop_Full_Key = '{0}' and Ref_Key = '{1}' And Budget_Expense_Type_Id = '{2}')
                            BEGIN
	                            UPDATE [dbo].[Ref_Budget_Default]
                                 SET [ARM_Budget] = {3}
                               WHERE Crop_Full_Key = '{0}' And Ref_Key = '{1}' And Budget_Expense_Type_ID = '{2}'
                            END
                         ELSE
                            BEGIN                                
                                SET @ARM_BUDGET = {3}
	                            IF @ARM_BUDGET IS NOT NULL
                                    BEGIN
                                        INSERT INTO [dbo].[Ref_Budget_Default]
                                            (
                                                [Crop_Practice_ID]
                                                ,[Crop_Full_Key]
                                                ,[Budget_Expense_Type_ID]
                                                ,[ARM_Budget]
                                                ,[Distributor_Budget]
                                                ,[Third_Party_Budget]
                                                ,[Z_Crop_Code]
                                                ,[Z_Practice_Type_Code]
                                                ,[Z_Region_ID]
                                                ,[Z_Office_ID]
                                                ,[Status]
                                                ,[ref_key]
                                            )
                                        SELECT
                                             Crop_And_Practice_ID
                                            ,'{0}'
                                            ,'{2}'
                                            ,{3}
                                            ,NULL
                                            ,NULL
                                            ,Crop_Code
                                            ,Irr_Prac_Code
                                            ,'{4}'
                                            ,'{5}'
                                            ,1
                                            ,'{1}'
                                        FROM ( Select '{0}' as [Crop_Full_Key] ) A
										LEFT JOIN V_Crop_Price_Details VC on A.Crop_Full_Key = VC.Crop_Full_Key
                                    END
                            END";

        query = string.Format(query, crop_key, ref_key, expense_id, arm_budget, region_id, office_id);

        return query;
    }

    [WebMethod]
    public static void TruncateColumn(string ref_key, string crop_key)
    {
        string query = "Delete From Ref_Budget_Default Where Crop_Full_Key = '" + crop_key + "' And Ref_Key = '" + ref_key + "'";
        gen_db_utils.gp_sql_execute(query, dbKey);
        Update_Table_Audit();
    }

    [WebMethod]
    public static void CopyARMData(string ref_key, string crop_key, string from_ref_key)
    {
        string query = string.Empty;

        string[] array = ref_key.Split('_');
        var region_id = array[0];
        var office_id = array[1];

        query = @"INSERT INTO [dbo].[Ref_Budget_Default]
                  (
                        [Crop_Practice_ID]
                       ,[Crop_Full_Key]
                       ,[Budget_Expense_Type_ID]
                       ,[ARM_Budget]
                       ,[Distributor_Budget]
                       ,[Third_Party_Budget]
                       ,[Z_Crop_Code]
                       ,[Z_Practice_Type_Code]
                       ,[Z_Region_ID]
                       ,[Z_Office_ID]
                       ,[Status]
                       ,[ref_key]
                 )
                 SELECT 
		             [Crop_Practice_ID],
		             [Crop_Full_Key],
		             RB.[Budget_Expense_Type_ID],
		             [ARM_Budget],
		             0,
		             0,
		             [Z_Crop_Code],
		             [Z_Practice_Type_Code],
		             '" + region_id + @"',
		             '" + office_id + @"',
		             1,
		             '" + ref_key + @"'
	             FROM Ref_Budget_Default RB
	             JOIN Ref_Budget_Expense_Type RBET ON RB.Budget_Expense_Type_ID = RBET.Budget_Expense_Type_ID
	             Where Crop_Full_Key = '" + crop_key + @"' 
                 AND Ref_Key = '" + from_ref_key + @"'
                 AND (RBET.Standard_Ind = 1 OR RBET.Standard_Ind = 2)
                 AND RB.Budget_Expense_Type_ID NOT IN 
				 ( 
					SELECT Budget_Expense_Type_ID From [dbo].[Ref_Budget_Default] Where Crop_Full_Key = '" + crop_key + @"' and Ref_Key = '" + ref_key + @"'
				 )";

        query += Environment.NewLine;

        query += @"UPDATE Ref_Budget_Default
                   SET ARM_Budget = RB1.ARM_Budget
                  FROM Ref_Budget_Default RB2
                  JOIN 
                  ( 
	                  SELECT RB.Budget_Expense_Type_ID, RB.ARM_Budget, RB.Crop_Full_Key, RB.ref_key FROM Ref_Budget_Default RB
	                  JOIN Ref_Budget_Expense_Type RBET on RB.Budget_Expense_Type_ID = RBET.Budget_Expense_Type_ID
	                  Where Crop_Full_Key = '" + crop_key + @"' and Ref_Key = '" + from_ref_key + @"' and (RBET.Standard_Ind = 1 OR RBET.Standard_Ind = 2)
                  ) as RB1 ON RB1.Budget_Expense_Type_ID = RB2.Budget_Expense_Type_ID
                  Where RB2.Crop_Full_Key = '" + crop_key + @"' and RB2.Ref_Key = '" + ref_key + @"'";

        gen_db_utils.gp_sql_execute(query, dbKey);

        Update_Table_Audit();
    }

    [WebMethod]
    public static void TruncateRow(string expense_id, string crop_key)
    {
        string query = string.Empty;

        query = "Update Ref_Budget_Default Set ARM_Budget = NULL Where Crop_Full_Key = '" + crop_key + "' And Budget_Expense_Type_ID = '" + expense_id + "'";
        gen_db_utils.gp_sql_execute(query, dbKey);

        Update_Table_Audit();
    }
}