﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_ReferenceCache : System.Web.UI.Page
{
    static string dbKey = "gp_conn";
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [WebMethod]
    public static string BindReferenceCacheDetails()
    {
        string sql_qry = "select Ref_Cache_ID,Ref_Cache,Cache_Time from Ref_Cache";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static void SaveReferenceCacheDetails(dynamic lst)
    {
        foreach (var item in lst)
        {
            string qry = "";
            int val = Convert.ToInt32(item["Actionstatus"]);
            if (val == 1)
            {
               // Ref_Cache_ID,Ref_Cache,Cache_Time

                   qry = "INSERT INTO Ref_Cache (Ref_Cache,Cache_Time) " +
               " VALUES('" + ManageQuotes(item["Ref_Cache"]) + "','" + ManageQuotes(item["Cache_Time"]) + "')";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 2)
            {  
                qry = "UPDATE Ref_Cache SET Ref_Cache = '" + ManageQuotes(item["Ref_Cache"]) + "',[Cache_Time] = '" + ManageQuotes(item["Cache_Time"]) + "' where Ref_Cache_ID=" + item["Ref_Cache_ID"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 3)
            {
                qry = "DELETE FROM Ref_Cache where Ref_Cache_ID=" + item["Ref_Cache_ID"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
        }
    }

    public static string ManageQuotes(string value)
    {
        if (!String.IsNullOrEmpty(value) && value.Contains("'"))
        {
            value = value.Replace("'", "''");
        }
        return value;
    }
}