﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Services;

public partial class admin_ReferenceChevron : BasePage
{
    public admin_ReferenceChevron()
    {
        Table_Name = "Ref_Chevron";
    }

    [WebMethod]
    public static string GetTableInfo()
    {
        return Get_Table_Info();
    }

    [WebMethod]
    public static string BindChevronDetails()
    {
        string sql_qry = "SELECT  Chevron_ID, Chevron_Name, Tab_ID, Sort_Order, Status  FROM  Ref_Chevron order by Tab_ID, Chevron_ID ";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static void SaveChevronDetails(dynamic lst)
    {
        foreach (var item in lst)
        {
            string qry = "";
            int val = Convert.ToInt32(item["Actionstatus"]);
            if (val == 1)
            {
                qry = "INSERT INTO Ref_Chevron (Chevron_Name, Tab_ID, Sort_Order, Status ) " +
                    " VALUES('" + ManageQuotes(item["Chevron_Name"]) + "','" + ManageQuotes(item["Tab_ID"]) + "','" + ManageQuotes(item["Sort_Order"]) + "','" + ManageQuotes(item["Status"]) + "')";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 2)
            {
                qry = "UPDATE Ref_Chevron SET Chevron_Name = '" + ManageQuotes(item["Chevron_Name"]) + "',[Tab_ID] = '" + ManageQuotes(item["Tab_ID"]) + "',[Sort_Order] = '" + ManageQuotes(item["Sort_Order"]) + "' ,[Status] = '" + (item["Status"]) + "'   where Chevron_ID=" + item["Chevron_ID"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 3)
            {
                qry = "DELETE FROM Ref_Chevron where Chevron_ID=" + item["Chevron_ID"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
        }

        gen_db_utils.Update_Table_Audit_Trail("Ref_Chevron", userid, dbKey);
    }

    public static string ManageQuotes(string value)
    {
        if (!string.IsNullOrEmpty(value) && value.Contains("'"))
        {
            value = value.Replace("'", "''");
        }
        return value;
    }
}