﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_ReferenceCondition : System.Web.UI.Page
{
    static string dbKey = "gp_conn";
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    [WebMethod]
    public static string BindRefcondition()
    {
        string sql_qry = "select Condition_ID,Condition_Name,Condition_Level,Sort_Order," +
            "Standard_Condition_Ind,Document_ID,Association_Type,Status,Multiple from Ref_Conditions order by Condition_ID";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
    }

    [WebMethod, ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static void SaveRefconditionDetails(dynamic lst)
    {
        foreach (var item in lst)
        {
            string qry = "";
            int val = Convert.ToInt32(item["Actionstatus"]);
            if (val == 1)
            {
               // qry = "INSERT INTO V_Crop_Price_Details ([Crop_Code],[Crop_Name],[Crop_Type_Code] ,[Crop_Type_Name],[Practice_type_code],[Crop_Full_Key],[Irr_NI_Ind],[State_ID],[RMA_Ref_Yield],[Std_Crop_Type_Practice_Type_Ind],[Final_Planting_Date],[Region_ID],[Office_ID],[Crop_Year],[MPCI_UOM],[Price],[Status],[IsDelete]) " +
               //     " VALUES('" + ManageQuotes(item["Crop_Code"]) + "','" + ManageQuotes(item["Crop_Name"]) + "','" + ManageQuotes(item["Crop_Type_Code"]) + "','" + ManageQuotes(item["Crop_Type_Name"]) + "','" + ManageQuotes(item["Practice_type_code"]) + "','" + ManageQuotes(item["Crop_Full_Key"]) + "','" + ManageQuotes(item["Irr_NI_Ind"]) + "' ,'" + ManageQuotes(item["State_ID"]) + "','" + ManageQuotes(item["RMA_Ref_Yield"]) + "','" + ManageQuotes(item["Std_Crop_Type_Practice_Type_Ind"]) + "','" + ManageQuotes(item["Final_Planting_Date"]) + "','" + ManageQuotes(item["Region_ID"]) + "','" + ManageQuotes(item["Office_ID"]) + "','" + ManageQuotes(item["Crop_Year"]) + "'  ,'" + ManageQuotes(item["MPCI_UOM"]) + "','" + ManageQuotes(item["Price"]) + "','" + ManageQuotes(item["Status"]) + "','" + ManageQuotes(item["IsDelete"]) + "')";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 2)
            {

                //,,,,
                //,,,,
                qry = "UPDATE Ref_Conditions SET [Condition_Name] = '" + ManageQuotes(item["Condition_Name"]) + "' ,[Condition_Level] = '" + ManageQuotes(item["Condition_Level"]) + "',[Sort_Order] = '" + ManageQuotes(item["Sort_Order"]) + "' ,[Standard_Condition_Ind] = '" + ManageQuotes(item["Standard_Condition_Ind"]) + "' ,[Document_ID] = '" + ManageQuotes(item["Document_ID"]) + "' ,[Association_Type] = '" + ManageQuotes(item["Association_Type"]) + "' ,Status = '" + (item["Status"]) + "', [Multiple] = '" + ManageQuotes(item["Multiple"]) + "'  where Condition_ID=" + item["Condition_ID"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 3)
            {
                qry = "DELETE FROM V_Crop_Price_Details  where Crop_And_Practice_ID=" + item["Crop_And_Practice_ID"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }

        }
    }

    public static string ManageQuotes(string value)
    {
        if (!String.IsNullOrEmpty(value) && value.Contains("'"))
        {
            value = value.Replace("'", "''");
        }
        return value;
    }
}