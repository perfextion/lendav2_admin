﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_ReferenceConditionView : System.Web.UI.Page
{
    static string dbKey = "gp_conn";
    protected void Page_Load(object sender, EventArgs e)
    {
        lbl_table.Text = LoadTable("");
    }

    [WebMethod]
    public static string LoadTable(string Job_Title_ID)
    {

        string str_sql = "";
        string str_return = "";

        str_sql = "select Condition_ID,Condition_Name,Condition_Level,Sort_Order,Multiple from " +
                "Ref_Conditions order by Condition_Level,Sort_Order";

        DataTable dt1 = gen_db_utils.gp_sql_get_datatable(str_sql, dbKey);
        var distinctValues = dt1.AsEnumerable()
                          .Select(row => new
                          {
                              Condition_Level = row.Field<int>("Condition_Level")
                          })
                          .Distinct();
        foreach (var item in distinctValues)
        {
            if (item.Condition_Level == 1)
            {
                str_return += "<div>" +
                              "<div> <strong><h3><b style=font-size:14px;> Condition Level " + item.Condition_Level + ". Prerequisite Documents </b><h3></div>";
                var conditionnames = dt1.AsEnumerable()
                  .Where(s => s["Condition_Level"].Equals(item.Condition_Level))
                  .Select(row => new
                  {
                      Condition_ID = row.Field<int>("Condition_ID"),
                      Condition_Name = row.Field<string>("Condition_Name"),
                      Multiple = row.Field<int>("Multiple")
                  });
                foreach (var subitems in conditionnames)
                {
                    if (subitems.Multiple == 0)
                    {
                        str_return += "<div>" +
                                  //"<li>" + subitems.ParentQuestionID + "). " + subitems.ParentQuestionText + "</li>";
                                  "<div> <h5><b style=font-weight:500;font-size:14px;>&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;" + subitems.Condition_ID + ". " + subitems.Condition_Name + "</b></h5></div>";
                    }
                    else
                    {
                        str_return += "<div>" +
                                 //"<li>" + subitems.ParentQuestionID + "). " + subitems.ParentQuestionText + "</li>";
                                 "<div> <h5><b style=font-weight:600;font-size:14px;>&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;" + subitems.Condition_ID + ". " + subitems.Condition_Name + "</b></h5></div>";

                    }
                }
            }
            else if (item.Condition_Level == 2)
            {
                str_return += "<div>" +
                                "<div> <strong><h3><b style=font-size:14px;> Condition Level " + item.Condition_Level + ". Prerequisite Documents </b><h3></div>";
                var conditionnames = dt1.AsEnumerable()
                  .Where(s => s["Condition_Level"].Equals(item.Condition_Level))
                  .Select(row => new
                  {
                      Condition_ID = row.Field<int>("Condition_ID"),
                      Condition_Name = row.Field<string>("Condition_Name"),
                      Multiple = row.Field<int>("Multiple")
                  });
                foreach (var subitems in conditionnames)
                {
                    if (subitems.Multiple == 0)
                    {
                        str_return += "<div>" +
                                  //"<li>" + subitems.ParentQuestionID + "). " + subitems.ParentQuestionText + "</li>";
                                  "<div> <h5><b style=font-weight:500;font-size:14px;>&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;" + subitems.Condition_ID + ". " + subitems.Condition_Name + "</b></h5></div>";
                    }
                    else
                    {
                        str_return += "<div>" +
                                 //"<li>" + subitems.ParentQuestionID + "). " + subitems.ParentQuestionText + "</li>";
                                 "<div> <h5><b style=font-weight:600;font-size:14px;>&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;" + subitems.Condition_ID + ". " + subitems.Condition_Name + "</b></h5></div>";

                    }
                }
            }
        }
         str_return += "<br /><br /><br /><br /></div></div></div></div></div></div>";
        return str_return;
    }
}