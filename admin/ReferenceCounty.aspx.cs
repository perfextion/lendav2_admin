﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_ReferenceCounty : BasePage
{
    public admin_ReferenceCounty()
    {
        Table_Name = "Ref_County";
    }

    [WebMethod]
    public static string GetTableInfo()
    {
        return Get_Table_Info();
    }

    [WebMethod]
    public static string GetReferenceCounty()
    {
        string sql_qry = "SELECT 0 as Actionstatus,County_ID,County_Name,rc.State_ID,Z_State_Abbr,Contains_Rated_Land,Status,State_Name,state_abbrev FROM Ref_County rc join Ref_State rs on rc.State_ID = rs.State_ID";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static Dictionary<string, string> GetStates()
    {
        string sql_qry = "select CAST (State_ID as Varchar) as [State_ID], State_Abbrev from Ref_State";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return dt.AsEnumerable().ToDictionary(a => a.Field<string>("State_ID"), a => a.Field<string>("State_Abbrev"));
    }


    [WebMethod, ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static void SaveReferenceCounty(dynamic lst)
    {
        foreach (var item in lst)
        {
            string qry = "";
            int val = Convert.ToInt32(item["Actionstatus"]);
            if (val == 1)
            {
                qry = "INSERT INTO Ref_County(County_Name,State_ID,Z_State_Abbr,Contains_Rated_Land,Status)" +
                    " VALUES('" + ManageQuotes(item["County_Name"]) + "','" + ManageQuotes(item["State_ID"]) + "','" + ManageQuotes(item["Z_State_Abbr"]) + "','" + ManageQuotes(item["Contains_Rated_Land"]) + "','" + ManageQuotes(item["Status"]) + "')";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 2)
            {
                qry = "UPDATE Ref_County SET County_Name = '" + ManageQuotes(item["County_Name"]) + "' ,State_ID = '" + ManageQuotes(item["State_ID"]) + "' ,Z_State_Abbr = '" + ManageQuotes(item["Z_State_Abbr"]) + "',Contains_Rated_Land = '" + ManageQuotes(item["Contains_Rated_Land"]) + "' ,Status = '" + ManageQuotes(item["Status"]) + "' where County_ID=" + item["County_ID"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 3)
            {
                qry = "DELETE FROM Ref_County  where County_ID=" + item["County_ID"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
        }

        gen_db_utils.Update_Table_Audit_Trail("Ref_County",userid, dbKey);
    }

    public static string ManageQuotes(string value)
    {
        if (value.Contains("'"))
        {
            value = value.Replace("'", "''");
        }
        return value;
    }
}