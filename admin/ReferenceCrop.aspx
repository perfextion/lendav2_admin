﻿<%@ Page Title="Crop Price Default" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="ReferenceCrop.aspx.cs" Inherits="admin_ReferenceCrop" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
     <style>
        .ui-jqgrid .ui-jqgrid-htable th div {
            height: auto;
            overflow: hidden;
            padding-right: 4px;
            padding-top: 2px;
            position: relative;
            vertical-align: text-top;
            white-space: normal !important;
        }

        .cvteste {
            padding-right: 14px !important;
        }

        .right {
            padding-left: 14px !important;
        }

        #content {
            padding: 14px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="ref-crop-price-default reference-table">
        <div class="row switch-controls">
            <div class="switch-control disabled" id="editControl">
                <label for="editBtn">Edit</label>
                <div class="toggle-btn small">            
                    <input type="checkbox" id="editBtn" class="cb-value" />
                    <span class="round-btn"></span>
                </div>
            </div>
            <div class="switch-control disabled" id="enableControl">
                <label for="enableBtn">Enable</label>
                <div class="toggle-btn small">            
                    <input type="checkbox" id="enableBtn" class="cb-value" />
                    <span class="round-btn"></span>
                </div>
            </div>
        </div>
        <div class="loader"></div>
        <div class="row main-content" style="position: relative">
            <div id="truncateTable" style="position: absolute; top: -25px; display: none">
                <a href="javascript:truncatetable()" class='glyphicon glyphicon-trash' style='color:red'></a>
            </div>
            <div>
                <table id="jqGrid"></table>
                <div id="jqGridPager"></div>
            </div>
        </div>
        <br />
        <br />
        <br />
        <br />
    </div>

    <script type="text/javascript">  
        var DeleteRows = [];
        var timer;
        var lastSelection;
        var regionList = {};
        var canEdit = false;

        $(function () {
            var obj = {
                Init: function () {
                    obj.getRegionList();
                    obj.bindgrid();

                    $("#navbtnadd").click(function () {
                        obj.add();
                    });

                    $("#navbtnsave").click(function () {
                        $('.loader').show();
                        obj.save();
                    });

                    $("#navbtnrefresh").click(function () {
                        $('.loader').show();
                        obj.bindgrid();
                    });

                    $('#navbtncolumns').click(function () {
                        $("#jqGrid").columnChooser({});
                    });

                    $("#navbtndownload").click(function () {
                        $("#jqGrid").jqGrid("exportToExcel", {
                            includeLabels: true,
                            includeGroupHeader: true,
                            includeFooter: true,
                            fileName: "Crop Price Default.xlsx",
                            maxlength: 40
                        })
                    });

                    $("#txtSearchBar").on("keyup", function () {
                        var self = this;
                        obj.search(self);
                    });

                    $('#editBtn').change(function () {
                        canEdit = $(this).is(':checked');
                        if (!canEdit) {
                            if (lastSelection != null) {
                                var grid = $("#jqGrid");
                                grid.jqGrid('saveRow', lastSelection);
                                grid.jqGrid('restoreRow', lastSelection);
                                lastSelection = null;
                            }
                            $('#navbtnupload').addClass('disabled');
                        } else {
                            $('#navbtnupload').removeClass('disabled');
                        }
                    });                    
                },
                getRegionList: function () {
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "ReferenceCrop.aspx/GetRegionList",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            regionList = JSON.parse(data.d);
                            regionList['0'] = 'ARM';
                        }
                    });
                },
                getTableInfo: function () {
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "ReferenceCrop.aspx/GetTableInfo",
                        data: JSON.stringify({ }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            var data = JSON.parse(res.d);

                            var table_info = data.Table_Info[0];
                            $('#txtTableName').html(table_info.Table_Name);
                            $('#txtLastUpdated').html(table_info.Last_Updated);
                            $('#txtRecordsCount').html(table_info.Records_Count);

                            if (data.CanEdit) {
                                $('#editControl').removeClass('disabled');
                            } else {
                                $('#editControl').addClass('disabled');
                            }

                            $('#enableControl').addClass('disabled');
                            
                        }
                    });
                },
                bindgrid: function () {
                    $.ajax({
                        type: "POST",
                        url: "ReferenceCrop.aspx/BindReferenceCrop",
                        data: JSON.stringify({ }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var resData = JSON.parse(data.d);
                            $('.loader').show();
                            for (var i = 0; i < resData.length; i++) {
                                resData[i].rowid = i + 1;
                            }
                            obj.loadgrid(resData);
                            obj.getTableInfo();
                            $('.loader').hide();
                        }
                    });
                },
                loadgrid: function (data) {
                    $('.loader').show();
                    $.jgrid.gridUnload("#jqGrid");
                    $("#jqGrid").jqGrid({
                        datatype: "local",
                        styleUI: 'Bootstrap',
                        data: data,
                        colModel: [
                            { label: 'rowid', name: 'rowid', width: 75, editable: false, key: true, hidden: true, hidedlg: true  },
                            { label: 'Crop ID', name: 'Crop_ID', width: 120, sorttype: 'number', align: 'center', editable: false },
                            { label: 'Crop Code', name: 'Crop_Code', width: 150, align: 'center', classes: "center", editable: true },
                            { label: 'Crop Name', name: 'Crop_Name', width: 200, align: 'left', classes: "right", editable: false, hidden: true, hidedlg: true },
                            {
                                label: 'Crop Type',
                                name: 'Crop_Type',
                                width: 180,
                                classes: "right",
                                align: 'left',
                                editable: true
                            },
                            {
                                label: 'Region',
                                name: 'Region_ID',
                                width: 120,
                                align: 'left',
                                editable: true,
                                classes: "right",
                                edittype: 'select',
                                formatter: 'select',
                                editoptions: {
                                    value: regionList
                                },
                                hidden: true
                            },
                            {
                                label: 'Crop Price',
                                name: 'Crop_Price',
                                width: 150,
                                align: 'right',
                                classes: 'cvteste',
                                editable: true,
                                editoptions: {
                                    maxlength: 10,
                                    dataEvents: [
                                        {
                                            type: 'keypress',
                                            fn: function (e) {
                                                return NumberOnly(e, this);
                                            }
                                        }
                                    ]
                                },
                                formatter: 'priceFormatter'
                            },
                            {
                                label: 'Sort Order',
                                name: 'Sort_Order',
                                sorttype: 'number',
                                align: 'center',
                                width: 90,
                                editable: true,
                                editoptions: {
                                    maxlength: 10,
                                    dataEvents: [
                                        {
                                            type: 'keypress',
                                            fn: function (e) {
                                                return NumberOnly(e, this, false);
                                            }
                                        }
                                    ]
                                }
                            },
                            {
                                label: 'Status',
                                name: 'Status',
                                width: 90,
                                align: 'center',
                                editable: true,
                                editoptions: {
                                    maxlength: 1,
                                    dataEvents: [
                                        {
                                            type: 'keypress',
                                            fn: function (e) {
                                                return NumberOnly(e, this, false);
                                            }
                                        }
                                    ]
                                }
                            },
                            { label: 'Actionstatus', name: 'Actionstatus', width: 70, editable: false, hidden: true, hidedlg: true },
                            { label: '', name: '', width: 60, align: 'center', formatter: obj.deleteLink, exportcol: false, hidedlg: true  },
                        ],
                        viewrecords: true,
                        loadonce: true,
                        restoreAfterSelect: false,
                        saveAfterSelect: true,
                        sortable: true,
                        onSelectRow: obj.edit,
                        onPaging: obj.changePage,
                        height: 'auto',
                        rowNum: 100,
                        pager: "#jqGridPager",
                        loadComplete: function () {
                            obj.showDeleteButton();
                        }
                    });
                },
                showDeleteButton: function () {
                    var width = $('#gbox_jqGrid').width();
                    $('#truncateTable').css('left', width + 17 + 'px');
                    $('#truncateTable').show();
                },
                add: function () {

                    var grid = $("#jqGrid");

                    var rowsperPage = parseInt(grid.getGridParam('rowNum'));
                    var gridlength = grid.jqGrid('getGridParam', 'data').length;
                    var curpage = parseInt($(".ui-pg-input").val());
                    var totPages = Math.ceil(gridlength / rowsperPage);

                    //if (rowsperPage * curpage == gridlength) {
                    if (rowsperPage * totPages == gridlength) {
                        var id = $('.inline-edit-cell').parent().parent().prop('id');
                        grid.jqGrid('saveRow', id);
                        var row = obj.newrow();
                        // var newRowId = grid.jqGrid('getGridParam', 'data').length + 1;
                        var newRowId = row.rowid;
                        grid.jqGrid('addRowData', newRowId, row, "first");
                        grid.trigger('reloadGrid');
                        lastSelection = newRowId;
                        grid.jqGrid('saveRow', lastSelection);
                        grid.jqGrid('restoreRow', lastSelection);
                        $('.glyphicon-step-forward').trigger('click');

                    } else {

                        $('.glyphicon-step-forward').trigger('click');
                        var row = obj.newrow();
                        // var newRowId = grid.jqGrid('getGridParam', 'data').length + 1;
                        var newRowId = row.rowid;
                        grid.jqGrid('addRowData', newRowId, row, "first");
                    }


                    lastSelection = newRowId;
                    grid.jqGrid('saveRow', lastSelection);
                    grid.jqGrid('restoreRow', lastSelection);

                    var eid = $('.inline-edit-cell').parent().parent().prop('id')
                    grid.jqGrid('saveRow', eid);
                    grid.jqGrid('restoreRow', eid);
                    grid.jqGrid('editRow', newRowId);
                },
                edit: function (id) {
                    if (id && id !== lastSelection && canEdit) {
                        var grid = $("#jqGrid");

                        grid.jqGrid('saveRow', lastSelection);
                        grid.jqGrid('restoreRow', lastSelection);
                        var row = grid.jqGrid('getRowData', lastSelection);

                        if (!jQuery.isEmptyObject(row)) {
                            var dataobj = jQuery("#jqGrid").jqGrid('getGridParam', 'data');
                            index = dataobj.findIndex(x => x.rowid == row.rowid);
                            if (row.rowid > 0 && row.Actionstatus != 1) {
                                row.Actionstatus = 2;
                                grid.jqGrid('getGridParam', 'data')[index] = row;
                            }
                        }

                        grid.jqGrid('editRow', id);
                        lastSelection = id;
                    }
                },
                newrow: function () {
                    var newid = 0;
                    var grid = $("#jqGrid");
                    var length = jQuery("#jqGrid").jqGrid('getGridParam', 'records');
                    var dataobj = grid.jqGrid('getGridParam', 'data');
                    var dataobj = grid.jqGrid('getGridParam', 'data');

                    var Crop_ID = [];

                    Crop_IDs = dataobj.map(function (e) { return e.rowid });
                    if (Crop_IDs.length > 0) {
                        newid = Crop_IDs.reduce(function (a, b) { return Math.max(a, b); });
                    }

                    var row = {
                        Actionstatus: 1,
                        rowid: newid + 1,
                        Crop_ID: 0,
                        Crop_Code: "",
                        Region_ID: 0,
                        Crop_Name: "",
                        Status: 1,
                        Crop_Price: 0,
                        Sort_Order: Math.max(...dataobj.map(a => a.Sort_Order)) + 1
                    };

                    return row;
                },
                save: function () {
                    var regionId = $("#ddlRegion").val();
                    $('.loader').show();
                    var grid = $("#jqGrid");
                    var allrows = [];


                    var id = $('.inline-edit-cell').parent().parent().prop('id');
                    grid.jqGrid('saveRow', id);

                    var rowData = grid.jqGrid('getRowData', id);
                    if (rowData.rowid > 0) {
                        rowData.Actionstatus = 2;
                        grid.jqGrid('getGridParam', 'data')[id] = rowData;
                    }

                    for (var i = 0; i < DeleteRows.length; i++) {
                        DeleteRows[i].Actionstatus = 3
                        allrows.push(DeleteRows[i]);
                    }
                    var dataobj = grid.jqGrid('getGridParam', 'data');
                    var newRows = $.grep(dataobj, function (e) {
                        if (e != undefined) {
                            return e.Actionstatus == 1 || e.Actionstatus == 2
                        }
                    });
                    for (var i = 0; i < newRows.length; i++) {
                        allrows.push(newRows[i]);
                    }

                    $.ajax({
                        type: "POST",
                        url: "ReferenceCrop.aspx/SaveReferenceCrop",
                        data: JSON.stringify({ lst: allrows }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            toastr.success("Saved Sucessful");
                            setTimeout(function () {
                                location.reload();
                            }, 1000);
                        },
                        failure: function (response) {
                            console.log(response.d);
                        }

                    });
                },
                search: function (self) {
                    if (timer) { clearTimeout(timer); }
                    timer = setTimeout(function () {
                        obj.changePage();
                        $("#jqGrid").jqGrid('filterInput', self.value);
                        var length = jQuery("#jqGrid").jqGrid('getGridParam', 'records');
                        $('#txtRecordsCount').html('Records Count: ' + length);
                    }, 0);
                },
                changePage: function () {

                    lastSelection = "";
                    var grid = $("#jqGrid");

                    var id = $('.inline-edit-cell').parent().parent().prop('id');
                    if (id != undefined) {


                        grid.jqGrid('saveRow', id);
                        grid.jqGrid('restoreRow', id);
                        var row = grid.jqGrid('getRowData', id);
                        if (!jQuery.isEmptyObject(row)) {
                            var dataobj = jQuery("#jqGrid").jqGrid('getGridParam', 'data');
                            index = dataobj.findIndex(x => x.rowid == row.rowid);
                            if (row.rowid > 0 && row.Actionstatus != 1) {
                                row.Actionstatus = 2;
                                grid.jqGrid('getGridParam', 'data')[index] = row;
                            }
                        }
                    }
                },

                deleteLink: function (cellValue, options, rowdata, action) {
                    return "<a href='javascript:deleteRecord(" + options.rowId + ")' class='glyphicon glyphicon-trash' style='color:red'></a>";
                }
            }
            obj.Init();
        });
        function deleteRecord(id) {

            if (canEdit) {
                var grid = $('#jqGrid');
                var result = confirm("Are you sure you Want to delete?");

                if (result == true) {
                    var ediId = $('.inline-edit-cell').parent().parent().prop('id');
                    grid.jqGrid('saveRow', ediId);
                    var rowData = grid.jqGrid('getRowData', ediId);
                    if (rowData.rowid > 0 && rowData.Actionstatus != 1) {
                        rowData.Actionstatus = 2;
                        grid.jqGrid('getGridParam', 'data')[ediId - 1] = rowData;
                    }

                    var row = grid.jqGrid('getRowData', id);
                    grid.jqGrid('delRowData', id);
                    if (row.Actionstatus != 1) {
                        DeleteRows.push(row);
                    }
                    var data = jQuery("#jqGrid").jqGrid('getGridParam', 'data');
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].Crop_ID == 0) {
                            data[i].rowid = i + 1;
                            data[i].id = i + 1;
                        } else
                            data[i].rowid = i + 1;
                    }
                    var curpage = parseInt($(".ui-pg-input").val());


                    jQuery('#jqGrid').jqGrid('clearGridData');
                    jQuery('#jqGrid').jqGrid('setGridParam', { data: data });
                    $("#jqGrid").trigger("reloadGrid", [{ page: curpage }]);
                    $("#jqGrid").trigger("reloadGrid", [{ page: curpage }]);
                    lastSelection = id;
                }
            }
        }

        function unique(list) {
            var result = [];
            $.each(list, function (i, e) {
                if ($.inArray(e, result) == -1) result.push(e);
            });
            return result;
        }

        function Upload_Excel_Data(data) {
            if (canEdit) {
                $.ajax({
                    type: "POST",
                    url: "ReferenceCrop.aspx/UploadExcel",
                    data: JSON.stringify({ list: data }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (res) {
                        toastr.success("Data Uploaded Sucessfully");
                        if (timer) { clearTimeout(timer); }
                        timer = setTimeout(function () {
                            location.reload();
                        }, 1000);
                    },
                    failure: function (response) {
                        $('.loader').hide();
                        var val = response.d;
                        toastr.warning(val);
                    }
                });
            }
        }

        function truncatetable() {
            if (canEdit) {
                var result = confirm('Are you sure you want to delete all record?');
                if (result) {
                    $('.loader').show();
                    $.ajax({
                        type: "POST",
                        url: "ReferenceCrop.aspx/TruncateTable",
                        data: JSON.stringify({}),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            toastr.success("Sucessful");
                            if (timer) { clearTimeout(timer); }
                            timer = setTimeout(function () {
                                location.reload();
                            }, 1000);
                        },
                        failure: function (response) {
                            $('.loader').hide();
                            var val = response.d;
                            toastr.warning(val);
                        }
                    });
                }
            }
        }

    </script>


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" runat="Server">
</asp:Content>

