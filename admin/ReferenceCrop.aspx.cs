﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;

public partial class admin_ReferenceCrop : BasePage
{
    public admin_ReferenceCrop()
    {
        Table_Name = "Ref_Crop";
    }

    [WebMethod]
    public static string GetTableInfo()
    {
        Count_Query = @"DECLARE @Count int;
                            Select @Count = Count(*)  From Ref_Crop Where Status = 1";
        return Get_Table_Info();
    }

    [WebMethod]
    public static string GetRegionList()
    {
        string sql_qry = "select Region_Id, Region_Name from ref_region where [status] = 1 order by region_id";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return dt.DataTableToJsonstring();
    }

    [WebMethod]
    public static string BindReferenceCrop()
    {
        string sql_qry = @"SELECT [Crop_ID]
                          ,[Crop_Code]
                          ,[Crop_Name]
                          ,[Crop_Type]
                          ,[Crop_Price]
                          ,[Region_ID]
                          ,[Sort_Order]
                          ,[Status]
                      FROM[dbo].[Ref_Crop] Where [Status] = 1 order by sort_order";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return JsonConvert.SerializeObject(dt);
    }

    [WebMethod, ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static void SaveReferenceCrop(dynamic lst)
    {
        foreach (var item in lst)
        {
            string qry = "";
            int val = Convert.ToInt32(item["Actionstatus"]);
            if (val == 1)
            {
                qry += "INSERT INTO Ref_Crop " +
                        "(" +
                            "[Crop_Code]," +
                            "[Crop_Name]," +
                            "[Crop_Type]," +
                            "[Status] ," +
                            "[Crop_Price]," +
                            "[Region_ID]," +
                            "[Sort_Order]" +
                        ") " + "" +
                    " VALUES" +
                        "(" +
                            "'" + ManageQuotes(item["Crop_Code"]) + "','"
                            + ManageQuotes(item["Crop_Name"]) + "','"
                            + ManageQuotes(item["Crop_Type"]) + "','"
                            + ManageQuotes(item["Status"]) + "','"
                            + item["Crop_Price"] + "','"
                            + item["Region_ID"] + "','"
                            + ManageQuotes(item["Sort_Order"])
                        + "')";

                qry += Environment.NewLine;
            }
            else if (val == 2)
            {
                qry += "UPDATE Ref_Crop SET " +
                            "[Crop_Code] = '" + ManageQuotes(item["Crop_Code"]) + "' ," +
                            "[Crop_Name] = '" + ManageQuotes(item["Crop_Name"]) + "' ," +
                            "[Crop_Type] = '" + ManageQuotes(item["Crop_Type"]) + "'," +
                            "[Status] = '" + ManageQuotes(item["Status"]) + "' ," +
                            "[Crop_Price] = '" + item["Crop_Price"] + "' ," +
                            "[Region_ID] = '" + item["Region_ID"] + "' ," +
                            "[Sort_Order] = '" + ManageQuotes(item["Sort_Order"]) +
                    "' where Crop_ID = " + item["Crop_ID"] + "  ";

                qry += Environment.NewLine;
            }
            else if (val == 3)
            {
                qry = "DELETE FROM Ref_Crop  where Crop_ID = " + item["Crop_ID"] + "  ";

                qry += Environment.NewLine;
            }

            if(!string.IsNullOrEmpty(qry))
            {
                gen_db_utils.gp_sql_execute(qry, dbKey);
                Update_Table_Audit();
            }            
        }
    }

    [WebMethod]
    public static void TruncateTable()
    {
        string qry = "TRUNCATE TABLE Ref_Crop";
        gen_db_utils.gp_sql_execute(qry, dbKey);
        Update_Table_Audit();
    }

    [WebMethod]
    public static void UploadExcel(dynamic list)
    {
        string qry = string.Empty;

        foreach (var item in list)
        {
            var status = Get_Value(item, "Status");

            if (string.IsNullOrEmpty(status))
            {
                var Crop_Code = TrimSQL(Get_Value(item, "Crop Code"));
                string Crop_Type = TrimSQL(Get_Value(item, "Crop Type"));
                if(!string.IsNullOrEmpty(Crop_Type))
                {
                    Crop_Type = Crop_Type.Replace('_', '-');
                }

                dynamic Crop_Price = Get_Value(item, "Crop Price");
                if(string.IsNullOrEmpty(Crop_Price))
                {
                    Crop_Price = Get_Value(item, " Crop Price ");
                }

                try
                {
                    Crop_Price = Convert.ToDouble(Crop_Price.Trim());
                }
                catch
                {
                    Crop_Price = 0;
                }

                var Sort_Order = Get_Value(item, "Sort Order");
                var Status = Get_Value(item, "Status");

                qry += @"IF NOT EXISTS
                        (
                            SELECT 1 FROM [Ref_Crop] 
                            Where Crop_Code = '" + Crop_Code + @"'
                            And Crop_Type = '" + Crop_Type + @"'
                        )
                         BEGIN
                            INSERT INTO [dbo].[Ref_Crop]
                                   ([Crop_Code]
                                   ,[Crop_Type]
                                   ,[Crop_Price]
                                   ,[Region_ID]
                                   ,[Sort_Order]
                                   ,[Status])
                             VALUES
                               (
                                    '" + Crop_Code + @"'" +
                                   ",'" + Crop_Type + @"'" +
                                   ",'" + Crop_Price + @"'" +
                                   ",0" +
                                   ",'" + Sort_Order + @"'" +
                                   ",'" + Status + @"'
                                )
                         END";
                qry += Environment.NewLine;
            }
        }

        if (!string.IsNullOrEmpty(qry))
        {
            gen_db_utils.gp_sql_execute(qry, dbKey);
            Update_Table_Audit();
        }
    }

    public static string ManageQuotes(string value)
    {
        if (!string.IsNullOrEmpty(value) && value.Contains("'"))
        {
            value = value.Replace("'", "''");
        }
        return value;
    }
}