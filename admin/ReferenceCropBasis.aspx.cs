﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Services;

public partial class admin_ReferenceCropBasis : BasePage
{
    public admin_ReferenceCropBasis()
    {
        Table_Name = "Ref_Crop_Basis";
    }

    [WebMethod]
    public static string GetTableInfo(string Office_Id, string Crop_Code)
    {
        try
        {
            string sql_query = "DECLARE @Count int; " +
                               "Select @Count = Count(*) FROM [dbo].[Ref_Crop_Basis] Where [Status] = 1 and crop_code != 'DEFAULT'";

            if (!string.IsNullOrEmpty(Office_Id))
            {
                sql_query += " And Z_Office_ID = '" + Office_Id + "'";

                if (!string.IsNullOrEmpty(Crop_Code))
                {
                    sql_query += " And Crop_Code = '" + Crop_Code + "'";
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(Crop_Code))
                {
                    sql_query += " And Crop_Code = '" + Crop_Code + "'";
                }
            }

            Count_Query = sql_query;
            return Get_Table_Info();
        }
        catch
        {
            return JsonConvert.SerializeObject(new List<object> { new object() });
        }
    }

    [WebMethod]
    public static Dictionary<string, string> GetOfficeList()
    {
        string sql_qry = "select CAST(Office_Id as varchar) as Office_Id, Office_Name from Ref_Office";

        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return dt.AsEnumerable().ToDictionary(a => a.Field<string>("Office_Id"), a => a.Field<string>("Office_Name"));
    }

    [WebMethod]
    public static Dictionary<string, string> GetCropList()
    {
        string sql_qry = "Select Distinct RB.Crop_Code, VC.Crop_Name from Ref_Crop_Basis RB JOIN V_Crop_Price_Details VC on RB.Crop_Code = VC.Crop_Code";

        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return dt.AsEnumerable().ToDictionary(a => a.Field<string>("Crop_Code"), a => a.Field<string>("Crop_Name"));
    }

    public static string GetRegionList()
    {
        string sql_qry = "select Region_Id, Region_Name from ref_region where [status] = 1 order by region_id";

        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return dt.DataTableToJsonstring();
    }

    [WebMethod]
    public static string BindInitialData()
    {
        string sql_qry = "select Office_ID, Office_Name, Region_ID from ref_office where [status] = 1 order by region_id";

        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);

        var data = new
        {
            officeListDropdown = dt.DataTableToJsonstring(),
            officeList = JsonConvert.SerializeObject(dt),
            regionList = GetRegionList()
        };

        return JsonConvert.SerializeObject(data);
    }

    [WebMethod]
    public static string BindReferenceCropBasis(string Office_Id, string Crop_Code)
    {
        string sql_qry = "SELECT * FROM [dbo].[Ref_Crop_Basis] Where [Status] = 1 and crop_code != 'DEFAULT'";

        if(!string.IsNullOrEmpty(Office_Id))
        {
            sql_qry += " And Z_Office_ID = '" + Office_Id + "'";

            if(!string.IsNullOrEmpty(Crop_Code))
            {
                sql_qry += " And Crop_Code = '" + Crop_Code + "'";
            }
        }
        else
        {
            if (!string.IsNullOrEmpty(Crop_Code))
            {
                sql_qry += " And Crop_Code = '" + Crop_Code + "'";
            }
        }

        sql_qry += " order by crop_code, z_office_id, z_region_id";

        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static void SaveReferenceCropBasis(List<dynamic> lst)
    {
        try
        {
            if (lst != null && lst.Count > 0)
            {
                string query = string.Empty;

                foreach (var item in lst)
                {
                    int Actionstatus = Convert.ToInt32(item["Actionstatus"]);

                    switch (Actionstatus)
                    {
                        case 1:
                            query += @"INSERT INTO [dbo].[Ref_Crop_Basis]
                                   (
                                        [Crop_Code]
                                       ,[Crop_Name]
                                       ,[Crop_Type_Code]
                                       ,[Basis_Adj]
                                       ,[Z_Office_ID]
                                       ,[Z_Region_ID]
                                       ,[Status]
                                    )
                             VALUES
                                   (
                                        '" + ManageQuotes(item["Crop_Code"]) + "'" +
                                       ",'" + ManageQuotes(item["Crop_Name"]) + "'" +
                                       ",'" + ManageQuotes(item["Crop_Type_Code"]) + "'" +
                                       ",'" + item["Basis_Adj"] + "'" +
                                       ",'" + item["Z_Office_ID"] + "'" +
                                       ",'" + item["Z_Region_ID"] + "'" +
                                       ",'" + item["Status"] + "'" +
                                    ")";

                            query += Environment.NewLine;
                            break;

                        case 2:
                            query += @"UPDATE [dbo].[Ref_Crop_Basis]
                                SET [Crop_Code] = '" + ManageQuotes(item["Crop_Code"]) + @"'" +
                                  ",[Crop_Name] = '" + ManageQuotes(item["Crop_Name"]) + @"'" +
                                  ",[Crop_Type_Code] = '" + ManageQuotes(item["Crop_Type_Code"]) + @"'" +
                                  ",[Basis_Adj] = '" + item["Basis_Adj"] + @"'" +
                                  ",[Z_Office_ID] = '" + item["Z_Office_ID"] + @"'" +
                                  ",[Z_Region_ID] = '" + item["Z_Region_ID"] + @"'" +
                                  ",[Status] = '" + item["Status"] + @"'" +
                                " WHERE Ref_Basis_ID = '" + item["Ref_Basis_ID"] + "'";

                            query += Environment.NewLine;
                            break;

                        case 3:
                            query += "DELETE FROM Ref_Crop_Basis where Ref_Basis_ID = '" + item["Ref_Basis_ID"] + "'";
                            query += Environment.NewLine;
                            break;
                    }
                }

                gen_db_utils.gp_sql_execute(query, dbKey);
            }

            Update_Table_Audit();
        }
        catch (Exception ex)
        {
        }
    }

    [WebMethod]
    public static void TruncateTable()
    {
        string qry = "TRUNCATE TABLE Ref_Crop_Basis";
        gen_db_utils.gp_sql_execute(qry, dbKey);
        Update_Table_Audit();
    }

    [WebMethod]
    public static void UploadExcel(dynamic list)
    {
        string qry = string.Empty;

        foreach (var item in list)
        {
            var status = Get_Value(item, "Status");
            if (!string.IsNullOrEmpty(status))
            {
                var Crop_Code = TrimSQL(Get_Value(item, "Crop Code"));
                string Crop_Type = TrimSQL(Get_Value(item, "Crop Type"));
                if (!string.IsNullOrEmpty(Crop_Type))
                {
                    Crop_Type = Crop_Type.Replace('_', '-');
                }

                var Basis_Adj = Get_Value(item, "Basis Adj");
                var Z_Office_ID = Get_Value(item, "Office ID");
                var Z_Region_ID = Get_Value(item, "Region ID");
                var Status = Get_Value(item, "Status");

                qry += @"IF NOT EXISTS
                        (
                            SELECT 1 FROM [Ref_Crop_Basis] 
                            Where Crop_Code = '" + Crop_Code + @"'
                            And Crop_Type_Code = '" + Crop_Type + @"'
                            And Z_Office_ID = '" + Z_Office_ID + @"'
                            And Z_Region_ID = '" + Z_Region_ID + @"'
                        )
                         BEGIN
                            INSERT INTO [dbo].[Ref_Crop_Basis]
                                   ([Crop_Code]
                                   ,[Crop_Type_Code]
                                   ,[Basis_Adj]
                                   ,[Z_Office_ID]
                                   ,[Z_Region_ID]
                                   ,[Status])
                             VALUES
                               (
                                    '" + Crop_Code + @"'" +
                                   ",'" + Crop_Type + @"'" +
                                   ",'" + Basis_Adj + @"'" +
                                   ",'" + Z_Office_ID + @"'" +
                                   ",'" + Z_Office_ID + @"'" +
                                   ",'" + Status + @"'
                                )
                         END";
                qry += Environment.NewLine;
            }
        }

        if (!string.IsNullOrEmpty(qry))
        {
            gen_db_utils.gp_sql_execute(qry, dbKey);
            Update_Table_Audit();
        }
    }    

    public static string ManageQuotes(string value)
    {
        if (!string.IsNullOrEmpty(value) && value.Contains("'"))
        {
            value = value.Replace("'", "''");
        }
        return value;
    }
}