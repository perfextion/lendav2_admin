﻿<%@ Page Title="" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="ReferenceDiscounts.aspx.cs" Inherits="admin_ReferenceDiscounts" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="http://www.guriddo.net/demo/js/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" />

    <script type="text/ecmascript" src="http://www.guriddo.net/demo/js/trirand/i18n/grid.locale-en.js"></script>
    <script type="text/ecmascript" src="http://www.guriddo.net/demo/js/trirand/src/jquery.jqGrid.js"></script>
    <link rel="stylesheet" type="text/css" href="http://www.guriddo.net/demo/css/trirand/ui.jqgrid-bootstrap.css" />
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

    <style>
        .ui-jqgrid .ui-jqgrid-htable th div {
            height: auto;
            overflow: hidden;
            padding-right: 4px;
            padding-top: 2px;
            position: relative;
            vertical-align: text-top;
            white-space: normal !important;
        }
    </style>
    <div>
        <div style="width: 1500px">
            <div style="width: 56%; margin-left: 2.6%; float: left;">
                <div style="margin-top: 1%;">
                    <div id="btnAddRow" class="btn btn-info" style="width: 70px;">
                        add
                    </div>
                    <div id="btnSave" class="btn btn-success" style="width: 70px;">Save</div>
                </div>
            </div>
            <div style="width: 40%; float: left;">
                <div class="form-inline" style="margin-top: 3%; margin-bottom: 2%;">
                    <label for="lbldiskgroupcode">Group Code :</label>
                    <select id="ddldiskgroupcode" class="form-control" style="width: 200px">
                        <option value="" selected="selected">Select</option>
                    </select>
                    <label for="txtSearch">Search :</label>
                    <input type="text" class="form-control" name="search" id="txtSearch" style="width: 200px" />
                </div>
            </div>
        </div>
        <div class="loader"></div>
        <div class="row">
            <div style="margin-top: 4%; margin-left: 50px">
                <table id="jqGrid"></table>
                <div id="jqGridPager"></div>
            </div>
        </div>
    </div>

    <%--<div class="col-md-12">
        <div class="panel-body row-padding">
            <div class="row">
                <div class="col-md-7" style="padding-left: 1.3%;">
                    <div id="btnAddRow" class="btn btn-info" style="width: 60px;">
                        add
                    </div>
                    <div id="btnSave" class="btn btn-success" style="width: 60px;">Save</div>
                </div>
                <div class="col-md-5" style="margin-left: -7%;">
                    <div class="form-inline">
                        <div class="form-group">
                            <label for="lbldiskgroupcode">Group Code :</label>
                            <select id="ddldiskgroupcode" class="form-control" style="width: 200px">
                                <option value="" selected="selected">Select</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="txtSearch">Search :</label>
                            <input type="text" class="form-control" name="search" id="txtSearch" style="width: 200px" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>--%>
    <%--<div class="loader"></div>
    <div class="row">
        <div style="margin-left: 50px">
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>--%>
    <br />
    <div class="col-md-4" style="padding-left: 51px;">
        <div id="btnAddRowbtm" class="btn btn-info" style="width: 60px;">
            add
        </div>
        <div id="btnSavebtm" class="btn btn-success" style="width: 60px;">Save</div>
    </div>
    <asp:HiddenField ID="DiscGroupCode" runat="server" />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <script type="text/javascript">  
        var DeleteRows = [];
        var timer;
        var lastSelection;
        var ddlval;
        $.urlParam = function (name) {
            var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
            if (results == null) {
                return null;
            }
            return decodeURI(results[1]) || 0;
        }
        $(function () {
            var obj = {
                Init: function () {
                    //ddlval = $.urlParam('ddlval');
                    obj.getdiscgroupcode();
                    if ($('#ContentPlaceHolder1_DiscGroupCode').val() != '') {
                        $('#ddldiskgroupcode').val($('#ContentPlaceHolder1_DiscGroupCode').val());
                        var DiscGroupCode = $('#ContentPlaceHolder1_DiscGroupCode').val();
                    }
                    obj.bindgrid();
                    $("#btnAddRow").click(function () {
                        obj.add();
                    });
                    $("#btnSave").click(function () {
                        obj.save();
                    });
                    $("#btnAddRowbtm").click(function () {
                        obj.add();
                    });
                    $("#btnSavebtm").click(function () {
                        obj.save();
                    });
                    $("#txtSearch").on("keyup", function () {
                        var self = this;
                        obj.search(self);
                    });
                    $("#ddldiskgroupcode").on("change", function () {
                        //obj.filedSearch();
                        //obj.Searchdiscgroupcode();
                        obj.bindgrid();
                    });
                    //if (ddlval || 0 > 0 && ddlval > 0) {
                    //    $('#ddldiskgroupcode').val(ddlval);
                    //    $('#ddldiskgroupcode').val(ddlval).trigger('change');
                    //}
                },
                bindgrid: function () {
                    var discgroupcode = $("#ddldiskgroupcode :selected").text();
                    $.ajax({
                        type: "POST",
                        url: "ReferenceDiscounts.aspx/BindReferenceDiscountsDetails",
                        async: false,
                        data: JSON.stringify({ DiscGroupCode: discgroupcode }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var resData = JSON.parse(data.d);
                            $('.loader').show();
                            for (var i = 0; i < resData.length; i++) {
                                resData[i].rowid = i + 1;
                            }
                            obj.loadgrid(resData);
                            $('.loader').hide();
                        }
                    });
                },
                loadgrid: function (data) {
                    $.jgrid.gridUnload("#jqGrid");
                    $("#jqGrid").jqGrid({
                        datatype: "local",
                        styleUI: 'Bootstrap',
                        data: data,
                        colModel: [
                            { label: 'rowid', name: 'rowid', width: 75, editable: false, key: true, hidden: true },
                            { label: 'Discount Id', name: 'Discount_Id', align: 'center', width: 75, editable: false },
                            { label: 'Discount Key', name: 'Discount_Key', align: 'center', width: 100, editable: true },
                            {
                                label: 'Discount Value', name: 'Discount_Value', align: 'center', width: 80, editable: true, editoptions: {
                                    maxlength: 10, dataEvents: [
                                        {
                                            type: 'keypress', fn: function (e) {
                                                var charCode = (e.which) ? e.which : e.keyCode;
                                                if (((charCode != 46 || (charCode == 46 && $(this).val() == '')) ||
                                                    $(this).val().indexOf('.') != -1) && (charCode < 48 || charCode > 57)) {
                                                    return false;
                                                }
                                            }
                                        }
                                    ]
                                }
                            },
                            { label: 'Disc Detail', name: 'Disc_Detail', width: 350, editable: true },
                            { label: 'Disc Group', name: 'Disc_Group', align: 'center', width: 80, editable: true },
                            { label: 'Disc Group Code', name: 'Disc_Group_Code', align: 'center', width: 100, editable: true },
                            { label: 'CreatedOn', name: 'CreatedOn', align: 'center', width: 120, editable: false },
                            { label: 'Actionstatus', name: 'Actionstatus', width: 70, editable: false, hidden: true },
                            { label: '', name: '', width: 45, align: 'center', formatter: obj.deleteLink },
                        ],
                        viewrecords: true,
                        loadonce: true,
                        width: 1400,
                        restoreAfterSelect: false,
                        saveAfterSelect: true,
                        sortable: true,
                        onSelectRow: obj.edit,
                        onPaging: obj.changePage,
                        //height: 350,
                        height: 'auto',
                        rowNum: 100,
                        pager: "#jqGridPager"
                    });
                },
                add: function () {
                    var grid = $("#jqGrid");
                    var rowsperPage = parseInt(grid.getGridParam('rowNum'));
                    var gridlength = grid.jqGrid('getGridParam', 'data').length;
                    var curpage = parseInt($(".ui-pg-input").val());
                    var totPages = Math.ceil(gridlength / rowsperPage);
                    //if (rowsperPage * curpage == gridlength) {
                    if (rowsperPage * totPages == gridlength) {
                        var id = $('.inline-edit-cell').parent().parent().prop('id');
                        grid.jqGrid('saveRow', id);
                        var row = obj.newrow();
                        // var newRowId = grid.jqGrid('getGridParam', 'data').length + 1;
                        var newRowId = row.rowid;
                        grid.jqGrid('addRowData', newRowId, row);
                        grid.trigger('reloadGrid');
                        lastSelection = newRowId;
                        grid.jqGrid('saveRow', lastSelection);
                        grid.jqGrid('restoreRow', lastSelection);
                        $('.glyphicon-step-forward').trigger('click');

                    } else {

                        $('.glyphicon-step-forward').trigger('click');
                        var row = obj.newrow();
                        // var newRowId = grid.jqGrid('getGridParam', 'data').length + 1;
                        var newRowId = row.rowid;
                        grid.jqGrid('addRowData', newRowId, row);
                    }

                    lastSelection = newRowId;
                    grid.jqGrid('saveRow', lastSelection);
                    grid.jqGrid('restoreRow', lastSelection);

                    var eid = $('.inline-edit-cell').parent().parent().prop('id')
                    grid.jqGrid('saveRow', eid);
                    grid.jqGrid('restoreRow', eid);
                    grid.jqGrid('editRow', newRowId);
                },
                edit: function (id) {
                    if (id && id !== lastSelection) {
                        var grid = $("#jqGrid");
                        grid.jqGrid('saveRow', lastSelection);
                        grid.jqGrid('restoreRow', lastSelection);
                        var row = grid.jqGrid('getRowData', lastSelection);
                        if (!jQuery.isEmptyObject(row)) {
                            var dataobj = jQuery("#jqGrid").jqGrid('getGridParam', 'data');
                            index = dataobj.findIndex(x => x.rowid == row.rowid);
                            if (row.rowid > 0 && row.Actionstatus != 1) {
                                row.Actionstatus = 2;
                                grid.jqGrid('getGridParam', 'data')[index] = row;
                            }
                        }
                        grid.jqGrid('editRow', id);
                        lastSelection = id;
                    }
                },
                newrow: function () {
                    var newid = 0;
                    var grid = $("#jqGrid");
                    var length = jQuery("#jqGrid").jqGrid('getGridParam', 'records');
                    var dataobj = grid.jqGrid('getGridParam', 'data');
                    var dataobj = grid.jqGrid('getGridParam', 'data');

                    IDs = dataobj.map(function (e) { return e.rowid });
                    if (IDs.length > 0) {
                        newid = IDs.reduce(function (a, b) { return Math.max(a, b); });
                    }
                    var row = {
                        Actionstatus: 1,
                        rowid: newid + 1,
                        Discount_Id: 0,
                        Discount_Key: "",
                        Discount_Value: "",
                        Disc_Detail: "",
                        Disc_Group: "",
                        Disc_Group_Code: "",
                        CreatedOn: ""
                    };
                    return row;
                },
                save: function () {
                    $('.loader').show();
                    var grid = $("#jqGrid");
                    var allrows = [];
                    var id = $('.inline-edit-cell').parent().parent().prop('id');
                    grid.jqGrid('saveRow', id);
                    var rowData = grid.jqGrid('getRowData', id);
                    if (rowData.rowid > 0) {
                        rowData.Actionstatus = 2;
                        grid.jqGrid('getGridParam', 'data')[id] = rowData;
                    }
                    for (var i = 0; i < DeleteRows.length; i++) {
                        DeleteRows[i].Actionstatus = 3
                        allrows.push(DeleteRows[i]);
                    }
                    var dataobj = grid.jqGrid('getGridParam', 'data');
                    var newRows = $.grep(dataobj, function (e) {
                        if (e != undefined) {
                            return e.Actionstatus == 1 || e.Actionstatus == 2
                        }
                    });
                    for (var i = 0; i < newRows.length; i++) {
                        allrows.push(newRows[i]);
                    }
                    ddlval = $("#ddldiskgroupcode").val();
                    $.ajax({
                        type: "POST",
                        url: "ReferenceDiscounts.aspx/SaveReferenceDiscountsDetails",
                        data: JSON.stringify({ lst: allrows }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            //location.reload();
                            //$('#ddldiskgroupcode').val(ddlval).trigger('change');
                            toastr.success("Saved Sucessful");
                            if (timer) { clearTimeout(timer); }
                            timer = setTimeout(function () {
                                window.location.href = "ReferenceDiscounts.aspx?ddlval=" + ddlval;
                            }, 2000);

                        },
                        failure: function (response) {
                            var val = console.log(response.d);
                            toastr.warning(val);
                        }
                    });
                },
                search: function (self) {

                    if (timer) { clearTimeout(timer); }
                    timer = setTimeout(function () {
                        obj.changePage();
                        $("#jqGrid").jqGrid('filterInput', self.value);
                    }, 0);
                },
                getdiscgroupcode: function () {
                    $.ajax({
                        type: "POST",
                        url: "ReferenceDiscounts.aspx/GetRefDiscCode",
                        async: false,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            $.each(res.d, function (data, value) {
                                $("#ddldiskgroupcode").append($("<option></option>").val(value).html(value));
                            });
                        }
                    });
                },
                Searchdiscgroupcode: function () {
                    var discgroupcode = $("#ddldiskgroupcode :selected").text();
                    $.ajax({
                        type: "POST",
                        url: "ReferenceDiscounts.aspx/Searchdiscgroupcode",
                        data: JSON.stringify({ DiscGroupCode: discgroupcode }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var resData = JSON.parse(data.d);
                            obj.loadgrid(resData);
                        }
                    });
                },
                filedSearch: function () {
                    var postData = $("#jqGrid").jqGrid("getGridParam", "postData"),
                        colModel = $("#jqGrid").jqGrid("getGridParam", "colModel"),
                        rules = [],
                        searchText = $("#ddldiskgroupcode :selected").text();
                    if (searchText != "Select") {
                        l = colModel.length;
                        if (searchText != "") {
                            rules.push({ field: "Disc_Group_Code", op: "eq", data: searchText });
                        }
                        postData.filters = JSON.stringify({
                            groupOp: "OR",
                            rules: rules
                        });

                        $("#jqGrid").jqGrid("setGridParam", { search: true });
                        $("#jqGrid").trigger("reloadGrid", [{ page: 1, current: true }]);
                        // return false;
                    }
                    else {
                        obj.bindgrid();
                    }
                },
                changePage: function () {
                    lastSelection = "";
                    var grid = $("#jqGrid");
                    var id = $('.inline-edit-cell').parent().parent().prop('id');
                    if (id != undefined) {
                        grid.jqGrid('saveRow', id);
                        grid.jqGrid('restoreRow', id);
                        var row = grid.jqGrid('getRowData', id);
                        if (!jQuery.isEmptyObject(row)) {
                            var dataobj = jQuery("#jqGrid").jqGrid('getGridParam', 'data');
                            index = dataobj.findIndex(x => x.rowid == row.rowid);
                            if (row.rowid > 0 && row.Actionstatus != 1) {
                                row.Actionstatus = 2;
                                grid.jqGrid('getGridParam', 'data')[index] = row;
                            }
                        }
                    }
                },
                deleteLink: function (cellValue, options, rowdata, action) {
                    return "<a href='javascript:deleteRecord(" + options.rowId + ")' class='glyphicon glyphicon-trash' style='color:red'></a>";
                }
            }
            obj.Init();
        });
        function deleteRecord(id) {
            var grid = $('#jqGrid');
            var result = confirm("Are you sure you Want to delete?");
            if (result == true) {
                var ediId = $('.inline-edit-cell').parent().parent().prop('id');
                grid.jqGrid('saveRow', ediId);
                var rowData = grid.jqGrid('getRowData', ediId);
                if (rowData.rowid > 0 && rowData.Actionstatus != 1) {
                    rowData.Actionstatus = 2;
                    grid.jqGrid('getGridParam', 'data')[ediId - 1] = rowData;
                }
                var row = grid.jqGrid('getRowData', id);
                grid.jqGrid('delRowData', id);
                if (row.Actionstatus != 1) {
                    DeleteRows.push(row);
                }
                var data = jQuery("#jqGrid").jqGrid('getGridParam', 'data');
                for (var i = 0; i < data.length; i++) {
                    if (data[i].Discount_Id == 0) {
                        data[i].rowid = i + 1;
                        data[i].id = i + 1;
                    } else
                        data[i].rowid = i + 1;
                }
                var curpage = parseInt($(".ui-pg-input").val());
                jQuery('#jqGrid').jqGrid('clearGridData');
                jQuery('#jqGrid').jqGrid('setGridParam', { data: data });
                $("#jqGrid").trigger("reloadGrid", [{ page: curpage }]);
                $("#jqGrid").trigger("reloadGrid", [{ page: curpage }]);
                lastSelection = id;
            }
        }
        function isValidNumber(e) {

            var charCode = (e.which) ? e.which : e.keyCode;
            if (((charCode != 46 || (charCode == 46 && $(this).val() == '')) ||
                $(this).val().indexOf('.') != -1) && (charCode < 48 || charCode > 57)) {
                return false;
            }
        }
        function unique(list) {
            var result = [];
            $.each(list, function (i, e) {
                if ($.inArray(e, result) == -1) result.push(e);
            });
            return result;
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" runat="Server">
</asp:Content>

