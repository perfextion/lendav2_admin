﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_ReferenceDiscounts : System.Web.UI.Page
{
    static string dbKey = "gp_conn";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["DiscGroupCode"] != null)
        {
            DiscGroupCode.Value = Session["DiscGroupCode"].ToString();
        }
    }
    [WebMethod]
    public static string BindReferenceDiscountsDetails(string DiscGroupCode)
    {
        HttpContext.Current.Session["DiscGroupCode"] = DiscGroupCode;
        string sql_qry = "select Discount_Id,Discount_Key,Discount_Value,Disc_Detail,Disc_Group,Disc_Group_Code,CreatedOn from Ref_Discounts ";
        if (!string.IsNullOrEmpty(DiscGroupCode) && DiscGroupCode != "Select")
        {
            sql_qry += "where Disc_Group_Code='" + DiscGroupCode + "'";
        }
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
    }
    [WebMethod]
    public static string Searchdiscgroupcode(string DiscGroupCode)
    {

        string sql_qry = "select Discount_Id,Discount_Key,Discount_Value,Disc_Detail,Disc_Group,Disc_Group_Code,CreatedOn from Ref_Discounts where Disc_Group_Code='" + DiscGroupCode + "'";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
    }
    [WebMethod]
    public static void SaveReferenceDiscountsDetails(dynamic lst)
    {
        foreach (var item in lst)
        {
            string qry = "";
            int val = Convert.ToInt32(item["Actionstatus"]);
            if (val == 1)
            {
                qry = "INSERT INTO Ref_Discounts (Discount_Key,Discount_Value,Disc_Detail,Disc_Group,Disc_Group_Code,CreatedOn) " +
               " VALUES('" + ManageQuotes(item["Discount_Key"]) + "','" + ManageQuotes(item["Discount_Value"]) + "','" + ManageQuotes(item["Disc_Detail"]) + "'," +
               "'" + ManageQuotes(item["Disc_Group"]) + "','" + ManageQuotes(item["Disc_Group_Code"]) + "','" + DateTime.Now + "')";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 2)
            {   //  Discount_Id,Discount_Key,Discount_Value,CreatedOn
                qry = "UPDATE Ref_Discounts SET Discount_Key = '" + ManageQuotes(item["Discount_Key"]) + "',Discount_Value = '" + ManageQuotes(item["Discount_Value"]) + "'," +
                    "Disc_Detail = '" + ManageQuotes(item["Disc_Detail"]) + "',Disc_Group = '" + ManageQuotes(item["Disc_Group"]) + "'," +
                    "Disc_Group_Code = '" + ManageQuotes(item["Disc_Group_Code"]) + "',CreatedOn = '" + ManageQuotes(item["CreatedOn"]) + "' where Discount_Id='" + item["Discount_Id"] + "'";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 3)
            {
                qry = "DELETE FROM Ref_Discounts where Discount_Id=" + item["Discount_Id"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
        }
    }
    [WebMethod]
    public static List<string> GetRefDiscCode()
    {
        string sql_qry = "select distinct Disc_Group_Code from Ref_Discounts";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);

        List<string> Disccode = new List<string>();
        Disccode = (from DataRow row in dt.Rows
                    select row["Disc_Group_Code"].ToString()
               ).ToList();
        return Disccode;
    }
    public static string ManageQuotes(string value)
    {
        if (!String.IsNullOrEmpty(value) && value.Contains("'"))
        {
            value = value.Replace("'", "''");
        }
        return value;
    }
}