﻿using System;
using System.Data;
using System.Web.Script.Services;
using System.Web.Services;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_ReferenceExceptionLevel : System.Web.UI.Page
{
    static string dbKey = "gp_conn";

    protected void Page_Load(object sender, EventArgs e)
    {

    }
    [WebMethod]
    public static string BindReferenceExceptionLevel()
    {
        string sql_qry = "select [Exception_Level_ID],[Exception_ID],[Exception_ID_Level],[Threshold_Ind],[Threshold_Operative],[Threshold_List],[Threshold_Min],[Threshold_Max],[Threshold_Value],[Threshold_Hyper_Code],[Mitigation_Text_Required_Ind],[Condition_ID],[Status] from Ref_Exception_Level";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
    }

    [WebMethod, ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static void SaveReferenceExceptionLevel(dynamic lst)
    {
        //Dictionary<string, string> dic = lst;

        foreach (var item in lst)
        {
            string qry = "";
            int val = Convert.ToInt32(item["Actionstatus"]);
            if (val == 1)
            {

                qry = "INSERT INTO Ref_Exception_Level ([Exception_ID],[Exception_ID_Level],[Threshold_Ind] ,[Threshold_Operative],[Threshold_List],[Threshold_Min],[Threshold_Max],[Threshold_Value],[Threshold_Hyper_Code],[Mitigation_Text_Required_Ind],[Condition_ID],[Status]) " +
                    " VALUES('" + ManageQuotes(item["Exception_ID"]) + "','" + ManageQuotes(item["Exception_ID_Level"]) + "','" + ManageQuotes(item["Threshold_Ind"]) + "','" + ManageQuotes(item["Threshold_Operative"]) + "','" + ManageQuotes(item["Threshold_List"]) + "','" + ManageQuotes(item["Threshold_Min"]) + "','" + ManageQuotes(item["Threshold_Max"]) + "' ,'" + ManageQuotes(item["Threshold_Value"]) + "','" + ManageQuotes(item["Threshold_Hyper_Code"]) + "','" + ManageQuotes(item["Mitigation_Text_Required_Ind"]) + "','" + ManageQuotes(item["Condition_ID"]) + "','" + ManageQuotes(item["Status"]) + "')";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 2)
            {
                qry = "UPDATE Ref_Exception_Level SET [Exception_ID] = '" + ManageQuotes(item["Exception_ID"]) + "' ,[Exception_ID_Level] = '" + ManageQuotes(item["Exception_ID_Level"]) + "'," +
                    "[Threshold_Ind] = '" + ManageQuotes(item["Threshold_Ind"]) + "' ,[Threshold_Operative] = '" + ManageQuotes(item["Threshold_Operative"]) + "' ,[Threshold_List] = '" + ManageQuotes(item["Threshold_List"]) + "' ,[Threshold_Min] = '" +
                    ManageQuotes(item["Threshold_Min"]) + "' ,Threshold_Max = '" + (item["Threshold_Max"]) + "', [Threshold_Value] = '" + ManageQuotes(item["Threshold_Value"]) + "' ,[Threshold_Hyper_Code] = '" + (item["Threshold_Hyper_Code"]) + "'," +
                    "[Mitigation_Text_Required_Ind] = '" + (item["Mitigation_Text_Required_Ind"]) + "' ,[Condition_ID] = '" + (item["Condition_ID"]) + "' ,[Status] = '" + ManageQuotes(item["Status"]) + "' where Exception_Level_ID=" + item["Exception_Level_ID"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 3)
            {
                qry = "DELETE FROM Ref_Exception_Level  where Exception_Level_ID=" + item["Exception_Level_ID"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }

        }
    }

    public static string ManageQuotes(string value)
    {
        if (!String.IsNullOrEmpty(value) && value.Contains("'"))
        {
            value = value.Replace("'", "''");
        }
        return value;
    }
}