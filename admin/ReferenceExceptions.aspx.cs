﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_ReferenceExceptions : System.Web.UI.Page
{
    static string dbKey = "gp_conn";
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [WebMethod]
    public static string BindExceptionsDetails()
    {
        string sql_qry = "SELECT Exception_ID,Exception_ID_Text,Sort_Order,Status FROM Ref_Exception";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static void SaveExceptionsDetails(dynamic lst)
    {
        foreach (var item in lst)
        {
            string qry = "";
            int val = Convert.ToInt32(item["Actionstatus"]);
            if (val == 1)
            {
                qry = "INSERT INTO Ref_Exception (Exception_ID_Text,Sort_Order,Status ) " +
                    " VALUES('" + ManageQuotes(item["Exception_ID_Text"]) + "','" + ManageQuotes(item["Sort_Order"]) + "','" + ManageQuotes(item["Status"]) + "')";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 2)
            {
                qry = "UPDATE Ref_Exception SET Exception_ID_Text = '" + ManageQuotes(item["Exception_ID_Text"]) + "',[Sort_Order] = '" + ManageQuotes(item["Sort_Order"]) + "',[Status] = '" + ManageQuotes(item["Status"]) + "' where Exception_ID=" + item["Exception_ID"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 3)
            {
                qry = "DELETE FROM Ref_Exception where Exception_ID=" + item["Exception_ID"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
        }
    }

    public static string ManageQuotes(string value)
    {
        if (!String.IsNullOrEmpty(value) && value.Contains("'"))
        {
            value = value.Replace("'", "''");
        }
        return value;
    }
}