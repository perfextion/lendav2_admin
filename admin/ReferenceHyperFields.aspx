﻿<%@ Page Title="" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="ReferenceHyperFields.aspx.cs" Inherits="admin_ReferenceHyperFields" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="http://www.guriddo.net/demo/js/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" />

    <script type="text/ecmascript" src="http://www.guriddo.net/demo/js/trirand/i18n/grid.locale-en.js"></script>
    <script type="text/ecmascript" src="http://www.guriddo.net/demo/js/trirand/src/jquery.jqGrid.js"></script>
    <link rel="stylesheet" type="text/css" href="http://www.guriddo.net/demo/css/trirand/ui.jqgrid-bootstrap.css" />
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <style>
        .ui-jqgrid .ui-jqgrid-htable th div {
            height: auto;
            overflow: hidden;
            padding-right: 4px;
            padding-top: 2px;
            position: relative;
            vertical-align: text-top;
            white-space: normal !important;
        }

        .fc-head-container thead tr, .table thead tr {
            height: 50px;
        }
    </style>
    <div class="">
        <div style="width: 1500px">
            <div style="width: 64.5%; float: left; margin-left: 2.2%!important; margin: 1%;">
                <div id="btnAddRow" class="btn btn-info" style="width: 60px;">
                    add
                </div>
                <div id="btnSave" class="btn btn-success" style="width: 60px;">Save</div>
            </div>
            <div style="width: 28%; float: left; margin-top: 10px; margin-right: 22px;">
                <div class="form-inline" style="margin-left: 100px;">
                    <a href="HyperFieldsview.aspx" class="btn btn-primary">View</a>
                    <label for="txtSearch">Search :</label>
                    <input type="text" class="form-control" name="search" id="txtSearch" style="width: 200px" />
                </div>
            </div>
        </div>
    </div>
    <div class="loader"></div>
    <div class="row">
        <div style="margin-left: 50px">
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
    <br />
    <br />
    <br />

    <script type="text/javascript">  
        var DeleteRows = [];
        var timer;
        var lastSelection;

        $(function () {

            var obj = {
                Init: function () {
                    obj.bindgrid();
                    $("#btnAddRow").click(function () {
                        obj.add();
                    });
                    $("#btnSave").click(function () {
                        obj.save();
                    });
                    $("#txtSearch").on("keyup", function () {
                        var self = this;
                        obj.search(self);
                    });
                },
                bindgrid: function () {
                    $.ajax({
                        type: "POST",
                        url: "ReferenceHyperFields.aspx/BindReferenceHyperFields",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var resData = JSON.parse(data.d);
                            $('.loader').show();
                            for (var i = 0; i < resData.length; i++) {
                                resData[i].rowid = i + 1;
                            }
                            obj.loadgrid(resData);
                            $('.loader').hide();
                        }
                    });
                },
                loadgrid: function (data) {
                    $("#jqGrid").jqGrid({
                        datatype: "local",
                        styleUI: 'Bootstrap',
                        data: data,
                        colModel: [
                            { label: 'rowid', name: 'rowid', width: 75, editable: false, key: true, hidden: true },
                            { label: 'HyperField ID', name: 'HyperField_ID', align: 'center', width: 75, editable: false },
                            { label: 'HyperField TableName', name: 'HyperField_TableName', width: 130, editable: true },
                            { label: 'HyperField Name', name: 'HyperField_Name', width: 200, editable: true },
                            { label: 'Array Item ID Exp', name: 'Array_Item_ID_Exp', width: 150, editable: true },
                            { label: 'HyperField Expression', name: 'HyperField_Expression', width: 300, editable: true },
                            { label: 'Chevron ID', name: 'Chevron_ID', align: 'center', width: 50, editable: true },
                            { label: 'Exception ID', name: 'Exception_ID', align: 'center', width: 70, editable: true },
                            { label: 'Status', name: 'Status', width: 70, align: 'center', editable: true },

                            { label: 'Actionstatus', name: 'Actionstatus', width: 70, editable: false, hidden: true },
                            { label: '', name: '', width: 45, align: 'center', formatter: obj.deleteLink },
                        ],
                        viewrecords: true,
                        loadonce: true,
                        width: 1400,
                        restoreAfterSelect: false,
                        saveAfterSelect: true,
                        sortable: true,
                        onSelectRow: obj.edit,
                        onPaging: obj.changePage,
                        height: 'auto',
                        rowNum: 100,
                        pager: "#jqGridPager"
                    });
                },
                add: function () {

                    var grid = $("#jqGrid");

                    var rowsperPage = parseInt(grid.getGridParam('rowNum'));
                    var gridlength = grid.jqGrid('getGridParam', 'data').length;
                    var curpage = parseInt($(".ui-pg-input").val());
                    var totPages = Math.ceil(gridlength / rowsperPage);

                    //if (rowsperPage * curpage == gridlength) {
                    if (rowsperPage * totPages == gridlength) {
                        var id = $('.inline-edit-cell').parent().parent().prop('id');
                        grid.jqGrid('saveRow', id);
                        var row = obj.newrow();
                        // var newRowId = grid.jqGrid('getGridParam', 'data').length + 1;
                        var newRowId = row.rowid;
                        grid.jqGrid('addRowData', newRowId, row);
                        grid.trigger('reloadGrid');
                        lastSelection = newRowId;
                        grid.jqGrid('saveRow', lastSelection);
                        grid.jqGrid('restoreRow', lastSelection);
                        $('.glyphicon-step-forward').trigger('click');

                    } else {

                        $('.glyphicon-step-forward').trigger('click');
                        var row = obj.newrow();
                        // var newRowId = grid.jqGrid('getGridParam', 'data').length + 1;
                        var newRowId = row.rowid;
                        grid.jqGrid('addRowData', newRowId, row);
                    }


                    lastSelection = newRowId;
                    grid.jqGrid('saveRow', lastSelection);
                    grid.jqGrid('restoreRow', lastSelection);

                    var eid = $('.inline-edit-cell').parent().parent().prop('id')
                    grid.jqGrid('saveRow', eid);
                    grid.jqGrid('restoreRow', eid);
                    grid.jqGrid('editRow', newRowId);
                },
                edit: function (id) { 

                    if (id && id !== lastSelection) {
                        var grid = $("#jqGrid");

                        grid.jqGrid('saveRow', lastSelection);
                        grid.jqGrid('restoreRow', lastSelection);
                        var row = grid.jqGrid('getRowData', lastSelection);

                        if (!jQuery.isEmptyObject(row)) {
                            var dataobj = jQuery("#jqGrid").jqGrid('getGridParam', 'data');
                            index = dataobj.findIndex(x => x.rowid == row.rowid);
                            if (row.rowid > 0 && row.Actionstatus != 1) {
                                row.Actionstatus = 2;
                                grid.jqGrid('getGridParam', 'data')[index] = row;
                            }
                        }

                        grid.jqGrid('editRow', id);
                        lastSelection = id;
                    }
                },
                newrow: function () {
                    var newid = 0;

                    var grid = $("#jqGrid");
                    var length = jQuery("#jqGrid").jqGrid('getGridParam', 'records');
                    var dataobj = grid.jqGrid('getGridParam', 'data');
                    var dataobj = grid.jqGrid('getGridParam', 'data');
                    var HyperField_ID = [];
                    HyperField_IDs = dataobj.map(function (e) { return e.rowid });
                    if (HyperField_IDs.length > 0) {
                        newid = HyperField_IDs.reduce(function (a, b) { return Math.max(a, b); });
                    }
                    var row = {
                        Actionstatus: 1,
                        rowid: newid + 1,
                        HyperField_ID: 0,

                        HyperField_TableName: "",
                        HyperField_Name: "",
                        Array_Item_ID_Exp: "",
                        HyperField_Expression: "",
                        Chevron_ID: "",
                        Exception_ID: "",
                        Status: ""

                    };
                    return row;
                },
                save: function () {

                    $('.loader').show();
                    var grid = $("#jqGrid");
                    var allrows = [];


                    var id = $('.inline-edit-cell').parent().parent().prop('id');
                    grid.jqGrid('saveRow', id);

                    var rowData = grid.jqGrid('getRowData', id);
                    if (rowData.rowid > 0) {
                        rowData.Actionstatus = 2;
                        grid.jqGrid('getGridParam', 'data')[id] = rowData;
                    }

                    for (var i = 0; i < DeleteRows.length; i++) {
                        DeleteRows[i].Actionstatus = 3
                        allrows.push(DeleteRows[i]);
                    }
                    var dataobj = grid.jqGrid('getGridParam', 'data');
                    var newRows = $.grep(dataobj, function (e) {
                        if (e != undefined) {
                            return e.Actionstatus == 1 || e.Actionstatus == 2
                        }
                    });
                    for (var i = 0; i < newRows.length; i++) {
                        allrows.push(newRows[i]);
                    }

                    $.ajax({
                        type: "POST",
                        url: "ReferenceHyperFields.aspx/SaveReferenceHyperFields",
                        data: JSON.stringify({ lst: allrows }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            location.reload();
                        },
                        failure: function (response) {
                            console.log(response.d);
                        }

                    });
                },
                search: function (self) {
                    if (timer) { clearTimeout(timer); }
                    timer = setTimeout(function () {
                        obj.changePage();
                        $("#jqGrid").jqGrid('filterInput', self.value);
                    }, 0);
                },
                changePage: function () {

                    lastSelection = "";
                    var grid = $("#jqGrid");

                    var id = $('.inline-edit-cell').parent().parent().prop('id');
                    if (id != undefined) {


                        grid.jqGrid('saveRow', id);
                        grid.jqGrid('restoreRow', id);
                        var row = grid.jqGrid('getRowData', id);
                        if (!jQuery.isEmptyObject(row)) {
                            var dataobj = jQuery("#jqGrid").jqGrid('getGridParam', 'data');
                            index = dataobj.findIndex(x => x.rowid == row.rowid);
                            if (row.rowid > 0 && row.Actionstatus != 1) {
                                row.Actionstatus = 2;
                                grid.jqGrid('getGridParam', 'data')[index] = row;
                            }
                        }
                    }
                },

                deleteLink: function (cellValue, options, rowdata, action) {
                    return "<a href='javascript:deleteRecord(" + options.rowId + ")' class='glyphicon glyphicon-trash' style='color:red'></a>";
                }
            }
            obj.Init();
        });
        function deleteRecord(id) {

            var grid = $('#jqGrid');
            var result = confirm("Are you sure you Want to delete?");

            if (result == true) {
                var ediId = $('.inline-edit-cell').parent().parent().prop('id');
                grid.jqGrid('saveRow', ediId);
                var rowData = grid.jqGrid('getRowData', ediId);
                if (rowData.rowid > 0 && rowData.Actionstatus != 1) {
                    rowData.Actionstatus = 2;
                    grid.jqGrid('getGridParam', 'data')[ediId - 1] = rowData;
                }

                var row = grid.jqGrid('getRowData', id);
                grid.jqGrid('delRowData', id);
                if (row.Actionstatus != 1) {
                    DeleteRows.push(row);
                }
                var data = jQuery("#jqGrid").jqGrid('getGridParam', 'data');
                for (var i = 0; i < data.length; i++) {
                    if (data[i].HyperField_ID == 0) {
                        data[i].rowid = i + 1;
                        data[i].id = i + 1;
                    } else
                        data[i].rowid = i + 1;
                }
                var curpage = parseInt($(".ui-pg-input").val());


                jQuery('#jqGrid').jqGrid('clearGridData');
                jQuery('#jqGrid').jqGrid('setGridParam', { data: data });
                $("#jqGrid").trigger("reloadGrid", [{ page: curpage }]);
                $("#jqGrid").trigger("reloadGrid", [{ page: curpage }]);
                lastSelection = id;
            }
        }

        function unique(list) {
            var result = [];
            $.each(list, function (i, e) {
                if ($.inArray(e, result) == -1) result.push(e);
            });
            return result;
        }



    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" runat="Server">
</asp:Content>

