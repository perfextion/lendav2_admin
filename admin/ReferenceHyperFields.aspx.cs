﻿using System;
using System.Data;
using System.Web.Script.Services;
using System.Web.Services;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_ReferenceHyperFields : System.Web.UI.Page
{
    static string dbKey = "gp_conn";

    protected void Page_Load(object sender, EventArgs e)
    {

    }
    [WebMethod]
    public static string BindReferenceHyperFields()
    {
        string sql_qry = "select [HyperField_ID],[HyperField_TableName],[HyperField_Name],[Array_Item_ID_Exp],[HyperField_Expression],[Chevron_ID],[Exception_ID],[Status] from Ref_HyperFields";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
    }

    [WebMethod, ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static void SaveReferenceHyperFields(dynamic lst)
    {
        //Dictionary<string, string> dic = lst;

        foreach (var item in lst)
        {
            string qry = "";
            int val = Convert.ToInt32(item["Actionstatus"]);
            if (val == 1)
            {

                qry = "INSERT INTO Ref_HyperFields ([HyperField_TableName],[HyperField_Name],[Array_Item_ID_Exp] ,[HyperField_Expression],[Chevron_ID],[Exception_ID],[Status]) " + " VALUES('" + ManageQuotes(item["HyperField_TableName"]) + "','" + ManageQuotes(item["HyperField_Name"]) + "','" + ManageQuotes(item["Array_Item_ID_Exp"]) + "','" + ManageQuotes(item["HyperField_Expression"]) + "','" + ManageQuotes(item["Chevron_ID"]) + "','" + ManageQuotes(item["Exception_ID"]) + "','" + ManageQuotes(item["Status"]) + "')";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 2)
            {
                qry = "UPDATE Ref_HyperFields SET [HyperField_TableName] = '" + ManageQuotes(item["HyperField_TableName"]) + "' ,[HyperField_Name] = '" + ManageQuotes(item["HyperField_Name"]) + "'," + "[Array_Item_ID_Exp] = '" + ManageQuotes(item["Array_Item_ID_Exp"]) + "' ,[HyperField_Expression] = '" + ManageQuotes(item["HyperField_Expression"]) + "' ,[Chevron_ID] = '" + ManageQuotes(item["Chevron_ID"]) + "',[Exception_ID] = '" + ManageQuotes(item["Exception_ID"]) + "',[Status] = '" + ManageQuotes(item["Status"]) + "' where HyperField_ID=" + item["HyperField_ID"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 3)
            {
                qry = "DELETE FROM Ref_HyperFields  where HyperField_ID=" + item["HyperField_ID"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }

        }
    }

    public static string ManageQuotes(string value)
    {
        if (!String.IsNullOrEmpty(value) && value.Contains("'"))
        {
            value = value.Replace("'", "''");
        }
        return value;
    }
}