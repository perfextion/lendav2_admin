﻿using System;
using System.Data;
using System.Web.Script.Services;
using System.Web.Services;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;

public partial class admin_ReferenceLoanStatus : BasePage
{
    public admin_ReferenceLoanStatus()
    {
        Table_Name = "Ref_Loan_Status";
    }

    [WebMethod]
    public static string GetTableInfo()
    {
        return Get_Table_Info();
    }

    [WebMethod]
    public static string BindReferenceLoanStatus()
    {
        string sql_qry = "select [Loan_Status_ID],[Loan_Status_Code], [Loan_Status_Name],[Sort_Order],[Icon_Code],[Status] from Ref_Loan_Status";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
    }

    [WebMethod, ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static void SaveReferenceLoanStatus(dynamic lst)
    {
        //Dictionary<string, string> dic = lst;

        foreach (var item in lst)
        {
            string qry = "";
            int val = Convert.ToInt32(item["Actionstatus"]);
            if (val == 1)
            {

                qry = "INSERT INTO Ref_Loan_Status ([Loan_Status_Code], [Loan_Status_Name],[Sort_Order],[Icon_Code],[Status]) " +
                    " VALUES('" + ManageQuotes(item["Loan_Status_Code"]) + "','" + ManageQuotes(item["Loan_Status_Name"]) + "','" + ManageQuotes(item["Sort_Order"]) + "','" + ManageQuotes(item["Icon_Code"]) + "','" + ManageQuotes(item["Status"]) + "')";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 2)
            {
                qry = "UPDATE Ref_Loan_Status SET [Loan_Status_Code] = '" + ManageQuotes(item["Loan_Status_Code"]) + "' ,[Loan_Status_Name] = '" + ManageQuotes(item["Loan_Status_Name"]) + "' ,[Sort_Order] = '" + ManageQuotes(item["Sort_Order"]) + "'," +
                    "[Icon_Code] = '" + ManageQuotes(item["Icon_Code"]) + "' ,[Status] = '" + ManageQuotes(item["Status"]) + "' where Loan_Status_ID=" + item["Loan_Status_ID"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 3)
            {
                qry = "DELETE FROM Ref_Loan_Status  where Loan_Status_ID=" + item["Loan_Status_ID"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
        }

        gen_db_utils.Update_Table_Audit_Trail("Ref_Loan_Status", userid, dbKey);
    }

    public static string ManageQuotes(string value)
    {
        if (!String.IsNullOrEmpty(value) && value.Contains("'"))
        {
            value = value.Replace("'", "''");
        }
        return value;
    }
}