﻿<%@ Page Title="" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="ReferenceMasterFarmer.aspx.cs" Inherits="admin_ReferenceMasterFarmer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href='../css/fullcalendar.min.css' rel='stylesheet' />
    <link href='../css/fullcalendar.print.min.css' rel='stylesheet' media='print' />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src='../js/moment.min.js'></script>
    <script src='../js/fullcalendar/jquery.min.js'></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src='../js/fullcalendar/fullcalendar.min.js'></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <div class="loader"></div>
    <div class="col-md-12 col-sm-4" style="margin-top: 15px">
        <div class="col-md-3">

           <%-- <div id="btnAddFarmer" class="btn btn-md btn-primary" style="margin-left: 2%">
                Add New Farmer
            </div>--%>
        </div>
    </div>
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <%--style="width:758px"--%>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">User Details</h4>
                </div>
                <div class="modal-body" style="border-bottom: 1px solid #e5e5e5;">
                    <div class="row" style="margin-left: 5px">
                        <div id="divModelSave">
                            <div class="col-md-12">
                                <div id="divFarmerID" class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtFarmerId">Farmer ID</label>
                                        <input type="text" id="txtFid" class="form-control" disabled="disabled" name="Farmer Type ID" style="width: 250px" />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtSearch">Farmer ID Type</label>
                                        <input type="text" id="txtfarmeridtype" class="form-control" name="FarmerIDType" style="width: 250px" />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtSearch">Farmer SSN Hash</label>
                                        <input type="text" id="txtFarmerSSNHash" class="form-control" name="FarmerSSNHash" style="width: 250px" />
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtFarmerfn">Farmer First Name</label>
                                        <input type="text" id="txtfarmerfirstname" class="form-control" name="Farmer First Name" style="width: 250px" />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtSearch">Farmer MI</label>
                                        <input type="text" id="txtfarmermi" class="form-control" name="Farmermi" style="width: 250px" />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtSearch">Farmer Last Name</label>
                                        <input type="text" id="txtFarmerlastname" class="form-control" name="Farmerlastname" style="width: 250px" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-body">
                    <div class="row" style="margin-left: 5px">
                        <div>
                            <div class="col-md-12">

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtSearch">Farmer Address</label>
                                        <textarea id="txtFarmerfarmeraddress" style="height: 108px;" class="form-control" name="Farmerfarmeraddress"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtFarmerId">Farmer City</label>
                                        <input type="text" id="txtfcity" class="form-control" name="Farmer City" style="width: 200px" />
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtSearch">Farmer State ID</label>
                                        <select id="ddlfarmerstate" class="form-control" style="width: 80%;">
                                            <option value=" ">Select</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtSearch">Farmer Zip</label>
                                        <input type="text" id="txtfarmerzip" class="form-control" name="Farmerzip" style="width: 200px" />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtFarmerId">Farmer Phone</label>
                                        <input type="text" id="txtfarmerphone" class="form-control" name="farmerphone" style="width: 200px" />
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtSearch">Farmer Email</label>
                                        <input type="text" id="txtfarmeremail" class="form-control" name="farmeremail" style="width: 250px" />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtSearch">Farmer Pref Contact Ind</label>
                                        <input type="text" id="txtfarmerprefcontactind" class="form-control" style="width: 200px" name="txtfarmerprefcontactind" />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtSearch">Farmer DL State</label>
                                        <select id="ddlfarmerdlstate" class="form-control" style="width: 80%;">
                                            <option value=" ">Select</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtSearch">FarmerDLNum</label>
                                        <input type="text" id="txtfarmerdlnum" class="form-control" style="width: 200px" name="txtfarmerprefcontactind" />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtSearch">Farmer DOB</label>
                                        <input type="text" id="txtfarmerdob" class="form-control datepicker" name="farmerdob" style="width: 200px" />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtSearch">Year Begin farming</label>
                                        <input type="text" id="txtyearbeginfarming" class="form-control" style="width: 200px" name="txtyearbeginfarming" />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtSearch">Year Begin Client</label>
                                        <input type="text" id="txtyearbeginclient" class="form-control" name="yearbeginclient" style="width: 200px" />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtFarmerId">Farmer Entity Type</label>
                                        <input type="text" id="txtFarmerentitytype" class="form-control" name="FarmerEntityType" style="width: 200px" />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtSearch">Farmer Entity Notes</label>
                                        <input type="text" id="txtfarmerentitynotes" class="form-control" name="FarmerEntityNotes" style="width: 200px" />
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtSearch">Status</label>
                                        <select id="ddlstatus" class="form-control" style="width: 80%;">
                                            <option value="1">Active</option>
                                            <option value="0">InActive</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-8">
                                </div>
                                <div class="col-md-4 text-center">
                                    <div class="btn btn-primary" id="btnModalSave">Save</div>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>

            </div>

        </div>
    </div>
    <div class="modal fade" id="myModal1" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <%--style="width:758px"--%>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Confirm Delete</h4>
                </div>
                <div class="modal-body">
                    <div class="row" style="margin-left: 5px">
                        <div id="divModelSave1">
                            <div>
                                <input type="hidden" id="hdn_farmer_master_id" />
                                <h4>Are you sure, want to delete this Farmer details ?</h4>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-8">
                                </div>
                                <div class="col-md-4 text-center">
                                    <div class="btn btn-primary" id="btnModalYes">YES</div>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">NO</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top:-12px">
        <div class="col-md-11" style="padding-left: 51px;">
            <asp:Label ID="lbl_table" runat="server" Text="lbl_table"></asp:Label>
        </div>
    </div>
    <script>
        $(function () {
            $(".datepicker").datepicker({ dateFormat: 'yy-mm-dd' });
        });
        $(function () {

            var obj = {
                Init: function () {
                    obj.buttonsave();
                    // obj.loadtable();
                    obj.getallstates();
                    $('#navbtnadd').on("click", function () {
                        obj.ClearModelData();
                        $('#divFarmerID').hide();
                        $('#divModelSave').show();
                        $('#myModal').modal('show');

                    });
                    $("#navbtndownload").click(function () {
                        exportTableToExcel('tblActiveUsers', 'ReferenceMasterFarmer');
                    });
                    $("#navbtnrefresh").click(function () {
                        obj.bindgrid();
                    });
                    $('.loader').hide();
                    $('#btnModalYes').click(function () {
                        obj.buttonYes();
                    });
                    $(document).keydown(function (event) {
                        if (event.keyCode == 27) {
                            $('#myModal').modal('hide');
                        }
                    });
                },
                buttonYes: function () {
                    $('.loader').show();
                    var divModelSave = $('#divModelSave1').show();
                    $('#spnException').hide();
                    $.ajax({
                        type: "POST",
                        url: "ReferenceMasterFarmer.aspx/DeleteFarmerDetails",
                        data: JSON.stringify({ Farmer_master_id: $('#hdn_farmer_master_id').val() }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            $('.loader').show();
                            obj.loadtable();
                            $('#myModal1').modal('hide');
                        },
                        failure: function (response) {
                        }
                    });

                },
                ClearModelData: function () {
                    $('#txtFid').val('');
                    $('#txtfarmeridtype').val('');
                    $('#txtFarmerSSNHash').val('');
                    $('#txtFarmerentitytype').val('');
                    $('#txtfarmerentitynotes').val('');
                    $('#txtFarmerlastname').val('');
                    $('#txtfarmerfirstname').val('');
                    $('#txtfarmermi').val('');
                    $('#txtFarmerfarmeraddress').val('');
                    $('#txtfcity').val('');
                    $('#ddlfarmerstate').val('');
                    $('#txtfarmerzip').val('');
                    $('#txtfarmerphone').val('');
                    $('#txtfarmeremail').val('');
                    $('#txtfarmerprefcontactind').val('');
                    $('#ddlfarmerdlstate').val('');
                    $('#txtfarmerdlnum').val('');
                    $('#txtfarmerdob').val('');
                    $('#txtyearbeginfarming').val('');
                    $('#txtyearbeginclient').val('');
                    $('#ddlstatus').val('');
                },
                buttonsave: function () {
                    $('#btnModalSave').click(function () {
                        $('.loader').show();
                        var dataobj = {};
                        dataobj.Farmer_ID = $('#txtFid').val();
                        dataobj.Farmer_ID_Type = $('#txtfarmeridtype').val();
                        dataobj.Farmer_SSN_Hash = $('#txtFarmerSSNHash').val();
                        dataobj.Farmer_Entity_Type = $('#txtFarmerentitytype').val();
                        dataobj.Farmer_Entity_Notes = $('#txtfarmerentitynotes').val();
                        dataobj.Farmer_Last_Name = $('#txtFarmerlastname').val();
                        dataobj.Farmer_First_Name = $('#txtfarmerfirstname').val();
                        dataobj.Farmer_MI = $('#txtfarmermi').val();
                        dataobj.Farmer_Address = $('#txtFarmerfarmeraddress').val();
                        dataobj.Farmer_City = $('#txtfcity').val();
                        dataobj.Farmer_State_ID = $('#ddlfarmerstate').val();
                        dataobj.Farmer_Zip = $('#txtfarmerzip').val();
                        dataobj.Farmer_Phone = $('#txtfarmerphone').val();
                        dataobj.Farmer_Email = $('#txtfarmeremail').val();
                        dataobj.Farmer_Pref_Contact_Ind = $('#txtfarmerprefcontactind').val();
                        dataobj.Farmer_DL_State = $('#ddlfarmerdlstate').val();
                        dataobj.Farmer_DL_Num = $('#txtfarmerdlnum').val();
                        dataobj.Farmer_DOB = $('#txtfarmerdob').val();
                        dataobj.Year_Begin_farming = $('#txtyearbeginfarming').val();
                        dataobj.Year_Begin_Client = $('#txtyearbeginclient').val();
                        dataobj.Status = $('#ddlstatus').val();

                        var divModelSave = $('#divModelSave').show();
                        $('#spnException').hide();
                        $.ajax({
                            type: "POST",
                            url: "ReferenceMasterFarmer.aspx/UpdateFarmerDetails",
                            data: JSON.stringify({ lst: dataobj }),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (data) {
                                $('.loader').show();
                                obj.loadtable();
                                $('#myModal').modal('hide');
                            },
                            failure: function (response) {
                            }
                        });
                    });
                },
                loadtable: function () {

                    $('#ContentPlaceHolder1_lbl_table').text('');
                    $.ajax({
                        type: "POST",
                        url: "ReferenceMasterFarmer.aspx/LoadTable",
                        //  data: JSON.stringify({ }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            $('#ContentPlaceHolder1_lbl_table').html(res.d);
                            $('.loader').hide();
                        }
                    });

                },
                getallstates: function () {
                    $.ajax({
                        type: "POST",
                        url: "ReferenceMasterFarmer.aspx/Getallstates",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            $.each(res.d, function (data, value) {
                                $("#ddlfarmerstate").append($("<option></option>").val(data).html(value));
                                $("#ddlfarmerdlstate").append($("<option></option>").val(data).html(value));

                            });
                        }

                    });
                }
            }
            obj.Init();
        });
        function DeleteRecord(id) {
            $('#hdn_farmer_master_id').val(id);
            $('#divModelSave1').show();
            $('#myModal1').modal('show');
        }
        function manageRecord(id) {
            $('#divFarmerID').show();

            $.ajax({
                type: "POST",
                url: "ReferenceMasterFarmer.aspx/GetRefMasterFarmerByID",
                data: JSON.stringify({ Farmer_ID: id }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (res) {

                    var objdata = JSON.parse(res.d);
                    var data = objdata[0];
                    $('#txtFid').val(data.Farmer_ID);
                    $('#txtfarmeridtype').val(data.Farmer_ID_Type);
                    $('#txtFarmerSSNHash').val(data.Farmer_SSN_Hash);
                    $('#txtFarmerentitytype').val(data.Farmer_Entity_Type);
                    $('#txtfarmerentitynotes').val(data.Farmer_Entity_Notes);
                    $('#txtFarmerlastname').val(data.Farmer_Last_Name);
                    $('#txtfarmerfirstname').val(data.Farmer_First_Name);
                    $('#txtfarmermi').val(data.Farmer_MI);
                    $('#txtFarmerfarmeraddress').val(data.Farmer_Address);
                    $('#txtfcity').val(data.Farmer_City);
                    $('#ddlfarmerstate').val(data.Farmer_State_ID);
                    $('#txtfarmerzip').val(data.Farmer_Zip);
                    $('#txtfarmerphone').val(data.Farmer_Phone);
                    $('#txtfarmeremail').val(data.Farmer_Email);
                    $('#txtfarmerprefcontactind').val(data.Farmer_Pref_Contact_Ind);
                    $('#ddlfarmerdlstate').val(data.Farmer_DL_State);
                    $('#txtfarmerdlnum').val(data.Farmer_DL_Num);
                    $('#txtfarmerdob').val(data.Farmer_DOB);
                    $('#txtyearbeginfarming').val(data.Year_Begin_farming);
                    $('#txtyearbeginclient').val(data.Year_Begin_Client);
                    $('#ddlstatus').val(data.Status);
                    $('#divModelSave').show();
                    $('#myModal').modal('show');
                }
            });
        }
        $('#navbtnsave').addClass('disabled');
        $('#navbtncolumns').addClass('disabled');
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" runat="Server">
</asp:Content>

