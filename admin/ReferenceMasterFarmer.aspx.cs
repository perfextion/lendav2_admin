﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_ReferenceMasterFarmer : System.Web.UI.Page
{
    static string dbKey = "gp_conn";
    protected void Page_Load(object sender, EventArgs e)
    {
        lbl_table.Text = LoadTable();
    }

    [WebMethod]
    public static void UpdateFarmerDetails(Dictionary<string, string> lst)
    {
        MasterFarmer objmanag = new MasterFarmer();
        string json = JsonConvert.SerializeObject(lst);
        MasterFarmer objfarmer = JsonConvert.DeserializeObject<MasterFarmer>(json);
        string qry = "";
        if (objfarmer.Farmer_ID != "")
        {
            qry = "UPDATE Master_Farmer SET Farmer_ID_Type = '" + objfarmer.Farmer_ID_Type + "',Farmer_SSN_Hash = '" + objfarmer.Farmer_SSN_Hash + "'," +
                "Farmer_Entity_Type = '" + objfarmer.Farmer_Entity_Type + "',Farmer_Entity_Notes = '" + objfarmer.Farmer_Entity_Notes + "'," +
                "Farmer_Last_Name = '" + objfarmer.Farmer_Last_Name + "',Farmer_First_Name = '" + objfarmer.Farmer_First_Name + "'," +
                "Farmer_MI = '" + objfarmer.Farmer_MI + "',Farmer_Address = '" + objfarmer.Farmer_Address + "',Farmer_City = '" + objfarmer.Farmer_City + "'," +
                "Farmer_State_ID = '" + objfarmer.Farmer_State_ID + "',Farmer_Zip = '" + objfarmer.Farmer_Zip + "',Farmer_Phone = '" + objfarmer.Farmer_Phone + "'," +
                "Farmer_Email = '" + objfarmer.Farmer_Email + "',Farmer_Pref_Contact_Ind = '" + objfarmer.Farmer_Pref_Contact_Ind + "'," +
                "Farmer_DL_State = '" + objfarmer.Farmer_DL_State + "',Farmer_DL_Num = '" + objfarmer.Farmer_DL_Num + "',Farmer_DOB = '" + objfarmer.Farmer_DOB + "'," +
                "Year_Begin_farming = '" + objfarmer.Year_Begin_farming + "',Year_Begin_Client = '" + objfarmer.Year_Begin_Client + "'," +
                "Status = '" + objfarmer.Status + "'WHERE Farmer_ID = '" + objfarmer.Farmer_ID + "'";
        }
        else
        {
            qry = "INSERT INTO Master_Farmer (Farmer_ID_Type,Farmer_SSN_Hash,Farmer_Entity_Type,Farmer_Entity_Notes," +
                "Farmer_Last_Name,Farmer_First_Name,Farmer_MI,Farmer_Address,Farmer_City,Farmer_State_ID,Farmer_Zip," +
                "Farmer_Phone,Farmer_Email,Farmer_Pref_Contact_Ind,Farmer_DL_State,Farmer_DL_Num,Farmer_DOB,Year_Begin_farming," +
                "Year_Begin_Client,Status) VALUES('" + objfarmer.Farmer_ID_Type + "','" + objfarmer.Farmer_SSN_Hash + "'," +
                "'" + objfarmer.Farmer_Entity_Type + "', '" + objfarmer.Farmer_Entity_Notes + "','" + objfarmer.Farmer_Last_Name + "'," +
                "'" + objfarmer.Farmer_First_Name + "','" + objfarmer.Farmer_MI + "', '" + objfarmer.Farmer_Address + "','" + objfarmer.Farmer_City + "'," +
                " '" + objfarmer.Farmer_State_ID + "','" + objfarmer.Farmer_Zip + "','" + objfarmer.Farmer_Phone + "','" + objfarmer.Farmer_Email + "'," +
                "'" + objfarmer.Farmer_Pref_Contact_Ind + "','" + objfarmer.Farmer_DL_State + "','" + objfarmer.Farmer_DL_Num + "'," +
                "'" + objfarmer.Farmer_DOB + "','" + objfarmer.Year_Begin_farming + "','" + objfarmer.Year_Begin_Client + "','" + objfarmer.Status + "')";

        }
        gen_db_utils.gp_sql_execute(qry, dbKey);
    }

    [WebMethod]
    public static string LoadTable()
    {
        string str_sql = "";
        string str_return = "";


        str_sql = "SELECT a.Farmer_ID,b.loan_full_id,a.Farmer_ID_Type,a.Farmer_SSN_Hash,a.Farmer_Entity_Type,Farmer_Entity_Notes, " +
            "a.Farmer_Last_Name,a.Farmer_First_Name,a.Farmer_MI,Farmer_Address,a.Farmer_City,Farmer_State_ID,a.Farmer_Zip, " +
            "a.Farmer_Phone,a.Farmer_Email,Farmer_Pref_Contact_Ind,a.Farmer_DL_State,a.Farmer_DL_Num,a.Farmer_DOB,a.Year_Begin_farming, " +
            "a.Year_Begin_Client,Status from Master_Farmer a " +
            "LEFT JOIN	 (SELECT farmer_id, loan_full_id =STUFF((SELECT ',' + loan_full_id FROM loan_master bb WHERE bb.farmer_id = aa.farmer_id FOR XML PATH('')), 1, 1, '') " +
            "FROM loan_master aa GROUP BY farmer_id)	b on a.Farmer_ID = b.Farmer_ID ";


        DataTable dt1 = gen_db_utils.gp_sql_get_datatable(str_sql, dbKey);
        if (dt1 != null && dt1.Rows.Count > 0)
        {
            str_return += "<span style='color: green;'>Count of records : " + dt1.Rows.Count + "</span><div> <table id='tblActiveUsers' class='table table-striped table-bordered' >  "
                              + "<thead> "
                              + "     <tr> "
                             + "      <th > Farmer Details </th> "
                             + "      <th > Loan Full Id </th> "
                             + "      <th > ID Type Details </th> "
                             + "      <th > Address Details </th> "
                             + "      <th > Farming Details </th> "


                             + "      <th style='text-align:center;'> Action </th> "
                                         + "      <th style='text-align:center;'> Delete </th> "



                              + "    </tr> "
                              + "      </thead> "
                              + "        <tbody> ";
            string datetime = "";
            foreach (DataRow dtr1 in dt1.Rows)
            {

                if (dtr1["Farmer_DOB"].ToString() != "")
                {
                    datetime = Convert.ToDateTime(dtr1["Farmer_DOB"]).ToString("yyyy-MM-dd");
                }
                else
                {
                    datetime = "";
                }
                str_return += "<tr>"
                             + "<td> (" + dtr1["Farmer_ID"].ToString() + "" +
                               ") <br>" + dtr1["Farmer_First_Name"].ToString() + " " + dtr1["Farmer_MI"].ToString() + "" + dtr1["Farmer_Last_Name"].ToString() + "<br>" +
                               dtr1["Farmer_DL_State"].ToString() + "" + dtr1["Farmer_DL_Num"].ToString() + "<br>" +
                               datetime +
                               "</td>"

                               + "<td>" + dtr1["loan_full_id"].ToString().Replace(",", "<br>") + " </td>"

                               + "<td>Farmer ID Type : " + dtr1["Farmer_ID_Type"].ToString() +
                               "<br>SSN : " + dtr1["Farmer_SSN_Hash"].ToString() +
                               "<br>Entity : " + dtr1["Farmer_Entity_Type"].ToString() +
                               "</td>"

                              + "<td>" + dtr1["Farmer_Address"].ToString() +
                              "<br>" + dtr1["Farmer_City"].ToString() + " , " + dtr1["Farmer_State_ID"].ToString() + " , " + dtr1["Farmer_Zip"].ToString() +
                              "<br>" + dtr1["Farmer_Phone"].ToString() + "<br>" + dtr1["Farmer_Email"].ToString() +
                              "</td>"

                              + "<td>" + dtr1["Year_Begin_farming"].ToString() + "<br>" + dtr1["Year_Begin_Client"].ToString() + "<br>" + dtr1["Status"].ToString() + "</td>"
                             + "<td style='text-align:center;'><a href =' javascript:manageRecord(" + dtr1["Farmer_ID"] + ")' > Manage</a></td>"
                             + "<td style='text-align:center;'><a href =' javascript:DeleteRecord(" + dtr1["Farmer_ID"] + ")' > Delete</a></td>"

                               + "</tr>";

            }

            str_return += " </tbody> "
                                    + " </table> "
                                      + "</div></div></div >";
        }
        else
        {
            str_return = "    No data available...";
        }

        return str_return;

    }
    [WebMethod]
    public static void DeleteFarmerDetails(string Farmer_master_id)
    {
        string qry = " Delete from Master_Farmer where Farmer_ID='" + Farmer_master_id + "'";
        gen_db_utils.gp_sql_execute(qry, dbKey);
    }
    [WebMethod]
    public static string GetRefMasterFarmerByID(string Farmer_ID)
    {
        string sql_qry = "SELECT Farmer_ID,Farmer_ID_Type,Farmer_SSN_Hash,Farmer_Entity_Type,Farmer_Entity_Notes," +
                    "Farmer_Last_Name,Farmer_First_Name,Farmer_MI,Farmer_Address,Farmer_City,Farmer_State_ID,Farmer_Zip," +
                    "Farmer_Phone,Farmer_Email,Farmer_Pref_Contact_Ind,Farmer_DL_State,Farmer_DL_Num,Farmer_DOB,Year_Begin_farming," +
                    "Year_Begin_Client,Status from Master_Farmer  where Farmer_ID='" + Farmer_ID + "'";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);

    }

    [WebMethod]
    public static Dictionary<string, string> Getallstates()
    {
        string sql_qry = "select State_ID,CONCAT( State_ID,'.',State_Name) as State_Name,State_Abbrev from Ref_State order by State_ID";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);

        return dt.AsEnumerable().ToDictionary<DataRow, string, string>(row => row[2].ToString(),
                                       row => row[1].ToString());
    }
}
public class MasterFarmer
{
    public string Farmer_ID { set; get; }
    public string Farmer_ID_Type { set; get; }
    public string Farmer_SSN_Hash { set; get; }
    public string Farmer_Entity_Type { set; get; }
    public string Farmer_Entity_Notes { set; get; }
    public string Farmer_Last_Name { set; get; }
    public string Farmer_First_Name { set; get; }
    public string Farmer_MI { set; get; }
    public string Farmer_Address { set; get; }
    public string Farmer_City { set; get; }
    public string Farmer_State_ID { set; get; }
    public string Farmer_Zip { set; get; }
    public string Farmer_Phone { set; get; }
    public string Farmer_Email { set; get; }
    public string Farmer_Pref_Contact_Ind { set; get; }
    public string Farmer_DL_State { set; get; }
    public string Farmer_DL_Num { set; get; }
    public string Farmer_DOB { set; get; }
    public string Year_Begin_farming { set; get; }
    public string Year_Begin_Client { set; get; }
    public string Status { set; get; }
    public MasterFarmer()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}