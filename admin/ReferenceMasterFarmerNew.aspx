﻿<%@ Page Title="Borrower Farmer Affiliate Name" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="ReferenceMasterFarmerNew.aspx.cs" Inherits="admin_ReferenceMasterFarmerNew" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .ui-jqgrid .ui-jqgrid-htable th {
            height: 45px;
        }

        .cvteste {
            padding-right: 14px !important;
        }

        #content {
            padding: 14px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="ref-master-borrower-section reference-table">
        <div class="row switch-controls">
            <div class="switch-control disabled" id="editControl">
                <label for="editBtn">Edit</label>
                <div class="toggle-btn small">            
                    <input type="checkbox" id="editBtn" class="cb-value" />
                    <span class="round-btn"></span>
                </div>
            </div>
            <div class="switch-control disabled" id="enableControl">
                <label for="enableBtn">Enable</label>
                <div class="toggle-btn small">            
                    <input type="checkbox" id="enableBtn" class="cb-value" />
                    <span class="round-btn"></span>
                </div>
            </div>
        </div>
        <div class="loader"></div>
        <div class="row main-content">
            <div>
                <table id="jqGrid"></table>
                <div id="jqGridPager"></div>
            </div>
        </div>
    </div>
    <br />
    <br />
    <br />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" runat="Server">
    <script type="text/javascript">
        var DeleteRows = [];
        var timer;
        var lastSelection;
        var entity_types = {};
        var pref_contacts = {};
        var state = {};
        var canEdit = false;

        $(function () {
            var obj = {
                Init: function () {
                    $('.loader').show();
                    obj.getListItems();
                    obj.bindGrid();
                    obj.getTableInfo();

                    $("#navbtnadd").click(function () {
                        obj.add();
                    });

                    $("#navbtnsave").click(function () {
                        $('.loader').show();
                        obj.save();
                    });

                    $("#navbtnrefresh").click(function () {
                        $('.loader').show();
                        lastSelection = null;
                        obj.bindGrid();
                    });

                    $("#navbtndownload").click(function () {
                        $("#jqGrid").jqGrid("exportToExcel", {
                            includeLabels: true,
                            includeGroupHeader: true,
                            includeFooter: true,
                            fileName: "Borrower Farmer Affiliate Name.xlsx",
                            maxlength: 40, // maxlength for visible string data,
                            replaceStr: function _replStrFunc(v) {
                                return v.replace('&#160;', '');
                            }
                        })
                    });

                    $("#txtSearchBar").on("keyup", function () {
                        var self = this;
                        obj.search(self);
                    });

                    $('#navbtncolumns').click(function () {
                        $("#jqGrid").columnChooser({});
                    });

                    $('#editBtn').change(function () {
                        canEdit = $(this).is(':checked');
                        if (!canEdit) {
                            if (lastSelection != null) {
                                var grid = $("#jqGrid");
                                grid.jqGrid('saveRow', lastSelection);
                                grid.jqGrid('restoreRow', lastSelection);
                                lastSelection = null;
                            }
                        }
                    });

                },
                getTableInfo: function() {
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "ReferenceMasterFarmerNew.aspx/GetTableInfo",
                        data: JSON.stringify({  }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            var data = JSON.parse(res.d);

                            var table_info = data.Table_Info[0];
                            $('#txtTableName').html(table_info.Table_Name);
                            $('#txtLastUpdated').html(table_info.Last_Updated);
                            $('#txtRecordsCount').html(table_info.Records_Count);

                            if (data.CanEdit) {
                                $('#editControl').removeClass('disabled');
                            } else {
                                $('#editControl').addClass('disabled');
                            }

                            $('#enableControl').addClass('disabled');
                            
                        }
                    });
                },
                getListItems: function () {
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "ReferenceMasterFarmerNew.aspx/GetListItems",
                        data: JSON.stringify({}),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            var data = JSON.parse(res.d);

                            entity_types = {};
                            pref_contacts = {};
                            state = {};

                            for (var i of data) {
                                if (i.List_Group_Code == 'BORROWER_ENTITY_TYPE') {
                                    entity_types[i.List_Item_Value] = i.List_Item_Name;
                                }

                                if (i.List_Group_Code == 'PREF_CONTACT') {
                                    pref_contacts[i.List_Item_Value] = i.List_Item_Name;
                                }

                                if (i.List_Group_Code == 'STATE') {
                                    state[i.List_Item_Value] = i.List_Item_Name;
                                }
                            }
                        }

                    });
                },
                bindGrid: function () {
                    $.ajax({
                        type: "POST",
                        url: "ReferenceMasterFarmerNew.aspx/GetMasterFarmerData",
                        data: JSON.stringify({}),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {

                            var resData = JSON.parse(data.d);
                            $('.loader').show();
                            for (var i = 0; i < resData.length; i++) {
                                resData[i].rowid = i + 1;
                            }
                            obj.loadgrid(resData);
                            $('.loader').hide();
                        }
                    });
                },
                loadgrid: function (data) {
                    $.jgrid.gridUnload("#jqGrid");
                    $("#jqGrid").jqGrid({
                        datatype: 'local',
                        styleUI: 'Bootstrap',
                        data: data,
                        colModel: [
                            {
                                label: 'rowid',
                                name: 'rowid',
                                width: 75,
                                editable: false,
                                key: true,
                                hidden: true,
                                hidedlg: true
                            },
                            {
                                label: 'Farmer ID',
                                name: 'Farmer_ID',
                                align: 'center',
                                classes: 'center',
                                width: 120,
                                editable: false,
                                sorttype: 'integer'
                            },
                            {
                                label: 'Farmer ID Type',
                                name: 'Farmer_ID_Type',
                                align: 'left',
                                classes: 'left',
                                width: 120,
                                editable: true,
                                edittype: 'select',
                                editoptions: {
                                    value: {
                                        1: 'SSN',
                                        2: 'Tax ID'
                                    }
                                },
                                formatter: 'select'
                            },
                            {
                                label: 'Farmer SSN Hash',
                                name: 'Farmer_SSN_Hash',
                                align: 'left',
                                classes: 'left',
                                width: 120,
                                editable: true,
                                editoptions: {
                                    maxlength: 9,
                                    dataEvents: [
                                        {
                                            type: 'keypress',
                                            fn: function (e) {
                                                return NumberOnly(e, this, false);
                                            }
                                        }
                                    ]
                                }
                            },
                            {
                                label: 'Farmer Entity Type',
                                name: 'Farmer_Entity_Type',
                                align: 'center',
                                classes: 'center',
                                width: 120,
                                editable: true
                            },
                            {
                                label: 'Farmer Entity Notes',
                                name: 'Farmer_Entity_Notes',
                                align: 'left',
                                classes: 'left',
                                width: 180,
                                editable: true
                            },
                            {
                                label: 'Farmer Last Name',
                                name: 'Farmer_Last_Name',
                                align: 'left',
                                classes: 'left',
                                width: 120,
                                editable: true
                            },
                            {
                                label: 'Farmer First Name',
                                name: 'Farmer_First_Name',
                                align: 'left',
                                classes: 'left',
                                width: 120,
                                editable: true
                            },
                            {
                                label: 'Farmer MI',
                                name: 'Farmer_MI',
                                align: 'center',
                                width: 90,
                                editable: true,
                                editoptions: {
                                    maxlength: 1
                                }
                            },
                            {
                                label: 'Farmer Address',
                                name: 'Farmer_Address',
                                align: 'left',
                                classes: 'left',
                                width: 180,
                                editable: true
                            },
                            {
                                label: 'Farmer City',
                                name: 'Farmer_City',
                                align: 'left',
                                classes: 'left',
                                width: 120,
                                editable: true
                            },
                            {
                                label: 'Farmer State Abbrev',
                                name: 'Farmer_State_ID',
                                align: 'center',
                                width: 120,
                                editable: true,
                                edittype: 'select',
                                editoptions: {
                                    value: state
                                },
                                formatter: 'select'
                            },
                            {
                                label: 'State_Name',
                                name: 'Farmer_State_ID',
                                align: 'left',
                                classes: 'left',
                                width: 120,
                                editable: false,
                                hidden: true,
                                hidedlg: true
                            },
                            {
                                label: 'Farmer Zip',
                                name: 'Farmer_Zip',
                                align: 'left',
                                classes: 'left',
                                width: 120,
                                editable: true,
                                editoptions: {
                                    maxlength: 5,
                                    dataEvents: [
                                        {
                                            type: 'keypress',
                                            fn: function (e) {
                                                return NumberOnly(e, this, false);
                                            }
                                        }
                                    ]
                                }
                            },
                            {
                                label: 'Farmer Phone',
                                name: 'Farmer_Phone',
                                align: 'left',
                                classes: 'left',
                                width: 120,
                                editable: true,
                                editoptions: {
                                    maxlength: 10,
                                    dataEvents: [
                                        {
                                            type: 'keypress',
                                            fn: function (e) {
                                                return NumberOnly(e, this, false);
                                            }
                                        }
                                    ]
                                }
                            },
                            {
                                label: 'Farmer Email',
                                name: 'Farmer_Email',
                                align: 'left',
                                classes: 'left',
                                width: 180,
                                editable: true
                            },
                            {
                                label: 'Farmer Preferred Contact Ind',
                                name: 'Farmer_Pref_Contact_Ind',
                                align: 'left',
                                classes: 'left',
                                width: 120,
                                editable: true,
                                edittype: 'select',
                                editoptions: {
                                    value: pref_contacts
                                },
                                formatter: 'select'
                            },
                            {
                                label: 'Farmer DL State Abbrev',
                                name: 'Farmer_DL_State',
                                align: 'center',
                                width: 120,
                                editable: true,
                                edittype: 'select',
                                editoptions: {
                                    value: state
                                },
                                formatter: 'select'
                            },
                            {
                                label: 'DL_State_Name',
                                name: 'DL_State_Name',
                                align: 'left',
                                classes: 'left',
                                width: 120,
                                editable: false,
                                hidden: true,
                                hidedlg: true
                            },
                            {
                                label: 'Farmer DL Num',
                                name: 'Farmer_DL_Num',
                                align: 'left',
                                classes: 'left',
                                width: 120,
                                editable: true,
                                editoptions: {
                                    maxlength: 10,
                                    dataEvents: [
                                        {
                                            type: 'keypress',
                                            fn: function (e) {
                                                return NumberOnly(e, this, false);
                                            }
                                        }
                                    ]
                                }
                            },
                            {
                                label: 'Farmer DOB',
                                name: 'Farmer_DOB',
                                align: 'left',
                                classes: 'left',
                                width: 120,
                                editable: true,
                                formatter: 'date',
                                formatoptions: {
                                    srcformat: 'd/m/Y',
                                    newformat: 'd/m/Y'
                                },
                                edittype: 'text',
                                editoptions: {
                                    dataInit: function (element) {
                                        var date = new Date();
                                        $(element).datepicker({
                                            id: 'orderDate_datePicker',
                                            dateFormat: 'm/d/yy',
                                            defaultDate: date,
                                            showOn: 'focus'
                                        });
                                    }
                                }
                            },
                            {
                                label: 'FARMing',
                                name: 'Year_Begin_farming',
                                align: 'left',
                                classes: 'left',
                                width: 120,
                                editable: true,
                                editoptions: {
                                    maxlength: 4,
                                    dataEvents: [
                                        {
                                            type: 'keypress',
                                            fn: function (e) {
                                                return NumberOnly(e, this, false);
                                            }
                                        }
                                    ]
                                },
                                sorttype: 'integer'
                            },
                            {
                                label: 'ARMing',
                                name: 'Year_Begin_Client',
                                align: 'left',
                                classes: 'left',
                                width: 120,
                                editable: true,
                                editoptions: {
                                    maxlength: 4,
                                    dataEvents: [
                                        {
                                            type: 'keypress',
                                            fn: function (e) {
                                                return NumberOnly(e, this, false);
                                            }
                                        }
                                    ]
                                },
                                sorttype: 'integer'
                            },
                            {
                                label: 'Status',
                                name: 'Status',
                                align: 'center',
                                width: 90,
                                editable: true,
                                hidden: false
                            },
                            {
                                label: 'Actionstatus',
                                name: 'Actionstatus',
                                width: 70,
                                editable: false,
                                hidden: true,
                                exportcol: false,
                                hidedlg: true
                            },
                            {
                                label: '',
                                name: '',
                                width: 35,
                                align: 'center',
                                formatter: obj.deleteLink,
                                exportcol: false,
                                hidedlg: true
                            }
                        ],
                        viewrecords: true,
                        loadonce: true,
                        restoreAfterSelect: false,
                        saveAfterSelect: true,
                        sortable: true,
                        onSelectRow: obj.edit,
                        onPaging: obj.changePage,
                        height: 'auto',
                        rowNum: 100,
                        pager: '#jqGridPager'
                    });
                },
                add: function () {

                    var grid = $("#jqGrid");
                    var rowsperPage = parseInt(grid.getGridParam('rowNum'));
                    var gridlength = grid.jqGrid('getGridParam', 'data').length;
                    var curpage = parseInt($(".ui-pg-input").val());
                    var totPages = Math.ceil(gridlength / rowsperPage);

                    if (rowsperPage * totPages == gridlength) {
                        var id = $('.inline-edit-cell').parent().parent().prop('id');
                        grid.jqGrid('saveRow', id);
                        var row = obj.newrow();

                        var newRowId = row.rowid;
                        grid.jqGrid('addRowData', newRowId, row, "first");
                        grid.trigger('reloadGrid');
                        lastSelection = newRowId;
                        grid.jqGrid('saveRow', lastSelection);
                        grid.jqGrid('restoreRow', lastSelection);
                        $('.glyphicon-step-forward').trigger('click');
                    } else {
                        $('.glyphicon-step-forward').trigger('click');
                        var row = obj.newrow();

                        var newRowId = row.rowid;
                        grid.jqGrid('addRowData', newRowId, row, "first");
                    }

                    lastSelection = newRowId;
                    grid.jqGrid('saveRow', lastSelection);
                    grid.jqGrid('restoreRow', lastSelection);

                    var eid = $('.inline-edit-cell').parent().parent().prop('id')
                    grid.jqGrid('saveRow', eid);
                    grid.jqGrid('restoreRow', eid);
                    grid.jqGrid('editRow', newRowId);

                    $('#navbtnsave').addClass('syncItems');
                    $('#navbtnsave>i').addClass('syncItems');
                },
                edit: function (id) {

                    if (id && id !== lastSelection && canEdit) {
                        var grid = $("#jqGrid");
                        grid.jqGrid('saveRow', lastSelection);
                        grid.jqGrid('restoreRow', lastSelection);
                        var row = grid.jqGrid('getRowData', lastSelection);

                        if (!jQuery.isEmptyObject(row)) {
                            var dataobj = jQuery("#jqGrid").jqGrid('getGridParam', 'data');
                            index = dataobj.findIndex(x => x.rowid == row.rowid);
                            if (row.rowid > 0 && row.Actionstatus != 1) {
                                row.Actionstatus = 2;
                                grid.jqGrid('getGridParam', 'data')[index] = row;
                            }
                        }

                        grid.jqGrid('editRow', id);
                        lastSelection = id;

                        $('#navbtnsave').addClass('syncItems');
                        $('#navbtnsave>i').addClass('syncItems');
                    }
                },
                newrow: function () {
                    var newid = 0;

                    var grid = $("#jqGrid");
                    var length = jQuery("#jqGrid").jqGrid('getGridParam', 'records');
                    var dataobj = grid.jqGrid('getGridParam', 'data');
                    var id = [];
                    ids = dataobj.map(function (e) { return e.rowid });
                    if (ids.length > 0) {
                        newid = ids.reduce(function (a, b) { return Math.max(a, b); });
                    }

                    var row = {
                        List_Item_Code: '',
                        List_Item_Name: '',
                        List_Group_Code: 'ASSOC_TYPE',
                        Actionstatus: 1,
                        Status: 1,
                        rowid: newid + 1
                    };
                    return row;
                },
                save: function () {

                    $('.loader').show();
                    var grid = $("#jqGrid");
                    var allrows = [];
                    var id = $('.inline-edit-cell').parent().parent().prop('id');
                    grid.jqGrid('saveRow', id);
                    var rowData = grid.jqGrid('getRowData', id);
                    if (rowData.rowid > 0) {
                        rowData.Actionstatus = 2;
                        grid.jqGrid('getGridParam', 'data')[id] = rowData;
                    }

                    for (var i = 0; i < DeleteRows.length; i++) {
                        DeleteRows[i].Actionstatus = 3
                        allrows.push(DeleteRows[i]);
                    }
                    var dataobj = grid.jqGrid('getGridParam', 'data');
                    var newRows = $.grep(dataobj, function (e) {
                        if (e != undefined) {
                            return e.Actionstatus == 1 || e.Actionstatus == 2
                        }
                    });
                    for (var i = 0; i < newRows.length; i++) {
                        allrows.push(newRows[i]);
                    }

                    $.ajax({
                        type: "POST",
                        url: "ReferenceMasterFarmerNew.aspx/SaveAssocType",
                        data: JSON.stringify({ ListItems: allrows }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            toastr.success("Saved Sucessful");
                            $('#navbtnsave').removeClass('syncItems');
                            $('#navbtnsave>i').removeClass('syncItems');
                            if (timer) { clearTimeout(timer); }
                            timer = setTimeout(function () {
                                location.reload();
                            }, 1000);
                        },
                        failure: function (response) {
                            var val = console.log(response.d);
                            toastr.warning(val);
                        }
                    });
                },
                search: function (self) {
                    if (timer) { clearTimeout(timer); }
                    timer = setTimeout(function () {
                        obj.changePage();
                        $("#jqGrid").jqGrid('filterInput', self.value);
                        var length = jQuery("#jqGrid").jqGrid('getGridParam', 'records');
                        $('#txtRecordsCount').html('Records Count: ' + length);
                    }, 0);
                },
                changePage: function () {

                    lastSelection = "";
                    var grid = $("#jqGrid");
                    var id = $('.inline-edit-cell').parent().parent().prop('id');
                    if (id != undefined) {
                        grid.jqGrid('saveRow', id);
                        grid.jqGrid('restoreRow', id);
                        var row = grid.jqGrid('getRowData', id);
                        if (!jQuery.isEmptyObject(row)) {
                            var dataobj = jQuery("#jqGrid").jqGrid('getGridParam', 'data');
                            index = dataobj.findIndex(x => x.rowid == row.rowid);
                            if (row.rowid > 0 && row.Actionstatus != 1) {
                                row.Actionstatus = 2;
                                grid.jqGrid('getGridParam', 'data')[index] = row;
                            }
                        }
                    }
                },
                deleteLink: function (cellValue, options, rowdata, action) {
                    return "<a href='javascript:deleteRecord(" + options.rowId + ")' class='glyphicon glyphicon-trash' style='color:red'></a>";
                }
            }
            obj.Init();

        });

        function deleteRecord(id) {
            if (canEdit) {
                var grid = $('#jqGrid');
                var result = confirm("Are you sure you Want to delete?");

                if (result == true) {
                    var ediId = $('.inline-edit-cell').parent().parent().prop('id');
                    grid.jqGrid('saveRow', ediId);
                    var rowData = grid.jqGrid('getRowData', ediId);
                    if (rowData.rowid > 0 && rowData.Actionstatus != 1) {
                        rowData.Actionstatus = 2;
                        grid.jqGrid('getGridParam', 'data')[ediId - 1] = rowData;
                    }

                    var row = grid.jqGrid('getRowData', id);
                    grid.jqGrid('delRowData', id);
                    if (row.Actionstatus != 1) {
                        DeleteRows.push(row);
                    }
                    var data = jQuery("#jqGrid").jqGrid('getGridParam', 'data');
                    for (var i = 0; i < data.length; i++) {
                        data[i].rowid = i + 1;
                    }
                    var curpage = parseInt($(".ui-pg-input").val());
                    jQuery('#jqGrid').jqGrid('clearGridData');
                    jQuery('#jqGrid').jqGrid('setGridParam', { data: data });
                    $("#jqGrid").trigger("reloadGrid", [{ page: curpage }]);
                    $("#jqGrid").trigger("reloadGrid", [{ page: curpage }]);
                    lastSelection = id;
                }
            }            
        }
        function unique(list) {
            var result = [];
            $.each(list, function (i, e) {
                if ($.inArray(e, result) == -1) result.push(e);
            });
            return result;
        }
    </script>
</asp:Content>
