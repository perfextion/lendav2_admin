﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Services;

public partial class admin_ReferenceMasterFarmerNew : BasePage
{
    public admin_ReferenceMasterFarmerNew()
    {
        Table_Name = "Master_Farmer";
    }

    [WebMethod]
    public static string GetTableInfo()
    {
        return Get_Table_Info();
    }

    [WebMethod]
    public static string GetListItems()
    {
        string sql_qry = @"select List_Item_Value, List_Item_Name, List_Group_Code from ref_list_item
                            where list_group_code in ( 'BORROWER_ENTITY_TYPE', 'PREF_CONTACT')
                            UNION
                            Select State_Abbrev as List_Item_Value, State_Abbrev as List_Item_Name, 'STATE' as List_Group_Code From Ref_State";

        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static string GetMasterFarmerData()
    {
        string sql_qry = @"SELECT *, 0 as [Actionstatus] from [Master_Farmer]";

        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static void SaveAssocType(dynamic ListItems)
    {
        string qry = string.Empty;

        foreach (var item in ListItems)
        {
           
            int val = Convert.ToInt32(item["Actionstatus"]);

            if (val == 1)
            {
                qry += @"INSERT INTO [dbo].[Master_Farmer]
                                   ([Farmer_ID_Type]
                                   ,[Farmer_SSN_Hash]
                                   ,[Farmer_Entity_Type]
                                   ,[Farmer_Entity_Notes]
                                   ,[Farmer_Last_Name]
                                   ,[Farmer_First_Name]
                                   ,[Farmer_MI]
                                   ,[Farmer_Address]
                                   ,[Farmer_City]
                                   ,[Farmer_State_ID]
                                   ,[Farmer_Zip]
                                   ,[Farmer_Phone]
                                   ,[Farmer_Email]
                                   ,[Farmer_Pref_Contact_Ind]
                                   ,[Farmer_DL_State]
                                   ,[Farmer_DL_Num]
                                   ,[Farmer_DOB]
                                   ,[Year_Begin_farming]
                                   ,[Year_Begin_Client]
                                   ,[Status])
                             VALUES
                               (
                                    '" + item["Farmer_ID_Type"] + @"'" +
                                   ",'" + item["Farmer_SSN_Hash"] + @"'" +
                                   ",'" + item["Farmer_Entity_Type"] + @"'" +
                                   ",'" + item["Farmer_Entity_Notes"].Replace("'", "''") + @"'" +
                                   ",'" + item["Farmer_Last_Name"].Replace("'", "''") + @"'" +
                                   ",'" + item["Farmer_First_Name"].Replace("'", "''") + @"'" +
                                   ",'" + item["Farmer_MI"].Replace("'", "''") + @"'" +
                                   ",'" + item["Farmer_Address"].Replace("'", "''") + @"'" +
                                   ",'" + item["Farmer_City"].Replace("'", "''") + @"'" +
                                   ",'" + item["Farmer_State_ID"] + @"'" +
                                   ",'" + item["Farmer_Zip"] + @"'" +
                                   ",'" + item["Farmer_Phone"] + @"'" +
                                   ",'" + item["Farmer_Email"].Replace("'", "''") + @"'" +
                                   ",'" + item["Farmer_Pref_Contact_Ind"] + @"'" +
                                   ",'" + item["Farmer_DL_State"] + @"'" +
                                   ",'" + item["Farmer_DL_Num"] + @"'" +
                                   ",'" + item["Farmer_DOB"] + @"'" +
                                   ",'" + item["Year_Begin_farming"] + @"'" +
                                   ",'" + item["Year_Begin_Client"] + @"'" +
                                   ",'" + item["Status"] + @"'
                                )  ";

                qry += Environment.NewLine;
            }
            else if (val == 2)
            {
                qry += @"UPDATE [dbo].[Master_Farmer]
                           SET  [Farmer_ID_Type] = '" + item["Farmer_ID_Type"] + @"' " +
                              ",[Farmer_SSN_Hash] = '" + item["Farmer_SSN_Hash"].Replace("'", "''") + @"' " +
                              ",[Farmer_Entity_Type] = '" + item["Farmer_Entity_Type"] + @"' " +
                              ",[Farmer_Entity_Notes] = '" + item["Farmer_Entity_Notes"].Replace("'", "''") + @"' " +
                              ",[Farmer_Last_Name] = '" + item["Farmer_Last_Name"].Replace("'", "''") + @"' " +
                              ",[Farmer_First_Name] = '" + item["Farmer_First_Name"].Replace("'", "''") + @"' " +
                              ",[Farmer_MI] = '" + item["Farmer_MI"].Replace("'", "''") + @"' " +
                              ",[Farmer_Address] = '" + item["Farmer_Address"].Replace("'", "''") + @"' " +
                              ",[Farmer_City] = '" + item["Farmer_City"].Replace("'", "''") + @"' " +
                              ",[Farmer_State_ID] = '" + item["Farmer_State_ID"] + @"' " +
                              ",[Farmer_Zip] = '" + item["Farmer_Zip"] + @"' " +
                              ",[Farmer_Phone] = '" + item["Farmer_Phone"] + @"' " +
                              ",[Farmer_Email] = '" + item["Farmer_Email"].Replace("'", "''") + @"' " +
                              ",[Farmer_Pref_Contact_Ind] = '" + item["Farmer_Pref_Contact_Ind"] + @"' " +
                              ",[Farmer_DL_State] = '" + item["Farmer_DL_State"] + @"' " +
                              ",[Farmer_DL_Num] = '" + item["Farmer_DL_Num"] + @"' " +
                              ",[Farmer_DOB] = '" + item["Farmer_DOB"] + @"' " +
                              ",[Year_Begin_farming] = '" + item["Year_Begin_farming"] + @"' " +
                              ",[Year_Begin_Client] = '" + item["Year_Begin_Client"] + @"' " +
                              ",[Status] = '" + item["Status"] + @"' " +
                         "WHERE Farmer_ID = '" + item["Farmer_ID"] + "' ";

                qry += Environment.NewLine;
            }
            else if (val == 3)
            {
                qry += "DELETE FROM [Master_Farmer]  where Farmer_ID = '" + item["Farmer_ID"] + "' ";
                qry += Environment.NewLine;
            }
        }

        if(!string.IsNullOrEmpty(qry))
        {
            gen_db_utils.gp_sql_execute(qry, dbKey);
            gen_db_utils.Update_Table_Audit_Trail("Master_Farmer", userid, dbKey);
        }
    }
}