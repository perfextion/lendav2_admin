﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_ReferenceOffice : BasePage
{
    public admin_ReferenceOffice()
    {
        Table_Name = "Ref_Office";
    }

    [WebMethod]
    public static string GetTableInfo()
    {
        return Get_Table_Info();
    }

    [WebMethod]
    public static string BindReferenceOffice()
    {
        string sql_qry = "select[Office_ID],[Office_Abbrev],[Office_Name],[Address],[City],[State],[Zip],[Phone],[Region_ID],[Lat],[Long],[Status] from[dbo].[Ref_Office]";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
    }

    [WebMethod, ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static void SaveReferenceOffice(dynamic lst)
    {
        //Dictionary<string, string> dic = lst;

        foreach (var item in lst)
        {
            string qry = "";
            int val = Convert.ToInt32(item["Actionstatus"]);
            if (val == 1)
            {

                qry = "INSERT INTO Ref_Office ([Office_Abbrev],[Office_Name],[Address] ,[City],[State],[Zip],[Phone],[Region_ID],[Lat],[Long],[Status]) " +
                    " VALUES('" + ManageQuotes(item["Office_Abbrev"]) + "','" + ManageQuotes(item["Office_Name"]) + "','" + ManageQuotes(item["Address"]) + "','" + ManageQuotes(item["City"]) + "','" + ManageQuotes(item["State"]) + "','" + ManageQuotes(item["Zip"]) + "','" + ManageQuotes(item["Phone"]) + "' ,'" + ManageQuotes(item["Region_ID"]) + "','" + ManageQuotes(item["Lat"]) + "','" + ManageQuotes(item["Long"]) + "','" + ManageQuotes(item["Status"]) + "')";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 2)
            {
                qry = "UPDATE Ref_Office SET [Office_Abbrev] = '" + ManageQuotes(item["Office_Abbrev"]) + "' ,[Office_Name] = '" + ManageQuotes(item["Office_Name"]) + "'," +
                    "[Address] = '" + ManageQuotes(item["Address"]) + "' ,[City] = '" + ManageQuotes(item["City"]) + "' ,[State] = '" + ManageQuotes(item["State"]) + "' ,[Zip] = '" +
                    ManageQuotes(item["Zip"]) + "' ,Phone = '" + (item["Phone"]) + "', [Region_ID] = '" + ManageQuotes(item["Region_ID"]) + "' ,[Lat] = '" + (item["Lat"]) + "'," +
                    "[Long] = '" + (item["Long"]) + "' ,[Status] = '" + ManageQuotes(item["Status"]) + "' where Office_ID=" + item["Office_ID"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 3)
            {
                qry = "DELETE FROM Ref_Office  where Office_ID=" + item["Office_ID"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
        }

        gen_db_utils.Update_Table_Audit_Trail("Ref_Office", userid, dbKey);
     }

    public static string ManageQuotes(string value)
    {
        if (!String.IsNullOrEmpty(value) && value.Contains("'"))
        {
            value = value.Replace("'", "''");
        }
        return value;
    }
}