﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_ReferencePendingAction : BasePage
{
    public admin_ReferencePendingAction()
    {
        Table_Name = "Ref_Pending_Action";
    }

    [WebMethod]
    public static string GetTableInfo()
    {
        return Get_Table_Info();
    }

    [WebMethod]
    public static string GetRefPendingAction()
    {
        //Ref_PA_ID,PA_Code,PA_Name,PA_Level,PA_Level_Name,Sort_Order,Icon_Code,Icon_Color_Code,Status,SaveRefPendingAction,GetRefPendingAction
        string sql_qry = "SELECT  0 as Actionstatus,Ref_PA_ID,PA_Code,PA_Name,PA_Level,PA_Group,PA_Level_Name,Sort_Order,Icon_Code,Icon_Color_Code,Status FROM Ref_Pending_Action";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
    }
    [WebMethod]
    public static void SaveRefPendingAction(dynamic lst)
    {
        foreach (var item in lst)
        {
            string qry = "";
            int val = Convert.ToInt32(item["Actionstatus"]);
            if (val == 1)
            {
                qry = "INSERT INTO Ref_Pending_Action (PA_Code,PA_Name,PA_Level,PA_Level_Name,PA_Group,Sort_Order,Icon_Code,Icon_Color_Code,Status)" +
                    " VALUES('" + item["PA_Code"].Replace("'", "''") + "','" + item["PA_Name"].Replace("'", "''") + "'," +
                    "'" + item["PA_Level"].Replace("'", "''") + "','" + item["PA_Level_Name"].Replace("'", "''") + "'," + item["PA_Group"].Replace("'", "''") + "'," +
                    "'" + item["Sort_Order"].Replace("'", "''") + "','" + item["Icon_Code"].Replace("'", "''") + "'," +
                    "'" + item["Icon_Color_Code"].Replace("'", "''") + "','" + item["Status"].Replace("'", "''") + "')";
                  gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 2)
            {
                qry = "UPDATE Ref_Pending_Action SET PA_Code = '" + item["PA_Code"].Replace("'", "''") + "' ," +
                    "PA_Name = '" + item["PA_Name"].Replace("'", "''") + "',PA_Level = '" + item["PA_Level"].Replace("'", "''") + "'," + "PA_Group = '" + item["PA_Group"].Replace("'", "''") + "'," +
                    "PA_Level_Name = '" + item["PA_Level_Name"].Replace("'", "''") + "',Sort_Order = '" + item["Sort_Order"].Replace("'", "''") + "'," +
                    "Icon_Code = '" + item["Icon_Code"].Replace("'", "''") + "',Icon_Color_Code = '" + item["Icon_Color_Code"].Replace("'", "''") + "'," +
                    "Status = '" + item["Status"].Replace("'", "''") + "' where Ref_PA_ID=" + item["Ref_PA_ID"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            } 
            else if (val == 3)
            {
                qry = "DELETE FROM Ref_Pending_Action  where Ref_PA_ID=" + item["Ref_PA_ID"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
        }

        gen_db_utils.Update_Table_Audit_Trail("Ref_Pending_Action", userid, dbKey);
    }
}