﻿<%@ Page Title="Risk Question" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="ReferenceQuestions.aspx.cs" Inherits="ReferenceQuestions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>

        .fc-head-container thead tr, .table thead tr {
            height: 50px;
        }

        #content {
            padding: 14px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="risk-questions reference-table">
        <div class="row switch-controls">
            <div class="switch-control disabled" id="editControl">
                <label for="editBtn">Edit</label>
                <div class="toggle-btn small">            
                    <input type="checkbox" id="editBtn" class="cb-value" />
                    <span class="round-btn"></span>
                </div>
            </div>
            <div class="switch-control disabled" id="enableControl">
                <label for="enableBtn">Enable</label>
                <div class="toggle-btn small">            
                    <input type="checkbox" id="enableBtn" class="cb-value" />
                    <span class="round-btn"></span>
                </div>
            </div>
        </div>
        <div class="loader"></div>
        <div class="row main-content">
            <div>
                <table id="jqGrid"></table>
                <div id="jqGridPager"></div>
            </div>
        </div>
    </div>
    <br />
    <br />
    <br />
    <br />
    <br />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" runat="Server">
    <script type="text/javascript">  
        var DeleteRows = [];
        var timer;
        var lastSelection;
        var canEdit = false;

        $(function () {
            var obj = {
                Init: function () {
                    $('.loader').show();

                    obj.bindGrid();

                    $("#navbtnadd").click(function () {
                        obj.add();
                    });

                    $("#navbtnsave").click(function () {
                         $('.loader').show();
                        obj.save();
                    });

                    $("#navbtnrefresh").click(function () {
                         $('.loader').show();
                        obj.bindGrid();
                    });

                    $("#txtSearchBar").on("keyup", function () {
                        var self = this;
                        obj.search(self);
                    });
                    $('#navbtncolumns').click(function () {
                        $("#jqGrid").columnChooser({});
                    });
                    $("#navbtndownload").click(function () {
                        $("#jqGrid").jqGrid("exportToExcel", {
                            includeLabels: true,
                            includeGroupHeader: true,
                            includeFooter: true,
                            fileName: "Risk Questions.xlsx",
                            maxlength: 40, // maxlength for visible string data
                            replaceStr: function _replStrFunc(v) {
                                return v.replace('&#160;', '');
                            }
                        })
                    });

                    $('#editBtn').change(function () {
                        canEdit = $(this).is(':checked');
                        if (!canEdit) {
                            if (lastSelection != null) {
                                var grid = $("#jqGrid");
                                grid.jqGrid('saveRow', lastSelection);
                                grid.jqGrid('restoreRow', lastSelection);
                                lastSelection = null;
                            }
                        }
                    });

                    obj.getexception();
                    obj.checkexception();
                    obj.buttonsave();
                    obj.refview();
                },
                getTableInfo: function () {
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "ReferenceQuestions.aspx/GetTableInfo",
                        data: JSON.stringify({ }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            var data = JSON.parse(res.d);

                            var table_info = data.Table_Info[0];
                            $('#txtTableName').html(table_info.Table_Name);
                            $('#txtLastUpdated').html(table_info.Last_Updated);
                            $('#txtRecordsCount').html(table_info.Records_Count);

                            if (data.CanEdit) {
                                $('#editControl').removeClass('disabled');
                            } else {
                                $('#editControl').addClass('disabled');
                            }

                            $('#enableControl').addClass('disabled');
                            
                        }
                    });
                },
                bindGrid: function () {
                    $.ajax({
                        type: "POST",
                        url: "ReferenceQuestions.aspx/BindReferenceQuestions",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var resData = JSON.parse(data.d);
                            $('.loader').show();
                            for (var i = 0; i < resData.length; i++) {
                                resData[i].rowid = i + 1;
                            }
                            obj.loadgrid(resData);
                            obj.getTableInfo();
                            $('.loader').hide();

                        }
                    });
                },
                loadgrid: function (data) {
                    $.jgrid.gridUnload("#jqGrid");
                    $("#jqGrid").jqGrid({
                        datatype: "local",
                        styleUI: 'Bootstrap',
                        data: data,
                        colModel: [
                            { label: 'rowid', name: 'rowid', width: 75, editable: false, key: true, hidden: true, hidedlg: true  },
                            { label: 'Question ID', name: 'Question_ID', align: 'center', width: 90, editable: false, sorttype: 'integer' },
                            { label: 'Question ID Text', name: 'Question_ID_Text', width: 300, editable: true, edittype: 'textarea' },
                            { label: 'Chevron ID', name: 'Chevron_ID', align: 'center', width: 90, editable: true, sorttype: 'integer' },
                            { label: 'Cat Code', name: 'Questions_Cat_Code', align: 'center', width: 90, editable: true },
                            { label: 'Response Req Ind', name: 'Response_Detail_Reqd_Ind', align: 'center', width: 90, editable: true },
                            { label: 'Subsidiary Question ID', name: 'Subsidiary_Question_ID_Ind', align: 'center', width: 90, editable: true },
                            //{ label: 'Subsidiary_Question_ID', name: 'Subsidiary_Question_ID', width: 150, editable: true },
                            { label: 'Parent Question ID', name: 'Parent_Question_ID', align: 'center', width: 90, editable: true, sorttype: 'integer' },
                            { label: 'Neg', name: 'Choice1', width: 90, align: 'center', editable: true },
                            { label: 'Pos', name: 'Choice2', width: 90, align: 'center', editable: true },
                            {
                                label: 'Validation ID', name: 'Validation_ID', align: 'center', width: 90, editable: true, editoptions: {
                                    maxlength: 4,
                                    dataEvents: [
                                        {
                                            type: 'keypress',
                                            fn: function (e) {
                                                return NumberOnly(e, this, false);
                                            }
                                        }
                                    ]
                                },
                                sorttype: 'integer'
                            },
                            {
                                label: 'Exception ID', name: 'Exception_ID', align: 'center', width: 90, editable: true,
                                editoptions: {
                                    maxlength: 4,
                                    dataEvents: [
                                        {
                                            type: 'keypress',
                                            fn: function (e) {
                                                return NumberOnly(e, this, false);
                                            }
                                        }
                                    ]
                                },
                                sorttype: 'integer'
                            },
                            { label: 'Exception ID Text', name: 'Exception_ID_Text', align: 'left', width: 300, editable: false },
                            {
                                label: 'Sort Order', name: 'Sort_Order', sorttype: 'number', align: 'center', width: 60, editable: true,
                                editoptions: {
                                    maxlength: 3,
                                    dataEvents: [
                                        {
                                            type: 'keypress',
                                            fn: function (e) {
                                                return NumberOnly(e, this, false);
                                            }
                                        }
                                    ]
                                }
                            },
                            {
                                label: 'Status',
                                name: 'Status',
                                align: "center",
                                width: 90,
                                editable: true,
                                hidden: false,
                                editoptions: {
                                    maxlength: 1,
                                    dataEvents: [
                                        {
                                            type: 'keypress',
                                            fn: function (e) {
                                                return NumberOnly(e, this, false);
                                            }
                                        }
                                    ]
                                }
                            },
                            { label: 'Actionstatus', name: 'Actionstatus', align: 'center', width: 60, editable: false, hidden: true, hidedlg: true },
                            { label: '', name: '', width: 60, align: 'center', formatter: obj.deleteLink, hidedlg: true, exportcol: false  },
                        ],
                        viewrecords: true,
                        loadonce: true,
                        restoreAfterSelect: false,
                        saveAfterSelect: true,
                        sortable: true,
                        onSelectRow: obj.edit,
                        onPaging: obj.changePage,
                        height: 'auto',
                        rowNum: 100,
                        pager: "#jqGridPager"
                    });
                },
                add: function () {

                    var grid = $("#jqGrid");

                    var rowsperPage = parseInt(grid.getGridParam('rowNum'));
                    var gridlength = grid.jqGrid('getGridParam', 'data').length;
                    var curpage = parseInt($(".ui-pg-input").val());
                    var totPages = Math.ceil(gridlength / rowsperPage);

                    //if (rowsperPage * curpage == gridlength) {
                    if (rowsperPage * totPages == gridlength) {
                        var id = $('.inline-edit-cell').parent().parent().prop('id');
                        grid.jqGrid('saveRow', id);
                        var row = obj.newrow();
                        // var newRowId = grid.jqGrid('getGridParam', 'data').length + 1;
                        var newRowId = row.rowid;
                        grid.jqGrid('addRowData', newRowId, row, "first");
                        grid.trigger('reloadGrid');
                        lastSelection = newRowId;
                        grid.jqGrid('saveRow', lastSelection);
                        grid.jqGrid('restoreRow', lastSelection);
                        $('.glyphicon-step-forward').trigger('click');

                    } else {

                        $('.glyphicon-step-forward').trigger('click');
                        var row = obj.newrow();
                        // var newRowId = grid.jqGrid('getGridParam', 'data').length + 1;
                        var newRowId = row.rowid;
                        grid.jqGrid('addRowData', newRowId, row, "first");
                    }


                    lastSelection = newRowId;
                    grid.jqGrid('saveRow', lastSelection);
                    grid.jqGrid('restoreRow', lastSelection);

                    var eid = $('.inline-edit-cell').parent().parent().prop('id')
                    grid.jqGrid('saveRow', eid);
                    grid.jqGrid('restoreRow', eid);
                    grid.jqGrid('editRow', newRowId);
                },
                edit: function (id) { 

                    if (id && id !== lastSelection && canEdit) {
                        var grid = $("#jqGrid");

                        grid.jqGrid('saveRow', lastSelection);
                        grid.jqGrid('restoreRow', lastSelection);
                        var row = grid.jqGrid('getRowData', lastSelection);

                        if (!jQuery.isEmptyObject(row)) {
                            var dataobj = jQuery("#jqGrid").jqGrid('getGridParam', 'data');
                            index = dataobj.findIndex(x => x.rowid == row.rowid);
                            if (row.rowid > 0 && row.Actionstatus != 1) {
                                row.Actionstatus = 2;
                                grid.jqGrid('getGridParam', 'data')[index] = row;
                            }
                        }

                        grid.jqGrid('editRow', id);
                        lastSelection = id;
                    }
                },
                newrow: function () {
                    var newid = 0;

                    var grid = $("#jqGrid");
                    var length = jQuery("#jqGrid").jqGrid('getGridParam', 'records');
                    var dataobj = grid.jqGrid('getGridParam', 'data');
                    var dataobj = grid.jqGrid('getGridParam', 'data');
                    var Question_ID = [];
                    Question_IDs = dataobj.map(function (e) { return e.rowid });
                    if (Question_IDs.length > 0) {
                        newid = Question_IDs.reduce(function (a, b) { return Math.max(a, b); });
                    }

                    var row = {
                        Question_ID: 0,
                        Actionstatus: 1,
                        rowid: newid + 1,
                        Question_ID_Text: "",
                        Questions_Cat_Code: "",
                        Response_Detail_Reqd_Ind: "",
                        Sort_Order: "",
                        Status: "",
                        Subsidiary_Question_ID: "",
                        Subsidiary_Question_ID_Ind: "",
                        Parent_Question_ID: "",
                        Choice1: "",
                        Choice2: "",
                        Chevron_ID: ""

                    };
                    return row;
                },
                save: function () {

                    $('.loader').show();
                    var grid = $("#jqGrid");
                    var allrows = [];


                    var id = $('.inline-edit-cell').parent().parent().prop('id');
                    grid.jqGrid('saveRow', id);

                    var rowData = grid.jqGrid('getRowData', id);
                    if (rowData.rowid > 0) {
                        rowData.Actionstatus = 2;
                        grid.jqGrid('getGridParam', 'data')[id] = rowData;
                    }

                    for (var i = 0; i < DeleteRows.length; i++) {
                        DeleteRows[i].Actionstatus = 3
                        allrows.push(DeleteRows[i]);
                    }
                    var dataobj = grid.jqGrid('getGridParam', 'data');
                    var newRows = $.grep(dataobj, function (e) {
                        if (e != undefined) {
                            return e.Actionstatus == 1 || e.Actionstatus == 2
                        }
                    });
                    for (var i = 0; i < newRows.length; i++) {
                        allrows.push(newRows[i]);
                    }

                    $.ajax({
                        type: "POST",
                        url: "ReferenceQuestions.aspx/SaveReferenceQuestions",
                        data: JSON.stringify({ lst: allrows }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            location.reload();
                        },
                        failure: function (response) {
                            console.log(response.d);
                        }

                    });
                },
                search: function (self) {
                    if (timer) { clearTimeout(timer); }
                    timer = setTimeout(function () {
                        obj.changePage();
                        $("#jqGrid").jqGrid('filterInput', self.value);
                        var length = jQuery("#jqGrid").jqGrid('getGridParam', 'records');
                        $('#txtRecordsCount').html('Records Count: ' + length);
                    }, 0);
                },
                changePage: function () {

                    var grid = $("#jqGrid");
                    var id = $('.inline-edit-cell').parent().parent().prop('id');
                    if (id != undefined) {
                        grid.jqGrid('saveRow', id);
                        grid.jqGrid('restoreRow', id);
                        var row = grid.jqGrid('getRowData', id);
                        if (!jQuery.isEmptyObject(row)) {
                            var dataobj = jQuery("#jqGrid").jqGrid('getGridParam', 'data');
                            index = dataobj.findIndex(x => x.rowid == row.rowid);
                            if (row.rowid > 0 && row.Actionstatus != 1) {
                                row.Actionstatus = 2;
                                grid.jqGrid('getGridParam', 'data')[index] = row;
                            }
                        }
                    }
                },
                deleteLink: function (cellValue, options, rowdata, action) {
                    return "<a href='javascript:deleteRecord(" + options.rowId + ")' class='glyphicon glyphicon-trash' style='color:red'></a>";
                },
                manageLink: function (cellValue, options, rowdata, action) {
                    return "<a href='javascript:manageRecord(" + options.rowId + ")' >Manage</a>";
                },
                checkexception: function () {
                    $('#chkIsException').click(function () {

                        if (this.checked) {
                            $('#divModelSave').show();
                        } else {
                            $('#divModelSave').hide();
                        }
                    });
                },
                buttonsave: function () {
                    $('#btnModalSave').click(function () {

                        var qId = $('#spnQuestionID').text();
                        var exceptionId = $('#ddlExceptions').val();

                        $('#spnException').hide();
                        $.ajax({
                            type: "POST",
                            url: "ReferenceQuestions.aspx/UpdateExceptionbyID",
                            data: JSON.stringify({ exceptionID: exceptionId, QuestionId: qId }),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (data) {
                                $("#jqGrid").jqGrid("setCell", lastSelection, "Exception_ID", exceptionId);
                                $('#myModal').modal('hide');
                            },
                            failure: function (response) {
                            }
                        });
                    });
                },
                getexception: function () {
                    $.ajax({
                        type: "POST",
                        url: "ReferenceQuestions.aspx/GetAllExecptions",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            $.each(res.d, function (data, value) {
                                $("#ddlExceptions").append($("<option></option>").val(data).html(value));
                            });
                        }

                    });
                },
                refview: function () {
                    $('#divViewAllExe').click(function () {
                        window.location.href = "/admin/ReferenceQuestionsView.aspx";
                    });
                },
                Loadtable: function () {
                    $.ajax({
                        type: "POST",
                        url: "ReferenceQuestions.aspx/Loadtable",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            $.each(res.d, function (data, value) {
                                $("#ddlExceptions").append($("<option></option>").val(data).html(value));
                            });
                        }

                    });
                },

            }
            obj.Init();

        });

        function deleteRecord(id) {

            if (canEdit) {
                var grid = $('#jqGrid');
                var result = confirm("Are you sure you Want to delete?");

                if (result == true) {
                    var ediId = $('.inline-edit-cell').parent().parent().prop('id');
                    grid.jqGrid('saveRow', ediId);
                    var rowData = grid.jqGrid('getRowData', ediId);
                    if (rowData.rowid > 0 && rowData.Actionstatus != 1) {
                        rowData.Actionstatus = 2;
                        grid.jqGrid('getGridParam', 'data')[ediId - 1] = rowData;
                    }

                    var row = grid.jqGrid('getRowData', id);
                    grid.jqGrid('delRowData', id);
                    if (row.Actionstatus != 1) {
                        DeleteRows.push(row);
                    }
                    var data = jQuery("#jqGrid").jqGrid('getGridParam', 'data');
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].Question_ID == 0) {
                            data[i].rowid = i + 1;
                            data[i].id = i + 1;
                        } else
                            data[i].rowid = i + 1;
                    }
                    var curpage = parseInt($(".ui-pg-input").val());


                    jQuery('#jqGrid').jqGrid('clearGridData');
                    jQuery('#jqGrid').jqGrid('setGridParam', { data: data });
                    $("#jqGrid").trigger("reloadGrid", [{ page: curpage }]);
                    $("#jqGrid").trigger("reloadGrid", [{ page: curpage }]);
                    lastSelection = id;
                }
            }

        }
        function manageRecord(id, questionID) {

            lastSelection = id;
            var grid = $("#jqGrid");
            //var rowData = grid.jqGrid('getRowData', id);
            // var rowData = grid.jqGrid('getGridParam', 'data')[id - 1];

            var dataobj = jQuery("#jqGrid").jqGrid('getGridParam', 'data');
            index = dataobj.findIndex(x => x.rowid == id);

            var rowData = dataobj[index];

            $('#spnQuestion').text(rowData.Question_ID_Text);
            $('#spnQuestionID').text(rowData.Question_ID);

            $('#spnException').hide();
            $('#spnQuestionID').hide();

            if (rowData.Exception_ID > 0) {
                $('#ddlExceptions').val(rowData.Exception_ID);
                $('#divModelSave').show();
                $('#chkIsException').prop('checked', true);
            } else {
                $('#chkIsException').prop('checked', false);
                $('#divModelSave').hide();
                $('#ddlExceptions').val('');
            }
            $('#myModal').modal('show');
        }
        function unique(list) {
            var result = [];
            $.each(list, function (i, e) {
                if ($.inArray(e, result) == -1) result.push(e);
            });
            return result;
        }
    </script>
</asp:Content>

