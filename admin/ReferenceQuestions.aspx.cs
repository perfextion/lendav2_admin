﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;

public partial class ReferenceQuestions : BasePage
{
    public ReferenceQuestions()
    {
        Table_Name = "Ref_Questions";
    }

    [WebMethod]
    public static string GetTableInfo()
    {
        return Get_Table_Info();
    }

    [WebMethod]
    public static string BindReferenceQuestions()
    {
        string sql_qry = @"SELECT 0 as Actionstatus, 
                                [Question_ID], 
                                [Question_ID_Text],
                                RQ.[Sort_Order],
                                [Questions_Cat_Code],
                                [Response_Detail_Reqd_Ind],
                                [Subsidiary_Question_ID_Ind],
                                RQ.[Status],
                                RQ.[Chevron_ID],
                                [Parent_Question_ID] ,
                                [Choice1],
                                [Choice2],
                                [Validation_ID],
                                RE.Exception_ID, 
                                RE.Exception_ID_Text 
                            FROM [dbo].[Ref_Questions] RQ
                            LEFT JOIN Ref_Exception RE on RQ.Exception_ID = RE.Exception_ID";

        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return JsonConvert.SerializeObject(dt);
    }    

    [WebMethod, ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static void SaveReferenceQuestions(dynamic lst)
    {
        foreach (var item in lst)
        {
            string qry = "";
            int val = Convert.ToInt32(item["Actionstatus"]);

            var Validation_Exception_Ind = 0;

            if(!string.IsNullOrEmpty(item["Validation_ID"]) || !string.IsNullOrEmpty(item["Exception_ID"]))
            {
                Validation_Exception_Ind = 1;
            }

            if (val == 1)
            {
                qry = "INSERT INTO [Ref_Questions]" +
                        "(" +
                            "[Question_ID_Text], " +
                            "[Exception_ID], " +
                            "[Validation_ID], " +
                            "[Validation_Exception_Ind], " +
                            "[Sort_Order]," +
                            "[Questions_Cat_Code]," +
                            "[Response_Detail_Reqd_Ind]," +
                            "[Subsidiary_Question_ID_Ind]," +
                            "[Parent_Question_ID]," +
                            "[Status]," +
                            "[Chevron_ID]," +
                            "[Choice1]," +
                            "[Choice2]" +
                        ")" +
                    " VALUES(" +
                            "'" + ManageQuotes(item["Question_ID_Text"]) + "'," +
                            "'" + ManageQuotes(item["Exception_ID"]) + "'," +
                            "'" + ManageQuotes(item["Validation_ID"]) + "'," +
                            "'" + Validation_Exception_Ind + "'," +
                            "'" + ManageQuotes(item["Sort_Order"]) + "'," +
                            "'" + ManageQuotes(item["Questions_Cat_Code"]) + "'," +
                            "'" + ManageQuotes(item["Response_Detail_Reqd_Ind"]) + "'," +
                            "'" + ManageQuotes(item["Subsidiary_Question_ID_Ind"]) + "'," +
                            "'" + ManageQuotes(item["Parent_Question_ID"]) + "'," +
                            "'" + ManageQuotes(item["Status"]) + "'," +
                            "'" + ManageQuotes(item["Chevron_ID"]) + "'," +
                            "'" + ManageQuotes(item["Choice1"]) + "'," +
                            "'" + ManageQuotes(item["Choice2"]) + "'" +
                        ")";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 2)
            {
                qry = "UPDATE Ref_Questions SET " +
                        "[Question_ID_Text] = '" + ManageQuotes(item["Question_ID_Text"]) + "'," +
                        "[Validation_ID] = '" + ManageQuotes(item["Validation_ID"]) + "' ," +
                        "[Exception_ID] = '" + ManageQuotes(item["Exception_ID"]) + "' ," +
                        "[Validation_Exception_Ind] = '" + Validation_Exception_Ind + "' ," +
                        "[Sort_Order] = '" + ManageQuotes(item["Sort_Order"]) + "'," +
                        "[Questions_Cat_Code] = '" + ManageQuotes(item["Questions_Cat_Code"]) + "'," +
                        "[Response_Detail_Reqd_Ind] = '" + ManageQuotes(item["Response_Detail_Reqd_Ind"]) + "' ," +
                        "[Subsidiary_Question_ID_Ind] = '" + ManageQuotes(item["Subsidiary_Question_ID_Ind"]) + "' ," +
                        "[Parent_Question_ID] = '" + ManageQuotes(item["Parent_Question_ID"]) + "' ," +
                        "[Status] = '" + ManageQuotes(item["Status"]) + "' ," +
                        "[Chevron_ID] = '" + ManageQuotes(item["Chevron_ID"]) + "'," +
                        "[Choice1] = '" + ManageQuotes(item["Choice1"]) + "'," +
                        "[Choice2] = '" + ManageQuotes(item["Choice2"]) + "' " +
                    "where Question_ID=" + item["Question_ID"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 3)
            {
                qry = "DELETE FROM Ref_Questions  where Question_ID = '" + item["Question_ID"] + "'  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
        }

        gen_db_utils.Update_Table_Audit_Trail("Ref_Questions", userid, dbKey);
    }
    [WebMethod]
    public static void UpdateReferenceQuestions(int id)
    {
        string sql_qry = "UPDATE Ref_Questions SET [Question_ID_Text] = '' ,[Sort_Order] = '',[Questions_Cat_Code] = '' ,[Response_Detail_Reqd_Ind] = '' ,[Subsidiary_Question_ID_Ind] = '' ,[Subsidiary_Question_ID] = '' ,[Status] = '' where Question_ID=" + id + "  ";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);

        gen_db_utils.gp_sql_execute(sql_qry, dbKey);
        gen_db_utils.Update_Table_Audit_Trail("Ref_Questions", userid, dbKey);
    }
    [WebMethod]
    public static void DeleteReferenceQuestions(int id)
    {
        string sql_qry = "DELETE FROM Ref_Questions where Question_ID=" + id + "  ";
        gen_db_utils.gp_sql_execute(sql_qry, dbKey);
        gen_db_utils.Update_Table_Audit_Trail("Ref_Questions", userid, dbKey);
    }

    [WebMethod]
    public static Dictionary<string, string> GetAllExecptions()
    {
        string sql_qry = "select Exception_ID, CONCAT( Exception_ID,'.',Exception_ID_Text) as Exception_ID_Text from Ref_Exception order by sort_order ";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);

        return dt.AsEnumerable().ToDictionary<DataRow, string, string>(row => row[0].ToString(),
                                       row => row[1].ToString());

    }

    [WebMethod]
    public static void UpdateExceptionbyID(string exceptionID, string QuestionId)
    {
        string qry = "";
        if (exceptionID == " ")
            qry = "UPDATE Ref_Questions set Exception_ID=Null where Question_ID=" + QuestionId + "";
        else
            qry = "UPDATE Ref_Questions set Exception_ID='" + exceptionID + "' where Question_ID=" + QuestionId + "";

        gen_db_utils.gp_sql_execute(qry, dbKey);
        gen_db_utils.Update_Table_Audit_Trail("Ref_Questions", userid, dbKey);
    }



    public static string ManageQuotes(string value)
    {
        if (value.Contains("'"))
        {
            value = value.Replace("'", "''");
        }
        return value;
    }

}