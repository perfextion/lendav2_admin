﻿<%@ Page Title="" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="ReferenceQuestionsView.aspx.cs" Inherits="admin_ReferenceQuestionsView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

    <div class="panel-body row-padding">
        <div class="row">
            <div class="col-md-8 col-sm-8" style="padding-left: 51px;">
                <div id="divBack" class="btn btn-primary">Back</div>
            </div>
            <div class="loader"></div>

            <div class="col-md-4 col-sm-4">
                <div class="form-inline">
                    <div class="form-group">
                        <label for="ddlChevron">Chevron Name :</label>
                        <select id="ddlChevron" class="form-control" style="width: 200px">
                            <option value="">Select</option>
                        </select>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col-md-11" style="padding-left: 51px;">
            <asp:Label ID="lbl_table" runat="server" Text="lbl_table"></asp:Label>
        </div>
    </div>
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Question Details</h4>
                </div>
                <div class="modal-body">
                    <div class="row" style="margin-left: 5px">
                        <div class="form-inline">
                            <div class="form-group">
                                <label for="txtSearch">Question :</label>
                                <span id="spnQuestion"></span>
                                <span id="spnQuestionID"></span>
                            </div>
                        </div>
                        <input type="checkbox" id="chkIsException" name="name" value="" />
                        Add Execption
                        <div class="form-inline" id="divModelSave">
                            <div class="form-group">
                                <label for="txtSearch">Search :</label>
                                <select id="ddlExceptions" class="form-control" style="width: 50%;">
                                    <option value="">Select</option>
                                </select>
                                <div class="btn btn-primary" id="btnModalSave">Save</div>
                            </div>
                            <span id="spnException" style="color: red">Select Exception </span>
                        </div>

                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    <script>
        $(function () {
            $.ajax({
                type: "POST",
                url: "ReferenceQuestionsView.aspx/GetddlChevrons",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (res) {
                    $('.loader').show();
                    $.each(res.d, function (data, value) {
                        $("#ddlChevron").append($("<option></option>").val(data).html(value));
                        $('.loader').hide();
                    });
                }
            });
            $('#ddlChevron').change(function () {
                $('#ContentPlaceHolder1_lbl_table').text('');
                var chevronID = $('#ddlChevron').val();
                $.ajax({
                    type: "POST",
                    url: "ReferenceQuestionsView.aspx/LoadTable",
                    data: JSON.stringify({ ChevronID: chevronID }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (res) {
                        $('.loader').show();
                        $('#ContentPlaceHolder1_lbl_table').html(res.d);
                        $('.loader').hide();
                    }
                });
            });
            $('#divBack').click(function () {
                window.location.href = "/admin/ReferenceQuestions.aspx";
            });
        });
        $(function () {
            var obj = {
                Init: function () {
                    obj.getexception();
                    obj.checkexception();
                    obj.buttonsave();
                    obj.refview();
                },
                checkexception: function () {
                    $('#chkIsException').click(function () {
                        if (this.checked) {
                            $('#divModelSave').show();
                        } else {
                            $('#divModelSave').hide();
                        }
                    });
                },
                buttonsave: function () {
                    $('#btnModalSave').click(function () {

                        var qId = $('#spnQuestionID').text();
                        var exceptionId = $('#ddlExceptions').val();

                        $('#spnException').hide();
                        $.ajax({
                            type: "POST",
                            url: "ReferenceQuestions.aspx/UpdateExceptionbyID",
                            data: JSON.stringify({ exceptionID: exceptionId, QuestionId: qId }),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (data) {
                                $('.loader').show();
                                $('#ddlChevron').trigger('change');
                                $('#myModal').modal('hide');
                                //  $('.loader').hide();
                            },
                            failure: function (response) {
                            }
                        });
                    });
                },
                getexception: function () {
                    $.ajax({
                        type: "POST",
                        url: "ReferenceQuestions.aspx/GetAllExecptions",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            $.each(res.d, function (data, value) {
                                $("#ddlExceptions").append($("<option></option>").val(data).html(value));
                            });
                        }

                    });
                },
                refview: function () {
                    $('#divViewAllExe').click(function () {
                        window.location.href = "/admin/ReferenceQuestionsView.aspx";
                    });
                }
            }
            obj.Init();
        });

        function manageRecord(id) {
            $.ajax({
                type: "POST",
                url: "ReferenceQuestionsView.aspx/GetUserDetailsByRefQuestionId",
                data: JSON.stringify({ QId: id }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (res) {
                    var objdata = JSON.parse(res.d);
                    var data = objdata[0];
                    $('#spnQuestion').text(data.Question_ID_Text);
                    $('#spnQuestionID').text(data.Question_ID);
                    $('#spnException').hide();
                    $('#spnQuestionID').hide();

                    if (data.Exception_ID > 0) {
                        $('#ddlExceptions').val(data.Exception_ID);
                        $('#divModelSave').show();
                        $('#chkIsException').prop('checked', true);
                    } else {
                        $('#chkIsException').prop('checked', false);
                        $('#divModelSave').hide();
                        $('#ddlExceptions').val('');
                    }
                }
            });

            $('#myModal').modal('show');
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" runat="Server">
</asp:Content>

