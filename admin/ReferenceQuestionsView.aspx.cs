﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_ReferenceQuestionsView : System.Web.UI.Page
{
    static string dbKey = "gp_conn";
    protected void Page_Load(object sender, EventArgs e)
    {
        lbl_table.Text = LoadTable("");
    }
    [WebMethod]
    public static string LoadTable(string ChevronID)
    {
        string strQry = "", str_return = "";        
        strQry = "select rq.Chevron_ID,Question_ID,Question_ID_Text,Exception_ID_Text,rq.Exception_ID from " +
            "Ref_Questions rq left join Ref_Exception re on rq.Exception_ID=re.Exception_ID ";

        if (ChevronID != "")
        {
            strQry += " where Chevron_ID=" + ChevronID + "";
        }
       
        DataTable dt = gen_db_utils.gp_sql_get_datatable(strQry, dbKey);

        if (dt != null && dt.Rows.Count > 0)
        {
            str_return += "<div> <table id='tblActiveUsers' class='table table-striped table-bordered' >  "
                              + "<thead> "
                              + "     <tr> "
                              + "       <th >Question_ID</th> "
                              + "       <th >Question_ID_Text</th> "
                              + "       <th >Exception_ID</th> "
                              + "       <th >Exception_ID_Text</th> "
                              + "       <th >Action</th> "
                              + "    </tr> "
                              + "      </thead> "
                              + "        <tbody> ";

            foreach (DataRow dtr1 in dt.Rows)
            {
                str_return += "<tr>"
                                  + "<td>" + dtr1["Question_ID"].ToString() + "</td>"
                                  + "<td>" + dtr1["Question_ID_Text"].ToString() + "</td>"
                                  + "<td>" + dtr1["Exception_ID"].ToString() + "</td>"
                                  + "<td>" + dtr1["Exception_ID_Text"].ToString() + "</td>"
                                   //+ "<td><a href =' javascript:manageRecord(" + dtr1["Question_ID"] + "," + JsonConvert.SerializeObject(dtr1) + ")' > Manage</a></td>"
                                   + "<td><a href =' javascript:manageRecord(" + dtr1["Question_ID"] + ")' > Manage</a></td>"
                                  + "</tr>";
            }

            str_return += " </tbody> "
                                    + " </table> "
                                      + "</div></div></div >";
        }
        else
        {
            str_return = "    No data available...";
        }

        return str_return;

    }

    [WebMethod]
    public static string GetUserDetailsByRefQuestionId(string QId)
    {
        string sql_qry = "select Chevron_ID, Question_ID, Question_ID_Text, Exception_ID_Text," +
            " rq.Exception_ID from Ref_Questions rq left join Ref_Exception re on rq.Exception_ID = re.Exception_ID " +
            "where Question_ID='" + QId + "'";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);

    }
    [WebMethod]
    public static Dictionary<string, string> GetddlChevrons()
    {
        string sql_qry = "select Chevron_ID,Chevron_Name from Ref_Chevron";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);

        return dt.AsEnumerable().ToDictionary<DataRow, string, string>(row => row[0].ToString(),
                                       row => row[1].ToString());

    }
}