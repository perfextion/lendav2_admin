﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
public partial class admin_ReferenceQuestionsViewChildrens : System.Web.UI.Page
{
    static string dbKey = "gp_conn";
    protected void Page_Load(object sender, EventArgs e)
    {
        lbl_table.Text = LoadTable("");
    }

    [WebMethod]
    public static string LoadTable(string Job_Title_ID)
    {

        string str_sql = "";
        string str_return = "";
        string str_sqleid = "";

        //str_sql = "select t1.Chevron_ID,Chevron_Name,t1.Question_ID,t1.Question_ID_Text,t2.Question_ID as ChildID," +
        //    "t2.Question_ID_Text as ChildQuestion from Ref_Questions t1 left join Ref_Questions t2 on " +
        //    "t1.Question_ID = t2.Parent_Question_ID join [REF_CHEVRON] rc on t1.Chevron_ID = rc.Chevron_ID ORDER BY " +
        //    "t1.[Chevron_ID] ,t1.[Sort_Order]";

        str_sql = " Select t1.Chevron_ID,Chevron_Name,t1.Question_ID,t1.Question_ID_Text,t1.Exception_ID ,re.Exception_ID_text,t1.Questions_cat_code,t1.choice1,t1.choice2  ," +

            "t2.Question_ID as ChildID,t2.Question_ID_Text as ChildQuestion,t2.Exception_ID as ChildException_ID,re2.Exception_ID_text as ChildException_ID_text,t2.questions_cat_code as ChildQuestions_cat_code,t2.choice1 as ChildChoice1,t2.choice2 as ChildChoice2 " +
            " from Ref_Questions t1 left join Ref_Questions t2 on t1.Question_ID = t2.Parent_Question_ID join [REF_CHEVRON] rc on t1.Chevron_ID = rc.Chevron_ID " +
            " left join Ref_Exception re on t1.Exception_ID = re.Exception_ID left join Ref_Exception re2 on t2.Exception_ID = re2.Exception_ID ORDER BY  t1.[Chevron_ID] ,t1.[Sort_Order] ";

        DataTable dt1 = gen_db_utils.gp_sql_get_datatable(str_sql, dbKey);


        str_sqleid = " select [Exception_ID],[Exception_ID_Level],[Threshold_Operative],[Threshold_Value] " +
                      " from Ref_Exception_Level order by[Exception_ID],[Exception_ID_Level]";

        DataTable dteid = gen_db_utils.gp_sql_get_datatable(str_sqleid, dbKey);


        var distinctValues = dt1.AsEnumerable()
                          .Select(row => new
                          {
                              Chevron_ID = row.Field<int>("Chevron_ID"),
                              Chevron_Name = row.Field<string>("Chevron_Name")
                          })
                          .Distinct();
        foreach (var item in distinctValues)
        {
            str_return += "<div>" +
                                  "<div> <strong><h3><b style=font-size:16px;> Chevron " + item.Chevron_ID + "." + item.Chevron_Name + "</b><h3></strong></div>";
            var ParentQuestions = dt1.AsEnumerable()
                  .Where(s => s["Chevron_ID"].Equals(item.Chevron_ID))
                  .Select(row => new
                  {
                      ParentQuestionID = row.Field<int>("Question_ID"),
                      ParentQuestionText = row.Field<string>("Question_ID_Text"),
                      ParentChoice1 = row.Field<string>("choice1"),
                      ParentChoice2 = row.Field<string>("choice2"),
                      ParentQuestionsCatCode = row.Field<int?>("Questions_cat_code"),
                      ParentExceptionID = row.Field<int?>("Exception_ID"),
                      ParentExceptionText = row.Field<string>("Exception_ID_text")
                  }).Distinct();
            foreach (var subitems in ParentQuestions)
            {
                //string elid = dteid.Rows[0]["Exception_ID"].ToString();
                string refeid = subitems.ParentExceptionID.ToString();
                //if (elid == refeid)
                if (refeid !="")
                {
                    if (subitems.ParentQuestionsCatCode == 1)
                    {
                        str_return += "<div>" +
                                  "<div> <h5><b style=font-weight:540;font-size:16px;padding-left:45px;>" + subitems.ParentQuestionID + ". " + subitems.ParentQuestionText + "&nbsp;&nbsp;&nbsp;&nbsp;[ " + subitems.ParentChoice1 + " | " + subitems.ParentChoice2 + " ] </b></h5></div>" +
                                   "<div><h5><b style=font-weight:500;font-size:16px;padding-left:75px;> <i style=font-size:14px;> - Exception. " + subitems.ParentExceptionID + ". " + subitems.ParentExceptionText + "</i></b></h5></div>";
                    }
                    else
                    {
                        str_return += "<div>" +
                                      "<div> <h5><b style=font-weight:540;font-size:16px;padding-left:45px;>" + subitems.ParentQuestionID + ". " + subitems.ParentQuestionText + "&nbsp;&nbsp;&nbsp;&nbsp;[ FIB ] </b></h5></div>" +
                                       "<div><h5><b style=font-weight:500;font-size:16px;padding-left:75px;><i style=font-size:14px;> - Exception. " + subitems.ParentExceptionID + ". " + subitems.ParentExceptionText + "</i></b></h5></div>";
                    }
                    var ParentExceptionLevels = dteid.AsEnumerable()
                    .Where(s => s["Exception_ID"].Equals(subitems.ParentExceptionID))
                    .Select(row => new
                    {
                        ParentEid = row.Field<int>("Exception_ID"),
                        ParentELid = row.Field<int>("Exception_ID_Level"),
                        ParentETO = row.Field<string>("Threshold_Operative"),
                        ParentETV = row.Field<string>("Threshold_Value")
                    });

                    foreach (var itemelid in ParentExceptionLevels)
                    {
                        if (itemelid.ParentELid == 1)
                        {
                            str_return += "<div style=padding-left:113px;color:orange;margin-top:-7px;> Level " + itemelid.ParentELid + " : " + itemelid.ParentETO + "  " + itemelid.ParentETV + "</div>";
                        }
                        else
                        {
                            str_return += "<div style=padding-left:113px;color:red;> Level " + itemelid.ParentELid + " : " + itemelid.ParentETO + "  " + itemelid.ParentETV + "</div>";
                        }
                    }
                }
                else
                {
                    if (subitems.ParentQuestionsCatCode == 1)
                    {
                        str_return += "<div>" +
                                  "<div> <h5><b style=font-weight:500;font-size:16px;padding-left:45px;>" + subitems.ParentQuestionID + ". " + subitems.ParentQuestionText + "&nbsp;&nbsp;&nbsp;&nbsp;[ " + subitems.ParentChoice1 + " | " + subitems.ParentChoice2 + " ] </b></h5></div>";
                    }
                    else
                    {
                        str_return += "<div>" +
                                      "<div> <h5><b style=font-weight:500;font-size:16px;padding-left:45px;>" + subitems.ParentQuestionID + ". " + subitems.ParentQuestionText + "&nbsp;&nbsp;&nbsp;&nbsp;[ FIB ] </b></h5></div>";
                    }
                }
                var Child = dt1.AsEnumerable()
                  .Where(s => s["Question_ID"].Equals(subitems.ParentQuestionID))
                  .Select(row => new
                  {
                      ChildQuestionID = row.Field<int?>("ChildID"),
                      ChildQuestionText = row.Field<string>("ChildQuestion"),
                      ChildChoice1 = row.Field<string>("ChildChoice1"),
                      ChildChoice2 = row.Field<string>("ChildChoice2"),
                      ChildQuestionsCatCode = row.Field<int?>("ChildQuestions_cat_code"),
                      ChildExceptionID = row.Field<int?>("ChildException_ID"),
                      ChildExceptionText = row.Field<string>("ChildException_ID_text")
                  }).Distinct();

                foreach (var subchilditems in Child)
                {
                    string childrefeid = subchilditems.ChildExceptionID.ToString();
                    if (subchilditems.ChildQuestionID != null)
                    {
                        if (childrefeid != "")
                        {
                                                   
                            if (subchilditems.ChildQuestionsCatCode == 1)
                            {
                                str_return += "<div> " +
                                       "<div><h6 style=font-weight:500;font-size:14px;padding-left:125px>" + subchilditems.ChildQuestionID + ". " + subchilditems.ChildQuestionText + "&nbsp;&nbsp;&nbsp;&nbsp;[ " + subchilditems.ChildChoice1 + " | " + subchilditems.ChildChoice2 + " ] </ h6></div>" +
                                       "<div><h6><b style=font-weight:500;font-size:14px;padding-left:150px><i style=font-size:14px;>- Exception. " + subchilditems.ChildExceptionID + ". " + subchilditems.ChildExceptionText + "</i></b></h5></div>";
                            }
                            else
                            {
                                str_return += "<div> " +
                                      "<div><h6 style=font-weight:500;font-size:14px;padding-left:125px>" + subchilditems.ChildQuestionID + ". " + subchilditems.ChildQuestionText + "&nbsp;&nbsp;&nbsp;&nbsp;[ FIB ] </ h6></div>" +
                                      "<div><h6><b style=font-weight:500;font-size:14px;padding-left:150px><i style=font-size:14px;>- Exception. " + subchilditems.ChildExceptionID + ". " + subchilditems.ChildExceptionText + "</i></b></h5></div>";
                            }
                                var childExceptionLevels = dteid.AsEnumerable()
                                    .Where(s => s["Exception_ID"].Equals(subitems.ParentExceptionID))
                                    .Select(row => new
                                    {
                                        ParentEid = row.Field<int>("Exception_ID"),
                                        ParentELid = row.Field<int>("Exception_ID_Level"),
                                        ParentETO = row.Field<string>("Threshold_Operative"),
                                        ParentETV = row.Field<string>("Threshold_Value")
                                    });

                                foreach (var itemelid in childExceptionLevels)
                                {
                                    if (itemelid.ParentELid == 1)
                                    {
                                        str_return += "<div style=padding-left:175px;color:orange;margin-top:-7px;> Level " + itemelid.ParentELid + " :  " + itemelid.ParentETO + "  " + itemelid.ParentETV + "</div>";
                                    }
                                    else
                                    {
                                        str_return += "<div style=padding-left:175px;color:red;> Level " + itemelid.ParentELid + " :  " + itemelid.ParentETO + "  " + itemelid.ParentETV + "</div>";
                                    }
                                }
                        }
                        else
                        {
                            if (subchilditems.ChildQuestionsCatCode == 1)
                            {
                                str_return += "<div> " +
                                       "<div><h6 style=font-weight:500;font-size:14px;padding-left:100px>" + subchilditems.ChildQuestionID + ". " + subchilditems.ChildQuestionText + "[ " + subchilditems.ChildChoice1 + " | " + subchilditems.ChildChoice2 + " ] </ h6></div>";
                            }
                            else
                            {
                                str_return += "<div> " +
                                      "<div><h6 style=font-weight:500;font-size:14px;padding-left:100px>" + subchilditems.ChildQuestionID + ". " + subchilditems.ChildQuestionText + "[ FIB ] </ h6></div>";
                            }
                        }
                    }
                }
            }
        }
        str_return += "<br /><br /><br /><br /></div></div></div>";
        return str_return;
    }
}