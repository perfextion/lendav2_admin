﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_ReferenceRegion : BasePage
{
    public admin_ReferenceRegion()
    {
        Table_Name = "Ref_Region";
    }

    [WebMethod]
    public static string GetTableInfo()
    {
        return Get_Table_Info();
    }

    [WebMethod]
    public static string Getregion()
    {
        string sql_qry = "SELECT 0 as Actionstatus,Region_ID,Region_Name,Status from  Ref_Region ";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
    }
    [WebMethod]
    public static void SaveRegion(dynamic lst)
    {
        foreach (var item in lst)
        {
            string qry = "";
            int val = Convert.ToInt32(item["Actionstatus"]);
            if (val == 1)
            {
                qry = "INSERT INTO Ref_Region(Region_Name,Status)" +
                    " VALUES('" + item["Region_Name"].Replace("'", "''") + "','" + item["Status"].Replace("'", "''") + "')";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 2)
            {
                qry = "UPDATE Ref_Region SET Region_Name = '" + item["Region_Name"].Replace("'", "''") + "' ,Status = '" + item["Status"].Replace("'", "''") + "' where Region_ID=" + item["Region_ID"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 3)
            {
                qry = "DELETE FROM Ref_Region  where Region_ID=" + item["Region_ID"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
        }
        gen_db_utils.Update_Table_Audit_Trail("Ref_Region", userid, dbKey);
    }
}