﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class admin_ReferenceState : BasePage
{
    public admin_ReferenceState()
    {
        Table_Name = "Ref_State";
    }

    [WebMethod]
    public static string GetTableInfo()
    {
        return Get_Table_Info();
    }

    [WebMethod]
    public static string BindStateDetails()
    {
        string sql_qry = "Select State_ID,State_Abbrev,State_Name from Ref_State Order by State_ID";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static void SaveStateDetails(dynamic lst)
    {
        foreach (var item in lst)
        {
            string qry = "";
            int val = Convert.ToInt32(item["Actionstatus"]);
            if (val == 1)
            {
                qry = "INSERT INTO Ref_State (State_Abbrev,State_Name ) " +
                    " VALUES('" + ManageQuotes(item["State_Abbrev"]) + "','" + ManageQuotes(item["State_Name"]) + "')";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 2)
            {
                qry = "UPDATE Ref_State SET State_Abbrev = '" + ManageQuotes(item["State_Abbrev"]) + "',[State_Name] = '" + ManageQuotes(item["State_Name"]) + "' where State_ID=" + item["State_ID"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 3)
            {
                qry = "DELETE FROM Ref_State where State_ID=" + item["State_ID"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
        }

        gen_db_utils.Update_Table_Audit_Trail("Ref_State", userid, dbKey);
    }

    public static string ManageQuotes(string value)
    {
        if (!String.IsNullOrEmpty(value) && value.Contains("'"))
        {
            value = value.Replace("'", "''");
        }
        return value;
    }
}