﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_Reference_VCA : System.Web.UI.Page
{
    static string dbKey = "gp_conn";
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    [WebMethod]
    public static string Getrefvca()
    {//SELECT  [Ref_VCA_ID],[CA_No],[Name],[Hyper_Code],[Status]  FROM [Ref_VCA_Fields]
        string sql_qry = "SELECT 0 as Actionstatus,[Ref_VCA_ID],[CA_No],[Name],[Hyper_Code],[Status]  FROM [Ref_VCA_Fields] ";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
    }


    [WebMethod]
    public static void SaveRefVCA(dynamic lst)
    {
        foreach (var item in lst)
        {
            string qry = "";
            int val = Convert.ToInt32(item["Actionstatus"]);
            if (val == 1)
            {
                qry = "INSERT INTO Ref_VCA_Fields(CA_No,Name,Hyper_Code,Status)" +
                    " VALUES('" + item["CA_No"].Replace("'", "''") + "','" + item["Name"].Replace("'", "''") + "'," +
                    "'" + item["Hyper_Code"].Replace("'", "''") + "','" + item["Status"].Replace("'", "''") + "')";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 2)
            {
                qry = "UPDATE Ref_VCA_Fields SET CA_No = '" + item["CA_No"].Replace("'", "''") + "' ," +
                    "Name = '" + item["Name"].Replace("'", "''") + "' ,Hyper_Code = '" + item["Hyper_Code"].Replace("'", "''") + "' ," +
                    "Status = '" + item["Status"].Replace("'", "''") + "' where Ref_VCA_ID=" + item["Ref_VCA_ID"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 3)
            {
                qry = "DELETE FROM Ref_VCA_Fields  where Ref_VCA_ID=" + item["Ref_VCA_ID"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
                    }
    }


}