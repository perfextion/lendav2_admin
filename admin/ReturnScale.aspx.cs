﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ReturnScale : BasePage
{
    public ReturnScale()
    {
        Table_Name = "ReturnScale";
    }

    [WebMethod]
    public static string GetTableInfo()
    {
        Count_Query = @"DECLARE @Count int;
                            Select @Count = Count(*)  from ref_discounts where Disc_Group_Code = 'RETURN_SCALE';";
        return Get_Table_Info();
    }

    [WebMethod]
    public static string GetReturnScale()
    {
        string sql_qry = @"select * from ref_discounts where Disc_Group_Code = 'RETURN_SCALE'";

        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static void SaveReturnScale(dynamic lst)
    {
        foreach (var item in lst)
        {
            string qry = "";
            int val = Convert.ToInt32(item["Actionstatus"]);

            if (val == 1)
            {
                qry = @"INSERT INTO [dbo].[ref_discounts]
                               ([discount_key]
                               ,[discount_value]
                               ,[CreatedOn]
                               ,[Disc_Group_Code])
                         VALUES
                               (
                                    '" + item["Discount_Key"].Replace("'", "''") + @"'" +
                                   ",'" + item["Discount_Value"] + @"', GETDATE()" +
                                   ",'RETURN_SCALE'" + @"
                                )  ";

                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 2)
            {
                qry = @"UPDATE [dbo].[ref_discounts]
                           SET  [discount_key] = '" + item["Discount_Key"] + @"' " +
                              ",[discount_value] = '" + item["Discount_Value"] + @"' " +
                         "WHERE Discount_Id = '" + item["Discount_Id"] + "' ";

                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 3)
            {
                qry = "DELETE FROM ref_discounts  where Discount_Id = '" + item["Discount_Id"] + "' ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
        }

        gen_db_utils.Update_Table_Audit_Trail("ReturnScale", userid, dbKey);
    }
}