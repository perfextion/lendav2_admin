﻿<%@ Page Title="Other Queue" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="RiskOtherQueue_New.aspx.cs" Inherits="admin_RiskOtherQueue_New" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style>
        .ui-jqgrid .ui-jqgrid-htable th div {
            height: auto;
            overflow: hidden;
            padding-right: 4px;
            padding-top: 2px;
            position: relative;
            vertical-align: text-top;
            white-space: normal !important;
        }

        .cvteste {
            padding-right: 14px !important;
        }

        .left {
            padding-left: 14px !important;
        }

        #content {
            padding: 14px !important;
        }

        td[data-title=not-mapped] {
            color: red;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="risk-admin-default">
        <div class="row" style="margin-left: 20px;">
            <div class="form-inline">
                <div class="col-md-11">
                    <div class="form-group">
                        <label for="ddlFieldID" style="margin-top: 16px; margin-bottom: 18px; margin-left: -1px;"> Reference Table :</label>
                        <select id="ddlFieldID" class="form-control" style="width: 241px">
                            <option value="">Select</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="loader"></div>
        <div class="row">
            <div style="margin-left: 50px">
                <table id="jqGrid"></table>
                <div id="jqGridPager"></div>
            </div>
        </div>
    </div>
    <br />
    <br />
    <br />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" Runat="Server">
    <script type="text/javascript">  
        var DeleteRows = [];
        var timer;
        var lastSelection;
        var canEdit = false;

        var databaseTypes = {};
        var assocList = [];
        var expenseList = [];

        $(function () {
            var obj = {
                Init: function () {
                    $('.loader').show();
                    obj.getListItems();
                    obj.getFieldIdList();                    

                    $("#navbtnadd").addClass('disabled');

                    $("#navbtnsave").click(function () {
                        $('.loader').show();
                        obj.buttonfinalize();
                    });

                    $("#navbtnrefresh").addClass('disabled');

                    $("#navbtndownload").click(function () {
                        $("#jqGrid").jqGrid("exportToExcel", {
                            includeLabels: true,
                            includeGroupHeader: true,
                            includeFooter: true,
                            fileName: "Other Queue.xlsx",
                            maxlength: 40 // maxlength for visible string data 
                        })
                    });
                    $("#txtSearchBar").on("keyup", function () {
                        var self = this;
                        obj.search(self);
                    });
                    $('#navbtncolumns').click(function () {
                        $("#jqGrid").columnChooser({});
                    });

                    $('#editBtn').change(function () {
                        canEdit = $(this).is(':checked');
                        if (!canEdit) {
                            if (lastSelection != null) {
                                var grid = $("#jqGrid");
                                grid.jqGrid('saveRow', lastSelection);
                                grid.jqGrid('restoreRow', lastSelection);
                                lastSelection = null;
                            }
                        }
                    });

                    $('#ddlFieldID').change(function () {
                        $('.loader').show();
                        obj.bindGrid();
                    });

                    $(".savetbox").change(function () {
                        var currentRow = $(this).closest("tr");
                        var assocoldname = currentRow.find("td:eq(4)").text();
                        var field_id = this.name;
                        var ref_asso_id = this.value;
                        var roqid = this.id;

                        $.ajax({
                            type: "POST",
                            url: "RiskOtherQueue_New.aspx/UpdateRiskother",
                            data: JSON.stringify({ ref_asso_id: ref_asso_id, ROQ_ID: roqid, Field_id: field_id, assocname: assocoldname }),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (data) {
                                $('#ddlFieldID').val(field_id).trigger('change');
                            },
                            failure: function (response) {
                            }
                        });
                    });
                },
                getListItems: function () {
                    $.ajax({
                        type: "POST",
                        url: "RiskOtherQueue_New.aspx/GetExpenseTypeList",
                        data: JSON.stringify({}),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            expenseList = JSON.parse(data.d);

                            $.ajax({
                                type: "POST",
                                url: "RiskOtherQueue_New.aspx/GetAssocList",
                                data: JSON.stringify({}),
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                success: function (data) {
                                    assocList = JSON.parse(data.d);
                                    obj.bindGrid();
                                },
                                failure: function () {
                                    obj.bindGrid();
                                }
                            });
                        },
                        failure: function () {
                            obj.bindGrid();
                        }
                    });
                    
                },
                buttonfinalize: function () {
                    $('.loader').show();
                    $.ajax({
                        type: "POST",
                        url: "RiskOtherQueue_New.aspx/Finalze",
                        data: JSON.stringify({ FieldID: '' }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            location.reload();
                            $('.loader').hide();
                        },
                        failure: function (response) {
                        }
                    });

                },
                getTableInfo: function () {
                    var filedid = $('#ddlFieldID').val();
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "RiskOtherQueue_New.aspx/GetTableInfo",
                        data: JSON.stringify({ FieldID: filedid }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            var data = JSON.parse(res.d);

                            var table_info = data.Table_Info[0];
                            $('#txtTableName').html(table_info.Table_Name);
                            $('#txtLastUpdated').html(table_info.Last_Updated);
                            $('#txtRecordsCount').html(table_info.Records_Count);

                            if (data.CanEdit) {
                                $('#editControl').removeClass('disabled');
                            } else {
                                $('#editControl').addClass('disabled');
                            }

                            $('#enableControl').addClass('disabled');
                            
                        }
                    });
                },
                getFieldIdList: function () {
                    $.ajax({
                        type: "POST",
                        url: "RiskOtherQueue_New.aspx/GetFieldIdList",
                        async: false,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            $.each(res.d, function (data, value) {
                                var htmlval;
                                
                                if ('AIP' == value) {
                                    valid = 'AIP'
                                    htmlval = "Association Name (AIP)"
                                }
                                
                                if ('DIS' == value) {
                                    htmlval = "Association Name (DIS)"
                                }

                                if (value == 'THR') {
                                    htmlval = "Association Name (THR)";
                                }

                                if ('Expense' == value) {
                                    htmlval = "Budget Expense Type (Expense)"
                                }

                                databaseTypes[value] = htmlval;

                                $("#ddlFieldID").append($("<option></option>").val(value).html(htmlval));
                            });
                        }

                    });
                },
                bindGrid: function () {
                    var filedid = $('#ddlFieldID').val();
                    $.ajax({
                        type: "POST",
                        url: "RiskOtherQueue_New.aspx/LoadTable",
                        data: JSON.stringify({ FieldID: filedid }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {

                            var resData = JSON.parse(data.d);
                            $('.loader').show();
                            for (var i = 0; i < resData.length; i++) {
                                resData[i].rowid = i + 1;
                            }
                            obj.getTableInfo();
                            obj.loadgrid(resData);
                            $('.loader').hide();
                        }
                    });
                },
                loadgrid: function (data) {
                    $.jgrid.gridUnload("#jqGrid");
                    $("#jqGrid").jqGrid({
                        datatype: "local",
                        styleUI: 'Bootstrap',
                        data: data,
                        colModel: [
                            { label: 'rowid', name: 'rowid', width: 75, editable: false, key: true, hidden: true, hidedlg: true },
                            { label: 'ROQ_ID', name: 'ROQ_ID', align: "left", classes: "left", width: 150, editable: false, hidden: true, hidedlg: true },
                            {
                                label: 'Loan Full ID',
                                name: 'Loan_Full_ID',
                                width: 150,
                                align: "center",
                                classes: "center",
                                editable: false
                            },
                            { label: 'Date Time', name: 'Data_Time_Entered', align: "left", classes: "left", width: 240, editable: false },
                            { label: 'User', name: 'Name', align: "left", classes: "left", width: 240, editable: false },
                            { label: 'Reference Table', name: 'Field_ID', formatter: obj.refDatabaseLink, align: "left", classes: "left", width: 240, editable: false },
                            { label: 'Field_ID', name: 'Field_ID', align: "left", classes: "left", width: 240, editable: false, hidden: true },
                            { label: 'Suggested_Ref_ID_Copy', name: 'Suggested_Ref_ID_Copy', align: "left", classes: "left", width: 240, editable: false, hidden: true },
                            {
                                label: 'Entry Data',
                                name: 'Other_Text',
                                align: "left",
                                classes: "left",
                                width: 240,
                                editable: false,
                                cellattr: function (rowId, val, rawObject, cm, rdata) {
                                    if (rawObject.Suggested_Ref_ID == 0) {
                                        return 'data-title=not-mapped';
                                    }
                                }
                            },
                            {
                                label: 'Ref Data',
                                name: 'Suggested_Ref_ID',
                                width: 240,
                                align: "left",
                                classes: "left",
                                cellattr: function (rowId, val, rawObject, cm, rdata) {
                                    return 'data-roq-id=' + rawObject.ROQ_ID + ' data-field-id=' + rawObject.Field_ID + ' data-suggested-ref-id=' + rawObject.Suggested_Ref_ID;
                                },
                                editable: true,
                                hidden: false,
                                edittype: 'select',
                                editoptions: {
                                    value: function () {
                                        var obj1 = $(this);
                                        return obj.getRefDataList(obj1);
                                    },
                                    dataEvents: [ 
                                        {
                                            type: 'change',
                                            fn: function (e) {
                                                var parent = $(this).parent();
                                                var field_id = parent.data('fieldId');
                                                var roqId = parent.data('roqId');

                                                if (e.target.value) {

                                                    var selected_option = $(this).children("option:selected");

                                                    var val = selected_option.val();
                                                    var text = selected_option.text();
                                                    obj.updateSuggestedRefId(val, text, field_id, roqId);
                                                }
                                            }
                                        }
                                    ]
                                },
                                formatter: 'select'                                
                            },
                            { label: 'Actionstatus', name: 'Actionstatus', width: 70, editable: false, hidden: true, exportcol: false, hidedlg: true  }
                        ],
                        viewrecords: true,
                        loadonce: true,
                        restoreAfterSelect: false,
                        saveAfterSelect: true,
                        sortable: true,
                        onSelectRow: obj.edit,
                        onPaging: obj.changePage,
                        height: 'auto',
                        rowNum: 100,
                        pager: "#jqGridPager",
                        loadComplete: function () {
                            var $this = $(this), ids = $this.jqGrid('getDataIDs'), i, l = ids.length;
                            for (i = 0; i < l; i++) {
                                $this.jqGrid('editRow', ids[i], true);

                                var row = $this.jqGrid('getRowData', ids[i]);
                                var select = $('#' + ids[i]).find('select');

                                select.val(row.Suggested_Ref_ID_Copy);
                            }
                        }
                    });
                },                
                getRefDataList: function (obj1) {
                    var grid = $("#jqGrid");
                    var rowData = grid.jqGrid('getRowData', obj1[0].rowId);

                    var values = {};
                    values['0'] = 'Select';

                    if (rowData.Field_ID == "Expense") {
                        expenseList.forEach(e => {
                            values[e.Budget_Expense_Type_ID] = e.Budget_Expense_Name;
                        });
                    } else {
                        assocList.filter(a => a.Assoc_Type_Code == rowData.Field_ID).forEach(a => {
                            values[a.Ref_Association_ID] = a.Assoc_Name;
                        });
                    }

                    return values;
                },
                updateSuggestedRefId: function (ref_asso_id, assocoldname, field_id, roq_id) {
                    $('.loader').show();
                     $.ajax({
                            type: "POST",
                            url: "RiskOtherQueue_New.aspx/UpdateRiskother",
                            data: JSON.stringify({ ref_asso_id: ref_asso_id, ROQ_ID: roq_id, Field_id: field_id, assocname: assocoldname }),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (data) {
                                $('#ddlFieldID').val(field_id).trigger('change');
                            },
                            failure: function (response) {
                            }
                        });
                },
                edit: function (id) {
                    if (id && id !== lastSelection && canEdit) {
                        var grid = $("#jqGrid");
                        grid.jqGrid('saveRow', lastSelection);
                        grid.jqGrid('restoreRow', lastSelection);
                        var row = grid.jqGrid('getRowData', lastSelection);

                        if (!jQuery.isEmptyObject(row)) {
                            var dataobj = jQuery("#jqGrid").jqGrid('getGridParam', 'data');
                            index = dataobj.findIndex(x => x.rowid == row.rowid);
                            if (row.rowid > 0 && row.Actionstatus != 1) {
                                row.Actionstatus = 2;
                                grid.jqGrid('getGridParam', 'data')[index] = row;
                            }
                        }

                        grid.jqGrid('editRow', id);
                        lastSelection = id;
                    }
                },
                search: function (self) {
                    if (timer) { clearTimeout(timer); }
                    timer = setTimeout(function () {
                        obj.changePage();
                        $("#jqGrid").jqGrid('filterInput', self.value);
                        var length = jQuery("#jqGrid").jqGrid('getGridParam', 'records');
                        $('#txtRecordsCount').html('Records Count: ' + length);
                    }, 0);
                },
                changePage: function () {

                    lastSelection = "";
                    var grid = $("#jqGrid");
                    var id = $('.inline-edit-cell').parent().parent().prop('id');
                    if (id != undefined) {
                        grid.jqGrid('saveRow', id);
                        grid.jqGrid('restoreRow', id);
                        var row = grid.jqGrid('getRowData', id);
                        if (!jQuery.isEmptyObject(row)) {
                            var dataobj = jQuery("#jqGrid").jqGrid('getGridParam', 'data');
                            index = dataobj.findIndex(x => x.rowid == row.rowid);
                            if (row.rowid > 0 && row.Actionstatus != 1) {
                                row.Actionstatus = 2;
                                grid.jqGrid('getGridParam', 'data')[index] = row;
                            }
                        }
                    }
                },
                deleteLink: function (cellValue, options, rowdata, action) {
                    return "<a href='javascript:deleteRecord(" + options.rowId + ")' class='glyphicon glyphicon-trash' style='color:red'></a>";
                },
                refDatabaseLink: function (cellValue, options, rowdata, action) {
                    return "<span>" + databaseTypes[rowdata.Field_ID] + "</span> ";
                }
            }
            obj.Init();

        });

        function deleteRecord(id) {

            if (canEdit) {
                var grid = $('#jqGrid');
                var result = confirm("Are you sure you Want to delete?");

                if (result == true) {
                    var ediId = $('.inline-edit-cell').parent().parent().prop('id');
                    grid.jqGrid('saveRow', ediId);
                    var rowData = grid.jqGrid('getRowData', ediId);
                    if (rowData.rowid > 0 && rowData.Actionstatus != 1) {
                        rowData.Actionstatus = 2;
                        grid.jqGrid('getGridParam', 'data')[ediId - 1] = rowData;
                    }

                    var row = grid.jqGrid('getRowData', id);
                    grid.jqGrid('delRowData', id);
                    if (row.Actionstatus != 1) {
                        DeleteRows.push(row);
                    }
                    var data = jQuery("#jqGrid").jqGrid('getGridParam', 'data');
                    for (var i = 0; i < data.length; i++) {
                        data[i].rowid = i + 1;
                    }
                    var curpage = parseInt($(".ui-pg-input").val());
                    jQuery('#jqGrid').jqGrid('clearGridData');
                    jQuery('#jqGrid').jqGrid('setGridParam', { data: data });
                    $("#jqGrid").trigger("reloadGrid", [{ page: curpage }]);
                    $("#jqGrid").trigger("reloadGrid", [{ page: curpage }]);
                    lastSelection = id;
                }
            }
        }
        function unique(list) {
            var result = [];
            $.each(list, function (i, e) {
                if ($.inArray(e, result) == -1) result.push(e);
            });
            return result;
        }
    </script>
</asp:Content>

