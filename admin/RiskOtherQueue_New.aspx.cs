﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_RiskOtherQueue_New : BasePage
{
    public admin_RiskOtherQueue_New()
    {
        Table_Name = "Risk_Other_Queue";
    }

    [WebMethod]
    public static string GetTableInfo(string FieldID)
    {
        Count_Query = @"DECLARE @Count int; SELECT @Count= Count(*) FROM Risk_Other_Queue where [Status] IS NULL";

        if (!string.IsNullOrEmpty(FieldID))
        {
            Count_Query += " and Field_ID = '" + FieldID + "' ";
        }

        return Get_Table_Info();
    }

    [WebMethod]
    public static List<string> GetFieldIdList()
    {
        string sql_qry = "select Distinct(Field_ID) from Risk_Other_Queue where Status IS NULL ";

        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        List<string> emp = new List<string>();
        return (from DataRow row in dt.Rows
                select row["Field_ID"].ToString()
               ).ToList();
    }

    [WebMethod]
    public static string GetExpenseTypeList()
    {
        string sql = "select CAST(Budget_Expense_Type_ID as varchar(10)) as [Budget_Expense_Type_ID],Budget_Expense_Name from Ref_Budget_Expense_Type";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql, dbKey);

        return JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static string GetAssocList()
    {
        string sql = "select Distinct(Assoc_Name), Assoc_Type_Code, cast(Ref_Association_ID as varchar(10)) as [Ref_Association_ID] from [Ref_Association] where Assoc_type_Code in (select Distinct(Field_ID) from Risk_Other_Queue Where Ref_Database_ID = 1)";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql, dbKey);

        return JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static string LoadTable(string FieldID)
    {
        string strQuery = "SELECT " +
                                "ROQ_ID," +
                                "Loan_Full_ID," +
                                "Field_ID," +
                                "Ref_Database_ID," +
                                "Ref_Column_ID," +
                                "ROQ.User_ID," +
                                "CONCAT(firstname, ' ', lastname) as [Name] ," +
                                "FORMAT(Data_Time_Entered, 'MM/dd/yyyy hh:mm:ss tt') as [Data_Time_Entered]," +
                                "FORMAT(Date_Time_Action, 'MM/dd/yyyy hh:mm:ss tt') as [Date_Time_Action]," +
                                "Other_Text," +
                                "Risk_Admin_ID," +
                                "CAST(ISNULL(Suggested_Ref_ID, 0) as varchar(20)) as [Suggested_Ref_ID]," +
                                "CAST(ISNULL(Suggested_Ref_ID, 0) as varchar(20)) as [Suggested_Ref_ID_Copy]," +
                                "Response_Ind," +
                                "ROQ.Status " +
                         "FROM Risk_Other_Queue ROQ " +
                         "LEFT JOIN Users us on us.UserID = ROQ.User_ID where ROQ.Status IS NULL ";

        if (!string.IsNullOrEmpty(FieldID))
        {
            strQuery += " and Field_ID='" + FieldID + "' ";
        }

        DataTable dt = gen_db_utils.gp_sql_get_datatable(strQuery, dbKey);
        return JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static string Finalze(string FieldID)
    {

        string sql_qry, Suggestedid, Contact, Location, Phone, Email, Ref_Column_ID, EntryData = "";
        sql_qry = " select ROQ_ID,Suggested_Ref_ID,Ref_Column_ID,Field_ID,[Status], Ref_Database_ID from [Risk_Other_Queue] " +
            "where  suggested_ref_id is not null and status is null ";

        if (!string.IsNullOrEmpty(FieldID))
        {
            sql_qry += " and field_id='" + FieldID + "'";
        }
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);

        foreach (DataRow dtrow in dt.Rows)
        {
            Suggestedid = dtrow["Suggested_Ref_ID"].ToString();
            Ref_Column_ID = dtrow["Ref_Column_ID"].ToString();
            if (dtrow["Field_ID"].ToString() == "Expense")
            {
                sql_qry = "select Budget_Expense_Type_ID, Budget_Expense_Name from Ref_Budget_Expense_Type where Budget_Expense_Type_ID= '" + Suggestedid + "'";
                DataTable dt1 = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
                EntryData = dt1.Rows[0][1].ToString();

                sql_qry = "update  Risk_Other_Queue set Status='1',Other_Text='" + EntryData + "' WHERE ROQ_ID='" + dtrow["ROQ_ID"].ToString() + "'";
                gen_db_utils.gp_sql_execute(sql_qry, dbKey);
            }
            else if (dtrow["Ref_Database_ID"].ToString() == "1")
            {
                sql_qry = "select [Ref_Association_ID],[Assoc_Name],Contact,Location,Phone,Email from [Ref_Association] where Ref_Association_ID= '" + Suggestedid + "'";
                DataTable dt1 = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
                EntryData = dt1.Rows[0][1].ToString();
                Contact = dt1.Rows[0][2].ToString();
                Location = dt1.Rows[0][3].ToString();
                Phone = dt1.Rows[0][4].ToString();
                Email = dt1.Rows[0][5].ToString();

                sql_qry = "update Loan_Association set Ref_Assoc_ID = '" + Suggestedid + "',Assoc_Name='" + EntryData + "', contact = '" + Contact + "'," +
                    " Location = '" + Location + "', Phone = '" + Phone + "', Email = '" + Email + "' where Assoc_ID = '" + Ref_Column_ID + "'";
                gen_db_utils.gp_sql_execute(sql_qry, dbKey);

                sql_qry = "update  Risk_Other_Queue set Status='1',Other_Text='" + EntryData + "' WHERE ROQ_ID='" + dtrow["ROQ_ID"].ToString() + "'";
                gen_db_utils.gp_sql_execute(sql_qry, dbKey);
            }
            else
            {

            }
        }
        return "Sucess";
    }

    [WebMethod]
    public static string UpdateRiskother(string ref_asso_id, string ROQ_ID, string Field_id, string assocname)
    {
        string sql_qry = string.Empty;

        if (!string.IsNullOrEmpty(ROQ_ID) && !string.IsNullOrEmpty(ref_asso_id))
        {
            string EntryData = string.Empty;
            if (Field_id == "Expense")
            {
                sql_qry = "select * from Ref_Budget_Expense_Type where Budget_Expense_Type_ID= '" + ref_asso_id + "'";
                DataTable dt1 = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
                EntryData = dt1.Rows[0][1].ToString();
            }
            else
            {
                sql_qry = "select * from Ref_Association where Ref_Association_ID= '" + ref_asso_id + "'";
                DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
                EntryData = dt.Rows[0][3].ToString();
            }

            sql_qry = "update Risk_Other_Queue  set Suggested_Ref_ID ='" + ref_asso_id + "'  where ROQ_ID ='" + ROQ_ID + "'";
            gen_db_utils.gp_sql_execute(sql_qry, dbKey);

            sql_qry = "update Risk_Other_Queue  set Suggested_Ref_ID ='" + ref_asso_id + "'  where Field_ID ='" + Field_id + "' and Other_Text ='" + assocname.Trim() + "'";
            gen_db_utils.gp_sql_execute(sql_qry, dbKey);

            if (Field_id != "Expense")
            {
                sql_qry = "update Ref_Association set ROQ_ID = '" + ROQ_ID + "' where Ref_Association_ID = '" + ref_asso_id + "'";
                gen_db_utils.gp_sql_execute(sql_qry, dbKey);
            }

            Update_Table_Audit();
        }
        else
        {
            return "failure";
        }
        return "Sucess";
    }
}