﻿<%@ Page Title="Other Queue" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="RiskOtherQueuetest.aspx.cs" Inherits="admin_RiskOtherQueuetest" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <div class="loader"></div>
    <div class="col-md-12">
        <div class="row">
            <div class="form-inline">
                <div class="col-md-11">
                    <div class="form-group">
                        <label for="lblofficenames" style="margin-top: 16px; margin-bottom: 18px; margin-left: -1px;">
                            Filter By :</label>
                        <select id="ddlFieldID" class="form-control" style="width: 241px">
                            <option value="">Select</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-1" style="margin-top: 10px;">
                    <div class="form-inline">
                        <div class="btn btn-primary" id="btnfinal">Finalize</div>
                    </div>
                </div>
            </div>
        </div>


    </div>
    <asp:HiddenField ID="fieldidhdn" runat="server" />
    <div class="form-horizontal">
        <div class="row">
            <div class="col-md-12">
                <div style="margin-left: 13px; margin-right: 13px;">
                    <asp:Label ID="lbl_table" runat="server" Text="lbl_table"></asp:Label>
                </div>
            </div>
        </div>
    </div>

    <script>

        $(function () {
            var obj = {
                Init: function () {
                    
                    $('.loader').hide();
                    obj.getfieldid();
                    //obj.ddlchange();
                    $('#ddlFieldID').change(function () {
                        var fieldid = $('#ddlFieldID').val();
                        $('#ContentPlaceHolder1_lbl_table').text('');
                        obj.changefieldid(fieldid);

                        $(".savetbox").change(function () {
                            
                            var currentRow = $(this).closest("tr");
                            var col1 = currentRow.find("td:eq(0)").text();
                            var col2 = currentRow.find("td:eq(1)").text();

                            var assocoldname = currentRow.find("td:eq(4)").text();
                            var field_id = this.name;
                            var ref_asso_id = this.value;
                            var roqid = this.id;

                            $.ajax({
                                type: "POST",
                                url: "RiskOtherQueuetest.aspx/UpdateRiskother",
                                data: JSON.stringify({ ref_asso_id: ref_asso_id, ROQ_ID: roqid, Field_id: field_id, assocname: assocoldname }),
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                success: function (data) {
                                    //window.location.href = "RiskOtherQueuetest.aspx?Field_ID=" + field_id;
                                    $('#ddlFieldID').val(field_id).trigger('change');
                                },
                                failure: function (response) {
                                }
                            });
                        });

                    });

                    //var fldname = $('#ContentPlaceHolder1_fieldidhdn').val();
                    //if (fldname != '' && fldname != null) {
                    //    $('#ddlFieldID').val(fldname).trigger('change');
                    //}
                    $('#btnfinal').click(function () {
                        obj.buttonfinalize();
                    });

                    $(".savetbox").change(function () {

                        var currentRow = $(this).closest("tr");
                        var col1 = currentRow.find("td:eq(0)").text();
                        var col2 = currentRow.find("td:eq(1)").text();

                        var assocoldname = currentRow.find("td:eq(4)").text();
                        var field_id = this.name;
                        var ref_asso_id = this.value;
                        var roqid = this.id;

                        $.ajax({
                            type: "POST",
                            url: "RiskOtherQueuetest.aspx/UpdateRiskother",
                            data: JSON.stringify({ ref_asso_id: ref_asso_id, ROQ_ID: roqid, Field_id: field_id, assocname: assocoldname }),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (data) {
                                //window.location.href = "RiskOtherQueuetest.aspx?Field_ID=" + field_id;
                                $('#ddlFieldID').val(field_id).trigger('change');
                            },
                            failure: function (response) {
                            }
                        });
                    });

                },

                buttonfinalize: function () {
                    $('.loader').show();
                    var filedid = $('#ddlFieldID').val();
                    $.ajax({
                        type: "POST",
                        url: "RiskOtherQueuetest.aspx/Finalze",
                        data: JSON.stringify({ FieldID: filedid }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            window.location.href = "RiskOtherQueuetest.aspx?Field_ID=" + filedid;
                            $('.loader').hide();
                        },
                        failure: function (response) {
                        }
                    });

                },
                getfieldid: function () {
                    $.ajax({
                        type: "POST",
                        url: "RiskOtherQueuetest.aspx/FieldID",
                        async: false,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            $.each(res.d, function (data, value) {
                                var htmlval;
                                var valid;
                                if ('AGY' == value) {
                                    valid = 'AGY'
                                    htmlval = "Association: Agency"
                                }
                                if ('AIP' == value) {
                                    valid = 'AIP'
                                    htmlval = "Association: AIP"
                                }
                                if ('BUY' == value) {
                                    valid = 'BUY'
                                    htmlval = "Association: Buyer"
                                }
                                if ('DIS' == value) {
                                    valid = 'DIS'
                                    htmlval = "Association: Distributor"
                                }
                                if ('Expense' == value) {
                                    valid = 'Expense'
                                    htmlval = "Budget: Budget Expense"
                                }
                                if (valid != null && valid != '') {
                                    $("#ddlFieldID").append($("<option></option>").val(valid).html(htmlval));
                                }
                            });
                        }

                    });
                },
                changefieldid: function (fieldid) {
                    $('.loader').show();
                    $.ajax({
                        type: "POST",
                        url: "RiskOtherQueuetest.aspx/LoadTable",
                        data: JSON.stringify({ FieldID: fieldid }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            $('#ContentPlaceHolder1_lbl_table').html(res.d);
                            $('.loader').hide();
                            //ddlchange: function () {

                            $(".savetbox").change(function () {
                                
                                var currentRow = $(this).closest("tr");
                                var col1 = currentRow.find("td:eq(0)").text();
                                var col2 = currentRow.find("td:eq(1)").text();

                                var assocoldname = currentRow.find("td:eq(4)").text();
                                var field_id = this.name;
                                var ref_asso_id = this.value;
                                var roqid = this.id;

                                $.ajax({
                                    type: "POST",
                                    url: "RiskOtherQueuetest.aspx/UpdateRiskother",
                                    data: JSON.stringify({ ref_asso_id: ref_asso_id, ROQ_ID: roqid, Field_id: field_id, assocname: assocoldname }),
                                    contentType: "application/json; charset=utf-8",
                                    dataType: "json",
                                    success: function (data) {
                                        //window.location.href = "RiskOtherQueuetest.aspx?Field_ID=" + field_id;
                                        $('#ddlFieldID').val(field_id).trigger('change');
                                    },
                                    failure: function (response) {
                                    }
                                });
                            });
                        }
                    });
                },
            }
            obj.Init();

        });

    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" runat="Server">
</asp:Content>

