﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_RiskOtherQueuetest : System.Web.UI.Page
{
    static string dbKey = "gp_conn";
    static string Field_ID = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        Field_ID = Request.QueryString.GetValues("Field_ID") == null ? null : (Request.QueryString.GetValues("Field_ID")[0].ToString());
        fieldidhdn.Value = Field_ID;
        if (!IsPostBack)
        {
            if (Session["UserID"] == null)
            {
                Response.Redirect("../Login.aspx");
            }
            else if (!string.IsNullOrEmpty(Field_ID))
            {
                lbl_table.Text = LoadTable(Field_ID);
            }
            else
            {
                lbl_table.Text = LoadTable("");
            }


        }
    }

    [WebMethod]
    public static List<string> FieldID()
    {
        string sql_qry = "select Distinct(Field_ID) from Risk_Other_Queue";

        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        List<string> emp = new List<string>();
        return (from DataRow row in dt.Rows
                select row["Field_ID"].ToString()
               ).ToList();
    }

    [WebMethod]
    public static string Finalze(string FieldID)
    {
        string sql_qry = "";
        sql_qry = "update  Risk_Other_Queue set Status='1' WHERE Field_ID='" + FieldID + "' and Suggested_Ref_ID IS NOT NULL;";
        gen_db_utils.gp_sql_execute(sql_qry, dbKey);
        return "Sucess";
    }

    [WebMethod]
    public static string LoadTable(string FieldID)
    {
        string strQuery = "SELECT ROQ_ID,Loan_ID,Loan_Seq_ID,Loan_Full_ID,Field_ID,Ref_Database_ID,Ref_Column_ID," +
            "ROQ.User_ID,firstname,lastname,Data_Time_Entered,Date_Time_Action,Other_Text,Risk_Admin_ID,Suggested_Ref_ID," +
            "Response_Ind,ROQ.Status FROM Risk_Other_Queue ROQ left join Users us on us.UserID = ROQ.User_ID where ROQ.Status IS NULL ";

        if (!string.IsNullOrEmpty(FieldID))
        {
            strQuery += " and Field_ID='" + FieldID + "' ";
        }
        DataTable dt = gen_db_utils.gp_sql_get_datatable(strQuery, dbKey);
        strQuery = "select Distinct(Assoc_Name),Assoc_type_Code from [Ref_Association] where 1=1";
        if (!string.IsNullOrEmpty(FieldID))
        {
            strQuery += " and Assoc_type_Code='" + FieldID + "'";
        }
        DataTable dtnames = gen_db_utils.gp_sql_get_datatable(strQuery, dbKey);
        return GeneteateGrid(dt, FieldID);
    }
    [WebMethod]
    public static string UpdateRiskother(string ref_asso_id, string ROQ_ID, string Field_id, string assocname)
    {
        string sql_qry = "";
        //string Contact, Location, Phone, Email;
        if (!string.IsNullOrEmpty(ROQ_ID) && !string.IsNullOrEmpty(ref_asso_id))
        {

            sql_qry = "select * from Ref_Association where Ref_Association_ID= '" + ref_asso_id + "'";
            DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
            string EntryData = dt.Rows[0][3].ToString();
            sql_qry = "";
            sql_qry = "update Risk_Other_Queue  set Suggested_Ref_ID='" + ref_asso_id + "',Other_Text='" + EntryData + "' where ROQ_ID='" + ROQ_ID + "'";
            gen_db_utils.gp_sql_execute(sql_qry, dbKey);
            sql_qry = "";
            sql_qry = "update Risk_Other_Queue  set Suggested_Ref_ID='" + ref_asso_id + "',Other_Text='" + EntryData + "' where Field_ID='" + Field_id + "' and Other_Text='" + assocname.Trim() + "'";
            gen_db_utils.gp_sql_execute(sql_qry, dbKey);
            sql_qry = "";
            sql_qry = "update Ref_Association set ROQ_ID='" + ROQ_ID + "' where Ref_Association_ID= '" + ref_asso_id + "'";
            gen_db_utils.gp_sql_execute(sql_qry, dbKey);
        }
        else
        {
            return "failure";
        }
        return "Sucess";
    }
    public static string GeneteateGrid(DataTable dt, string FieldID)
    {
        string str_return = "";
        string ddlnames = "";



        if (dt != null && dt.Rows.Count > 0)
        {
            str_return += "<div> <table id='tblActiveUsers' class='table table-striped table-bordered' >"
           + " <thead> "
            + " <tr> "
            + "<th style='width:200px;'> Loan ID </th>"
            + "<th style='width:230px;'> Data Time </th>"
             + "<th style='width:200px;'> User </th>"
             + "<th style='width:200px;'> Database</th>"
            + "<th style='width:450px;'> Entry Data </th>"
            + "<th style='width:260px;;'> Ref Data </th>"
            + "<th style='text-align:center;width: 100px;'> Action </th>"
             + "  </tr> "
             + "    </thead> "
             + "      <tbody>";

            foreach (DataRow dtrow in dt.Rows)
            {
                string color, strQuery = "";
                strQuery = "select Distinct(Assoc_Name),Assoc_type_Code,Ref_Association_ID from [Ref_Association] where 1=1 and Assoc_type_Code='" + dtrow["Field_ID"].ToString() + "'";
                //if (!string.IsNullOrEmpty(FieldID))
                //{
                //    strQuery += " and Assoc_type_Code='" + dtrow["Field_ID"].ToString() + "'";
                //}
                DataTable dtnames = gen_db_utils.gp_sql_get_datatable(strQuery, dbKey);
                ddlnames = "";
                foreach (DataRow dtddlrow in dtnames.Rows)
                {
                    if (dtrow["Other_Text"].ToString() == dtddlrow["Assoc_Name"].ToString())
                    {
                        ddlnames += "<option value='" + dtddlrow["Ref_Association_ID"].ToString() + "' selected>" + dtddlrow["Assoc_Name"].ToString() + "</option>";
                    }
                    else
                    {
                        ddlnames += "<option value='" + dtddlrow["Ref_Association_ID"].ToString() + "'>" + dtddlrow["Assoc_Name"].ToString() + "</option>";
                    }
                }



                if (string.IsNullOrEmpty(dtrow["Suggested_Ref_ID"].ToString()))
                {
                    color = "<tr style='color: red;'>";
                }
                else
                {
                    color = "<tr>";
                }
                string Fieldid = dtrow["Field_ID"].ToString();
                if ("AGY" == dtrow["Field_ID"].ToString())
                {
                    Fieldid = "Agency";
                }
                if ("BUY" == dtrow["Field_ID"].ToString())
                {
                    Fieldid = "Buyer";
                }
                if ("DIS" == dtrow["Field_ID"].ToString())
                {
                    Fieldid = "Distributor";
                }
                if ("Expense" == dtrow["Field_ID"].ToString())
                {
                    Fieldid = "Budget Expense";
                }

                str_return += color
                + "<td>" + dtrow["Loan_Full_ID"].ToString() + " </td>"
                + "<td>" + Convert.ToDateTime(dtrow["Data_Time_Entered"]).ToString("dd MMM yyyy hh:mm tt") + " </td>"
                + "<td> " + dtrow["firstname"].ToString() + " " + dtrow["lastname"].ToString() + "</td>"
                + "<td> " + Fieldid + " </td>"
                //+ "<td> " + dtrow["Field_ID"].ToString() + " </td>"
                 + "<td> " + dtrow["Other_Text"].ToString() + " </td>"

                 + "<td><select id='" + dtrow["ROQ_ID"].ToString() + "' class='form-control savetbox' name='" + dtrow["Field_ID"].ToString() + "' style='width:241px'>" +
                 "<option value='0'>Select</option>" + ddlnames + "</select></td>"




                + "<td style='text-align:center;'><a href = '../admin/RefAssociationMaping.aspx?ROQ_ID=" + dtrow["ROQ_ID"].ToString() + "' class='glyphicon glyphicon-th'></a> </td>"
                 + "</tr>";
            }
            str_return += " </tbody> "
            + " </table> "
            + " </div></div></div > ";
        }
        else
        {
            str_return = "    No data available...";
        }
        return str_return;
    }

}