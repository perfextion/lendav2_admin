﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_RiskVerify : BasePage
{
    public admin_RiskVerify()
    {
        Table_Name = "RiskVerify";
    }

    [WebMethod]
    public static string GetTableInfo()
    {
        Count_Query = @"DECLARE @Count int;
            SELECT @Count= Count(*) from Risk_Verify";
        return Get_Table_Info();
    }

    [WebMethod]
    public static string LoadTable()
    {
        string sql_qry = @"Select
                           [Risk_Verify_ID]
                          ,[Field_ID]
                          ,[Office_ID]
                          ,FORMAT([Verify_Date_1], 'MM-dd-yyyy') as [Verify_Date_1]
                          ,FORMAT([Verify_Date_2], 'MM-dd-yyyy') as [Verify_Date_2]
                          ,[Status]
                          ,0 as [Actionstatus]
                      FROM [dbo].[Risk_Verify]";

        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static void TruncateTable()
    {
        string qry = "TRUNCATE TABLE Risk_Verify";
        gen_db_utils.gp_sql_execute(qry, dbKey);
        Update_Table_Audit();
    }

    [WebMethod]
    public static void UploadExcel(dynamic list)
    {
        string qry = string.Empty;

        foreach (var item in list)
        {
            if(!string.IsNullOrEmpty(item["Status"]))
            {
                qry += @"INSERT INTO [dbo].[Risk_Verify]
                               ([Field_ID]
                               ,[Office_ID]
                               ,[Verify_Date_1]
                               ,[Verify_Date_2]
                               ,[Status])
                         VALUES
                               (
                                    '" + TrimSQL(item["Field ID"]) + @"'" +
                                   ",'" + item["Office ID"] + @"'" +
                                   ",'" + item["Verify Date 1"] + @"'" +
                                   ",'" + item["Verify Date 2"] + @"'" +
                                   ",'" + item["Status"] + @"'
                                )  ";
                qry += Environment.NewLine;
            }      
        }

        if(!string.IsNullOrEmpty(qry))
        {
            gen_db_utils.gp_sql_execute(qry, dbKey);
            Update_Table_Audit();
        }
    }

    [WebMethod]
    public static void SaveTable(dynamic ListItems)
    {
        foreach (var item in ListItems)
        {
            string qry = string.Empty;
            int val = Convert.ToInt32(item["Actionstatus"]);

            if (val == 1)
            {
                qry = @"INSERT INTO [dbo].[Risk_Verify]
                               ([Field_ID]
                               ,[Office_ID]
                               ,[Verify_Date_1]
                               ,[Verify_Date_2]
                               ,[Status])
                         VALUES
                               (
                                    '" + TrimSQL(item["Field_ID"]) + @"'" +
                                   ",'" + item["Office_ID"] + @"'" +
                                   ",'" + item["Verify_Date_1"] + @"'" +
                                   ",'" + item["Verify_Date_2"] + @"'" +
                                   ",'" + item["Status"] + @"'
                                )  ";

                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 2)
            {
                qry = @"UPDATE [dbo].[Risk_Verify]
                           SET  [Field_ID] = '" + TrimSQL(item["Field_ID"]) + @"' " +
                              ",[Office_ID] = '" + item["Office_ID"] + @"' " +
                              ",[Verify_Date_1] = '" + item["Verify_Date_1"] + @"' " +
                              ",[Verify_Date_2] = '" + item["Verify_Date_2"] + @"' " +
                              ",[Status] = '" + item["Status"] + @"' " +
                         "WHERE Risk_Verify_ID = '" + item["Risk_Verify_ID"] + "' ";

                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 3)
            {
                qry = "DELETE FROM Risk_Verify  where Risk_Verify_ID = '" + item["Risk_Verify_ID"] + "' ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
        }

        if (ListItems != null)
        {
            Update_Table_Audit();
        }
    }
}