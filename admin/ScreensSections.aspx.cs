﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_ScreensSections : System.Web.UI.Page
{
    static string dbKey = "gp_conn";
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    [WebMethod]
    public static string GetSections()
    {
        string sql_qry = "SELECT 0 as Actionstatus,z_screen_id,Screen,Subsection, rowid = ROW_NUMBER() OVER (ORDER BY z_screen_id) from  Z_Screens_SubSections ";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
    }


    [WebMethod]
    public static void SaveSections(dynamic lst)
    {
        foreach (var item in lst)
        {
            string qry = "";
            int val = Convert.ToInt32(item["Actionstatus"]);
            if (val == 1)
            {
                qry = "INSERT INTO Z_Screens_SubSections(Screen,Subsection)" +
                    " VALUES('" + item["Screen"].Replace("'", "''") + "','" + item["Subsection"].Replace("'", "''") + "')";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 2)
            {
                qry = "UPDATE Z_Screens_SubSections SET Screen = '" + item["Screen"].Replace("'", "''") + "' ,Subsection = '" + item["Subsection"].Replace("'", "''") + "' where z_screen_id=" + item["z_screen_id"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 3)
            {
                qry = "DELETE FROM Z_Screens_SubSections  where z_screen_id=" + item["z_screen_id"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }



        }
    }

}