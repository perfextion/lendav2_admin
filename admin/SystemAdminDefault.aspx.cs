﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_SystemAdminDefault : BasePage
{
    public admin_SystemAdminDefault()
    {
        Table_Name = "SystemAdminDefault";
    }

    [WebMethod]
    public static string GetTableInfo()
    {
        Count_Query = @"DECLARE @Count int;
            SELECT @Count= Count(*) from Ref_Admin_Default Where Admin_Ind = 'SysAdmin'";
        return Get_Table_Info();
    }

    [WebMethod]
    public static string LoadTable()
    {
        string sql_qry = @"SELECT *, 0 as [Actionstatus] from Ref_Admin_Default Where Admin_Ind = 'SysAdmin'";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static void SaveTable(dynamic ListItems)
    {
        foreach (var item in ListItems)
        {
            string qry = string.Empty;
            int val = Convert.ToInt32(item["Actionstatus"]);

            if (val == 1)
            {
                qry = @"INSERT INTO [dbo].[Ref_Admin_Default]
                               ([Admin_Default_Code]
                               ,[Admin_Default_Name]
                               ,[Default_Text]
                               ,[Admin_Ind]
                               ,[Status])
                         VALUES
                               (
                                    '" + TrimSQL(item["Admin_Default_Code"]) + @"'" +
                                   ",'" + TrimSQL(item["Admin_Default_Name"]) + @"'" +
                                   ",'" + TrimSQL(item["Default_Text"]) + @"'" +
                                   ",'SysAdmin'" +
                                   ",'" + item["Status"] + @"'
                                )  ";

                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 2)
            {
                qry = @"UPDATE [dbo].[Ref_Admin_Default]
                           SET  [Admin_Default_Code] = '" + TrimSQL(item["Admin_Default_Code"]) + @"' " +
                              ",[Admin_Default_Name] = '" + TrimSQL(item["Admin_Default_Name"]) + @"' " +
                              ",[Default_Text] = '" + TrimSQL(item["Default_Text"]) + @"' " +
                              ",[Admin_Ind] = 'SysAdmin' " +
                              ",[Status] = '" + item["Status"] + @"' " +
                         "WHERE Admin_Default_ID = '" + item["Admin_Default_ID"] + "' ";

                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 3)
            {
                qry = "DELETE FROM Ref_Admin_Default  where Admin_Default_ID = '" + item["Admin_Default_ID"] + "' ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
        }

        if (ListItems != null)
        {
            Update_Table_Audit();
        }
    }
}