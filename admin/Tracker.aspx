﻿<%@ Page Title="" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="Tracker.aspx.cs" Inherits="admin_Tracker" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/free-jqgrid/4.15.2/jquery.jqgrid.src.js"></script>
    <script src="http://www.guriddo.net/demo/js/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" />
    <script type="text/ecmascript" src="http://www.guriddo.net/demo/js/trirand/i18n/grid.locale-en.js"></script>
    <script type="text/ecmascript" src="http://www.guriddo.net/demo/js/trirand/src/jquery.jqGrid.js"></script>
    <link rel="stylesheet" type="text/css" href="http://www.guriddo.net/demo/css/trirand/ui.jqgrid-bootstrap.css" />
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <style>
        .severity-0 {
            background-color: #b1eaab;
        }

        .severity-1 {
            background-color: #f9af74;
        }

        .severity-2 {
            background-color: #ff0000;
        }
    </style>
    <div class="">
        <div class="panel-body row-padding">
            <div class="row">
                <div class="col-md-2" style="padding-left: 51px;">
                    <div id="btnAddRow" class="btn btn-info" style="width: 60px;">
                        add
                    </div>
                    <div id="btnSave" class="btn btn-success" style="width: 60px;">Save</div>
                </div>
                <div class="col-md-10">
                    <div class="form-inline">
                        <div class="form-group">
                            <label for="lblScreen">Screen :</label>
                            <select id="ddlscreen" class="form-control" style="width: 200px">
                                <option value="">Select</option>
                            </select>
                        </div>

                        <div class="form-group" style="padding-right: 10px">
                            <label for="lblsubsection">Sub Section :</label>
                            <select id="ddlsubsection" class="form-control" style="width: 200px">
                                <option value="">Select</option>
                            </select>
                        </div>

                        <label class="checkbox-inline">
                            <input type="checkbox" checked="checked" id="chkAll" value="" />Show All Users
                        </label>
                        <label class="checkbox-inline">
                            <input type="checkbox" id="chkCompleted" value="" />Show Archive 
                        </label>

                        <div class="form-group" style="padding-left: 30px">
                            <label for="txtSearch">Search :</label>
                            <input type="text" class="form-control" name="search" id="txtSearch" style="width: 200px" />
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="loader"></div>
        <div class="row">
            <div style="margin-left: 60px">
                <table id="jqGrid"></table>
                <div id="jqGridPager"></div>
            </div>
        </div>
    </div>
    <br />
    <br />
    <br />




    <script type="text/javascript">  
        var DeleteRows = [];
        var timer;
        var lastSelection;

        $(function () {
            var screenList = {};
            var subSectionsList = {};
            var userList = {};
            var assignedList = {};
            var statusList = {};

            var obj = {
                Init: function () {

                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "Tracker.aspx/GetScreen",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {

                            screenList = JSON.parse(data.d);
                            $.each(screenList, function (data, value) {
                                $("#ddlscreen").append($("<option></option>").val(data).html(value));
                            });
                        }
                    });
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "Tracker.aspx/GetUsers",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            userList = JSON.parse(data.d);
                        }
                    });
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "Tracker.aspx/GetAssignUsers",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            assignedList = JSON.parse(data.d);
                        }
                    });
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "Tracker.aspx/GetStatus",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            statusList = JSON.parse(data.d);
                        }
                    });
                    // obj.getSubSectionsbyScreen();
                    obj.loadSubSection("");
                    obj.getTrackDetails('Init');

                    $("#btnAddRow").click(function () {
                        obj.add();
                    });
                    $("#btnSave").click(function () {
                        obj.save();
                    });
                    $("#txtSearch").on("keyup", function () {
                        var self = this;
                        obj.search(self);
                    });

                    $("#chkAll").click(function () {
                        obj.showAll();
                    });
                    $("#chkCompleted").click(function () {
                        obj.showCompleted();
                    });

                    $("#ddlscreen").change(function () {

                        obj.getSubSectionsbyScreen(this.value);
                        obj.getTrackDetails();
                    });

                    $("#ddlsubsection").change(function () {
                        obj.getTrackDetails();

                    });

                },
                getSubSectionsbyScreen: function (screenName) {
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "Tracker.aspx/GetSubSections",
                        data: JSON.stringify({ sectionName: screenName }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            $.each(JSON.parse(data.d), function (data, value) {
                                $("#ddlsubsection").append($("<option></option>").val(data).html(value));
                            });
                        }
                    });
                },
                getTrackDetails: function (methodname) {

                    var isShowAll = $('#chkAll').is(":checked");
                    var isShowCompleted = $('#chkCompleted').is(":checked");
                    var userId = $('#lblUsername').text();

                    var screen = $("#ddlscreen").val();
                    var subSection = $("#ddlsubsection").val();

                    $.ajax({
                        type: "POST",
                        url: "Tracker.aspx/GetTrackerDetails",
                        data: JSON.stringify({ userId: userId, showAll: isShowAll, showCompleted: isShowCompleted, Screen: screen, SubSection: subSection }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            $('.loader').show();
                            if (methodname == 'Init') {
                                obj.loadgrid(data.d);
                            } else {
                                $('#jqGrid').jqGrid('clearGridData');
                                $('#jqGrid').jqGrid('setGridParam', { data: JSON.parse(data.d) });
                                $('#jqGrid').trigger('reloadGrid');
                            }

                            $('.loader').hide();

                        }
                    });
                },
                loadgrid: function (data) {
                    $("#jqGrid").jqGrid({
                        datatype: "local",
                        styleUI: 'Bootstrap',
                        data: JSON.parse(data),
                        colModel: [
                            { label: 'rowid', name: 'rowid', width: 75, editable: false, key: true, hidden: true },
                            { label: 'Issue ID', name: 'Issue_Id', width: 40, editable: false },
                            {
                                name: 'Screen', index: 'Screen', width: 150, editable: true, edittype: "select", formatter: 'select',
                                editoptions: {
                                    value: screenList,
                                    dataInit: function (elem) {
                                        //var val = $(elem).val();
                                        //obj.loadSubSection(val);
                                        setTimeout(function () {
                                            $(elem).change();
                                        }, 0);

                                        //$("#jqGrid").setColProp('SubSection', {
                                        //    editoptions: { value: obj.loadSubSection(val) }
                                        //});
                                    },
                                    dataEvents: [
                                        {
                                            type: 'change',
                                            fn: function (e) {
                                                obj.loadSubSection(this.value);
                                            }
                                        }]
                                }
                            },

                            { label: 'SubSection', name: 'SubSection', width: 100, editable: true, edittype: "select", formatter: 'select', editoptions: { value: subSectionsList } },
                            {
                                label: 'Date_Opened',
                                name: 'Date_Opened',
                                width: 100,
                                editable: true,
                                edittype: "text",
                                editoptions: {
                                    // dataInit is the client-side event that fires upon initializing the toolbar search field for a column
                                    // use it to place a third party control to customize the toolbar
                                    dataInit: function (element) {
                                        var date = new Date();
                                        $(element).datepicker({
                                            id: 'orderDate_datePicker',
                                            dateFormat: 'm/d/yy',
                                            defaultDate: date,
                                            //minDate: new Date(2000, 0, 1),
                                            //maxDate: new Date(2020, 0, 1),
                                            showOn: 'focus'
                                        });
                                    }
                                }
                            },
                            { label: 'Description', name: 'Description', width: 420, editable: true, edittype: 'textarea' },
                            { label: 'Severity', name: 'Severity', width: 70, editable: true, formatter: 'select', edittype: 'select', editoptions: { value: "0:0;1:1;2:2" } },
                            { label: 'Status', name: 'Status', width: 100, editable: true, edittype: "select", formatter: 'select', editoptions: { value: statusList } },
                            { label: 'Assigned_to', name: 'Assigned_to', width: 80, editable: true, edittype: "select", formatter: 'select', editoptions: { value: userList } },
                            { label: 'Assigned_by', name: 'Assigned_by', width: 80, editable: true, edittype: "select", formatter: 'select', editoptions: { value: assignedList } },
                            //  { label: 'Assigned_by', name: 'Assigned_by', width: 100, editable: false },
                            { label: 'Status_Code', name: 'Status_Code', width: 60, editable: true, formatter: 'select', edittype: 'select', editoptions: { value: "0:0;1:1" } },
                            { label: 'lastmod_by', name: 'lastmod_by', width: 80, editable: false },
                            { label: 'Actionstatus', name: 'Actionstatus', width: 70, editable: false, hidden: true },
                            { label: '', name: '', width: 55, formatter: obj.editLink }
                        ],
                        viewrecords: true,
                        loadonce: true,
                        restoreAfterSelect: false,
                        saveAfterSelect: true,
                        sortable: true,
                        loadComplete: function () {
                            ChangeCellColors();
                        },
                        'cellsubmit': 'clientArray',
                        onSelectRow: obj.edit,
                        onPaging: obj.changePage,
                        height: 'auto',
                        rowNum: 100,
                        pager: "#jqGridPager"
                    });
                },
                add: function () {

                    var grid = $("#jqGrid");

                    var rowsperPage = parseInt(grid.getGridParam('rowNum'));
                    var gridlength = grid.jqGrid('getGridParam', 'data').length;
                    var curpage = parseInt($(".ui-pg-input").val());

                    if (rowsperPage * curpage == gridlength) {

                        var row = obj.newrow();
                        var newRowId = grid.jqGrid('getGridParam', 'data').length + 1;
                        grid.jqGrid('addRowData', newRowId, row);
                        grid.trigger('reloadGrid');
                        $('.glyphicon-step-forward').trigger('click');

                    } else {

                        $('.glyphicon-step-forward').trigger('click');
                        var row = obj.newrow();
                        var newRowId = grid.jqGrid('getGridParam', 'data').length + 1;
                        grid.jqGrid('addRowData', newRowId, row);
                    }


                    lastSelection = newRowId;
                    grid.jqGrid('saveRow', lastSelection);
                    grid.jqGrid('restoreRow', lastSelection);

                    //  var eid = $('.inline-edit-cell').parent().parent().prop('id')
                    var eid = $('.editable').parent().parent().prop('id');
                    grid.jqGrid('saveRow', eid);
                    grid.jqGrid('restoreRow', eid);
                    grid.jqGrid('editRow', newRowId);

                    ChangeCellColors();
                },
                edit: function (id) { 

                    if (id && id !== lastSelection) {
                        var grid = $("#jqGrid");
                        var currentindex, prevpage;
                        var rowsperPage = parseInt(grid.getGridParam('rowNum'));
                        var curpage = parseInt($(".ui-pg-input").val());

                        if (curpage > 1) {
                            prevpage = curpage - 1;
                            currentindex = rowsperPage * prevpage + parseInt(lastSelection) - 1
                        } else {
                            currentindex = lastSelection;
                        }

                        grid.jqGrid('saveRow', lastSelection);
                        grid.jqGrid('restoreRow', lastSelection);
                        var row = grid.jqGrid('getRowData', lastSelection);
                        if (row.Issue_Id > 0) {
                            row.Actionstatus = 2;
                            grid.jqGrid('getGridParam', 'data')[currentindex] = row;
                        }

                        grid.jqGrid('editRow', id);
                        lastSelection = id;
                        ChangeCellColors();
                    }
                },
                newrow: function () {
                    var today = new Date();
                    var userName = $('#lblUsername').text();
                    var length = jQuery("#jqGrid").jqGrid('getGridParam', 'records');
                    var newid = length + 1;
                    var isselected = "1";
                    var isselected1 = false;
                    if (newid % 2 == 0) isselected = "0";
                    if (newid % 2 == 0) isselected1 = true;
                    var row = {
                        Actionstatus: 1,
                        Issue_Id: 0,
                        Screen: "",
                        SubSection: "",
                        Date_Opened: (today.getMonth() + 1) + '/' + today.getDate() + '/' + today.getFullYear(),
                        Description: "",
                        Severity: "",
                        Status: "",
                        Assigned_to: "",
                        Assigned_by: userName,
                        Status_Code: "",
                        lastmod_by: userName,
                    };
                    return row;
                },
                save: function () {

                    var grid = $("#jqGrid");
                    var allrows = [];

                    // var id = grid.jqGrid('getGridParam', 'selrow');
                    var id = $('.inline-edit-cell').parent().parent().prop('id')
                    // var id = $('.editable').parent().parent().prop('id');
                    grid.jqGrid('saveRow', id);

                    var rowData = grid.jqGrid('getRowData', id);
                    if (rowData.Issue_Id > 0) {
                        rowData.Actionstatus = 2;
                        grid.jqGrid('getGridParam', 'data')[id] = rowData;
                    }

                    for (var i = 0; i < DeleteRows.length; i++) {
                        DeleteRows[i].Actionstatus = 3
                        allrows.push(DeleteRows[i]);
                    }
                    var dataobj = grid.jqGrid('getGridParam', 'data');
                    var newRows = $.grep(dataobj, function (e) { return e.Actionstatus == 1 || e.Actionstatus == 2 });
                    for (var i = 0; i < newRows.length; i++) {
                        allrows.push(newRows[i]);
                    }

                    $.ajax({
                        type: "POST",
                        url: "Tracker.aspx/SaveTracker",
                        data: JSON.stringify({ lst: allrows }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            location.reload();
                        },
                        failure: function (response) {
                            alert(response.d);
                        }

                    });
                },
                delete: function (id) {

                },
                search: function (self) {
                    if (timer) { clearTimeout(timer); }
                    timer = setTimeout(function () {
                        $("#jqGrid").jqGrid('filterInput', self.value);
                    }, 0);
                },
                changePage: function () {

                    var grid = $("#jqGrid");
                    // var id = grid.jqGrid('getGridParam', 'selrow');
                    var id = $('.inline-edit-cell').parent().parent().prop('id');
                    //  var id = $('.editable').parent().parent().prop('id');

                    var currentindex, prevpage;
                    var rowsperPage = parseInt(grid.getGridParam('rowNum'));
                    var curpage = parseInt($(".ui-pg-input").val());


                    if (curpage > 1) {
                        prevpage = curpage - 1;
                        currentindex = rowsperPage * prevpage + parseInt(id) - 1
                    } else {
                        currentindex = id;
                    }

                    grid.jqGrid('saveRow', id);
                    grid.jqGrid('restoreRow', id);

                    var rowData = grid.jqGrid('getRowData', id);
                    if (rowData.Issue_Id > 0) {
                        rowData.Actionstatus = 2;
                        grid.jqGrid('getGridParam', 'data')[currentindex] = rowData;

                    }
                },
                editLink: function (cellValue, options, rowdata, action) {
                    return "<a href='/admin/TrackerArchives.aspx?IssueID=" + rowdata.Issue_Id + "' >View </a>";
                },
                loadSubSection: function (screenName) {

                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "Tracker.aspx/GetSubSections",
                        data: JSON.stringify({ sectionName: screenName }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {

                            subSectionsList = JSON.parse(data.d);
                            var id = $('.inline-edit-cell').parent().parent().prop('id');
                            if (id != undefined) {
                                selectedValues = $('#' + id + '_SubSection').val();
                                $('#' + id + '_SubSection').empty();
                                $.jgrid.fillSelectOptions($('#' + id + '_SubSection')[0], subSectionsList, ":", ";", false, selectedValues);
                            }
                        }
                    });
                },
                getSubSections: function (screen) {
                    obj.loadSubSection(screen);
                },
                showAll: function () {
                    obj.getTrackDetails();
                },
                showCompleted: function () {
                    obj.getTrackDetails();
                }

            }
            obj.Init();

        });
        function deleteRecord(id) {

            lastSelection = id;
            var grid = $('#jqGrid');
            var result = confirm("Are you sure you Want to delete?");

            if (result == true) {

                var ediId = grid.jqGrid('getGridParam', 'selrow');
                grid.jqGrid('saveRow', ediId);

                var rowData = grid.jqGrid('getRowData', ediId);
                if (rowData.Issue_Id > 0) {
                    rowData.Actionstatus = 2;
                    grid.jqGrid('getGridParam', 'data')[ediId] = rowData;
                }

                var row = grid.jqGrid('getRowData', id);
                grid.jqGrid('delRowData', id);

                var rowsperPage = parseInt(grid.getGridParam('rowNum'));
                var gridlength = grid.jqGrid('getGridParam', 'data').length;
                var curpage = parseInt($(".ui-pg-input").val());

                if (curpage > 1) {
                    if (gridlength == rowsperPage * (curpage - 1)) {
                        $('#prev_jqGridPager').trigger('click');
                    }
                }
                if (row.Issue_Id > 0) {
                    DeleteRows.push(row);
                }
                // grid.trigger('reloadGrid');


            }
        }
        function unique(list) {
            var result = [];
            $.each(list, function (i, e) {
                if ($.inArray(e, result) == -1) result.push(e);
            });
            return result;
        }
        function FormattedDate(val) {
            var today = new Date(val);
            var dd = today.getDate();
            var mm = today.getMonth() + 1;

            var yyyy = today.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            // var date = dd + '/' + mm + '/' + yyyy;
            var date = yyyy + '-' + mm + '-' + dd;
            return date
        }
        function ChangeCellColors() {
            var rowIDs = jQuery("#jqGrid").getDataIDs();
            //Going through lines
            for (var i = 0; i < rowIDs.length; i = i + 1) {
                rowData = jQuery("#jqGrid").getRowData(rowIDs[i]);

                if (rowData.Severity == '0')
                    $('#' + rowIDs[i] + ' td:eq(5)').addClass("severity-0");
                else if (rowData.Severity == '1')
                    $('#' + rowIDs[i] + ' td:eq(5)').addClass("severity-1");
                else if (rowData.Severity == '2')
                    $('#' + rowIDs[i] + ' td:eq(5)').addClass("severity-2");
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" runat="Server">
</asp:Content>

