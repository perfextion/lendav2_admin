﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_Tracker : System.Web.UI.Page
{
    static string dbKey = "gp_conn";
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    [WebMethod]
    public static string GetScreen()
    {
        string sql_qry = "select distinct UPPER(Screen) as Screen,UPPER(Screen) as Screen from  Z_Screens_SubSections ";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return DataTableToJsonstring(dt);
    }
    [WebMethod]
    public static string GetSubSections(string sectionName)
    {
        string sql_qry = "";
        if (string.IsNullOrEmpty(sectionName))
            sql_qry = "SELECT distinct UPPER(Subsection) as Subsection,UPPER(Subsection) as Subsection from  Z_Screens_SubSections";
        else
            sql_qry = "SELECT distinct UPPER(Subsection) as Subsection,UPPER(Subsection) as Subsection from  Z_Screens_SubSections where upper(screen)='" + sectionName.Replace("'", "''") + "' ";

        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return DataTableToJsonstring(dt);
    }

    [WebMethod]
    public static string GetUsers()
    {
        DataTable table = new DataTable();
        table.Columns.Add("Name", typeof(string));
        table.Columns.Add("USer", typeof(string));
        foreach (var item in Constant_Variables.Users)
        {
            table.Rows.Add(item, item);
        }

        return DataTableToJsonstring(table);
    }
    [WebMethod]
    public static string GetAssignUsers()
    {
        DataTable table = new DataTable();
        table.Columns.Add("Name", typeof(string));
        table.Columns.Add("USer", typeof(string));
        foreach (var item in Constant_Variables.Users)
        {
            table.Rows.Add(item, item);
        }

        return DataTableToJsonstring(table);
    }
    [WebMethod]
    public static string GetStatus()
    {
        DataTable table = new DataTable();
        table.Columns.Add("Name", typeof(string));
        table.Columns.Add("USer", typeof(string));
        foreach (var item in Constant_Variables.Status)
        {
            table.Rows.Add(item, item);
        }

        return DataTableToJsonstring(table);
    }


    [WebMethod]
    public static string GetTrackerDetails(string userId, bool showAll, bool showCompleted, string Screen, string SubSection)
    {
        string sql_qry = "SELECT 0 as Actionstatus,Issue_Id,UPPER(Screen) as Screen,UPPER(SubSection) as SubSection,CONVERT(VARCHAR(10), Date_Opened, 101) as Date_Opened,Description,Severity,Status,Assigned_to,Assigned_by,Status_Code,lastmod_date,lastmod_by  FROM Z_Issue_Tracker where 1=1 ";


        if (!string.IsNullOrEmpty(Screen))
        {
            sql_qry += " and Screen='" + Screen + "'";
        }
        if (!string.IsNullOrEmpty(SubSection))
        {
            sql_qry += " and SubSection='" + SubSection + "'";
        }
        if (showAll == false)
        {
            sql_qry += " and Assigned_by='" + userId + "'";

        }
        if (showCompleted == false)
        {
            sql_qry += " and  status<>'Archive'";
        }

        //  string sql_qry = "SELECT 0 as Actionstatus,Issue_Id,UPPER(Screen) as Screen,UPPER(SubSection) as SubSection,CONVERT(VARCHAR(10), Date_Opened, 101) as Date_Opened,Description,Severity,Status,Assigned_to,Assigned_by,Status_Code,lastmod_date,lastmod_by  FROM Z_Issue_Tracker ";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
    }


    [WebMethod]

    public static Dictionary<string, string> GetSection()
    {
        string sql_qry = "select Job_Title_ID,CONCAT( Job_Title_ID,'.',Job_Title) as Job_Title from Job_Title order by Job_Title_ID";
        // string sql_qry = "select Job_Title_ID,Job_Title from Job_Title order by Job_Title_ID";

        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);

        return dt.AsEnumerable().ToDictionary<DataRow, string, string>(row => row[0].ToString(),
                                       row => row[1].ToString());
    }


    [WebMethod]
    public static void SaveTracker(dynamic lst)
    {
        foreach (var item in lst)
        {
            string qry = "";
            int val = Convert.ToInt32(item["Actionstatus"]);
            if (val == 1)
            {
                qry = "INSERT INTO Z_Issue_Tracker(Screen,SubSection,Date_Opened,Description,Severity,Status,Assigned_to,Assigned_by,Status_Code,lastmod_date,lastmod_by)" +
                    " VALUES('" + item["Screen"].Replace("'", "''") + "','" + item["SubSection"].Replace("'", "''") + "','" + item["Date_Opened"].Replace("'", "''") + "','" + item["Description"].Replace("'", "''") + "','" + item["Severity"] + "','" + item["Status"] + "','" + item["Assigned_to"] + "','" + item["Assigned_by"] + "','" + item["Status_Code"] + "',current_timestamp,'" + item["lastmod_by"] + "'); SELECT SCOPE_IDENTITY()";
                //   gen_db_utils.gp_sql_execute(qry, dbKey);
                string lastIssueId = gen_db_utils.gp_sql_scalar(qry, dbKey);

                string sqlqry = "  INSERT INTO Z_Issue_Tracker_Archives  (Issue_Id,Screen,SubSection, Date_Opened,Date_Edited,Description,Severity,Status,Assigned_to,Assigned_by,Status_Code,lastmod_date,lastmod_by) "
                              + " SELECT Issue_Id, Screen, SubSection, Date_Opened, current_timestamp, Description, Severity, Status, Assigned_to, Assigned_by, Status_Code, lastmod_date, lastmod_by "
                              + " FROM Z_Issue_Tracker WHERE Issue_Id= " + lastIssueId + " ";

                gen_db_utils.gp_sql_execute(sqlqry, dbKey);


            }
            else if (val == 2)
            {

                qry = "UPDATE Z_Issue_Tracker SET Screen = '" + item["Screen"].Replace("'", "''") + "' ,Subsection = '" + item["SubSection"].Replace("'", "''") + "',  Date_Opened = '" + item["Date_Opened"].Replace("'", "''") + "' ,Description = '" + item["Description"].Replace("'", "''") + "',Severity = '" + item["Severity"] + "' ,Status = '" + item["Status"] + "',Assigned_to = '" + item["Assigned_to"] + "',Assigned_by = '" + item["Assigned_by"] + "',Status_Code = '" + item["Status_Code"] + "' ,lastmod_by = '" + item["lastmod_by"] + "'    where Issue_Id=" + item["Issue_Id"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);

                string sqlqry = "  INSERT INTO Z_Issue_Tracker_Archives  (Issue_Id,Screen,SubSection, Date_Opened,Date_Edited,Description,Severity,Status,Assigned_to,Assigned_by,Status_Code,lastmod_date,lastmod_by) "
                               + " SELECT Issue_Id, Screen, SubSection, Date_Opened, current_timestamp, Description, Severity, Status, Assigned_to, Assigned_by, Status_Code, lastmod_date, lastmod_by "
                               + " FROM Z_Issue_Tracker WHERE Issue_Id= " + item["Issue_Id"] + " ";

                gen_db_utils.gp_sql_execute(sqlqry, dbKey);
            }
            else if (val == 3)
            {
                qry = "DELETE FROM Z_Issue_Tracker  where Issue_Id=" + item["Issue_Id"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }

        }
    }

    public static string DataTableToJsonstring(DataTable dt)
    {
        DataSet ds = new DataSet();
        ds.Merge(dt);
        StringBuilder JsonStr = new StringBuilder();
        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            JsonStr.Append("{");
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                JsonStr.Append("\"" + ds.Tables[0].Rows[i][0].ToString() + "\":" + "\"" + ds.Tables[0].Rows[i][1].ToString() + "\",");
            }
            JsonStr = JsonStr.Remove(JsonStr.Length - 1, 1);
            JsonStr.Append("}");
            return JsonStr.ToString();
        }
        else
        {
            return null;
        }
    }

    public static string ManageQuotes(string value)
    {
        return value.Replace("'", "''");
    }
}