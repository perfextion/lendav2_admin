﻿<%@ Page Title="" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="TrackerArchives.aspx.cs" Inherits="admin_Tracker" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="http://www.guriddo.net/demo/js/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" />

    <script type="text/ecmascript" src="http://www.guriddo.net/demo/js/trirand/i18n/grid.locale-en.js"></script>
    <script type="text/ecmascript" src="http://www.guriddo.net/demo/js/trirand/src/jquery.jqGrid.js"></script>
    <link rel="stylesheet" type="text/css" href="http://www.guriddo.net/demo/css/trirand/ui.jqgrid-bootstrap.css" />
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <style>
        .severity-0 {
            background-color: #b1eaab;
        }

        .severity-1 {
            background-color: #f9af74;
        }

        .severity-2 {
            background-color: #ff0000;
        }
    </style>
    <div class="">
        <div class="panel-body row-padding">
            <div class="row">
                <div class="col-md-9" style="padding-left: 51px;">
                </div>
                <div class="col-md-3" style="padding-left: 0px;">
                    <div class="form-inline">
                        <div class="form-group">
                            <label for="txtSearch">Search :</label>
                            <input type="text" class="form-control" name="search" id="txtSearch" style="width: 200px" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="loader"></div>
        <div class="row">
            <div style="margin-left: 60px">
                <table id="jqGrid"></table>
                <div id="jqGridPager"></div>
            </div>
        </div>
    </div>
    <br />
    <br />
    <br />


    <script type="text/javascript">  
        $.urlParam = function (name) {
            var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
            if (results == null) {
                return null;
            }
            else {
                return decodeURI(results[1]) || 0;
            }
        }
        var timer;
        var issueID = $.urlParam('IssueID');
        $(document).ready(function () {
            // $.jgrid.defaults.responsive = true;
            $.ajax({
                type: "POST",
                async: false,
                url: "TrackerArchives.aspx/GetTrackerArchiveDetails",
                data: JSON.stringify({ issueID: issueID }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    $('.loader').show();

                    $("#jqGrid").jqGrid({
                        datatype: "local",
                        styleUI: 'Bootstrap',
                        data: JSON.parse(data.d),
                        colModel: [
                            { label: 'Archive ID', name: 'Archive_id', width: 50 },
                            { label: 'Issue ID', name: 'Issue_Id', width: 50 },
                            { label: 'Screen', name: 'Screen', width: 100 },
                            { label: 'SubSection', name: 'SubSection', width: 100 },
                            { label: 'Date_Opened', name: 'Date_Opened', width: 140 },
                            { label: 'Date_Edited', name: 'Date_Edited', width: 140 },
                            { label: 'Description', name: 'Description', width: 300 },
                            { label: 'Severity', name: 'Severity', width: 50 },
                            { label: 'Status', name: 'Status', width: 100 },
                            { label: 'Assigned_to', name: 'Assigned_to', width: 75 },
                            { label: 'Assigned_by', name: 'Assigned_by', width: 75 },
                            { label: 'Status_Code', name: 'Status_Code', width: 80 },
                            { label: 'lastmod_by', name: 'lastmod_by', width: 75 }
                        ],
                        viewrecords: true,
                        loadonce: true,
                        restoreAfterSelect: false,
                        saveAfterSelect: true,
                        sortable: true,
                        height: 'auto',
                        rowNum: 100,
                        pager: "#jqGridPager",
                        loadComplete: function () {
                            ChangeCellColors();
                        }
                    });
                    $('.loader').hide();
                }
            });

            function ChangeCellColors() {
                var rowIDs = jQuery("#jqGrid").getDataIDs();

                //Going through lines
                for (var i = 0; i < rowIDs.length; i = i + 1) {
                    rowData = jQuery("#jqGrid").getRowData(rowIDs[i]);

                    if (rowData.Severity == '0')
                        $('#' + rowIDs[i] + ' td:eq(7)').addClass("severity-0");
                    else if (rowData.Severity == '1')
                        $('#' + rowIDs[i] + ' td:eq(7)').addClass("severity-1");
                    else if (rowData.Severity == '2')
                        $('#' + rowIDs[i] + ' td:eq(7)').addClass("severity-2");
                }
            }

            $("#txtSearch").on("keyup", function () {
                var self = this;
                if (timer) { clearTimeout(timer); }
                timer = setTimeout(function () {
                    $("#jqGrid").jqGrid('filterInput', self.value);
                }, 0);
            });


        });


    </script>


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" runat="Server">
</asp:Content>

