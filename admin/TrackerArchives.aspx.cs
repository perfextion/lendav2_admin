﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_Tracker : System.Web.UI.Page
{
    static string dbKey = "gp_conn";
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [WebMethod]
    public static string GetTrackerArchiveDetails(int issueID)
    {
        string sql_qry = "select Archive_id,Issue_Id,Screen,SubSection,FORMAT(Date_Opened,'dd/MM/yyyy hh:mm:ss') as Date_Opened,FORMAT(Date_Edited,'dd/MM/yyyy hh:mm:ss') as Date_Edited,Description,Severity,Status,Assigned_to,Assigned_by,Status_Code,lastmod_date,lastmod_by from Z_Issue_Tracker_Archives where Issue_Id=" + issueID + "  order by Archive_id desc";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
    }


}