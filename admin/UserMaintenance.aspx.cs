﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class admin_UserMaintenance : System.Web.UI.Page
{
    static string dbKey = "gp_conn";
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    [WebMethod]
    public static string Getuserdetails()
    {
        string sql_qry = "SELECT 0 as Actionstatus,UserID,Username,Password,Accesslevel,Status from HR_Users ";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
    }


    [WebMethod]
    public static void Saveuserdetails(dynamic lst)
    {
        foreach (var item in lst)
        {
            string qry = "";
            int val = Convert.ToInt32(item["Actionstatus"]);
            if (val == 1)
            {
                qry = "INSERT INTO HR_Users(Username,Password,Accesslevel,Status)" +
                    " VALUES('" + item["Username"].Replace("'", "''") + "','" + item["Password"].Replace("'", "''") + "','" + item["Accesslevel"].Replace("'", "''") + "','" + item["Status"].Replace("'", "''") + "')";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 2)
            {
                qry = "UPDATE HR_Users SET Username = '" + item["Username"].Replace("'", "''") + "',Password = '" + item["Password"].Replace("'", "''") + "',Accesslevel = '" + item["Accesslevel"].Replace("'", "''") + "',Status = '" + item["Status"].Replace("'", "''") + "' where UserID=" + item["UserID"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 3)
            {
                qry = "DELETE FROM HR_Users  where UserID=" + item["UserID"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
        }
    }
}