﻿<%@ Page Title="User Maintainance" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" EnableViewState="False" CodeFile="UserManagement.aspx.cs" Inherits="admin_UserManagement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href='../css/fullcalendar.min.css' rel='stylesheet' />
    <link href='../css/fullcalendar.print.min.css' rel='stylesheet' media='print' />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" />
    <script src='../js/moment.min.js'></script>

    <script src='../js/fullcalendar/jquery.min.js'></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src='../js/fullcalendar/fullcalendar.min.js'></script>

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://rawgit.com/abodelot/jquery.json-viewer/master/json-viewer/jquery.json-viewer.js"></script>
    <link href="https://rawgit.com/abodelot/jquery.json-viewer/master/json-viewer/jquery.json-viewer.css" rel="stylesheet" />

    <style>
        #content {
            padding: 25px 14px !important;
        }

        .email-link {
            text-decoration: underline;
        }

        .user-management-section .col-md-3 {
            width: 308px !important;
        }

        .user-management table#tblActiveUsers thead th:nth-child(1) {
            width: 16%;
        }

        .user-management table#tblActiveUsers thead th:nth-child(2) {
            width: 16%;
        }

        .user-management table#tblActiveUsers thead th:nth-child(3) {
            width: 16%;
        }

        .user-management table#tblActiveUsers thead th:nth-child(4) {
            width: 16%;
        }

        .user-management table#tblActiveUsers thead th:nth-child(5) {
            width: 16%;
        }

        .user-management table#tblActiveUsers thead th:nth-child(7) {
            width: 2%;
        }

        .user-management table#tblActiveUsers thead th:nth-child(8) {
            width: 2%;
        }

        .modal-body .name-div {
            margin-left: -5px;
            margin-right: -30px;
        }

        .edit-hide {
            display: none;
        }

        .edit-user div#idSection {
            display: none;
        }

        .edit-user .edit-hide {
            display: block !important;
        }

        .edit-user textarea {
            height: 38px;
            max-height: 38px;
            resize: none;
        }

        .edit-user div.nameSection  .col-md-4 {
            width: 100% !important;
        }

        .edit-user div.nameSection .col-md-4 .form-group {
            display: flex;
            margin-bottom: 0px;
        }

        .edit-user div.nameSection  .col-md-4 .form-group label {
            width: 45% !important;
        }

        .edit-user div.nameSection  .col-md-4 .form-group *:not(label) {
            width: 55% !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="loader"></div>
    <div class="row user-management-section" style="padding-left: 38px;">
        <div class="col-md-12" style="padding: 0px">            
            <div class="form-inline col-md-3" style="width: 292px !important;">
                <label for="ddlofficenames">Office: </label>
                <select id="ddlofficenames" class="form-control" style="width: 200px">
                    <option value="">Select</option>
                </select>
            </div>
            <div class="form-inline col-md-3">
                <label for="ddljobtitlename">Job Title: </label>
                <select id="ddljobtitlename" class="form-control" style="width: 200px">
                    <option value="">Select</option>
                </select>
            </div>
            <div class="form-inline col-md-3">
                <label for="ddlreporttoid1">Report To: </label>
                <select id="ddlreporttoid1" class="form-control" style="width: 200px">
                    <option value="">Select</option>
                </select>
            </div>
        </div>
    </div>
    <div class="row switch-controls">
        <div class="switch-control disabled" id="editControl">
            <label for="editBtn">Edit</label>
            <div class="toggle-btn small">            
                <input type="checkbox" id="editBtn" class="cb-value" />
                <span class="round-btn"></span>
            </div>
        </div>
        <div class="switch-control disabled" id="enableControl">
            <label for="enableBtn">Enable</label>
            <div class="toggle-btn small">            
                <input type="checkbox" id="enableBtn" class="cb-value" />
                <span class="round-btn"></span>
            </div>
        </div>
    </div>
    <div class="row user-management">
        <div class="col-md-12" style="padding-left: 51px; padding-top: 8px;">
            <asp:Label ID="lbl_table" runat="server"></asp:Label>
        </div>
    </div>
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">User Details</h4>
                </div>
                <div class="modal-body" style="margin-bottom: -10px;">
                    <div class="row name-div">
                        <div id="divModelSave">
                            <div class="col-md-12 nameSection">
                                <div id="divUserID" class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtuid">User ID</label>
                                        <input type="text" id="txtuid" class="form-control" disabled="disabled" name="UserName"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12" style="margin-bottom: 12px;" id="idSection">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <input type="checkbox" name="Processor" id="ddlSysAdmin" value="Bike" />
                                        System Admin                                       
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <input type="checkbox" name="Processor" id="ddlRiskAdmin" value="Bike" />
                                        Risk Admin                                     
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <input type="checkbox" name="Processor" id="ddlProcessor" value="Bike" />
                                        Processor                                        
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <input type="checkbox" name="Processor" id="ddlisadmin" value="Bike" />
                                        Is Admin 
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 nameSection" id="nameSection">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtuname">User Name :</label>
                                        <input type="text" id="txtuname" class="form-control" name="UserName" />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtutypeid">User Type Code</label>
                                        <select id="txtutypeid" class="form-control">
                                            <option value="">Select</option>
                                            <option value="0">ARM</option>
                                            <option value="1">Borrower</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="ddljobtitleid">Role Type Code</label>
                                        <select id="ddljobtitleid" class="form-control">
                                            <option value=" ">Select</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4 edit-hide">
                                    <div class="form-group">
                                        <label for="txtAccessLevel">Access Level</label>
                                        <input type="text" id="Job_Level" class="form-control" name="Job_Level"/>
                                    </div>
                                </div>
                                <div class="col-md-4 edit-hide">
                                    <div class="form-group">
                                        <label for="txtAccessFailedCount">Access Failed Count</label>
                                        <input type="text" id="Access_Failed_Count" class="form-control" name="Access_Failed_Count"/>
                                    </div>
                                </div>
                                <div class="col-md-4 edit-hide">
                                    <div class="form-group">
                                        <label for="lockoutEnabled">Lockout Enabled</label>
                                        <input type="text" id="Lockout_Enabled" class="form-control datepicker" disabled="disabled" name="Lockout_Enabled" />
                                    </div>
                                </div>
                                <div class="col-md-4 edit-hide">
                                    <div class="form-group">
                                        <label for="lockoutEndDate">Lockout End Date</label>
                                        <input type="text" id="Lockout_End_Date" class="form-control datepicker" disabled="disabled" name="Lockout_End_Date" />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtlastname">Last Name</label>
                                        <input type="text" id="txtlastname" class="form-control" name="UserName"/>
                                    </div>
                                </div> 
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtfirstname">First Name</label>
                                        <input type="text" id="txtfirstname" class="form-control" name="UserName"/>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtmi">MI</label>
                                        <input type="text" id="txtmi" class="form-control" name="UserName" />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtSearch">Address</label>
                                        <textarea id="txtaddress" class="form-control" style="height: 105px; resize: none;"></textarea>
                                    </div>
                                </div>
                                 <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtcity">City</label>
                                        <input type="text" id="txtcity" class="form-control" name="City" />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="ddlstate">State</label>
                                        <select id="ddlstate" class="form-control">
                                            <option value=" ">Select</option>
                                        </select>
                                    </div>
                                </div>
                                 <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtzip">Zip</label>
                                        <input type="text" id="txtzip" class="form-control" name="UserName" />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtphone">Phone</label>
                                        <input type="text" id="txtphone" class="form-control" name="UserName" />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtemail">Email</label>
                                        <input type="text" id="txtemail" class="form-control" name="UserName" />
                                    </div>
                                </div>
                                 <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="ddlofficename">Office ID</label>
                                        <select id="ddlofficename" class="form-control">
                                            <option value=" ">Select</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtdate">Hire Date</label>
                                        <input type="text" id="txtdate" class="form-control datepicker" name="UserName"/>
                                    </div>
                                </div>
                                <div class="col-md-4 edit-hide">
                                    <div class="form-group">
                                        <label for="Vacation_Start">Unavailable Begin</label>
                                        <input type="text" id="Vacation_Start" class="form-control datepicker" name="Vacation_Start"/>
                                    </div>
                                </div>
                                <div class="col-md-4 edit-hide">
                                    <div class="form-group">
                                        <label for="Vacation_End">Unavailable End</label>
                                        <input type="text" id="Vacation_End" class="form-control datepicker" name="Vacation_End"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row nameSection" id="addressSection">
                        <div class="row" style="margin-left: 10px">
                            <div class="col-md-12">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="ddlreporttoid">Report To ID</label>
                                        <select id="ddlreporttoid" class="form-control">
                                            <option value=" ">Select</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txtlastcommite">Last Committe Assignment</label>
                                        <input type="text" id="txtlastcommite" class="form-control datepicker" name="UserName" />
                                    </div>
                                </div>
                                <div class="col-md-4 edit-hide">
                                    <div class="form-group">
                                        <label for="txtPreferredContactInd">Preferred Contact Ind</label>                                        
                                        <select class="form-control" id="txtPreferredContactInd">
                                            <option value="">Select</option>
                                            <option value="1">Phone</option>
                                            <option value="2">Email</option>
                                            <option value="3">Text</option>
                                            <option value="4">Email & Text</option>
                                            <option value="5">Mail</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4 edit-hide">
                                    <div class="form-group">
                                        <label for="txtLoanCloseAvgDays">Loan Close Avg Days</label>
                                        <input type="text" id="txtLoanCloseAvgDays" class="form-control" disabled="disabled" name="txtLoanCloseAvgDays" />
                                    </div>
                                </div>
                                <div class="col-md-4 edit-hide">
                                    <div class="form-group">
                                        <label for="txtLoanCloseDealCount">Loan Close Deal Count</label>
                                        <input type="text" id="txtLoanCloseDealCount" class="form-control" disabled="disabled" name="txtLoanCloseDealCount" />
                                    </div>
                                </div>
                                <div class="col-md-4 edit-hide">
                                    <div class="form-group">
                                        <label for="txtLoanCloseTotalCommit">Loan Close Total Commit</label>
                                        <input type="text" id="txtLoanCloseTotalCommit" class="form-control" disabled="disabled" name="txtLoanCloseTotalCommit" />
                                    </div>
                                </div>
                                <div class="col-md-4 edit-hide">
                                    <div class="form-group">
                                        <label for="txtLossRatio">Loss Ratio</label>
                                        <input type="text" id="txtLossRatio" class="form-control" disabled="disabled" name="txtLossRatio" />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="ddlstatus">Status</label>
                                        <select id="ddlstatus" class="form-control">
                                            <option value="1">Active</option>
                                            <option value="0">InActive</option>
                                        </select>
                                    </div>
                                </div>                                
                            </div>                        
                        </div>                        
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="col-md-12">
                        <div class="col-md-8">
                        </div>
                        <div class="col-md-4 text-right">
                            <div class="btn btn-primary" id="btnModalSave">Save</div>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
            <asp:HiddenField ID="hdnOfficeID" runat="server" />
            <asp:HiddenField ID="hdnjobID" runat="server" />
            <asp:HiddenField ID="hdnkeyword" runat="server" />
            <asp:HiddenField ID="hdnJob_Title_ID" runat="server" />
            <asp:HiddenField ID="hdnOffice_ID" runat="server" />
            <asp:HiddenField ID="hdnJobLevel" runat="server" />
            <asp:HiddenField ID="hdnReportid" runat="server" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" runat="Server">
    <script>
        var canEdit = false;
        $(function () {
            $(".datepicker").datepicker();
        });

        $(function () {
            var obj = {
                Init: function () {
                    obj.buttonsave();
                    $('.loader').show();
                    obj.getjobtitlename();
                    obj.officename();
                    obj.ddljobchange();
                    obj.ddlofficeid();
                    obj.ddlreportchange();
                    obj.ddljoblevelchange();
                    obj.getreportfirstname();
                    obj.getallstates();
                    obj.loadjson();

                    $('#editBtn').change(function () {
                        canEdit = $(this).is(':checked');
                        if (!canEdit) {
                            $('.manage-link').css('text-decoration', 'none');
                        } else {
                            $('.manage-link').css('text-decoration', 'underline');
                        }
                    });

                    $("#txtSearchBar").on("keyup", function () {
                        var value = $(this).val().toLowerCase();
                        var count = 0;
                        $("#tblActiveUsers tbody tr").filter(function () {
                            var lineStr = $(this).text().toLowerCase();
                            if (lineStr.indexOf(value) === -1) {
                                $(this).hide();
                            } else {
                                $(this).show();
                                count++;
                            }
                        });

                        $('#txtRecordsCount').html('Records Count: ' + count);
                    });

                    $("#navbtndownload").click(function () {
                        exportTableToExcel_Exclude('tblActiveUsers', 'User Maintainance', [7]);
                    });

                    $("#btnSearch").on("click", function () {
                        var self = this;
                        obj.autosearch(self);
                        obj.getTableInfo();
                    });

                    $('#navbtnadd').on("click", function () {
                        $('#myModal').removeClass('edit-user');
                        obj.ClearModelData();
                        $('#divUserID').hide();
                        $('#divModelSave').show();
                        $('#ddlofficename').val(1)
                        $('#myModal').modal('show');
                    })

                    $('#navbtnrefresh').addClass('disabled');

                    obj.session();
                },
                session: function () {
                    if ($('#ContentPlaceHolder1_hdnOfficeID').val() == '' && $('#ContentPlaceHolder1_hdnjobID').val() == '') {
                        if ($('#ContentPlaceHolder1_hdnJob_Title_ID').val() != '') {
                            $('#ddljobtitlename').val($('#ContentPlaceHolder1_hdnJob_Title_ID').val());
                        }
                        if ($('#ContentPlaceHolder1_hdnOffice_ID').val() != '') {
                            $('#ddlofficenames').val($('#ContentPlaceHolder1_hdnOffice_ID').val());
                        }
                        if ($('#ContentPlaceHolder1_hdnReportid').val() != '') {
                            $('#ddlreporttoid1').val($('#ContentPlaceHolder1_hdnReportid').val());
                        }
                        obj.loadtable();
                    } else {
                        if ($('#ContentPlaceHolder1_hdnOfficeID').val() != '') {
                            $('#ddlofficenames').val($('#ContentPlaceHolder1_hdnOfficeID').val());
                        }
                        if ($('#ContentPlaceHolder1_hdnjobID').val() != '') {
                            $('#ddljobtitlename').val($('#ContentPlaceHolder1_hdnjobID').val());
                        }
                        obj.loadtable();
                    }
                },
                loadjson: function () {
                    $('.loadJsonViewer').on("click", function (index) {
                        var userid = $(this).attr('name');
                        $("#" + userid).css({ "cursor": "wait" });
                        $.ajax({
                            type: "POST",
                            url: "UserManagement.aspx/GetUsersettings",
                            data: JSON.stringify({ userId: userid }),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (res) {
                                var objdata = JSON.parse(res.d);
                                var settings = objdata[0];
                                $("#" + userid).attr("jsondata", settings.User_Settings);
                                obj.jsonviewer(userid);
                                $("#" + userid).css({ 'cursor': 'pointer' });
                            },
                            failure: function (response) {
                            }
                        });
                    });
                },
                jsonviewer: function (userid) {
                    let data = $("#" + userid).attr('jsondata');
                    if (data != undefined && data != "") {
                        $("#" + userid).jsonViewer(JSON.parse(data));
                        $("#" + userid).find('.json-toggle').click();

                    }
                },
                ClearModelData: function () {
                    $('#txtuid').val('');
                    $('#txtuname').val('');
                    $('#ddlisadmin').val('');
                    $('#txtutypeid').val('');
                    $('#txtfirstname').val('');
                    $('#txtlastname').val('');
                    $('#txtmi').val('');
                    $('#txtphone').val('');
                    $('#txtemail').val('');
                    $('#txtaddress').val('');
                    $('#txtcity').val('');
                    $('#ddlstate').val(' ');
                    $('#txtzip').val('');
                    $('#ddljobtitleid').val(' ');
                    $('#ddlofficename').val(' ');
                    $('#ddlreporttoid').val(' ');
                    $('#txtdate').val('');
                    $('#txtlastcommite').val('');
                    $('#ddlstatus').val('');
                    $('#ddlSysAdmin').val('');
                    $('#ddlRiskAdmin').val('');
                    $('#ddlProcessor').val('');
                    //$('#ddlJoblevel').val('');
                },
                buttonsave: function () {
                    $('#btnModalSave').click(function () {
                        $('.loader').show();
                        var dataobj = {}
                        dataobj.UserID = $('#txtuid').val();
                        dataobj.Username = $('#txtuname').val();
                        if ($("#ddlisadmin").is(":checked") == true) {
                            dataobj.IsAdmin = 1;
                        }
                        else {
                            dataobj.IsAdmin = 0;
                        }
                        //dataobj.IsAdmin = $('#ddlisadmin').val();
                        dataobj.User_Type_ID = $('#txtutypeid').val();
                        dataobj.FirstName = $('#txtfirstname').val();
                        dataobj.LastName = $('#txtlastname').val();
                        dataobj.MI = $('#txtmi').val();
                        dataobj.Phone = $('#txtphone').val();
                        dataobj.Email = $('#txtemail').val();
                        dataobj.Address = $('#txtaddress').val();
                        dataobj.City = $('#txtcity').val();
                        dataobj.State = $('#ddlstate').val();
                        dataobj.Zip = $('#txtzip').val();
                        dataobj.Job_Title_ID = $('#ddljobtitleid').val();
                        dataobj.Office_ID = $('#ddlofficename').val();
                        dataobj.Report_To_ID = $('#ddlreporttoid').val();
                        dataobj.Hire_Date = $('#txtdate').val();
                        dataobj.Last_Committe_Assignment = $('#txtlastcommite').val();
                        dataobj.Status = $('#ddlstatus').val();
                        if ($("#ddlSysAdmin").is(":checked") == true) {
                            dataobj.SysAdmin = 1;
                        }
                        else {
                            dataobj.SysAdmin = 0;
                        }

                        if ($("#ddlRiskAdmin").is(":checked") == true) {
                            dataobj.RiskAdmin = 1;
                        }
                        else {
                            dataobj.RiskAdmin = 0;
                        }

                        if ($("#ddlProcessor").is(":checked") == true) {
                            dataobj.Processor = 1;
                        }
                        else {
                            dataobj.Processor = 0;
                        }

                        var divModelSave = $('#divModelSave').show();
                        $('#spnException').hide();
                        $.ajax({
                            type: "POST",
                            url: "UserManagement.aspx/UpdateUserDetails",
                            data: JSON.stringify({ lst: dataobj }),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (data) {
                                $('.loader').show();
                                obj.loadtable();
                                $('#myModal').modal('hide');
                            },
                            failure: function (response) {
                            }
                        });
                    });
                },
                getjobtitlename: function () {
                    $.ajax({
                        type: "POST",
                        url: "UserManagement.aspx/GetAllJobtitle",
                        async: false,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            $.each(res.d, function (data, value) {
                                $("#ddljobtitleid").append($("<option></option>").val(data).html(value));
                                $("#ddljobtitlename").append($("<option></option>").val(data).html(value));
                            });
                        }
                    });
                },
                getTableInfo: function () {
                    var jobtitleid = $('#ddljobtitlename').val();
                    var officeid = $('#ddlofficenames').val();
                    var reportid = $('#ddlreporttoid1').val();
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "UserManagement.aspx/GetTableInfo",
                        data: JSON.stringify({ Job_Title_ID: jobtitleid, Office_ID: officeid, Reportid: reportid }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            var data = JSON.parse(res.d);

                            var table_info = data.Table_Info[0];
                            $('#txtTableName').html(table_info.Table_Name);
                            $('#txtLastUpdated').html(table_info.Last_Updated);
                            $('#txtRecordsCount').html(table_info.Records_Count);

                            if (data.CanEdit) {
                                $('#editControl').removeClass('disabled');
                            } else {
                                $('#editControl').addClass('disabled');
                            }

                            $('#enableControl').addClass('disabled');
                            
                        }
                    });
                },
                loadtable: function () {
                    $('#ContentPlaceHolder1_lbl_table').text('');
                    var jobtitleid = $('#ddljobtitlename').val();
                    var officeid = $('#ddlofficenames').val();
                    var reportid = $('#ddlreporttoid1').val();
                    $('.loader').show();
                    $.ajax({
                        type: "POST",
                        url: "UserManagement.aspx/LoadTable",
                        data: JSON.stringify({ Job_Title_ID: jobtitleid, Office_ID: officeid, Reportid: reportid }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            $('#ContentPlaceHolder1_lbl_table').html(res.d);
                            //loadJsonViewer();
                            obj.loadjson();
                            obj.getTableInfo();
                            $('.loader').hide();
                        }
                    });
                },
                officename: function () {
                    $.ajax({
                        type: "POST",
                        url: "UserManagement.aspx/GetAllofficenames",
                        async: false,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            $.each(res.d, function (data, value) {
                                $("#ddlofficename").append($("<option></option>").val(data).html(value));
                                $("#ddlofficenames").append($("<option></option>").val(data).html(value));
                            });
                            //var officeid = $('#ContentPlaceHolder1_hdnOfficeID').val().trim();
                            //if (officeid != '') {
                            //    $('#ddlofficenames').val(officeid);
                            //}
                            //var jobid = $('#ContentPlaceHolder1_hdnjobID').val().trim();

                            //if (jobid != '') {
                            //    $('#ddljobtitlename').val(jobid);
                            //}

                        }

                    });
                },
                ddljobchange: function () {
                    $('#ddljobtitlename').change(function () {
                        $('#ContentPlaceHolder1_lbl_table').text('');
                        var jobtitleid = $('#ddljobtitlename').val();
                        var officeid = $('#ddlofficenames').val();
                        var value = $('#txtSearch1').val();
                        var reportid = $('#ddlreporttoid1').val();
                        var joblevel = $('#ddlJoblevel1').val();
                        $('.loader').show();
                        $.ajax({
                            type: "POST",
                            url: "UserManagement.aspx/LoadTable",
                            data: JSON.stringify({ keyword: value, Job_Title_ID: jobtitleid, Office_ID: officeid, Reportid: reportid, JobLevel: joblevel }),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (res) {
                                $('.loader').show();
                                $('#ContentPlaceHolder1_lbl_table').html(res.d);
                                //  obj.autosearch();
                                //loadJsonViewer();
                                obj.loadjson();
                                obj.getTableInfo();
                                $('.loader').hide();
                            }
                        });
                    });
                },
                ddlofficeid: function () {
                    $('#ddlofficenames').change(function () {
                        $('#ContentPlaceHolder1_lbl_table').text('');
                        var jobtitleid = $('#ddljobtitlename').val();
                        var officeid = $('#ddlofficenames').val();
                        var value = $('#txtSearch1').val();
                        var reportid = $('#ddlreporttoid1').val();
                        var joblevel = $('#ddlJoblevel1').val();
                        $('.loader').show();
                        $.ajax({
                            type: "POST",
                            url: "UserManagement.aspx/LoadTable",
                            data: JSON.stringify({ keyword: value, Job_Title_ID: jobtitleid, Office_ID: officeid, Reportid: reportid, JobLevel: joblevel }),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (res) {
                                $('.loader').show();
                                $('#ContentPlaceHolder1_lbl_table').html(res.d);
                                // obj.autosearch();
                                //loadJsonViewer();
                                obj.loadjson();
                                obj.getTableInfo();
                                $('.loader').hide();

                            }
                        });
                    });
                },
                ddlreportchange: function () {
                    $('#ddlreporttoid1').change(function () {

                        $('#ContentPlaceHolder1_lbl_table').text('');
                        var jobtitleid = $('#ddljobtitlename').val();
                        var officeid = $('#ddlofficenames').val();
                        var value = $('#txtSearch1').val();
                        var reportid = $('#ddlreporttoid1').val();
                        var joblevel = $('#ddlJoblevel1').val();
                        $('.loader').show();
                        $.ajax({
                            type: "POST",
                            url: "UserManagement.aspx/LoadTable",
                            data: JSON.stringify({ keyword: value, Job_Title_ID: jobtitleid, Office_ID: officeid, Reportid: reportid, JobLevel: joblevel }),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (res) {
                                $('.loader').show();
                                $('#ContentPlaceHolder1_lbl_table').html(res.d);
                                //  obj.autosearch();
                                //loadJsonViewer();
                                obj.loadjson();
                                obj.getTableInfo();
                                $('.loader').hide();
                            }
                        });
                    });
                },
                ddljoblevelchange: function () {
                    $('#ddlJoblevel1').change(function () {
                        $('#ContentPlaceHolder1_lbl_table').text('');
                        var jobtitleid = $('#ddljobtitlename').val();
                        var officeid = $('#ddlofficenames').val();
                        var value = $('#txtSearch1').val();
                        var reportid = $('#ddlreporttoid1').val();
                        var joblevel = $('#ddlJoblevel1').val();
                        $.ajax({
                            type: "POST",
                            url: "UserManagement.aspx/LoadTable",
                            data: JSON.stringify({ keyword: value, Job_Title_ID: jobtitleid, Office_ID: officeid, Reportid: reportid, JobLevel: joblevel }),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (res) {
                                $('.loader').show();
                                $('#ContentPlaceHolder1_lbl_table').html(res.d);
                                //  obj.autosearch();
                                //loadJsonViewer();
                                obj.loadjson();
                                obj.getTableInfo();
                                $('.loader').hide();
                            }
                        });
                    });
                },
                autosearch: function () {
                    var value = $('#txtSearch1').val();
                    var jobtitleid = $('#ddljobtitlename').val();
                    var officeid = $('#ddlofficenames').val();
                    var reportid = $('#ddlreporttoid1').val();
                    var joblevel = $('#ddlJoblevel1').val();
                    $('.loader').show();
                    $.ajax({
                        type: "POST",
                        url: "UserManagement.aspx/LoadTable",
                        data: JSON.stringify({ keyword: value, Job_Title_ID: jobtitleid, Office_ID: officeid, Reportid: reportid, JobLevel: joblevel }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            $('#ContentPlaceHolder1_lbl_table').html(res.d);
                            $('.loader').hide();
                        }
                    });

                },
                getreportfirstname: function () {
                    $.ajax({
                        type: "POST",
                        url: "UserManagement.aspx/Getallfirstnames",
                        async: false,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            $.each(res.d, function (data, value) {
                                $("#ddlreporttoid").append($("<option></option>").val(data).html(value));
                                $("#ddlreporttoid1").append($("<option></option>").val(data).html(value));
                            });
                        }

                    });
                },
                getallstates: function () {
                    $.ajax({
                        type: "POST",
                        url: "UserManagement.aspx/Getallstates",
                        async: false,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {

                            $.each(res.d, function (data, value) {
                                $("#ddlstate").append($("<option></option>").val(data).html(value));
                            });
                        }

                    });
                }
            }
            obj.Init();
        });
        function manageRecord(id) {
            if (!canEdit) {
                return;
            }
            $('#divUserID').hide();
            $.ajax({
                type: "POST",
                url: "UserManagement.aspx/GetUserDetailsByUserID",
                data: JSON.stringify({ userId: id }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (res) {
                    $('#myModal').addClass('edit-user');
                    var objdata = JSON.parse(res.d);
                    var data = objdata[0];
                    $('#txtuid').val(data.UserID);
                    $('#divUserID').show();
                    $('#txtuname').val(data.Username);
                    if (data.IsAdmin == 1) {
                        $("#ddlisadmin").attr('checked', true);
                    }
                    else if (data.IsAdmin == 0) {
                        $("#ddlisadmin").attr('checked', false);
                    }
                    else {
                        $("#ddlisadmin").attr('checked', false);
                    }

                    //$('#ddlisadmin').val(data.IsAdmin);
                    $('#txtutypeid').val(data.User_Type_ID);
                    $('#txtfirstname').val(data.FirstName);
                    $('#txtlastname').val(data.LastName);
                    $('#txtmi').val(data.MI);
                    $('#txtphone').val(data.Phone);
                    $('#txtemail').val(data.Email);
                    $('#txtaddress').val(data.Address);
                    $('#txtcity').val(data.City);
                    $('#ddlstate').val(data.State);
                    $('#txtzip').val(data.Zip);
                    $('#ddljobtitleid').val(data.Job_Title_ID);
                    $('#ddlofficename').val(data.Office_ID);
                    $('#ddlreporttoid').val(data.Report_To_ID);
                    $('#txtdate').val(data.Hire_Date);
                    $('#txtlastcommite').val(data.Last_Committe_Assignment);
                    $('#ddlstatus').val(data.Status);

                    $('#Job_level').val(data.Job_level);
                    $('#Access_Failed_Count').val(data.Access_Failed_Count);
                    $('#Lockout_Enabled').val(data.Lockout_Enabled);
                    $('#Lockout_End_Date').val(data.Lockout_End_Date);
                    $('txtLoanCloseDealCount').val(data.Close_Deal_Count);
                    $('txtLoanCloseAvgDays').val(data.Average_Close_Days);
                    $('#txtLoanCloseTotalCommit').val(data.Total_Commitment);
                    $('#txtLossRatio').val(data.Loss_Ratio);

                    if (data.SysAdmin == 1) {
                        $("#ddlSysAdmin").attr('checked', true);
                    }
                    else if (data.SysAdmin == 0) {
                        $("#ddlSysAdmin").attr('checked', false);
                    }
                    else {
                        $("#ddlSysAdmin").attr('checked', false);
                    }


                    if (data.RiskAdmin == 1) {
                        $("#ddlRiskAdmin").attr('checked', true);
                    }
                    else if (data.SysAdmin == 0) {
                        $("#ddlRiskAdmin").attr('checked', false);
                    }
                    else {
                        $("#ddlRiskAdmin").attr('checked', false);
                    }


                    if (data.Processor == 1) {
                        $("#ddlProcessor").attr('checked', true);
                    }
                    else if (data.Processor == 0) {
                        $("#ddlProcessor").attr('checked', false);
                    }
                    else {
                        $("#ddlProcessor").attr('checked', false);
                    }
                    //$('#ddlSysAdmin').val(data.SysAdmin);
                    //$('#ddlRiskAdmin').val(data.RiskAdmin);
                    //$('#ddlProcessor').val(data.Processor);

                    //$('#ddlJoblevel').val(data.Job_level);
                }
            });
            $('#divModelSave').show();
            $('#myModal').modal('show');
        }
        $('#navbtncolumns').addClass('disabled');
    </script>
</asp:Content>

