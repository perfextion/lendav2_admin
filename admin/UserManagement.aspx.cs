﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_UserManagement : BasePage
{
    public admin_UserManagement()
    {
        Table_Name = "Users_Management";
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        string jobid = string.Empty;
        string officeid = string.Empty;
        string keyword = string.Empty;
        string Job_Title_ID = string.Empty;
        string Reportid = string.Empty;
        string JobLevel = string.Empty;
        string Office_ID = string.Empty;
        if (!IsPostBack)
        {
            jobid = Convert.ToString(Request.QueryString["JobtitleID"]);
            officeid = Convert.ToString(Request.QueryString["OfficeID"]);
            hdnOfficeID.Value = officeid;
            hdnjobID.Value = jobid;
            if (Session["keyword"] != null)
            {
                hdnkeyword.Value = Session["keyword"].ToString();
                keyword = Session["keyword"].ToString();
            }
            if (Session["Office_ID"] != null)
            {
                hdnOffice_ID.Value = Session["Office_ID"].ToString();
                Office_ID = Session["Office_ID"].ToString();
            }
            if (Session["Job_Title_ID"] != null)
            {
                hdnJob_Title_ID.Value = Session["Job_Title_ID"].ToString();
                Job_Title_ID = Session["Job_Title_ID"].ToString();
            }
            if (Session["Reportid"] != null)
            {
                hdnReportid.Value = Session["Reportid"].ToString();
                Reportid = Session["Reportid"].ToString();
            }
            if (Session["JobLevel"] != null)
            {
                hdnJobLevel.Value = Session["JobLevel"].ToString();
                JobLevel = Session["JobLevel"].ToString();
            }
        }

        base.Page_Load(sender, e);
    }

    [WebMethod]
    public static string GetTableInfo(string Job_Title_ID, string Office_ID, string Reportid)
    {
        string str_sql = Get_SQL_Query(Job_Title_ID, Office_ID, Reportid, false);

        Count_Query = @"DECLARE @Count int;
                        Select @Count = Count(*)  From(" + str_sql + @") A";

        return Get_Table_Info();
    }


    [WebMethod]
    public static string GeneteateGrid(DataTable dt1)
    {
        string str_return = "";
        if (dt1 != null && dt1.Rows.Count > 0)
        {
            str_return += "<div> <table id='tblActiveUsers' class='table table-striped table-bordered' >  "
                                 + "<thead> "
                                 + "     <tr> "
                                      + "       <th >User Details</th> "
                                      + "       <th >Job Details</th> "
                                      + "       <th >HR Details</th> "
                                      + "       <th >Admin Levels</th> "
                                      + "       <th >Loan Stats</th> "
                                      + "       <th >User Settings</th> "
                                      + "       <th >Status</th> "
                                     + "      <th >  </th> "
                                 + "    </tr> "
                                 + "      </thead> "
                                 + "        <tbody> ";

            foreach (DataRow dtr1 in dt1.Rows)
            {
                string jsonval = string.Empty;
                jsonval = "<td ><pre class='loadJsonViewer' id='" + dtr1["UserID"].ToString() + "' style='padding-left: 20px;cursor:pointer;' name='" + dtr1["UserID"].ToString() + "' jsondata=''>Click To Show</pre>  </td>";

                str_return += "<tr>"
                                 + "<td>" 
                                        + dtr1["UserID"].ToString() + " - " + dtr1["Name"].ToString() +
                                        "<br> <a class='email-link' target='_top' href='mailto:" + dtr1["Email"].ToString() + "'>" + dtr1["Email"].ToString() +"</a>" +
                                        "<br>" + dtr1["Phone"].ToString() 
                                 + "</td>"

                                 + "<td>" 
                                        + dtr1["Job_Title"].ToString() + (!string.IsNullOrEmpty(dtr1["Job_Title_Code"].ToString() ) ? " (" + dtr1["Job_Title_Code"].ToString() + ")" : "") +
                                        "<br>" + dtr1["Office_Name"].ToString() + ", " + dtr1["State"].ToString() +
                                        "<br>" + (string.IsNullOrEmpty(dtr1["Report_To_Name"].ToString()) ? "" : "Report To  " + dtr1["Report_To_Name"].ToString())
                                  + "</td>" 

                                  + "<td>" 
                                        + "Hire Date: "+ dtr1["Hire_Date"].ToString() +
                                        "<br>" + "Unavailable Bgn: " + dtr1["Vacation_Start"].ToString() +
                                        "<br>" + "Unavailable End: " + dtr1["Vacation_End"].ToString()
                                  + "</td>"

                                  + "<td>"
                                        + (dtr1["IsAdmin"].ToString() == "0" ? "<span class='hide'></span>" : "<span> Developer Admin </span> <br>") +
                                        (dtr1["RiskAdmin"].ToString() == "0" ? "<span class='hide'></span>" : "<span> Risk Admin </span> <br>") +
                                        (dtr1["SysAdmin"].ToString() == "0" ? "<span class='hide'></span>" : "<span> System Admin </span>")
                                  + "</td>"

                                  + "<td>" 
                                        + "Deals: " + dtr1["Total_Commitment"].ToString() +
                                        "<br>" + "Avg Days to Close: " + dtr1["Average_Close_Days"].ToString() +
                                        "<br>" + "Loss Ratio: " + dtr1["Loss_Ratio"].ToString() +
                                  "</td>"

                                  + jsonval
                                  + "<td style='text-align:center'>" + dtr1["Status"].ToString() + "</td>"
                                  + "<td><a class='manage-link' href =' javascript:manageRecord(" + dtr1["UserID"] + ")' > Manage</a></td>"
                              + "</tr>";

            }

            str_return += " </tbody> "
                                    + " </table> "
                                      + "</div></div></div >";

        }
        else
        {
            str_return = "    No data available...";
        }
        return str_return;
    }
    public static string GetUserDetails()
    {
        string sql_qry = "SELECT UserID,Username,IsAdmin,User_Type_ID,FirstName,LastName,MI,Phone," +
                        "Email,Address,City,State,Zip,Job_Title_ID,Office_ID,Report_To_ID,Hire_Date," +
                        "Last_Committe_Assignment,Status,SysAdmin,RiskAdmin,Processor,Job_level  FROM Users";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);

    }
    [WebMethod]
    public static string GetUserDetailsByUserID(string userId)
    {
        string sql_qry = "SELECT UserID,Username,IsAdmin,User_Type_ID,FirstName,LastName,MI,Phone," +
                            "Email,Address,City,State,Zip,Job_Title_ID,Office_ID,Report_To_ID," +
                            "Convert(varchar(20),Hire_Date, 101) as [Hire_Date]," +
                            "Convert(varchar(20),Vacation_Start, 101) as [Vacation_Start]," +
                            "Convert(varchar(20),Vacation_End, 101) as [Vacation_End]," +
                            "Convert(varchar(20),Lockout_Enabled, 101) as [Lockout_Enabled]," +
                            "Convert(varchar(20),Lockout_End_Date, 101) as [Lockout_End_Date]," +
                            "Convert(varchar(20),Last_Committe_Assignment, 101) as [Last_Committe_Assignment]," +
                            "FORMAT(0, 'P1') as [Loss_Ratio]," +
                            "0 as [Close_Deal_Count]," +
                            "(SELECT FORMAT(sum(arm_commitment), 'C0') FROM Loan_Master LM WHERE LM.Loan_Officer_ID  = UserID) as Total_Commitment, " +
                            "(SELECT avg(Datediff(day, Original_Application_Date, Close_Date)) FROM LOAN_MASTER  WHERE Loan_Officer_ID  = UserID) AS [Average_Close_Days], " +
                            "Status,SysAdmin,RiskAdmin,Processor," +
                            "Job_level, Access_Failed_Count " +
                        "FROM Users where UserID='" + userId + "'";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);

    }
    [WebMethod]
    public static string GetUsersettings(string userId)
    {
        string sql_qry = "SELECT User_Settings FROM Users where UserID='" + userId + "'";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);

    }

    [WebMethod]
    public static void UpdateUserDetails(Dictionary<string, string> lst)
    {
        Usermanagementsystem objmanag = new Usermanagementsystem();
        string json = JsonConvert.SerializeObject(lst);
        Usermanagementsystem usm = JsonConvert.DeserializeObject<Usermanagementsystem>(json);
        string qry, sql_qry = "";

        try
        {
            sql_qry = "select job_level,UserID from [Users] where UserID='" + usm.Report_To_ID + "'";
            DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);

            int val = Convert.ToInt32(dt.Rows[0][0]);
            usm.Job_level = val + 1;
        }
        catch
        {
            usm.Job_level = 1;
        }

        if (!string.IsNullOrEmpty(usm.UserID))
        {
            qry = "UPDATE Users set " +
                    "Username = '" + usm.Username + "'," +
                    "IsAdmin = '" + usm.IsAdmin + "'," +
                    "User_Type_ID = '" + usm.User_Type_ID + "'," +
                    "FirstName = '" + usm.FirstName + "'," +
                    "LastName ='" + usm.LastName + "'," +
                    "MI ='" + usm.MI + "'," +
                    "Phone ='" + usm.Phone + "'," +
                    "Email ='" + usm.Email + "'," +
                    "Address ='" + usm.Address + "'," +
                    "City ='" + usm.City + "'," +
                    "State ='" + usm.State + "'," +
                    "Zip ='" + usm.Zip + "'," +
                    "Job_Title_ID ='" + usm.Job_Title_ID + "'," +
                    "Office_ID ='" + usm.Office_ID + "'," +
                    "Report_To_ID ='" + usm.Report_To_ID + "'," +
                    "Hire_Date ='" + usm.Hire_Date + "'," +
                    "Last_Committe_Assignment ='" + usm.Last_Committe_Assignment + "'," +
                    "Status = '" + usm.Status + "'," +
                    "SysAdmin = '" + usm.SysAdmin + "'," +
                    "RiskAdmin = '" + usm.RiskAdmin + "'," +
                    "Processor = '" + usm.Processor + "'," +
                    "Job_level = '" + usm.Job_level + "' " +
                "where UserID = '" + usm.UserID + "'";
        }//
        else
        {
            qry = "DECLARE @UserId int;" +
                "insert into Users (Username,IsAdmin,User_Type_ID,FirstName,LastName,MI,Phone,Email,Address,City," +
                "State,Zip,Job_Title_ID,Office_ID,Report_To_ID,Hire_Date,Last_Committe_Assignment,Status,SysAdmin,RiskAdmin,Processor,Job_level) " +
                "values" +
                "(" +
                        "'" + usm.Username + "'," +
                        "'" + usm.IsAdmin + "'," +
                        "'" + usm.User_Type_ID + "'," +
                        "'" + usm.FirstName + "'," +
                        "'" + usm.LastName + "'," +
                        "'" + usm.MI + "'," +
                        "'" + usm.Phone + "'," +
                        "'" + usm.Email + "'," +
                        "'" + usm.Address + "'," +
                        "'" + usm.City + "'," +
                        "'" + usm.State + "'," +
                        "'" + usm.Zip + "'," +
                        "'" + usm.Job_Title_ID + "'," +
                        "'" + usm.Office_ID + "'," +
                        "'" + usm.Report_To_ID + "'," +
                        "'" + usm.Hire_Date + "'," +
                        "'" + usm.Last_Committe_Assignment + "'," +
                        "'" + usm.Status + "'," +
                        "'" + usm.SysAdmin + "'," +
                        "'" + usm.RiskAdmin + "'," +
                        "'" + usm.Processor + "'," +
                        "'" + usm.Job_level + "'" +
                  ") " +
                  "SET @UserId = @@IDENTITY;";

            qry += Environment.NewLine;

            qry += @"Update Users
                    Set User_Settings = (Select ARM_Default_Value From ARM_Defaults Where Default_Value_Type = 'PREFERENCES')
                    Where UserId = @UserId";
        }
        gen_db_utils.gp_sql_execute(qry, dbKey);

        gen_db_utils.Update_Table_Audit_Trail("Users_Management", userid, dbKey);
    }
    [WebMethod]
    public static Dictionary<string, string> GetAllJobtitle()
    {
        string sql_qry = "select Job_Title_ID,CONCAT( Job_Title_ID,'.',Job_Title) as Job_Title from Job_Title order by Job_Title_ID";
        // string sql_qry = "select Job_Title_ID,Job_Title from Job_Title order by Job_Title_ID";

        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);

        return dt.AsEnumerable().ToDictionary<DataRow, string, string>(row => row[0].ToString(),
                                       row => row[1].ToString());
    }
    [WebMethod]
    public static Dictionary<string, string> GetAllofficenames()
    {
        string sql_qry = "select Office_ID,CONCAT( Office_ID,'.',Office_Name) as Office_Name from Ref_Office order by Office_ID";
        //  string sql_qry = "select Office_ID,Office_Name from Ref_Office order by Office_ID";

        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);

        return dt.AsEnumerable().ToDictionary(row => row[0].ToString(),
                                       row => row[1].ToString());
    }
    [WebMethod]
    public static Dictionary<string, string> Getallfirstnames()
    {
        string sql_qry = "select UserID,CONCAT( UserID,'.',FirstName) as FirstName from Users order by UserID";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);

        return dt.AsEnumerable().ToDictionary<DataRow, string, string>(row => row[0].ToString(),
                                       row => row[1].ToString());
    }
    [WebMethod]
    public static Dictionary<string, string> Getallstates()
    {
        string sql_qry = "select State_ID,CONCAT( State_ID,'.',State_Name) as State_Name from Ref_State order by State_ID";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);

        return dt.AsEnumerable().ToDictionary<DataRow, string, string>(row => row[0].ToString(),
                                       row => row[1].ToString());
    }
    [WebMethod]
    public static string LoadTable(string Job_Title_ID, string Office_ID, string Reportid)
    {
        string str_sql = Get_SQL_Query(Job_Title_ID, Office_ID, Reportid);
        DataTable dt1 = gen_db_utils.gp_sql_get_datatable(str_sql, dbKey);
        return GeneteateGrid(dt1);
    }

    private static string Get_SQL_Query(string Job_Title_ID, string Office_ID, string Reportid, bool order_by = true)
    {
        string str_sql = string.Empty;

        HttpContext.Current.Session["Job_Title_ID"] = Job_Title_ID;
        HttpContext.Current.Session["Office_ID"] = Office_ID;
        HttpContext.Current.Session["Reportid"] = Reportid;

        str_sql = "SELECT UserID, User_Type_ID," +
           "Username," +
           "FirstName, LastName, MI," +
           "concat(LastName, (Case When NULLIF(LastName, '') != ''  then ', ' else '' end ) , FirstName, MI) as [Name], " +
           "a.Phone,Email, " +
           "a.Address,a.City,c.State,a.Zip,Job_Title,a.Job_Title_ID," +
           "ISNULL(b.Job_Title_Code, '') as [Job_Title_Code]," +
           "a.Office_ID,Office_Name,Report_To_ID," +
           "CONVERT(varchar, Vacation_End, 101) as [Vacation_End] , " +
           "Convert(varchar, Vacation_Start, 101) as [Vacation_Start], " +
           "CASE WHEN CONVERT(varchar, Hire_Date, 101) = '01/01/1900' THEN '' ELSE  CONVERT(varchar, Hire_Date, 101) END as [Hire_Date]," +
           "CONVERT(varchar, Last_Committe_Assignment, 101) as [Last_Committe_Assignment]," +
           "a.Status, " +
           "ISNULL(IsAdmin, 0) as [IsAdmin], " +
           "ISNULL(SysAdmin, 0) as [SysAdmin], " +
           "ISNULL(RiskAdmin, 0) as [RiskAdmin], " +
           "ISNULL(Processor, 0) as [Processor], " +
           "ISNULL(DevAdmin, 0) as [DevAdmin]," +
           "Job_level," +
           "FORMAT(0, 'P1') as [Loss_Ratio]," +
           "(SELECT FORMAT(sum(arm_commitment), 'C0') FROM Loan_Master LM WHERE LM.Loan_Officer_ID  = UserID)as Total_Commitment, " +
           "(SELECT avg(Datediff(day, Original_Application_Date, Close_Date)) FROM LOAN_MASTER  WHERE Loan_Officer_ID  = UserID) AS [Average_Close_Days], " +
           "dbo.[GetReportsToName](a.Report_To_ID) as Report_To_Name " +
           "FROM Users a LEFT JOIN Job_Title b on " +
           "a.Job_Title_ID = b.Job_Title_ID LEFT JOIN Ref_Office c on a.Office_ID = c.Office_ID where 1 = 1 ";
        

        if (!string.IsNullOrEmpty(Job_Title_ID))
        {
            str_sql += " and a.Job_Title_ID='" + Job_Title_ID + "' ";
        }

        if (!string.IsNullOrEmpty(Office_ID))
        {
            str_sql += " and a.Office_ID='" + Office_ID + "' ";
        }

        if (!string.IsNullOrEmpty(Reportid))
        {
            str_sql += " and a.Report_To_ID='" + Reportid + "' ";
        }

        if(order_by)
        {
            str_sql += " order by UserID";
        }

        return str_sql;
    }
}

public class Usermanagementsystem
{
    public string UserID { set; get; }
    public string Username { set; get; }
    public string IsAdmin { set; get; }
    public string User_Type_ID { set; get; }
    public string FirstName { set; get; }
    public string LastName { set; get; }
    public string MI { set; get; }
    public string Phone { set; get; }
    public string Email { set; get; }
    public string Address { set; get; }
    public string City { set; get; }
    public string State { set; get; }
    public string Zip { set; get; }
    public string Job_Title_ID { set; get; }
    public string Office_ID { set; get; }
    public string Report_To_ID { set; get; }
    public string Hire_Date { set; get; }
    public string Last_Committe_Assignment { set; get; }
    public string Status { set; get; }
    public string SysAdmin { set; get; }
    public string RiskAdmin { set; get; }
    public string Processor { set; get; }
    public int Job_level { set; get; }

    public Usermanagementsystem()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}