﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_UserPendingAction : BasePage
{
    public admin_UserPendingAction()
    {
        Table_Name = "User_Pending_Action";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        base.Page_Load(sender, e);
        LoanFullID.Value = Loan_Full_ID;
    }

    [WebMethod]
    public static string GetTableInfo(string Loan_Full_ID)
    {
        Count_Query = @"Declare @Count int;
                        select @Count = Count(*) FROM User_Pending_Action UP
                            LEFT JOIN Ref_Pending_Action RPA ON UP.Ref_PA_ID = RPA.Ref_PA_ID
                            LEFT JOIN Users U on U.UserId = UP.[User_ID]";

        if (!string.IsNullOrEmpty(Loan_Full_ID))
        {
            Count_Query += " Where Loan_Full_ID like '%" + Loan_Full_ID + "%'";
        }

        return Get_Table_Info();
    }

    
    [WebMethod]
    public static string GetUserPendingAction(string loanfullid)
    {
        HttpContext.Current.Session["Loan_Full_ID"] = loanfullid;
        string sql_qry = @"SELECT 0 as Actionstatus, CONCAT(User_ID, ': ', ISNULL(U.Username, U.Email)) as [Username], UP.Reminder_Ind, RPA.PA_Code, User_PA_ID, [User_ID],Loan_Full_ID,UP.Ref_PA_ID,RPA.PA_Level, From_User_ID,Custom_Txt,UP.[Status] 
                            FROM User_Pending_Action UP
                            LEFT JOIN Ref_Pending_Action RPA ON UP.Ref_PA_ID = RPA.Ref_PA_ID
                            LEFT JOIN Users U on U.UserId = UP.[User_ID]";

        if (!string.IsNullOrEmpty(loanfullid))
        {
            sql_qry += " Where UP.Loan_Full_ID like '%" + loanfullid + "%'";
        }

        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
    }
}