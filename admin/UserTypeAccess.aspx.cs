﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_UserTypeAccess : BasePage
{
    public admin_UserTypeAccess()
    {
        Table_Name = "Ref_User_Type_Access";
    }

    [WebMethod]
    public static string GetTableInfo()
    {
        Count_Query = string.Empty;
        return Get_Table_Info();
    }

    [WebMethod]
    public static string LoadTable()
    {
        string sql_qry = @"SELECT *, 0 as [Actionstatus] from Ref_User_Type_Access";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public static void SaveTable(dynamic ListItems)
    {
        foreach (var item in ListItems)
        {
            string qry = string.Empty;
            int val = Convert.ToInt32(item["Actionstatus"]);

            if (val == 1)
            {
                qry = @"INSERT INTO [dbo].[Ref_User_Type_Access]
                               ([User_Type_Code]
                               ,[User_Type_Name]
                               ,[System_Area]
                               ,[Access_Level]
                               ,[Status])
                         VALUES
                               (
                                    '" + item["User_Type_Code"].Replace("'", "''") + @"'" +
                                   ",'" + item["User_Type_Name"].Replace("'", "''") + @"'" +
                                   ",'" + item["System_Area"].Replace("'", "''") + @"'" +
                                   ",'" + item["Access_Level"].Replace("'", "''") + @"'" +
                                   ",'" + item["Status"] + @"'
                                )  ";

                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 2)
            {
                qry = @"UPDATE [dbo].[Ref_User_Type_Access]
                           SET  [User_Type_Code] = '" + item["User_Type_Code"].Replace("'", "''") + @"' " +
                              ",[User_Type_Name] = '" + item["User_Type_Name"].Replace("'", "''") + @"' " +
                              ",[System_Area] = '" + item["System_Area"] + @"' " +
                              ",[Access_Level] = '" + item["Access_Level"] + @"' " +
                              ",[Status] = '" + item["Status"] + @"' " +
                         "WHERE User_Type_Access_ID = '" + item["User_Type_Access_ID"] + "' ";

                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 3)
            {
                qry = "DELETE FROM Ref_User_Type_Access  where User_Type_Access_ID = '" + item["User_Type_Access_ID"] + "' ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
        }

        if (ListItems != null)
        {
            Update_Table_Audit();
        }
    }
}