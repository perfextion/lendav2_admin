﻿<%@ Page Title="User Type Access" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="UserTypeRoles.aspx.cs" Inherits="admin_UserTypeRoles" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href='../css/fullcalendar.min.css' rel='stylesheet' />
    <link href='../css/fullcalendar.print.min.css' rel='stylesheet' media='print' />
    <script src='../js/moment.min.js'></script>
    <script src='../js/fullcalendar/jquery.min.js'></script>
    <script src='../js/fullcalendar/fullcalendar.min.js'></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        table.paleBlueRows {
            font-family: Calibri;
            border: 1px solid #FFFFFF;
            width: 350px;
            height: 200px;
            /*text-align: center;*/
            border-collapse: collapse;
        }

            table.paleBlueRows td, table.paleBlueRows th {
                border: 1px solid #FFFFFF;
                padding: 3px 2px;
            }

            table.paleBlueRows tbody td {
                font-size: 14px;
            }

            /*table.paleBlueRows tr:nth-child(even) {
                background: #D0E4F5;
            }*/

            table.paleBlueRows thead {
                background: #0B6FA4;
                border-bottom: 5px solid #FFFFFF;
            }

                table.paleBlueRows thead th {
                    font-size: 17px;
                    font-weight: bold;
                    color: #FFFFFF;
                    /*text-align: center;*/
                    border-left: 2px solid #FFFFFF;
                }

                    table.paleBlueRows thead th:first-child {
                        border-left: none;
                    }

            table.paleBlueRows tfoot {
                font-size: 14px;
                font-weight: bold;
                color: #333333;
                background: #D0E4F5;
                border-top: 3px solid #444444;
            }

                table.paleBlueRows tfoot td {
                    font-size: 14px;
                }

        tr td:nth-child(2) { /* I don't think they are 0 based */
            text-align: center;
        }

        tr td:nth-child(3) { /* I don't think they are 0 based */
            text-align: center;
        }

        tr td:nth-child(4) { /* I don't think they are 0 based */
            text-align: center;
        }

        tr td:nth-child(5) { /* I don't think they are 0 based */
            text-align: center;
        }

        tr td:nth-child(6) { /* I don't think they are 0 based */
            text-align: center;
        }

        tr td:nth-child(7) { /* I don't think they are 0 based */
            text-align: center;
        }

        table {
            margin-top: 30px;
            margin-left: 36px;
            border-collapse: collapse;
        }

        table, td, th {
            border: 1px solid black;
        }

        .margins {
            margin-left: 24px;
        }

        .marginsin {
            margin-left: 45px;
        }

        .headers {
            background-color: rgb(36, 114, 179);
            color: white;
        }

        .firstbgcolor {
            background: #D0E4F5;
        }

        .subcolor {
            background-color: #ebeaea;
            background: #ebeaea;
        }

        .subtabcolor {
            background-color: #fbfbfb;
            background: #fbfbfb;
        }

        table.paleBlueRows td:nth-child(2), 
        table.paleBlueRows td:nth-child(3), 
        table.paleBlueRows td:nth-child(4), 
        table.paleBlueRows th:nth-child(2), 
        table.paleBlueRows th:nth-child(3),
        table.paleBlueRows th:nth-child(4){
          display: none;
        }

        table.paleBlueRows tbody td p {
            padding: 6px 5px 0px 5px;
        }
    </style>
    <table style="width: 1407px" class="paleBlueRows">
        <tbody>
            <tr class="headers">
                <th style="width: 594px;">
                    <p style="margin-top: 10px; margin-left: 10px; text-align: center;"><strong>User Type</strong></p>
                </th>
                <th style="width: 160px; text-align: center;">
                    <p style="margin-top: 10px;"><strong>Sys Admin</strong></p>
                </th>
                <th style="width: 142px; text-align: center;">
                    <p style="margin-top: 10px;"><strong>Risk Admin</strong></p>
                </th>
                <th style="width: 131px; text-align: center;">
                    <p style="margin-top: 10px;"><strong>Processor Admin</strong></p>
                </th>
                <th style="width: 122px; text-align: center;">
                    <p style="margin-top: 10px;"><strong>ARM</strong></p>
                </th>
                <th style="width: 124px; text-align: center;">
                    <p style="margin-top: 10px;"><strong>Dist</strong></p>
                </th>
                <th style="width: 136px; text-align: center;">
                    <p style="margin-top: 10px;"><strong>Borrower</strong></p>
                </th>
            </tr>
            <tr class="firstbgcolor">
                <td style="width: 594px">
                    <p><strong>List (web site and app)</strong></p>
                </td>
                <td style="width: 160px">&nbsp;</td>
                <td style="width: 142px">&nbsp;</td>
                <td style="width: 131px">&nbsp;</td>
                <td style="width: 122px">&nbsp;</td>
                <td style="width: 124px">&nbsp;</td>
                <td style="width: 136px">&nbsp;</td>
            </tr>
            <tr class="subcolor">
                <td style="width: 594px">
                    <p class="margins">My Preferences Farmer mapped / protected</p>
                </td>
                <td style="width: 160px">&nbsp;</td>
                <td style="width: 142px">&nbsp;</td>
                <td style="width: 131px">&nbsp;</td>
                <td style="width: 122px">&nbsp;</td>
                <td style="width: 124px">&nbsp;</td>
                <td style="width: 136px">
                    <input type="checkbox" class="form-check-input" />
                </td>
            </tr>
            <tr class="subcolor">
                <td style="width: 594px">
                    <p class="margins">My Preferences Dist mapped / protected</p>
                </td>
                <td style="width: 160px">&nbsp;</td>
                <td style="width: 142px">&nbsp;</td>
                <td style="width: 131px">&nbsp;</td>
                <td style="width: 122px">&nbsp;</td>
                <td style="width: 124px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 136px">&nbsp;</td>
            </tr>
            <tr class="firstbgcolor">
                <td style="width: 594px">
                    <p><strong>Dashboard</strong></p>
                </td>
                <td style="width: 160px">&nbsp;</td>
                <td style="width: 142px">&nbsp;</td>
                <td style="width: 131px">&nbsp;</td>
                <td style="width: 122px">&nbsp;</td>
                <td style="width: 124px">&nbsp;</td>
                <td style="width: 136px">&nbsp;</td>
            </tr>
            <tr class="subcolor">
                <td style="width: 594px">
                    <p class="margins">Dashboard Set 1</p>
                </td>
                <td style="width: 160px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 142px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 131px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 122px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 124px">&nbsp;</td>
                <td style="width: 136px">&nbsp;</td>
            </tr>
            <tr class="subcolor">
                <td style="width: 594px">
                    <p class="margins">Dashboard Set 2</p>
                </td>
                <td style="width: 160px">&nbsp;</td>
                <td style="width: 142px">&nbsp;</td>
                <td style="width: 131px">&nbsp;</td>
                <td style="width: 122px">&nbsp;</td>
                <td style="width: 124px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 136px">
                    <input type="checkbox" class="form-check-input" />
                </td>
            </tr>
            <tr class="firstbgcolor">
                <td style="width: 594px">
                    <p><strong>Tabs (web site and app)</strong></p>
                </td>
                <td style="width: 160px">&nbsp;</td>
                <td style="width: 142px">&nbsp;</td>
                <td style="width: 131px">&nbsp;</td>
                <td style="width: 122px">&nbsp;</td>
                <td style="width: 124px">&nbsp;</td>
                <td style="width: 136px">&nbsp;</td>
            </tr>
            <tr class="subcolor">
                <td style="width: 594px">
                    <p class="margins">Schematic Basic</p>
                </td>
                <td style="width: 160px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 142px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 131px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 122px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 124px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 136px">
                    <input type="checkbox" class="form-check-input" />
                </td>
            </tr>
            <tr class="subtabcolor">
                <td style="width: 594px">
                    <p class="marginsin">Schematic Detailed</p>
                </td>
                <td style="width: 160px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 142px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 131px">&nbsp;</td>
                <td style="width: 122px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 124px">&nbsp;</td>
                <td style="width: 136px">&nbsp;</td>
            </tr>
            <tr class="subcolor">
                <td style="width: 594px">
                    <p class="margins">Reports</p>
                </td>
                <td style="width: 160px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 142px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 131px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 122px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 124px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 136px">
                    <input type="checkbox" class="form-check-input" />
                </td>
            </tr>
            <tr class="subtabcolor">
                <td style="width: 594px">
                    <p class="marginsin">Summary</p>
                </td>
                <td style="width: 160px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 142px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 131px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 122px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 124px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 136px">&nbsp;</td>
            </tr>
            <tr class="subtabcolor">
                <td style="width: 594px">
                    <p class="marginsin">Cross Collateral</p>
                </td>
                <td style="width: 160px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 142px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 131px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 122px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 124px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 136px">&nbsp;</td>
            </tr>
            <tr class="subtabcolor">
                <td style="width: 594px">
                    <p class="marginsin">Borrower Summary</p>
                </td>
                <td style="width: 160px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 142px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 131px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 122px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 124px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 136px">
                    <input type="checkbox" class="form-check-input" />
                </td>
            </tr>
            <tr class="subtabcolor">
                <td style="width: 594px">
                    <p class="marginsin">Loan Inventory</p>
                </td>
                <td style="width: 160px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 142px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 131px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 122px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 124px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 136px">
                    <input type="checkbox" class="form-check-input" />
                </td>
            </tr>
            <tr class="subtabcolor">
                <td style="width: 594px">
                    <p class="marginsin">Balances</p>
                </td>
                <td style="width: 160px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 142px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 131px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 122px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 124px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 136px">
                    <input type="checkbox" class="form-check-input" />
                </td>
            </tr>
            <tr class="subtabcolor">
                <td style="width: 594px">
                    <p class="marginsin">Reconciliation</p>
                </td>
                <td style="width: 160px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 142px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 131px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 122px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 124px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 136px">&nbsp;</td>
            </tr>
            <tr class="subcolor">
                <td style="width: 594px">
                    <p class="margins">Borrower</p>
                </td>
                <td style="width: 160px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 142px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 131px">&nbsp;</td>
                <td style="width: 122px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 124px">&nbsp;</td>
                <td style="width: 136px">&nbsp;</td>
            </tr>
            <tr class="subcolor">
                <td style="width: 594px">
                    <p class="margins">Crop</p>
                </td>
                <td style="width: 160px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 142px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 131px">&nbsp;</td>
                <td style="width: 122px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 124px">&nbsp;</td>
                <td style="width: 136px">&nbsp;</td>
            </tr>
            <tr class="subcolor">
                <td style="width: 594px">
                    <p class="margins">Farm</p>
                </td>
                <td style="width: 160px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 142px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 131px">&nbsp;</td>
                <td style="width: 122px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 124px">&nbsp;</td>
                <td style="width: 136px">&nbsp;</td>
            </tr>
            <tr class="subcolor">
                <td style="width: 594px">
                    <p class="margins">Insurance</p>
                </td>
                <td style="width: 160px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 142px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 131px">&nbsp;</td>
                <td style="width: 122px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 124px">&nbsp;</td>
                <td style="width: 136px">&nbsp;</td>
            </tr>
            <tr class="subcolor">
                <td style="width: 594px">
                    <p class="margins">Budget</p>
                </td>
                <td style="width: 160px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 142px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 131px">&nbsp;</td>
                <td style="width: 122px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 124px">&nbsp;</td>
                <td style="width: 136px">&nbsp;</td>
            </tr>
            <tr class="subcolor">
                <td style="width: 594px">
                    <p class="margins">Optimizer</p>
                </td>
                <td style="width: 160px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 142px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 131px">&nbsp;</td>
                <td style="width: 122px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 124px">&nbsp;</td>
                <td style="width: 136px">&nbsp;</td>
            </tr>
            <tr class="subcolor">
                <td style="width: 594px">
                    <p class="margins">Collateral</p>
                </td>
                <td style="width: 160px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 142px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 131px">&nbsp;</td>
                <td style="width: 122px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 124px">&nbsp;</td>
                <td style="width: 136px">&nbsp;</td>
            </tr>
            <tr class="subcolor">
                <td style="width: 594px">
                    <p class="margins">Committee</p>
                </td>
                <td style="width: 160px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 142px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 131px">&nbsp;</td>
                <td style="width: 122px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 124px">&nbsp;</td>
                <td style="width: 136px">&nbsp;</td>
            </tr>
            <tr class="subcolor">
                <td style="width: 594px">
                    <p class="margins">Checkout</p>
                </td>
                <td style="width: 160px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 142px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 131px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 122px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 124px">&nbsp;</td>
                <td style="width: 136px">&nbsp;</td>
            </tr>
            <tr class="subcolor">
                <td style="width: 594px">
                    <p class="margins">Disburse</p>
                </td>
                <td style="width: 160px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 142px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 131px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 122px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 124px">&nbsp;</td>
                <td style="width: 136px">
                    <input type="checkbox" class="form-check-input" />
                </td>
            </tr>
            <tr class="subcolor">
                <td style="width: 594px">
                    <p class="margins">Reconcile</p>
                </td>
                <td style="width: 160px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 142px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 131px">&nbsp;</td>
                <td style="width: 122px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 124px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 136px">&nbsp;</td>
            </tr>
            <tr class="firstbgcolor">
                <td style="width: 594px">
                    <p><strong>Pending Action (web site and app)</strong></p>
                </td>
                <td style="width: 160px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 142px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 131px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 122px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 124px">&nbsp;</td>
                <td style="width: 136px">&nbsp;</td>
            </tr>
            <tr class="firstbgcolor">
                <td style="width: 594px">
                    <p><strong>My Preferences (web site and app)</strong></p>
                </td>
                <td style="width: 160px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 142px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 131px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 122px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 124px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 136px">
                    <input type="checkbox" class="form-check-input" />
                </td>
            </tr>
            <tr class="firstbgcolor">
                <td style="width: 594px">
                    <p><strong>Balances (web site and app)</strong></p>
                </td>
                <td style="width: 160px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 142px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 131px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 122px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 124px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 136px">
                    <input type="checkbox" class="form-check-input" />
                </td>
            </tr>
            <tr class="firstbgcolor">
                <td style="width: 594px">
                    <p><strong>Decide (web site and app)</strong></p>
                </td>
                <td style="width: 160px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 142px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 131px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 122px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 124px">&nbsp;</td>
                <td style="width: 136px">&nbsp;</td>
            </tr>
            <tr class="firstbgcolor">
                <td style="width: 594px">
                    <p><strong>Developer Admin Menu Choices</strong></p>
                </td>
                <td style="width: 160px">&nbsp;</td>
                <td style="width: 142px">&nbsp;</td>
                <td style="width: 131px">&nbsp;</td>
                <td style="width: 122px">&nbsp;</td>
                <td style="width: 124px">&nbsp;</td>
                <td style="width: 136px">&nbsp;</td>
            </tr>
            <tr class="firstbgcolor">
                <td style="width: 594px">
                    <p><strong>System Admin Menu Choices</strong></p>
                </td>
                <td style="width: 160px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 142px">&nbsp;</td>
                <td style="width: 131px">&nbsp;</td>
                <td style="width: 122px">&nbsp;</td>
                <td style="width: 124px">&nbsp;</td>
                <td style="width: 136px">&nbsp;</td>
            </tr>
            <tr class="firstbgcolor">
                <td style="width: 594px">
                    <p><strong>Risk Admin Menu Choices</strong></p>
                </td>
                <td style="width: 160px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 142px">
                    <input type="checkbox" class="form-check-input" />
                </td>
                <td style="width: 131px">&nbsp;</td>
                <td style="width: 122px">&nbsp;</td>
                <td style="width: 124px">&nbsp;</td>
                <td style="width: 136px">&nbsp;</td>
            </tr>
        </tbody>
    </table>

    <script>
        $('#navbtnadd').addClass('disabled');
        $('#navbtnsave').addClass('disabled');
        $('#navbtndownload').addClass('disabled');
        $('#navbtnrefresh').addClass('disabled');
        $('#navbtncolumns').addClass('disabled');

        $('#txtTableName').html('User Type Access');
        //$('#txtLastUpdated').html(data.Last_Updated);
        //$('#txtRecordsCount').html(data.Records_Count);
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" runat="Server">
</asp:Content>

