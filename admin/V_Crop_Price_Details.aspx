﻿<%@ Page Title="Crop Key" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="V_Crop_Price_Details.aspx.cs" Inherits="V_Crop_Price_Details" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .ui-jqgrid .ui-jqgrid-htable th div {
            height: auto;
            overflow: hidden;
            padding-right: 4px;
            padding-top: 2px;
            position: relative;
            vertical-align: text-top;
            white-space: normal !important;
        }

        #content {
            padding: 14px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="ref-crop-key reference-table">
        <div class="row switch-controls">
            <div class="switch-control disabled" id="editControl">
                <label for="editBtn">Edit</label>
                <div class="toggle-btn small">            
                    <input type="checkbox" id="editBtn" class="cb-value" />
                    <span class="round-btn"></span>
                </div>
            </div>
            <div class="switch-control disabled" id="enableControl">
                <label for="enableBtn">Enable</label>
                <div class="toggle-btn small">            
                    <input type="checkbox" id="enableBtn" class="cb-value" />
                    <span class="round-btn"></span>
                </div>
            </div>
        </div>
        <div class="loader"></div>
        <div class="row main-content" style="position: relative">
            <div id="truncateTable" style="position: absolute; top: -25px; display: none">
                <a href="javascript:truncatetable()" class='glyphicon glyphicon-trash' style='color:red'></a>
            </div>
            <div>
                <table id="jqGrid"></table>
                <div id="jqGridPager"></div>
            </div>
        </div>
        <br />
        <br />
        <br />
        <br />
    </div>
    <script type="text/javascript">  
        var DeleteRows = [];
        var timer;
        var lastSelection;
        var canEdit = false;

        $(function () {

            var obj = {
                Init: function () {
                    $('.loader').show();

                    obj.bindgrid();

                    $("#navbtnadd").click(function () {
                        obj.add();
                    });

                    $("#navbtndownload").click(function () {
                        $("#jqGrid").jqGrid("exportToExcel", {
                            includeLabels: true,
                            includeGroupHeader: true,
                            includeFooter: true,
                            fileName: "Crop Key.xlsx",
                            maxlength: 40 // maxlength for visible string data 
                        })
                    });

                    $("#navbtnrefresh").click(function () {
                        $('.loader').show();
                        obj.bindgrid();
                        $('.loader').hide();
                    });

                    $('#navbtncolumns').click(function () {
                        $("#jqGrid").columnChooser({});
                    });

                    $("#navbtnsave").click(function () {
                        $('.loader').show();
                        obj.save();
                        $('.loader').hide();
                    });

                    $("#txtSearchBar").on("keyup", function () {
                        var self = this;
                        obj.search(self);
                    });

                    $('#editBtn').change(function () {
                        canEdit = $(this).is(':checked');
                        if (!canEdit) {
                            if (lastSelection != null) {
                                var grid = $("#jqGrid");
                                grid.jqGrid('saveRow', lastSelection);
                                grid.jqGrid('restoreRow', lastSelection);
                                lastSelection = null;
                            }
                            $('#navbtnupload').addClass('disabled');
                        } else {
                            $('#navbtnupload').removeClass('disabled');
                        }
                    });
                },
                getTableInfo: function () {
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "V_Crop_Price_Details.aspx/GetTableInfo",
                        data: JSON.stringify({  }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            var data = JSON.parse(res.d);

                            var table_info = data.Table_Info[0];
                            $('#txtTableName').html(table_info.Table_Name);
                            $('#txtLastUpdated').html(table_info.Last_Updated);
                            $('#txtRecordsCount').html(table_info.Records_Count);

                            if (data.CanEdit) {
                                $('#editControl').removeClass('disabled');
                            } else {
                                $('#editControl').addClass('disabled');
                            }

                            $('#enableControl').addClass('disabled');
                            
                        }
                    });
                },
                bindgrid: function () {
                    $.ajax({
                        type: "POST",
                        url: "V_Crop_Price_Details.aspx/BindVCropPriceDetails",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var resData = JSON.parse(data.d);
                            $('.loader').show();
                            for (var i = 0; i < resData.length; i++) {
                                resData[i].rowid = i + 1;
                            }
                            obj.loadgrid(resData);
                            obj.getTableInfo();
                            $('.loader').hide();
                        }
                    });
                },
                loadgrid: function (data) {
                    $("#jqGrid").jqGrid({
                        datatype: "local",
                        styleUI: 'Bootstrap',
                        data: data,
                        colModel: [
                            { label: 'rowid', name: 'rowid', width: 70, editable: false, key: true, hidden: true, hidedlg: true  },
                            { label: 'Crop ID', name: 'Crop_And_Practice_ID', align: 'center', width: 90, editable: false, sorttype: 'number' },
                            { label: 'Crop Code', name: 'Crop_Code', width: 120, align: 'center', editable: true },
                            { label: 'Crop Name', name: 'Crop_Name', width: 120, editable: true },
                            { label: 'Crop Type', name: 'Crop_Type_Code', width: 180, align: 'left', classes: 'left', editable: true },
                            { label: 'Irr Prac', name: 'Irr_Prac_Code', width: 180, align: 'center', classes: 'center', editable: true },
                            { label: 'Crop Prac', name: 'Crop_Prac_Code', width: 180, align: 'left', classes: 'left', editable: true },
                            { label: 'Crop Full Key', name: 'Crop_Full_Key', align: 'left', classes: 'left', width: 300, editable: true },
                            {
                                label: 'Status', name: 'Status', width: 90, align: 'center', editable: true, editoptions: {
                                    maxlength: 1,
                                    dataEvents: [
                                        {
                                            type: 'keypress',
                                            fn: function (e) {
                                                return NumberOnly(e, this, false);
                                            }
                                        }
                                    ]
                                }
                            },
                            { label: 'IsDelete', name: 'IsDelete', width: 70, editable: true, hidden: true, hidedlg: true  },
                            { label: 'Actionstatus', name: 'Actionstatus', width: 70, editable: false, hidden: true, hidedlg: true  },
                            { label: '', name: '', width: 60, align: 'center', formatter: obj.deleteLink, exportcol: false, hidedlg: true },
                        ],
                        viewrecords: true,
                        loadonce: true,
                        restoreAfterSelect: false,
                        saveAfterSelect: true,
                        sortable: true,
                        onSelectRow: obj.edit,
                        onPaging: obj.changePage,
                        height: 'auto',
                        rowNum: 100,
                        pager: "#jqGridPager",
                        loadComplete: function () {
                            obj.showDeleteButton();
                        }
                    });
                },
                showDeleteButton: function () {
                    var width = $('#gbox_jqGrid').width();
                    $('#truncateTable').css('left', width + 17 + 'px');
                    $('#truncateTable').show();
                },
                add: function () {

                    var grid = $("#jqGrid");

                    var rowsperPage = parseInt(grid.getGridParam('rowNum'));
                    var gridlength = grid.jqGrid('getGridParam', 'data').length;
                    var curpage = parseInt($(".ui-pg-input").val());
                    var totPages = Math.ceil(gridlength / rowsperPage);

                    if (rowsperPage * totPages == gridlength) {
                        var id = $('.inline-edit-cell').parent().parent().prop('id');
                        grid.jqGrid('saveRow', id);
                        var row = obj.newrow();
                        var newRowId = row.rowid;
                        grid.jqGrid('addRowData', newRowId, row, "first");
                        grid.trigger('reloadGrid');
                        lastSelection = newRowId;
                        grid.jqGrid('saveRow', lastSelection);
                        grid.jqGrid('restoreRow', lastSelection);
                        $('.glyphicon-step-forward').trigger('click');

                    } else {

                        $('.glyphicon-step-forward').trigger('click');
                        var row = obj.newrow();
                        var newRowId = row.rowid;
                        grid.jqGrid('addRowData', newRowId, row, "first");
                    }


                    lastSelection = newRowId;
                    grid.jqGrid('saveRow', lastSelection);
                    grid.jqGrid('restoreRow', lastSelection);

                    var eid = $('.inline-edit-cell').parent().parent().prop('id')
                    grid.jqGrid('saveRow', eid);
                    grid.jqGrid('restoreRow', eid);
                    grid.jqGrid('editRow', newRowId);
                },
                edit: function (id) { 

                    if (id && id !== lastSelection && canEdit) {
                        var grid = $("#jqGrid");

                        grid.jqGrid('saveRow', lastSelection);
                        grid.jqGrid('restoreRow', lastSelection);
                        var row = grid.jqGrid('getRowData', lastSelection);

                        if (!jQuery.isEmptyObject(row)) {
                            var dataobj = jQuery("#jqGrid").jqGrid('getGridParam', 'data');
                            index = dataobj.findIndex(x => x.rowid == row.rowid);
                            if (row.rowid > 0 && row.Actionstatus != 1) {
                                row.Actionstatus = 2;
                                grid.jqGrid('getGridParam', 'data')[index] = row;
                            }
                        }

                        grid.jqGrid('editRow', id);
                        lastSelection = id;
                    }
                },
                newrow: function () {
                    var newid = 0;

                    var grid = $("#jqGrid");
                    var length = jQuery("#jqGrid").jqGrid('getGridParam', 'records');
                    var dataobj = grid.jqGrid('getGridParam', 'data');
                    var dataobj = grid.jqGrid('getGridParam', 'data');
                    var Crop_And_Practice_ID = [];
                    Crop_And_Practice_IDs = dataobj.map(function (e) { return e.rowid });
                    if (Crop_And_Practice_IDs.length > 0) {
                        newid = Crop_And_Practice_IDs.reduce(function (a, b) { return Math.max(a, b); });
                    }

                    var row = {
                        Actionstatus: 1,
                        rowid: newid + 1,
                        Crop_And_Practice_ID: 0,
                        Crop_Code: "",
                        Crop_Name: "",
                        Crop_Type_Code: "",

                        Irr_Prac_Code: "",
                        Crop_Prac_Code: "",

                        Crop_Full_Key: "",
                        Irr_NI_Ind: "",
                        Status: ""

                    };
                    return row;
                },
                save: function () {

                    $('.loader').show();
                    var grid = $("#jqGrid");
                    var allrows = [];


                    var id = $('.inline-edit-cell').parent().parent().prop('id');
                    grid.jqGrid('saveRow', id);

                    var rowData = grid.jqGrid('getRowData', id);
                    if (rowData.rowid > 0) {
                        rowData.Actionstatus = 2;
                        grid.jqGrid('getGridParam', 'data')[id] = rowData;
                    }

                    for (var i = 0; i < DeleteRows.length; i++) {
                        DeleteRows[i].Actionstatus = 3
                        allrows.push(DeleteRows[i]);
                    }
                    var dataobj = grid.jqGrid('getGridParam', 'data');
                    var newRows = $.grep(dataobj, function (e) {
                        if (e != undefined) {
                            return e.Actionstatus == 1 || e.Actionstatus == 2
                        }
                    });
                    for (var i = 0; i < newRows.length; i++) {
                        allrows.push(newRows[i]);
                    }

                    $.ajax({
                        type: "POST",
                        url: "V_Crop_Price_Details.aspx/SaveVCropPriceDetails",
                        data: JSON.stringify({ lst: allrows }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            toastr.success("Saved Sucessful");
                            if (timer) { clearTimeout(timer); }
                            timer = setTimeout(function () {
                                location.reload();
                            }, 2000);
                        },
                        failure: function (response) {
                            $('.loader').hide();
                            var val = response.d;
                            toastr.warning(val);
                        }
                    });
                },
                search: function (self) {
                    if (timer) { clearTimeout(timer); }
                    timer = setTimeout(function () {
                        obj.changePage();
                        $("#jqGrid").jqGrid('filterInput', self.value);
                        var length = jQuery("#jqGrid").jqGrid('getGridParam', 'records');
                        $('#txtRecordsCount').html('Records Count: ' + length);
                    }, 0);
                },
                changePage: function () {

                    lastSelection = "";
                    var grid = $("#jqGrid");

                    var id = $('.inline-edit-cell').parent().parent().prop('id');
                    if (id != undefined) {


                        grid.jqGrid('saveRow', id);
                        grid.jqGrid('restoreRow', id);
                        var row = grid.jqGrid('getRowData', id);
                        if (!jQuery.isEmptyObject(row)) {
                            var dataobj = jQuery("#jqGrid").jqGrid('getGridParam', 'data');
                            index = dataobj.findIndex(x => x.rowid == row.rowid);
                            if (row.rowid > 0 && row.Actionstatus != 1) {
                                row.Actionstatus = 2;
                                grid.jqGrid('getGridParam', 'data')[index] = row;
                            }
                        }
                    }
                },

                deleteLink: function (cellValue, options, rowdata, action) {
                    return "<a href='javascript:deleteRecord(" + options.rowId + ")' class='glyphicon glyphicon-trash' style='color:red'></a>";
                }
            }
            obj.Init();
        });
        function deleteRecord(id) {

            if (canEdit) {
                var grid = $('#jqGrid');
                var result = confirm("Are you sure you Want to delete?");

                if (result == true) {
                    var ediId = $('.inline-edit-cell').parent().parent().prop('id');
                    grid.jqGrid('saveRow', ediId);
                    var rowData = grid.jqGrid('getRowData', ediId);
                    if (rowData.rowid > 0 && rowData.Actionstatus != 1) {
                        rowData.Actionstatus = 2;
                        grid.jqGrid('getGridParam', 'data')[ediId - 1] = rowData;
                    }

                    var row = grid.jqGrid('getRowData', id);
                    grid.jqGrid('delRowData', id);
                    if (row.Actionstatus != 1) {
                        DeleteRows.push(row);
                    }
                    var data = jQuery("#jqGrid").jqGrid('getGridParam', 'data');
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].Crop_And_Practice_ID == 0) {
                            data[i].rowid = i + 1;
                            data[i].id = i + 1;
                        } else
                            data[i].rowid = i + 1;
                    }
                    var curpage = parseInt($(".ui-pg-input").val());


                    jQuery('#jqGrid').jqGrid('clearGridData');
                    jQuery('#jqGrid').jqGrid('setGridParam', { data: data });
                    $("#jqGrid").trigger("reloadGrid", [{ page: curpage }]);
                    $("#jqGrid").trigger("reloadGrid", [{ page: curpage }]);
                    lastSelection = id;
                }
            }
        }

        function unique(list) {
            var result = [];
            $.each(list, function (i, e) {
                if ($.inArray(e, result) == -1) result.push(e);
            });
            return result;
        }

        function Upload_Excel_Data(data) {
            if (canEdit) {
                $.ajax({
                    type: "POST",
                    url: "V_Crop_Price_Details.aspx/UploadExcel",
                    data: JSON.stringify({ list: data }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (res) {
                        toastr.success("Data Uploaded Sucessfully");
                        if (timer) { clearTimeout(timer); }
                        timer = setTimeout(function () {
                            location.reload();
                        }, 1000);
                    },
                    failure: function (response) {
                        $('.loader').hide();
                        var val = response.d;
                        toastr.warning(val);
                    }
                });
            }
        }

        function truncatetable() {
            if (canEdit) {
                var result = confirm('Are you sure you want to delete all record?');
                if (result) {
                    $('.loader').show();
                    $.ajax({
                        type: "POST",
                        url: "V_Crop_Price_Details.aspx/TruncateTable",
                        data: JSON.stringify({}),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            toastr.success("Sucessful");
                            if (timer) { clearTimeout(timer); }
                            timer = setTimeout(function () {
                                location.reload();
                            }, 1000);
                        },
                        failure: function (response) {
                            $('.loader').hide();
                            var val = response.d;
                            toastr.warning(val);
                        }
                    });
                }
            }
        }

    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" runat="Server">
</asp:Content>

