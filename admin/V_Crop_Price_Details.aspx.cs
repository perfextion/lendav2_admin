﻿using System;
using System.Data;
using System.Web.Script.Services;
using System.Web.Services;

public partial class V_Crop_Price_Details : BasePage
{
    public V_Crop_Price_Details()
    {
        Table_Name = "V_Crop_Price_Details";
    }

    [WebMethod]
    public static string GetTableInfo()
    {
        return Get_Table_Info();
    }

    [WebMethod]
    public static string BindVCropPriceDetails()
    {
        //string sql_qry = "SELECT[Crop_And_Practice_ID] ,[Crop_Code],[Crop_Name],[Crop_Type_Code], [Irr_Prac_Code]  ,[Crop_Prac_Code],[Crop_Full_Key],[Irr_NI_Ind],[State_ID],[RMA_Ref_Yield],[Std_Crop_Type_Practice_Type_Ind],CONVERT(VARCHAR(10), Final_Planting_Date, 101) as Final_Planting_Date,[Region_ID],[Office_ID],[Crop_Year],[MPCI_UOM],[Price],[Status] FROM[dbo].[V_Crop_Price_Details]";
        string sql_qry = "  SELECT[Crop_And_Practice_ID] ,[Crop_Code],[Crop_Name],[Crop_Type_Code], [Irr_Prac_Code]," +
            "[Crop_Prac_Code],[Crop_Full_Key],[Irr_NI_Ind],[Status]   FROM[dbo].[V_Crop_Price_Details] order by crop_code";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
    }

    [WebMethod, ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static void SaveVCropPriceDetails(dynamic lst)
    {
        foreach (var item in lst)
        {
            string qry = "";
            int val = Convert.ToInt32(item["Actionstatus"]);
            if (val == 1)
            {
                qry = "INSERT INTO V_Crop_Price_Details " +
                    "(" +
                        "[Crop_Code]," +
                        "[Crop_Name]," +
                        "[Crop_Type_Code] ," +
                        "[Irr_Prac_Code]," +
                        "[Crop_Prac_Code] ," +
                        "[Crop_Full_Key],[Status]" +
                    ") " +
                    " VALUES" +
                    "(" +
                        "'" + ManageQuotes(item["Crop_Code"]) + "'," +
                        "'" + ManageQuotes(item["Crop_Name"]) + "'," +
                        "'" + ManageQuotes(item["Crop_Type_Code"]) + "'," +
                        "'" + ManageQuotes(item["Irr_Prac_Code"]) + "'," +
                        "'" + ManageQuotes(item["Crop_Prac_Code"]) + "'," +
                        "'" + ManageQuotes(item["Crop_Full_Key"]) + "'," +
                        "'" + ManageQuotes(item["Status"]) + "'" +
                    ")";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 2)
            {
                qry = "UPDATE V_Crop_Price_Details SET " +
                        "[Crop_Code] = '" + ManageQuotes(item["Crop_Code"]) + "' ," +
                        "[Crop_Name] = '" + ManageQuotes(item["Crop_Name"]) + "'," +
                        "[Crop_Type_Code] = '" + ManageQuotes(item["Crop_Type_Code"]) + "' ," +
                        "[Irr_Prac_Code] = '" + ManageQuotes(item["Irr_Prac_Code"]) + "' ," +
                        "[Crop_Prac_Code] = '" + ManageQuotes(item["Crop_Prac_Code"]) + "' ," +
                        "[Crop_Full_Key] = '" + ManageQuotes(item["Crop_Full_Key"]) + "' ," +
                        "[Status] = '" + (item["Status"]) + "' " +
                    "where Crop_And_Practice_ID=" + item["Crop_And_Practice_ID"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 3)
            {
                qry = "DELETE FROM V_Crop_Price_Details  where Crop_And_Practice_ID=" + item["Crop_And_Practice_ID"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
        }

        Update_Table_Audit();
    }

    [WebMethod]
    public static void TruncateTable()
    {
        string qry = "TRUNCATE TABLE V_Crop_Price_Details";
        gen_db_utils.gp_sql_execute(qry, dbKey);
        Update_Table_Audit();
    }

    [WebMethod]
    public static void UploadExcel(dynamic list)
    {
        string qry = string.Empty;

        foreach (var item in list)
        {
            var status = Get_Value(item, "Status");
            if (!string.IsNullOrEmpty(status))
            {
                var Crop_Name = TrimSQL(Get_Value(item, "Crop Name"));
                var Crop_Code = TrimSQL(Get_Value(item, "Crop Code"));

                string Crop_Type_Code = TrimSQL(Get_Value(item, "Crop Type"));
                if (!string.IsNullOrEmpty(Crop_Type_Code))
                {
                    Crop_Type_Code = Crop_Type_Code.Replace('_', '-');
                }
                

                string Irr_Prac_Code = Get_Value(item, "Irr Prac");
                if (!string.IsNullOrEmpty(Irr_Prac_Code))
                {
                    Irr_Prac_Code = Irr_Prac_Code.Replace('_', '-');
                }

                var Crop_Prac_Code = Get_Value(item, "Crop Prac");
                if (!string.IsNullOrEmpty(Crop_Prac_Code))
                {
                    Crop_Prac_Code = Crop_Prac_Code.Replace('_', '-');
                }


                string Crop_Full_Key = Crop_Code;

                Crop_Full_Key = Get_key(Crop_Full_Key, Crop_Type_Code);
                Crop_Full_Key = Get_key(Crop_Full_Key, Irr_Prac_Code);
                Crop_Full_Key = Get_key(Crop_Full_Key, Crop_Prac_Code);

                Crop_Full_Key = Crop_Full_Key.ToUpper();

                int Irr_NI_Ind = 0;

                if(Irr_Prac_Code.ToUpper() == "IRR")
                {
                    Irr_NI_Ind = 1;
                }

                var Status = Get_Value(item, "Status");

                qry += @"IF NOT EXISTS
                        (
                            SELECT 1 FROM [V_Crop_Price_Details] 
                            Where Crop_Full_Key = '" + Crop_Full_Key + @"'
                        )
                         BEGIN
                            INSERT INTO [dbo].[V_Crop_Price_Details]
                                (
                                     [Crop_Name]
                                    ,[Crop_Code]
                                    ,[Crop_Type_Code]
                                    ,[Irr_Prac_Code]
                                    ,[Crop_Prac_Code]
                                    ,[Crop_Full_Key]
                                    ,[Irr_NI_Ind]
                                    ,[Status]
                                )
                             VALUES
                               (
                                    '" + Crop_Name + @"'" +
                                   ",'" + Crop_Code + @"'" +
                                   ",'" + Crop_Type_Code + @"'" +
                                   ",'" + Irr_Prac_Code + @"'" +
                                   ",'" + Crop_Prac_Code + @"'" +
                                   ",'" + Crop_Full_Key + @"'" +
                                   ",'" + Irr_NI_Ind + @"'" +
                                   ",'" + Status + @"'
                                )
                         END";

                qry += Environment.NewLine;
            }
        }

        if (!string.IsNullOrEmpty(qry))
        {
            gen_db_utils.gp_sql_execute(qry, dbKey);
            Update_Table_Audit();
        }
    }

    public static string Get_key(string Full_Key, string Key)
    {
        if (string.IsNullOrEmpty(Key) || Key == "-")
        {
            Full_Key += "_0";
        }
        else
        {
            Full_Key += "_" + Key;
        }

        return Full_Key;
    }

    public static string ManageQuotes(string value)
    {
        if (!String.IsNullOrEmpty(value) && value.Contains("'"))
        {
            value = value.Replace("'", "''");
        }
        return value;
    }
}