﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class admin_Z_Brad_DB_Design : System.Web.UI.Page
{
    static string dbKey = "gp_conn";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["GetDetailsTableName"] != null)
        {
            GetDetailsTableName.Value = Session["GetDetailsTableName"].ToString();
        }
    }

    [WebMethod]
    public static string GetDetails(string TableName)
    {
        HttpContext.Current.Session["GetDetailsTableName"] = TableName;
        string sql_qry = "SELECT Id,FileSno,Type,Tab,TableName,ColumnName,PrimaryModule," +
            "ReferenceDatabase,PrimaryLogic,Notes,Sort_Order from Z_Brad_DB_Design where 0=0 ";
        if (!string.IsNullOrEmpty(TableName))
        {
            sql_qry += " and TableName='" + TableName + "'";
        }
        sql_qry += " order by Sort_Order asc ";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
    }
    [WebMethod]
    public static List<string> GetLocalObject()
    {
        string sql_qry = "SELECT distinct TableName FROM Z_Brad_DB_Design order by TableName";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);

        List<string> emp = new List<string>();
        emp = (from DataRow row in dt.Rows
               select row["TableName"].ToString()
               ).ToList();
        return emp;
    }

    [WebMethod]
    public static void SaveZBradDBDesign(dynamic lst)
    {
        foreach (var item in lst)
        {
            string qry = "";
            int val = Convert.ToInt32(item["Actionstatus"]);

            if (val == 2)
            {
                qry = "UPDATE Z_Brad_DB_Design SET " +
                      "[Type] = '" + ManageQuotes(item["Type"]) + "' ," +
                      "Tab = '" + ManageQuotes(item["Tab"]) + "' ," +
                      "TableName = '" + ManageQuotes(item["TableName"]) + "' ," +
                      "ColumnName = '" + ManageQuotes(item["ColumnName"]) + "' ," +
                      "PrimaryModule = '" + ManageQuotes(item["PrimaryModule"]) + "' ," +
                      "ReferenceDatabase = '" + ManageQuotes(item["ReferenceDatabase"]) + "' ," +
                      "PrimaryLogic = '" + ManageQuotes(item["PrimaryLogic"]) + "' ," +
                      "Notes = '" + ManageQuotes(item["Notes"]) + "' , " +
                      "Sort_Order = '" + ManageQuotes(item["Sort_Order"]) + "'" +
                      "where Id=" + item["Id"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
        }
    }


    public static string ManageQuotes(string value)
    {
        if (value != null && value.Contains("'"))
        {
            value = value.Replace("'", "''");
        }
        return value;
    }
}