﻿<%@ Page Title="" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="Z_Doc_Index.aspx.cs" Inherits="admin_Z_Doc_Index" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="http://code.jquery.com/ui/1.9.1/themes/base/jquery-ui.css" rel="stylesheet" />
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery.fancytree/2.9.0/skin-awesome/ui.fancytree.min.css" rel="stylesheet" />
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" />

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.fancytree/2.9.0/jquery.fancytree-all.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/js/materialize.min.js">
    </script>


    <style>
        .chip {
            display: inline-block;
            padding: 0 25px;
            height: 30px;
            font-size: 16px;
            line-height: 30px;
            border-radius: 25px;
            background-color: #f1f1f1;
        }

            .chip img {
                float: left;
                margin: 0 10px 0 -25px;
                height: 30px;
                width: 30px;
                border-radius: 50%;
            }

        .closebtn {
            padding-left: 10px;
            padding-top: 5px;
            color: #888;
            font-weight: normal;
            float: right;
            font-size: 5px;
            cursor: pointer;
        }

            .closebtn:hover {
                color: #000;
            }
    </style>

    <style>
        .fancytree-node {
            display: flex;
            align-items: center;
        }

        span.fancytree-expander,
        span.fancytree-icon,
        span.fancytree-title {
            line-height: 1em;
        }

        span.fancytree-expander {
            text-align: center;
            padding: 2px 0;
        }

        span.fancytree-icon {
            margin-left: 10px;
            padding: 2px 0;
        }

        span.fancytree-title {
            margin-left: 2px;
            padding: 2px;
            box-sizing: border-box;
            border: 1px solid transparent;
        }

        .fancytree-focused span.fancytree-title {
            border: 1px solid #999;
        }

        .fancytree-active span.fancytree-title {
            background-color: #ddd;
        }

        ul.fancytree-container ul {
            padding: 2px 0 0 25px;
        }

        ul.fancytree-container li {
            padding: 2px 0;
        }
    </style>
    <script>
        $(document).ready(function () {
            $('table').addClass('table table-striped');
        });
    </script>
    <div class="loader"></div>
    <div class="row">
        <div id="tree" style="float: left" class="col-md-3">
        </div>
        <div id="dvcontent" class="col-md-9">
        </div>

    </div>

    <script>    
        $('#ContentPlaceHolder1_btnSearch').hide();
        $.ajax({
            type: "POST",
            url: "Z_Doc_Index.aspx/GetSections",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                $('.loader').show();
                var res = getJsonData(JSON.parse(data.d));
                LoadTree(res);
                $('.loader').hide();
            }
        });
        function LoadTree(res) {
            $("#tree").fancytree({
                aria: true, // Enable WAI-ARIA support..
                source: (res),
                autoActivate: false, // Automatically activate a node when it is focused (using keys).
                autoCollapse: false, // Automatically collapse all siblings, when a node is expanded.
                autoScroll: true, // Automatically scroll nodes into visible area.
                focusOnSelect: true, // Set focus when node is checked by a mouse click
                keyboard: true, // Support keyboard navigation.
                tabbable: true, // Whole tree behaves as one single control
                extensions: ["glyph"],
                glyph: {
                    map: {
                        doc: "fa fa-file-o fa-lg",
                        docOpen: "fa fa-file-o fa-lg",
                        error: "fa fa-warning",
                        expanderClosed: "fa fa-chevron-right",
                        expanderLazy: "fa fa-angle-right",
                        expanderOpen: "fa fa-chevron-down",
                        folder: "fa fa-folder fa-lg",
                        folderOpen: "fa fa-folder-open fa-lg",
                        loading: "fa fa-spinner fa-pulse"
                    }
                },
                activate: function (event, data) {
                    // $("#dvcontent").text(data.node.title);
                    $('#ContentPlaceHolder1_hdnid').val(data.node.data.id)
                    // $('#ContentPlaceHolder1_btnSearch').trigger('click');

                    $.ajax({
                        type: "POST",
                        url: "Z_Doc_Index.aspx/GetDocumentsDetails",
                        data: JSON.stringify({ id: data.node.data.id }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            $("#dvcontent").html(data.d);
                        },
                        failure: function (response) {

                        }

                    });

                    ////              alert(node.getKeyPath());
                    //if (data.node.url)
                    //    window.open(data.node.url, data.node.target);
                },
                deactivate: function (event, data) {
                    
                    //$("#echoSelected").text("-");
                },

            });
            $("#tree").fancytree("getTree").visit(function (node) {
                node.setExpanded();
            });
        }

        function getJsonData(dataobj) {
            var ParentData = $.grep(dataobj, function (e) {
                if (e != undefined) {
                    return e.ParentId == 0
                }
            });
            var res = [];
            loadTreeRecursive(dataobj, ParentData, res);
            return res;
        }


        function loadTreeRecursive(dataobj, ParentData, res) {

            for (var i = 0; i < ParentData.length; i++) {
                var ChildData = $.grep(dataobj, function (e) {
                    if (e != undefined) {
                        return e.ParentId == ParentData[i].Id
                    }
                });
                var childens = [];
                loadTreeRecursive(dataobj, ChildData, childens);
                res.push({
                    title: ParentData[i].Title,
                    folder: (childens.length > 0) ? true : false,
                    id: ParentData[i].Id,
                    children: childens
                });
            }
        }
    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" runat="Server">
</asp:Content>

