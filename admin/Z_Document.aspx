﻿<%@ Page Title="" Language="C#" MasterPageFile="~/gp_Master_document.master" AutoEventWireup="true" CodeFile="Z_Document.aspx.cs" Inherits="admin_Z_Document" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">

    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/js/materialize.min.js">
      </script>
    

    <style>
        .chip {
            display: inline-block;
            padding: 0 25px;
            height: 30px;
            font-size: 16px;
            line-height: 30px;
            border-radius: 25px;
            background-color: #f1f1f1;
        }

            .chip img {
                float: left;
                margin: 0 10px 0 -25px;
                height: 30px;
                width: 30px;
                border-radius: 50%;
            }

        .closebtn {
            padding-left: 10px;
            padding-top: 5px;
            color: #888;
            font-weight: normal;
            float: right;
            font-size: 5px;
            cursor: pointer;
        }

            .closebtn:hover {
                color: #000;
            }
    </style>
    <script>
        $(document).ready(function () {
            
            $('table').addClass('table table-striped');
        });
    </script>
    <div class="">
        <div class="row">
            <div class="form-inline">
                <div class="col-md-4">
                </div>
                <div class="col-md-6 text-align-right">
                    
                    <div class="form-group">
                        <label for="txtSearch">Enter :</label>
                        <asp:TextBox runat="server" CssClass="form-control" ID="txtSearch" Width="200px" />
                    </div>
                    <div class="form-group">
                        <asp:Button ID="btnSearch" CssClass="btn" OnClick="btnSearch_Click" Text="Search" runat="server" />
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-2">
                <nav id="toc" data-spy="affix" data-toggle="toc"></nav>
            </div>
            <div class="col-md-10">               
                <asp:Label ID="lblContent" Text="" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>

