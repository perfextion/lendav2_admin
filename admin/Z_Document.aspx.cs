﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_Z_Document : System.Web.UI.Page
{
    string dbKey = "gp_conn";


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            GetDocumentsSearch("Keyword", "Core");
        }
    }
    public void GetDocumentsDetails()
    {
        string sql_qry = "select * from z_documents ";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        BindDocumentDetails(dt);
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        lblContent.Text = "";
        GetDocumentsSearch("Keyword", txtSearch.Text);
    }
    public void GetDocumentsSearch(string colName, string serchText)
    {
        if (serchText.Contains("'"))
        {
            serchText = serchText.Replace("'", "''");
        }
        string sql_qry = "";
        if (!String.IsNullOrEmpty(serchText))
            if (String.IsNullOrEmpty(colName))
                sql_qry = "SELECT Z_Doc_ID ,Page_Name,Tab_Name,Doc_Body,Lastmod_at,Lastmod_by,status,Key_Fields,Title,attach_to_id,id,ParentId,ParentTitle  FROM Z_Documents zd  left join ( SELECT T1.id,T1.Title,T1.ParentId,T2.TITLE as ParentTitle FROM Z_DOC_INDEX T1, Z_DOC_INDEX T2 where T1.ParentId=T2.id ) zi on  zd.attach_to_id = zi.Id   where Page_Name like '%" + serchText + "%' or Tab_Name like '%" + serchText + "%' or Doc_Body like '%" + serchText + "%'  order by Lastmod_at desc ";
            else
            {
                if (colName == "Keyword")
                {
                    //string srchTxt = "{\"title\":\"" + serchText + "\"}";
                    string srchTxt = "{\"title\":\"" + serchText;
                    sql_qry = "SELECT Z_Doc_ID ,Page_Name,Tab_Name,Doc_Body,Lastmod_at,Lastmod_by,status,Key_Fields,Title,attach_to_id,id,ParentId,ParentTitle  FROM Z_Documents zd  left join ( SELECT T1.id,T1.Title,T1.ParentId,T2.TITLE as ParentTitle FROM Z_DOC_INDEX T1, Z_DOC_INDEX T2 where T1.ParentId=T2.id ) zi on  zd.attach_to_id = zi.Id  where Key_Fields like '%" + srchTxt + "%'  order by Lastmod_at desc  ";
                }
                else
                    sql_qry = "SELECT Z_Doc_ID ,Page_Name,Tab_Name,Doc_Body,Lastmod_at,Lastmod_by,status,Key_Fields,Title,attach_to_id,id,ParentId,ParentTitle  FROM Z_Documents zd  left join ( SELECT T1.id,T1.Title,T1.ParentId,T2.TITLE as ParentTitle FROM Z_DOC_INDEX T1, Z_DOC_INDEX T2 where T1.ParentId=T2.id ) zi on  zd.attach_to_id = zi.Id  where " + colName + " like '%" + serchText + "%'  order by Lastmod_at desc  ";
            }
        else
            sql_qry = "SELECT Z_Doc_ID ,Page_Name,Tab_Name,Doc_Body,Lastmod_at,Lastmod_by,status,Key_Fields,Title,attach_to_id,id,ParentId,ParentTitle FROM Z_Documents zd  left join ( SELECT T1.id,T1.Title,T1.ParentId,T2.TITLE as ParentTitle FROM Z_DOC_INDEX T1, Z_DOC_INDEX T2 where T1.ParentId=T2.id ) zi on  zd.attach_to_id = zi.Id order by Lastmod_at desc ";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        BindDocumentDetails(dt);
    }
    public void BindDocumentDetails(DataTable dt)
    {
        if (dt != null && dt.Rows.Count > 0)
        {
            foreach (DataRow dtr1 in dt.Rows)
            {
                DocKeyWord[] keyWord = null;
                string keywordsStr = "<ul id='lstKey' class='list-inline'>";
                if (dtr1["Key_Fields"] != null && !String.IsNullOrEmpty(dtr1["Key_Fields"].ToString()))
                {
                    keyWord = Newtonsoft.Json.JsonConvert.DeserializeObject<DocKeyWord[]>(dtr1["Key_Fields"].ToString());
                }

                if (keyWord != null)
                {
                    foreach (var item in keyWord)
                    {
                        keywordsStr = keywordsStr + " <li> <div class='chip'><span class='keyword'>" + item.title + "</span> </div> </li>";
                    }

                }
                string strDocTitle = (!string.IsNullOrEmpty(dtr1["id"].ToString())) ? "<h5>" + dtr1["ParentTitle"].ToString() + "," + dtr1["Title"].ToString() + "</h5>" : "";
                keywordsStr = keywordsStr + "</ul>";


                lblContent.Text += "<h2 style='padding-top:50px'>" + dtr1["Page_Name"].ToString() + "</h2>"
                                           + strDocTitle
                                          //  + "<h5>" + dtr1["ParentTitle"].ToString() + "," + dtr1["Title"].ToString() + "</h5>"
                                          + "<a href='/admin/Document_View.aspx?DocId= " + dtr1["Z_Doc_ID"] + "&pageName=" + dtr1["Page_Name"] + "&tabName=" + dtr1["Tab_Name"] + "' >  [view] </a>"
                                          + "<a href='/admin/Document_Editor.aspx?DocId= " + dtr1["Z_Doc_ID"] + "&pageName=" + dtr1["Page_Name"] + "&tabName=" + dtr1["Tab_Name"] + "' >  [edit] </a>"
                                          + "<a href='#' onclick='copyContent(" + dtr1["Z_Doc_ID"] + ")' >  [copy] </a>"
                                          + "<div class='col-md-12' style='padding-bottom:5px;'>" + keywordsStr + " </div>"
                                          + "<p>" + dtr1["doc_body"].ToString() + "</p>";
            }
        }
        else
            lblContent.Text = "no documents available...";
    }
}

