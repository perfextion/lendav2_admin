﻿<%@ Page Title="" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" Trace="false" CodeFile="dbg_full_loan1.aspx.cs" Inherits="dbg_full_loan" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script><script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script><link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" /><style>
        .panel-heading {
            position: relative;
        }
            .panel-heading[data-toggle="collapse"]:after {
                font-family: 'Glyphicons Halflings';
                content: "\e072"; /* "play" icon */
                position: absolute;
                color: #b0c5d8;
                font-size: 18px;
                line-height: 22px;
                right: 20px;
                top: calc(50% - 10px);
                /* rotate "play" icon from > (right arrow) to down arrow */
                -webkit-transform: rotate(-90deg);
                -moz-transform: rotate(-90deg);
                -ms-transform: rotate(-90deg);
                -o-transform: rotate(-90deg);
                transform: rotate(-90deg);
            }

            .panel-heading[data-toggle="collapse"].collapsed:after {
                /* rotate "play" icon from > (right arrow) to ^ (up arrow) */
                -webkit-transform: rotate(90deg);
                -moz-transform: rotate(90deg);
                -ms-transform: rotate(90deg);
                -o-transform: rotate(90deg);
                transform: rotate(90deg);
            }
    </style><div class="row">
        <section id="widget-grid" class="">
            <div class="row">
                <article class="col-lg-12">
                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget" id="wid-id-0">
                        <header>
                            <span class="widget-icon"><i class="fa fa-comments"></i></span>
                            <h2>OMR Form Listing</h2>
                        </header>

                        <!-- widget div-->

                        <!-- widget edit box -->
                        <div class="jarviswidget-editbox">
                            <!-- This area used as dropdown edit box -->
                        </div>
                        <!-- end widget edit box -->
                        <!-- widget content -->
                        <div class="widget-body">

                            <div id="movieForm" class="form-horizontal">
                                <fieldset>
                                    <div class="form-actions">
                                        <asp:Label ID="lbl_top_links" runat="server" Text="Links"></asp:Label>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Search</label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="txt_search" runat="server"></asp:TextBox>
                                            <asp:Button ID="btn_search" runat="server" Text="Button" />
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-11">
                                        </div>
                                        <div class="col-md-1 text-align-right" style="padding-right: 0px;">
                                            <a id="collapse-init" href="#">Collapse Out</a>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="col-md-8">
                                        </div>
                                        <div class="col-md-4">
                                        </div>
                                    </div>

                                </fieldset>
                            </div>
                            <asp:Label ID="lbl_table" runat="server" Text="lbl_table"></asp:Label>
                            <table id='datatable_fixed_column' class='table table-striped table-bordered'>
                                <thead>
                                    <tr>
                                        <th>Form</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </article>
            </div>
        </section>
    </div>

    <script>      
        active = true;
        $('#collapse-init').click(function () {
            if (active) {
                active = false;
                $('.panel-collapse.collapse').addClass('in');
                $('.panel-heading.accordion-toggle.collapsed').removeClass('collapsed');
                $(".panel-collapse.collapse.in").removeAttr("style");

                $(this).text('Collapse In');
            } else {
                active = true;
                $('.panel-collapse.collapse.in').removeClass('in');
                $('.panel-heading.accordion-toggle').addClass('collapsed');
                $(this).text('Collapse Out');
            }
        });
        $('.panel-heading.accordion-toggle.collapsed').click(function () {
            
            if ($('.panel-heading.accordion-toggle.collapsed').length == 0) {
                active = true;
                $('#collapse-init').text('Collapse Out');
            }
        })
    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" runat="Server">
</asp:Content>

