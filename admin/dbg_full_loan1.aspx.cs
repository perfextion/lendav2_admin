﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Newtonsoft.Json;

public partial class dbg_full_loan : System.Web.UI.Page
{
    //Hashtable h_id2state = new Hashtable();
    //Hashtable h_crop_practice2key = new Hashtable();
    //Hashtable h_expenseid2name = new Hashtable();
    string loan_full_id = "0";
    string dbKey = "gp_conn";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["st_1"] == null) load_lookups();
        load_lookups();
        //  h_id2state = load_ID2state();
        // h_crop_practice2key = load_crop_practice2key();

        loan_full_id = Request.QueryString.GetValues("loan_full_id") == null ? "000001-000" : (Request.QueryString.GetValues("loan_full_id")[0].ToString());

        lbl_table.Text = "";
        lbl_table.Text += "<div class='panel-group' id='accordion'>";

        lbl_table.Text += get_counts_table(loan_full_id);

        lbl_table.Text += fill_farms_table(loan_full_id);

        lbl_table.Text += fill_crop_practices_table(loan_full_id);

        lbl_table.Text += fill_crop_units_table(loan_full_id);

        lbl_table.Text += fill_agents_table(loan_full_id);

        lbl_table.Text += fill_loan_budget_table(loan_full_id);

        lbl_table.Text += "</div>";

        //  lbl_table.Text += fill_farmunit_table();






    }


    protected Hashtable load_ID2state()
    {
        Hashtable h_return = new Hashtable();

        string sql_farms = " select state_id, state_name from ref_state ";

        h_return = gen_db_utils.gp_sql_get_hashtable(sql_farms, dbKey);

        return h_return;

    }

    protected Hashtable load_crop_practice2key()
    {
        Hashtable h_return = new Hashtable();

        string sql_farms = " select crop_and_practice_id, crop_full_key from v_crop_price_details ";

        h_return = gen_db_utils.gp_sql_get_hashtable(sql_farms, dbKey);

        return h_return;

    }

    protected Hashtable load_expenseid2name()
    {
        Hashtable h_return = new Hashtable();

        string sql_farms = " select crop_and_practice_id, crop_full_key from v_crop_price_details ";

        h_return = gen_db_utils.gp_sql_get_hashtable(sql_farms, dbKey);

        return h_return;

    }

    protected void load_lookups()
    {
        string strSql = " select state_id, state_name from ref_state ";

        DataTable dt1 = gen_db_utils.gp_sql_get_datatable(strSql, dbKey);


        foreach (DataRow dtr1 in dt1.Rows)
        {
            Session["st_" + dtr1["state_id"].ToString()] = dtr1["state_name"].ToString();
        }

        strSql = " select county_id, county_name from ref_county ";

        dt1 = gen_db_utils.gp_sql_get_datatable(strSql, dbKey);


        foreach (DataRow dtr1 in dt1.Rows)
        {
            Session["county_" + dtr1["county_id"].ToString()] = dtr1["county_name"].ToString();
        }

        strSql = " select crop_and_practice_id, crop_full_key from v_crop_price_details ";

        dt1 = gen_db_utils.gp_sql_get_datatable(strSql, dbKey);


        foreach (DataRow dtr1 in dt1.Rows)
        {
            Session["cp_" + dtr1["crop_and_practice_id"].ToString()] = dtr1["crop_full_key"].ToString();
        }


    }


    protected string get_counts_table(string inp_loan_id)
    {
        string str_return;


        string strSQL = "   select 'Farms' as obj, count(*) as cnt from loan_farm where Loan_Full_ID = '" + inp_loan_id + "'   and (isDelete = 0 or isDelete is null ) "
                              + " union "
                           + " select 'Crops', count(*) from loan_crop where Loan_Full_ID = '" + inp_loan_id + "'   and (isDelete = 0 or isDelete is null ) "
                               + " union "
                            + " select 'Crop Practices', count(*) from Loan_Crop_Practice where Loan_Full_ID = '" + inp_loan_id + "'   and (isDelete = 0 or isDelete is null ) "
                               + " union "
                            + " select 'Crop Units', count(*) from Loan_Crop_Unit where Loan_Full_ID = '" + inp_loan_id + "'   and (isDelete = 0 or isDelete is null ) "
                              + " union "
                            + " select 'Counties', count(distinct(farm_county_id)) from Loan_Farm where Loan_Full_ID = '" + inp_loan_id + "'   and (isDelete = 0 or isDelete is null ) "
                               + " union "
                            + " select 'Ins Policies', count(*) from Loan_Policies where Loan_Full_ID = '" + inp_loan_id + "'   and (isDeleted = 0 or isDeleted is null ) ";



        DataTable dt1 = gen_db_utils.gp_sql_get_datatable(strSQL, dbKey);

        str_return = " <div class='panel panel-default'><div class='panel-heading accordion-toggle collapsed' data-toggle='collapse' data-parent='#accordion' data-target='#collapseOverallCounts'><h4 class='panel-title'>Overall Counts(" + dt1.Rows.Count + ")</h4></div> ";


        str_return += "<div id='collapseOverallCounts' class='panel-collapse collapse'><div class='panel-body'>  <table id='datatable_fixed_column' class='table table-striped table-bordered'> "
                              + "      <thead> "
                              + "     <tr> "
                              + "       <th >Object</th> "
                              + "       <th >Count</th> "
                              + "    </tr> "
                              + "      </thead> "
                              + "        <tbody> ";

        foreach (DataRow dtr1 in dt1.Rows)
        {


            str_return += "<tr>"
                          + "<td>" + dtr1["obj"].ToString() + "</td>"
                          + "<td>" + dtr1["cnt"].ToString() + "</td>"

                             + "</tr>";

        }

        str_return += " </tbody> "
                                + " </table>  "
                                + "</div> </div ></div >";

        return str_return;

    }
    protected string fill_farms_table(string inp_loan_id)
    {
        string str_return;
        string sql_farms = " select * from loan_farm where loan_full_id = '" + inp_loan_id + "' and isDelete = 0 or isDelete is null ";

        DataTable dt1 = gen_db_utils.gp_sql_get_datatable(sql_farms, dbKey);
        str_return = "<div class='panel panel-default'><div class='panel-heading accordion-toggle collapsed' data-toggle='collapse' data-parent='#accordion' data-target='#collapseFarms'><h4 class='panel-title'>Farms(" + dt1.Rows.Count + ")</h4></div> ";

        str_return += "<div id='collapseFarms' class='panel-collapse collapse'><div class='panel-body'> <table id='datatable_fixed_column' class='table table-striped table-bordered'> "
                              + "      <thead> "
                              + "     <tr> "
                              + "       <th >Farm ID</th> "
                              + "       <th >State</th> "
                              + "       <th >County</th> "
                               + "       <th >FSN</th> "
                                + "       <th >Section</th> "
                                 + "       <th >Rated</th> "
                                  + "       <th >Owned</th> "
                                   + "       <th >LandOwner</th> "
                                    + "       <th >Share Rent</th> "
                                     + "       <th >Cash Rent Total</th> "
                                      + "       <th >County</th> "
                                       + "       <th >County</th> "
                                       + "       <th >County</th> "
                                       + "       <th >County</th> "
                                + "       <th >County</th> "
                              + "    </tr> "
                              + "      </thead> "
                              + "        <tbody> "
                            ;

        foreach (DataRow dtr1 in dt1.Rows)
        {


            str_return += "<tr>"
                          + "<td>" + dtr1["farm_id"].ToString() + "</td>"
                              + "<td>" + dtr1["farm_state_id"].ToString() + "|" + fc_gen_utils.sess_lookup("st_" + dtr1["farm_state_id"].ToString(), "-") + "</td>"
                              + "<td>" + fc_gen_utils.sess_lookup("county_" + dtr1["farm_county_id"].ToString(), "-") + "</td>"
                              + "<td>" + dtr1["FSN"].ToString() + "</td>"
                              + "<td>" + dtr1["Section"].ToString() + "</td>"
                              + "<td>" + dtr1["Rated"].ToString() + "</td>"
                              + "<td>" + dtr1["owned"].ToString() + "</td>"
                              + "<td>" + dtr1["landowner"].ToString() + "</td>"
                              + "<td>" + dtr1["share_rent"].ToString() + "</td>"
                              + "<td>" + dtr1["cash_rent_total"].ToString() + "</td>"
                              + "<td>" + dtr1["cash_rent_per_acre"].ToString() + "</td>"
                              + "<td>" + dtr1["cash_rent_due_date"].ToString() + "</td>"
                              + "<td>" + dtr1["cash_rent_paid"].ToString() + "</td>"
                              + "<td>" + dtr1["permission_to_insure"].ToString() + "</td>"
                              + "<td>" + dtr1["cash_rent_waived"].ToString() + "</td>"
                              + "<td>" + dtr1["cash_rent_waived_amount"].ToString() + "</td>"
                              + "<td>" + dtr1["irr_acres"].ToString() + "</td>"
                              + "<td>" + dtr1["ni_acres"].ToString() + "</td>"
                              + "<td>" + dtr1["crop_share_detail_indicator"].ToString() + "</td>"
                              + "<td>" + dtr1["Status"].ToString() + "</td>"
                              + "<td>" + dtr1["isdelete"].ToString() + "</td>"

                             + "</tr>";

        }

        str_return += " </tbody> "
                                + " </table>  "
                                 + "</div></div></div >";

        return str_return;

    }
    protected string fill_crop_practices_table(string inp_loan_id)
    {
        string str_return;
        string str_sql_1 = " select * from loan_crop_Practice  where  loan_full_id = '" + inp_loan_id + "'  "
                              + " and (isDelete = 0 or isDelete is null) ";

        DataTable dt1 = gen_db_utils.gp_sql_get_datatable(str_sql_1, dbKey);
        str_return = " <div class='panel panel-default'><div class='panel-heading accordion-toggle collapsed' data-toggle='collapse' data-parent='#accordion' data-target='#collapseLoanCropPractices'><h4 class='panel-title'>Loan Crop Practices(" + dt1.Rows.Count + ")</h4></div> ";

        str_return += "<div id='collapseLoanCropPractices' class='panel-collapse collapse'><div class='panel-body'> <table id='datatable_fixed_column' class='table table-striped table-bordered'> "
                              + "      <thead> "
                              + "     <tr> "
                              + "       <th >Assoc ID</th> "
                              + "       <th >Assoc_Name</th> "
                              + "       <th >Contact</th> "
                               + "       <th >Location</th> "
                                + "       <th >Phone</th> "
                                 + "       <th >Email</th> "

                              + "    </tr> "
                              + "      </thead> "
                              + "        <tbody> ";

        foreach (DataRow dtr1 in dt1.Rows)
        {


            str_return += "<tr>"
                          + "<td>" + fc_gen_utils.sess_lookup("cp_" + dtr1["crop_practice_id"].ToString(), "-") + "</td>"
                              + "<td>" + dtr1["Loan_Full_ID"].ToString() + "</td>"
                              + "<td>" + dtr1["lcp_aph"].ToString() + "</td>"
                              + "<td>" + dtr1["lcp_acres"].ToString() + "</td>"
                               + "<td>" + "" + "</td>"



                             + "</tr>";

        }

        str_return += " </tbody> "
                                + " </table> "
                                  + "</div></div></div >";

        return str_return;

    }
    protected string fill_crop_units_table(string inp_loan_id)
    {
        string str_return;
        string str_sql_1 = " select * from loan_crop_unit  where  loan_full_id = '" + inp_loan_id + "'  "
                              + " and (isDelete = 0 or isDelete is null) ";

        DataTable dt1 = gen_db_utils.gp_sql_get_datatable(str_sql_1, dbKey);
        str_return = "  <div class='panel panel-default'><div class='panel-heading accordion-toggle collapsed' data-toggle='collapse' data-parent='#accordion' data-target='#collapseLoanCropUnits'><h4 class='panel-title'>Loan Crop Units(" + dt1.Rows.Count + ")</h4></div> ";

        str_return += "<div id='collapseLoanCropUnits' class='panel-collapse collapse'><div class='panel-body'> <table id='datatable_fixed_column' class='table table-striped table-bordered'> "
                              + "      <thead> "
                              + "     <tr> "
                              + "       <th >Loan CU ID</th> "
                              + "       <th >Farm ID</th> "
                              + "       <th >Crop Full Key</th> "
                               + "       <th >CU Acres</th> "
                                + "       <th >Price</th> "
                                 + "       <th >Email</th> "

                              + "    </tr> "
                              + "      </thead> "
                              + "        <tbody> ";

        foreach (DataRow dtr1 in dt1.Rows)
        {
            str_return += "<tr>"
                          + "<td>" + dtr1["Loan_cu_id"].ToString() + "</td>"
                           + "<td>" + dtr1["farm_id"].ToString() + "</td>"
                           + "<td>" + dtr1["crop_full_key"].ToString() + "</td>"
                              + "<td>" + dtr1["CU_acres"].ToString() + "</td>"

                              + "<td>" + dtr1["z_price"].ToString() + "</td>"
                               + "<td>" + "" + "</td>"



                             + "</tr>";

        }

        str_return += " </tbody> "
                                + " </table> "
                                + "</div></div></div >";

        return str_return;

    }
    protected string fill_agents_table(string inp_loan_id)
    {
        string str_return;
        string sql_farms = " select * from loan_association  where assoc_type_code = 'AGT' and loan_full_id = '" + inp_loan_id + "'  "
                              + " and isDelete = 0 or isDelete is null "; ;

        DataTable dt1 = gen_db_utils.gp_sql_get_datatable(sql_farms, dbKey);

        str_return = " <div class='panel panel-default'><div class='panel-heading accordion-toggle collapsed' data-toggle='collapse' data-parent='#accordion' data-target='#collapseAgents'><h4 class='panel-title'>Agents(" + dt1.Rows.Count + ")</h4></div> ";

        str_return += "<div id='collapseAgents' class='panel-collapse collapse'><div class='panel-body'> <table id='datatable_fixed_column' class='table table-striped table-bordered'> "
                              + "      <thead> "
                              + "     <tr> "
                              + "       <th >Assoc ID</th> "
                              + "       <th >Assoc_Name</th> "
                              + "       <th >Contact</th> "
                               + "       <th >Location</th> "
                                + "       <th >Phone</th> "
                                 + "       <th >Email</th> "

                              + "    </tr> "
                              + "      </thead> "
                              + "        <tbody> ";

        foreach (DataRow dtr1 in dt1.Rows)
        {
            str_return += "<tr>"
                          + "<td>" + dtr1["Assoc_id"].ToString() + "</td>"
                              + "<td>" + dtr1["Loan_Full_ID"].ToString() + "</td>"
                              + "<td>" + dtr1["Assoc_Name"].ToString() + "</td>"
                              + "<td>" + dtr1["Contact"].ToString() + "</td>"
                               + "<td>" + dtr1["Location"].ToString() + "</td>"
                                + "<td>" + dtr1["Phone"].ToString() + "</td>"

                                  + "<td>" + dtr1["email"].ToString() + "</td>"


                             + "</tr>";

        }

        str_return += " </tbody> "
                                + " </table> "
                                 + "</div></div></div >";

        return str_return;

    }
    protected string fill_loan_budget_table(string inp_loan_id)
    {
        string str_return;


        string str_sql_1 = " select * from loan_budget  where  loan_full_id = '" + inp_loan_id + "'  "
                              + " and (isDelete = 0 or isDelete is null) ";

        DataTable dt1 = gen_db_utils.gp_sql_get_datatable(str_sql_1, dbKey);

        str_return = " <div class='panel panel-default'><div class='panel-heading accordion-toggle collapsed' data-toggle='collapse' data-parent='#accordion' data-target='#collapseLoanBudgetItems'><h4 class='panel-title'>Loan Budget Items(" + dt1.Rows.Count + ")</h4></div>";


        str_return += "<div id='collapseLoanBudgetItems' class='panel-collapse collapse'><div class='panel-body'> <table id='datatable_fixed_column' class='table table-striped table-bordered'> "
                              + "      <thead> "
                              + "     <tr> "
                              + "       <th >crop_practice_id</th> "
                              + "       <th >Loan_Full_ID</th> "
                              + "       <th >Expense_type_id</th> "
                               + "       <th >arm_budget_acre</th> "
                                + "       <th >distributor_budget_acre</th> "
                                 + "       <th >third_party_budget_acre</th> "

                              + "    </tr> "
                              + "      </thead> "
                              + "        <tbody> ";

        foreach (DataRow dtr1 in dt1.Rows)
        {


            str_return += "<tr>"
                           //+ "<td>" + fc_gen_utils.hash_lookup(h_crop_practice2key, dtr1["crop_practice_id"].ToString(), "-") + "</td>"
                           + "<td>" + fc_gen_utils.sess_lookup("cp_" + dtr1["crop_practice_id"].ToString(), "-") + "</td>"
                              + "<td>" + dtr1["Loan_Full_ID"].ToString() + "</td>"
                              + "<td>" + dtr1["Expense_type_id"].ToString() + "</td>"
                              + "<td>" + dtr1["arm_budget_acre"].ToString() + "</td>"
                              + "<td>" + dtr1["distributor_budget_acre"].ToString() + "</td>"
                              + "<td>" + dtr1["third_party_budget_acre"].ToString() + "</td>"
                               + "<td>" + "" + "</td>"



                             + "</tr>";

        }

        str_return += " </tbody> "
                                + " </table> "
                                 + "</div></div></div >";

        return str_return;

    }
}