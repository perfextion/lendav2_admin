﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Newtonsoft.Json;

public partial class dbg_optimizer : System.Web.UI.Page
{
     string dbKey = "gp_conn";

    string loan_id = "0";
    Hashtable h_county = new Hashtable();
    Hashtable h_ip_level = new Hashtable();
    Hashtable h_ip_price = new Hashtable();
    Hashtable h_ip_premium = new Hashtable();

    protected void Page_Load(object sender, EventArgs e)
    {
        loan_id = Request.QueryString.GetValues("loan_id") == null ? "1017" : (Request.QueryString.GetValues("loan_id")[0].ToString());

        string sql_counties = " select id, name from county where id in ( "

                             + " select distinct(countyid) from farm where loanid = " + loan_id  + " ) ";

        h_county = gen_db_utils.gp_sql_get_hashtable(sql_counties, dbKey);

        string str_sql = " select cast(countyid as varchar) +'_'+ cast(cropid as varchar)+'_'+ cast(practice as varchar), level from InsurancePolicy where loanid = " + loan_id;

        h_ip_level = gen_db_utils.gp_sql_get_hashtable(str_sql, dbKey);

        str_sql = " select cast(countyid as varchar) +'_'+ cast(cropid as varchar)+'_'+ cast(practice as varchar), price from InsurancePolicy where loanid = " + loan_id;

         h_ip_price = gen_db_utils.gp_sql_get_hashtable(str_sql, dbKey);

         str_sql = " select cast(countyid as varchar) +'_'+ cast(cropid as varchar)+'_'+ cast(practice as varchar), premium from InsurancePolicy where loanid = " + loan_id;

         h_ip_premium = gen_db_utils.gp_sql_get_hashtable(str_sql, dbKey);

         lbl_table.Text = "";

      //  lbl_table.Text = fill_table();

        lbl_table.Text += fill_joined_table();

       // lbl_table.Text += fill_farm_table();

      //  lbl_table.Text += fill_farmunit_table();

       
        
        

  
    }


   


    protected string fill_table()
    {
        string str_return;


        string str_sql = "select * from InsurancePolicy where loanid = " + loan_id ;

        DataTable dt1 = gen_db_utils.gp_sql_get_datatable(str_sql, dbKey);

        str_return = " <table id='datatable_fixed_column' class='table table-striped table-bordered'> "
                              + "      <thead> "
                              + "     <tr> "
                              + "       <th >ID</th> "
                              + "       <th >Text</th> "
                              + "       <th >Edit</th> "
                              + "    </tr> "
                              + "      </thead> "
                              + "        <tbody> ";

        foreach (DataRow dtr1 in dt1.Rows)
        {
            
           
            str_return += "<tr>"
                              + "<td>" + dtr1["loanid"].ToString() + "</td>"
                              + "<td>" + dtr1["countyid"].ToString() + "</td>"
                              + "<td>" + dtr1["price"].ToString() + "</td>"
                             + "</tr>";

        }

        str_return += " </tbody> "
                                + " </table> ";

        return str_return;

    }

    protected string fill_farmunit_table()
    {
        string str_return;


        string str_sql = "select FU.id as FU_id,* from farmunit FU "
                         + " join farm F on F.id = fu.farmid "
                         + " where F.loanid = " + loan_id ;

        DataTable dt1 = gen_db_utils.gp_sql_get_datatable(str_sql, dbKey);

        str_return = " <table id='datatable_fixed_column' class='table table-striped table-bordered'> "
                              + "      <thead> "
                              + "     <tr> "
                              + "       <th >LoanID</th> "
                              + "       <th >farmid</th> "
                              + "       <th >farmunitid</th> "
                               + "       <th >countyid</th> "
                                + "       <th >acres</th> "
                                 + "       <th >practice</th> "
                                  + "       <th >FSN</th> "
                                   + "       <th >owner</th> "
                              + "    </tr> "
                              + "      </thead> "
                              + "        <tbody> ";

        foreach (DataRow dtr1 in dt1.Rows)
        {


            str_return += "<tr>"
                              + "<td>" + dtr1["loanid"].ToString() + "</td>"
                               + "<td>" + dtr1["farmid"].ToString() + "</td>"
                               + "<td>" + dtr1["FU_id"].ToString() + "</td>"
                               + "<td>" + dtr1["countyid"].ToString() + "</td>"
                              + "<td>" + dtr1["acres"].ToString() + "</td>"
                              + "<td>" + dtr1["practice"].ToString() + "</td>"
                              + "<td>" + dtr1["FSN"].ToString() + "</td>"
                               + "<td>" + dtr1["owner"].ToString() + "</td>"
                             + "</tr>";

        }

        str_return += " </tbody> "
                                + " </table> ";

        return str_return;

    }


    protected string fill_farm_table()
    {
        string str_return;

        str_return = " Farms <br><br> ";
        string str_sql = "select * from farm where loanid = " + loan_id;

        DataTable dt1 = gen_db_utils.gp_sql_get_datatable(str_sql, dbKey);

        str_return += " <table id='datatable_fixed_column' class='table table-striped table-bordered'> "
                              + "      <thead> "
                              + "     <tr> "
                                + "       <th >ID</th> "
                              + "       <th >LoanID</th> "
                              + "       <th >FSN</th> "
                              + "       <th >CountyID</th> "
                               + "       <th >owner</th> "
                            
                              + "    </tr> "
                              + "      </thead> "
                              + "        <tbody> ";

        foreach (DataRow dtr1 in dt1.Rows)
        {


            str_return += "<tr>"
                              + "<td>" + dtr1["id"].ToString() + "</td>"
                              + "<td>" + dtr1["loanid"].ToString() + "</td>"
                               + "<td>" + dtr1["FSN"].ToString() + "</td>"
                               + "<td>" + dtr1["countyid"].ToString() + fc_gen_utils.hash_lookup(h_county, dtr1["countyid"].ToString(), "NA") + "</td>"
                               + "<td>" + dtr1["owner"].ToString() + "</td>"
                      
                             + "</tr>";

        }

        str_return += " </tbody> "
                                + " </table> ";

        return str_return;

    }


    protected string fill_joined_table()
    {
        string str_return;

        str_return = " Full Joined Table  <br><br> ";
        string str_sql = "select f.id as farm_id, fu.id as farmunit_id, cu.id as cropunit_id, lc.id as Loancrop_id, "
                        +  " fsn, countyid,owner, practice, FU.acres as FU_acres, CU.acres as cu_acres, aph, cropid, saleprice from farm f " 
                       + " join farmunit fu on fu.farmid = f.id "
                         + "  join cropunit cu on cu.farmunitid = fu.id "
                         + "   join loancrop lc on lc.id = cu.loancropid "
                        + " where f.loanid= " + loan_id + " and CU.acres > 0  order by fsn";

        DataTable dt1 = gen_db_utils.gp_sql_get_datatable(str_sql, dbKey);

        str_return += " <table id='datatable_fixed_column' class='table table-striped table-bordered'> "
                              + "      <thead> "
                              + "     <tr> "
                                + "       <th >farm_id</th> "
                              + "       <th >farmunit_id</th> "
                              + "       <th >cropunit_id</th> "
                              + "       <th >Loancrop_id</th> "
                               + "       <th >FSN</th> "
                               + "       <th >countyid</th> "
                               + "       <th >owner</th> "
                               + "       <th >practice</th> "
                               + "       <th >FU_acres</th> "
                               + "       <th >cu_acres</th> "
                                  + "       <th >aph</th> "
                                     + "       <th >cropid</th> "
                               + "       <th >saleprice</th> "
                                   + "       <th >Level</th> "
                                       + "       <th >Price</th> "
                                           + "       <th >Premium</th> "
                                                   + "       <th >MPCI</th> "
                              + "    </tr> "
                              + "      </thead> "
                              + "        <tbody> ";

        double d_mpci_total = 0.0;

        foreach (DataRow dtr1 in dt1.Rows)
        {

            string ins_key = dtr1["countyid"].ToString() + "_" + dtr1["cropid"].ToString() + "_" + dtr1["practice"].ToString();

            string ins_level = fc_gen_utils.hash_lookup(h_ip_level, ins_key, "NA");
            string ins_price = fc_gen_utils.hash_lookup(h_ip_price, ins_key, "NA");
            string ins_premium = fc_gen_utils.hash_lookup(h_ip_premium, ins_key, "NA");

            double d_acres = Convert.ToDouble(dtr1["cu_acres"].ToString());
            double d_level = Convert.ToDouble(ins_level);
            double d_price = Convert.ToDouble(ins_price);
            double d_premium = Convert.ToDouble(ins_premium);
            double d_aph = Convert.ToDouble(dtr1["aph"].ToString()); 
         //   double d_mpci = (d_level*d_price*d_aph - d_premium)*d_acres/100.0;
            double d_mpci_1 = (d_level/100.0 * d_price * d_aph - d_premium);
            double d_mpci_2 = d_mpci_1*d_acres;
            d_mpci_total += d_mpci_2;

            str_return += "<tr>"
                              + "<td>" + dtr1["farm_id"].ToString() + "</td>"
                              + "<td>" + dtr1["farmunit_id"].ToString() + "</td>"
                               + "<td>" + dtr1["cropunit_id"].ToString() + "</td>"
                               + "<td>" + dtr1["Loancrop_id"].ToString()  + "</td>"
                               + "<td>" + dtr1["FSN"].ToString() + "</td>"
                               + "<td>" + dtr1["countyid"].ToString() + "</td>"
                               + "<td>" + dtr1["owner"].ToString() + "</td>"
                               + "<td>" + dtr1["practice"].ToString() + "</td>"
                               + "<td>" + dtr1["FU_acres"].ToString() + "</td>"
                               + "<td>" + dtr1["cu_acres"].ToString() + "</td>"
                               + "<td>" + dtr1["aph"].ToString() + "</td>"
                               + "<td>" + dtr1["cropid"].ToString() + "</td>"
                               + "<td>" + dtr1["saleprice"].ToString() + "</td>"
                               + "<td>" + ins_level + "</td>"
                               + "<td>" + ins_price + "</td>"
                               + "<td>" + ins_premium + "</td>"
                            
                               + "<td>" + d_mpci_2.ToString() + "</td>"
                             + "</tr>";

        }

        str_return += " </tbody> "
                                + " </table> ";

        str_return += d_mpci_total.ToString();

        return str_return;

    }
}