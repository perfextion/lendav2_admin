﻿<%@ Page Title="" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="hyperfieldvalidation.aspx.cs" Inherits="admin_hyperfieldvalidation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <style>
        .ui-jqgrid .ui-jqgrid-htable th div {
            height: auto;
            overflow: hidden;
            padding-right: 4px;
            padding-top: 2px;
            position: relative;
            vertical-align: text-top;
            white-space: normal !important;
        }
    </style>
    <div class="">
        <div style="width: 1500px">
            <div style="width: 42%; margin-left: 2.6%; float: left;">
                <div style="margin-top: 3%;">
                    <div id="btnAddRow" class="btn btn-info" style="width: 70px;">
                        add
                    </div>
                    <div id="btnSave" class="btn btn-success" style="width: 70px;">Save</div>
                </div>
            </div>
            <div style="width: 19%; float: left; margin-left: 20%;">
                <div class="form-inline" style="margin-top: 5%; margin-left: 2%; margin-bottom: 2%;">
                    <label for="txtSearch">Search :</label>
                    <input type="text" class="form-control" name="search" id="txtSearch" style="width: 200px" />
                </div>
            </div>
        </div>


        <div class="loader"></div>
        <div class="row">
            <div style="margin-top: 5%; margin-left: 50px">
                <table id="jqGrid"></table>
                <div id="jqGridPager"></div>
            </div>
        </div>
        <br />
        <br />
        <br />
        <br />
    </div>
    <br />
    <br />
    <br />
    <script type="text/javascript">  
        var DeleteRows = [];
        var timer;
        var lastSelection;
        var tabidslist = {};
        var chervonlist = {};
        $(function () {

            var obj = {
                Init: function () {

                    //obj.chervonid();
                    //obj.tabids();

                    $("#btnAddRow").click(function () {
                        obj.add();
                    });
                    $("#btnSave").click(function () {
                        obj.save();
                    });
                    $("#txtSearch").on("keyup", function () {
                        var self = this;
                        obj.search(self);
                    });

                    $.ajax({
                        type: "POST",
                        url: "hyperfieldvalidation.aspx/Getchervonid",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        async: false,
                        success: function (data) {
                            chervonlist = JSON.parse(data.d);
                            chervonlist[0] = 'Select'
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url: "hyperfieldvalidation.aspx/Gettabids",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        async: false,
                        success: function (data) {
                            tabidslist = JSON.parse(data.d);
                            tabidslist[0] = 'Select'
                        }
                    });

                    obj.bindGrid();
                },

                bindGrid: function () {
                    $.ajax({
                        type: "POST",
                        url: "hyperfieldvalidation.aspx/Gethyper",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        async: false,
                        success: function (data) {
                            var resData = JSON.parse(data.d);
                            $('.loader').show();
                            for (var i = 0; i < resData.length; i++) {
                                resData[i].rowid = i + 1;
                            }
                            obj.loadgrid(resData);
                            $('.loader').hide();
                        }
                    });
                },
                loadgrid: function (data) {
                    $("#jqGrid").jqGrid({
                        datatype: "local",
                        styleUI: 'Bootstrap',
                        data: data,
                        colModel: [
                            { label: 'rowid', name: 'rowid', width: 75, editable: false, key: true, hidden: true },
                            { label: 'Ref Validation ID', name: 'Ref_Validation_ID', align: 'center', cellsalign: 'right', width: 120, editable: false },
                            {
                                label: 'Table Name', name: 'Table_Name', index: 'Table_Name', width: 300, editable: true, edittype: "select", formatter: 'select',
                                editoptions: {
                                    value: ":Select;Lienholder:Lienholder;Distributer:Distributer",
                                    dataInit: function (elem) {
                                        setTimeout(function () {
                                            $(elem).change();
                                        }, 0);
                                    },
                                    dataEvents: [
                                        {//loadSubSection
                                            type: 'change',
                                            fn: function (e) {
                                                obj.gettab(this.value);
                                            }
                                        }]
                                }
                            },
                            // { name: 'Table_Name', index: 'Table_Name', width: 150, editable: true, edittype: "select", formatter: 'select', editoptions: { value: ":Select;Lienholder:Lienholder;Distributor:Distributor" } },
                            { label: 'Field', name: 'Field', index: 'Field', align: 'center', width: 100, editable: true, edittype: "select", formatter: 'select', editoptions: { value: ":Select;Email:Email;Phone:Phone" } },
                            { label: 'Operator', name: 'Operator', index: 'Operator', align: 'center', width: 100, editable: true, edittype: "select", formatter: 'select', editoptions: { value: ":Select;IS:IS" } },
                            { label: 'Compare Fn', name: 'Compare_Fn', index: 'Compare_Fn', width: 200, editable: true, edittype: "select", formatter: 'select', editoptions: { value: ":Select;InvalidEmail:InvalidEmail;InvalidPhone:InvalidPhone;Blank:Blank" } },
                            { label: 'Action Ind', name: 'Action_Ind', index: 'Action_Ind', width: 100, editable: true, edittype: "select", formatter: 'select', editoptions: { value: ":Select;V1:V1;V1Blank:V1Blank;V2:V2;V2Blank:V2Blank;E1:E1;E2:E2" } },
                            { label: 'Chevron Name', name: 'Chevron_Name', width: 175, editable: false },
                            { label: 'Tab Id', name: 'Tab_Id', width: 75, align: 'center', editable: false },
                            { name: 'Status', index: 'Status', width: 100, align: 'center', editable: true, edittype: "select", formatter: 'select', editoptions: { value: ":Select;0:0;-1:-1" } },
                            { label: 'Actionstatus', name: 'Actionstatus', width: 70, editable: false, hidden: true },
                            { label: '', name: '', width: 35, align: 'center', formatter: obj.deleteLink },
                        ],
                        viewrecords: true,
                        loadonce: true,
                        restoreAfterSelect: false,
                        saveAfterSelect: true,
                        sortable: true,
                        onSelectRow: obj.edit,
                        onPaging: obj.changePage,
                        height: 'auto',
                        width: 1200,
                        rowNum: 100,
                        pager: "#jqGridPager"
                    });
                },
                gettab: function (chervonname) {

                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "hyperfieldvalidation.aspx/Gettabidbychervon",
                        data: JSON.stringify({ ChervonName: chervonname }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var id = $('.inline-edit-cell').parent().parent().prop('id');
                            chervonlist = JSON.parse(data.d);

                            if (chervonname != '' && chervonname != null) {
                                if (id != undefined) {
                                    $("#jqGrid").jqGrid("setCell", id, "Chevron_Name", chervonlist[0].Chevron_Name);
                                    $("#jqGrid").jqGrid("setCell", id, "Tab_Id", chervonlist[0].Tab_Id);
                                }
                            }
                        }
                    });
                },
                add: function () {

                    var grid = $("#jqGrid");
                    var rowsperPage = parseInt(grid.getGridParam('rowNum'));
                    var gridlength = grid.jqGrid('getGridParam', 'data').length;
                    var curpage = parseInt($(".ui-pg-input").val());
                    var totPages = Math.ceil(gridlength / rowsperPage);


                    if (rowsperPage * totPages == gridlength) {
                        var id = $('.inline-edit-cell').parent().parent().prop('id');
                        grid.jqGrid('saveRow', id);
                        var row = obj.newrow();

                        var newRowId = row.rowid;
                        grid.jqGrid('addRowData', newRowId, row);
                        grid.trigger('reloadGrid');
                        lastSelection = newRowId;
                        grid.jqGrid('saveRow', lastSelection);
                        grid.jqGrid('restoreRow', lastSelection);
                        $('.glyphicon-step-forward').trigger('click');
                    } else {
                        $('.glyphicon-step-forward').trigger('click');
                        var row = obj.newrow();

                        var newRowId = row.rowid;
                        grid.jqGrid('addRowData', newRowId, row);
                    }


                    lastSelection = newRowId;
                    grid.jqGrid('saveRow', lastSelection);
                    grid.jqGrid('restoreRow', lastSelection);

                    var eid = $('.inline-edit-cell').parent().parent().prop('id')
                    grid.jqGrid('saveRow', eid);
                    grid.jqGrid('restoreRow', eid);
                    grid.jqGrid('editRow', newRowId);
                },
                edit: function (id) { 
                    if (id && id !== lastSelection) {
                        var grid = $("#jqGrid");
                        grid.jqGrid('saveRow', lastSelection);
                        grid.jqGrid('restoreRow', lastSelection);
                        var row = grid.jqGrid('getRowData', lastSelection);

                        if (!jQuery.isEmptyObject(row)) {
                            var dataobj = jQuery("#jqGrid").jqGrid('getGridParam', 'data');
                            index = dataobj.findIndex(x => x.rowid == row.rowid);
                            if (row.rowid > 0 && row.Actionstatus != 1) {
                                row.Actionstatus = 2;
                                grid.jqGrid('getGridParam', 'data')[index] = row;
                            }
                        }

                        grid.jqGrid('editRow', id);
                        lastSelection = id;
                    }
                },
                newrow: function () {
                    var newid = 0;

                    var grid = $("#jqGrid");
                    var length = jQuery("#jqGrid").jqGrid('getGridParam', 'records');
                    var dataobj = grid.jqGrid('getGridParam', 'data');
                    var dataobj = grid.jqGrid('getGridParam', 'data');
                    var Ref_Validation_ID = [];
                    Ref_Validation_IDs = dataobj.map(function (e) { return e.rowid });
                    if (Ref_Validation_IDs.length > 0) {
                        newid = Ref_Validation_IDs.reduce(function (a, b) { return Math.max(a, b); });
                    }
                    var row = {

                        Ref_Validation_ID: 0,
                        Actionstatus: 1,
                        rowid: newid + 1
                    };
                    return row;
                },
                save: function () {
                    $('.loader').show();
                    var grid = $("#jqGrid");
                    var allrows = [];
                    var id = $('.inline-edit-cell').parent().parent().prop('id');
                    grid.jqGrid('saveRow', id);
                    var rowData = grid.jqGrid('getRowData', id);
                    if (rowData.rowid > 0) {
                        rowData.Actionstatus = 2;
                        grid.jqGrid('getGridParam', 'data')[id] = rowData;
                    }

                    for (var i = 0; i < DeleteRows.length; i++) {
                        DeleteRows[i].Actionstatus = 3
                        allrows.push(DeleteRows[i]);
                    }
                    var dataobj = grid.jqGrid('getGridParam', 'data');
                    var newRows = $.grep(dataobj, function (e) {
                        if (e != undefined) {
                            return e.Actionstatus == 1 || e.Actionstatus == 2
                        }
                    });
                    for (var i = 0; i < newRows.length; i++) {
                        allrows.push(newRows[i]);
                    }
                    $.ajax({
                        type: "POST",
                        url: "hyperfieldvalidation.aspx/SaveHyper",
                        data: JSON.stringify({ lst: allrows }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            location.reload();
                        },
                        failure: function (response) {
                            console.log(response.d);
                        }

                    });
                },
                search: function (self) {
                    if (timer) { clearTimeout(timer); }
                    timer = setTimeout(function () {
                        obj.changePage();
                        $("#jqGrid").jqGrid('filterInput', self.value);
                    }, 0);
                },
                changePage: function () {

                    lastSelection = "";
                    var grid = $("#jqGrid");
                    var id = $('.inline-edit-cell').parent().parent().prop('id');
                    if (id != undefined) {
                        grid.jqGrid('saveRow', id);
                        grid.jqGrid('restoreRow', id);
                        var row = grid.jqGrid('getRowData', id);
                        if (!jQuery.isEmptyObject(row)) {
                            var dataobj = jQuery("#jqGrid").jqGrid('getGridParam', 'data');
                            index = dataobj.findIndex(x => x.rowid == row.rowid);
                            if (row.rowid > 0 && row.Actionstatus != 1) {
                                row.Actionstatus = 2;
                                grid.jqGrid('getGridParam', 'data')[index] = row;
                            }
                        }
                    }
                },
                deleteLink: function (cellValue, options, rowdata, action) {
                    return "<a href='javascript:deleteRecord(" + options.rowId + ")' class='glyphicon glyphicon-trash' style='color:red'></a>";
                }
            }
            obj.Init();

        });

        function deleteRecord(id) {

            var grid = $('#jqGrid');
            var result = confirm("Are you sure you Want to delete?");

            if (result == true) {
                var ediId = $('.inline-edit-cell').parent().parent().prop('id');
                grid.jqGrid('saveRow', ediId);
                var rowData = grid.jqGrid('getRowData', ediId);
                if (rowData.rowid > 0 && rowData.Actionstatus != 1) {
                    rowData.Actionstatus = 2;
                    grid.jqGrid('getGridParam', 'data')[ediId - 1] = rowData;
                }

                var row = grid.jqGrid('getRowData', id);
                grid.jqGrid('delRowData', id);
                if (row.Actionstatus != 1) {
                    DeleteRows.push(row);
                }
                var data = jQuery("#jqGrid").jqGrid('getGridParam', 'data');
                for (var i = 0; i < data.length; i++) {
                    if (data[i].Ref_Validation_ID == 0) {
                        data[i].rowid = i + 1;
                        data[i].id = i + 1;
                    } else
                        data[i].rowid = i + 1;
                }
                var curpage = parseInt($(".ui-pg-input").val());
                jQuery('#jqGrid').jqGrid('clearGridData');
                jQuery('#jqGrid').jqGrid('setGridParam', { data: data });
                $("#jqGrid").trigger("reloadGrid", [{ page: curpage }]);
                $("#jqGrid").trigger("reloadGrid", [{ page: curpage }]);
                lastSelection = id;
            }
        }
        function unique(list) {
            var result = [];
            $.each(list, function (i, e) {
                if ($.inArray(e, result) == -1) result.push(e);
            });
            return result;
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" runat="Server">
</asp:Content>

