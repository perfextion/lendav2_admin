﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_hyperfieldvalidation : System.Web.UI.Page
{
    static string dbKey = "gp_conn";
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    [WebMethod]
    public static string Gethyper()
    {//        

        //string sql_qry = "SELECT 0 as Actionstatus,Ref_Validation_ID, Table_Name, Field, " +
        //    "Operator, Compare_Fn, Action_Ind, Tab_Id, Chevron_Id, Status FROM Ref_Hyperfield_Validations";
        string sql_qry = "SELECT 0 as Actionstatus,Ref_Validation_ID,Table_Name,Field,Operator,Compare_Fn,Action_Ind,Rc.Tab_Id,Chevron_Name,rhv.Status " +
            "FROM Ref_Hyperfield_Validations Rhv left join Ref_Chevron Rc on Rhv.Chevron_Id=Rc.Chevron_Id";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
    }
    [WebMethod]
    public static string Getchervonid()
    {
        string sql_qry = "select Chevron_id,chevron_Name from [dbo].[Ref_Chevron]";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return DataTableToJsonstrings(dt);
    }
    [WebMethod]
    public static string Gettabids()
    {
        string sql_qry = "select distinct tab_id from [dbo].[Ref_Chevron]";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return DataTableToJsonstring(dt);
    }
    [WebMethod]
    public static string Gettabidbychervon(string ChervonName)
    {
        string sql_qry = "select Chevron_Name,Tab_Id from Ref_Chevron where Chevron_Name='" + ChervonName + "'";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
        //return DataTableToJsonstrings(dt);
    }
    public static string DataTableToJsonstring(DataTable dt)
    {
        DataSet ds = new DataSet();
        ds.Merge(dt);
        StringBuilder JsonStr = new StringBuilder();
        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            JsonStr.Append("{");
            //JsonStr.Append("\"\":" + "\"Select\",");
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                JsonStr.Append("\"" + ds.Tables[0].Rows[i][0].ToString() + "\":" + "\"" + ds.Tables[0].Rows[i][0].ToString() + "\",");
            }
            JsonStr = JsonStr.Remove(JsonStr.Length - 1, 1);
            JsonStr.Append("}");
            return JsonStr.ToString();
        }
        else
        {
            return null;
        }
    }
    public static string DataTableToJsonstrings(DataTable dt)
    {
        DataSet ds = new DataSet();
        ds.Merge(dt);
        StringBuilder JsonStr = new StringBuilder();
        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            JsonStr.Append("{");
            //JsonStr.Append("\"\":" + "\"Select\",");
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                JsonStr.Append("\"" + ds.Tables[0].Rows[i][0].ToString() + "\":" + "\"" + ds.Tables[0].Rows[i][1].ToString() + "\",");
            }
            JsonStr = JsonStr.Remove(JsonStr.Length - 1, 1);
            JsonStr.Append("}");
            return JsonStr.ToString();
        }
        else
        {
            return null;
        }
    }

    [WebMethod]
    public static void SaveHyper(dynamic lst)
    {
        foreach (var item in lst)
        {
            string qry = "";
            int val = Convert.ToInt32(item["Actionstatus"]);
            string chervon = item["Chevron_Id"];
            string tab = item["Chevron_Id"];
            if (chervon == "0")
            {
                chervon = "";
            }
            if (tab == "0")
            {
                tab = "";
            }
            if (val == 1)
            {
                qry = "INSERT INTO Ref_Hyperfield_Validations (Table_Name,Field,Operator,Compare_Fn,Action_Ind,Tab_Id,Chevron_Id,Status)" +
                    " VALUES('" + item["Table_Name"].Replace("'", "''") + "','" + item["Field"].Replace("'", "''") + "'," +
                    "'" + item["Operator"].Replace("'", "''") + "','" + item["Compare_Fn"].Replace("'", "''") + "'," +
                    "'" + item["Action_Ind"].Replace("'", "''") + "','" + tab + "'," +
                    "'" + chervon + "','" + item["Status"].Replace("'", "''") + "')";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 2)
            {
                qry = "UPDATE Ref_Hyperfield_Validations SET Table_Name = '" + item["Table_Name"].Replace("'", "''") + "' ," +
                    "Field = '" + item["Field"].Replace("'", "''") + "' ," +
                    "Operator = '" + item["Operator"].Replace("'", "''") + "' ," +
                    "Compare_Fn = '" + item["Compare_Fn"].Replace("'", "''") + "' ," +
                    "Action_Ind = '" + item["Action_Ind"].Replace("'", "''") + "' ," +
                    "Tab_Id = '" + item["Tab_Id"].Replace("'", "''") + "' ," +
                    "Chevron_Id = '" + item["Chevron_Id"].Replace("'", "''") + "' ," +
                    "Status = '" + item["Status"].Replace("'", "''") + "' where Ref_Validation_ID=" + item["Ref_Validation_ID"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
            else if (val == 3)
            {
                qry = "DELETE FROM Ref_Hyperfield_Validations  where Ref_Validation_ID=" + item["Ref_Validation_ID"] + "  ";
                gen_db_utils.gp_sql_execute(qry, dbKey);
            }
        }
    }
}