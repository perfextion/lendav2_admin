﻿<%@ Page Title="" Language="C#" MasterPageFile="~/gp_Master.master" AutoEventWireup="true" CodeFile="insurencetest.aspx.cs" Inherits="admin_insurencetest" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <%-- <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/bootstrap.tagsinput/0.4.2/bootstrap-tagsinput.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/bootstrap.tagsinput/0.4.2/bootstrap-tagsinput.min.js"></script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="http://twitter.github.io/typeahead.js/releases/latest/typeahead.bundle.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/3.2/select2.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/3.2/select2.css" />

    <style>
        input[type="text"] {
            width: 450px;
        }

        /*input[type="text"] {
            width: 200px;
        }*/
        input.wid100 {
            width: 70px;
        }

        table {
            border-collapse: collapse;
        }

        table, td, th {
            border: 1px solid black;
        }

        tr {
            text-align: center;
        }

        .disabled33 {
            pointer-events: none;
            opacity: .9;
        }
    </style>


    <div class="row">
        <%--<div class="col-md-10">
        </div>--%>
        <div class="col-md-1">
            <div class="form-group" style="margin-left: 46px;">
                <div class="btn btn-primary" style="background-color: green;" id="btnEnable">Enable</div>
            </div>
        </div>
        <div class="col-md-1">
            <div class="form-group">
                <div class="btn btn-primary" id="btnReload" style="background-color: red;">Disable</div>
            </div>
        </div>
    </div>
    <%--    <select multiple="multiple" id="e1" style="width: 300px">
    </select>--%>
    <div class="container1">
        <div style="overflow-x: auto; overflow-y: auto; height: 525px">
            <table>
                <thead>
                    <tr style="height: 40px;">
                        <th style="text-align: center; width: 50px;">Plan</th>
                        <th style="text-align: center; width: 80px;">Plan Type</th>
                        <th style="text-align: center; width: 430px;">Elligible Crops</th>
                        <th style="text-align: center;">Elligible States</th>
                        <th style="text-align: center;">Inelligible Practices</th>
                    </tr>
                </thead>
                <tbody>
                    <%--Hmax Standard--%>
                    <tr style="text-align: center;">
                        <td>Hmax</td>
                        <td>Standard</td>
                        <td>
                            <select id="HMAX_STANDARD_ELLIGIBLECROPS" style="width: 450px;" multiple="multiple"></select>
                            <%--<input type="text" id="HMAX_STANDARD_ELLIGIBLECROPS" class="form-control savetbox disabled"/>--%>
                        </td>
                        <td>
                            <input type="text" id="HMAX_STANDARD_ELLIGIBLESTATES" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="HMAX_STANDARD_INELLIGIBLEPRACTICES" class="form-control savetbox disabled" />
                        </td>
                    </tr>
                    <%--Hmax X1--%>
                    <tr style="text-align: center;">
                        <td>Hmax</td>
                        <td>X1</td>
                        <td>
                            <select id="HMAX_X1_ELLIGIBLECROPS" style="width: 450px" multiple="multiple"></select>
                            <%--<input type="text" id="HMAX_X1_ELLIGIBLECROPS" class="form-control savetbox disabled" />--%>
                        </td>
                        <td>
                            <input type="text" id="HMAX_X1_ELLIGIBLESTATES" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="HMAX_X1_INELLIGIBLEPRACTICES" class="form-control savetbox disabled" />
                        </td>
                    </tr>
                    <%--HMAX MAXRP--%>
                    <tr style="text-align: center;">
                        <td>Hmax</td>
                        <td>MaxRP</td>
                        <td>
                            <select id="HMAX_MAXRP_ELLIGIBLECROPS" style="width: 450px" multiple="multiple"></select>
                            <%--<input type="text" id="HMAX_MAXRP_ELLIGIBLECROPS" class="form-control savetbox disabled" />--%>
                        </td>
                        <td>
                            <input type="text" id="HMAX_MAXRP_ELLIGIBLESTATES" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="HMAX_MAXRP_INELLIGIBLEPRACTICES" class="form-control savetbox disabled" />
                        </td>
                    </tr>
                    <%--RAMP RY--%>
                    <tr style="text-align: center;">
                        <td>RAMP</td>
                        <td>RY</td>
                        <td>
                            <select id="RAMP_RY_ELLIGIBLECROPS" style="width: 450px" multiple="multiple"></select>
                            <%--<input type="text" id="RAMP_RY_ELLIGIBLECROPS" class="form-control savetbox disabled" />--%>
                        </td>
                        <td>
                            <input type="text" id="RAMP_RY_ELLIGIBLESTATES" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="RAMP_RY_INELLIGIBLEPRACTICES" class="form-control savetbox disabled" />
                        </td>
                    </tr>
                    <%--RAMP RR--%>
                    <tr style="text-align: center;">
                        <td>RAMP</td>
                        <td>RR</td>
                        <td>
                            <select id="RAMP_RR_ELLIGIBLECROPS" style="width: 450px" multiple="multiple"></select>
                            <%--<input type="text" id="RAMP_RR_ELLIGIBLECROPS" class="form-control savetbox disabled" />--%>
                        </td>
                        <td>
                            <input type="text" id="RAMP_RR_ELLIGIBLESTATES" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="RAMP_RR_INELLIGIBLEPRACTICES" class="form-control savetbox disabled" />
                        </td>
                    </tr>
                    <%--ICE BY--%>
                    <tr style="text-align: center;">
                        <td>ICE</td>
                        <td>BY</td>
                        <td>
                            <select id="ICE_BY_ELLIGIBLECROPS" style="width: 450px" multiple="multiple"></select>
                            <%--<input type="text" id="ICE_BY_ELLIGIBLECROPS" class="form-control savetbox disabled" />--%>
                        </td>
                        <td>
                            <input type="text" id="ICE_BY_ELLIGIBLESTATES" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_BY_INELLIGIBLEPRACTICES" class="form-control savetbox disabled" />
                        </td>
                    </tr>
                    <%--ICE BR--%>
                    <tr style="text-align: center;">
                        <td>ICE</td>
                        <td>BR</td>
                        <td>
                            <select id="ICE_BR_ELLIGIBLECROPS" style="width: 450px" multiple="multiple"></select>
                            <%--<input type="text" id="ICE_BR_ELLIGIBLECROPS" class="form-control savetbox disabled" />--%>
                        </td>
                        <td>
                            <input type="text" id="ICE_BR_ELLIGIBLESTATES" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_BR_INELLIGIBLEPRACTICES" class="form-control savetbox disabled" />
                        </td>
                    </tr>
                    <%--ICE CY--%>
                    <tr style="text-align: center;">
                        <td>ICE</td>
                        <td>CY</td>
                        <td>
                            <select id="ICE_CY_ELLIGIBLECROPS" style="width: 450px" multiple="multiple"></select>
                            <%--<input type="text" id="ICE_CY_ELLIGIBLECROPS" class="form-control savetbox disabled" />--%>
                        </td>
                        <td>
                            <input type="text" id="ICE_CY_ELLIGIBLESTATES" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_CY_INELLIGIBLEPRACTICES" class="form-control savetbox disabled" />
                        </td>
                    </tr>
                    <%--ICE CR--%>
                    <tr style="text-align: center;">
                        <td>ICE</td>
                        <td>CR</td>
                        <td>
                            <select id="ICE_CR_ELLIGIBLECROPS" style="width: 450px" multiple="multiple"></select>
                            <%--<input type="text" id="ICE_CR_ELLIGIBLECROPS" class="form-control savetbox disabled" />--%>
                        </td>
                        <td>
                            <input type="text" id="ICE_CR_ELLIGIBLESTATES" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ICE_CR_INELLIGIBLEPRACTICES" class="form-control savetbox disabled" />
                        </td>
                    </tr>
                    <%--ABC AY--%>
                    <tr style="text-align: center;">
                        <td>ABC</td>
                        <td>AY</td>
                        <td>
                            <select id="ABC_AY_ELLIGIBLECROPS" style="width: 450px" multiple="multiple"></select>
                            <%--<input type="text" id="ABC_AY_ELLIGIBLECROPS" class="form-control savetbox disabled" />--%>
                        </td>
                        <td>
                            <input type="text" id="ABC_AY_ELLIGIBLESTATES" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ABC_AY_INELLIGIBLEPRACTICES" class="form-control savetbox disabled" />
                        </td>
                    </tr>
                    <%--ABC AR--%>
                    <tr style="text-align: center;">
                        <td>ABC</td>
                        <td>AR</td>
                        <td>
                            <select id="ABC_AR_ELLIGIBLECROPS" style="width: 450px" multiple="multiple"></select>
                            <%--<input type="text" id="ABC_AR_ELLIGIBLECROPS" class="form-control savetbox disabled" />--%>
                        </td>
                        <td>
                            <input type="text" id="ABC_AR_ELLIGIBLESTATES" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="ABC_AR_INELLIGIBLEPRACTICES" class="form-control savetbox disabled" />
                        </td>
                    </tr>
                    <%--PCI - --%>
                    <tr style="text-align: center;">
                        <td>PCI</td>
                        <td>-</td>
                        <td>
                            <select id="PCI_ELLIGIBLECROPS" style="width: 450px" multiple="multiple"></select>
                            <%--<input type="text" id="PCI_ELLIGIBLECROPS" class="form-control savetbox disabled" />--%>
                        </td>
                        <td>
                            <input type="text" id="PCI_ELLIGIBLESTATES" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="PCI_INELLIGIBLEPRACTICES" class="form-control savetbox disabled" />
                        </td>
                    </tr>
                    <%--CROPHAIL BASIC --%>
                    <tr style="text-align: center;">
                        <td>CROPHAIL</td>
                        <td>BASIC</td>
                        <td>
                            <select id="CROPHAIL_BASIC_ELLIGIBLECROPS" style="width: 450px" multiple="multiple"></select>
                            <%--<input type="text" id="CROPHAIL_BASIC_ELLIGIBLECROPS" class="form-control savetbox disabled" />--%>
                        </td>
                        <td>
                            <input type="text" id="CROPHAIL_BASIC_ELLIGIBLESTATES" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="CROPHAIL_BASIC_INELLIGIBLEPRACTICES" class="form-control savetbox disabled" />
                        </td>
                    </tr>
                    <%--CROPHAIL COMPANION --%>
                    <tr style="text-align: center;">
                        <td>CROPHAIL</td>
                        <td>COMPANION</td>
                        <td>
                            <select id="CROPHAIL_COMPANION_ELLIGIBLECROPS" style="width: 450px" multiple="multiple"></select>
                            <%--<input type="text" id="CROPHAIL_COMPANION_ELLIGIBLECROPS" class="form-control savetbox disabled" />--%>
                        </td>
                        <td>
                            <input type="text" id="CROPHAIL_COMPANION_ELLIGIBLESTATES" class="form-control savetbox disabled" />
                        </td>
                        <td>
                            <input type="text" id="CROPHAIL_COMPANION_INELLIGIBLEPRACTICES" class="form-control savetbox disabled" />
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <script>

        $(function () {
            $('.disabled').prop('disabled', 'disabled');

            $("#HMAX_STANDARD_ELLIGIBLECROPS").select2();
            $("#PCI_ELLIGIBLECROPS").select2();
            $("#ABC_AR_ELLIGIBLECROPS").select2();
            $("#HMAX_X1_ELLIGIBLECROPS").select2();
            $("#RAMP_RY_ELLIGIBLECROPS").select2();
            $("#ICE_BR_ELLIGIBLECROPS").select2();
            $("#HMAX_MAXRP_ELLIGIBLECROPS").select2();
            $("#RAMP_RR_ELLIGIBLECROPS").select2();
            $("#ICE_BY_ELLIGIBLECROPS").select2();
            $("#ICE_CY_ELLIGIBLECROPS").select2();
            $("#ICE_CR_ELLIGIBLECROPS").select2();
            $("#ABC_AY_ELLIGIBLECROPS").select2();
            $("#CROPHAIL_BASIC_ELLIGIBLECROPS").select2();
            $("#CROPHAIL_COMPANION_ELLIGIBLECROPS").select2();
            var obj = {
                Init: function () {
                    obj.Getcrops();
                    obj.Getdata();
                    obj.Enable();
                    obj.Disable();

                    $('select').click(function (e) {

                        var id = e.target.id;
                        var value = $('#' + id + ' option:selected').toArray().map(item => item.text).join();
                        $.ajax({
                            type: "POST",
                            url: "insurencetest.aspx/UpdateKeysDetails",
                            data: JSON.stringify({ VKey: id, VValue: value }),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (data) {
                            },
                            failure: function (response) {
                            }
                        });

                    });
                    $(".savetbox").blur(function () {

                        var value = this.value;
                        var id = this.id;
                        $.ajax({
                            type: "POST",
                            url: "insurencetest.aspx/UpdateKeysDetails",
                            data: JSON.stringify({ VKey: id, VValue: value }),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (data) {
                            },
                            failure: function (response) {
                            }
                        });
                    });
                    $("td").addClass('disabled33');
                },

                Enable: function () {
                    $('#btnEnable').click(function () {
                        $('.disabled').prop('disabled', '');
                        $("td").removeClass("disabled33");
                    });
                },
                Disable: function () {
                    $('#btnReload').click(function () {
                        //  location.reload();
                        $('.disabled').prop('disabled', 'disabled');
                        $("td").addClass('disabled33');
                    });
                },
                Getdata: function () {
                    $.ajax({
                        type: "POST",
                        url: "insurencetest.aspx/GetinsDetails",
                        //  data: JSON.stringify({ lst: dataobj }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var objdata = JSON.parse(data.d);
                            for (var i = 0; i < objdata.length; i++) {
                                $('#' + objdata[i].ins_V_Key).val(objdata[i].Ins_V_Value);
                            }
                            for (var i = 0; i < objdata.length; i++) {
                                // $('#' + objdata[i].ins_V_Key).val("'" + objdata[1].Ins_V_Value.replace(/,/g, "','") + "'");
                                //var objvalue = objdata[i].Ins_V_Value.split(',');

                                $('#' + objdata[i].ins_V_Key).val(objdata[i].Ins_V_Value.split(',')).trigger('change');
                            }

                        },
                        failure: function (response) {
                        }
                    });
                },
                Getcrops: function () {
                    $.ajax({
                        type: "POST",
                        url: "insurencetest.aspx/Getcrops1",
                        //  data: JSON.stringify({ lst: dataobj }),
                        async: false,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {

                            $.each(res.d, function (data, value) {
                                //$("#e1").append($("<option></option>").val(data).html(value));
                                $("#HMAX_STANDARD_ELLIGIBLECROPS").append($("<option></option>").val(data).html(value));
                                $("#PCI_ELLIGIBLECROPS").append($("<option></option>").val(data).html(value));
                                $("#ABC_AR_ELLIGIBLECROPS").append($("<option></option>").val(data).html(value));
                                $("#HMAX_X1_ELLIGIBLECROPS").append($("<option></option>").val(data).html(value));
                                $("#RAMP_RY_ELLIGIBLECROPS").append($("<option></option>").val(data).html(value));
                                $("#ICE_BR_ELLIGIBLECROPS").append($("<option></option>").val(data).html(value));
                                $("#HMAX_MAXRP_ELLIGIBLECROPS").append($("<option></option>").val(data).html(value));
                                $("#RAMP_RR_ELLIGIBLECROPS").append($("<option></option>").val(data).html(value));
                                $("#ICE_BY_ELLIGIBLECROPS").append($("<option></option>").val(data).html(value));
                                $("#ICE_CY_ELLIGIBLECROPS").append($("<option></option>").val(data).html(value));
                                $("#ICE_CR_ELLIGIBLECROPS").append($("<option></option>").val(data).html(value));
                                $("#ABC_AY_ELLIGIBLECROPS").append($("<option></option>").val(data).html(value));
                                $("#CROPHAIL_BASIC_ELLIGIBLECROPS").append($("<option></option>").val(data).html(value));
                                $("#CROPHAIL_COMPANION_ELLIGIBLECROPS").append($("<option></option>").val(data).html(value));
                            });
                        },
                        failure: function (response) {
                        }
                    });
                }
            }
            obj.Init();
        });
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBodyScript" runat="Server">
</asp:Content>

