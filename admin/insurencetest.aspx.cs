﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_insurencetest : System.Web.UI.Page
{

    static string dbKey = "gp_conn";
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    [WebMethod]
    public static string GetinsDetails()
    {
        string sql_qry = "select ins_V_Key,Ins_V_Value from Ref_Ins_Validation";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
    }
    [WebMethod]
    public static Dictionary<string, string> Getcrops1()
    {
        string sql_qry = "select crop_id, crop_code from Ref_Crop";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        return dt.AsEnumerable().ToDictionary<DataRow, string, string>(row => row[1].ToString(),
                                       row => row[1].ToString());
    }
    [WebMethod]
    public static List<string> Getcrops()
    {
        string sql_qry = "select crop_id, crop_code from Ref_Crop";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(sql_qry, dbKey);
        List<string> emp = new List<string>();

        return (from DataRow row in dt.Rows
                select row["crop_code"].ToString()
               ).ToList();
    }

    [WebMethod]
    public static void UpdateKeysDetails(string VValue, string VKey)
    {
        string qry = "";
        qry = "select * from Ref_Ins_Validation where ins_V_Key='" + VKey + "'";
        DataTable dt = gen_db_utils.gp_sql_get_datatable(qry, dbKey);
        if (dt != null && dt.Rows.Count > 0)
        {
            qry = "update Ref_Ins_Validation set Ins_V_Value='" + VValue + "' where ins_V_Key='" + VKey + "'";
        }
        else
        {
            qry = "insert into Ref_Ins_Validation (ins_V_Key,Ins_V_Value)values('" + VKey + "','" + VValue + "')";
        }
        gen_db_utils.gp_sql_execute(qry, dbKey);
    }
}