﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Loan_validator : System.Web.UI.Page
{
    static string dbKey = "gp_conn";
    string loan_full_id;
    string str_temp = "";
    Hashtable h_expense_type_names = new Hashtable();

    protected void Page_Load(object sender, EventArgs e)
    {
        loan_full_id = Request.QueryString.GetValues("loan_full_id") == null ? "000001-000" : (Request.QueryString.GetValues("loan_full_id")[0].ToString());


        h_expense_type_names = get_expense_type_names();
        lbl_table.Text = get_core_counts();

        lbl_table.Text += get_farms();
     

        lbl_table.Text += get_all_budget_rows();
    }


    public Hashtable get_expense_type_names()
    {

       
        string str_sql = "select budget_expense_type_id,  budget_expense_name from ref_budget_expense_type ";
        Hashtable h1 = gen_db_utils.base_sql_get_hashtable(str_sql, dbKey);
      

   

        return h1;

    }

    public string get_farms()
    {

        string str_return = "";
         string str_sql = "select * from loan_farm where loan_full_id ='" + loan_full_id + "'";
        DataTable dt1 = gen_db_utils.gp_sql_get_datatable(str_sql, dbKey);
        str_return += " <table id='datatable_fixed_column' class='table table-striped table-bordered'> "
                              + "      <thead> "
                              + "     <tr> "
                              + "       <th >Farm_ID</th> "
                              + "       <th >Farm_state_ID</th> "
                              + "       <th >Farm_County_ID</th> "
                                 + "       <th >Farm_County_ID</th> "
                                    + "       <th >Farm_County_ID</th> "
                              + "    </tr> "
                              + "      </thead> "
                              + "        <tbody> ";

        foreach (DataRow dtr1 in dt1.Rows)
        {

            string state_county = "State: " + dtr1["Farm_State_ID"].ToString() + "<br>"
                                 + "County: " + dtr1["Farm_County_ID"].ToString();

            string fsn_section = "FSN: " + dtr1["fsn"].ToString() + "<br>"
                                 + "Section: " + dtr1["section"].ToString() + "<br>"
                                 + "percent_prod: " + dtr1["percent_prod"].ToString() + "<br>"
                                  + "Landowner: " + dtr1["Landowner"].ToString() + "<br>"
                                   + "Owned: " + dtr1["owned"].ToString() + "<br>"
                                 ;
            string group1 = "Rated: " + dtr1["Rated"].ToString() + "<br>"
                                 + "Share_Rent: " + dtr1["Share_Rent"].ToString() + "<br>"
                                 + "Cash_Rent_Total: " + dtr1["Cash_Rent_Total"].ToString() + "<br>"
                                 + "Cash_Rent_Per_Acre: " + dtr1["Cash_Rent_Per_Acre"].ToString() + "<br>"
                                   + "Cash_Rent_Due_date: " + dtr1["Cash_Rent_Per_Acre"].ToString() + "<br>"
                                   + "Cash_Rent_Paid: " + dtr1["Cash_Rent_Paid"].ToString();

                                 ;
            string group2 = "Permission_to_Insure: " + dtr1["Rated"].ToString() + "<br>"
                             + "Share_Rent: " + dtr1["Share_Rent"].ToString() + "<br>"
                             + "Rent_UOM: " + dtr1["Rent_UOM"].ToString() + "<br>"
                             + "Cash_Rent_Waived: " + dtr1["Cash_Rent_Waived"].ToString() + "<br>"
                             + "Cash_Rent_Waived_Amount: " + dtr1["Cash_Rent_Waived_Amount"].ToString() + "<br>"
                             + "Irr_Acres: " + dtr1["Irr_Acres"].ToString() + "<br>"
                             + "NI_Acres: " + dtr1["NI_Acres"].ToString() + "<br>"
                             + "Crop_Share_Detail_Indicator: " + dtr1["Crop_Share_Detail_Indicator"].ToString() + "<br>"
                             + "Status: " + dtr1["Status"].ToString() + "<br>"
                             + "IsDelete: " + dtr1["IsDelete"].ToString()
                                 ;

            str_return += "<tr>"
                              + "<td>" + dtr1["farm_id"].ToString() + "</td>"
                              + "<td>" + state_county + "</td>"
                              + "<td>" + fsn_section + "</td>"
                              + "<td>" + group1 + "</td>"
                              + "<td>" + group2 + "</td>"
                            
                              + "<td>" + "" + "</td>"
                              + "</tr>";

        }

        str_return += " </tbody> "
                                + " </table> ";


        return str_return;

    }


    public string get_all_budget_rows()
    {

        string str_return = "";
        string str_sql = "select * from loan_budget where loan_full_id ='" + loan_full_id + "' and isdelete <> 1 or isdelete is null ";
        DataTable dt1 = gen_db_utils.gp_sql_get_datatable(str_sql, dbKey);
        str_return += " <table id='datatable_fixed_column' class='table table-striped table-bordered'> "
                              + "      <thead> "
                              + "     <tr> "
                              + "       <th >Farm_ID</th> "
                              + "       <th >Farm_state_ID</th> "
                              + "       <th >Farm_County_ID</th> "
                                 + "       <th >Farm_County_ID</th> "
                                    + "       <th >Farm_County_ID</th> "
                              + "    </tr> "
                              + "      </thead> "
                              + "        <tbody> ";

        foreach (DataRow dtr1 in dt1.Rows)
        {


            ;
            string group1 = "(" + dtr1["expense_type_id"].ToString() + ") "
                                 + fc_gen_utils.hash_lookup(h_expense_type_names, dtr1["expense_type_id"].ToString(), "NA") ;
            //                     + "Cash_Rent_Total: " + dtr1["Cash_Rent_Total"].ToString() + "<br>"
            //                     + "Cash_Rent_Per_Acre: " + dtr1["Cash_Rent_Per_Acre"].ToString() + "<br>"
            //                       + "Cash_Rent_Due_date: " + dtr1["Cash_Rent_Per_Acre"].ToString() + "<br>"
            //                       + "Cash_Rent_Paid: " + dtr1["Cash_Rent_Paid"].ToString();

            //;
            //string group2 = "Permission_to_Insure: " + dtr1["Rated"].ToString() + "<br>"
            //                 + "Share_Rent: " + dtr1["Share_Rent"].ToString() + "<br>"
            //                 + "Rent_UOM: " + dtr1["Rent_UOM"].ToString() + "<br>"
            //                 + "Cash_Rent_Waived: " + dtr1["Cash_Rent_Waived"].ToString() + "<br>"
            //                 + "Cash_Rent_Waived_Amount: " + dtr1["Cash_Rent_Waived_Amount"].ToString() + "<br>"
            //                 + "Irr_Acres: " + dtr1["Irr_Acres"].ToString() + "<br>"
            //                 + "NI_Acres: " + dtr1["NI_Acres"].ToString() + "<br>"
            //                 + "Crop_Share_Detail_Indicator: " + dtr1["Crop_Share_Detail_Indicator"].ToString() + "<br>"
            //                 + "Status: " + dtr1["Status"].ToString() + "<br>"
            //                 + "IsDelete: " + dtr1["IsDelete"].ToString()
            //                     ;

            str_return += "<tr>"
                              + "<td>" + dtr1["loan_budget_id"].ToString() + "</td>"
                                + "<td>" + group1 + "</td>"
                              + "<td>" + dtr1["arm_budget_acre"].ToString() + "</td>"
                              + "<td>" + dtr1["distributor_budget_acre"].ToString() + "</td>"
                             + "<td>" + dtr1["third_party_budget_acre"].ToString() + "</td>"
                             + "<td>" + dtr1["total_budget_acre"].ToString() + "</td>"
                             + "<td>" + dtr1["third_party_budget_acre"].ToString() + "</td>"
                             + "<td>" + dtr1["arm_budget_crop"].ToString() + "</td>"
                             + "<td>" + dtr1["distributor_budget_crop"].ToString() + "</td>"
                             + "<td>" + dtr1["third_party_budget_crop"].ToString() + "</td>"
                             + "<td>" + dtr1["total_budget_crop_et"].ToString() + "</td>"

                              + "<td>" + "" + "</td>"
                              + "</tr>";

        }

        str_return += " </tbody> "
                                + " </table> ";


        return str_return;

    }


    public string get_each_budget()
    {

        string str_return = "";
        string str_sql = "select * from loan_budget where loan_full_id ='" + loan_full_id + "' and isdelete <> 1 or isdelete is null ";
        DataTable dt1 = gen_db_utils.gp_sql_get_datatable(str_sql, dbKey);
        str_return += " <table id='datatable_fixed_column' class='table table-striped table-bordered'> "
                              + "      <thead> "
                              + "     <tr> "
                              + "       <th >Farm_ID</th> "
                              + "       <th >Farm_state_ID</th> "
                              + "       <th >Farm_County_ID</th> "
                                 + "       <th >Farm_County_ID</th> "
                                    + "       <th >Farm_County_ID</th> "
                              + "    </tr> "
                              + "      </thead> "
                              + "        <tbody> ";

        foreach (DataRow dtr1 in dt1.Rows)
        {

            
                                 ;
            //string group1 = "Rated: " + dtr1["Rated"].ToString() + "<br>"
            //                     + "Share_Rent: " + dtr1["Share_Rent"].ToString() + "<br>"
            //                     + "Cash_Rent_Total: " + dtr1["Cash_Rent_Total"].ToString() + "<br>"
            //                     + "Cash_Rent_Per_Acre: " + dtr1["Cash_Rent_Per_Acre"].ToString() + "<br>"
            //                       + "Cash_Rent_Due_date: " + dtr1["Cash_Rent_Per_Acre"].ToString() + "<br>"
            //                       + "Cash_Rent_Paid: " + dtr1["Cash_Rent_Paid"].ToString();

            //;
            //string group2 = "Permission_to_Insure: " + dtr1["Rated"].ToString() + "<br>"
            //                 + "Share_Rent: " + dtr1["Share_Rent"].ToString() + "<br>"
            //                 + "Rent_UOM: " + dtr1["Rent_UOM"].ToString() + "<br>"
            //                 + "Cash_Rent_Waived: " + dtr1["Cash_Rent_Waived"].ToString() + "<br>"
            //                 + "Cash_Rent_Waived_Amount: " + dtr1["Cash_Rent_Waived_Amount"].ToString() + "<br>"
            //                 + "Irr_Acres: " + dtr1["Irr_Acres"].ToString() + "<br>"
            //                 + "NI_Acres: " + dtr1["NI_Acres"].ToString() + "<br>"
            //                 + "Crop_Share_Detail_Indicator: " + dtr1["Crop_Share_Detail_Indicator"].ToString() + "<br>"
            //                 + "Status: " + dtr1["Status"].ToString() + "<br>"
            //                 + "IsDelete: " + dtr1["IsDelete"].ToString()
            //                     ;

            str_return += "<tr>"
                              + "<td>" + dtr1["loan_budget_id"].ToString() + "</td>"
                              + "<td>" + dtr1["arm_budget_acre"].ToString() + "</td>"
                              + "<td>" + dtr1["distributor_budget_acre"].ToString() + "</td>"
                             + "<td>" + dtr1["third_party_budget_acre"].ToString() + "</td>"
                             + "<td>" + dtr1["total_budget_acre"].ToString() + "</td>"

                              + "<td>" + "" + "</td>"
                              + "</tr>";

        }

        str_return += " </tbody> "
                                + " </table> ";


        return str_return;

    }


    public string get_core_counts()
    {

        string str_return = "";
        string str_sql = "select count(*) from loan_farm where loan_full_id ='" + loan_full_id + "' and isDelete != 1 or isDelete is null";
        string str_count = gen_db_utils.gp_sql_scalar(str_sql, dbKey);

        str_return += " Number of Farms " + str_count + "<br>";

        str_sql = "select count(*) from loan_crop_unit where loan_full_id ='" + loan_full_id + "' and isDelete != 1 or isDelete is null";
        str_count = gen_db_utils.gp_sql_scalar(str_sql, dbKey);

        str_return += " Number of Loan Crop Units " + str_count + "<br>";
                         


        return str_return;

    }
}