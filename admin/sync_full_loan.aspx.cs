﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Newtonsoft.Json;

public partial class sync_full_loan : System.Web.UI.Page
{

    string loan_id = "0";
     string dbKey = "gp_conn";

    protected void Page_Load(object sender, EventArgs e)
    {
        loan_id = Request.QueryString.GetValues("loan_id") == null ? "1" : (Request.QueryString.GetValues("loan_id")[0].ToString());

      

         lbl_table.Text = "";

      //  lbl_table.Text = fill_table();

        lbl_table.Text += fill_farms_table(loan_id);

         lbl_table.Text += fill_agents_table(loan_id);

      //  lbl_table.Text += fill_farmunit_table();

       
        
        

  
    }


   


    protected string fill_farms_table(string inp_loan_id)
    {
        string str_return;

        str_return = "Farms <br><br> ";

        string sql_farms = " select * from loan_farm where loan_full_id = '" + inp_loan_id + "'  ";

        DataTable dt1 = gen_db_utils.gp_sql_get_datatable(sql_farms, dbKey);

        str_return += " <table id='datatable_fixed_column' class='table table-striped table-bordered'> "
                              + "      <thead> "
                              + "     <tr> "
                              + "       <th >Farm ID</th> "
                              + "       <th >State</th> "
                              + "       <th >County</th> "
                               + "       <th >FSN</th> "
                                + "       <th >Section</th> "
                                 + "       <th >Rated</th> "
                                  + "       <th >Owned</th> "
                                   + "       <th >LandOwner</th> "
                                    + "       <th >Share Rent</th> "
                                     + "       <th >Cash Rent Total</th> "
                                      + "       <th >County</th> "
                                       + "       <th >County</th> "
                                       + "       <th >County</th> "
                                       + "       <th >County</th> "
                                + "       <th >County</th> "
                              + "    </tr> "
                              + "      </thead> "
                              + "        <tbody> ";

        foreach (DataRow dtr1 in dt1.Rows)
        {
            
           
            str_return += "<tr>"
                          + "<td>" + dtr1["farm_id"].ToString() + "</td>"
                              + "<td>" + dtr1["farm_state_id"].ToString() + "</td>"
                              + "<td>" + dtr1["farm_county_id"].ToString() + "</td>"
                              + "<td>" + dtr1["FSN"].ToString() + "</td>"
                               + "<td>" + dtr1["Section"].ToString() + "</td>"
                                + "<td>" + dtr1["Rated"].ToString() + "</td>"
                                 + "<td>" + dtr1["owned"].ToString() + "</td>"
                                  + "<td>" + dtr1["landowner"].ToString() + "</td>"
                                   + "<td>" + dtr1["share_rent"].ToString() + "</td>"
                                    + "<td>" + dtr1["cash_rent_total"].ToString() + "</td>"
                                     + "<td>" + dtr1["cash_rent_per_acre"].ToString() + "</td>"
                                       + "<td>" + dtr1["cash_rent_due_date"].ToString() + "</td>"
                                         + "<td>" + dtr1["cash_rent_paid"].ToString() + "</td>"
                                           + "<td>" + dtr1["permission_to_insure"].ToString() + "</td>"
                                             + "<td>" + dtr1["cash_rent_waived"].ToString() + "</td>"
                                               + "<td>" + dtr1["cash_rent_waived_amount"].ToString() + "</td>"
                                                  + "<td>" + dtr1["irr_acres"].ToString() + "</td>"
                                                     + "<td>" + dtr1["ni_acres"].ToString() + "</td>"
                                                        + "<td>" + dtr1["crop_share_detail_indicator"].ToString() + "</td>"
                                                           + "<td>" + dtr1["Status"].ToString() + "</td>"
                                                           + "<td>" + dtr1["isdelete"].ToString() + "</td>"

                             + "</tr>";

        }

        str_return += " </tbody> "
                                + " </table> ";

        return str_return;

    }


    protected string fill_agents_table(string inp_loan_id)
    {
        string str_return;

        str_return = "Agents <br><br> ";

        string sql_farms = " select * from loan_association  where assoc_type_code = 'AGT' and loan_full_id = '" + inp_loan_id + "'  ";

        DataTable dt1 = gen_db_utils.gp_sql_get_datatable(sql_farms, dbKey);

        str_return += " <table id='datatable_fixed_column' class='table table-striped table-bordered'> "
                              + "      <thead> "
                              + "     <tr> "
                              + "       <th >Assoc ID</th> "
                              + "       <th >Assoc_Name</th> "
                              + "       <th >Contact</th> "
                               + "       <th >Location</th> "
                                + "       <th >Phone</th> "
                                 + "       <th >Email</th> "
                                  
                              + "    </tr> "
                              + "      </thead> "
                              + "        <tbody> ";

        foreach (DataRow dtr1 in dt1.Rows)
        {


            str_return += "<tr>"
                          + "<td>" + dtr1["Assoc_id"].ToString() + "</td>"
                              + "<td>" + dtr1["Loan_Full_ID"].ToString() + "</td>"
                              + "<td>" + dtr1["Assoc_Name"].ToString() + "</td>"
                              + "<td>" + dtr1["Contact"].ToString() + "</td>"
                               + "<td>" + dtr1["Location"].ToString() + "</td>"
                                + "<td>" + dtr1["Phone"].ToString() + "</td>"
                                
                                  + "<td>" + dtr1["email"].ToString() + "</td>"
                                 

                             + "</tr>";

        }

        str_return += " </tbody> "
                                + " </table> ";

        return str_return;

    }


}