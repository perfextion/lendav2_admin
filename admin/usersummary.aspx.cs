﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_usersummary : System.Web.UI.Page
{
    static string dbKey = "gp_conn";
    protected void Page_Load(object sender, EventArgs e)
    {
        lbl_table.Text = LoadTable("", "");
    }
    [WebMethod]
    public static string LoadTable(string Job_Title_ID, string Office_ID)
    {

        string str_sql = "";
        string str_return = "";

        str_sql = "SELECT a.Job_Title_ID,a.Office_ID,Office_Name,count(Office_Name) as officeidscount,concat(Job_Title,'(', count(Job_Title),')') as Job_Title FROM Users a" +
            " LEFT JOIN Job_Title b on a.Job_Title_ID = b.Job_Title_ID LEFT JOIN Ref_Office" +
            " c on a.Office_ID = c.Office_ID group by a.Job_Title_ID,a.Office_ID,Job_Title,Office_Name order by Office_Name,Job_Title";

        DataTable dt1 = gen_db_utils.gp_sql_get_datatable(str_sql, dbKey);
        var distinctValues = dt1.AsEnumerable()
                          .Select(row => new
                          {
                              Office_ID = row.Field<int?>("Office_ID"),
                              Office_Name = row.Field<string>("Office_Name")
                          })
                          .Distinct();

        str_return += "<div> <table id='tblActiveUsers' class='table table-striped table-bordered' >  "
                              + "<thead> "
                              + "     <tr> "
                              + "       <th >Office_Name</th> "
                              + "        <th > All Employees</th>"
                              + "        <th >Job_Title</th> "
                              + "    </tr> "
                              + "      </thead> "
                              + "        <tbody> ";

        foreach (var item in distinctValues)
        {

            var results = dt1.AsEnumerable()
                  .Where(s => s["Office_ID"].Equals(item.Office_ID))
                  .Select(row => new
                  {
                      Job_Title_ID = row.Field<int>("Job_Title_ID"),
                      Job_Title = row.Field<string>("Job_Title"),
                      officeidscount = row.Field<int>("officeidscount")
                  });

            string substring = "";
            var officeidscount = 0;
           
            foreach (var subitems in results)
            {
                 officeidscount = officeidscount+subitems.officeidscount;
                 substring += "<a href = '/admin/UserManagement.aspx?JobtitleID= " + subitems.Job_Title_ID + "&OfficeID=" + item.Office_ID + "' > " + subitems.Job_Title + " </a></br>";
            }
            int count1 = results.Count();

            str_return += "<tr>"
                              + "<td>" + item.Office_Name + "</td>"
                               + "<td><a href='/admin/UserManagement.aspx?OfficeID=" + item.Office_ID + "' > " + officeidscount + " </a></td>"
                              + "<td>" + substring + "</td>"
                               + "</tr>";

        }
        str_return += " </tbody> "
                                    + " </table> "
                                      + "</div></div></div >";

        return str_return;

    }
}