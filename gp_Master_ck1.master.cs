﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class rankadminmaster : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Username"] == null)
        {
            Response.Redirect("/Login.aspx");
            
        }
        if(Session["UserID"]==null)
        {
            Session["UserID"] ="-99";
        }
        lblUsername.Text = Session["Username"].ToString();
        lblUsername2.Text = Session["Username"].ToString();
        lbldatabasename.Text = gen_db_utils.get_database_name("gp_conn");
        
    }
 
}
