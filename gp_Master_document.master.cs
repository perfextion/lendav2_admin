﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class gp_Master_document : System.Web.UI.MasterPage
{
    int RoleID = 0;
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["username"] != null)
        {
            lblUsername.Text = Session["username"].ToString();
        }
        else
        {
            Response.Redirect("~/Login.aspx");
        }

        // lblUsername.Text = "Suresh";
        lblUsername2.Text = "Suresh";
        lbldatabasename.Text = gen_db_utils.get_database_name(ddlDBNames.Value);
        int RoleID = 1;
        CreateDynamicMenu(RoleID);
    }

    protected void CreateDynamicMenu(int RoleID)
    {
        if (RoleID == 1)
        {
            lblmenu.Text = gp_common_functions.get_admin_top_menu(RoleID.ToString());
        }
    }
}
