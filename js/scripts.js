﻿function NumberOnly(e, self, allowDecimal = true) {
    var charCode = (e.which) ? e.which : e.keyCode;

    if (allowDecimal) {
        if (((charCode != 46 || (charCode == 46 && $(self).val() == '')) || $(self).val().indexOf('.') != -1) && (charCode < 48 || charCode > 57)) {
            return false;
        }
    } else {
        if (charCode == 13 || (charCode > 31 && (charCode < 48 || charCode > 57))) {
            return false;
        }
    }
}

function unique(list) {
    var result = [];
    $.each(list, function (i, e) {
        if ($.inArray(e, result) == -1) result.push(e);
    });
    return result;
}

function toNumber(val) {
    let value = parseFloat(val);

    if (isNaN(value)) {
        value = 0;
    }
    return value;
}

function formatCurrency(val, decimals) {
    var value = toNumber(val);

    const formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'USD',
        minimumFractionDigits: decimals || 2
    });

    return formatter.format(value);
}

function formatNumber(val) {
    var value = toNumber(val);
    return parseFloat(value.toFixed(1)).toLocaleString('en-US');
}

jQuery.extend($.fn.fmatter, {
    numberFormatter: function (cellvalue, options = { minimumFractionDigits: 0, maximumFractionDigits: 0 }, rowdata) {
        var formatter = new Intl.NumberFormat('en-US', {
            minimumFractionDigits: options.minimumFractionDigits,
            maximumFractionDigits: options.minimumFractionDigits
        });

        if (cellvalue || cellvalue === 0 || cellvalue === '0') {
            let v = parseFloat(cellvalue);

            if (!isNaN(v)) {
                return formatter.format(v);
            } else {
                return cellvalue;
            }
        }
        return '';
    }
});

jQuery.extend($.fn.fmatter.numberFormatter, {
    unformat: function (cellvalue, options) {
        if (cellvalue) {
            return Number(cellvalue.replace(/[^0-9.-]+/g, ""));
        }
        return cellvalue;
    }
});

jQuery.extend($.fn.fmatter, {
    totalBudgetFormatter: function (cellvalue, options, rowdata) {
        var formatter = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'USD',
            minimumFractionDigits: 0,
            maximumFractionDigits: 0
        });

        if (cellvalue || cellvalue === 0 || cellvalue === '0') {
            return formatter.format(cellvalue);
        }
        return '';
    }
});

jQuery.extend($.fn.fmatter.totalBudgetFormatter, {
    unformat: function (cellvalue, options) {
        if (cellvalue) {
            return Number(cellvalue.replace(/[^0-9.-]+/g, ""));
        }
        return cellvalue;
    }
});


jQuery.extend($.fn.fmatter, {
    amountFormatter: function (cellvalue, options, rowdata) {
        var formatter = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'USD',
            minimumFractionDigits: 2,
            maximumFractionDigits: 2
        });

        if (cellvalue || cellvalue === 0 || cellvalue === '0') {
            let v = parseFloat(cellvalue);

            if (!isNaN(v)) {
                return formatter.format(v);
            } else {
                return cellvalue;
            }
        }
        return '';
    }
});

jQuery.extend($.fn.fmatter.amountFormatter, {
    unformat: function (cellvalue, options) {
        if (cellvalue) {
            return Number(cellvalue.replace(/[^0-9.-]+/g, ""));
        }
        return cellvalue;
    }
});

jQuery.extend($.fn.fmatter, {
    migrationAmountFormatter: function (cellvalue, options, rowdata) {
        var formatter = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'USD',
            minimumFractionDigits: 2,
            maximumFractionDigits: 2
        });
        
        if (cellvalue || cellvalue === 0 || cellvalue === '0') {
            let v = parseFloat(cellvalue);

            if (!isNaN(v)) {
                if (v == 0 && options.colModel.name.includes('diff')) {
                    return '-';
                }
                return formatter.format(v);
            } else {
                return cellvalue;
            }
        }
        return '';
    }
});

jQuery.extend($.fn.fmatter.migrationAmountFormatter, {
    unformat: function (cellvalue, options) {
        if (cellvalue) {
            return Number(cellvalue.replace(/[^0-9.-]+/g, ""));
        }
        return cellvalue;
    }
});

jQuery.extend($.fn.fmatter, {
    priceFormatter: function (cellvalue, options, rowdata) {
        var formatter = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'USD',
            minimumFractionDigits: 4,
            maximumFractionDigits: 4
        });

        if (cellvalue || cellvalue === 0 || cellvalue === '0') {
            return formatter.format(cellvalue);
        }
        return '';
    }
});

jQuery.extend($.fn.fmatter.priceFormatter, {
    unformat: function (cellvalue, options) {
        if (cellvalue != null || cellvalue != undefined) {
            return Number(cellvalue.replace(/[^0-9.-]+/g, ""));
        }
        return '';
    }
});


jQuery.extend($.fn.fmatter, {
    percentFormatter: function (cellvalue, options, rowdata) {
        var formatter = new Intl.NumberFormat('en-US', {
            style: 'percent',
            minimumFractionDigits: 1,
            maximumFractionDigits: 1
        });

        if (cellvalue || cellvalue === 0 || cellvalue === '0') {
            let v = parseFloat(cellvalue);

            if (!isNaN(v)) {
                return formatter.format(v / 100);
            } else {
                return cellvalue;
            }
        }
        return '';
    }
});

jQuery.extend($.fn.fmatter.percentFormatter, {
    unformat: function (cellvalue, options) {
        if (cellvalue != null || cellvalue != undefined) {
            return Number(cellvalue.replace(/[^0-9.-]+/g, ""));
        }
        return '';
    }
});


jQuery.extend($.fn.fmatter, {
    commonFormatter: function (cellvalue, options, rowdata) {
        if (cellvalue || cellvalue === 0 || cellvalue === '0') {
            var formatter = new Intl.NumberFormat('en-US', {
                style: 'percent',
                minimumFractionDigits: 1,
                maximumFractionDigits: 1
            });

            if (rowdata.Borrower_Rating_Code == "MAX") {
                formatter = new Intl.NumberFormat('en-US', {
                    style: 'currency',
                    currency: 'USD',
                    minimumFractionDigits: 0,
                    maximumFractionDigits: 0
                });
                return formatter.format(cellvalue);
            } else if (rowdata.Borrower_Rating_Code == 'FICO' || rowdata.Borrower_Rating_Code == 'YRF') {
                formatter = new Intl.NumberFormat('en-US');
                return formatter.format(cellvalue);
            } else if (rowdata.Borrower_Rating_Code == 'BR' || rowdata.Borrower_Rating_Code == 'CPA' || rowdata.Borrower_Rating_Code == 'TAX' || rowdata.Borrower_Rating_Code == 'BNK' || rowdata.Borrower_Rating_Code == 'JDG') {
                return cellvalue;
            }else {
                return formatter.format(cellvalue / 100);
            }
        }
        return '';
    }
});

jQuery.extend($.fn.fmatter.commonFormatter, {
    unformat: function (cellvalue, options) {
        if (cellvalue != null || cellvalue != undefined) {
            return Number(cellvalue.replace(/[^0-9.-]+/g, ""));
        }
        return '';
    }
});


jQuery.extend($.fn.fmatter, {
    assocAmountFormatter: function (cellvalue, options, rowdata) {
        var formatter = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'USD',
            minimumFractionDigits: 2,
            maximumFractionDigits: 2
        });

        if (rowdata.Assoc_Type_Code == 'THR') {
            if (cellvalue || cellvalue === 0 || cellvalue === '0') {
                return formatter.format(cellvalue);
            }
        }       
        return '';
    }
});

jQuery.extend($.fn.fmatter.assocAmountFormatter, {
    unformat: function (cellvalue, options) {
        if (cellvalue) {
            return Number(cellvalue.replace(/[^0-9.-]+/g, ""));
        }
        return cellvalue;
    }
});